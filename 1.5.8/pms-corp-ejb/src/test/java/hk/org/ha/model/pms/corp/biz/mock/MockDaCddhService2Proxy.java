package hk.org.ha.model.pms.corp.biz.mock;

import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("daCddhService2Proxy")
@Scope(ScopeType.APPLICATION)
public class MockDaCddhService2Proxy implements DaCddhServiceJmsRemote {

	@Override
	public List<DispOrderItem> retrieveDaDispOrderItem(CddhCriteria arg0) {
		// TODO Auto-generated method stub
		List<DispOrderItem> dispOrderItemLegacy = new ArrayList<DispOrderItem>();
		return dispOrderItemLegacy;
	}

	@Override
	public List<DispOrder> retrieveDispOrderLikePatName(CddhCriteria arg0, List<Patient> pasPatList) {
		// TODO Auto-generated method stub
		List<DispOrder> dispOrderList = new ArrayList<DispOrder>();
		return dispOrderList;
	}

	public List<DispOrderItem> retrieveDispOrderItemListForDuplicateItemCheck(
			DupChkDrugType arg0, String arg1, String arg2,
			List<String> arg3, String arg4, String arg5, Date arg6, Date arg7,
			String arg8, String arg9) {
		// TODO Auto-generated method stub
		return new ArrayList<DispOrderItem>();
	}

	@Override
	public List<DispOrderItem> retrieveDaDispOrderItemForCms(
			CddhDispenseInfoCriteria arg0) {
		// TODO Auto-generated method stub
		return new ArrayList<DispOrderItem>();
	}

	@Override
	public List<DispOrderItem> retrieveDaDispOrderItemForAsp(Date trxDate,
			String hospCode, List<String> itemCodeList,
			List<String> excludeWorkstoreList) {
		// TODO Auto-generated method stub
		return null;
	}

}
