package hk.org.ha.model.pms.corp.biz.order.mock;

import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderBatchProcessingType;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.service.biz.pms.interfaces.PmsServiceJmsRemote;

import java.util.Date;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.osoa.sca.annotations.OneWay;

@Startup
@Name("pmsServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockPmsServiceProxy implements PmsServiceJmsRemote {

	public void receiveNewOrder(MedOrder medOrder) {
	}

	public void receiveUpdateOrder(OpTrxType opTrxType, MedOrder medOrder) {
	}

	public void receiveConfirmRemark(MedOrder medOrder) {
	}
	
	public void receiveDiscontinue(OpTrxType opTrxType, MedOrder remoteMedOrder) {
	}
	
	public void receiveMedProfileOrder(MedProfileOrder medProfileOrder) {
	}
	
	public void updatePatient(List<MedOrder> arg0) {
	}

	@Override
	public void markProcessingFlagType(String arg0, List<Long> arg1, Boolean arg2, DispOrderBatchProcessingType arg3, int arg4) {
	}

	@Override
	public void updateDayEndCompleteInfoForDayEnd(String arg0, List<Long> arg1, Date arg2, String arg3, int arg4) {
	}

	@Override
	public void updateDayEndCompleteInfoForSendSuspend(String arg0, List<Long> arg1, List<Long> arg2, String arg3, String arg4, int arg5) {
	}

	@Override
	public void updateDayEndCompleteInfoForUncollect(String arg0, List<Long> arg1, List<Long> arg2, String arg3, int arg4) {
	}

	@Override
	public void markDhLatestFlag(String arg0, String arg1) {
	}

	@Override
	public void receiveChemoOrder(MedOrder arg0, List<MedProfileOrder> arg1, List<MedProfileOrder> arg2) {
	}

}
