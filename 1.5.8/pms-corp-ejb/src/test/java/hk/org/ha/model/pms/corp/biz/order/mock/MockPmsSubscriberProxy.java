package hk.org.ha.model.pms.corp.biz.order.mock;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.ActiveWard;
import hk.org.ha.model.pms.persistence.corp.WardAdminFreq;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import java.util.Date;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("pmsSubscriberProxy")
@Scope(ScopeType.APPLICATION)
public class MockPmsSubscriberProxy implements PmsSubscriberJmsRemote {

	@Override
	public void initJmsServiceProxy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void invalidateSession(Workstore workstore) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void initCache() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void initCacheByCluster(String clusterCode) {
		// TODO Auto-generated method stub
	}

	@Override
	public void initJmsServiceProxyByCluster(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void purgeMsWorkstoreDrug(Workstore arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void updateFollowUpWarningItemCode(List<String> arg0, Date arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createWaitTimeRptStat(Date batchDate, String clusterCode, Date createTime) {
	}
	
	@Override
	public void updateCapdSupplierItemForPms(List<CapdSupplierItem> arg0,
			Date arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePatientCatForPms(List<PatientCat> arg0, Date arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateSpecialtyForPms(List<Specialty> arg0, Date arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateWardForPms(List<Ward> arg0, Date arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateWardAdminFreqForPms(List<WardAdminFreq> arg0, Date arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestPmsSessionInfo(String arg0, Workstore arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestPmsSignatureCountResult(String requestId,
			Date batchDate, Date updateTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initCacheByClusterWithDmDrugList(String arg0, List<DmDrug> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void initCacheWithDmDrugList(List<DmDrug> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateActiveWardForPms(List<ActiveWard> arg0,
			List<String> arg1, Date arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateWardAdminFreqForPms(List<WardAdminFreq> arg0,
			List<String> arg1, Date arg2) {
		// TODO Auto-generated method stub
		
	}
}
