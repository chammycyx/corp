package hk.org.ha.model.pms.corp.biz.mock;

import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.corp.persistence.order.PatientEpisode;
import hk.org.ha.model.pms.corp.udt.order.PatientEpisodeType;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.patient.PasWard;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("pasServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockPasServiceProxy implements PasServiceJmsRemote {

	@Override
	public List<PasWard> retrieveIpasWard(String arg0) throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MedCase> retrievePasAppointment(String arg0, String arg1,
			String arg2, String arg3, String arg4, String arg5, String arg6,
			String arg7, String arg8) throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Patient retrievePasPatientByCaseNum(String arg0, String arg1)
			throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Patient retrievePasPatientByHkid(String arg0, String arg1,
			String arg2) throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Patient retrievePasPatientByPatKey(String arg0, String arg1,
			String arg2) throws PasException {
		// TODO Auto-generated method stub
		Patient patient = null;
		
		//30000005
		if (arg0 != null && arg0.equals("30000006")) {
			patient = new Patient();
			patient.setAddress("30000006address");
			patient.setHkid("A3345672");
			patient.setPatKey("30000006");
			patient.setPhone("12345678");
		}
		
		/*if (patient == null) {
			throw new PasException(new Exception());
		}*/
		return patient;
	}

	@Override
	public Patient retrievePasPatientCaseListByHkid(String arg0, String arg1,
			String arg2, String arg3) throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PasSpecialty> retrievePasSpecialty(String arg0)
			throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PasSpecialty> retrievePasSpecialtyWithSubSpecialty(String arg0)
			throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Patient> retrievePasPatientByNameSexAge(String arg0,
			Gender arg1, String arg2, String arg3) throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	private PatientEpisode createPatientEpisode(
			String patHospCode,
			String hkid,
			String patKey,
			String prevHkid,
			String prevPatKey,
			PatientEpisodeType type,
			Date trxDate) {
		
		PatientEpisode patEpisode = new PatientEpisode();
		patEpisode.setPatHospCode(patHospCode);
		patEpisode.setHkid(hkid);
		patEpisode.setPatKey(patKey);
		patEpisode.setPrevHkid(prevHkid);
		patEpisode.setPrevPatKey(prevPatKey);
		patEpisode.setType(type);
		patEpisode.setTrxDate(trxDate);
		
		return patEpisode;
	}
	
	@Override
	public List<PatientEpisode> retrievePatientEpisodeByTrxDate(Date arg0,
			Integer arg1) throws PasException {
		List<PatientEpisode> patEpisodeList = new ArrayList<PatientEpisode>();
		
		try {
			///////////////////////////////////////////////////1
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"B1234574", 
					"30000007", // Patient 2
					"B1234566", 
					"30000006",  //Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110914.151015")));
			/////////////////////////////////////////////////////
			// update by patient id
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"B1234590", 
					"30000009", // Patient 2
					"B1234582", 
					"30000008",  //Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110914.151015")));
			
			// update by prev id
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"B1234604", 
					"30000010", // Patient 2
					"B1234582", 
					"30000008",  //Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110914.151015")));
			
			// update by org pat id
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"B1234604", 
					"30000010", // Patient 2
					"B1234582", 
					"30000008",  //Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110914.151015")));
			
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"B1234612", 
					"30000011", // Patient 2
					"B1234582", 
					"30000008",  //Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110914.151015")));
			/////////////////////////////////////////////////////////////
			// update by patient id (5)
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"A1234571", 
					"30000002", // Patient 2
					"UM1234565", 
					"30000001",  //Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110915.151015")));
			
			// update by prev id
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"A123458A", 
					"30000003", // Patient 3
					"UM1234565", 
					"30000001", // Patient 1
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110916.151015")));
			
			//////////////////////////////////////////////
			// update hkid only
			patEpisodeList.add(createPatientEpisode(
					"QMH", 
					"A1234601",
					"30000004", // Patient 4
					"A1234598",
					"30000004", // Patient 4
					PatientEpisodeType.MergeHkid, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110917.151015")));//30000006
			
			// create patient required, 30000006, from 30000005
			patEpisodeList.add(createPatientEpisode(
					"QMH",
					"A3345672",
					"30000006", // Patient 6
					"A2345679",
					"30000005", // Patient 5
					PatientEpisodeType.UpdatePatientDemoMajorKey, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110918.121720")));
			
			// exception - PasException - status = E
			patEpisodeList.add(createPatientEpisode(
					"QMH",
					"A9999999",
					"39999999", // Patient 6
					"B1234620",
					"30000012", // Patient 5
					PatientEpisodeType.UpdatePatientDemoMajorKey, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110918.121720")));
			
			// exception - no such patient - status = X
			patEpisodeList.add(createPatientEpisode(
					"QMH",
					"A9999999",
					"39999999", // Patient 6
					"B1999999",
					"39999999", // Patient 5
					PatientEpisodeType.UpdatePatientDemoMajorKey, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110918.121720")));
			
			// exception - no such patient - status = X
			patEpisodeList.add(createPatientEpisode(
					"QMH",
					"B1234620",
					"30000012", // Patient 6
					"B1234639",
					"30000013", // Patient 5
					PatientEpisodeType.UpdatePatientDemoMajorKey, 
					new SimpleDateFormat("yyyyMMdd.HHmmss")
					.parse("20110918.121720")));
			
			return patEpisodeList;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

	@Override
	public Patient retrievePasPatientFullCaseListByHkid(String arg0, String arg1)
			throws PasException, UnreachableException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isPasPatientByHkid(String arg0, String arg1, String arg2)
			throws PasException, UnreachableException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Map<String, String> retrievePasPatientNameListByCaseNumList(
			String arg0, String arg1) throws PasException, UnreachableException {
		// TODO Auto-generated method stub
		return null;
	}

}
