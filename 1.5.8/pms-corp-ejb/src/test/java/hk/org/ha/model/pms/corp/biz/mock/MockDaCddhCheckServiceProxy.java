package hk.org.ha.model.pms.corp.biz.mock;

import hk.org.ha.model.pms.da.vo.cddh.DuplicateItemCheckCriteria;
import hk.org.ha.model.pms.da.vo.cddh.DuplicateItemCheckResult;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhCheckServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("daCddhCheckServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockDaCddhCheckServiceProxy implements DaCddhCheckServiceJmsRemote {

	public List<DuplicateItemCheckResult> retrieveDispOrderItemListByDispOrderForDuplicateItemCheck(
			List<DuplicateItemCheckCriteria> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
