package hk.org.ha.model.pms.corp.biz.test;

import static org.testng.AssertJUnit.assertNotNull;
import hk.org.ha.model.pms.corp.biz.PharmServiceLocal;
import hk.org.ha.model.pms.corp.vo.moe.ChargeRule;
import hk.org.ha.model.pms.corp.vo.moe.PrnDuration;
import hk.org.ha.model.pms.corp.vo.moe.RoutePrn;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PharmServiceTest extends SeamTest {

	@Test
	public void testGetRoutePrn() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				PharmServiceLocal pharmService = (PharmServiceLocal) getValue("#{pharmService}");
				List<RoutePrn> prnRouteList = pharmService.retrieveRoutePrnList("QMH", "WKS");

				assertNotNull( prnRouteList );
				for( RoutePrn routePrn : prnRouteList) {
					System.out.println(" routePrn : "+ToStringBuilder.reflectionToString(routePrn, ToStringStyle.MULTI_LINE_STYLE) );
				}
			}
		}.run();
	}
	
	@Test
	public void testGetPrnDuration() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				PharmServiceLocal pharmService = (PharmServiceLocal) getValue("#{pharmService}");
				PrnDuration prnDuration = pharmService.retrievePrnDuration("QMH", "WKS");
				
				assertNotNull( prnDuration );
				System.out.println(" prnDuration : "+ToStringBuilder.reflectionToString(prnDuration, ToStringStyle.MULTI_LINE_STYLE) );
			}
		}.run();
	}
	
	@Test
	public void testGetChargeRule() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				PharmServiceLocal pharmService = (PharmServiceLocal) getValue("#{pharmService}");
				ChargeRule chargeRule = pharmService.retrieveChargeRule("QMH", "WKS");
				
				assertNotNull( chargeRule );
				System.out.println(" chargeRule : "+ToStringBuilder.reflectionToString(chargeRule, ToStringStyle.MULTI_LINE_STYLE) );
			}
		}.run();
	}
}	
