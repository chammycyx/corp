package hk.org.ha.model.pms.corp.biz.test;

import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhServiceLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.model.pms.vo.printing.PrintOption;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CorpCddhServiceTest extends SeamTest {

	final String hospCode = "QMH";
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 

	/*@Test
	public void testRetrieveDispOrderLikePatName() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setPatientName("PATIENT,%");
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrder> dispOrderList = corpCddhService.retrieveDispOrderLikePatName(cddhCriteria, "Y");
				//assert dispOrderList.size() == 2;
				
				for (DispOrder dispOrder : dispOrderList){
					if (dispOrder.getId().equals(1L)){
						assert dispOrder.getPatName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0501");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(2L)){
						assert dispOrder.getPatName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0502");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					}
				}
			}
		}.run();
		
		// retrieve by name on label
		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setNameOnLabel("PATIENT,%");
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrder> dispOrderList = corpCddhService.retrieveDispOrderLikePatName(cddhCriteria, "Y");
				assert dispOrderList.size() == 3;
				
				for (DispOrder dispOrder : dispOrderList){
					if (dispOrder.getId().equals(1L)){
						assert dispOrder.getPatName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0501");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(2L)){
						assert dispOrder.getPatName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0502");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(3L)){
						assert dispOrder.getPatName().equals("PATIENT, UNKNOW1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0503");
						assert dispOrder.getPharmOrder().getId().equals(2L);
					}
				}
				
			}
		}.run();
	}*/
	
	@Test
	public void testRetrieveDispOrderLikePatNameOnLabel() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setNameOnLabel("PATIENT,%");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrder> dispOrderList = corpCddhService.retrieveDispOrderLikePatName(cddhCriteria);
				assert dispOrderList.size() == 3;
				
				for (DispOrder dispOrder : dispOrderList){
					if (dispOrder.getId().equals(1L)){
						assert dispOrder.getPharmOrder().getName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0501");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(2L)){
						assert dispOrder.getPharmOrder().getName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0502");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(3L)){
						assert dispOrder.getPharmOrder().getName().equals("PATIENT, UNKNOW1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0503");
						assert dispOrder.getPharmOrder().getId().equals(2L);
					}
				}
			}
		}.run();
		
		// retrieve by name on label
		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setNameOnLabel("PATIENT,%");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrder> dispOrderList = corpCddhService.retrieveDispOrderLikePatName(cddhCriteria);
				
				assert dispOrderList.size() == 3;
				
				for (DispOrder dispOrder : dispOrderList){
					if (dispOrder.getId().equals(1L)){
						assert dispOrder.getPharmOrder().getName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0501");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(2L)){
						assert dispOrder.getPharmOrder().getName().equals("PATIENT, SAMPLE1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0502");
						assert dispOrder.getPharmOrder().getId().equals(1L);
					} else if (dispOrder.getId().equals(3L)){
						assert dispOrder.getPharmOrder().getName().equals("PATIENT, UNKNOW1");
						assert dispOrder.getWorkstore().getHospCode().equals(hospCode);
						assert dispOrder.getWorkstore().getWorkstoreCode().equals("WKS1");
						assert dispOrder.getTicket().getTicketNum().equals("0503");
						assert dispOrder.getPharmOrder().getId().equals(2L);
					}
				}
				
			}
		}.run();
	}
	
	@Test
	public void testCddhRemarkUpdate() throws Exception {
		// create cddh remark
		new ComponentTest() { 
			protected void testComponents() throws Exception {
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setDispDateStart(df.parse("01/01/2008"));
				cddhCriteria.setDispDateEnd(df.parse("01/01/2015"));
				cddhCriteria.setHkid("A1234563");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);

				for(int i = 0; i < dispOrderItemList.size(); i++){
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);
					if (dispOrderItem.getId().equals(8L)) {
						dispOrderItem.setRemarkType(DispOrderItemRemarkType.ItemCancel);
						dispOrderItem.setRemarkText("DOCTOR REQUESTED TO CANCEL THE DISPENSING.");
						corpCddhService.updateDispOrderItemRemark(dispOrderItem, Boolean.FALSE);
					}
				}
			}
		}.run();
		
		// check remark
		new ComponentTest() { 
			protected void testComponents() throws Exception {
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setDispDateStart(df.parse("01/01/2008"));
				cddhCriteria.setDispDateEnd(df.parse("01/01/2015"));
				cddhCriteria.setHkid("A1234563");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);

				for(int i = 0; i < dispOrderItemList.size(); i++){
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);
					if (dispOrderItem.getId().equals(8L)) {
						assert dispOrderItem.getRemarkType().equals(DispOrderItemRemarkType.ItemCancel);
						assert dispOrderItem.getRemarkText().equals("DOCTOR REQUESTED TO CANCEL THE DISPENSING.");
					}
				}
			}
		}.run();
	}
	
	@Test
	public void testCddhRemarkDelete() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setDispDateStart(df.parse("01/01/2008"));
				cddhCriteria.setDispDateEnd(df.parse("01/01/2015"));
				cddhCriteria.setHkid("A1234563");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);

				for(int i = 0; i < dispOrderItemList.size(); i++){
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);
					if (dispOrderItem.getId().equals(8L)) {
						corpCddhService.deleteDispOrderItemRemark(dispOrderItem, Boolean.FALSE);
					}
				}
			}
		}.run();
		
		// check remark
		new ComponentTest() { 
			protected void testComponents() throws Exception {
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setDispDateStart(df.parse("01/01/2008"));
				cddhCriteria.setDispDateEnd(df.parse("01/01/2015"));
				cddhCriteria.setHkid("A1234563");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);
				
				for(int i = 0; i < dispOrderItemList.size(); i++){
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);
					if (dispOrderItem.getId().equals(8L)) {
						assert dispOrderItem.getRemarkType().equals(DispOrderItemRemarkType.Empty);
						assert dispOrderItem.getRemarkText() == null;
					}
				}
			}
		}.run();
	}
	
	@Test
	public void testRetrieveDispOrderItemByHkid() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setDispDateStart(df.parse("01/01/2008"));
				cddhCriteria.setDispDateEnd(df.parse("01/01/2015"));
				cddhCriteria.setHkid("A1234563");
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);
				assert dispOrderItemList.size() == 10;
				for(int i = 0; i < dispOrderItemList.size(); i++) {
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);

					if (dispOrderItem.getId().equals(8L)) {
						assert dispOrderItem.getWarnCode1().equals("7B");
						assert dispOrderItem.getWarnCode2().equals("10");
						assert dispOrderItem.getWarnCode3().equals("11");
						assert dispOrderItem.getWarnCode4().equals("16");
						
						assert dispOrderItem.getBaseLabel().getInstructionList().size() == 1;
						assert dispOrderItem.getBaseLabel().getInstructionList().get(0).getLineList().size() == 2;
						assert dispOrderItem.getBaseLabel().getWarningList().get(0).getLineList().size() == 2;
						
						
						//dispOrderItem.getDispLabel().setPrintLang("en");
						PrintOption printOption = new PrintOption();
						printOption.setPrintLang(PrintLang.Eng);
						dispOrderItem.getBaseLabel().setPrintOption(printOption);
						assert dispOrderItem.getBaseLabel().getInstructionText().equals("DISSOLVE UNDER THE TONGUE <1> TABLET(S) AT NOON\nDISSOLVE UNDER THE TONGUE <2> TABLET(S) AT NIGHT");
					}
				}
			}
		}.run();
		
		
	}
	
	@Test
	public void testRetrieveDispOrderItemByCaseNum() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				cddhCriteria.setDispDateStart(df.parse("01/01/2008"));
				cddhCriteria.setDispDateEnd(df.parse("01/01/2015"));
				cddhCriteria.setCaseNum("SOPD1010123A");
				cddhCriteria.setPatientHospcode(hospCode);
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);
				
				assert dispOrderItemList.size() == 10;
				for(int i = 0; i < dispOrderItemList.size(); i++) {
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);

					if (dispOrderItem.getId().equals(8L)) {
						assert dispOrderItem.getWarnCode1().equals("7B");
						assert dispOrderItem.getWarnCode2().equals("10");
						assert dispOrderItem.getWarnCode3().equals("11");
						assert dispOrderItem.getWarnCode4().equals("16");
						
						assert dispOrderItem.getBaseLabel().getInstructionList().size() == 1;
						assert dispOrderItem.getBaseLabel().getInstructionList().get(0).getLineList().size() == 2;
						assert dispOrderItem.getBaseLabel().getWarningList().get(0).getLineList().size() == 2;
						
						//dispOrderItem.getDispLabel().setPrintLang("en");
						PrintOption printOption = new PrintOption();
						printOption.setPrintLang(PrintLang.Eng);
						dispOrderItem.getBaseLabel().setPrintOption(printOption);
						assert dispOrderItem.getBaseLabel().getInstructionText().equals("DISSOLVE UNDER THE TONGUE <1> TABLET(S) AT NOON\nDISSOLVE UNDER THE TONGUE <2> TABLET(S) AT NIGHT");
					}
				}
			}
		}.run();
	}
	

	@Test
	public void testRetrieveExceptionDispOrderItem() throws Exception {

		// by exception case
		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				CddhCriteria cddhCriteria = new CddhCriteria();
				
				cddhCriteria.setNameOnLabel("PATIENT,");
				cddhCriteria.setDispHospCode(hospCode);
				cddhCriteria.setDispOrderId(3L);
				cddhCriteria.setDispOrderPatName("PATIENT, UNKNOW1");
				cddhCriteria.setPatListDispDate(df.parse("31/01/2011"));
				cddhCriteria.setHkpmiEnabled(Boolean.TRUE);
				cddhCriteria.setLegacyCddhIncluded(Boolean.TRUE);
				
				CorpCddhServiceLocal corpCddhService = (CorpCddhServiceLocal) getValue("#{corpCddhService}");
				List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);
				
				assert dispOrderItemList.size() == 1;
				for(int i = 0; i < dispOrderItemList.size(); i++) {
					DispOrderItem dispOrderItem = dispOrderItemList.get(i);

					if (dispOrderItem.getId().equals(10L)) {
						assert dispOrderItem.getWarnCode1().equals("7B");
						assert dispOrderItem.getWarnCode2().equals("10");
						assert dispOrderItem.getWarnCode3().equals("11");
						assert dispOrderItem.getWarnCode4().equals("16");
						
						assert dispOrderItem.getBaseLabel().getInstructionList().size() == 1;
						assert dispOrderItem.getBaseLabel().getInstructionList().get(0).getLineList().size() == 3;
						
						//dispOrderItem.getDispLabel().setPrintLang("en");
						PrintOption printOption = new PrintOption();
						printOption.setPrintLang(PrintLang.Eng);
						dispOrderItem.getBaseLabel().setPrintOption(printOption);
						assert dispOrderItem.getBaseLabel().getInstructionText().equals("Take with meal\nhalf tablet(s) in the morning and\n1 tablet(s) in the afternoon");
					}
				}
			}
		}.run();
	}
}	
