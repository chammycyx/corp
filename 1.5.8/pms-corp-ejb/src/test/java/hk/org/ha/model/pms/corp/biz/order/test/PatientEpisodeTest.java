package hk.org.ha.model.pms.corp.biz.order.test;

import hk.org.ha.model.pms.corp.batch.worker.order.PatientEpisodeLocal;
import hk.org.ha.model.pms.corp.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.persistence.disp.MedOrder;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PatientEpisodeTest extends SeamTest {

	final String hospCode = "QMH";
	
	// comment 'pmsServiceDispatcher.updatePatient(medOrderList);' code before running this test.

	@Test
	public void testPatientEpisode() throws Exception {

		new ComponentTest() { 
			protected void testComponents() throws Exception {
				
				OrderManagerLocal orderManager = (OrderManagerLocal) getValue("#{orderManager}");
				MedOrder beforeEpisodeMedOrder1 = orderManager.retrieveMedOrder("100003001");
				assert beforeEpisodeMedOrder1.getPrevPatientId() == null;
				assert beforeEpisodeMedOrder1.getPatient().getId().equals(7L);
				assert beforeEpisodeMedOrder1.getPatient().getPatKey().equals("30000001");
				assert beforeEpisodeMedOrder1.getPatient().getHkid().equals("UM1234565");
				
				MedOrder beforeEpisodeMedOrder2 = orderManager.retrieveMedOrder("100003002");
				assert beforeEpisodeMedOrder2.getPatient().getId().equals(10L);
				assert beforeEpisodeMedOrder2.getPatient().getHkid().equals("A1234598");
				assert beforeEpisodeMedOrder2.getPatient().getPatKey().equals("30000004");
				assert beforeEpisodeMedOrder2.getPrevPatientId() == null;
				
				MedOrder beforeEpisodeMedOrder3 = orderManager.retrieveMedOrder("100003003");
				assert beforeEpisodeMedOrder3.getPatient().getId().equals(11L);
				assert beforeEpisodeMedOrder3.getPatient().getHkid().equals("A2345679");
				assert beforeEpisodeMedOrder3.getPatient().getPatKey().equals("30000005");
				assert beforeEpisodeMedOrder3.getPrevPatientId() == null;
				
				PatientEpisodeLocal patientEpisodeService = (PatientEpisodeLocal) getValue("#{patientEpisodeBean}");
				patientEpisodeService.initBatch(null, Logger.getRootLogger());
				patientEpisodeService.processRecord(null);

				// test normal case, update patient, updated medOrder patientId
				MedOrder medOrderNormal1 = orderManager.retrieveMedOrder("100003006");
				medOrderNormal1.getPrevPatientId().equals(16L);
				medOrderNormal1.getPatient().getId().equals(9L);
				medOrderNormal1.getPatient().getPatKey().equals("100003007");
				medOrderNormal1.getPatient().getHkid().equals("B1234574");
				
				// test normal case, update by patientId, then by prevPatientId
				MedOrder medOrder1 = orderManager.retrieveMedOrder("100003001");
				assert medOrder1.getPrevPatientId().equals(8L);
				assert medOrder1.getPatient().getId().equals(9L);
				assert medOrder1.getPatient().getPatKey().equals("30000003");
				assert medOrder1.getPatient().getHkid().equals("A123458A");
				
				// test update of hkid only
				MedOrder medOrder2 = orderManager.retrieveMedOrder("100003002");
				assert medOrder2.getPatient().getHkid().equals("A1234601");
				assert medOrder2.getPatient().getId().equals(10L);
				assert medOrder2.getPatient().getPatKey().equals("30000004");
				assert medOrder2.getPrevPatientId() == null;
				
				// test the case with patient create
				MedOrder medOrder3 = orderManager.retrieveMedOrder("100003003");
				assert medOrder3.getPatient().getHkid().equals("A3345672");
				assert !medOrder3.getPatient().getId().equals(10L);
				assert medOrder3.getPatient().getPatKey().equals("30000006");
				assert medOrder3.getPrevPatientId().equals(11L);
				
				// the case that medOrder not found
				MedOrder medOrder4 = orderManager.retrieveMedOrder("100003004");
				assert medOrder4 == null;

				// the case that medOrder not found
				MedOrder medOrder5 = orderManager.retrieveMedOrder("100003005");
				assert medOrder5 == null;
				
				// move episode by original patient id
				MedOrder medOrderExceptCase_ByOrgPatientId = orderManager.retrieveMedOrder("100003007");
				assert medOrderExceptCase_ByOrgPatientId.getPatient().getHkid().equals("B1234612");
				assert medOrderExceptCase_ByOrgPatientId.getPatient().getId().equals(17L);
				assert medOrderExceptCase_ByOrgPatientId.getPatient().getPatKey().equals("30000011");
				assert medOrderExceptCase_ByOrgPatientId.getPrevPatientId().equals(16L);
				assert medOrderExceptCase_ByOrgPatientId.getOrgPatientId().equals(14L);
				
				// exceptional case, throw pas exception
				MedOrder medOrderExceptCase_PasException = orderManager.retrieveMedOrder("100003008");
				assert medOrderExceptCase_PasException.getPatient().getHkid().equals("B1234620");
				assert medOrderExceptCase_PasException.getPatient().getId().equals(18L);
				assert medOrderExceptCase_PasException.getPatient().getPatKey().equals("30000012");
				assert medOrderExceptCase_PasException.getPrevPatientId() == null;
				assert medOrderExceptCase_PasException.getOrgPatientId().equals(18L);
			}
		}.run();
	}
}	
