package hk.org.ha.model.pms.corp.biz.order.test;

import hk.org.ha.model.pms.corp.biz.order.CorpOrderServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.CorpPmsServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderItemType;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.Instruction;
import hk.org.ha.model.pms.vo.label.Warning;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CorpServiceTest extends SeamTest {

	final String hospCode = "QMH";

//	@Test
//	public void testVetting() throws Exception {
//
//		final String orderNum = "QMH1000001";
//
//		new ComponentTest() { 
//			protected void testComponents() throws Exception {
//				
//				// create MedOrder
//				MedOrder medOrder = createMedOrder(orderNum);
//		
//				CorpPmsServiceLocal corpPmsService = (CorpPmsServiceLocal) getValue("#{corpPmsService}");
//				CorpOrderServiceLocal  corpOrderService  = (CorpOrderServiceLocal) getValue("#{corpOrderService}");
//				corpOrderService.receiveNewOrder(medOrder);
//				
//				OrderManagerLocal orderManager = (OrderManagerLocal) getValue("#{orderManager}");
//				medOrder = orderManager.retrieveMedOrder(orderNum);
//				
//				// start vetting
//				StartVettingResult startVettingResult;
//				startVettingResult = corpPmsService.startVetting(medOrder.getOrderNum(), medOrder.getId(), hospCode);
//				
//				assert startVettingResult.getSuccessFlag();
//				assert !startVettingResult.getUpdateFlag();
//				assert startVettingResult.getMedOrder() == null;
//
//				// end vetting
//				createPharmOrderFromMedOrder(medOrder);
//
//				PharmOrder pharmOrder = (PharmOrder) getValue("#{pharmOrder}");
//				EndVettingResult endVettingResult = corpPmsService.endVetting(pharmOrder, null, hospCode, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
//				
//				assert endVettingResult.getSuccessFlag();
//				assert endVettingResult.getPharmOrder() != null;
//				
//				Set<Long> medOrderItemIdSet = new HashSet<Long>();
//				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
//					medOrderItemIdSet.add(medOrderItem.getId());
//				}
//				
//				Set<Long> pharmOrderItemIdSet = new HashSet<Long>();
//				for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
//					pharmOrderItemIdSet.add(pharmOrderItem.getId());
//				}
//				
//				for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
//					assert pharmOrderItem.getMedOrderItem() != null;
//					assert medOrderItemIdSet.contains(pharmOrderItem.getMedOrderItem().getId());
//				}
//				
//				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
//					assert medOrderItem.getPharmOrderItemList() != null;
//					assert medOrderItem.getPharmOrderItemList().size() == 1;
//					assert pharmOrderItemIdSet.contains(medOrderItem.getPharmOrderItemList().get(0).getId());
//				}
//				
//				// end dispensing
//				createPharmOrderFromMedOrder(medOrder);
//
//				PharmOrder localPharmOrder = medOrder.getPharmOrderList().get(0);
//
//				pharmOrderItemIdSet = new HashSet<Long>();
//				for (PharmOrderItem pharmOrderItem : localPharmOrder.getPharmOrderItemList()) {
//					pharmOrderItemIdSet.add(pharmOrderItem.getId());
//				}
//				
//				// create DispOrder
//				DispOrder localDispOrder = createDispOrderFromPharmOrder(localPharmOrder);
//
//				Set<Long> dispOrderItemIdSet = new HashSet<Long>();
//				for (DispOrderItem dispOrderItem : localDispOrder.getDispOrderItemList()) {
//					dispOrderItemIdSet.add(dispOrderItem.getId());
//				}
//					
//				// create Invoice
//				List<Invoice> invoiceList = createInvoiceListFromDispOrder(localDispOrder);
//				Invoice localInvoice = invoiceList.get(0);
//
//				Set<Long> invoiceItemIdSet = new HashSet<Long>();
//				for (InvoiceItem invoiceItem : localInvoice.getInvoiceItemList()) {
//					invoiceItemIdSet.add(invoiceItem.getId());
//				}
//
//				// send DispOrder
//				corpPmsService.saveDispOrder(localDispOrder, Boolean.FALSE, Boolean.FALSE);
//				PharmOrder remotePharmOrder = orderManager.retrieveMedOrder(medOrder.getOrderNum()).getPharmOrderList().get(0);
//
//				// test PharmOrder
//				assert remotePharmOrder != null;
//				assert remotePharmOrder.getId().equals(localPharmOrder.getId());
//				assert remotePharmOrder.getPharmOrderItemList().size() == localPharmOrder.getPharmOrderItemList().size();
//				for (PharmOrderItem pharmOrderItem : remotePharmOrder.getPharmOrderItemList()) {
//					assert pharmOrderItemIdSet.contains(pharmOrderItem.getId());
//					for (DispOrderItem dispOrderItem : pharmOrderItem.getDispOrderItemList()) {
//						dispOrderItemIdSet.contains(dispOrderItem.getId());
//					}
//				}
//				assert remotePharmOrder.getDispOrderList() != null;
//				assert remotePharmOrder.getDispOrderList().size() == 1;
//
//				// test DispOrder
//				DispOrder remoteDispOrder = remotePharmOrder.getDispOrderList().get(0);
//				assert remoteDispOrder != null;
//				assert remoteDispOrder.getId().equals(localDispOrder.getId());
//				assert remoteDispOrder.getDispOrderItemList().size() == localDispOrder.getDispOrderItemList().size();
//				for (DispOrderItem dispOrderItem : remoteDispOrder.getDispOrderItemList()) {
//					assert dispOrderItemIdSet.contains(dispOrderItem.getId());
//					assert pharmOrderItemIdSet.contains(dispOrderItem.getPharmOrderItem().getId());
//					for (InvoiceItem invoiceItem : dispOrderItem.getInvoiceItemList()) {
//						assert invoiceItemIdSet.contains(invoiceItem.getId());
//					}
//				}
//				assert remoteDispOrder.getInvoiceList() != null;
//				assert remoteDispOrder.getInvoiceList().size() == 1;
//				
//				// test Invoice
//				Invoice remoteInvoice = remoteDispOrder.getInvoiceList().get(0);
//				assert remoteInvoice != null;
//				assert remoteInvoice.getId().equals(localInvoice.getId());
//				assert remoteInvoice.getInvoiceItemList().size() == localInvoice.getInvoiceItemList().size();
//				for (InvoiceItem invoiceItem : remoteInvoice.getInvoiceItemList()) {
//					assert invoiceItemIdSet.contains(invoiceItem.getId());
//					assert dispOrderItemIdSet.contains(invoiceItem.getDispOrderItem().getId());
//				}
//			}
//		}.run();
//	}
//	
//	@Test(expectedExceptions={Exception.class})
//	public void testStartVettingWithInvalidOrderNum() throws Exception {
//
//		new ComponentTest() {
//			protected void testComponents() throws Exception {
//
//				CorpPmsServiceLocal corpPmsService = (CorpPmsServiceLocal) getValue("#{corpPmsService}");
//				MedOrder medOrder = createMedOrder("00000000");
//				corpPmsService.startVetting(medOrder.getOrderNum(), medOrder.getId(), hospCode);
//			}
//		}.run();
//	}
//
//	@Test
//	public void testStartVettingWithLockedOrder() throws Exception {
//
//		final String orderNum = "QMH1000002";
//
//		// start vetting with locked order
//		new ComponentTest() {
//			protected void testComponents() throws Exception {
//
//				// create MedOrder
//				StartVettingResult result;
//				MedOrder medOrder = createMedOrder(orderNum);
//
//				CorpPmsServiceLocal corpPmsService = (CorpPmsServiceLocal) getValue("#{corpPmsService}");
//				CorpOrderServiceLocal  corpOrderService  = (CorpOrderServiceLocal) getValue("#{corpOrderService}");
//				corpOrderService.receiveNewOrder(medOrder);
//				
//				OrderManagerLocal orderManager = (OrderManagerLocal) getValue("#{orderManager}");
//				medOrder = orderManager.retrieveMedOrder(orderNum);
//				
//				result = corpPmsService.startVetting(medOrder.getOrderNum(), medOrder.getId(), "ERR");
//				result = corpPmsService.startVetting(medOrder.getOrderNum(), medOrder.getId(), hospCode);
//				
//				assert !result.getSuccessFlag();
//			}
//		}.run();
//	}
//
//	@Test
//	public void testStartVettingWithCompletedOrder() throws Exception {
//
//		final String orderNum = "QMH1000003";
//
//		// start vetting with completed order
//		new ComponentTest() {
//			protected void testComponents() throws Exception {
//				
//				StartVettingResult result;
//				
//				MedOrder medOrder = createMedOrder(orderNum);
//				medOrder.setStatus(MedOrderStatus.Complete);
//
//				CorpPmsServiceLocal corpPmsService = (CorpPmsServiceLocal) getValue("#{corpPmsService}");
//				CorpOrderServiceLocal  corpOrderService  = (CorpOrderServiceLocal) getValue("#{corpOrderService}");
//				
//				corpOrderService.receiveNewOrder(medOrder);
//				result = corpPmsService.startVetting(medOrder.getOrderNum(), medOrder.getId(), hospCode);
//				
//				assert result.getSuccessFlag();
//			}
//		}.run();
//	}
//
//	@Test
//	public void testCancelVetting() throws Exception {
//
//		final String orderNum = "QMH1000004";
//
//		new ComponentTest() {
//			protected void testComponents() throws Exception {
//
//				// create MedOrder
//				MedOrder medOrder = createMedOrder(orderNum);
//		
//				CorpPmsServiceLocal corpPmsService = (CorpPmsServiceLocal) getValue("#{corpPmsService}");
//				CorpOrderServiceLocal  corpOrderService  = (CorpOrderServiceLocal) getValue("#{corpOrderService}");
//				corpOrderService.receiveNewOrder(medOrder);
//				
//				OrderManagerLocal orderManager = (OrderManagerLocal) getValue("#{orderManager}");
//				medOrder = orderManager.retrieveMedOrder(orderNum);
//				
//				// start vetting
//				StartVettingResult requestResult;
//				requestResult = corpPmsService.startVetting(medOrder.getOrderNum(), medOrder.getId(), hospCode);
//				
//				assert requestResult.getSuccessFlag();
//				assert !requestResult.getUpdateFlag();
//				assert requestResult.getMedOrder() == null;
//
//				// cancel vetting
//				corpPmsService.cancelVetting(orderNum, hospCode);
//			}
//		}.run();
//	}
//
//	private static MedOrder createMedOrder(String orderNum) {
//
//		MedOrder medOrder = new MedOrder();
//   	 
// 		medOrder.setDocType(MedOrderDocType.Normal);
// 		medOrder.setOrderNum(orderNum);
//		
// 		Calendar orderDate = Calendar.getInstance();
// 		orderDate.clear();
// 		orderDate.set(2011, 0, 15);	
// 		medOrder.setOrderDate(orderDate.getTime());
//     
// 		medOrder.setRefNum("0012");
// 		medOrder.setPrescType(MedOrderPrescType.Out);
// 		medOrder.setStatus(MedOrderStatus.Outstanding);
//		
// 		MedOrderCharge medOrderCharge = new MedOrderCharge();
//		medOrderCharge.setStatus("A");
// 		
// 		MedOrderChargeItem mOCI = new MedOrderChargeItem();
//		mOCI.setAmount(10.0);
//		mOCI.setStatus("A");
//		medOrderCharge.addMedOrderChargeItem(mOCI);
// 		
//		MedOrderItem medOrderItem = new MedOrderItem();
//		medOrderItem.setItemCode("PARA01");
//		medOrderItem.setActionStatus(ActionStatus.DispByPharm);
//		medOrderItem.setFirstDisplayName("ACET TESTING");
//		medOrderItem.setIssueQty(0);
//		medOrderItem.setBaseUnit("CAP");
//		medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
//		medOrderItem.setItemNum(0);
//		medOrderItem.setOrgItemNum(0);				
//		medOrderItem.setMedOrderChargeItem(mOCI);
//		
// 		MedOrderChargeItem mOCI2 = new MedOrderChargeItem();
//		mOCI2.setAmount(20.0);
//		mOCI2.setStatus("A");
//		medOrderCharge.addMedOrderChargeItem(mOCI2);
// 		
//		MedOrderItem medOrderItem2 = new MedOrderItem();
//		medOrderItem2.setItemCode("PARA02");
//		medOrderItem2.setActionStatus(ActionStatus.DispByPharm);
//		medOrderItem2.setFirstDisplayName("PARA TESTING");
//		medOrderItem2.setIssueQty(0);
//		medOrderItem2.setBaseUnit("CAP");
//		medOrderItem2.setStatus(MedOrderItemStatus.Outstanding);
//		medOrderItem2.setItemNum(0);
//		medOrderItem2.setOrgItemNum(0);				
//		medOrderItem2.setMedOrderChargeItem(mOCI2);				
//		
//		MedOrderItemAlert mOIA = new MedOrderItemAlert();
//		mOIA.setAckBy("MO001");
//		//mOIA.setAdditionInfo("Confirmed with drug-sensitivity test");
//		mOIA.setOverrideReason("No other alternative available");
//		medOrderItem2.addMedOrderItemAlert(mOIA);
//		
//		Patient patient = new Patient();
//		patient.setId(new Long(1));
//		patient.setSex(Gender.Male);
//		
//		medOrder.setDoctorCode("MO001");				
//		medOrder.setSpecCode("MED");     		
//
// 		Workstore workstore = new Workstore();
// 		workstore.setHospCode("QMH");
// 		workstore.setWorkstoreCode("WKS1");
//			
// 		medOrder.setPatHospCode("QMH");
// 		medOrder.setMedOrderCharge(medOrderCharge);
// 		medOrder.setPatient(patient);
// 		medOrder.setWorkstore(workstore);
// 		medOrder.addMedOrderItem(medOrderItem);
// 		medOrder.addMedOrderItem(medOrderItem2);
// 		
// 		medOrder.setMedOrderFmList(new ArrayList<MedOrderFm>());
// 		
// 		return medOrder;
//	}
//	
//	private static void createPharmOrderFromMedOrder(MedOrder medOrder) {
//
//		PharmOrder pharmOrder = new PharmOrder();
//
//		pharmOrder.setId(Long.valueOf(10001));
//		pharmOrder.setMedOrder(medOrder);
//		pharmOrder.setSpecCode("MED");
//		pharmOrder.setWardCode("5A");
//		pharmOrder.setPatCatCode("AC");
//		pharmOrder.setNumOfRefill(4);
//		pharmOrder.setNumOfGroup(4);
//		pharmOrder.setStatus(PharmOrderStatus.Outstanding);
//		pharmOrder.setWorkstore(medOrder.getWorkstore());
//		pharmOrder.setDispOrderList(new ArrayList<DispOrder>());
//
//		Long pharmOrderItemId = Long.valueOf(10001);
//		
//		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
//
//			PharmOrderItem pharmOrderItem = new PharmOrderItem();
//			
//			pharmOrderItem.setId(pharmOrderItemId++);
//			pharmOrderItem.setItemCode(medOrderItem.getItemCode());
//			pharmOrderItem.setItemType(PharmOrderItemType.General);
//			pharmOrderItem.setRegimen(new Regimen());
//			pharmOrderItem.setDoseQty(Integer.valueOf(1));
//			pharmOrderItem.setDoseUnit("MG");
//			pharmOrderItem.setCalQty(BigDecimal.ONE);
//			pharmOrderItem.setAdjQty(BigDecimal.ONE);
//			pharmOrderItem.setIssueQty(Integer.valueOf(1));
//			pharmOrderItem.setBaseUnit("MG");
//			pharmOrderItem.setStatus(PharmOrderItemStatus.Outstanding);
//			pharmOrderItem.setDispOrderItemList(new ArrayList<DispOrderItem>());
//
//			pharmOrder.addPharmOrderItem(pharmOrderItem);
//			medOrderItem.setPharmOrderItemList(new ArrayList<PharmOrderItem>());
//			medOrderItem.getPharmOrderItemList().add(pharmOrderItem);
//			pharmOrderItem.setMedOrderItem(medOrderItem);
//		}
//		
//		medOrder.setPharmOrderList(new ArrayList<PharmOrder>());
//	}
//	
//	private static DispOrder createDispOrderFromPharmOrder(PharmOrder pharmOrder) {
//
//		Long ticketId = Long.valueOf(2001);
//		String ticketNum = "0689";
//		
//		MedOrder medOrder = pharmOrder.getMedOrder();
//
//		DispOrder dispOrder = new DispOrder();
//		pharmOrder.getDispOrderList().add(dispOrder);
//
//		dispOrder.setId(Long.valueOf(1234));
//		dispOrder.setDispDate(Calendar.getInstance().getTime());
//		dispOrder.setPatName(medOrder.getPatient().getName());
//		dispOrder.setStatus(DispOrderStatus.Vetted);
//		dispOrder.setPharmOrder(pharmOrder);
//		dispOrder.setWorkstore(pharmOrder.getWorkstore());
//		dispOrder.setRefillScheduleList(new ArrayList<RefillSchedule>());
//		dispOrder.setInvoiceList(new ArrayList<Invoice>());
//
//		Long dispOrderItemId = Long.valueOf(10001);
//
//		for (PharmOrderItem pharmOrderItem : pharmOrder.getPharmOrderItemList()) {
//
//			DispOrderItem dispOrderItem = new DispOrderItem();
//			dispOrderItem.setId(dispOrderItemId++);
//			dispOrderItem.setDispQty(pharmOrderItem.getIssueQty());
//			
//			dispOrderItem.setWarnCode1("warnCode1");
//			dispOrderItem.setWarnCode2("warnCode2");
//			dispOrderItem.setWarnCode3("warnCode3");
//			dispOrderItem.setWarnCode4("warnCode4");
//            
//            dispOrderItem.setRemarkType(DispOrderItemRemarkType.Empty);
//			dispOrderItem.setPharmOrderItem(pharmOrderItem);
//			dispOrderItem.setStatus(DispOrderItemStatus.Vetted);
//			dispOrderItem.setInvoiceItemList(new ArrayList<InvoiceItem>());
//
//			dispOrderItem.setWorkstationCode("WS01");
//			
//			dispOrder.addDispOrderItem(dispOrderItem);
//			
//			pharmOrderItem.getDispOrderItemList().add(dispOrderItem);
//		}
//
//		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
//
//			DispLabel dispLabel = new DispLabel();
//			dispLabel.setInstructionList(new ArrayList<Instruction>());
//			dispLabel.setWarningList(new ArrayList<Warning>());
//			dispOrderItem.setDispLabel(dispLabel);
//		}
//		
//		// associate ticket to DispOrder
//		Ticket ticket = new Ticket();
//		ticket.setId(ticketId);
//		ticket.setTicketDate(Calendar.getInstance().getTime());
//		ticket.setTicketNum(ticketNum);
//		ticket.setWorkstore(pharmOrder.getWorkstore());
//		dispOrder.setTicket(ticket);
//
//		return dispOrder;
//	}
//	
//	private static List<Invoice> createInvoiceListFromDispOrder(DispOrder dispOrder) {
//
//        List<DispOrderItem> stdDispOrderItemList = new ArrayList<DispOrderItem>();	            
//        List<Invoice> invoiceList = new ArrayList<Invoice>();
//        
//        
//		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
//			stdDispOrderItemList.add(dispOrderItem);				
//		}
//		
//		if( stdDispOrderItemList.size() > 0 ){
//			invoiceList.add( createInvoiceForStdItem(stdDispOrderItemList) );
//		}
//		
//		dispOrder.setInvoiceList(invoiceList);
//		return invoiceList;
//	}
//	
//	private static Invoice createInvoiceForStdItem(List<DispOrderItem> stdDispOrderItemList){
//		
//		Invoice stdInvoice = new Invoice();
//		stdInvoice.setId(Long.valueOf(1234));
//		BigDecimal totalAmount = BigDecimal.ZERO;		
//		DispOrder dispOrder = stdDispOrderItemList.get(0).getDispOrder();
//		dispOrder.getPharmOrder().getMedOrder();
//		
//		Long invoiceItemId = Long.valueOf(10001);
//		
//		for ( DispOrderItem dispOrderItem : stdDispOrderItemList ){
//						
//			dispOrderItem.getPharmOrderItem().getMedOrderItem();
//			
//			InvoiceItem invoiceItem = new InvoiceItem();
//			invoiceItem.setId(invoiceItemId++);
//			invoiceItem.setCost(BigDecimal.ZERO);				
//			invoiceItem.setMarkupAmount(BigDecimal.ZERO);
//			
//			if( dispOrderItem.getPharmOrderItem().getMedOrderItem().getMedOrderChargeItem() != null ){			
//				BigDecimal chargeableAmount = new BigDecimal(dispOrderItem.getPharmOrderItem().getMedOrderItem().getMedOrderChargeItem().getAmount());
//				invoiceItem.setHandleAmount( chargeableAmount );
//			}
//
//			invoiceItem.setAmount(invoiceItem.getHandleAmount());
//			totalAmount = totalAmount.add(invoiceItem.getAmount());
//			
//			invoiceItem.setHandleExemptAmount(BigDecimal.ZERO);
//            invoiceItem.setHandleExemptUser("user");	            
//            invoiceItem.setStatus("I");	            
//            invoiceItem.setDispOrderItem(dispOrderItem);				
//			stdInvoice.addInvoiceItem(invoiceItem);
//		}
//		stdInvoice.setDocType(InvoiceDocType.Standard);
//		stdInvoice.setDispOrder(dispOrder);
//		stdInvoice.setTotalAmount(totalAmount.setScale(0, RoundingMode.HALF_UP));
//		stdInvoice.setForceProceedFlag(false);
//		
//		stdInvoice.setStatus(InvoiceStatus.Outstanding);
//		
//		stdInvoice.setInvoiceNum("I");		
//			
//		return stdInvoice;
//	}
}	
