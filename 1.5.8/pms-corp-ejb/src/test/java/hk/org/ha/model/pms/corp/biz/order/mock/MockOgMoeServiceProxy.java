package hk.org.ha.model.pms.corp.biz.order.mock;

import hk.org.ha.model.pms.corp.vo.PrnPropertyResult;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.udt.disp.DispTrxType;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.service.biz.pms.og.interfaces.OgServiceJmsRemote;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("ogMoeServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockOgMoeServiceProxy implements OgServiceJmsRemote {
	@Override
	public void sendOpOrderToCms(OpTrxType opTrxType, DispOrder dispOrder) {	
	}

	@Override
	public void sendMpOrderToCms(MpTrxType mpTrxType, MedProfileMoItem medProfileMoItem, Replenishment replenishment) {
	}
		
	@Override
	public void sendOrderToEpr(DispTrxType dispTrxType, DispOrder dispOrder) {
	}

	@Override
	public void sendPrnPropertyToCms(OpTrxType opTrxType, PrnPropertyResult prnPropertyResult) {
	}
}
