package hk.org.ha.model.pms.corp.biz.mock;

import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhRepServiceJmsRemote;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("daCddhRepServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockDaCddhRepServiceProxy implements DaCddhRepServiceJmsRemote {

	@Override
	public void createCddhRecord(DispOrder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeOutpatDispOrder(DispOrder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateRemark(DispOrderItem arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createCddhRecordForIp(List<DispOrder> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateDispStatus(DispOrder arg0, Date arg1) {
		// TODO Auto-generated method stub
		
	}
}
