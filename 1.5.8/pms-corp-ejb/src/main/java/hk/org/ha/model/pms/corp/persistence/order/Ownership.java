package hk.org.ha.model.pms.corp.persistence.order;

import hk.org.ha.model.pms.persistence.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "OWNERSHIP")
public class Ownership extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ownershipSeq")
	@SequenceGenerator(name = "ownershipSeq", sequenceName = "SQ_OWNERSHIP", initialValue = 100000000)
	private Long id;

	@Column(name = "ORDER_NUM", nullable = false, unique= true, length = 12)
	private String orderNum;
	
	@Column(name = "OWNER")
	private String owner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
