package hk.org.ha.model.pms.corp.biz.asp;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.asp.persistence.AspHospSpec;
import hk.org.ha.model.pms.asp.persistence.AspItem;
import hk.org.ha.model.pms.corp.biz.HospitalMappingManagerLocal;

@AutoCreate
@Stateless
@Name("corpAspService")
@MeasureCalls
public class CorpAspServiceBean implements CorpAspServiceLocal {

	@In
	AspReftableManagerLocal aspReftableManager;
	
	@In
	HospitalMappingManagerLocal hospitalMappingManager;
	
	@Override
	public AspHosp retrieveAspHospByHospCode(String hospCode) {
		
		return aspReftableManager.retrieveAspHospByHospCode(hospCode);
	}

	@Override
	public void updateAspHosp(AspHosp aspHosp) {
		aspReftableManager.updateAspHosp(aspHosp);
	}

	@Override
	public List<AspHosp> retrieveAspHospList() {
		return aspReftableManager.retrieveAspHospList();
	}

	@Override
	public Map<String, AspItem> retrieveAspItemMap() {
		return aspReftableManager.retrieveAspItemMap();
	}

	@Override
	public void updateAspHospSpec(List<AspHospSpec> aspHospSpecList) {
		aspReftableManager.updateAspHospSpecList(aspHospSpecList);
	}
	
	@Override
	public List<String> retrieveAspItemCodeList() {
		return aspReftableManager.retrieveAspItemCodeList();
	}

	@Override
	public List<String> retrievePatHospCodeList(String dispHospCode) {
		return hospitalMappingManager.retrievePatHospCodeList(dispHospCode);
	}
}
