package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;
import java.util.Date;

public class WardStock implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String patHospCode;
	private String itemCode;
	private String ward;
	private String phsWardCode;
	private String displayname;
	private String formCode;
	private String saltProperty;
	private Date updateDate;
	
	public WardStock() {		
	}
	
	public WardStock( 
			String itemCode, String phsWardCode) {
		this.itemCode = itemCode;
		this.phsWardCode = phsWardCode;
	}
	
	
	
	public String getPatHospCode() {
		return patHospCode;
	}



	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}



	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getWard() {
		return ward;
	}
	public void setWard(String ward) {
		this.ward = ward;
	}
	


	@Override
	public String toString() {
		return "WardStock [displayname=" + displayname + ", formCode="
				+ formCode + ", patHospCode=" + patHospCode
				+ ", itemCode=" + itemCode + ", phsWardCode="
				+ phsWardCode + ", saltProperty=" + saltProperty
				+ ", updateDate=" + updateDate + ", ward=" + ward
				+ "]";
	}



	public String getPhsWardCode() {
		return phsWardCode;
	}



	public void setPhsWardCode(String phsWardCode) {
		this.phsWardCode = phsWardCode;
	}



	public String getDisplayname() {
		return displayname;
	}



	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}



	public String getFormCode() {
		return formCode;
	}



	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}



	public String getSaltProperty() {
		return saltProperty;
	}



	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}



	public Date getUpdateDate() {
		return updateDate;
	}



	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((displayname == null) ? 0 : displayname.hashCode());
		result = prime * result
				+ ((formCode == null) ? 0 : formCode.hashCode());
		result = prime * result
				+ ((itemCode == null) ? 0 : itemCode.hashCode());
		result = prime * result
				+ ((patHospCode == null) ? 0 : patHospCode.hashCode());
		result = prime * result
				+ ((phsWardCode == null) ? 0 : phsWardCode.hashCode());
		result = prime * result
				+ ((saltProperty == null) ? 0 : saltProperty.hashCode());
		result = prime * result
				+ ((updateDate == null) ? 0 : updateDate.hashCode());
		result = prime * result + ((ward == null) ? 0 : ward.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WardStock other = (WardStock) obj;
		if (displayname == null) {
			if (other.displayname != null)
				return false;
		} else if (!displayname.equals(other.displayname))
			return false;
		if (formCode == null) {
			if (other.formCode != null)
				return false;
		} else if (!formCode.equals(other.formCode))
			return false;
		if (itemCode == null) {
			if (other.itemCode != null)
				return false;
		} else if (!itemCode.equals(other.itemCode))
			return false;
		if (patHospCode == null) {
			if (other.patHospCode != null)
				return false;
		} else if (!patHospCode.equals(other.patHospCode))
			return false;
		if (phsWardCode == null) {
			if (other.phsWardCode != null)
				return false;
		} else if (!phsWardCode.equals(other.phsWardCode))
			return false;
		if (saltProperty == null) {
			if (other.saltProperty != null)
				return false;
		} else if (!saltProperty.equals(other.saltProperty))
			return false;
		if (updateDate == null) {
			if (other.updateDate != null)
				return false;
		} else if (!updateDate.equals(other.updateDate))
			return false;
		if (ward == null) {
			if (other.ward != null)
				return false;
		} else if (!ward.equals(other.ward))
			return false;
		return true;
	}
	
	public String getKey() {
		return patHospCode + '\n' + itemCode + '\n' + ward;
	}
	
	
}
