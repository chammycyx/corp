package hk.org.ha.service.biz.pms.corp.interfaces;

import java.util.List;

import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.disp.OpTrxType;

import org.osoa.sca.annotations.OneWay;

public interface CorpOrderServiceJmsRemote {

	@OneWay
	void receiveMedOrder(OpTrxType opTrxType, MedOrder remoteMedOrder);		
	
	@OneWay
	void receiveMedProfileOrder(MedProfileOrder remoteMedProfileOrder);

	@OneWay
	void receiveChemoOrder(MedOrder medOrder, List<MedProfileOrder> medProfileOrderList);
	
	@OneWay
	void receivePatientTrx(MedProfileOrder medProfileOrder);
}
