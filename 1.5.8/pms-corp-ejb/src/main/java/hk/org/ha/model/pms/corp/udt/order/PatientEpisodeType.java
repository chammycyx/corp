package hk.org.ha.model.pms.corp.udt.order;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PatientEpisodeType implements StringValuedEnum {

	MergeHkid("020", "Merge HKID"),
	UpdatePatientDemoMajorKey("030", "Update Major Patient Demographic Data"),
	UpdateFromPseudoHkidToRealHkid("031", "Update HKID from pseudo HKID to real HKID"),
	MoveEpisode("040", "Move Episode from HKID to another HKID");
	
    private final String dataValue;
    private final String displayValue;
        
    PatientEpisodeType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PatientEpisodeType dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(PatientEpisodeType.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<PatientEpisodeType> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PatientEpisodeType> getEnumClass() {
    		return PatientEpisodeType.class;
    	}
    }
}
