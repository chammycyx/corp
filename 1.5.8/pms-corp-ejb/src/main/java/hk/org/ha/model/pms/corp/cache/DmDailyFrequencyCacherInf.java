package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.udt.vetting.DailyFreqSort;

import java.util.List;

public interface DmDailyFrequencyCacherInf extends BaseCacherInf {

    DmDailyFrequency getDmDailyFrequencyByDailyFreqCode(String DailyFreqCode);
	
	List<DmDailyFrequency> getDmDailyFrequencyList( DailyFreqSort sortSeq );
	
	List<DmDailyFrequency> getDmDailyFrequencySuspendList( DailyFreqSort sortSeq );
}
