package hk.org.ha.model.pms.corp.biz.order;


import hk.org.ha.service.biz.pms.corp.interfaces.CorpOrderServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface CorpOrderServiceLocal extends CorpOrderServiceJmsRemote {
}
