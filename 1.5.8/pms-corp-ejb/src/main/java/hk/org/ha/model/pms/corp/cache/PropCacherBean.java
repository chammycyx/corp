package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.PropEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Stateless
@Name("propCacher")
@MeasureCalls
public class PropCacherBean extends BaseCacher implements PropCacherLocal {
		
	private static final int PROP_TIMEOUT_FOR_NULL = 10*60*1000; // 10 mins

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
		
    private static Map<StringArray, WorkstorePropCacher> workstorePropCacherMap = new HashMap<StringArray, WorkstorePropCacher>();		
    private static Map<String, HospitalPropCacher> hospitalPropCacherMap = new HashMap<String, HospitalPropCacher>();	
    private static CorporatePropCacher corporatePropCacher;	
                        
    public PropEntity getPropEntity(Workstore workstore, String name) {
    	if (workstore == null) {
    		throw new IllegalArgumentException("Missing workstore");
    	}
		return this.getWorkstorePropCacher(workstore).getProp(name);
    }        

    public PropEntity getPropEntity(Hospital hospital, String name) {
    	if (hospital == null) {
    		throw new IllegalArgumentException("Missing hospital");
    	}
		return this.getHospitalPropCacher(hospital).getProp(name);
    }        
    
    public PropEntity getPropEntity(String name) {
		return this.getCorporatePropCacher().getProp(name);
    }        

    public void clearWorkstoreCache(Workstore workstore) {
    	
		StringArray key = new StringArray(
				workstore.getHospCode(), 
				workstore.getWorkstoreCode());    	
		
    	synchronized (workstorePropCacherMap) {
    		workstorePropCacherMap.remove(key);
    	}
    }
        
    private WorkstorePropCacher getWorkstorePropCacher(Workstore workstore) {
    	
    	synchronized (workstorePropCacherMap) {
    		        	
    		StringArray key = new StringArray(
    				workstore.getHospCode(), 
    				workstore.getWorkstoreCode());

    		WorkstorePropCacher workstorePropCacher = workstorePropCacherMap.get(key);
	    	if (workstorePropCacher == null) {
	    		workstorePropCacher = new WorkstorePropCacher(workstore);
	    		workstorePropCacherMap.put(key, workstorePropCacher);
	    	}

	    	return workstorePropCacher;
    	}
    }
    
    private HospitalPropCacher getHospitalPropCacher(Hospital hospital) {
    	synchronized (hospitalPropCacherMap) {

    		String key = hospital.getHospCode();
    		
	    	HospitalPropCacher hospitalPropCacher = hospitalPropCacherMap.get(key);
	    	if (hospitalPropCacher == null) {
	    		hospitalPropCacher = new HospitalPropCacher(hospital);
	    		hospitalPropCacherMap.put(key, hospitalPropCacher);
	    	}
	    	
	    	return hospitalPropCacher;
    	}
    }    
    
    private CorporatePropCacher getCorporatePropCacher() {
    	synchronized (this) {
	    	if (corporatePropCacher == null) {
	    		corporatePropCacher = new CorporatePropCacher();
	    	}		    	
	    	return corporatePropCacher;
    	}
    }    
	    
	// Workstore
	private class WorkstorePropCacher extends ExtendedCacher<String, WorkstoreProp>
	{		
        private Workstore workstore = null;

        public WorkstorePropCacher(Workstore workstore) {
              this.workstore = workstore;
        }
        		
		@SuppressWarnings("unchecked")
		public WorkstoreProp create(String name) {
			List<WorkstoreProp>  list = em.createQuery(
					"select o from WorkstoreProp o" + // 20120307 index check : WorkstoreProp.workstore,prop : UI_WORKSTORE_PROP_01
					" where o.workstore = :workstore" +
					" and o.prop.name = :name")
					.setParameter("workstore", workstore)
					.setParameter("name", name)
					.getResultList();
			return (!list.isEmpty()) ? list.get(0) : new WorkstoreProp(); 			
		}

		public PropEntity getProp(String name) {
			WorkstoreProp workstoreProp = this.get(name);
			if (workstoreProp.getId() == null) {
				return PropCacherBean.this.getHospitalPropCacher(workstore.getHospital()).getProp(name);
			} else {
				return workstoreProp;
			}
		}

		public Collection<WorkstoreProp> createAll() {
			throw new UnsupportedOperationException();
		}

		public String retrieveKey(WorkstoreProp arg0) {
			throw new UnsupportedOperationException();
		}
	}

	// Hospital
	private class HospitalPropCacher extends ExtendedCacher<String, HospitalProp>
	{		
        private Hospital hospital = null;

        public HospitalPropCacher(Hospital hospital) {
              this.hospital = hospital;
        }
		
		@SuppressWarnings("unchecked")
		public HospitalProp create(String name) {
			List<HospitalProp>  list = em.createQuery(
					"select o from HospitalProp o" + // 20120307 index check : HospitalProp.hospital,prop : UI_HOSPITAL_PROP_01
					" where o.hospital = :hospital" +
					" and o.prop.name = :name")
					.setParameter("hospital", hospital)
					.setParameter("name", name)
					.getResultList();
			return (!list.isEmpty()) ? list.get(0) : new HospitalProp(); 			
		}
		
		public PropEntity getProp(String name) {
			HospitalProp hospitalProp = this.get(name);
			if (hospitalProp.getId() == null) {
				return PropCacherBean.this.getCorporatePropCacher().getProp(name);
			} else {
				return hospitalProp;
			}
		}
				
		public Collection<HospitalProp> createAll() {
			throw new UnsupportedOperationException();
		}

		public String retrieveKey(HospitalProp arg0) {
			throw new UnsupportedOperationException();
		}
	}
	
	//Corporate
	private class CorporatePropCacher extends ExtendedCacher<String, CorporateProp>
	{		
		@SuppressWarnings("unchecked")
		public CorporateProp create(String name) {
			List<CorporateProp>  list = em.createQuery(
					"select o from CorporateProp o" + // 20120307 index check : CorporateProp.prop : UI_CORPORATE_PROP_01
					" where o.prop.name = :name")
					.setParameter("name", name)
					.getResultList();
			return (!list.isEmpty()) ? list.get(0) : new CorporateProp(); 			
		}

		public PropEntity getProp(String name) {
			return this.get(name);
		}
		
		public Collection<CorporateProp> createAll() {
			throw new UnsupportedOperationException();
		}

		public String retrieveKey(CorporateProp arg0) {
			throw new UnsupportedOperationException();
		}
	}
		
	public static PropCacherLocal instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		} 
		return (PropCacherLocal) Component.getInstance("propCacher",
				ScopeType.APPLICATION);
	}
	
	private abstract class ExtendedCacher<K,V extends PropEntity> extends AbstractCacher<K, V>
	{				
		public ExtendedCacher() {
			super(0);
		}

		@Override
		public int extendExpireTime(PropEntity propEntity) {
			if (propEntity.getProp() == null) {
				return PROP_TIMEOUT_FOR_NULL;
			} else {
				return propEntity.getProp().getCacheExpireTime() * 1000;
			}
		}
	}		
}
