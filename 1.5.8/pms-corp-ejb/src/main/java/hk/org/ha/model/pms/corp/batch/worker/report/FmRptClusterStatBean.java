package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.report.FmReportType;

import java.util.Arrays;
import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

/**
 * Session Bean implementation class DayendMainBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("fmRptClusterStatBean")
@MeasureCalls
public class FmRptClusterStatBean implements FmRptClusterStatLocal {
		
	@In
	private FmRptManagerLocal fmRptManager;
	
	@In
	private FmRptFilesManagerLocal fmRptFilesManager;
		
	private Logger logger = null;
	private String jobId = null;
	private String clusterCode = null;
	private DateTime startDate = null;
	
	private Date batchDate;
	private DateTime batchStartDate = null;
	
    /**
     * Default constructor. 
     */
    public FmRptClusterStatBean() {
        // TODO Auto-generated constructor stub
    }

	public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void endChunk() throws Exception {
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        } else {
        	batchDate = params.getDate("batchDate");
        	batchStartDate = new DateTime(batchDate).dayOfMonth().withMinimumValue();
        }
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
	
	public void processRecord(Record record) throws Exception {
    	clusterCode = record.getString("CLUSTER_CODE");
    	startDate = new DateTime();
    	logger.info("Generate Cluster Statistic Report for " + clusterCode + " with batch date " + batchStartDate + " start ");
    	long startTime;
    	long generateMonthlyClusterSpecStatReportTime;
    	long generateMonthlyClusterStatReportTime;
    	long generateYearlyAccumulatedClusterStatReportTime;
    	
    	try {
    		startTime = processBeginTime();
	    	fmRptManager.generateMonthlyClusterSpecStatReport(batchStartDate, clusterCode, jobId);
	    	generateMonthlyClusterSpecStatReportTime = totalPorcessTime(startTime);
	    	
	    	startTime = processBeginTime();
	    	fmRptManager.generateMonthlyClusterStatReport(batchStartDate, clusterCode, jobId);
	    	generateMonthlyClusterStatReportTime = totalPorcessTime(startTime);
	    	
	    	startTime = processBeginTime();
	    	fmRptManager.generateYearlyAccumulatedClusterStatReport(batchStartDate, clusterCode, jobId);
	    	generateYearlyAccumulatedClusterStatReportTime = totalPorcessTime(startTime);
	    	
	    	fmRptFilesManager.moveToDestDirectory(batchStartDate, "ALL", clusterCode, jobId, startDate, 
	    									 Arrays.asList(FmReportType.MonthlyClusterSpecStat, FmReportType.MonthlyClusterStat, 
	    											 	   FmReportType.YearlyAccumulatedClusterStat));
	    	
	    	logger.info("Generate cluster statistic report for cluster " + clusterCode + " completed" +
    				"\n1. generateMonthlyClusterSpecStatReport time used: " + generateMonthlyClusterSpecStatReportTime +
    				"\n2. generateMonthlyClusterStatReport time used: " + generateMonthlyClusterStatReportTime + 
    				"\n3. generateYearlyAccumulatedClusterStatReport time used: " + generateYearlyAccumulatedClusterStatReportTime);
    	} catch (Exception e) {
    		logger.info(jobId + " with Cluster  " + clusterCode + " Fail. Move files to error folder");
    		fmRptFilesManager.moveToErrorDirectory(jobId, startDate);
    		throw e;
		}
    }
	
	private long processBeginTime()
	{
		return System.currentTimeMillis();
	}
	
	private long totalPorcessTime(long startTime)
	{
		return ((int)(System.currentTimeMillis() - startTime));
	}
}
