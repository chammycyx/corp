package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmFormCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmFormCacher extends BaseCacher implements DmFormCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;
		
	private List<DmForm> cachedDmFormListByAllUsageType;
	
	private List<DmForm> cachedDmFormListByOutPatOnly = new ArrayList<DmForm>();

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
			cachedDmFormListByAllUsageType = null;
			cachedDmFormListByOutPatOnly.clear();
		}
	}
	
	public DmForm getFormByFormCodeByAllUsageType(String formCode)
	{
		getCachedDmFormList();
		synchronized (this) {
	    	for (DmForm dmForm : cachedDmFormListByAllUsageType)
	    	{
	    		if (dmForm.getFormCode().equals(formCode))
	    		{
	    			return dmForm;
	    		}
	    	}
		}
		
    	return null;
	}
	
    public DmForm getFormByFormCode(String formCode) {
    	getCachedDmFormList();
    	
		synchronized (this) {
	    	for (DmForm dmForm : cachedDmFormListByOutPatOnly)
	    	{
	    		if (dmForm.getFormCode().equals(formCode))
	    		{
	    			return dmForm;
	    		}
	    	}
		}
		
    	return null;
	}
    
	public List<DmForm> getFormList() {
		getCachedDmFormList();
		
		synchronized (this) {
			return cachedDmFormListByOutPatOnly;
		}
	}

	public List<DmForm> getFormListByFormCode(String prefixFormCode) {
		List<DmForm> ret = new ArrayList<DmForm>();
		boolean found = false;
		for (DmForm dmForm : getFormList()) {
			if (dmForm.getFormCode().startsWith(prefixFormCode)) {
				ret.add(dmForm);
				found = true;
			} else {
				if (found) {
					break;
				}
			}
		}
		return ret;
	}

	public List<String> getDistinctFormDesc() {
		HashMap<String, Boolean> formDescMap = new HashMap<String, Boolean>();
		List<String> retFormDescList = new ArrayList<String>();
		for (DmForm dmForm : getFormList()) {
			String moeDesc;
			if (dmForm.getMoeDesc() == null) {
				moeDesc = "";
			} else {
				moeDesc = dmForm.getMoeDesc();
			}
			
			if ( ! formDescMap.containsKey(moeDesc) ) {
				retFormDescList.add(moeDesc);
				formDescMap.put(moeDesc, Boolean.TRUE);
			}
		}
		return retFormDescList;
	}
	
	 private void getCachedDmFormList()
	    {
	    	synchronized (this)
			{
	    		List<DmForm> dmFormList = (List<DmForm>) this.getCacher().getAll();
	    		if (cachedDmFormListByAllUsageType == null || (cachedDmFormListByAllUsageType != dmFormList)) 
				{
					// if yes just clear everything
	    			cachedDmFormListByOutPatOnly.clear();
	    		
					// hold the reference
	    			cachedDmFormListByAllUsageType = dmFormList;	
	    	
		    	for (DmForm dmForm : cachedDmFormListByAllUsageType)
		    	{
		    		if (!dmForm.getUsageType().equals("O"))
		    		{
		    			cachedDmFormListByOutPatOnly.add(dmForm);
		    		}
		    	}
			}
		}
    }
	
	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmForm> {
		
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmForm create(String formCode) {
			this.getAll();
			return this.internalGet(formCode);
		}

		@Override
		public Collection<DmForm> createAll() {
			return dmsPmsServiceProxy.retrieveLightWeightDmFormList();
		}

		@Override
		public String retrieveKey(DmForm dmForm) {
			return dmForm.getFormCode();
		}
	}

	public static DmFormCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmFormCacherInf) Component.getInstance("dmFormCacher",
				ScopeType.APPLICATION);
	}

}
