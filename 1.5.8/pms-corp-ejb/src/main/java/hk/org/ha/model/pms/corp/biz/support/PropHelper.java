package hk.org.ha.model.pms.corp.biz.support;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.OperationProp;
import hk.org.ha.model.pms.persistence.reftable.Prop;
import hk.org.ha.model.pms.persistence.reftable.WorkstationProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("propHelper")
@Scope(ScopeType.APPLICATION)
public class PropHelper {
	
	@In
	private AuditLogger auditLogger; 
	
	private static String MESSAGE_ID_4029 = "#4029 Property Maintenance|Update Corporate Property|PropName [#0]|OrgValue [#1]|NewValue [#2]|";
	private static String MESSAGE_ID_4034 = "#4034 Corporate Property Maintenance|Update Corporate Property|PropName [#0]|OrgValue [#1]|NewValue [#2]|";
	
	@SuppressWarnings("unchecked")
	public List<CorporateProp> retrieveCorporatePropList(EntityManager em) {
		return em.createQuery(
				"select o from CorporateProp o" + // 20120303 index check : none
				" order by o.prop.description")
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<HospitalProp> retrieveHospitalPropList(EntityManager em) {
		return em.createQuery(
				"select o from HospitalProp o" + // 20120303 index check : none
				" order by o.prop.name")
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<WorkstoreProp> retrieveWorkstorePropList(EntityManager em, Hospital hospital) {
		return em.createQuery(
				"select o from WorkstoreProp o" + // 20120303 index check : Workstore.hospital : FK_WORKSTORE_01
				" where o.workstore.hospital = :hospital" +
				" order by o.prop.name")
				.setParameter("hospital", hospital)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Prop> retrievePropListByWorkstationPropName(EntityManager em) {
		List<String> propNameList = em.createQuery(
				"select distinct o.prop.name from WorkstationProp o" + // 20120303 index check : none
				" order by o.prop.name")
				.getResultList();
		
		if ( !propNameList.isEmpty() ) {
			return em.createQuery(
					"select o from Prop o" + // 20120303 index check : Prop.name : UI_PROP_01
					" where o.name in :propNameList" +
					" order by o.name")
					.setParameter("propNameList", propNameList)
					.getResultList();
		}
		return new ArrayList<Prop>();
	} 
	
	@SuppressWarnings("unchecked")
	public List<WorkstationProp> retrieveWorkstationPropListByHostName(EntityManager em, String hospCode, String hostName) {
		return em.createQuery(
				"select o from WorkstationProp o" + // 20120303 index check : Workstore.hospital : FK_WORKSTORE_01
				" where o.workstation.workstore.hospCode = :hospCode" +
				" and o.workstation.hostName = :hostName" +
				" order by o.prop.name")
				.setParameter("hospCode", hospCode)
				.setParameter("hostName", hostName)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<OperationProp> retrieveOperationPropList(EntityManager em, Long operationProfileId) {
		return em.createQuery(
				"select o from OperationProp o" + // 20120831 index check : OperationProp.operationProfile,prop : UI_OPERATION_PROP_01
				" where o.operationProfile.id = :operationProfileId" +
				" and o.prop.maintScreen = :maintScreen" +
				" order by o.sortSeq")
				.setParameter("operationProfileId", operationProfileId)
				.setParameter("maintScreen", MaintScreen.Support)
				.getResultList();
	}
	
	@Deprecated // pms-corp version 1.3.2.12
	public void updateCorporatePropList(EntityManager em, List<CorporateProp> corporatePropList) {
		updateCorporatePropList(em, corporatePropList, "#0314");//PMS system message code 0314	
	}
	
	/*start using in pms-pms version 3.3.2.16*/
	public void updateCorporatePropList(EntityManager em, List<CorporateProp> corporatePropList, String auditLoggerMessageId) {
		String msgId = auditLoggerMessageId;
		if ( "#4029".equals(auditLoggerMessageId) ) {
			msgId = MESSAGE_ID_4029;
		} else if ( "#4034".equals(auditLoggerMessageId) ) {
			msgId = MESSAGE_ID_4034;
		}
		
		for ( CorporateProp corporateProp : corporatePropList ) {
			em.merge(corporateProp);
			auditLogger.log(msgId, corporateProp.getProp().getName(), corporateProp.getOrgValue(), corporateProp.getValue());
		}
		em.flush();
	}
	
	public void updateHospitalPropList(EntityManager em, List<HospitalProp> hospitalPropList) {
		for ( HospitalProp hospitalProp : hospitalPropList ) {
			em.merge(hospitalProp);
			auditLogger.log("#0315", hospitalProp.getHospital().getHospCode(), 
										hospitalProp.getProp().getName(), 
										hospitalProp.getOrgValue(), 
										hospitalProp.getValue());
		}
		em.flush();
	}
	
	public void updateWorkstorePropList(EntityManager em, List<WorkstoreProp> workstorePropList) {
		for ( WorkstoreProp workstoreProp : workstorePropList ) {
			em.merge(workstoreProp);
			auditLogger.log("#0320", workstoreProp.getWorkstore().getHospCode(), 
										workstoreProp.getWorkstore().getWorkstoreCode(), 
										workstoreProp.getProp().getName(), 
										workstoreProp.getOrgValue(), 
										workstoreProp.getValue());
		}
		em.flush();
	}
	
	public void updateWorkstationPropList(EntityManager em, List<WorkstationProp> workstationPropList) {
		for ( WorkstationProp workstationProp : workstationPropList ) {
			if ( workstationProp.getId() == null ) {
				em.persist(workstationProp);
			} else {
				em.merge(workstationProp);
			}
			auditLogger.log("#0335", 
									workstationProp.getWorkstation().getWorkstore().getHospCode(), 
									workstationProp.getWorkstation().getWorkstore().getWorkstoreCode(), 
									workstationProp.getWorkstation().getHostName(), 
									workstationProp.getProp().getName(), 
									StringUtils.trimToEmpty(workstationProp.getOrgValue()), 
									workstationProp.getValue());
		}
		em.flush();
	}
	
	public void updateOperationPropList(EntityManager em, List<OperationProp> operationPropList) {
		for ( OperationProp operationProp : operationPropList ) {
			em.merge(operationProp);
			auditLogger.log("#0565", 
								operationProp.getOperationProfile().getWorkstore().getHospCode(), 
								operationProp.getOperationProfile().getWorkstore().getWorkstoreCode(), 
								operationProp.getOperationProfile().getName(), 
								operationProp.getProp().getName(), 
								StringUtils.trimToEmpty(operationProp.getOrgValue()), 
								operationProp.getValue());
		}
		em.flush();
	}
	
}
