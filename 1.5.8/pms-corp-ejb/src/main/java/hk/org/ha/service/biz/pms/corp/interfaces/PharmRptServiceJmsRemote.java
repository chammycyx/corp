package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItem;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItemCriteria;

import java.util.List;

public interface PharmRptServiceJmsRemote {

	List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria);
	
	List<CddhDispenseItem> retrieveCddhDispenseItemList(CddhDispenseItemCriteria criteria);
}
