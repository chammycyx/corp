package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.util.Aggregater;
import hk.org.ha.fmk.pms.util.ObjectArray;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.FmHospitalMappingManagerLocal;
import hk.org.ha.model.pms.corp.cache.DmBnfCacherInf;
import hk.org.ha.model.pms.corp.cache.DmCorpIndicationCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugBnfCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSupplIndicationCacherInf;
import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;
import hk.org.ha.model.pms.corp.persistence.report.FmRptStat;
import hk.org.ha.model.pms.corp.vo.report.FmReportInfo;
import hk.org.ha.model.pms.corp.vo.report.FmReportOrder;
import hk.org.ha.model.pms.corp.vo.report.FmReportPatient;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.report.FmReportType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Fluid;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.RxDrug;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

/**
 * Session Bean implementation class DayendMainBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("fmRptHospDtlBean")
@MeasureCalls
public class FmRptHospDtlBean implements FmRptHospDtlLocal {
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@PersistenceContext(unitName="PMSCOR1_PMS_RPT")
	private EntityManager emReport;
	
	@In
	private FmRptManagerLocal fmRptManager;
	
	@In
	private FmRptFilesManagerLocal fmRptFilesManager;
	
	@In
	private FmHospitalMappingManagerLocal fmHospitalMappingManager;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmDrugBnfCacherInf dmDrugBnfCacher;
	
	@In
	private DmBnfCacherInf dmBnfCacher;
	
	@In
	private DmSupplIndicationCacherInf dmSupplIndicationCacher;

	@In
	private DmCorpIndicationCacherInf dmCorpIndicationCacher;
	
	@In
	private FmRptConverterInf fmRptConverter;
	
	private Logger logger = null;
	private String jobId = null;;
	private String patHospCode = null;
	private String orderNum = null;
	
	private Date batchDate;
	private DateTime batchStartDate = null;
	private static final char SPACE = ' ';
	
	private DateFormat yearFormat = new SimpleDateFormat("yyyy");
	private DateFormat monthFormat = new SimpleDateFormat("MM");
	
	private static final String VOLUME_PATTERN = "#######.####";
	
    /**
     * Default constructor. 
     */
    public FmRptHospDtlBean() {
        // TODO Auto-generated constructor stub
    }

	public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
				
		logger.info("initBatch " + info.getJobId());
		
		jobId = info.getJobId();
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        } else {
        	batchDate = params.getDate("batchDate");
    		batchStartDate = new DateTime(batchDate).dayOfMonth().withMinimumValue();
        }
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
	}
	
	private Map<Long, MedOrderItem> convertVettedMoiMap(List<MedOrder> vettedMedOrderList)
	{
		Map<Long, MedOrderItem> vettedMedOrderItemMap = new HashMap<Long, MedOrderItem>();
		for (MedOrder medOrder : vettedMedOrderList)
		{
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList())
			{
				if (vettedMedOrderItemMap.get(medOrderItem.getId()) == null)
				{
					vettedMedOrderItemMap.put(medOrderItem.getId(), medOrderItem);
				}
			}
		}
		
		return vettedMedOrderItemMap;
	}
		
	public void processRecord(Record record) throws Exception {
    	String clusterCode = "";
    	patHospCode = record.getString("PAT_HOSP_CODE");
    	DateTime startDate = new DateTime();
    	logger.info("Generate raw data, statistics data and details report for " + patHospCode + " with Batch Date from " + 
			    	batchStartDate.toDate() + " to " + new DateTime(batchDate).dayOfMonth().withMinimumValue().toDate() + " start");
    	
		Map<String, File> dataFileMap = new HashMap<String, File>();
		
    	try {
    		if (statisticRecordEmpty()) {
    			Map<StringArray, FmReportPatient> fmReportPatientMap = new HashMap<StringArray, FmReportPatient>();
    			Map<StringArray, FmRptStat> fmRptStatMap = new HashMap<StringArray, FmRptStat>();

    			String log;
    			long startTime; 
				long retrieveUnvetMoeOrderTime;
				long retrieveVettedMedOrderTime;
				long convertVettedMoiMapTime;
				long retrieveVettedMoeOrderTime;
				long convertFmPatientListTime;
				long convertFmReportForUnvetOrderTime;
				long convertFmReportForVettedOrderTime;				
				long genStatisticReportTime = 0;
				long aggregateMedOrderItemTime = 0;
				long aggregateMedOrderTime = 0;
				long aggregatePatientTime = 0;
				long insertDbTime;
				int nonGeneralDrugCount = 0;
				int totalDrugCount = 0;
    			
				for (int i = 1; i <= batchStartDate.dayOfMonth().getMaximumValue(); i++) {
										
					List<FmReportInfo> fmReportList = new ArrayList<FmReportInfo>();
	    			List<FmReportInfo> fmReportAllList = new ArrayList<FmReportInfo>();
	    			List<FmReportOrder> fmReportOrderList = new ArrayList<FmReportOrder>();
	    			
					DateMidnight fromDate = new DateMidnight(batchStartDate).plusDays(i-1);
					DateTime toDate = new DateTime(batchStartDate).plusDays(i).minus(1);
					
					startTime = fmRptManager.processBeginTime();
					List<MedOrder> unvetMedOrderList = retrieveUnvetMoeOrder(fromDate, toDate, patHospCode);
					retrieveUnvetMoeOrderTime = fmRptManager.totalPorcessTime(startTime);
					
					startTime = fmRptManager.processBeginTime();
					List<MedOrder> vettedMedOrderList = retrieveVettedMedOrder(fromDate, toDate, patHospCode);
					retrieveVettedMedOrderTime = fmRptManager.totalPorcessTime(startTime);
					
					startTime = fmRptManager.processBeginTime();
					Map<Long, MedOrderItem> vettedMedOrderItemMap = convertVettedMoiMap(vettedMedOrderList);
					convertVettedMoiMapTime = fmRptManager.totalPorcessTime(startTime);
					
					startTime = fmRptManager.processBeginTime();
					List<DispOrderItem> vettedDoiList = retrieveVettedMoeOrder(fromDate, toDate, patHospCode);
					retrieveVettedMoeOrderTime = fmRptManager.totalPorcessTime(startTime);
					
					startTime = fmRptManager.processBeginTime();
					convertFmPatientList(unvetMedOrderList, vettedDoiList, fmReportPatientMap);
					convertFmPatientListTime = fmRptManager.totalPorcessTime(startTime);
					 
					startTime = fmRptManager.processBeginTime();
					convertFmReportForUnvetOrder(unvetMedOrderList, fmReportList, fmReportAllList);
					convertFmReportForUnvetOrderTime = fmRptManager.totalPorcessTime(startTime);
					
					startTime = fmRptManager.processBeginTime();
					convertFmReportForVettedOrder(vettedMedOrderItemMap, vettedDoiList, fmReportList, fmReportAllList);
					convertFmReportForVettedOrderTime = fmRptManager.totalPorcessTime(startTime);
					 
					emReport.clear();
					
					nonGeneralDrugCount += fmReportList.size();
					totalDrugCount += fmReportAllList.size();
					
					log = "Read and prepare raw data for statistics for patHosp " + patHospCode + " completed" +
						"\n 1. start date: " + fromDate +
						"\n 2. retrieveUnvetMoeOrder: total record: " + unvetMedOrderList.size() + "\ttime used: " + retrieveUnvetMoeOrderTime +
						"\n 3. retrieveVettedMedOrder: total record: " + vettedMedOrderList.size() + "\ttime used: " + retrieveVettedMedOrderTime +
						"\n 4. convertVettedMoiMap (convert vetted order to moe order format): total record: " + vettedMedOrderList.size() + "\ttime used: " + convertVettedMoiMapTime +
						"\n 5. retrieveVettedMoeOrder (disp order item): total record: " + vettedDoiList.size() + "\ttime used: " + retrieveVettedMoeOrderTime +
						"\n 6. convertFmPatientList: time used: " + convertFmPatientListTime +
						"\n 7. convertFmReportForUnvetOrder: time used: " + convertFmReportForUnvetOrderTime +
						"\n 8. convertFmReportForVettedOrder: time used: " +  convertFmReportForVettedOrderTime;
					
					logger.info(log);
					
					if (StringUtils.isEmpty(clusterCode) && ! fmReportAllList.isEmpty()) {
						clusterCode = fmReportAllList.get(0).getClusterCode();
					}
					
					if (totalDrugCount > 0)
					{
						startTime = fmRptManager.processBeginTime();
						
						//check record is over 65336 as excel version before 2010 only support 66536
						if (!fmReportList.isEmpty() && nonGeneralDrugCount < 65336)
						{							
							dataFileMap = fmRptManager.generateDailyDataCsvData(fmReportList, fmReportAllList, dataFileMap, 
									batchStartDate, patHospCode, clusterCode, jobId);
							genStatisticReportTime += fmRptManager.totalPorcessTime(startTime);
						}
						else if (nonGeneralDrugCount >= 65336)
						{
							logger.info(patHospCode + " skip preparing csv for non-general detail report for " + fromDate + ", total number of non general drug for " + patHospCode + " is " + nonGeneralDrugCount);
							genStatisticReportTime += fmRptManager.totalPorcessTime(startTime);
						}
						else if (fmReportList.isEmpty())
						{
							logger.info(patHospCode + " no detail record for " + fromDate + ", total number of non general drug for " + patHospCode + " is " + nonGeneralDrugCount);
							genStatisticReportTime += fmRptManager.totalPorcessTime(startTime);
						}
						
						startTime = fmRptManager.processBeginTime();
						aggregateMedOrderItem(fmReportAllList, fmRptStatMap);
						aggregateMedOrderItemTime += fmRptManager.totalPorcessTime(startTime);
						
						startTime = fmRptManager.processBeginTime();
						aggregateMedOrder(convertAggregateMedOrder(fmReportOrderList, fmReportAllList), fmRptStatMap);
						aggregateMedOrderTime += fmRptManager.totalPorcessTime(startTime);
					}
    			}	
				
				if (totalDrugCount > 0)
				{
					if (nonGeneralDrugCount > 0 && nonGeneralDrugCount < 65336) {
						fmRptManager.createMonthlyDrugDetailsReport(batchStartDate, patHospCode, clusterCode, jobId, dataFileMap.get("nonGenDrugMap"));
						
					} else { //skip generate report if more than 65336 record as excel only support 66536 at version before 2010
						logger.info(patHospCode + " skip non-general detail report, total number of non general drug for " + patHospCode + " is " + nonGeneralDrugCount);
					}
					
					startTime = fmRptManager.processBeginTime();
					aggregatePatient(fmReportPatientMap, fmRptStatMap);			
					aggregatePatientTime += fmRptManager.totalPorcessTime(startTime);
										
					for (Map.Entry<StringArray, FmRptStat> entry : fmRptStatMap.entrySet()) {
		    			FmRptStat fmRptStat = entry.getValue();
		    			em.persist(fmRptStat);
		    			em.flush();
		    		}
					
					insertDbTime = fmRptManager.totalPorcessTime(startTime);
					
					log = "Generate statistics and detail report for patHosp " + patHospCode + " completed" +
						"\n 1. generate fm statistics report: total record: " + totalDrugCount + " \ttime used: " + genStatisticReportTime +
						"\n 2. aggregateMedOrderItem: time used: " + aggregateMedOrderItemTime +
						"\n 3. aggregateMedOrder: time used: " + aggregateMedOrderTime +
						"\n 4. aggregatePatient: time used: " + aggregatePatientTime +
						"\n 5. insert to DB: time used: " + insertDbTime;
					
					logger.info(log);
					
					if (logger.isDebugEnabled()) {
						logger.debug(patHospCode + " Generate detail report complete, total number of non general drug for " + patHospCode + " is " + nonGeneralDrugCount);
					}
					
					fmRptFilesManager.moveToDestDirectory(batchStartDate, patHospCode, clusterCode, jobId, startDate, Arrays.asList(FmReportType.MonthlyData));
				}
				else
				{
					logger.info("No need to generate raw data, statistics data and detail report for " + patHospCode);
				}
    		} else {
    			logger.info(patHospCode + " raw data, statistics and details report had been generated");
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		logger.info(jobId + " with Hospital  " + patHospCode + " Fail. Order num: " + orderNum + " Move files to error folder");
    		//delete CSV files here (not implemented)
    		fmRptFilesManager.moveToErrorDirectory(jobId, startDate);
    		throw e;
    		
		} finally {
			if (dataFileMap.get("nonGenDrugMap") != null) {
				dataFileMap.get("nonGenDrugMap").delete();
			}
		}
    }
			
	private List<MedOrderItem> convertToMedOrderItemList(List<MedOrder> medOrderList)
	{
		List<MedOrderItem> medOrderItemList = new ArrayList<MedOrderItem>();
		for (MedOrder medOrder : medOrderList)
		{
			medOrderItemList.addAll(medOrder.getMedOrderItemList());
		}
		
		return medOrderItemList;
	}
	
	@SuppressWarnings("unchecked")
	private Boolean statisticRecordEmpty() {
		List<FmRptStat> fmRptStatList = emReport.createQuery(
				"select o from FmRptStat o" + // 20120225 index check : FmRptStat.year,month,patHospCode : UI_FM_RPT_STAT_01
				" where o.year = :year and o.month = :month" + 
				" and o.patHospCode = :patHospCode")
				.setParameter("year", batchStartDate.getYear())
				.setParameter("month", batchStartDate.getMonthOfYear())
				.setParameter("patHospCode", patHospCode)
				.getResultList();
		
		return fmRptStatList.isEmpty();
	}

	private String mapClusterCode(String oriClusterCode)
	{
		FmHospitalMapping fmHospitalMapping = fmHospitalMappingManager.retrieveFmHospitalMapping(patHospCode);
		if(fmHospitalMapping != null && fmHospitalMapping.getFromClusterCode().equals(oriClusterCode))
		{
			return fmHospitalMapping.getToClusterCode();
		}
		return oriClusterCode;
	}
	
	private void aggregateMedOrderItem(List<FmReportInfo> fmReportList, Map<StringArray, FmRptStat> fmRptStatMap) throws Exception {

		Aggregater<FmReportInfo> aggFm = new Aggregater<FmReportInfo>();
		Aggregater.Aggregate<FmReportInfo> aggregateFm = aggFm.createAggregate(new String[] {"year", "patHospCode", "eisSpecCode", "clusterCode"});
		
		Aggregater.AggregateColumn<FmReportInfo> generalDrugCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("generalDrugCnt") {
			public double getValue(FmReportInfo e) {
				return e.getGeneralDrugCnt().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sDrugCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("specialDrugCnt") {
			public double getValue(FmReportInfo e) {
				return e.getSpecialDrugCnt().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sDrugExistPatCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("specialAndExistPat") {
			public double getValue(FmReportInfo e) {
				return e.getSpecialAndExistPat().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sDrugOtherCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("specialAndOther") {
			public double getValue(FmReportInfo e) {
				return e.getSpecialAndOther().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sDrugIndicatedCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("specialAndIndicated") {
			public double getValue(FmReportInfo e) {
				return e.getSpecialAndIndicated().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sDrugNonFormularyCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("specialAndNonformInd") {
			public double getValue(FmReportInfo e) {
				return e.getSpecialAndNonformInd().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sfiCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("sfi") {
			public double getValue(FmReportInfo e) {
				return e.getSfi().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> unregisteredDrugCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("namedPatient") {
			public double getValue(FmReportInfo e) {
				return e.getNamedPatient().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> sfiSafetyNetCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("sfiSn") {
			public double getValue(FmReportInfo e) {
				return e.getSfiSn().doubleValue();
			}
		};
		
		Aggregater.AggregateColumn<FmReportInfo> issueOrderCountColumn = new Aggregater.AggregateColumn<FmReportInfo>("haIssue") {
			public double getValue(FmReportInfo e) {
				if ("Y".equals(e.getHaIssue())) {
					return 1;
				}
				return 0;
			}
		};
				
		aggregateFm.addAggregateColumn(generalDrugCountColumn);
		aggregateFm.addAggregateColumn(sDrugCountColumn);
		aggregateFm.addAggregateColumn(sDrugExistPatCountColumn);
		aggregateFm.addAggregateColumn(sDrugOtherCountColumn);
		aggregateFm.addAggregateColumn(sDrugIndicatedCountColumn);
		aggregateFm.addAggregateColumn(sDrugNonFormularyCountColumn);
		aggregateFm.addAggregateColumn(sfiCountColumn);
		aggregateFm.addAggregateColumn(unregisteredDrugCountColumn);
		aggregateFm.addAggregateColumn(sfiSafetyNetCountColumn);
		aggregateFm.addAggregateColumn(issueOrderCountColumn);
		
		for (FmReportInfo fmReportInfo : fmReportList) {
			aggFm.load(fmReportInfo);
		}
		
		constructMedOrderItemFmStat(aggregateFm, generalDrugCountColumn, sDrugCountColumn, sDrugExistPatCountColumn, sDrugOtherCountColumn,
									sDrugIndicatedCountColumn, sDrugNonFormularyCountColumn, sfiCountColumn, unregisteredDrugCountColumn, sfiSafetyNetCountColumn,
									issueOrderCountColumn, fmRptStatMap);
	}
	
	private void constructMedOrderItemFmStat(Aggregater.Aggregate<FmReportInfo> aggregateFm, Aggregater.AggregateColumn<FmReportInfo> generalDrugCountColumn,
											 Aggregater.AggregateColumn<FmReportInfo> sDrugCountColumn, Aggregater.AggregateColumn<FmReportInfo> sDrugExistPatCountColumn, 
											 Aggregater.AggregateColumn<FmReportInfo> sDrugOtherCountColumn, Aggregater.AggregateColumn<FmReportInfo> sDrugIndicatedCountColumn,
											 Aggregater.AggregateColumn<FmReportInfo> sDrugNonFormularyCountColumn, Aggregater.AggregateColumn<FmReportInfo> sfiCountColumn,
											 Aggregater.AggregateColumn<FmReportInfo> unregisteredDrugCountColumn, Aggregater.AggregateColumn<FmReportInfo> sfiSafetyNetCountColumn,
											 Aggregater.AggregateColumn<FmReportInfo> issueOrderCountColumn,Map<StringArray, FmRptStat> fmRptStatMap) {
		
		for (Map.Entry<ObjectArray, Map<Aggregater.AggregateColumn<FmReportInfo>, Aggregater.AggregateResult<FmReportInfo>>> 
			 entry : aggregateFm.getAggregateColumnMap().entrySet()) {
			
			Object[] cols = entry.getKey().get();
			Aggregater.AggregateResult<FmReportInfo> numOfPrescItem = entry.getValue().values().iterator().next();
			Aggregater.AggregateResult<FmReportInfo> numOfGeneralDrug = entry.getValue().get(generalDrugCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSDrug = entry.getValue().get(sDrugCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSDrugExistPat = entry.getValue().get(sDrugExistPatCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSDrugOther = entry.getValue().get(sDrugOtherCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSDrugIndicated = entry.getValue().get(sDrugIndicatedCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSDrugNonFormulary = entry.getValue().get(sDrugNonFormularyCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSfi = entry.getValue().get(sfiCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfUnregisteredDrug = entry.getValue().get(unregisteredDrugCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfSfiSafetyNet = entry.getValue().get(sfiSafetyNetCountColumn);
			Aggregater.AggregateResult<FmReportInfo> numOfIssueOrder = entry.getValue().get(issueOrderCountColumn);
			
			StringArray key = new StringArray((String) cols[0], (String) cols[1], (String) cols[2], (String) cols[3]);
			FmRptStat fmRptStat = fmRptStatMap.get(key);
			if (fmRptStat == null)
			{
				fmRptStat = new FmRptStat();
				fmRptStat.setYear(Integer.parseInt(yearFormat.format(batchStartDate.toDate())));
				fmRptStat.setMonth(Integer.parseInt(monthFormat.format(batchStartDate.toDate())));
				fmRptStat.setPatHospCode((String) cols[1]);
				fmRptStat.setEisSpecCode((String) cols[2]);
				fmRptStat.setClusterCode((String) cols[3]);
				fmRptStat.setOrderItemCount(numOfPrescItem.getCount());
				fmRptStat.setIssueOrderCount(((Double)numOfIssueOrder.getSum()).intValue());
				fmRptStat.setGeneralDrugCount(((Double)numOfGeneralDrug.getSum()).intValue());
				fmRptStat.setsDrugCount(((Double)numOfSDrug.getSum()).intValue());
				fmRptStat.setsDrugExistPatCount(((Double)numOfSDrugExistPat.getSum()).intValue());
				fmRptStat.setsDrugOtherCount(((Double)numOfSDrugOther.getSum()).intValue());
				fmRptStat.setsDrugIndicatedCount(((Double)numOfSDrugIndicated.getSum()).intValue());
				fmRptStat.setsDrugNonFormularyCount(((Double)numOfSDrugNonFormulary.getSum()).intValue());
				fmRptStat.setSfiCount(((Double)numOfSfi.getSum()).intValue());
				fmRptStat.setUnregisteredDrugCount(((Double)numOfUnregisteredDrug.getSum()).intValue());
				fmRptStat.setSfiSafetyNetCount(((Double)numOfSfiSafetyNet.getSum()).intValue());
				
				if (fmRptStat.getMonth() >= 4 && fmRptStat.getYear() < batchStartDate.getYear()) {
					fmRptStat.setFiscalYear(fmRptStat.getYear());
				} else if (fmRptStat.getMonth() < 4 && fmRptStat.getYear() == batchStartDate.getYear()) {
					fmRptStat.setFiscalYear(batchStartDate.getYear()-1);
				} else {
					fmRptStat.setFiscalYear(batchStartDate.getYear());
				}
				
				fmRptStatMap.put(key, fmRptStat);
			}
			else
			{
				fmRptStat.setOrderItemCount(fmRptStat.getOrderItemCount() + numOfPrescItem.getCount());
				fmRptStat.setIssueOrderCount(fmRptStat.getIssueOrderCount() + ((Double)numOfIssueOrder.getSum()).intValue());
				fmRptStat.setGeneralDrugCount(fmRptStat.getGeneralDrugCount() + ((Double)numOfGeneralDrug.getSum()).intValue());
				fmRptStat.setsDrugCount(fmRptStat.getsDrugCount() + ((Double)numOfSDrug.getSum()).intValue());
				fmRptStat.setsDrugExistPatCount(fmRptStat.getsDrugExistPatCount() + ((Double)numOfSDrugExistPat.getSum()).intValue());
				fmRptStat.setsDrugOtherCount(fmRptStat.getsDrugOtherCount() + ((Double)numOfSDrugOther.getSum()).intValue());
				fmRptStat.setsDrugIndicatedCount(fmRptStat.getsDrugIndicatedCount() + ((Double)numOfSDrugIndicated.getSum()).intValue());
				fmRptStat.setsDrugNonFormularyCount(fmRptStat.getsDrugNonFormularyCount() + ((Double)numOfSDrugNonFormulary.getSum()).intValue());
				fmRptStat.setSfiCount(fmRptStat.getSfiCount() + ((Double)numOfSfi.getSum()).intValue());
				fmRptStat.setUnregisteredDrugCount(fmRptStat.getUnregisteredDrugCount() + ((Double)numOfUnregisteredDrug.getSum()).intValue());
				fmRptStat.setSfiSafetyNetCount(fmRptStat.getSfiSafetyNetCount() + ((Double)numOfSfiSafetyNet.getSum()).intValue());
			}			
		}
	}
	
	private List<FmReportOrder> convertAggregateMedOrder(List<FmReportOrder> fmReportOrderList, List<FmReportInfo> fmReportList)
	{
		Map<StringArray, FmReportOrder> fmReportMedOrderMap = new HashMap<StringArray, FmReportOrder>();
		for (FmReportInfo fmReportInfo : fmReportList)
		{
			StringArray key = new StringArray(fmReportInfo.getYear(), fmReportInfo.getPatHospCode(), 
					fmReportInfo.getEisSpecCode(), fmReportInfo.getClusterCode(), Integer.toString(fmReportInfo.getOrderNum()));
			
			FmReportOrder fmReportOrder = fmReportMedOrderMap.get(key);
			if (fmReportOrder == null)
			{
				fmReportOrder = new FmReportOrder();
				fmReportOrder.setYear(fmReportInfo.getYear());
				fmReportOrder.setPatHospCode(fmReportInfo.getPatHospCode());
				fmReportOrder.setEisSpecCode(fmReportInfo.getEisSpecCode());
				fmReportOrder.setClusterCode(fmReportInfo.getClusterCode());
				fmReportOrder.setOrderNum(Integer.toString(fmReportInfo.getOrderNum()));
				fmReportOrder.setSfiDrugCnt(fmReportInfo.getSfi());
				fmReportMedOrderMap.put(key, fmReportOrder);
				fmReportOrderList.add(fmReportOrder);
			}
			else
			{
				if (fmReportOrder.getSfiDrugCnt().equals(0) && fmReportInfo.getSfi().equals(1))
				{
					fmReportOrder.setSfiDrugCnt(1);
				}
			}
		}
				
		return fmReportOrderList;
	}
	
	
	private void aggregateMedOrder(List<FmReportOrder> fmReportOrderList, Map<StringArray, FmRptStat> fmRptStatMap) throws Exception {
		Aggregater<FmReportOrder> aggMo = new Aggregater<FmReportOrder>();
		Aggregater.Aggregate<FmReportOrder> aggregateMo = aggMo.createAggregate(new String[] {"year", "patHospCode", "eisSpecCode", 
				"clusterCode"});
			
		Aggregater.AggregateColumn<FmReportOrder> numOfSfiOrderColumn = new Aggregater.AggregateColumn<FmReportOrder>("sfiOrderCount") {
			public double getValue(FmReportOrder e) {			
				return e.getSfiDrugCnt().doubleValue();
			}
		};
		
		aggregateMo.addAggregateColumn(numOfSfiOrderColumn);
		
		for (FmReportOrder fo : fmReportOrderList) {
			aggMo.load(fo);
		}
	
		for (Map.Entry<ObjectArray, Map<Aggregater.AggregateColumn<FmReportOrder>, Aggregater.AggregateResult<FmReportOrder>>> 
			 entry : aggregateMo.getAggregateColumnMap().entrySet()) {
						
			Object[] cols = entry.getKey().get();
			StringArray key = new StringArray((String) cols[0], (String) cols[1], (String) cols[2], (String) cols[3]);	
			FmRptStat fmRptStat = fmRptStatMap.get(key);
			Aggregater.AggregateResult<FmReportOrder> numOfOrder = entry.getValue().values().iterator().next();;
			Aggregater.AggregateResult<FmReportOrder> numOfSfiOrder = entry.getValue().get(numOfSfiOrderColumn);
			fmRptStat.setOrderCount((fmRptStat.getOrderCount()==null?0:fmRptStat.getOrderCount()) + numOfOrder.getCount());
			fmRptStat.setOrderSfiCount((fmRptStat.getOrderSfiCount()==null?0:fmRptStat.getOrderSfiCount()) + ((Double)numOfSfiOrder.getSum()).intValue());	
		}
	}
       
    private void aggregatePatient(Map<StringArray, FmReportPatient> fmReportPatientMap, Map<StringArray, FmRptStat> fmRptStatMap) throws Exception {
    	
    	Aggregater<FmReportPatient> aggPat = new Aggregater<FmReportPatient>();
    	Aggregater.Aggregate<FmReportPatient> aggregatePat = aggPat.createAggregate(new String[] {"year", "patHospCode", "eisSpecCode", "clusterCode"});
		
		Aggregater.AggregateColumn<FmReportPatient> numOfPatSfiColumn = new Aggregater.AggregateColumn<FmReportPatient>("sfiDrugCnt") {
			public double getValue(FmReportPatient e) {
				return e.getSfiDrugCnt().doubleValue();
			}
		};
		
		aggregatePat.addAggregateColumn(numOfPatSfiColumn);
		
		for (Map.Entry<StringArray, FmReportPatient> entry : fmReportPatientMap.entrySet()) {
			aggPat.load(entry.getValue());
		}
				
		for (Map.Entry<ObjectArray, Map<Aggregater.AggregateColumn<FmReportPatient>, Aggregater.AggregateResult<FmReportPatient>>> 
			 entry : aggregatePat.getAggregateColumnMap().entrySet()) {
			
			Object[] cols = entry.getKey().get();
			StringArray key = new StringArray((String) cols[0], (String) cols[1], (String) cols[2], (String) cols[3]);
			Aggregater.AggregateResult<FmReportPatient> numOfPat = entry.getValue().values().iterator().next();
			Aggregater.AggregateResult<FmReportPatient> numOfPatSfi = entry.getValue().get(numOfPatSfiColumn);
			FmRptStat fmRptStat = fmRptStatMap.get(key);
			fmRptStat.setPatCount(numOfPat.getCount());
			fmRptStat.setPatSfiCount(((Double)numOfPatSfi.getSum()).intValue());
		}
    }
    
    private void convertFmPatientList(List<MedOrder> medOrderList, List<DispOrderItem> dispOrderItemList, 
			  						  Map<StringArray, FmReportPatient> fmReportPatientMap) {
    	for (MedOrder medOrder : medOrderList) {
    		MedCase medCase = medOrder.getMedCase();
    		StringArray key = new StringArray(yearFormat.format(batchStartDate.toDate()), medOrder.getPatHospCode(), 
    										  medOrder.getEisSpecCode(), medCase.getCaseNum());
    		
    		FmReportPatient fmReportPatient = fmReportPatientMap.get(key);
    		if (fmReportPatient == null) {
    			fmReportPatient = new FmReportPatient();
	    		fmReportPatient.setYear(yearFormat.format(batchStartDate.toDate()));
	    		fmReportPatient.setPatHospCode(medOrder.getPatHospCode());
	    		fmReportPatient.setEisSpecCode(medOrder.getEisSpecCode());
	    		fmReportPatient.setClusterCode(mapClusterCode(medOrder.getClusterCode()));
	    		fmReportPatient.setPatKey(medCase.getCaseNum());
	    		
	    		fmReportPatientMap.put(key, fmReportPatient);
    		}
	    		
    		if (fmReportPatient.getSfiDrugCnt().equals(0)) {
	    		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList())
	    		{
	    			if (isSfiItem(medOrderItem)) {
	    				fmReportPatient.setSfiDrugCnt(1);
	    				break;
	    			}
	    		}
    		}
    	}
    	
    	for (DispOrderItem dispOrderItem : dispOrderItemList) {
    		MedOrder medOrder = dispOrderItem.getDispOrder().getPharmOrder().getMedOrder();
    		PharmOrder pharmOrder = dispOrderItem.getDispOrder().getPharmOrder();
    		String patKey = (StringUtils.isEmpty(pharmOrder.getPatKey())?pharmOrder.getMedCase().getCaseNum():pharmOrder.getPatKey());
    		StringArray key = new StringArray(yearFormat.format(batchStartDate.toDate()), medOrder.getPatHospCode(), 
    										  medOrder.getEisSpecCode(), patKey);
    			
    		if (fmReportPatientMap.get(key) == null) {
	    		FmReportPatient fmReportPatient = new FmReportPatient();
	    		fmReportPatient.setYear(yearFormat.format(batchStartDate.toDate()));
	    		fmReportPatient.setPatHospCode(medOrder.getPatHospCode());
	    		fmReportPatient.setEisSpecCode(medOrder.getEisSpecCode());
	    		fmReportPatient.setClusterCode(mapClusterCode(medOrder.getClusterCode()));
    			fmReportPatient.setPatKey(patKey);
	    		fmReportPatientMap.put(key, fmReportPatient);   		
    		}
	    		
    		if (fmReportPatientMap.get(key).getSfiDrugCnt().equals(0))
    		{
        		if (isSfiItem(dispOrderItem.getPharmOrderItem().getMedOrderItem())) {
        			fmReportPatientMap.get(key).setSfiDrugCnt(1);
        		}
    		}
    	}
    }
    
    @SuppressWarnings("unchecked")
    private List<MedOrder> retrieveUnvetMoeOrder(DateMidnight batchStartDate, DateTime batchEndDate, String patHospCode) {
    	return emReport.createQuery(
    			"select o from MedOrder o" + // 20140818 index check : MedOrder.patHospCode,status,orderType,orderDate : I_MED_ORDER_02
    			" where o.patHospCode = :patHospCode" +
    			" and o.status in :status" +
    			" and o.orderType = :orderType" + 
    			" and o.orderDate between :orderStartDate and :orderEndDate" +
    			" and o.createDate between :createStartDate and :createEndDate" +
    			" and o.docType = :docType" +
				" order by o.orderNum")
				.setParameter("patHospCode", patHospCode)
				.setParameter("status", MedOrderStatus.Outstanding_Withhold_PreVet)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("orderStartDate", batchStartDate.toDate())
				.setParameter("orderEndDate", batchEndDate.toDate())
				.setParameter("createStartDate", this.batchStartDate.toDate())
				.setParameter("createEndDate", new DateMidnight().plusDays(1).toDate())
				.setParameter("docType", MedOrderDocType.Normal)			
				.setHint(QueryHints.BATCH, "o.medOrderItemList")
				.setHint(QueryHints.BATCH, "o.medOrderFmList")
				.setHint(QueryHints.FETCH, "o.medCase")
				.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	private List<MedOrder> retrieveVettedMedOrder(DateMidnight batchStartDate, DateTime batchEndDate, String patHospCode) {
    	return emReport.createQuery(
    			"select o from MedOrder o" + // 20140818 index check : MedOrder.patHospCode,status,orderType,orderDate : I_MED_ORDER_02
				" where o.patHospCode = :patHospCode" +
				" and o.status in :status" +
    			" and o.orderType = :orderType" + 
    			" and o.orderDate between :orderStartDate and :orderEndDate" +
    			" and o.createDate between :createStartDate and :createEndDate" +
				" and o.docType = :docType" +
				" order by o.orderNum")
				.setParameter("patHospCode", patHospCode)
				.setParameter("status", Arrays.asList(MedOrderStatus.Complete))
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("orderStartDate", batchStartDate.toDate())
				.setParameter("orderEndDate", batchEndDate.toDate())
				.setParameter("createStartDate", this.batchStartDate.toDate())
				.setParameter("createEndDate", new DateMidnight().plusDays(1).toDate())
				.setParameter("docType", MedOrderDocType.Normal)
				.setHint(QueryHints.BATCH, "o.medOrderItemList")
				.setHint(QueryHints.BATCH, "o.medOrderFmList")
				.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveVettedMoeOrder(DateMidnight batchStartDate, DateTime batchEndDate, String patHospCode) {
    	return emReport.createQuery(
    			"select o from DispOrderItem o" + // 20140818 index check : MedOrder.patHospCode,status,orderType,orderDate : I_MED_ORDER_02
    			" where o.dispOrder.pharmOrder.medOrder.patHospCode = :patHospCode" +
				" and o.dispOrder.pharmOrder.medOrder.status in :status" +
    			" and o.dispOrder.pharmOrder.medOrder.orderType = :orderType" +
    			" and o.dispOrder.pharmOrder.medOrder.orderDate between :orderStartDate and :orderEndDate" +
    			" and o.dispOrder.pharmOrder.medOrder.createDate between :createStartDate and :createEndDate" +
    			" and o.dispOrder.pharmOrder.createDate between :createStartDate and :createEndDate" +
    			" and o.dispOrder.createDate between :createStartDate and :createEndDate" +
    			" and o.createDate between :createStartDate and :createEndDate" +
    			" and o.dispOrder.pharmOrder.medOrder.docType = :docType" + 
				" and o.dispOrder.refillFlag = :refillFlag" +
				" and o.dispOrder.status not in :dispOrderStatus" +
				" and o.pharmOrderItem.medOrderItem.remarkItemStatus not in :remarkItemStatus" +			
				" order by o.dispOrder.pharmOrder.medOrder.orderNum")
				.setParameter("patHospCode", patHospCode)
				.setParameter("status", Arrays.asList(MedOrderStatus.Complete))
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("orderStartDate", batchStartDate.toDate())
				.setParameter("orderEndDate", batchEndDate.toDate())
				.setParameter("createStartDate", this.batchStartDate.toDate())
				.setParameter("createEndDate", new DateMidnight().plusDays(1).toDate())
				.setParameter("docType", MedOrderDocType.Normal)
				.setParameter("refillFlag", Boolean.FALSE)
				.setParameter("dispOrderStatus", Arrays.asList(DispOrderStatus.SysDeleted, DispOrderStatus.Deleted))			
				.setParameter("remarkItemStatus", Arrays.asList(RemarkItemStatus.ConfirmedOldOriginal, RemarkItemStatus.ConfirmedOriginal, 
																RemarkItemStatus.Deleted, RemarkItemStatus.UnconfirmedDelete, RemarkItemStatus.UnconfirmedOrginial))
				.setHint(QueryHints.FETCH, "o.pharmOrderItem.pharmOrder.medCase")
				.setHint(QueryHints.FETCH, "o.pharmOrderItem.medOrderItem.medOrder")
				.getResultList();
    }
    
    private void convertFmReportForUnvetOrder(List<MedOrder> medOrderList, List<FmReportInfo> fmReportList, List<FmReportInfo> fmReportAllList) {
    	if (logger.isDebugEnabled()) {
    		logger.debug("Total number of not dispense item for " + patHospCode + 
    					 " for unvet order " + convertToMedOrderItemList(medOrderList).size());
    	}
    	
    	for (MedOrder medOrder : medOrderList) {
    		MedCase medCase = medOrder.getMedCase();
    		orderNum = medOrder.getOrderNum();
    		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
    			medOrderItem.loadDmInfo(false, false);
    			FmDetailReport fmDetailReport = constructFmReportInfoDetails(medOrderItem, medOrder.getMedOrderFmList(),
						medOrder, medCase, null, null, null);
    			fmReportList.addAll(fmDetailReport.getFmReportDataList());
    			fmReportAllList.addAll(fmDetailReport.getFmReportAllDataList());
    		}
    	}
    }
    
    private void convertFmReportForVettedOrder(Map<Long, MedOrderItem> vettedMedOrderItemMap, List<DispOrderItem> dispOrderItemList, 
			   List<FmReportInfo> fmReportList, List<FmReportInfo> fmReportAllList) 
	{
		Map<Long, FmDetailReport> dispOrderHash = new HashMap<Long, FmDetailReport>();
		
		for (DispOrderItem dispOrderItem : dispOrderItemList) 
		{
			if (!dispOrderHash.containsKey(dispOrderItem.getPharmOrderItem().getMedOrderItem().getId())) 
			{
				MedOrderItem medOrderItem = vettedMedOrderItemMap.get(dispOrderItem.getPharmOrderItem().getMedOrderItem().getId());
				DispOrder dispOrder = dispOrderItem.getDispOrder();
				PharmOrder pharmOrder = dispOrder.getPharmOrder();
				MedCase medCase = dispOrder.getPharmOrder().getMedCase();
				MedOrder medOrder = medOrderItem.getMedOrder();
				medOrderItem.loadDmInfo(false, false);
				orderNum = medOrder.getOrderNum();
				
				FmDetailReport fmDetailReport = constructFmReportInfoDetails(medOrderItem, medOrder.getMedOrderFmList(),
						medOrder, medCase, pharmOrder, dispOrder, dispOrderItem);
				dispOrderHash.put(dispOrderItem.getPharmOrderItem().getMedOrderItem().getId(), fmDetailReport);
				fmReportList.addAll(fmDetailReport.getFmReportDataList());
    			fmReportAllList.addAll(fmDetailReport.getFmReportAllDataList());
			} 
		}
	}
    
    private FmDetailReport constructFmReportInfoDetails(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList, MedOrder medOrder, MedCase medCase, 
			PharmOrder pharmOrder, DispOrder dispOrder, DispOrderItem dispOrderItem)
    {		
		FmReportInfo fmReportInfo;
		FmDetailReport fmDetailReport = new FmDetailReport();
		
    	switch (medOrderItem.getRxItemType()) 
    	{
    		case Oral:
    			fmReportInfo = constructFmReportInfo(medOrderItem.getRxDrug(), medOrder, medOrderItem.getItemNum(), 
    					retrieveMedOrderFm(medOrderItem.getRxDrug(), medOrderFmList));
    			if (fmReportInfo.getAddToDetailReport())
    			{
    				fmDetailReport.getFmReportDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			}

    			fmDetailReport.getFmReportAllDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			break;
    			
    		case Injection:
    			fmReportInfo = constructFmReportInfo(medOrderItem.getRxDrug(), medOrder, medOrderItem.getItemNum(), 
    					retrieveMedOrderFm(medOrderItem.getRxDrug(), medOrderFmList));
    			if (fmReportInfo.getAddToDetailReport())
    			{
    				fmDetailReport.getFmReportDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			}

    			fmDetailReport.getFmReportAllDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			
    			for (DoseGroup doseGroup : medOrderItem.getRxDrug().getRegimen().getDoseGroupList())
    			{
    				for (Dose dose : doseGroup.getDoseList())
    				{
    					if (dose.getDoseFluid() != null)
    					{
    						if (!StringUtils.isBlank(dose.getDoseFluid().getDiluentCode()))
    	    				{
		    					fmReportInfo = constructFmReportInfoForFluid(dose.getDoseFluid(), medOrder, medOrderItem.getItemNum());
		    					if (fmReportInfo.getAddToDetailReport())
		    	    			{
		    	    				fmDetailReport.getFmReportDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
		    	    			}
	
		    	    			fmDetailReport.getFmReportAllDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    	    				}
    					}
    				}
    			}
    			break;
    			
    		case Iv:
    			for (IvAdditive ivAdditive : medOrderItem.getIvRxDrug().getIvAdditiveList())
    			{
    				fmReportInfo = constructFmReportInfo(ivAdditive, medOrder, medOrderItem.getItemNum(), 
        					retrieveMedOrderFm(ivAdditive, medOrderFmList));       
    				if (fmReportInfo.getAddToDetailReport())
        			{
        				fmDetailReport.getFmReportDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
        			}

        			fmDetailReport.getFmReportAllDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			}
    			
    			for (IvFluid ivFluid : medOrderItem.getIvRxDrug().getIvFluidList())
    			{
    				if (!StringUtils.isBlank(ivFluid.getDiluentCode()))
    				{
	    				fmReportInfo = constructFmReportInfoForFluid(ivFluid, medOrder, medOrderItem.getItemNum());
	    				if (fmReportInfo.getAddToDetailReport())
	        			{
	        				fmDetailReport.getFmReportDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
	        			}
	
	        			fmDetailReport.getFmReportAllDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    				}
    			}
    			break;
    			
    		case Capd:
    			fmReportInfo = constructFmReportInfoForCapd(medOrderItem.getCapdRxDrug(), medOrder, medOrderItem.getItemNum());
    			if (fmReportInfo.getAddToDetailReport())
    			{
    				fmDetailReport.getFmReportDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			}

    			fmDetailReport.getFmReportAllDataList().add(constructRestFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem));
    			break;
    	}
    	
    	return fmDetailReport;
    }
    
    private FmReportInfo constructRestFmReportInfo(FmReportInfo fmReportInfo, MedOrder medOrder, MedCase medCase, 
    		PharmOrder pharmOrder, DispOrder dispOrder, DispOrderItem dispOrderItem)
    {
    	if (dispOrder == null)
		{
			constructUnvetOrderFmReportInfo(fmReportInfo, medOrder, medCase);
		}
		else
		{
			constructVettedOrderFmReportInfo(fmReportInfo, medOrder, medCase, pharmOrder, dispOrder, dispOrderItem);
		}
    	
    	return fmReportInfo;
    }
    
    private MedOrderFm retrieveMedOrderFm(RxDrug rxDrug, List<MedOrderFm> medOrderFmList) 
	{
		for (MedOrderFm medOrderFm : medOrderFmList) 
		{
			if (rxDrug.getStrengthCompulsory() != null && rxDrug.getStrengthCompulsory()) 
			{
				if (StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) && 
					StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) && 
					StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) && 
					medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) && 
					StringUtils.equals(medOrderFm.getItemCode(), rxDrug.getItemCode())) 
				{
					return medOrderFm;
				}
			} 
			else 
			{
				if (StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) && 
					StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) && 
					StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) && 
					medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) ) 
				{
					return medOrderFm;
				}
			}
        }
        
        return null;
  }

	private FmReportInfo constructFmReportInfo(RxDrug rxDrug, MedOrder medOrder, Integer itemNum, MedOrderFm medOrderFm)
	{
		FmReportInfo fmReportInfo = new FmReportInfo();
		fmReportInfo.setPatHospCode(medOrder.getPatHospCode());
		fmReportInfo.setOrderDate(medOrder.getOrderDate());
		fmReportInfo.setRefNum(medOrder.getRefNum());
		fmReportInfo.setOrderStatus(convertOrderStatus(medOrder));
		fmReportInfo.setOrderSubtype(medOrder.getCmsOrderSubType().getDataValue());
		fmReportInfo.setOrderNum(Integer.parseInt(medOrder.getOrderNum().substring(3)));
		fmReportInfo.setSfiCateogry(rxDrug.getSfiCat());
		fmReportInfo.setItemNum(itemNum);
		fmReportInfo.setDisplayName(rxDrug.getDisplayName());
		fmReportInfo.setFormCode(rxDrug.getFormCode());
		fmReportInfo.setSaltProperty(rxDrug.getSaltProperty());
		fmReportInfo.setFormDesc(rxDrug.getFormDesc());
		fmReportInfo.setRouteDesc(rxDrug.getRouteDesc());
		fmReportInfo.setStrength(rxDrug.getStrength());
		fmReportInfo.setFmStatus(rxDrug.getFmStatus());
		fmReportInfo.setActionStatus(rxDrug.getActionStatus());
		fmReportInfo.setEisSpecCode(medOrder.getEisSpecCode());
		fmReportInfo.setImisSpecialty("R");
		fmReportInfo.setDoctorCode(medOrder.getDoctorCode());
		fmReportInfo.setMedicalOfficerName(medOrder.getDoctorName());
		fmReportInfo.setRank(medOrder.getDoctorRankCode());
		fmReportInfo.setBatchNum(0);
		fmReportInfo.setYear(yearFormat.format(batchStartDate.toDate()));
		fmReportInfo.setMonth(monthFormat.format(batchStartDate.toDate()));
		fmReportInfo.setClusterCode(mapClusterCode(medOrder.getWorkstore().getHospital().getHospitalCluster().getClusterCode()));
		fmReportInfo.setOrderDesc(convertOrderDesc(rxDrug));
		fmReportInfo.setVolText(convertVolumeText(rxDrug));
		
    	//using rule engine to convert data
		if (medOrderFm != null)
		{
			medOrderFm.loadDmCorpIndicationCacher();
		}
		
		fmRptConverter.convertFmRxDrugReportData(fmReportInfo, medOrderFm, rxDrug);
		fmReportInfo.setCoIndDesc(dmCorpIndicationCacher.getDmCorpIndicationDesc(fmReportInfo.getCoInd()));
		fmReportInfo.setSuIndDesc(dmSupplIndicationCacher.getDmSupplIndicationDesc(fmReportInfo.getSuInd()));
		fmReportInfo.setBnfNum(dmDrugBnfCacher.getDmDrugBnfNum(fmReportInfo.getItemCode()));
		fmReportInfo.setBnfDesc(dmBnfCacher.getDmBnfDesc(fmReportInfo.getBnfNum()));
		fmReportInfo.setAddToDetailReport(addToDetailDataReport(rxDrug, medOrderFm));
				
		return fmReportInfo;
	}
	
	private Boolean addToDetailDataReport(RxDrug rxDrug, MedOrderFm medOrderFm)
	{
		if (medOrderFm != null && medOrderFm.getPrescOption() != null)
		{
			return true;
		}
		else if (!rxDrug.getFmStatus().isEmpty() && (rxDrug.getFmStatus().equals(FmStatus.SpecialDrug.getDataValue()) || 
				 rxDrug.getFmStatus().equals(FmStatus.SafetyNetItem.getDataValue()) ||
				 rxDrug.getFmStatus().equals(FmStatus.SelfFinancedItemC.getDataValue()) || 
				 rxDrug.getFmStatus().equals(FmStatus.SelfFinancedItemD.getDataValue()) || 
				 rxDrug.getFmStatus().equals(FmStatus.UnRegisteredDrug.getDataValue())))
		{
			return true;
		}
		else if (rxDrug.getActionStatus() == ActionStatus.PurchaseByPatient || rxDrug.getActionStatus() == ActionStatus.SafetyNet)
		{
			return true;
		}
		
		return false;
	}
	
	private FmReportInfo constructFmReportInfoForFluid(Fluid fluid, MedOrder medOrder, Integer itemNum)
	{
		FmReportInfo fmReportInfo = new FmReportInfo();
		fmReportInfo.setPatHospCode(medOrder.getPatHospCode());
		fmReportInfo.setOrderDate(medOrder.getOrderDate());
		fmReportInfo.setRefNum(medOrder.getRefNum());
		fmReportInfo.setOrderStatus(convertOrderStatus(medOrder));
		fmReportInfo.setOrderSubtype(medOrder.getCmsOrderSubType().getDataValue());
		fmReportInfo.setOrderNum(Integer.parseInt(medOrder.getOrderNum().substring(3)));
		fmReportInfo.setItemNum(itemNum);
		fmReportInfo.setFmStatus(FmStatus.GeneralDrug.getDataValue());
		fmReportInfo.setActionStatus(fluid.getActionStatus());
		fmReportInfo.setEisSpecCode(medOrder.getEisSpecCode());
		fmReportInfo.setImisSpecialty("R");
		fmReportInfo.setDoctorCode(medOrder.getDoctorCode());
		fmReportInfo.setMedicalOfficerName(medOrder.getDoctorName());
		fmReportInfo.setRank(medOrder.getDoctorRankCode());
		fmReportInfo.setBatchNum(0);
		fmReportInfo.setYear(yearFormat.format(batchStartDate.toDate()));
		fmReportInfo.setMonth(monthFormat.format(batchStartDate.toDate()));
		fmReportInfo.setClusterCode(mapClusterCode(medOrder.getWorkstore().getHospital().getHospitalCluster().getClusterCode()));
		fmReportInfo.setOrderDesc(fluid.getDiluentName());
		List<DmDrug> dmDrugList = dmDrugCacher.getDmDrugListByDiluentCode(medOrder.getWorkstore(), fluid.getDiluentCode());
		String sfiCat = "";
		if (!dmDrugList.isEmpty())
		{
			sfiCat = dmDrugList.get(0).getDmMoeProperty().getSfiCategory();
		}
		
		fmReportInfo.setSfiCateogry(sfiCat);
		fmReportInfo.setAddToDetailReport(addToDetailDataReportForFluid(fluid));
		fmRptConverter.convertFmFluidReportData(fmReportInfo, fluid);
		return fmReportInfo;
	}
	
	private Boolean addToDetailDataReportForFluid(Fluid fluid)
	{
		if (fluid.getActionStatus() == ActionStatus.PurchaseByPatient || fluid.getActionStatus() == ActionStatus.SafetyNet)
		{
			return true;
		}
		
		return false;
	}
	
	private FmReportInfo constructFmReportInfoForCapd(CapdRxDrug capdRxDrug, MedOrder medOrder, Integer itemNum)
	{
		FmReportInfo fmReportInfo = new FmReportInfo();
		fmReportInfo.setPatHospCode(medOrder.getPatHospCode());
		fmReportInfo.setOrderDate(medOrder.getOrderDate());
		fmReportInfo.setRefNum(medOrder.getRefNum());
		fmReportInfo.setOrderStatus(convertOrderStatus(medOrder));
		fmReportInfo.setOrderSubtype(medOrder.getCmsOrderSubType().getDataValue());
		fmReportInfo.setOrderNum(Integer.parseInt(medOrder.getOrderNum().substring(3)));
		fmReportInfo.setItemNum(itemNum);
		fmReportInfo.setFmStatus(FmStatus.GeneralDrug.getDataValue());
		fmReportInfo.setActionStatus(capdRxDrug.getActionStatus());
		fmReportInfo.setEisSpecCode(medOrder.getEisSpecCode());
		fmReportInfo.setImisSpecialty("R");
		fmReportInfo.setDoctorCode(medOrder.getDoctorCode());
		fmReportInfo.setMedicalOfficerName(medOrder.getDoctorName());
		fmReportInfo.setRank(medOrder.getDoctorRankCode());
		fmReportInfo.setBatchNum(0);
		fmReportInfo.setYear(yearFormat.format(batchStartDate.toDate()));
		fmReportInfo.setMonth(monthFormat.format(batchStartDate.toDate()));
		fmReportInfo.setClusterCode(mapClusterCode(medOrder.getWorkstore().getHospital().getHospitalCluster().getClusterCode()));
		fmReportInfo.setAddToDetailReport(addToDetailDataReportForCapd(capdRxDrug));
		
		fmRptConverter.convertFmCapdRxDrugReportData(fmReportInfo, capdRxDrug);
		return fmReportInfo;
	}
	
	private Boolean addToDetailDataReportForCapd(CapdRxDrug capdRxDrug)
	{
		if (capdRxDrug.getActionStatus() == ActionStatus.PurchaseByPatient || capdRxDrug.getActionStatus() == ActionStatus.SafetyNet)
		{
			return true;
		}
		
		return false;
	}

	private FmReportInfo constructUnvetOrderFmReportInfo(FmReportInfo fmReportInfo, MedOrder medOrder, MedCase medCase)
	{		
		fmReportInfo.setCaseNum(medCase.getCaseNum());
		fmReportInfo.setPasSpecCode(medCase.getPasSpecCode());
		fmReportInfo.setPasSubSpecCode(medCase.getPasSubSpecCode());
		fmReportInfo.setSpecCode(medOrder.getSpecCode());
		fmReportInfo.setHaIssue("N");			
		fmReportInfo.setPayCode(medCase.getPasPayCode());
		fmReportInfo.setPatKey(medCase.getCaseNum());
		
		if (medCase.getPasPatInd() == MedCasePasPatInd.Private) {
			fmReportInfo.setPrivatePat("Y");
		} 
		else 
		{
			fmReportInfo.setPrivatePat("N");
		}
		
		return fmReportInfo;
	}
	
	private FmReportInfo constructVettedOrderFmReportInfo(FmReportInfo fmReportInfo, MedOrder medOrder, MedCase medCase, PharmOrder pharmOrder,
			DispOrder dispOrder, DispOrderItem dispOrderItem)
	{		
		fmReportInfo.setCaseNum(medCase.getCaseNum());
		fmReportInfo.setPasSpecCode(medCase.getPasSpecCode());
		fmReportInfo.setPasSubSpecCode(medCase.getPasSubSpecCode());
		fmReportInfo.setSpecCode(dispOrder.getSpecCode());
		fmReportInfo.setHaIssue(convertHaIssue(dispOrder));
		fmReportInfo.setPayCode(medCase.getPasPayCode());
		
		if (StringUtils.isEmpty(pharmOrder.getPatKey())) 
		{
			fmReportInfo.setPatKey(pharmOrder.getMedCase().getCaseNum());
		} 
		else 
		{
			fmReportInfo.setPatKey(pharmOrder.getPatKey());
		}
		
		if (pharmOrder.getPrivateFlag()) 
		{
			fmReportInfo.setPrivatePat("Y");
		} 
		else 
		{
			fmReportInfo.setPrivatePat("N");
		}
						
		return fmReportInfo;
	}
	
	private String convertHaIssue(DispOrder dispOrder) 
	{
    	if (dispOrder.getStatus() == DispOrderStatus.Issued && dispOrder.getAdminStatus() != DispOrderAdminStatus.Suspended) 
    	{
    		return "Y";
    	}
    	
    	return "N";
    }
	
	private String convertOrderDesc(RxDrug rxDrug) 
	{
    	StringBuilder sb = new StringBuilder();
    	String volumeText = convertVolumeText(rxDrug);
    	
    	if (StringUtils.isNotBlank(rxDrug.getDisplayName())) 
    	{
    		sb.append(rxDrug.getDisplayName()).append(SPACE);
    	}
    	
    	if (StringUtils.isNotBlank(rxDrug.getSaltProperty())) 
    	{
    		sb.append(rxDrug.getSaltProperty()).append(SPACE);
    	}
    	
    	if (StringUtils.isNotBlank(rxDrug.getFormDesc())) 
    	{
    		sb.append(rxDrug.getFormDesc()).append(SPACE);
    	}

		if (StringUtils.isNotBlank(rxDrug.getStrength())) 
		{
			sb.append(rxDrug.getStrength()).append(SPACE);
		}
		
		if (StringUtils.isNotBlank(volumeText)) {
			sb.append(volumeText);
		}
		
    	return sb.toString().toUpperCase();
    }
	
	private String convertVolumeText(RxDrug rxDrug) 
	{
    	StringBuilder sb = new StringBuilder();    	
    	if (rxDrug.getVolume() != null && rxDrug.getVolume().compareTo(BigDecimal.ZERO) > 0 && StringUtils.isNotBlank(rxDrug.getVolumeUnit())) 
    	{
    		sb.append((new DecimalFormat(VOLUME_PATTERN)).format(rxDrug.getVolume())).append(rxDrug.getVolumeUnit().toLowerCase());
    	}
    	
    	return sb.toString();
    }

    private String convertOrderStatus(MedOrder medOrder)
    {
    	if (medOrder.getStatus() == MedOrderStatus.Outstanding || medOrder.getStatus() == MedOrderStatus.Withhold || 
    		medOrder.getStatus() == MedOrderStatus.PreVet)
    	{
    		return "unvet";
    	}
    	
    	return "vetted";
    }
    
    private boolean isSfiItem(MedOrderItem medOrderItem)
    {		
		if (medOrderItem.getRxItemType() == RxItemType.Oral)
		{
			if (medOrderItem.getRxDrug().getActionStatus() == ActionStatus.PurchaseByPatient)
			{
				return true;
			}
		}
		else if (medOrderItem.getRxItemType() == RxItemType.Injection)
		{						
			if (medOrderItem.getRxDrug().getActionStatus() == ActionStatus.PurchaseByPatient)
			{
				return true;
			}
				
			for (DoseGroup doseGroup : medOrderItem.getRxDrug().getRegimen().getDoseGroupList())
			{
				for (Dose dose : doseGroup.getDoseList())
				{		    					
					if (dose.getDoseFluid() != null)
					{
						if (dose.getDoseFluid().getActionStatus() == ActionStatus.PurchaseByPatient)
						{
							return true;
						}
					}
				}
			}
		}
		else if (medOrderItem.getRxItemType() == RxItemType.Iv)
		{
			for (IvAdditive ivAdditive : medOrderItem.getIvRxDrug().getIvAdditiveList())
			{
				if (ivAdditive.getActionStatus() == ActionStatus.PurchaseByPatient)
				{
					return true;
				}
			}
			
			for (IvFluid ivFluid : medOrderItem.getIvRxDrug().getIvFluidList())
			{
				if (ivFluid.getActionStatus() == ActionStatus.PurchaseByPatient)
				{
					return true;
				}
			}
		}
		else if (medOrderItem.getRxItemType() == RxItemType.Capd)
		{
			if (medOrderItem.getCapdRxDrug().getActionStatus() ==  ActionStatus.PurchaseByPatient)
			{
				return true;
			}
		}				
		
		return false;
    }
    
    private class FmDetailReport
    {
    	private List<FmReportInfo> fmReportAllDataList;
    	private List<FmReportInfo> fmReportDataList;
    	
    	public FmDetailReport()
    	{
    		fmReportAllDataList = new ArrayList<FmReportInfo>();
    		fmReportDataList = new ArrayList<FmReportInfo>();
    	}

		public List<FmReportInfo> getFmReportAllDataList() {
			return fmReportAllDataList;
		}

		public List<FmReportInfo> getFmReportDataList() {
			return fmReportDataList;
		}
    }
}