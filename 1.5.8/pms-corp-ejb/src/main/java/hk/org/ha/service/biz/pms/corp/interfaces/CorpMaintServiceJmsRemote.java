package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;

import java.util.List;

import org.osoa.sca.annotations.OneWay;

public interface CorpMaintServiceJmsRemote {

	@OneWay
	void receiveItemBinList(List<ItemLocation> itemLocationList, Workstore workstore);
	
}
