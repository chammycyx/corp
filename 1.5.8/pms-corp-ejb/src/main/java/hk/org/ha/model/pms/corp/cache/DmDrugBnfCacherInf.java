package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;

public interface DmDrugBnfCacherInf extends BaseCacherInf {

	String getDmDrugBnfNum(String itemCode);
	
}
