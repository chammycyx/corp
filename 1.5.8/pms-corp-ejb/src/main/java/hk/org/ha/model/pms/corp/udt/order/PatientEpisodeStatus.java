package hk.org.ha.model.pms.corp.udt.order;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PatientEpisodeStatus implements StringValuedEnum {

	None("N", "None"),
	Completed("C", "Completed"),
	NoRxAssociated("X", "No Rx Associated"),
	PasException("E", "PasException");
	
    private final String dataValue;
    private final String displayValue;
        
    PatientEpisodeStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static PatientEpisodeStatus dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(PatientEpisodeStatus.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<PatientEpisodeStatus> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PatientEpisodeStatus> getEnumClass() {
    		return PatientEpisodeStatus.class;
    	}
    }
}
