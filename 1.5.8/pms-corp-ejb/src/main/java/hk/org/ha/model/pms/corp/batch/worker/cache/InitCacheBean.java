package hk.org.ha.model.pms.corp.batch.worker.cache;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.service.biz.pms.asp.interfaces.AspSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.cmm.interfaces.CmmSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.dpp.interfaces.DppSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.dqa.interfaces.DqaSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.AsaSubscriberJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * Session Bean implementation class InitCacheBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("initCacheBean")
public class InitCacheBean implements InitCacheLocal {
	
	private static final String PMS = "PMS";
	private static final String DPP = "DPP";
	private static final String DQA = "DQA";
	private static final String CMM = "CMM";
	private static final String DMS = "DMS";
	private static final String ASP = "ASP";
	
	private static final String CORP = "CORP";

	private Logger logger = null;
	
	private String appCode = null;
	
	private String clusterCode = null;
	
	private String loadDrugFrom = null;
	
	@In
    private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		
		BatchParameters params = info.getBatchParameters();
		
		appCode = params.getString("appCode");
		
		clusterCode = params.getString("clusterCode");
		
		loadDrugFrom = params.getString("loadDrugFrom");
	}

	public void beginChunk(Chunk arg0) throws Exception {
	}

	public void processRecord(Record arg0) throws Exception {

		Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);
                
        if (appCode == null || clusterCode != null || PMS.equals(appCode)) {
        	if (CORP.equals(loadDrugFrom)) {        		
        		logger.info("start loading dmDrugList for PMS");
	        	long startTime = System.currentTimeMillis();
	        	List<DmDrug> dmDrugList = dmsPmsServiceProxy.retrieveDmDrugList();
	        	logger.info("finished loading dmDrugList for PMS, timeused:" + (System.currentTimeMillis() - startTime) + " size:" + dmDrugList.size());
	        	if (clusterCode == null) {
		        	this.getCorpSubscriberProxy().initCacheWithDmDrugList(dmDrugList);
            		this.getPmsSubscriberProxy().initCacheWithDmDrugList(dmDrugList);
                	this.getAsaSubscriberProxy().initCache();
	        	} else {
	        		this.getPmsSubscriberProxy().initCacheByClusterWithDmDrugList(clusterCode, dmDrugList);
	        	}
        	} else {
	        	if (clusterCode == null) {
	        		this.getCorpSubscriberProxy().initCache();        
            		this.getPmsSubscriberProxy().initCache();
                	this.getAsaSubscriberProxy().initCache();
	        	} else {
	        		this.getPmsSubscriberProxy().initCacheByCluster(clusterCode);
	        	}
        	}
        }
                
        if ((appCode == null && clusterCode == null) || DPP.equals(appCode)) {
        	this.getDppSubscriberProxy().initCache();
        }
        if ((appCode == null && clusterCode == null) || DQA.equals(appCode)) {
        	this.getDqaSubscriberProxy().initCache();
        }
        if ((appCode == null && clusterCode == null) || CMM.equals(appCode)) {
        	this.getCmmSubscriberProxy().initCache();
        }
        if ((appCode == null && clusterCode == null) || DMS.equals(appCode)) {
        	this.getDmsSubscriberProxy().initCache();
        }
		if ((appCode == null && clusterCode == null) || ASP.equals(appCode)) {
			this.getAspSubscriberProxy().initCache();
		}
			
		Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}

	private CorpSubscriberJmsRemote getCorpSubscriberProxy() {	
		return (CorpSubscriberJmsRemote) Component.getInstance("corpSubscriberProxy", false);
	}

	private PmsSubscriberJmsRemote getPmsSubscriberProxy() {	
		return (PmsSubscriberJmsRemote) Component.getInstance("pmsSubscriberProxy", false);
	}
	
	private AsaSubscriberJmsRemote getAsaSubscriberProxy() {	
		return (AsaSubscriberJmsRemote) Component.getInstance("asaSubscriberProxy", false);
	}

	private DppSubscriberJmsRemote getDppSubscriberProxy() {	
		return (DppSubscriberJmsRemote) Component.getInstance("dppSubscriberProxy", false);
	}
	
	private DqaSubscriberJmsRemote getDqaSubscriberProxy() {	
		return (DqaSubscriberJmsRemote) Component.getInstance("dqaSubscriberProxy", false);
	}
	
	private CmmSubscriberJmsRemote getCmmSubscriberProxy() {	
		return (CmmSubscriberJmsRemote) Component.getInstance("cmmSubscriberProxy", false);
	}

	private DmsSubscriberJmsRemote getDmsSubscriberProxy() {
		return (DmsSubscriberJmsRemote) Component.getInstance("dmsSubscriberProxy", false);
	}
	
	private AspSubscriberJmsRemote getAspSubscriberProxy() {
		return (AspSubscriberJmsRemote) Component.getInstance("aspSubscriberProxy", false);
	}

	public void endChunk() throws Exception {
	}

    @Remove
	public void destroyBatch() {
	}
}
