package hk.org.ha.model.pms.corp.batch.worker.downtime;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.persistence.corp.HospitalCluster;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.joda.time.DateTime;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

@Stateful
@Scope(ScopeType.SESSION)
@Name("upldDtDbBean")
public class UpldDtDbBean implements UpldDtDbLocal {

	private static final String REFDATA_PREFIX = "refdata";

	private Logger logger = null;
	
	private Date batchDate = null;
	private String dtDbDir = null;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		
	@In
	private ApplicationProp applicationProp;

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		
		this.logger = logger;
		
		BatchParameters params = info.getBatchParameters();
		
		if ( null == params.getDate("batchDate") ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = new DateTime(params.getDate("batchDate")).plusDays(1).toDate();
		}
		
		if ( null == params.getString("dtDbDir") ) {
			throw new Exception("batch parameter : dtDbDir is null");
		} else {
			dtDbDir = params.getString("dtDbDir");
		}
			
	}

	public void beginChunk(Chunk chunk) {
	}

	@SuppressWarnings("unchecked")
	public void processRecord(Record record) throws SftpException {

		Context context = Contexts.getEventContext();
		context.set("sftp_downtime_host", applicationProp.getSftpDowntimeHost());
		context.set("sftp_downtime_username", applicationProp.getSftpDowntimeUsername());
		context.set("sftp_downtime_password", applicationProp.getSftpDowntimePassword());
		context.set("sftp_downtime_rootDir", applicationProp.getSftpDowntimeRootDir());
		ChannelSftp downtimeChannelSftp = (ChannelSftp) Component.getInstance("downtimeChannelSftp");

		
    	Date startDatetime = new Date();
		logger.info("process record start... @" + startDatetime);
		
		List<HospitalCluster> hospitalClusterList = (List<HospitalCluster>) em.createQuery(
				"select o from HospitalCluster o" + // 20160616 index check : none
				" where o.status = :status")
				.setParameter("status", RecordStatus.Active)
				.getResultList();
		
		File outputFile;
		for ( HospitalCluster hospitalCluster : hospitalClusterList ) {
			String clusterCode = hospitalCluster.getClusterCode();
			logger.info("process record ... " + clusterCode);
			
			outputFile = new File(applicationProp.getBatchDataDir()+"/"+dtDbDir+"/pms-dt-"+StringUtils.lowerCase(clusterCode)+
									"-"+dateFormat.format(batchDate)+
									".accde");

			downtimeChannelSftp.put(outputFile.getPath(), outputFile.getName());

			logger.info(" DT template file SFTP source : " + outputFile.getPath());
			logger.info(" DT template file SFTP destination : " + outputFile.getName());
			
			outputFile = new File(applicationProp.getBatchDataDir()+"/"+dtDbDir+"/pms-dt-"+StringUtils.lowerCase(clusterCode)+
									"-"+dateFormat.format(batchDate)+
									"-"+REFDATA_PREFIX +
									".accde");
			
			logger.info(" DT refTable file SFTP source : " + outputFile.getPath());
			logger.info(" DT refTable file SFTP destination : " + outputFile.getName());
			
			downtimeChannelSftp.put(outputFile.getPath(), outputFile.getName());
		}

    	Date endDatetime = new Date();
        logger.info("process record complete... @" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
				
	}

	public void endChunk() {
	}
    
    @Remove
    public void destroyBatch() {
    }

}
