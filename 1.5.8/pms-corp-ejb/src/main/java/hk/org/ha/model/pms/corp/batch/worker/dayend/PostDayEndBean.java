package hk.org.ha.model.pms.corp.batch.worker.dayend;


import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * Session Bean implementation class DayEndBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("postDayEndBean")
public class PostDayEndBean implements PostDayEndLocal {

	private static final String FCS = "FCS";
	private static final String PMS = "PMS";
	private static final String SLASH = "/";
	private static final String DOT = ".";
	private static final String MONTH_DIR_NAME = "month";
	private static final String COUNT_FILE_EXTENSION = "cnt";
	
	@In
	private DayEndOrderManagerLocal dayEndOrderManager;
	
	@In
	private ApplicationProp applicationProp;

	private Logger logger = null;

	private Date batchDate = null;
	private String jobDir = null;
	
	private String year = null;
	private String month = null;
	private String day = null;

	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	
	public void beginChunk(Chunk chunk) throws Exception {
		
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		String jobId = info.getJobId();
		String lastToken = jobId.substring(jobId.lastIndexOf("_")+1);
		if ("HOSP".equals(lastToken)) {
			jobDir = jobId.substring(0, jobId.lastIndexOf("_"));
		} else {
			jobDir = jobId;
		}
		
		BatchParameters params = info.getBatchParameters();
		
		if (params.getDate("batchDate") == null) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
			year = DateTimeFormat.forPattern("yyyy").print(new DateTime(batchDate));
			month = DateTimeFormat.forPattern("MM").print(new DateTime(batchDate));
			day = DateTimeFormat.forPattern("dd").print(new DateTime(batchDate));
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
	}
	
	public void processRecord(Record record) throws Exception {
		
		String hospCode = record.getString("HOSP_CODE");
		
		Date startDatetime = new Date();
		logger.info("process record start for hospCode [" + hospCode + "]... @" + startDatetime);
		
		BatchJobState batchJobState = dayEndOrderManager.retrieveBatchJobState(jobDir, batchDate, hospCode, null);
		if (batchJobState != null) {
			logger.info("Job " + jobDir + " is already done for batchDate [" + batchDate + "], hospCode [" + hospCode + "]");
			return;
		}
		
		// file name (primitive) classification
		List<String> fcsFilePrefixList = Arrays.asList(applicationProp.getFcsChargingTrxFile());
		List<String> pmsFilePrefixList = Arrays.asList(applicationProp.getDnldTrxFile());
		
		//////// Directory involved //////
		File inFcsDir, inPmsDir;
		File workFcsDir, workPmsDir;
		File outFcsDir, outPmsDir;
		File doneFcsDir, donePmsDir;
		File errorFcsDir, errorPmsDir;
		File hospFcsChargingTrxFile, hospFcsChargingTrxCountFile, hospPmsDlTrxFile, dnldFile;
		
		try {
			inFcsDir = getDirectory(hospCode, jobDir, DataDirType.In, FCS);
			inPmsDir = getDirectory(hospCode, jobDir, DataDirType.In, PMS);
	
			workFcsDir = getDirectory(hospCode, jobDir, DataDirType.Work, FCS);
			workPmsDir = getDirectory(hospCode, jobDir, DataDirType.Work, PMS);
	
			outFcsDir = getOutDoneDirectory(jobDir, DataDirType.Out, FCS, hospCode, true);
			outPmsDir = getOutDoneDirectory(jobDir, DataDirType.Out, PMS, hospCode, true);
	
			doneFcsDir = getOutDoneDirectory(jobDir, DataDirType.Done, FCS, hospCode, true);
			donePmsDir = getOutDoneDirectory(jobDir, DataDirType.Done, PMS, hospCode, true);
	
			errorFcsDir = getDirectory(hospCode, jobDir, DataDirType.Error, FCS);
			errorPmsDir = getDirectory(hospCode, jobDir, DataDirType.Error, PMS);
			
			// final Files output in out folder
			hospFcsChargingTrxFile = getHospFcsFile(workFcsDir, applicationProp.getFcsChargingTrxFile(), batchDate, hospCode, null);
			hospFcsChargingTrxCountFile = getHospFcsFile(workFcsDir, applicationProp.getFcsChargingTrxFile(), batchDate, hospCode, COUNT_FILE_EXTENSION);
			hospPmsDlTrxFile = getFile(workPmsDir, applicationProp.getDnldTrxFile(), batchDate, null);
			
			dnldFile = getHospPmsDnldFile(hospCode);

			// validate that hospital file does not exist in OUT dir
			// (since file is unknown file one when entry does not exists in BatchJobState, exception is thrown)
			checkFileExist(outFcsDir, Arrays.asList(hospFcsChargingTrxFile.getName()));
			checkFileExist(outFcsDir, Arrays.asList(hospFcsChargingTrxCountFile.getName()));
			checkFileExist(outPmsDir, Arrays.asList(hospPmsDlTrxFile.getName()));
			
		} catch (Exception e) {
			// workstore file of IN folder does not move to ERROR folder for post day end retry 
	        logger.info("Throw exception: " + hospCode + " - " + e.getMessage());
	        throw e;
		}
		
		try {
			// validate that workstore files exists in IN dir
			validateFile(inFcsDir, fcsFilePrefixList);
			validateFile(inPmsDir, pmsFilePrefixList);
			
		} catch (Exception e) {
			// workstore file of IN folder does not move to ERROR folder for post day end retry 
			
			// do not throw exception for non-server exception
			logger.error("Exception, batchDate=" + batchDate + ",hospCode=" + hospCode + " - ", e);
			return;
		}
		
		try {
			moveFromDirectoryToDirectory(inFcsDir, workFcsDir, fcsFilePrefixList);
			moveFromDirectoryToDirectory(inPmsDir, workPmsDir, pmsFilePrefixList);
			
			concat(workFcsDir, applicationProp.getFcsChargingTrxFile(), hospFcsChargingTrxFile, hospFcsChargingTrxCountFile);
			concat(workPmsDir, applicationProp.getDnldTrxFile(), hospPmsDlTrxFile);
			
			// copy dnld file to dnld path
			FileUtils.copyFile(hospPmsDlTrxFile, dnldFile);
			if (logger.isDebugEnabled()) {
				logger.debug("processRecord: copied " + hospPmsDlTrxFile.getPath() + " to " + dnldFile.getPath());
			}
			
			// move intermediate files
			FileUtils.moveFileToDirectory(hospFcsChargingTrxFile, outFcsDir, false);
			FileUtils.moveFileToDirectory(hospFcsChargingTrxCountFile, outFcsDir, false);
			FileUtils.moveFileToDirectory(hospPmsDlTrxFile, outPmsDir, false);
			
			// move workstore files
			moveFromDirectoryToDirectory(workFcsDir, doneFcsDir, fcsFilePrefixList, startDatetime);
			moveFromDirectoryToDirectory(workPmsDir, donePmsDir, pmsFilePrefixList, startDatetime);
			
		} catch (Exception e) {
	        // move back workstore files to IN folder for post day end retry
			moveFromDirectoryToDirectory(workFcsDir, inFcsDir, fcsFilePrefixList);
			moveFromDirectoryToDirectory(workPmsDir, inPmsDir, pmsFilePrefixList);
	        
			// move hospital files to ERROR folder
			moveAllFileFromDirectoryToDirectory(workFcsDir, errorFcsDir, startDatetime);
			moveAllFileFromDirectoryToDirectory(workPmsDir, errorPmsDir, startDatetime);
			
	        logger.info("Throw exception: " + hospCode + " - " + e.getMessage());
	        throw e;
		}
		
		// save to log table when day end job is done
		dayEndOrderManager.saveBatchJobState(jobDir, batchDate, hospCode, null);
		
		Date endDatetime = new Date();
        logger.info("process record complete for hospCode [" + hospCode + "]..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}

	private File getDirectory(
			String hospCode, 
			String jobId,
			DataDirType dataDirType, 
			String systemType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + SLASH + 
				jobId + SLASH + 
				dataDirType.getDataValue() + SLASH + 
				systemType + SLASH + 
				hospCode);
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}

	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
			logger.debug("PostDayEndBean - createDirectory: create path status: " + created);
		}
		
		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("PostDayEndBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("PostDayEndBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("PostDayEndBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}

	private File getOutDoneDirectory(
			String jobId,
			DataDirType dataDirType,
			String systemType,
			String hospCode,
			boolean createDir) throws Exception {
		
		StringBuilder basicPath = new StringBuilder();
		basicPath.append(applicationProp.getBatchDataDir())
						.append(SLASH).append(jobId)
						.append(SLASH).append(dataDirType.getDataValue());
		if (systemType != null) {
			basicPath.append(SLASH).append(systemType);
		}
		basicPath.append(SLASH).append(MONTH_DIR_NAME);
		
		return getOutDoneDirectory(basicPath.toString(), hospCode, createDir);
	}
	
	private File getOutDoneDirectory(
			String basicPath,
			String hospCode,
			boolean createDir) throws Exception {
		
		File basicDir = new File(basicPath);
		if ( ! basicDir.exists()) {
			throw new Exception("PostDayEndBean: date directory could not be created as parent directory " + basicDir.getParent() + " not exists.");
		}

		StringBuilder destPath = new StringBuilder();
		destPath.append(basicPath)
						.append(SLASH).append(year)
						.append(SLASH).append(month)
						.append(SLASH).append(hospCode);
		
		File destDir = new File(destPath.toString());
		if ( ! destDir.exists()) {
			if (createDir) {
				if ( ! destDir.mkdirs()) {
					throw new Exception("PostDayEndBean: directory " + destDir.getPath() + " could not be found and created. Please check (eg. Access Right...).");
				}
			} else {
				throw new Exception("PostDayEndBean: directory " + destDir.getPath() + " could not be found. Please check (eg. Access Right...).");
			}
		}
		
		return destDir;
	}
	
	private File getFile(
			File dir, 
			String fileName,
			Date batchDate,
			Date currentDate) {
				
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(dateFormat.format(batchDate));
		}
		
		if (currentDate != null) {
			finalFileName.append(DOT)
						 .append(dateTimeFormat.format(currentDate));
		}
		
		File file = new File(
				    dir + SLASH + 
					finalFileName );
		
		return file;
	}
	
	private File getHospFcsFile(
			File dir, 
			String fileName,
			Date batchDate,
			String hospCode,
			String fileExtension) {
		
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(dateFormat.format(batchDate));
		}
		
		if (hospCode != null) {
			finalFileName.append(DOT)
						 .append(hospCode);
		}

		if (fileExtension != null) {
			finalFileName.append(DOT)
						 .append(fileExtension);
		}
		
		File file = new File(
				    dir + SLASH + 
					finalFileName );
		
		return file;
	}

	private File getHospPmsDnldFile(String hospCode) throws Exception {
		File destDir = new File(applicationProp.getBatchDataDir() + SLASH + applicationProp.getDnldTrxDataSubDir() + SLASH + MONTH_DIR_NAME + SLASH + year + SLASH + month + SLASH + hospCode);
		if (!destDir.exists()) {
			if(!destDir.mkdirs()) {
				throw new Exception("PostDayEndBean: directory " + destDir.getPath() + " could not be found. Please check (eg. Access Right...).");
			}
		}
		
		return new File(destDir.getPath() + SLASH + applicationProp.getDnldTrxFile() + "-" + (month+day) + ".dat");
	}
	
	private void validateFile(File dir, List<String> fileNameList) throws Exception {
		File[] files = dir.listFiles();
		
		final String allFileNamePattern = "^(" + StringUtils.join(fileNameList.toArray(), "|") + ")\\.[A-Za-z0-9]+\\.[0-9]{8}$";
		Pattern allDayendFileNamePattern = Pattern.compile(allFileNamePattern, Pattern.CASE_INSENSITIVE);
		
		Map<String, List<File>> fileMap = new HashMap<String, List<File>>();
		
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
		
		for (String fileName: fileNameList) {
			fileMap.put(fileName, new ArrayList<File>());
			
			Pattern fileNamePattern = Pattern.compile("^" + fileName + "\\.[A-Za-z0-9]+\\.[0-9]{8}$", Pattern.CASE_INSENSITIVE);
			
			for (File file: files) {
				
				if (!file.isFile()) {
					throw new Exception("PostDayEndBean: " + file.getPath() + " is not a file.");
				}
				
				if (!allDayendFileNamePattern.matcher(file.getName()).find()) {
					throw new Exception("PostDayEndBean: invalid file format - " + file.getPath());
				}
				
				try {
					
					String [] fileNamePart = file.getName().split("\\.");
					String datePart = fileNamePart[2];
					
					DateTime fileDate = formatter.parseDateTime(datePart);
					
			        if (!fileDate.isEqual(batchDate.getTime()) && 
			        		!fileDate.isBefore(batchDate.getTime())) {
			        	throw new Exception("PostDayEndBean: invalid file date (date should be equal to or before the batch date) - " + file.getPath());
			        }
			        
			        if (fileNamePattern.matcher(file.getName()).find()) {
			        	fileMap.get(fileName).add(file);
			        }
			        
				} catch (IllegalFieldValueException e) {
					throw new Exception("PostDayEndBean: invalid file date - " + file.getPath());
				} catch(ArrayIndexOutOfBoundsException e) {
					throw new Exception("PostDayEndBean: invalid file format - " + file.getPath());
				}
			}
		}
		
		for (String fileName: fileNameList) {
			if (fileMap.get(fileName) == null || fileMap.get(fileName).isEmpty()) {
				throw new Exception("PostDayEndBean: missing file(s) - " + fileName + " in " + dir.getPath());
			}
		}
	}
	
	private void checkFileExist(File dir, List<String> fileNameList) throws Exception {
		for (String fileName: fileNameList) {
			File file = getFile(dir, fileName, null, null);
			if (file.exists()) {
				throw new Exception("PostDayEndBean: file already exists - " + file.getPath());
			}
		}
	}
	
	// for getting specific batch of files
	private FileFilter getWorkstoreFileFilter(final String dayEndFileName) {
	
		FileFilter fileFilter = new FileFilter() {
			private Pattern fileNamePattern = Pattern.compile("^" + dayEndFileName + "\\.[A-Za-z0-9]+\\.[0-9]{8}$", Pattern.CASE_INSENSITIVE);
			private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			
			public boolean accept(File file) {
				boolean isValid = false;
				
				if (file.isFile() && fileNamePattern.matcher(file.getName()).find()) {
					formatter.setLenient(false);
					
					try {
						String [] fileNamePart = file.getName().split("\\.");
						String datePart = fileNamePart[2];
						
						Date fileDate = formatter.parse(datePart);
						
						DateTime dtFileDate = new DateTime(fileDate.getTime());
				        if (dtFileDate.isEqual(batchDate.getTime()) || 
				        		dtFileDate.isBefore(batchDate.getTime())) {
				        	isValid = true;
				        }
					} catch (ParseException e) {
						isValid = false;
					} catch(ArrayIndexOutOfBoundsException e) {
						isValid = false;
					}
				}
				
				return isValid;
			}
		};
		
		return fileFilter;
	}
	
	private File concat(File dir, String fileNameMask, File outFile) throws Exception {
		return concat(dir, fileNameMask, outFile, null);
	}
	
	private File concat(File dir, String fileNameMask, File outFile, File countFile) throws Exception {
		int recCount = 0;
		
		File [] fileList = dir.listFiles(getWorkstoreFileFilter(fileNameMask));
		if (fileList.length > 0) {
			BufferedWriter outWriter = null;
			
			try {
				outWriter = new BufferedWriter(new FileWriter(outFile));
				for (File file: fileList) {
					BufferedReader reader = new BufferedReader(new FileReader(file));
					
					try {
						String line = null;
						while((line = reader.readLine()) != null) {
							outWriter.write(line);
							outWriter.newLine();
							recCount++;
						}
					} finally {
						reader.close();
					}
				}
			} finally {
				if (outWriter != null) {
					outWriter.close();
				}
			}
		}
		
		if (countFile != null) {
			BufferedWriter outCountWriter = null;
			
			try {
				outCountWriter = new BufferedWriter(new FileWriter(countFile));
				outCountWriter.write(String.valueOf(recCount));
				outCountWriter.newLine();
			} finally {
				if (outCountWriter != null) {
					outCountWriter.close();
				}
			}
		}
		
		return outFile;
	}

	private void moveFromDirectoryToDirectory(File srcDir, File destDir, List<String> fileNameList) throws IOException {
		for (String fileName: fileNameList) {
			File[] files = srcDir.listFiles(getWorkstoreFileFilter(fileName));
			for (File file: files) {
				if (file.isFile()) {
					FileUtils.moveFileToDirectory(file, destDir, false);
				} else if (file.isDirectory()) {
					FileUtils.moveDirectoryToDirectory(file, destDir, false);
				}
			}
		}
	}
	
	private void moveFromDirectoryToDirectory(File srcDir, File destDir, List<String> fileNameList, Date currentDate) throws IOException {
		for (String fileName: fileNameList) {
			File[] files = srcDir.listFiles(getWorkstoreFileFilter(fileName));
			for (File file: files) {
				if (file.isFile()) {
					FileUtils.moveFile(file, getFile(destDir, file.getName(), null, currentDate));
				} else if (file.isDirectory()) {
					FileUtils.moveDirectory(file, getFile(destDir, file.getName(), null, currentDate));
				}
			}
		}
	}
	
	private void moveAllFileFromDirectoryToDirectory(File srcDir, File destDir, Date currentDate) throws IOException {
		File[] files = srcDir.listFiles();
		for (File file: files) {
			if (file.isFile()) {
				FileUtils.moveFile(file, getFile(destDir, file.getName(), null, currentDate));
			} else if (file.isDirectory()) {
				FileUtils.moveDirectory(file, getFile(destDir, file.getName(), null, currentDate));
				//FileUtils.moveDirectoryToDirectory(file, destDir, false); // TODO: test
			}
			
		}
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
}
