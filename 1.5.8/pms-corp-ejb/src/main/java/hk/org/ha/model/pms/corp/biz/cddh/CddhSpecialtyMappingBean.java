package hk.org.ha.model.pms.corp.biz.cddh;

import hk.org.ha.model.pms.persistence.corp.CddhSpecialtyMapping;
import hk.org.ha.model.pms.persistence.corp.CddhSpecialtyMappingPK;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.auth.Subject;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import weblogic.security.Security;
import weblogic.security.principal.WLSUserImpl;

@Stateful
@Scope(ScopeType.SESSION)
@Name("cddhSpecialtyMapping")
public class CddhSpecialtyMappingBean implements CddhSpecialtyMappingLocal {
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	private String userName;
	
	private String hospCode;
	
	private String specCode;
	
	private String cddhSpecCode;
	
	private boolean newRecord = true; 

	private boolean saveComplete = false; 
	
	private String successMessage;
	
	private String errorMessage;
	

    // for JSF binding start
	public String getUserName() {
		Subject subject = Security.getCurrentSubject();  
		Set<Principal> allPrincipals = subject.getPrincipals();
		for (Principal principal : allPrincipals) {  
			if (principal instanceof WLSUserImpl) {
				Identity.instance().getCredentials().setUsername(principal.getName());
				return principal.getName();  
			}              
		}
		
		return "";
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getCddhSpecCode() {
		return cddhSpecCode;
	}

	public void setCddhSpecCode(String cddhSpecCode) {
		this.cddhSpecCode = cddhSpecCode;
	}

	public boolean getNewRecord() {
		return newRecord;
	}

	public void setNewRecord(boolean newRecord) {
		this.newRecord = newRecord;
	}

	public boolean getSaveComplete() {
		return saveComplete;
	}

	public void setSaveComplete(boolean saveComplete) {
		this.saveComplete = saveComplete;
	}

	public boolean getIsSuccess() {
		return (!StringUtils.isBlank(successMessage));
	}
	
	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public boolean getHasError() {
		return (!StringUtils.isBlank(errorMessage));
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
    // for JSF binding end

	
    @Override
	public void retrieveCddhSpecialtyMapping() {
		// --------- pre process before search ------------
		successMessage = null;
		errorMessage = null;
		// ------------------------------------------------

		String inHospCode = StringUtils.trimToEmpty(hospCode);
		String inSpecCode = StringUtils.trimToEmpty(specCode);
		
		validateHospCodeSpecCode(inHospCode, inSpecCode);
		if (errorMessage != null) {
			return;
		}
		
		CddhSpecialtyMappingPK cddhSpecialtyMappingPK = new CddhSpecialtyMappingPK(inHospCode, inSpecCode);				
		CddhSpecialtyMapping cddhSpecialtyMapping = em.find(CddhSpecialtyMapping.class, cddhSpecialtyMappingPK);
		
		if (cddhSpecialtyMapping == null || cddhSpecialtyMapping.getStatus() != RecordStatus.Active) {
			errorMessage = "Record not found";
			cddhSpecCode = null;
			return;
		}
		
		cddhSpecCode = cddhSpecialtyMapping.getCddhSpecCode();
    	newRecord = false;
    }

    @Override
	public void updateCddhSpecialtyMapping() {
		// --------- pre process before search ------------
		successMessage = null;
		errorMessage = null;
		// ------------------------------------------------

		String inHospCode = StringUtils.trimToEmpty(hospCode);
		String inSpecCode = StringUtils.trimToEmpty(specCode);
		String inCddhSpecCode = StringUtils.trimToEmpty(cddhSpecCode);
		
		if (newRecord) {
			validateHospCodeSpecCode(inHospCode, inSpecCode);
			if (errorMessage != null) {
				return;
			}
		}
		
		validateCddhSpecCode(inCddhSpecCode);
		if (errorMessage != null) {
			return;
		}
		
		if (newRecord) {
			validateDuplication(inHospCode, inSpecCode);
			if (errorMessage != null) {
				return;
			}
		}
		
		CddhSpecialtyMapping cddhSpecialtyMapping = null;
		
		if (newRecord) {
			cddhSpecialtyMapping = new CddhSpecialtyMapping();
			cddhSpecialtyMapping.setHospCode(inHospCode);
			cddhSpecialtyMapping.setSpecCode(inSpecCode);
			cddhSpecialtyMapping.setCddhSpecCode(inCddhSpecCode);
			cddhSpecialtyMapping.setStatus(RecordStatus.Active);
			
			cddhSpecialtyMapping.setCreateUser(getUserName());
			cddhSpecialtyMapping.setUpdateUser(getUserName());
			
			em.persist(cddhSpecialtyMapping);
			
		} else {
			CddhSpecialtyMappingPK cddhSpecialtyMappingPK = new CddhSpecialtyMappingPK(inHospCode, inSpecCode);				
			cddhSpecialtyMapping = em.find(CddhSpecialtyMapping.class, cddhSpecialtyMappingPK);
			
			cddhSpecialtyMapping.setCddhSpecCode(inCddhSpecCode);
			
			cddhSpecialtyMapping.setUpdateUser(getUserName());
		}
		
		// --------- post process after send message ------------
		successMessage = "Record is saved.";
		saveComplete = true;
		// --------------------------------------------------------
	}
    
    @Override
	public void clearScreen() {
    	hospCode = null;
    	specCode = null;
    	cddhSpecCode = null;
    	
    	newRecord = true;
    	saveComplete = false;
    	
		successMessage = null;
		errorMessage = null;
	}

    @SuppressWarnings("unchecked")
    private void validateHospCodeSpecCode(String hospCode, String specCode) {
		if (StringUtils.isBlank(hospCode)) {
			errorMessage = "Please input hospital code";
			return;
		}
		
		if (StringUtils.isBlank(specCode)) {
			errorMessage = "Please input specialty code";
			return;
		}
		
		Hospital hospital = em.find(Hospital.class, hospCode);
		if (hospital == null || hospital.getStatus() != RecordStatus.Active) {
			errorMessage = "Invalid hospital code";
			return;
		}
		
		List<Specialty> specialtyList = em.createQuery( 
				"select o from Specialty o" + // 20150206 index check : none
				" where o.specCode = :specCode")
				.setParameter("specCode", specCode)
				.getResultList();

		boolean activeSpecialtyExist = false;
		for (Specialty specialty : specialtyList) {
			if (specialty.getStatus() == RecordStatus.Active) {
				activeSpecialtyExist = true;
				break;
			}
		}
		
		if ( ! activeSpecialtyExist) {
			errorMessage = "Invalid specialty code";
			return;
		}
    }    

    private void validateCddhSpecCode(String cddhSpecCode) {
		if (StringUtils.isBlank(cddhSpecCode)) {
			errorMessage = "Please input CDDH specialty code";
			return;
		}
    }
    
    private void validateDuplication(String hospCode, String specCode) {
		CddhSpecialtyMappingPK cddhSpecialtyMappingPK = new CddhSpecialtyMappingPK(hospCode, specCode);				
		if (em.find(CddhSpecialtyMapping.class, cddhSpecialtyMappingPK) != null) {
			errorMessage = "Duplicate record is inserted";
			return;
		}    	
    }
    
	@Remove
	public void destroy() 
	{
	}	
}
