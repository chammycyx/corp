package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmCorpIndication;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmCorpIndicationCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmCorpIndicationCacher extends BaseCacher implements DmCorpIndicationCacherInf {
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;
	
	private List<DmCorpIndication> cachedDmCorpIndicationList;
	
	private Map<String, DmCorpIndication> dmCorpIndicationMap = new HashMap<String, DmCorpIndication>();
	
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
			cachedDmCorpIndicationList = null;
			dmCorpIndicationMap.clear();
		}		
	}

	public String getDmCorpIndicationDesc(String corpIndicationCode) {		
		DmCorpIndication dmCorpIndication = this.getDmCorpIndication(corpIndicationCode);
		if (dmCorpIndication != null) {
			return dmCorpIndication.getCorpIndicationDesc();
		} else {
			return "";
		}
	}
	
	public DmCorpIndication getDmCorpIndication(String corpIndicationCode) {
		this.getDmCorpIndicationList();
		
		return dmCorpIndicationMap.get(corpIndicationCode);		
	}

	private List<DmCorpIndication> getDmCorpIndicationList() {
		synchronized (this)
		{
			List<DmCorpIndication> dmCorpIndicationList = (List<DmCorpIndication>) this.getCacher().getAll();

			// check whether the Full DmCorpIndicationList expire
			if (cachedDmCorpIndicationList == null || (cachedDmCorpIndicationList != dmCorpIndicationList)) 
			{
				// hold the reference
				cachedDmCorpIndicationList = dmCorpIndicationList;
				
				this.buildDmCorpIndicationKeyMap(cachedDmCorpIndicationList);
			}			

			return cachedDmCorpIndicationList;
		}
	}
	
	private void buildDmCorpIndicationKeyMap(List<DmCorpIndication> dmCorpIndicationList)
	{
		dmCorpIndicationMap.clear();
		
		for (DmCorpIndication dmCorpIndication : dmCorpIndicationList) {
			dmCorpIndicationMap.put(dmCorpIndication.getCorpIndicationCode(), dmCorpIndication);
		}
		
	}
	
	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmCorpIndication> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmCorpIndication create(String corpIndicationCode) {
			this.getAll();
			return this.internalGet(corpIndicationCode);
		}

		@Override
		public Collection<DmCorpIndication> createAll() {
			return dmsPmsServiceProxy.retrieveDmCorpIndicationList();
		}

		@Override
		public String retrieveKey(DmCorpIndication dmCorpIndication) {
			return dmCorpIndication.getCorpIndicationCode();
		}
	}
	
	public static DmCorpIndicationCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmCorpIndicationCacherInf) Component.getInstance("dmCorpIndicationCacher", ScopeType.APPLICATION);
    }
}
