package hk.org.ha.model.pms.corp.persistence.report;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "FM_RPT_STAT")
public class FmRptStat extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fmRptStatSeq")
	@SequenceGenerator(name = "fmRptStatSeq", sequenceName = "SQ_FM_RPT_STAT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "YEAR", nullable = false)
	private Integer year;

	@Column(name = "MONTH", nullable = false)
	private Integer month;
	
	@Column(name = "FISCAL_YEAR", nullable = false)
	private Integer fiscalYear;

	@Column(name = "EIS_SPEC_CODE", length = 4)
	private String eisSpecCode;

	@Column(name = "PAT_COUNT", nullable = false)
	private Integer patCount;

	@Column(name = "PAT_SFI_COUNT", nullable = false)
	private Integer patSfiCount;

	@Column(name = "ORDER_COUNT", nullable = false)
	private Integer orderCount;
	
	@Column(name = "ORDER_SFI_COUNT", nullable = false)
	private Integer orderSfiCount;

	@Column(name = "ORDER_ITEM_COUNT", nullable = false)
	private Integer orderItemCount;

	@Column(name = "ISSUE_ORDER_COUNT", nullable = false)
	private Integer issueOrderCount;

	@Column(name = "GENERAL_DRUG_COUNT", nullable = false)
	private Integer generalDrugCount;

	@Column(name = "S_DRUG_COUNT", nullable = false)
	private Integer sDrugCount;

	@Column(name = "S_DRUG_EXIST_PAT_COUNT", nullable = false)
	private Integer sDrugExistPatCount;

	@Column(name = "S_DRUG_OTHER_COUNT", nullable = false)
	private Integer sDrugOtherCount;

	@Column(name = "S_DRUG_INDICATED_COUNT", nullable = false)
	private Integer sDrugIndicatedCount;

	@Column(name = "S_DRUG_NON_FORMULARY_COUNT", nullable = false)
	private Integer sDrugNonFormularyCount;

	@Column(name = "SFI_COUNT", nullable = false)
	private Integer sfiCount;

	@Column(name = "UNREGISTERED_DRUG_COUNT", nullable = false)
	private Integer unregisteredDrugCount;

	@Column(name = "SFI_SAFETY_NET_COUNT", nullable = false)
	private Integer sfiSafetyNetCount;
		
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;
	
	@Column(name = "CLUSTER_CODE", nullable = false, length = 3)
	private String clusterCode;
		
	@Transient
	private BigDecimal percentageOfGeneralDrug;
	
	@Transient
	private BigDecimal percentageOfSDrug;
	
	@Transient
	private BigDecimal percentageOfSDrugExistPat;
	
	@Transient
	private BigDecimal percentageOfSDrugOther;
	
	@Transient
	private BigDecimal percentageOfSDrugIndicated;
	
	@Transient
	private BigDecimal percentageOfSDrugNonFormulary;
	
	@Transient
	private BigDecimal percentageOfSfi;
	
	@Transient
	private BigDecimal percentageOfUnregisteredDrug;
	
	@Transient
	private BigDecimal percentageOfSfiSafetyNet;
		
	@Transient
	private BigDecimal percentageOfPatSfi;
	
	@Transient
	private BigDecimal percentageOfOrderSfi;
		
	public FmRptStat() {
		
	}
	
	public FmRptStat(
			Integer year, Integer month, String patHospCode, String clusterCode,
			Long patCount, Long patSfiCount, Long orderCount, Long orderSfiCount, Long orderItemCount, 
			Long issueOrderCount, Long generalDrugCount, Long sDrugCount, Long sDrugExistPatCount, Long sDrugOtherCount, 
			Long sDrugIndicatedCount, Long sDrugNonFormularyCount, Long sfiCount, Long unregisteredDrugCount, Long sfiSafetyNetCount) {
		this.year = year;
		this.month = month;
		this.patHospCode = patHospCode;
		this.clusterCode = clusterCode;
		this.patCount = patCount.intValue();
		this.patSfiCount = patSfiCount.intValue();
		this.orderCount = orderCount.intValue();
		this.orderSfiCount = orderSfiCount.intValue();
		this.orderItemCount = orderItemCount.intValue();
		this.issueOrderCount = issueOrderCount.intValue();
		this.generalDrugCount = generalDrugCount.intValue();
		this.sDrugCount = sDrugCount.intValue();
		this.sDrugExistPatCount = sDrugExistPatCount.intValue();
		this.sDrugOtherCount = sDrugOtherCount.intValue();
		this.sDrugIndicatedCount = sDrugIndicatedCount.intValue();
		this.sDrugNonFormularyCount = sDrugNonFormularyCount.intValue();
		this.sfiCount = sfiCount.intValue();
		this.unregisteredDrugCount = unregisteredDrugCount.intValue();
		this.sfiSafetyNetCount = sfiSafetyNetCount.intValue();
	}
	
	public FmRptStat(
			Integer year, Integer month, String patHospCode, String clusterCode, String eisSpecCode, 
			Long patCount, Long patSfiCount, Long orderCount, Long orderSfiCount, Long orderItemCount, 
			Long issueOrderCount, Long generalDrugCount, Long sDrugCount, Long sDrugExistPatCount, Long sDrugOtherCount, 
			Long sDrugIndicatedCount, Long sDrugNonFormularyCount, Long sfiCount, Long unregisteredDrugCount, Long sfiSafetyNetCount) {
		this.year = year;
		this.month = month;
		this.patHospCode = patHospCode;
		this.clusterCode = clusterCode;
		this.eisSpecCode = eisSpecCode;
		this.patCount = patCount.intValue();
		this.patSfiCount = patSfiCount.intValue();
		this.orderCount = orderCount.intValue();
		this.orderSfiCount = orderSfiCount.intValue();
		this.orderItemCount = orderItemCount.intValue();
		this.issueOrderCount = issueOrderCount.intValue();
		this.generalDrugCount = generalDrugCount.intValue();
		this.sDrugCount = sDrugCount.intValue();
		this.sDrugExistPatCount = sDrugExistPatCount.intValue();
		this.sDrugOtherCount = sDrugOtherCount.intValue();
		this.sDrugIndicatedCount = sDrugIndicatedCount.intValue();
		this.sDrugNonFormularyCount = sDrugNonFormularyCount.intValue();
		this.sfiCount = sfiCount.intValue();
		this.unregisteredDrugCount = unregisteredDrugCount.intValue();
		this.sfiSafetyNetCount = sfiSafetyNetCount.intValue();
	}
	
	public FmRptStat(
			String clusterCode, String patHospCode, Integer fiscalYear,
			Long patCount, Long patSfiCount, Long orderCount, Long orderSfiCount, Long orderItemCount, 
			Long issueOrderCount, Long generalDrugCount, Long sDrugCount, Long sDrugExistPatCount, Long sDrugOtherCount, 
			Long sDrugIndicatedCount, Long sDrugNonFormularyCount, Long sfiCount, Long unregisteredDrugCount, Long sfiSafetyNetCount) {
		this.clusterCode = clusterCode;
		this.patHospCode = patHospCode;
		this.fiscalYear = fiscalYear;
		this.patCount = patCount.intValue();
		this.patSfiCount = patSfiCount.intValue();
		this.orderCount = orderCount.intValue();
		this.orderSfiCount = orderSfiCount.intValue();
		this.orderItemCount = orderItemCount.intValue();
		this.issueOrderCount = issueOrderCount.intValue();
		this.generalDrugCount = generalDrugCount.intValue();
		this.sDrugCount = sDrugCount.intValue();
		this.sDrugExistPatCount = sDrugExistPatCount.intValue();
		this.sDrugOtherCount = sDrugOtherCount.intValue();
		this.sDrugIndicatedCount = sDrugIndicatedCount.intValue();
		this.sDrugNonFormularyCount = sDrugNonFormularyCount.intValue();
		this.sfiCount = sfiCount.intValue();
		this.unregisteredDrugCount = unregisteredDrugCount.intValue();
		this.sfiSafetyNetCount = sfiSafetyNetCount.intValue();
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getEisSpecCode() {
		return eisSpecCode;
	}

	public void setEisSpecCode(String eisSpecCode) {
		this.eisSpecCode = eisSpecCode;
	}

	public Integer getPatCount() {
		return patCount;
	}

	public void setPatCount(Integer patCount) {
		this.patCount = patCount;
	}

	public Integer getPatSfiCount() {
		return patSfiCount;
	}

	public void setPatSfiCount(Integer patSfiCount) {
		this.patSfiCount = patSfiCount;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public Integer getOrderSfiCount() {
		return orderSfiCount;
	}

	public void setOrderSfiCount(Integer orderSfiCount) {
		this.orderSfiCount = orderSfiCount;
	}

	public Integer getOrderItemCount() {
		return orderItemCount;
	}

	public void setOrderItemCount(Integer orderItemCount) {
		this.orderItemCount = orderItemCount;
	}

	public Integer getIssueOrderCount() {
		return issueOrderCount;
	}

	public void setIssueOrderCount(Integer issueOrderCount) {
		this.issueOrderCount = issueOrderCount;
	}

	public Integer getGeneralDrugCount() {
		return generalDrugCount;
	}

	public void setGeneralDrugCount(Integer generalDrugCount) {
		this.generalDrugCount = generalDrugCount;
	}

	public Integer getsDrugCount() {
		return sDrugCount;
	}

	public void setsDrugCount(Integer sDrugCount) {
		this.sDrugCount = sDrugCount;
	}

	public Integer getsDrugExistPatCount() {
		return sDrugExistPatCount;
	}

	public void setsDrugExistPatCount(Integer sDrugExistPatCount) {
		this.sDrugExistPatCount = sDrugExistPatCount;
	}

	public Integer getsDrugOtherCount() {
		return sDrugOtherCount;
	}

	public void setsDrugOtherCount(Integer sDrugOtherCount) {
		this.sDrugOtherCount = sDrugOtherCount;
	}

	public Integer getsDrugIndicatedCount() {
		return sDrugIndicatedCount;
	}

	public void setsDrugIndicatedCount(Integer sDrugIndicatedCount) {
		this.sDrugIndicatedCount = sDrugIndicatedCount;
	}

	public Integer getsDrugNonFormularyCount() {
		return sDrugNonFormularyCount;
	}

	public void setsDrugNonFormularyCount(Integer sDrugNonFormularyCount) {
		this.sDrugNonFormularyCount = sDrugNonFormularyCount;
	}

	public Integer getSfiCount() {
		return sfiCount;
	}

	public void setSfiCount(Integer sfiCount) {
		this.sfiCount = sfiCount;
	}

	public Integer getUnregisteredDrugCount() {
		return unregisteredDrugCount;
	}

	public void setUnregisteredDrugCount(Integer unregisteredDrugCount) {
		this.unregisteredDrugCount = unregisteredDrugCount;
	}

	public Integer getSfiSafetyNetCount() {
		return sfiSafetyNetCount;
	}

	public void setSfiSafetyNetCount(Integer sfiSafetyNetCount) {
		this.sfiSafetyNetCount = sfiSafetyNetCount;
	}

	public Integer getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	public void setPercentageOfGeneralDrug(BigDecimal percentageOfGeneralDrug) {
		this.percentageOfGeneralDrug = percentageOfGeneralDrug;
	}

	public BigDecimal getPercentageOfGeneralDrug() {
		return percentageOfGeneralDrug;
	}

	public BigDecimal getPercentageOfSDrug() {
		return percentageOfSDrug;
	}

	public void setPercentageOfSDrug(BigDecimal percentageOfSDrug) {
		this.percentageOfSDrug = percentageOfSDrug;
	}

	public BigDecimal getPercentageOfSDrugExistPat() {
		return percentageOfSDrugExistPat;
	}

	public void setPercentageOfSDrugExistPat(BigDecimal percentageOfSDrugExistPat) {
		this.percentageOfSDrugExistPat = percentageOfSDrugExistPat;
	}

	public BigDecimal getPercentageOfSDrugOther() {
		return percentageOfSDrugOther;
	}

	public void setPercentageOfSDrugOther(BigDecimal percentageOfSDrugOther) {
		this.percentageOfSDrugOther = percentageOfSDrugOther;
	}

	public BigDecimal getPercentageOfSDrugIndicated() {
		return percentageOfSDrugIndicated;
	}

	public void setPercentageOfSDrugIndicated(BigDecimal percentageOfSDrugIndicated) {
		this.percentageOfSDrugIndicated = percentageOfSDrugIndicated;
	}

	public BigDecimal getPercentageOfSDrugNonFormulary() {
		return percentageOfSDrugNonFormulary;
	}

	public void setPercentageOfSDrugNonFormulary(
			BigDecimal percentageOfSDrugNonFormulary) {
		this.percentageOfSDrugNonFormulary = percentageOfSDrugNonFormulary;
	}

	public BigDecimal getPercentageOfSfi() {
		return percentageOfSfi;
	}

	public void setPercentageOfSfi(BigDecimal percentageOfSfi) {
		this.percentageOfSfi = percentageOfSfi;
	}

	public BigDecimal getPercentageOfUnregisteredDrug() {
		return percentageOfUnregisteredDrug;
	}

	public void setPercentageOfUnregisteredDrug(BigDecimal percentageOfUnregisteredDrug) {
		this.percentageOfUnregisteredDrug = percentageOfUnregisteredDrug;
	}

	public BigDecimal getPercentageOfSfiSafetyNet() {
		return percentageOfSfiSafetyNet;
	}

	public void setPercentageOfSfiSafetyNet(BigDecimal percentageOfSfiSafetyNet) {
		this.percentageOfSfiSafetyNet = percentageOfSfiSafetyNet;
	}

	public BigDecimal getPercentageOfPatSfi() {
		return percentageOfPatSfi;
	}

	public void setPercentageOfPatSfi(BigDecimal percentageOfPatSfi) {
		this.percentageOfPatSfi = percentageOfPatSfi;
	}

	public BigDecimal getPercentageOfOrderSfi() {
		return percentageOfOrderSfi;
	}

	public void setPercentageOfOrderSfi(BigDecimal percentageOfOrderSfi) {
		this.percentageOfOrderSfi = percentageOfOrderSfi;
	}

	public String getYearMonth() {
		if (month < 10) {
			return this.getYear().toString() + "0" + this.getMonth().toString();
		}		
			
		return this.getYear().toString() + this.getMonth().toString();
	}
	
	public String getDisplayFiscalYear() {
		return this.getFiscalYear().toString().substring(2) + String.valueOf(this.getFiscalYear()+1).substring(2);
	}
	
}
