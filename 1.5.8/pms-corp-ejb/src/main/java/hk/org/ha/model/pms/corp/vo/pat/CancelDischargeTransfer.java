package hk.org.ha.model.pms.corp.vo.pat;

import java.io.Serializable;
import java.util.Date;

public class CancelDischargeTransfer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String caseNum;
	private String transactionType;
	private Date dischargeDate;
	private Date transferDate;	
	private Date systemDate;
	private String hospCode;
	private String pasWardCode;
	private String prevPasWardCode;
	
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	public Date getDischargeDate() {
		return dischargeDate;
	}
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}
	public Date getTransferDate() {
		return transferDate;
	}
	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	public Date getSystemDate() {
		return systemDate;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public String getHospCode() {
		return hospCode;
	}
	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}
	public String getPasWardCode() {
		return pasWardCode;
	}
	public void setPrevPasWardCode(String prevPasWardCode) {
		this.prevPasWardCode = prevPasWardCode;
	}
	public String getPrevPasWardCode() {
		return prevPasWardCode;
	}
}
