package hk.org.ha.model.pms.corp.prop;

import org.joda.time.DateTime;

public class Prop {
	public static final PropHolder<String>        ALERT_HLA_DHORDER_OVERRIDE_REASON                           = new PropHolder<String>        ("alert.hla.dhOrder.override.reason");
	public static final PropHolder<Boolean>       BATCH_DAYEND_ENABLED                                        = new PropHolder<Boolean>       ("batch.dayEnd.enabled");
	public static final PropHolder<Integer>       BATCH_DAYEND_ORDER_DURATION                                 = new PropHolder<Integer>       ("batch.dayEnd.order.duration");
	public static final PropHolder<DateTime>      BATCH_PATIENTEPISODE_LASTTRXDATE                            = new PropHolder<DateTime>      ("batch.patientEpisode.lastTrxDate");
	public static final PropHolder<Integer>       BATCH_PATIENTEPISODE_LIMIT                                  = new PropHolder<Integer>       ("batch.patientEpisode.limit");
	public static final PropHolder<Integer>       BATCH_UNKNOWNPATIENT_DURATION                               = new PropHolder<Integer>       ("batch.unknownPatient.duration");
	public static final PropHolder<String>        CDDH_EXTRAINFO_SOURCE                                       = new PropHolder<String>        ("cddh.extraInfo.source");
	public static final PropHolder<Boolean>       CDDH_LEGACY_ENQUIRY_ENABLED                                 = new PropHolder<Boolean>       ("cddh.legacy.enquiry.enabled");
	public static final PropHolder<DateTime>      CDDH_TRXDATE_MIN                                            = new PropHolder<DateTime>      ("cddh.trxDate.min");
	public static final PropHolder<Integer>       CHARGING_SFI_INVOICE_COPIES                                 = new PropHolder<Integer>       ("charging.sfi.invoice.copies");
	public static final PropHolder<String>        CHARGING_SFI_INVOICE_INVOICENUM_PREFIX                      = new PropHolder<String>        ("charging.sfi.invoice.invoiceNum.prefix");
	public static final PropHolder<String>        CHARGING_SFI_INVOICE_REMARKCHI                              = new PropHolder<String>        ("charging.sfi.invoice.remarkChi");
	public static final PropHolder<String>        CHARGING_SFI_INVOICE_REMARKENG                              = new PropHolder<String>        ("charging.sfi.invoice.remarkEng");
	public static final PropHolder<Boolean>       CHARGING_SFI_IPMARKUP_ENABLED                               = new PropHolder<Boolean>       ("charging.sfi.ipMarkup.enabled");
	public static final PropHolder<Integer>       CHARGING_SFI_IPMARKUP_MAX                                   = new PropHolder<Integer>       ("charging.sfi.ipMarkup.max");
	public static final PropHolder<Boolean>       CHARGING_SFI_OPMARKUP_ENABLED                               = new PropHolder<Boolean>       ("charging.sfi.opMarkup.enabled");
	public static final PropHolder<Integer>       CHARGING_SFI_OPMARKUP_MAX                                   = new PropHolder<Integer>       ("charging.sfi.opMarkup.max");
	public static final PropHolder<Integer>       CHARGING_STANDARD_CHARGEABLEUNIT                            = new PropHolder<Integer>       ("charging.standard.chargeableUnit");
	public static final PropHolder<Boolean>       CHARGING_STANDARD_REFILL_PARTIALPAYMENT_ENABLED             = new PropHolder<Boolean>       ("charging.standard.refill.partialPayment.enabled");
	public static final PropHolder<String>        HOSPITAL_NAMECHI                                            = new PropHolder<String>        ("hospital.nameChi");
	public static final PropHolder<String>        HOSPITAL_NAMEENG                                            = new PropHolder<String>        ("hospital.nameEng");
	public static final PropHolder<String>        LABEL_DISPLABEL_LANG                                        = new PropHolder<String>        ("label.dispLabel.lang");
	public static final PropHolder<Boolean>       LABEL_DISPLABEL_LARGELAYOUT_DELTACHANGEINDICATOR_VISIBLE    = new PropHolder<Boolean>       ("label.dispLabel.largeLayout.deltaChangeIndicator.visible");
	public static final PropHolder<Integer>       LABEL_DISPLABEL_UPDATEUSER_CHARACTER_MASK                   = new PropHolder<Integer>       ("label.dispLabel.updateUser.character.mask");
	public static final PropHolder<Boolean>       LABEL_DISPLABEL_UPDATEUSER_VISIBLE                          = new PropHolder<Boolean>       ("label.dispLabel.updateUser.visible");
	public static final PropHolder<Boolean>       MEDPROFILE_ENABLED                                          = new PropHolder<Boolean>       ("medProfile.enabled");
	public static final PropHolder<Boolean>       MEDPROFILE_MSGCOUNT_ENABLED                                 = new PropHolder<Boolean>       ("medProfile.msgCount.enabled");
	public static final PropHolder<Integer>       MEDPROFILE_MSGCOUNT_INTERVAL                                = new PropHolder<Integer>       ("medProfile.msgCount.interval");
	public static final PropHolder<Integer>       ORDER_ALLOWUPDATE_DURATION                                  = new PropHolder<Integer>       ("order.allowUpdate.duration");
	public static final PropHolder<Boolean>       REPORT_FM_LEGACY_PERSISTENANCE_ENABLED                      = new PropHolder<Boolean>       ("report.fm.legacy.persistence.enabled");
	public static final PropHolder<Integer>       VETTING_ORDER_RECON_ENDRX_DURATION                          = new PropHolder<Integer>       ("vetting.order.recon.endRx.duration");
	public static final PropHolder<Integer>       VETTING_ORDER_RECON_ENDRX_RETAIN_DURATION                   = new PropHolder<Integer>       ("vetting.order.recon.endRx.retain.duration");
	public static final PropHolder<Integer>       VETTING_ORDER_RECON_STARTDATE_DURATION                      = new PropHolder<Integer>       ("vetting.order.recon.startDate.duration");
	public static final PropHolder<Integer>       VETTING_REGIMEN_PRN_DURATION                                = new PropHolder<Integer>       ("vetting.regimen.prn.duration");
	public static final PropHolder<Integer>       VETTING_REGIMEN_PRN_PERCENT                                 = new PropHolder<Integer>       ("vetting.regimen.prn.percent");
}
