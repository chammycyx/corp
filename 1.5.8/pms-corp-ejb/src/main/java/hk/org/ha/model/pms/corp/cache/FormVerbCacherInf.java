package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.vo.FormVerb;

import java.util.List;

import javax.ejb.Local;

@Local
public interface FormVerbCacherInf extends BaseCacherInf {

	List<FormVerb> getFormVerbList();
	
	List<FormVerb> getFormVerbListByFormCode(String formCode);

	FormVerb getDefaultFormVerbByFormCode(String formCode);
}
