package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.udt.report.FmReportType;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.io.FileUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("fmRptFilesManager")
public class FmRptFilesManager implements FmRptFilesManagerLocal {
	
	@Logger
	private Log logger = null;
	
	@In
	private ApplicationProp applicationProp;

	private DateFormat yearFormat = new SimpleDateFormat("yyyy");
	private DateFormat monthFormat = new SimpleDateFormat("yyyyMM");
	private DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		
	private static final String ALL = "ALL";
	private static final String SLASH = "/";
	
	public File getErrorFile(File errorDir, String fileName, DateTime startDate) {
		StringBuilder finalFileName = new StringBuilder(fileName);
		finalFileName.append(".").append(dateTimeFormat.format(startDate.toDate()));
				
		return new File(errorDir + SLASH + finalFileName);
	}
	
	public File getProcessingDirectory(String jobId, DataDirType dataDirType) {
		return new File(applicationProp.getBatchDataDir() + SLASH + jobId + SLASH + dataDirType.getDataValue());
	}
	
	public String getDirectory(DateTime batchStartDate, String folder, FmReportType fmReportType) {
		switch(fmReportType) {
			case MonthlyData: 
			case MonthlyHospSpecStat: 
			case MonthlyHospStat:
			case YearlyAccumulatedHospStat:
			case MonthlyClusterSpecStat:
			case MonthlyClusterStat:
			case YearlyAccumulatedClusterStat:
				return applicationProp.getFmRptDataSubDir() + SLASH + yearFormat.format(batchStartDate.toDate()) + SLASH + folder;						
			
			case MonthlyCorpSpecStat:
			case MonthlyCorpStat:
			case YearlyAccumulatedCorpStat:
				return applicationProp.getFmRptDataSubDir() + SLASH + yearFormat.format(batchStartDate.toDate());
				
			default:
				throw new RuntimeException("Invalid FmReportType");
		}
	}
	
	public String getReportName(DateTime batchStartDate, String patHospCode, String clusterCode, FmReportType FmReportType) {
		switch(FmReportType) {
			case MonthlyData: 
				return applicationProp.getFmRptMonthlyDetailFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";		
			case MonthlyHospSpecStat: 
				return applicationProp.getFmRptMonthlySummarySpecialtyFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case MonthlyHospStat:
				return applicationProp.getFmRptMonthlySummaryFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case MonthlyClusterSpecStat:
				return applicationProp.getFmRptMonthlySummarySpecialtyFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case MonthlyClusterStat:
				return applicationProp.getFmRptMonthlySummaryFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case MonthlyCorpSpecStat:
				return applicationProp.getFmRptMonthlySummarySpecialtyFile() + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case MonthlyCorpStat:
				return applicationProp.getFmRptMonthlySummaryFile() + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case YearlyAccumulatedHospStat:
				return applicationProp.getFmRptYearlySummaryFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			case YearlyAccumulatedClusterStat:
				return applicationProp.getFmRptYearlySummaryFile() + applicationProp.getFmRptFileDelimiter() + clusterCode + applicationProp.getFmRptFileDelimiter() + patHospCode + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";	
			case YearlyAccumulatedCorpStat:
				return applicationProp.getFmRptYearlySummaryFile() + applicationProp.getFmRptFileDelimiter() + ALL + applicationProp.getFmRptFileDelimiter() + ALL + applicationProp.getFmRptFileDelimiter() + monthFormat.format(batchStartDate.toDate()) + ".xls";
			default:
				throw new RuntimeException("Invalid FmReportType");
		}
	}
	
	public void createDirectory(File directory) {
		boolean created = directory.mkdirs();
		if (logger.isDebugEnabled()) {
			logger.debug("fmRptFilesManager - createDirectory: create path status: " + created);
		}
		
		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new RuntimeException("fmRptFilesManager: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new RuntimeException("fmRptFilesManager: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("fmRptFilesManager - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}
	
	public void moveToDestDirectory(DateTime batchStartDate, String patHospCode, String clusterCode, String jobId, DateTime startTime,
									List<FmReportType> fmReportTypeList) throws Exception {

		File workDir = getProcessingDirectory(jobId, DataDirType.Work);
		if (workDir.exists()) {
			for (FmReportType fmReportType : fmReportTypeList) {
				String targetReportName = getReportName(batchStartDate, patHospCode, clusterCode, fmReportType);
				File destDir;
				
				if (fmReportType==FmReportType.MonthlyClusterSpecStat ||
					fmReportType==FmReportType.MonthlyClusterStat ||
					fmReportType==FmReportType.YearlyAccumulatedClusterStat) {
					destDir = new File(applicationProp.getBatchDataDir() + SLASH + getDirectory(batchStartDate, clusterCode, fmReportType));
					
				} else if (fmReportType==FmReportType.MonthlyCorpSpecStat ||
							fmReportType==FmReportType.MonthlyCorpStat ||
							fmReportType==FmReportType.YearlyAccumulatedCorpStat) {
					destDir = new File(applicationProp.getBatchDataDir() + SLASH + getDirectory(batchStartDate, ALL, fmReportType));
					
				} else {
					destDir = new File(applicationProp.getBatchDataDir() + SLASH + getDirectory(batchStartDate, patHospCode, fmReportType));
				}
			
				if (!destDir.exists()) {
					createDirectory(destDir);
				}
			
				File[] files = workDir.listFiles();
				File[] destFiles = destDir.listFiles();
				
				//rename the old file if same file exists
				for (File file: destFiles) {
					if (file.getName().equals(targetReportName)) {
						StringBuilder finalFileName = new StringBuilder(file.getName());
						finalFileName.append(".").append(dateTimeFormat.format(startTime.toDate()));
						FileUtils.moveFile(file, new File(destDir + SLASH + finalFileName));				
						break;
					}
				}			
				
				for (File file: files) {
					if (file.getName().equals(targetReportName)) {
						FileUtils.moveFile(file, new File(destDir + SLASH + file.getName()));				
						break;
					}
				}
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("no files need to be moved to destination folder");
			}
		}
	}

	public void moveToErrorDirectory(String jobId, DateTime startTime) throws Exception {
		File workDir = getProcessingDirectory(jobId, DataDirType.Work);
		File errorDir = getProcessingDirectory(jobId, DataDirType.Error);
		
		if (workDir.exists()) {
			File[] files = workDir.listFiles();
			if (files != null) {
			
				if (!errorDir.exists()) {
					createDirectory(errorDir);
				}
			
				for (File file: files) {
					if (file.isFile()) {
						FileUtils.moveFile(file, getErrorFile(errorDir, file.getName(), startTime));
					} else if (file.isDirectory()) {
						FileUtils.moveDirectory(file, getErrorFile(errorDir, file.getName(), startTime));
					}		
				}
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("no files need to be moved to error folder");
			}
		}
	}
	
	@Remove
	public void destory() {
		
	}
}
