package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;

public interface DmSupplIndicationCacherInf extends BaseCacherInf {
	String getDmSupplIndicationDesc(String supplIndicationCode);
}
