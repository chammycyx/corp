package hk.org.ha.model.pms.corp.batch.worker.report;

import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.corp.persistence.report.DayEndRptStat;
import hk.org.ha.model.pms.corp.persistence.report.DayEndRptStatPK;
import hk.org.ha.model.pms.corp.persistence.report.MpWorkloadStat;
import hk.org.ha.model.pms.corp.prop.Prop;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRptDtl;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRptHdr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateMidnight;


/**
 * Session Bean implementation class ExportDispTrxBean
 */
@AutoCreate
@Stateless
@Name("dayEndRptManager")
@MeasureCalls
public class DayEndRptManagerBean implements DayEndRptManagerLocal {
	
	private static final int CREATE_DATE_BUFFER = 30;

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@PersistenceContext(unitName="PMSCOR1_PMS_RPT")
	private EntityManager emRpt;

	private String exceptItemList = null;
	
	private final static String D_DAY_END = "D_DAY_END";
	private final static String D_UNCOLLECT_TRX = "D_UNCOLLECT_TRX";
	
	@Override
	public List<DayEndFileExtractRpt> retrieveDayEndFileExtractRptList(String hospCode, String workstoreCode, Date batchDate) {
		Workstore workstore = new Workstore(hospCode, workstoreCode); 
		
		
		DayEndFileExtractRpt dayEndFileExtractRpt = new DayEndFileExtractRpt();
		
		DayEndFileExtractRptHdr dayEndFileExtractRptHdr = new DayEndFileExtractRptHdr();
		dayEndFileExtractRptHdr.setHospCode(hospCode);
		dayEndFileExtractRptHdr.setWorkstoreCode(workstoreCode);
		dayEndFileExtractRptHdr.setHospName(Prop.HOSPITAL_NAMEENG.get(workstore));
		dayEndFileExtractRptHdr.setBatchDate(batchDate);
		
		dayEndFileExtractRpt.setDayEndFileExtractRptHdr(dayEndFileExtractRptHdr);
		
		
		DayEndRptStatPK dayEndRptStatPK = new DayEndRptStatPK(hospCode, workstoreCode, batchDate);				
		DayEndRptStat dayEndRptStat = em.find(DayEndRptStat.class, dayEndRptStatPK);

		if (dayEndRptStat == null) {
			
			if ( !isDayEndJobFinish(workstore, batchDate) ) {
				return new ArrayList<DayEndFileExtractRpt>();
			}
			
			List<DispOrderDayEndStatus> doDayEndStatusList = new ArrayList<DispOrderDayEndStatus>();
			doDayEndStatusList.add(DispOrderDayEndStatus.Completed);
			doDayEndStatusList.add(DispOrderDayEndStatus.Uncollected);
			
			// since DispOrder table is partitioned by createDate, createDate range is applied to SQL to ensure only 1-2 
			// partition is searched for performance tuning
			// (Normally day end includes orders which dispDate is within 90 days from batchDate. Since dispDate = createDate
			//  for both OP and IP order, createDate range is set to 90 days + 30 days buffer)
	        Integer dayEndDuration = BATCH_DAYEND_ORDER_DURATION.get();
			if (dayEndDuration == null) {
				throw new UnsupportedOperationException("corporate property : batch.dayEnd.order.duration is null");
			}
			Date createDateStart = new DateMidnight(batchDate).minusDays(dayEndDuration).minusDays(CREATE_DATE_BUFFER).toDate();
			Date createDateEnd = new DateMidnight(batchDate).toDateTime().plusDays(1).minus(1).toDate();
			
			
			dayEndRptStat = new DayEndRptStat();
			dayEndRptStat.setHospCode(hospCode);
			dayEndRptStat.setWorkstoreCode(workstoreCode);
			dayEndRptStat.setBatchDate(batchDate);
			
			int opItemCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.dispOrder.workstore = :workstore" +
								" AND o.dispOrder.orderType = :orderType" +
								" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
								" AND o.status <> :dispOrderItemStatus" +
								" AND o.pharmOrderItem.itemCode <> 'SAMP01'" +
								" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" + 
								" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setItemCount(opItemCount);
			
			int opPrescCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM DispOrder o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.workstore = :workstore" +
								" AND o.orderType = :orderType" +
								" AND o.dayEndStatus in :doDayEndStatusList" +
								" AND o.dayEndBatchDate = :dayEndBatchDate" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setOrderCount(opPrescCount);
			
			int opDispSampleCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.dispOrder.workstore = :workstore" +
								" AND o.dispOrder.orderType = :orderType" +
								" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
								" AND o.status <> :dispOrderItemStatus" +
								" AND (o.pharmOrderItem.itemCode = 'SAMP01'" +
								" OR o.pharmOrderItem.itemCode like 'S0%')" +
								" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
								" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setSampleItemCount(opDispSampleCount);
	
			int opDispSfiCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.dispOrder.workstore = :workstore" +
								" AND o.dispOrder.orderType = :orderType" +
								" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
								" AND o.status <> :dispOrderItemStatus" +
								" AND o.pharmOrderItem.actionStatus = :actionStatus" +
								" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
								" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
								.setParameter("actionStatus", ActionStatus.PurchaseByPatient)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setSfiItemCount(opDispSfiCount);
			
			int opDispSafetyNetItemCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.dispOrder.workstore = :workstore" +
								" AND o.dispOrder.orderType = :orderType" +
								" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
								" AND o.status <> :dispOrderItemStatus" +
								" AND o.pharmOrderItem.actionStatus = :actionStatus" +
								" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
								" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
								.setParameter("actionStatus", ActionStatus.SafetyNet)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setSafetyNetItemCount(opDispSafetyNetItemCount);
			
			int capdVoucherCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM CapdVoucher o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.dispOrder.workstore = :workstore" +
								" AND o.dispOrder.orderType = :orderType" +
								" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
								" AND o.status <> :capdVoucherStatus" +
								" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
								" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("capdVoucherStatus", CapdVoucherStatus.Deleted)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setCapdVoucherCount(capdVoucherCount);
			
			int capdItemCount = ((Long) emRpt.createQuery(
								"SELECT count(o) FROM CapdVoucherItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
								" WHERE o.capdVoucher.dispOrder.workstore = :workstore" +
								" AND o.capdVoucher.dispOrder.orderType = :orderType" +
								" AND o.capdVoucher.dispOrder.dayEndStatus in :doDayEndStatusList" +
								" AND o.capdVoucher.status <> :capdVoucherStatus" +
								" AND o.capdVoucher.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
								" AND o.capdVoucher.dispOrder.createDate between :createDateStart and :createDateEnd" +
								" AND o.capdVoucher.createDate between :createDateStart and :createDateEnd" +
								" AND o.createDate between :createDateStart and :createDateEnd")
								.setParameter("workstore", workstore)
								.setParameter("orderType", OrderType.OutPatient)
								.setParameter("doDayEndStatusList", doDayEndStatusList)
								.setParameter("capdVoucherStatus", CapdVoucherStatus.Deleted)
								.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
								.setParameter("createDateStart", createDateStart)
								.setParameter("createDateEnd", createDateEnd)
								.getSingleResult()).intValue();
			dayEndRptStat.setCapdVoucherItemCount(capdItemCount);
	
			
			int mpItemCount = ((Long) emRpt.createQuery(
					"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
					" WHERE o.dispOrder.workstore = :workstore" +
					" AND o.dispOrder.orderType = :orderType" +
					" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
					" AND o.status <> :dispOrderItemStatus" +
					" AND o.pharmOrderItem.itemCode not in :exceptItemList" +
					" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
					" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
					" AND o.createDate between :createDateStart and :createDateEnd")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("doDayEndStatusList", doDayEndStatusList)
					// apart from no label item, for IPMOE ward stock or danger drug item with with reDispFlag = N, DOI status 
					// is DispOrderItemStatus.KeepRecord and will be filtered out
					.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
					.setParameter("exceptItemList", Arrays.asList(exceptItemList.split(",")))
					.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
					.setParameter("createDateStart", createDateStart)
					.setParameter("createDateEnd", createDateEnd)
					.getSingleResult()).intValue();
			dayEndRptStat.setMpItemCount(mpItemCount);
	
			int mpDispSampleCount = ((Long) emRpt.createQuery(
					"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
					" WHERE o.dispOrder.workstore = :workstore" +
					" AND o.dispOrder.orderType = :orderType" +
					" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
					" AND o.status <> :dispOrderItemStatus" +
					" AND (o.pharmOrderItem.itemCode = 'SAMP01'" +
					" OR o.pharmOrderItem.itemCode like 'S0%')" +
					" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
					" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
					" AND o.createDate between :createDateStart and :createDateEnd")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("doDayEndStatusList", doDayEndStatusList)
					.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
					.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
					.setParameter("createDateStart", createDateStart)
					.setParameter("createDateEnd", createDateEnd)
					.getSingleResult()).intValue();
			dayEndRptStat.setMpSampleItemCount(mpDispSampleCount);
			
			int mpDispSfiCount = ((Long) emRpt.createQuery(
					"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
					" WHERE o.dispOrder.workstore = :workstore" +
					" AND o.dispOrder.orderType = :orderType" +
					" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
					" AND o.status <> :dispOrderItemStatus" +
					" AND o.pharmOrderItem.itemCode not in :exceptItemList" +
					" AND o.pharmOrderItem.actionStatus = :actionStatus" +
					" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
					" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
					" AND o.createDate between :createDateStart and :createDateEnd")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("doDayEndStatusList", doDayEndStatusList)
					.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
					.setParameter("exceptItemList", Arrays.asList(exceptItemList.split(",")))
					.setParameter("actionStatus", ActionStatus.PurchaseByPatient)
					.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
					.setParameter("createDateStart", createDateStart)
					.setParameter("createDateEnd", createDateEnd)
					.getSingleResult()).intValue();
			dayEndRptStat.setMpSfiItemCount(mpDispSfiCount);
			
			int mpDispSafetyNetItemCount = ((Long) emRpt.createQuery(
					"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
					" WHERE o.dispOrder.workstore = :workstore" +
					" AND o.dispOrder.orderType = :orderType" +
					" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
					" AND o.status <> :dispOrderItemStatus" +
					" AND o.pharmOrderItem.itemCode not in :exceptItemList" +
					" AND o.pharmOrderItem.actionStatus = :actionStatus" +
					" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
					" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
					" AND o.createDate between :createDateStart and :createDateEnd")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("doDayEndStatusList", doDayEndStatusList)
					.setParameter("dispOrderItemStatus", DispOrderItemStatus.KeepRecord)
					.setParameter("exceptItemList", Arrays.asList(exceptItemList.split(",")))
					.setParameter("actionStatus", ActionStatus.SafetyNet)
					.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
					.setParameter("createDateStart", createDateStart)
					.setParameter("createDateEnd", createDateEnd)
					.getSingleResult()).intValue();
			dayEndRptStat.setMpSafetyNetItemCount(mpDispSafetyNetItemCount);
	
			int mpDispWardStockCount = ((Long) emRpt.createQuery(
					"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
					" WHERE o.dispOrder.workstore = :workstore" +
					" AND o.dispOrder.orderType = :orderType" +
					" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
					" AND o.pharmOrderItem.itemCode not in :exceptItemList" +
					" AND o.pharmOrderItem.wardStockFlag = true" +
					" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
					" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
					" AND o.createDate between :createDateStart and :createDateEnd")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("doDayEndStatusList", doDayEndStatusList)
					.setParameter("exceptItemList", Arrays.asList(exceptItemList.split(",")))
					.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
					.setParameter("createDateStart", createDateStart)
					.setParameter("createDateEnd", createDateEnd)
					.getSingleResult()).intValue();
			dayEndRptStat.setMpWardStockItemCount(mpDispWardStockCount);
	
			int mpDispDangerDrugCount = ((Long) emRpt.createQuery(
					"SELECT count(o) FROM DispOrderItem o" + // 20121112 index check : DispOrder.workstore,dayEndBatchDate : I_DISP_ORDER_08
					" WHERE o.dispOrder.workstore = :workstore" +
					" AND o.dispOrder.orderType = :orderType" +
					" AND o.dispOrder.dayEndStatus in :doDayEndStatusList" +
					" AND o.pharmOrderItem.itemCode not in :exceptItemList" +
					" AND o.pharmOrderItem.dangerDrugFlag = true" +
					" AND o.dispOrder.dayEndBatchDate = :dayEndBatchDate" +
					" AND o.dispOrder.createDate between :createDateStart and :createDateEnd" +
					" AND o.createDate between :createDateStart and :createDateEnd")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("doDayEndStatusList", doDayEndStatusList)
					.setParameter("exceptItemList", Arrays.asList(exceptItemList.split(",")))
					.setParameter("dayEndBatchDate", batchDate, TemporalType.DATE)
					.setParameter("createDateStart", createDateStart)
					.setParameter("createDateEnd", createDateEnd)
					.getSingleResult()).intValue();
			dayEndRptStat.setMpDangerDrugItemCount(mpDispDangerDrugCount);
			
			em.persist(dayEndRptStat);
		}

		
		DayEndFileExtractRptDtl dayEndFileExtractRptDtl = new DayEndFileExtractRptDtl();
		
		dayEndFileExtractRptDtl.setOpItemCount(dayEndRptStat.getItemCount());
		dayEndFileExtractRptDtl.setOpPrescCount(dayEndRptStat.getOrderCount());
		dayEndFileExtractRptDtl.setOpDispSampleCount(dayEndRptStat.getSampleItemCount());
		dayEndFileExtractRptDtl.setOpDispSfiCount(dayEndRptStat.getSfiItemCount());
		dayEndFileExtractRptDtl.setOpDispSafetyNetItemCount(dayEndRptStat.getSafetyNetItemCount());
		dayEndFileExtractRptDtl.setCapdVoucherCount(dayEndRptStat.getCapdVoucherCount());
		dayEndFileExtractRptDtl.setCapdItemCount(dayEndRptStat.getCapdVoucherItemCount());
		dayEndFileExtractRptDtl.setMpItemCount(dayEndRptStat.getMpItemCount());
		dayEndFileExtractRptDtl.setMpDispSampleCount(dayEndRptStat.getMpSampleItemCount());
		dayEndFileExtractRptDtl.setMpDispSfiCount(dayEndRptStat.getMpSfiItemCount());
		dayEndFileExtractRptDtl.setMpDispSafetyNetItemCount(dayEndRptStat.getMpSafetyNetItemCount());
		dayEndFileExtractRptDtl.setMpDispWardStockCount(dayEndRptStat.getMpWardStockItemCount());
		dayEndFileExtractRptDtl.setMpDispDangerDrugCount(dayEndRptStat.getMpDangerDrugItemCount());
		dayEndFileExtractRptDtl.setItemCount(dayEndRptStat.getItemCount() + dayEndRptStat.getMpItemCount());
		
		dayEndFileExtractRpt.setDayEndFileExtractRptDtl(dayEndFileExtractRptDtl);
		
		
		List<DayEndFileExtractRpt> dayEndFileExtractRptList = new ArrayList<DayEndFileExtractRpt>();
		dayEndFileExtractRptList.add(dayEndFileExtractRpt);
		
		return dayEndFileExtractRptList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MpWorkloadStat> retrieveMpWorkloadStatRptList(Workstore workstore, Date startDate, Date endDate) {
		return emRpt.createQuery(
				"select o from MpWorkloadStat o" + // 20130520 index check : MpWorkloadStat.workstore,dayEndBatchDate : UI_MP_WORKLOAD_STAT_01
				" where o.workstore = :workstore"+
				" and o.dayEndBatchDate between :startDate and :endDate "+
				" order by o.dayEndBatchDate desc")
				.setParameter("workstore", workstore)
				.setParameter("startDate", startDate, TemporalType.DATE)
				.setParameter("endDate", endDate, TemporalType.DATE)
				.getResultList();
	}	
	
	@SuppressWarnings("unchecked")
	private boolean isDayEndJobFinish(Workstore workstore, Date batchDate) {		
		List<BatchJobState> batchJobStateList = em.createQuery(
				"select o from BatchJobState o" + // 20161129 index check : BatchJobState.jobName,batchDate,hospCode,workstoreCode : UI_BATCH_JOB_STATE_01
				" where o.hospCode = :hospCode" +
				" and o.batchDate = :batchDate" +
				" and ((o.jobName = :dDayEnd" +
				" and o.workstoreCode = :workstoreCode)" +
				" or (o.jobName = :dUncollectTrx))")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("batchDate", batchDate, TemporalType.DATE)
				.setParameter("workstoreCode", workstore.getWorkstoreCode())
				.setParameter("dDayEnd", D_DAY_END)
				.setParameter("dUncollectTrx", D_UNCOLLECT_TRX)
				.getResultList();
		
		return batchJobStateList.size() == 2;
	}
}
