package hk.org.ha.model.pms.corp.batch.worker.dayend;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.io.BufferedWriter;
import java.util.List;

import javax.ejb.Local;

@Local
public interface ExportChargingTrxLocal {
	void process(
			String hospCode,
    		String workstoreCode,
    		List<DispOrderItem> dispOrderItemList,
    		BufferedWriter dctrxWriter,
    		List<String> exceptItemList) throws Exception;
}
