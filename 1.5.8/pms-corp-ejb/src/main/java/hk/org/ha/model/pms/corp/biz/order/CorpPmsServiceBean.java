package hk.org.ha.model.pms.corp.biz.order;

import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_REGIMEN_PRN_DURATION;
import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_REGIMEN_PRN_PERCENT;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.corp.batch.worker.report.DayEndRptManagerLocal;
import hk.org.ha.model.pms.corp.biz.CorpPmsSessionInfoCoordinatorLocal;
import hk.org.ha.model.pms.corp.biz.FmHospitalMappingManagerLocal;
import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhCheckServiceLocal;
import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhServiceLocal;
import hk.org.ha.model.pms.corp.biz.charging.InvoiceManagerLocal;
import hk.org.ha.model.pms.corp.biz.medprofile.MedProfileManagerLocal;
import hk.org.ha.model.pms.corp.biz.onestop.OneStopManagerLocal;
import hk.org.ha.model.pms.corp.biz.support.PropManagerLocal;
import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;
import hk.org.ha.model.pms.corp.persistence.PatientProfile;
import hk.org.ha.model.pms.corp.persistence.charging.PatientSfiProfile;
import hk.org.ha.model.pms.corp.persistence.order.OrderSubscribe;
import hk.org.ha.model.pms.corp.persistence.report.MpWorkloadStat;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.corp.vo.PrnPropertyResult;
import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.corp.vo.UnvetOrderResult;
import hk.org.ha.model.pms.persistence.RemarkEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.DispTrxType;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.checkissue.DispInfo;
import hk.org.ha.model.pms.vo.checkissue.DispItemInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.support.PropInfoItem;
import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;
import hk.org.ha.service.biz.pms.asg.interfaces.AsgCorpServiceJmsRemote;
import hk.org.ha.service.biz.pms.cmm.interfaces.CmmServiceJmsRemote;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.og.interfaces.OgServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.AsaSubscriberJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("corpPmsService")
@MeasureCalls
public class CorpPmsServiceBean implements CorpPmsServiceLocal {

	private final static int PREV_DISP_ORDER_SEARCH_LIMIT = 100;

	private static final String JAXB_CONTEXT_MO = "hk.org.ha.model.pms.persistence.disp";
	
	
	
	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger; 
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
		
	@In
	private OrderManagerLocal orderManager;

	@In
	private OwnershipServiceLocal ownershipService;
	
	@In
	private CorpCddhServiceLocal corpCddhService;
	
	@In
	private CorpCddhCheckServiceLocal corpCddhCheckService;
	
	@In
	private OgServiceJmsRemote ogServiceDispatcher;
	
	@In
	private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	@In
	private AsaSubscriberJmsRemote asaSubscriberProxy;
	
	@In
	private MedProfileManagerLocal medProfileManager;

	@In
	private DayEndRptManagerLocal dayEndRptManager;
	
	@In
	private PropManagerLocal propManager;
	
	@In
	private FcsManagerLocal fcsManager;
	
	@In
	private OrderSubscribeManagerLocal orderSubscribeManager;
	
	@In 
	private CorpPmsSessionInfoCoordinatorLocal corpPmsSessionInfoCoordinator;
	
	@In
	private InvoiceManagerLocal invoiceManager;
	
	@In
	private OneStopManagerLocal oneStopManager;
	
	@In
	private FmHospitalMappingManagerLocal fmHospitalMappingManager;
	
	
	private CmmServiceJmsRemote getCmmServiceProxy() {
		return (CmmServiceJmsRemote) Component.getInstance("cmmServiceProxy", false);
	}	
	
	private AsgCorpServiceJmsRemote getAsgCorpServiceProxy() {
		return (AsgCorpServiceJmsRemote) Component.getInstance("asgServiceProxy", false);
	}
	
	//obsolete after pms-pms verions 3.4.2
	public StartVettingResult startVetting(String orderNum, Long medOrderId, String hospCode) {
		return startVetting(orderNum, medOrderId, hospCode, "");
	}

	//start using in pms-pms verions 3.4.2
	public StartVettingResult startVetting(String orderNum, Long medOrderId, String hospCode, String hkid) {
		if (orderNum == null) {
			return vetUnvetDhOrder(medOrderId, hospCode, hkid);
		}
		else {
			return vetNormalOrder(orderNum, medOrderId, hospCode, hkid);
		}
	}	

	private StartVettingResult vetNormalOrder(String orderNum, Long medOrderId, String hospCode, String hkid) {

		StartVettingResult result = new StartVettingResult();
		
		// request ownership
		if (!ownershipService.requestOwnership(orderNum, hospCode)) {
			MedOrder medOrder = em.find(MedOrder.class, medOrderId);
			result.setSuccessFlag(Boolean.FALSE);
			if (medOrder.isDhOrder()) {
				result.setMessageCode("0899");
				result.setMessageParam(new String[]{ownershipService.retrieveOwnership(orderNum).getOwner()});
			}
			else {
				result.setMessageCode("0129");
			}
			return result;
		}

		// check for update
		MedOrder medOrder = em.find(MedOrder.class, medOrderId);
		if (medOrder.isDhOrder()) {
			result.setUpdateFlag(!medOrder.getDhLatestFlag());
		}
		else {
			if (medOrder.getStatus() == MedOrderStatus.SysDeleted) {
				if (logger.isDebugEnabled()) {
					logger.debug("remoteMedOrder require update, orderNum : #0", orderNum);
				}

				medOrder = orderManager.retrieveMedOrder(orderNum); // retrieve latest version
				medOrder.loadChild();
				medOrder.clearDmInfo(); // clear all DmInfo before returning to PMS
				result.setUpdateFlag(Boolean.TRUE);
				result.setMedOrder(medOrder);
			}
		}

		if ( StringUtils.isNotBlank(hkid) ) {
			result.setPatientSfiProfile(em.find(PatientSfiProfile.class, hkid));
		}
		
		return result;
	}
	
	private StartVettingResult vetUnvetDhOrder(Long medOrderId, String hospCode, String hkid) {
		
		StartVettingResult result = new StartVettingResult();
		
		MedOrder medOrder = em.find(MedOrder.class, medOrderId);
		
		if (medOrder.getDhLatestFlag() && !ownershipService.requestOwnership(medOrder.getMedCase().getCaseNum(), hospCode)) {
			result.setSuccessFlag(Boolean.FALSE);
			result.setMessageCode("0899");
			result.setMessageParam(new String[]{ownershipService.retrieveOwnership(medOrder.getMedCase().getCaseNum()).getOwner()});
			return result;
		}

		medOrder.loadChild();
		medOrder.clearDmInfo(); // clear all DmInfo before returning to PMS
		
		if ( StringUtils.isNotBlank(hkid) ) {
			result.setPatientSfiProfile(em.find(PatientSfiProfile.class, hkid));
		}
		
		result.setMedOrder(medOrder);
		
		return result;
	}
	
	public EndVettingResult endVetting(
			DispOrder dispOrder, 
			Long prevPharmOrderId, 
			String hospCode,
			Boolean remarkFlag, 
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled) {
		return endVetting(dispOrder, prevPharmOrderId, hospCode, remarkFlag, fcsPersistenceEnabled, legacyPersistenceEnabled, false);
	}

	public EndVettingResult endVetting(
			DispOrder dispOrder, 
			Long prevPharmOrderId, 
			String hospCode,
			Boolean remarkFlag, 
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled,
			Boolean dhEnabled) {
		
		dispOrder.getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		dispOrder.getPharmOrder().loadDmInfo();

		EndVettingResult result = new EndVettingResult();
		
		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();
		
		checkDuplicateMedOrder(medOrder.getId(), medOrder.getOrderNum());
	
		// mapping from poi to moi
		Map<Integer, Integer> itemMap = pharmOrder.createMedOrderItemMap();

		if (medOrder.isDhOrder()) {
			if (!ownershipService.requestOwnership(medOrder.getMedCase().getCaseNum(), hospCode)) {
				result.setSuccessFlag(Boolean.FALSE);
				result.setMessageCode("0899");
				result.setMessageParam(new String[]{ownershipService.retrieveOwnership(medOrder.getMedCase().getCaseNum()).getOwner()});
				return result;
			}
			else if (!em.find(MedOrder.class, medOrder.getId()).getDhLatestFlag()) {
				result.setSuccessFlag(Boolean.FALSE);
				result.setMessageCode("0900");
				return result;
			}
		}
		else {
			if (medOrder.getId() != null && !ownershipService.releaseOwnership(medOrder.getOrderNum(), hospCode)) {
				result.setSuccessFlag(Boolean.FALSE);
				result.setMessageCode("0129");
				return result;
			}
		}
		
		if (isTicketUsed(dispOrder)) {
			result.setSuccessFlag(Boolean.FALSE);
			result.setMessageCode("0249");
			result.setMessageParam(new String[] {dispOrder.getTicket().getTicketNum()});
			return result;
		}
		
		// mark delete
		List<DispOrder> delDispOrderList = orderManager.markOrderSysDeleted(pharmOrder, prevPharmOrderId, remarkFlag);
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>();
		DispOrder prevDispOrder = null;
		for (DispOrder delDispOrder : delDispOrderList) {
			if (!delDispOrder.getRefillFlag()) {
				prevDispOrder = delDispOrder;
				break;
			}
		}
		
		Date prevVetDate = null;
		if (prevDispOrder != null) {
			voidInvoiceList.addAll(prevDispOrder.getInvoiceList());
			prevVetDate = prevDispOrder.getPharmOrder().getMedOrder().getVetDate();
		}
		
		this.updateEndVettingDeleteItemRemarkStatus(pharmOrder, prevPharmOrderId, remarkFlag);
		
		// save MedOrder
		medOrder = orderManager.saveMedOrder(medOrder, remarkFlag, hospCode, medOrder.getPatHospCode());
		
		// save PharmOrder
		orderManager.savePharmOrder(medOrder, pharmOrder, itemMap);
		
		// save DispOrder and Invoice
		orderManager.saveDispOrder(pharmOrder, dispOrder, prevDispOrder);
		
		// save CapdVoucher
		orderManager.saveCapdVoucher(dispOrder);

		// update MedOrder
		orderManager.updateMedOrderStatus(medOrder, prevVetDate);
		
		// return result
		result.setPharmOrder(pharmOrder);
		
		// save cddh item info to patient profile
		PatientProfile patientProfile = null;
		for (DispOrder delDispOrder : delDispOrderList) {
			if (delDispOrder.getPrevStatus() == DispOrderStatus.Issued) {	// used in PMS mode
				PatientProfile updatedPatientProfile = corpCddhService.removePatientProfileCddh("endVetting", delDispOrder);
				
				if ( updatedPatientProfile != null && dispOrder.getPharmOrder().getMedOrder().getPatient() != null &&
						updatedPatientProfile.getPatientId().equals(dispOrder.getPharmOrder().getMedOrder().getPatient().getId()) ) {
					// if patient profile of prev disp order match with saving disp order, reuse the managed patient profile in create patient profile CDDH 
					patientProfile = updatedPatientProfile;
					break;
				}
			}
		}
		if (dispOrder.getStatus() == DispOrderStatus.Issued) {	// used in PMS mode
			corpCddhService.createPatientProfileCddh("endVetting", dispOrder, patientProfile);
		}

		// update delta change type for all DOI, and sync delta change type to PMS to update PMS DOI
		// (note: must be after removePatientProfileCddh function, otherwise prev item in patient profile is used for checking)
		// (note: must be before markSfiInvoiceSysDeleted function, because it is possible to call em clear inside the function)
		Map<Long, DeltaChangeType> deltaChangeTypeMap = corpCddhCheckService.updateDeltaChangeTypeForLabel(dispOrder);
		result.setDeltaChangeTypeMap(deltaChangeTypeMap);
		
		//updateVoidInvoiceList Status
		voidInvoiceList = orderManager.markSfiInvoiceSysDeleted(voidInvoiceList, dispOrder.getInvoiceList());

		em.flush();

		// set label desc for EPR CREATE/UPDATE and CDDH create
		dispOrder.getPharmOrder().loadDmInfo();
		InstructionBuilder builder = InstructionBuilder.instance();
		dispOrder = builder.buildLabelDesc(dispOrder);
		
		// clear all DmInfo before sending to OG and DA
		for (DispOrder delDispOrder : delDispOrderList) {
			delDispOrder.getPharmOrder().clearDmInfo();
		}
		dispOrder.getPharmOrder().clearDmInfo();
		
		//TODO : need to merge with saveDispOrder
		if (fcsPersistenceEnabled) {
			fcsManager.saveFcsSfiInvoice(voidInvoiceList, dispOrder.getInvoiceList());
		}
		
		if (legacyPersistenceEnabled) {
			for (DispOrder delDispOrder : delDispOrderList) {
				if (delDispOrder.getPrevStatus() == DispOrderStatus.Issued) { // only issued order has been saved in legacy cddh
					corpCddhService.removeLegacyCddhDispOrder(delDispOrder);
				}
			}
			
			if (dispOrder.getStatus() == DispOrderStatus.Issued) { // use in pms mode
				corpCddhService.createLegacyCddhRecord(dispOrder);
			}
		}

		if (dispOrder.getStatus() == DispOrderStatus.Issued) {	// used in PMS mode
			if (dispOrder.getPrevDispOrderId() == null) {
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispCreate, dispOrder);
			} else {
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispUpdate, dispOrder);
			}
		}
		
		// load cmsExtraXml for sending message to CMS (because it is lazy)
		// (must be loaded after em.flush, otherwise local managed MedOrder will be overwritten by old MedOrder
		//  of corp DB (only affect MedOrder merge case but not persist case, so only affect vet MOE order without remark)
		medOrder.getCmsExtraXml();

		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		
		em.clear();
		
		// send MedOrder to CMM if it is subscribed by CMM
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in endVetting, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
		}
		
		// backup original MOI list
		List<MedOrderItem> orgMedOrderItemList = medOrder.getMedOrderItemList();
		// replace MOI list which contain all item
		medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));
		
		if (medOrder.isDhOrder()) {
			if (dhEnabled && prevDispOrder == null && !dispOrder.getDispOrderItemList().isEmpty()) {
				getAsgCorpServiceProxy().returnfirstVettedStatus(dispOrder);
			}
		}
		else {
			// send MOE OP order to OG 
			OpTrxType opTrxType = null;
			if (medOrder.getDocType() == MedOrderDocType.Normal) {
				opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, false);
			} else {
				if (medOrder.getPrevOrderNum() == null) {
					opTrxType = OpTrxType.OpCreateManualOrder;
				} else {
					opTrxType = OpTrxType.OpModifyManualOrder;
				}
			}
			ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
		}
		
		// restore original MOI list
		medOrder.setMedOrderItemList(orgMedOrderItemList);

		// clear all DmInfo before returning to PMS
		if (result.getPharmOrder() != null) {
			result.getPharmOrder().clearDmInfo();
		}
		
		return result;
	}
	
	public void cancelVetting(
			String orderNum, 
			String hospCode) {
		
		ownershipService.releaseOwnership(orderNum, hospCode);
	}

	public EndVettingResult refillDispensing(
			DispOrder dispOrder, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled,
			List<MedOrderItem> confirmDiscontinueMoiList,
			String hospCodeForOwnerShip) {
		
		return refillDispensing(dispOrder, legacyPersistenceEnabled, fcsPersistenceEnabled, confirmDiscontinueMoiList, hospCodeForOwnerShip, null);		
	}
	
	public EndVettingResult refillDispensing(
			DispOrder dispOrder, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled,
			List<MedOrderItem> confirmDiscontinueMoiList,
			String hospCodeForOwnerShip,
			Map<Long, Boolean> largeLayoutFlagMap) {
		
		dispOrder.getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		dispOrder.getPharmOrder().loadDmInfo();
		
		EndVettingResult result = new EndVettingResult();

		PharmOrder pharmOrder = dispOrder.getPharmOrder();
		MedOrder medOrder = pharmOrder.getMedOrder();

		if( hospCodeForOwnerShip != null && !medOrder.isDhOrder() ){
			if (medOrder.getId() != null && !ownershipService.releaseOwnership(medOrder.getOrderNum(), hospCodeForOwnerShip)) {
				result.setSuccessFlag(Boolean.FALSE);
				result.setMessageCode("0129"); 
				return result;
			}
		}
		
		if (isTicketUsed(dispOrder)) {
			result.setSuccessFlag(Boolean.FALSE);
			result.setMessageCode("0249");
			result.setMessageParam(new String[] {dispOrder.getTicket().getTicketNum()});
			return result;
		}
		
		pharmOrder = em.find(PharmOrder.class, dispOrder.getPharmOrder().getId());
		pharmOrder.loadChild();
		
		DispOrder prevDispOrder = null;
		if (dispOrder.getPrevDispOrderId() != null) {
			prevDispOrder = em.find(DispOrder.class, dispOrder.getPrevDispOrderId());
		}
		
		orderManager.saveDispOrder(pharmOrder, dispOrder, prevDispOrder);

		// save cddh item info to patient profile
		PatientProfile patientProfile = null;
		if (prevDispOrder != null && prevDispOrder.getStatus() == DispOrderStatus.Issued) {
			PatientProfile updatedPatientProfile = corpCddhService.removePatientProfileCddh("refillDispensing", prevDispOrder);
			
			if ( updatedPatientProfile != null && dispOrder.getPharmOrder().getMedOrder().getPatient() != null &&
					updatedPatientProfile.getPatientId().equals(dispOrder.getPharmOrder().getMedOrder().getPatient().getId()) ) {
				// if patient profile of prev disp order match with saving disp order, reuse the managed patient profile in create patient profile CDDH 
				patientProfile = updatedPatientProfile;
			}
		}
		if (dispOrder.getStatus() == DispOrderStatus.Issued) {	// used in PMS mode
			corpCddhService.createPatientProfileCddh("refillDispensing", dispOrder, patientProfile);
		}

		// update delta change type for all DOI, and sync delta change type to PMS to update PMS DOI
		// (note: must be after removePatientProfileCddh function, otherwise prev item in patient profile is used for checking)
		// (note: must be before markSfiInvoiceSysDeleted function, because it is possible to call em clear inside the function)
		Map<Long, DeltaChangeType> deltaChangeTypeMap = corpCddhCheckService.updateDeltaChangeTypeForLabel(dispOrder);
		result.setDeltaChangeTypeMap(deltaChangeTypeMap);
		
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>();
		
		if( prevDispOrder != null ){
			prevDispOrder.loadChild();
			voidInvoiceList = prevDispOrder.markSysDeleted();
		}
		
		voidInvoiceList = orderManager.markSfiInvoiceSysDeleted(voidInvoiceList, dispOrder.getInvoiceList());

		if (largeLayoutFlagMap != null) {
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
				if (largeLayoutFlagMap.containsKey(dispOrderItem.getId())) {
					dispOrderItem.setLargeLayoutFlag(largeLayoutFlagMap.get(dispOrderItem.getId()));
				}
			}			
		}
		
		em.flush();
		
		for( MedOrderItem medOrderItem : confirmDiscontinueMoiList ){
			MedOrderItem corpMoi = em.find(MedOrderItem.class, medOrderItem.getId());
			corpMoi.setDiscontinueStatus(DiscontinueStatus.Confirmed);
		}
		
		// set label desc for EPR CREATE/UPDATE and CDDH create
		dispOrder.getPharmOrder().loadDmInfo();
		InstructionBuilder builder = InstructionBuilder.instance();
		dispOrder = builder.buildLabelDesc(dispOrder);
		
		// clear all DmInfo before sending to OG and DA
		if (prevDispOrder != null) {
			prevDispOrder.getPharmOrder().clearDmInfo();
		}
		dispOrder.getPharmOrder().clearDmInfo();
		
		if (legacyPersistenceEnabled) {
			if (prevDispOrder != null && prevDispOrder.getPrevStatus() == DispOrderStatus.Issued) {
				corpCddhService.removeLegacyCddhDispOrder(prevDispOrder);
			}
			 
			if (dispOrder.getStatus() == DispOrderStatus.Issued) { // use in PMS mode
				corpCddhService.createLegacyCddhRecord(dispOrder);
			}
		}

		if (fcsPersistenceEnabled) {
			fcsManager.saveFcsSfiInvoice(voidInvoiceList, dispOrder.getInvoiceList());
		}
		
		if (dispOrder.getStatus() == DispOrderStatus.Issued) {	// used in PMS mode
			if (dispOrder.getPrevDispOrderId() == null) {
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispCreate, dispOrder);
			} else {
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispUpdate, dispOrder);	// for SFI refill
			}
		}
		
		
		result.setSuccessFlag(Boolean.TRUE);
		
		// clear all DmInfo before returning to PMS
		if (result.getPharmOrder() != null) {
			result.getPharmOrder().clearDmInfo();
		}
		
		return result;
	}
	
	public void cancelDispensing(
			Long dispOrderId, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled) {
		this.cancelDispensing(dispOrderId, legacyPersistenceEnabled, fcsPersistenceEnabled, null);
	}
	
	public void cancelDispensing(
			Long dispOrderId, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled,
			String hospCodeForOwnerShip) {
		
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		dispOrder.loadChild();
		
		if( hospCodeForOwnerShip != null && !dispOrder.getPharmOrder().getMedOrder().isDhOrder() ){
			MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
			ownershipService.releaseOwnership(medOrder.getOrderNum(), hospCodeForOwnerShip);
		}
		
		List<Invoice> voidInvoiceList = dispOrder.markDeleted();
		
		if (dispOrder.getPrevStatus() == DispOrderStatus.Issued) {
			corpCddhService.removePatientProfileCddh("cancelDispensing", dispOrder);
		}
		
		em.flush();
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();
		
		if (legacyPersistenceEnabled) {
			if (dispOrder.getPrevStatus() == DispOrderStatus.Issued) {
				corpCddhService.removeLegacyCddhDispOrder(dispOrder);
			}
		}
		
		if (fcsPersistenceEnabled) {
			fcsManager.voidFcsSfiInvoice(voidInvoiceList);
		}
		
		if (dispOrder.getPrevStatus() == DispOrderStatus.Issued) {	
			ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, dispOrder);
			
		} else {
			// if current order is not issued, search for the first issued disp order in previous order chain to
			// send back EPR delete message 
			// (do nothing if no issued disp order in previous order chain)
			int searchCount = 0;
			
			DispOrder prevDispOrder = null;
			if (dispOrder.getPrevDispOrderId() != null) {
				prevDispOrder = em.find(DispOrder.class, dispOrder.getPrevDispOrderId());
			}
			
			while (prevDispOrder != null && prevDispOrder.getIssueDate() == null && searchCount < PREV_DISP_ORDER_SEARCH_LIMIT) {
				searchCount++;
				
				if (prevDispOrder.getPrevDispOrderId() != null) {
					prevDispOrder = em.find(DispOrder.class, prevDispOrder.getPrevDispOrderId());
				} else {
					prevDispOrder = null;
				}
			}
			
			if (searchCount == PREV_DISP_ORDER_SEARCH_LIMIT) {
				throw new UnsupportedOperationException("Search prev disp order for sending EPR delete fails, disp order num = " + dispOrder.getDispOrderNum());
			}
			
			if (prevDispOrder != null) {
				prevDispOrder.loadChild();
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, prevDispOrder);
			}
		}
	}
	
	/*start using in pms-pms version 3.2.4.4*/
	public void endDispensing(Long dispOrderId, 
								DispInfo dispInfo,
								Boolean fcsPersistenceEnabled,
								Boolean legacyPersistenceEnabled, 
								Invoice sfiInvoice) {
		endDispensing(dispOrderId, null, null, dispInfo, fcsPersistenceEnabled, legacyPersistenceEnabled);
		if ( sfiInvoice != null ) {
			updateFcsSfiInvoiceForceProceedInfo(sfiInvoice, fcsPersistenceEnabled);
		}
	}
	
	/*start using in pms-pms version 3.0.3.2*/
	public void endDispensing(
			Long dispOrderId, 
			DispInfo dispInfo,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled) {
		
		endDispensing(dispOrderId, null, null, dispInfo, fcsPersistenceEnabled, legacyPersistenceEnabled);
	}
	
	@SuppressWarnings("unchecked")
	public void endDispensingForBatchIssue(List<DispInfo> dispInfoList, Boolean fcsPersistenceEnabled, Boolean legacyPersistenceEnabled) {
		List<Long> dispOrderIdList = new ArrayList<Long>();
		Map<Long, DispInfo> dispInfoMap = new HashMap<Long, DispInfo>();
		for ( DispInfo dispInfo : dispInfoList ) {
			dispOrderIdList.add(dispInfo.getDispOrderId());
			dispInfoMap.put(dispInfo.getDispOrderId(), dispInfo);
		}
		
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20150206 index check : DispOrder.id : PK_DISP_ORDER
				" where o.id in :dispOrderIdList")
				.setParameter("dispOrderIdList", dispOrderIdList)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		for ( DispOrder dispOrder : dispOrderList ) {
			if ( dispOrder.getBatchProcessingFlag() ) {
				throw new RuntimeException("endDispensingForBatchIssue: order is processing by dayend");
			}
			
			DispInfo dispInfo = dispInfoMap.get(dispOrder.getId());
			dispOrder.loadChild();
			endDispensing(dispOrder, null, null, dispInfo, fcsPersistenceEnabled, legacyPersistenceEnabled );
		}
	}
	
	private void endDispensing(
			Long dispOrderId,
			String issueUser,
			Date issueDate,
			DispInfo dispInfo,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled) {

		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		dispOrder.loadChild();
		
		endDispensing(dispOrder, issueUser, issueDate, dispInfo, fcsPersistenceEnabled, legacyPersistenceEnabled);
	}
		
	private void endDispensing(
			DispOrder dispOrder,
			String issueUser,
			Date issueDate,
			DispInfo dispInfo,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled) {
		
		dispOrder.setStatus(DispOrderStatus.Issued);
		dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
		
		//>> remove after all pms-pms version using 3.0.3.2
		if (issueDate != null && issueUser != null) {
			dispOrder.setIssueDate(issueDate);
			dispOrder.setIssueUser(issueUser);
		}
		//<< remove after all pms-pms version using 3.0.3.2
		
		if ( dispInfo != null ) {
			dispOrder.setPickDate(dispInfo.getPickDate());
			dispOrder.setAssembleDate(dispInfo.getAssembleDate());
			dispOrder.setCheckDate(dispInfo.getCheckDate());
			dispOrder.setCheckUser(dispInfo.getCheckUser());
			dispOrder.setIssueDate(dispInfo.getIssueDate());
			dispOrder.setIssueUser(dispInfo.getIssueUser());
			
			for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				DispItemInfo dispItemInfo = dispInfo.getDispItemInfoMap().get(dispOrderItem.getId());
				
				if ( dispItemInfo != null ) {
					dispOrderItem.setAssembleDate(dispItemInfo.getAssembleDate());
					dispOrderItem.setAssembleUser(dispItemInfo.getAssembleUser());
					dispOrderItem.setPickDate(dispItemInfo.getPickDate());
					dispOrderItem.setPickUser(dispItemInfo.getPickUser());
					
					dispOrderItem.setLargeLayoutFlag(dispItemInfo.getLargeLayoutFlag());
				}
			}
		}
		
		
		for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
			if (dispOrderItem.getStatus() != DispOrderItemStatus.KeepRecord) {
				dispOrderItem.setStatus(DispOrderItemStatus.Issued);
			}
		}

		// save cddh item info to patient profile
		corpCddhService.createPatientProfileCddh("endDispensing", dispOrder);
		
		em.flush();
		
		// set label desc for EPR CREATE/UPDATE and CDDH create
		dispOrder.getPharmOrder().loadDmInfo();
		InstructionBuilder builder = InstructionBuilder.instance();
		dispOrder = builder.buildLabelDesc(dispOrder);
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();
		
		// legacy CDDH
		if (legacyPersistenceEnabled) {
			corpCddhService.createLegacyCddhRecord(dispOrder);
		}

		if (fcsPersistenceEnabled) {
			fcsManager.updateFcsSfiInvoiceStatus(dispOrder);
		}
		
		if (dispOrder.getPrevDispOrderId() == null) {
			ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispCreate, dispOrder);
		} else {
			ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispUpdate, dispOrder);
		}
	}
	
	public UnvetOrderResult unvetOrder(
			MedOrder medOrder,
			Long prevMedOrderId, 
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled,
			Boolean removeOrderByCms,
			Boolean removeOrderByPms) {

		medOrder.clearDmInfo();		// for version 3.0.8 compatibility
		medOrder.loadDmInfo();

		if ( ! removeOrderByCms) {
			// if delete a vetted MOE order for remark, unvetOrder() is called befoer removeOrder()
			// and check duplicate here
			checkDuplicateMedOrder(prevMedOrderId, medOrder.getOrderNum());
		}		
		
		if (removeOrderByCms) {
			// lock the order by pessimistic lock
			orderManager.lockMedOrder(prevMedOrderId);
		}
		
        MedOrder prevMedOrder = em.find(MedOrder.class, prevMedOrderId);
        orderManager.loadOrderList(prevMedOrder);

        for (PharmOrder prevPharmOrder : prevMedOrder.getPharmOrderList()) {
        	for (DispOrder prevDispOrder : prevPharmOrder.getDispOrderList()) {
    			// must load immediately after getting DO list, otherwise transisent variables of DO which is 
        		// used for sending EPR message may be deleted
        		prevDispOrder.loadChild();
        	}
        }
        
        // check whether there is pharmacy remark delete item in the order before unvet
        // (to determine whether to include item detail in VO2)
		if ( ! removeOrderByCms && ! removeOrderByPms) {
			boolean containPharmRemarkDeleteItem = false;
			for (MedOrderItem prevMedOrderItem : prevMedOrder.getMedOrderItemList()) {
				if (prevMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete) {
					containPharmRemarkDeleteItem = true;
					break;
				}
			}
			medOrder.setIsUnvetPharmRemarkDeleteItem(containPharmRemarkDeleteItem);
		}
        
		// mark delete for previous MedOrder
		List<DispOrder> prevDispOrderList = prevMedOrder.markSysDeleted();
        
		for (DispOrder prevDispOrder : prevDispOrderList) {
			prevDispOrder.markItemDeleted();
		}
		prevMedOrder.setUnvetFlag(Boolean.TRUE);
		
        if (removeOrderByCms) {
        	medOrder.markDeleted();
        	updatePrevItemRemarkForUnvetByCms(prevMedOrder);
        	
        	// em.flush(), em.clear() is called after processing prevMedOrder
        	
        } else {
    		fallbackPrevItemRemarkForUnvetByPms(prevMedOrder);	// must be before updateMedOrderPharmRemarkItemForUnvet
    		
        	// em.flush(), em.clear() is called after processing prevMedOrder 
    		
            fallbackCurrentItemRemarkForUnvetByPms(medOrder);
            
    		// fallback charge info if not remove by CMS (because pharm remark is fallback)
            for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
            	medOrderItem.setChargeFlag(medOrderItem.getCmsConfirmChargeFlag());
            	medOrderItem.setCmsChargeFlag(medOrderItem.getCmsConfirmChargeFlag());
            	medOrderItem.setUnitOfCharge(medOrderItem.getCmsConfirmUnitOfCharge());
            	
				if (medOrderItem.getRxItemType() == RxItemType.Capd) {
					for (int i=0; i<medOrderItem.getCapdRxDrug().getCapdItemList().size(); i++) {
						CapdItem capdItem = medOrderItem.getCapdRxDrug().getCapdItemList().get(i);
						if (capdItem.getCmsConfirmUnitOfCharge() == null) {
							// checking null is for safety only, cmsConfirmUnitOfCharge is mandatory 
							capdItem.setUnitOfCharge(0);
						} else {
							capdItem.setUnitOfCharge(capdItem.getCmsConfirmUnitOfCharge());
						}
					}
				}
            }
        }
        
		// set reference to previous PharmOrder
        if (removeOrderByCms || removeOrderByPms) {
        	medOrder.setRefPharmOrderId(prevDispOrderList.get(0).getPharmOrder().getId());
        }		
        
		// replace Patient by the one in corp MedOrder
		// (because day-end will only update patient info in corp, the patient of cluster MedOrder may be outdated  
		medOrder.setPatient(prevMedOrder.getPatient());

		medOrder.setPrevMedOrderId(prevMedOrder.getId());
		
		// replace CmsExtraXml by the one in corp MedOrder
		// (because CmsExtraXml is lazy and not loaded to cluster MedOrder)
		// (currently prevMedOrder is detached and not yet load with cmsExtraXml, so get the managed prevMedOrder again) 
		MedOrder tempPrevMedOrder = em.find(MedOrder.class, prevMedOrderId);
		medOrder.setCmsExtraXml(tempPrevMedOrder.getCmsExtraXml());
		
		medOrder.clearId();
		orderManager.insertMedOrderByUnvet(medOrder);

		for (DispOrder prevDispOrder : prevDispOrderList) {
			if (prevDispOrder.getPrevStatus() == DispOrderStatus.Issued) {
				corpCddhService.removePatientProfileCddh("unvetOrder", prevDispOrder);
			}
		}
		
		em.flush();
		
		if ( ! removeOrderByCms && ! removeOrderByPms) {
			// load cmsExtraXml for sending message to CMS (because it is lazy)
			// (loaded after em.flush for consistency only. Because in unvet MedOrder is persist instead of merge,
			//  even if CmsExtraXml is loaded before em.flush, local managed MedOrder will not be overwritten)
			medOrder.getCmsExtraXml();
		}

		OrderSubscribe cmmOrderSubscribe = null;
		if ( ! removeOrderByPms) {
			cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		}
		
		em.clear();

		// clear all DmInfo before sending to OG and DA
		for (DispOrder prevDispOrder : prevDispOrderList) {
			prevDispOrder.getPharmOrder().clearDmInfo();
		}
		medOrder.clearDmInfo();
		
		// send EPR order to OG
		for (DispOrder prevDispOrder : prevDispOrderList) {
			if (prevDispOrder.getPrevStatus() == DispOrderStatus.Issued) {
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, prevDispOrder);
				
				// legacy cddh remove
				if (legacyPersistenceEnabled) {
					corpCddhService.removeLegacyCddhDispOrder(prevDispOrder);
				}
				
			} else {
				// if current order is not issued, search for the first issued disp order in previous order chain to
				// send back EPR delete message 
				// (do nothing if no issued disp order in previous order chain)
				int searchCount = 0;
				
				DispOrder tempPrevDispOrder = null;
				if (prevDispOrder.getPrevDispOrderId() != null) {
					tempPrevDispOrder = em.find(DispOrder.class, prevDispOrder.getPrevDispOrderId());
				}
				
				while (tempPrevDispOrder != null && tempPrevDispOrder.getIssueDate() == null && searchCount < PREV_DISP_ORDER_SEARCH_LIMIT) {
					searchCount++;
					
					if (tempPrevDispOrder.getPrevDispOrderId() != null) {
						tempPrevDispOrder = em.find(DispOrder.class, tempPrevDispOrder.getPrevDispOrderId());
					} else {
						tempPrevDispOrder = null;
					}
				}
				
				if (searchCount == PREV_DISP_ORDER_SEARCH_LIMIT) {
					throw new UnsupportedOperationException("Search prev disp order for sending EPR delete fails, disp order num = " + prevDispOrder.getDispOrderNum());
				}
				
				if (tempPrevDispOrder != null) {
					tempPrevDispOrder.loadChild();
					ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, tempPrevDispOrder);
				}
			}
			
			if (fcsPersistenceEnabled) {
				fcsManager.voidFcsSfiInvoice(prevDispOrder.getInvoiceList());
			}
		}
		
		if ( ! removeOrderByPms) {
			// send MedOrder to CMM if it is subscribed by CMM
			// (send if unvet is triggered by unvet button and CMS cancel. If unvet is triggered by delete Rx, CMM message is sent in removeOrder)
			if (cmmOrderSubscribe != null) {
				JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
				String medOrderXml = moJaxbWrapper.marshall(medOrder);
				this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
				
				logger.info("send MedOrder to CMM complete in unvetOrder, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
						medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
			}
		}
		
		if ( ! removeOrderByCms && ! removeOrderByPms) {
			// backup original MOI list
			List<MedOrderItem> orgMedOrderItemList = medOrder.getMedOrderItemList();
			// replace MOI list which contain all item
			medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));
			// set MOE OP order
			DispOrder dispOrder = new DispOrder();
			PharmOrder pharmOrder = new PharmOrder();
			dispOrder.setPharmOrder(pharmOrder);
			pharmOrder.setMedOrder(medOrder);
	
			// send MOE OP order to OG
			OpTrxType opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, false);
			ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
			
			// restore original MOI list
			medOrder.setMedOrderItemList(orgMedOrderItemList);
		}
		
		// clear all DmInfo before returning to PMS
		medOrder.clearDmInfo();

		return new UnvetOrderResult(medOrder);
	}

	public void updateDispOrderAdminStatusAndForceProceed(Long dispOrderId, 
															DispOrderAdminStatus dispOrderAdminStatus,
															Date issueDate,
															Boolean legacyPersistenceEnabled,
															Boolean legacyFcsPersistenceEnabled, 
															Invoice invoice){
		
		updateDispOrderAdminStatus(dispOrderId, dispOrderAdminStatus, issueDate, legacyPersistenceEnabled, legacyFcsPersistenceEnabled, null);
		
		if (invoice != null){
			fcsManager.updateFcsSfiInvoiceForceProceedInfo(invoice, legacyFcsPersistenceEnabled);
		}
	}

	public void updateDispOrderAdminStatus(
			Long dispOrderId, 
			DispOrderAdminStatus dispOrderAdminStatus,
			Date issueDate,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled) {
		
		updateDispOrderAdminStatus(dispOrderId, dispOrderAdminStatus, issueDate, legacyPersistenceEnabled, legacyFcsPersistenceEnabled, null);
	}
	
	public void updateDispOrderAdminStatus(
			Long dispOrderId, 
			DispOrderAdminStatus dispOrderAdminStatus,
			Date issueDate,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled,
			Map<Long, Boolean> largeLayoutFlagMap) {

		
		DispOrder dispOrder = em.find(DispOrder.class, dispOrderId);
		dispOrder.setAdminStatus(dispOrderAdminStatus);
		
		Date prevIssueDate = null;
		if (dispOrderAdminStatus == DispOrderAdminStatus.Normal && dispOrder.getStatus() == DispOrderStatus.Issued) {
			if (issueDate != null) {	// TODO: remove this checking after pms-pms 3.1.4.3 rollout to all cluster, it is for backward compatibility
				prevIssueDate = dispOrder.getIssueDate();
				dispOrder.setIssueDate(issueDate);
			}
		}
		
		if (largeLayoutFlagMap != null) {
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
				if (largeLayoutFlagMap.containsKey(dispOrderItem.getId())) {
					dispOrderItem.setLargeLayoutFlag(largeLayoutFlagMap.get(dispOrderItem.getId()));
				}
			}			
		}

		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			corpCddhService.updatePatientProfileCddhAdminStatus(dispOrder);
		}

		em.flush();
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();
		
		if (legacyPersistenceEnabled && dispOrder.getStatus() == DispOrderStatus.Issued) {
			dispOrder.getPharmOrder().getMedOrder().getPatient();
			corpCddhService.updateLegacyCddhDispStatus(dispOrder, prevIssueDate);
		}

		if( legacyFcsPersistenceEnabled ){
			fcsManager.updateFcsSfiInvoiceStatus(dispOrder);
		}
	}

	public void reversePharmacyRemark(MedOrder clusterMedOrder) {
		clusterMedOrder.clearDmInfo();	// for version 3.0.8 compatibility
		clusterMedOrder.loadDmInfo();

		checkDuplicateMedOrder(clusterMedOrder.getId(), clusterMedOrder.getOrderNum());
		
		MedOrder medOrder = em.find(MedOrder.class, clusterMedOrder.getId());
		medOrder.loadChild();
		medOrder.setRefPharmOrderId(null);
		medOrder.setRemarkStatus(clusterMedOrder.getRemarkStatus());
		medOrder.setRemarkText(null);
		medOrder.setRemarkCreateUser(null);
		medOrder.setRemarkCreateDate(null);
		medOrder.setCmsUpdateDate(clusterMedOrder.getCmsUpdateDate());
		
		em.flush();
		
		// load cmsExtraXml for sending message to CMS (because it is lazy)
		// (must be loaded after em.flush, otherwise local managed MedOrder will be overwritten by old MedOrder
		//  of corp DB)
		medOrder.getCmsExtraXml();
		
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		
		em.clear();

		// clear all DmInfo before sending to OG
		medOrder.clearDmInfo();

		// send MedOrder to CMM if it is subscribed by CMM
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in reversePharmacyRemark, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
		}
		
		// replace MOI list which contain all item
		medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));
		// set MOE OP order
		DispOrder dispOrder = new DispOrder();
		PharmOrder pharmOrder = new PharmOrder();
		dispOrder.setPharmOrder(pharmOrder);
		pharmOrder.setMedOrder(medOrder);

		// send MOE OP order to OG
		OpTrxType opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, true);
		ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
	}

	public void updateAdminStatus(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled) {
		
		updateAdminStatus(clusterDispOrder, dispOrderAdminStatus, pharmOrderAdminStatus, legacyPersistenceEnabled, legacyFcsPersistenceEnabled, null);	
	}
	
	public void updateAdminStatus(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled,
			Map<Long, Boolean> largeLayoutFlagMap) {
		
		clusterDispOrder.getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		clusterDispOrder.getPharmOrder().loadDmInfo();
		
		checkDuplicateMedOrder(clusterDispOrder.getPharmOrder().getMedOrder().getId(), clusterDispOrder.getPharmOrder().getMedOrder().getOrderNum());
		
		DispOrder dispOrder = em.find(DispOrder.class, clusterDispOrder.getId());		
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		dispOrder.loadChild();
        
		dispOrder.setAdminStatus(dispOrderAdminStatus);
		
		Date prevIssueDate = null;
		if (dispOrderAdminStatus == DispOrderAdminStatus.Normal && dispOrder.getStatus() == DispOrderStatus.Issued && 
				dispOrder.getDayEndStatus() == DispOrderDayEndStatus.None && ! dispOrder.getBatchProcessingFlag()) {
			prevIssueDate = dispOrder.getIssueDate();
			dispOrder.setIssueDate(clusterDispOrder.getIssueDate());
		}
		
		if (largeLayoutFlagMap != null) {
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
				if (largeLayoutFlagMap.containsKey(dispOrderItem.getId())) {
					dispOrderItem.setLargeLayoutFlag(largeLayoutFlagMap.get(dispOrderItem.getId()));
				}
			}			
		}
		
		dispOrder.getPharmOrder().setAdminStatus(pharmOrderAdminStatus);
		medOrder.setCmsUpdateDate(clusterDispOrder.getPharmOrder().getMedOrder().getCmsUpdateDate());

		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			corpCddhService.updatePatientProfileCddhAdminStatus(dispOrder);
		}
		
		em.flush();
		
		// load cmsExtraXml for sending message to CMS (because it is lazy)
		// (must be loaded after em.flush, otherwise local managed MedOrder will be overwritten by old MedOrder
		//  of corp DB)
		medOrder.getCmsExtraXml();
		
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		
		em.clear();
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();

		// send MedOrder to CMM if it is subscribed by CMM
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in updateAdminStatus, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
		}
		
		// replace MOI list which contain all item
		medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));

		// send MOE OP order to OG (include allow modify, reverse allow modify, uncollect, reverse uncollect)
		OpTrxType opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, true);
		ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
		
		if (legacyPersistenceEnabled && dispOrder.getStatus() == DispOrderStatus.Issued) {
			dispOrder.getPharmOrder().getMedOrder().getPatient();
			corpCddhService.updateLegacyCddhDispStatus(dispOrder, prevIssueDate);
		}
		
		if(legacyFcsPersistenceEnabled){
			fcsManager.updateFcsSfiInvoiceStatus(dispOrder);
		}
	}

	public void updateAdminStatusForceProceed(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Invoice invoice, 
			Boolean legacyFcsPersistenceEnabled) {
		
		updateAdminStatusForceProceed(clusterDispOrder, dispOrderAdminStatus, pharmOrderAdminStatus, legacyPersistenceEnabled, invoice,  legacyFcsPersistenceEnabled, null);		
	}
	
	public void updateAdminStatusForceProceed(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Invoice invoice, 
			Boolean legacyFcsPersistenceEnabled,
			Map<Long, Boolean> largeLayoutFlagMap) {
		
		clusterDispOrder.getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		clusterDispOrder.getPharmOrder().loadDmInfo();
		
		DispOrder dispOrder = em.find(DispOrder.class, clusterDispOrder.getId());		
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		dispOrder.loadChild();
        
		Boolean isAllowModifyUncollect = (dispOrder.getPharmOrder().getAdminStatus() == PharmOrderAdminStatus.AllowModify || 
				dispOrder.getPharmOrder().getAdminStatus() == PharmOrderAdminStatus.Revoke);

		if (isAllowModifyUncollect) {
			checkDuplicateMedOrder(clusterDispOrder.getPharmOrder().getMedOrder().getId(), clusterDispOrder.getPharmOrder().getMedOrder().getOrderNum());
		}
		
		dispOrder.setAdminStatus(dispOrderAdminStatus);
		
		Date prevIssueDate = null;
		if (dispOrderAdminStatus == DispOrderAdminStatus.Normal && dispOrder.getStatus() == DispOrderStatus.Issued) {
			prevIssueDate = dispOrder.getIssueDate();
			dispOrder.setIssueDate(clusterDispOrder.getIssueDate());
		}
		
		if (largeLayoutFlagMap != null) {
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
				if (largeLayoutFlagMap.containsKey(dispOrderItem.getId())) {
					dispOrderItem.setLargeLayoutFlag(largeLayoutFlagMap.get(dispOrderItem.getId()));
				}
			}			
		}
		
		dispOrder.getPharmOrder().setAdminStatus(pharmOrderAdminStatus);
		medOrder.setCmsUpdateDate(clusterDispOrder.getPharmOrder().getMedOrder().getCmsUpdateDate());

		if (dispOrder.getStatus() == DispOrderStatus.Issued) {
			corpCddhService.updatePatientProfileCddhAdminStatus(dispOrder);
		}
		
		em.flush();
		
		// load cmsExtraXml for sending message to CMS (because it is lazy)
		// (must be loaded after em.flush, otherwise local managed MedOrder will be overwritten by old MedOrder
		//  of corp DB)
		medOrder.getCmsExtraXml();

		OrderSubscribe cmmOrderSubscribe = null;
		if (isAllowModifyUncollect) {
			cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		}
		
		em.clear();
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();
		
		if (isAllowModifyUncollect)
		{
			// send MedOrder to CMM if it is subscribed by CMM
			if (cmmOrderSubscribe != null) {
				JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
				String medOrderXml = moJaxbWrapper.marshall(medOrder);
				this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
				
				logger.info("send MedOrder to CMM complete in updateAdminStatusForceProceed, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
						medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
			}
			
			// replace MOI list which contain all item
			medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));
	
			// send MOE OP order to OG (include allow modify, reverse allow modify, uncollect, reverse uncollect)
			OpTrxType opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, true);
			ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
		}
		
		if (legacyPersistenceEnabled && dispOrder.getStatus() == DispOrderStatus.Issued) {
			dispOrder.getPharmOrder().getMedOrder().getPatient();
			corpCddhService.updateLegacyCddhDispStatus(dispOrder, prevIssueDate);
		}	
		
		if (invoice != null){
			fcsManager.updateFcsSfiInvoiceForceProceedInfo(invoice, legacyFcsPersistenceEnabled);
		}
	}

	public void updateAllowCommentType(
			DispOrder clusterDispOrder, 
			AllowCommentType allowCommentType) {
		
		clusterDispOrder.getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		clusterDispOrder.getPharmOrder().loadDmInfo();
		
		checkDuplicateMedOrder(clusterDispOrder.getPharmOrder().getMedOrder().getId(), clusterDispOrder.getPharmOrder().getMedOrder().getOrderNum());
		
		DispOrder dispOrder = em.find(DispOrder.class, clusterDispOrder.getId());		
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		dispOrder.loadChild();
        
		dispOrder.getPharmOrder().setAllowCommentType(allowCommentType);
		medOrder.setCmsUpdateDate(clusterDispOrder.getPharmOrder().getMedOrder().getCmsUpdateDate());
		//TODO: call fcs - no status change - no need

		em.flush();
		
		// load cmsExtraXml for sending message to CMS (because it is lazy)
		// (must be loaded after em.flush, otherwise local managed MedOrder will be overwritten by old MedOrder
		//  of corp DB)
		medOrder.getCmsExtraXml();
		
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		
		em.clear();
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();
		
		// send MedOrder to CMM if it is subscribed by CMM
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in updateAllowCommentType, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
		}
		
		// replace MOI list which contain all item
		medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));

		// send MOE OP order to OG (include allow comment, reverse allow comment
		OpTrxType opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, true);
		ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
	}

	public void sendMedProfileOrder(MpTrxType mpTrxType, List<MedProfileMoItem> medProfileMoItemList) {
		for (MedProfileMoItem medProfileMoItem : medProfileMoItemList) {
			ogServiceDispatcher.sendMpOrderToCms(mpTrxType, medProfileMoItem, null);
		}
	}

	public void sendMedProfileReplenishment(MpTrxType mpTrxType, List<Replenishment> replenishmentList) {
		for (Replenishment replenishment : replenishmentList) {
			ogServiceDispatcher.sendMpOrderToCms(mpTrxType, null, replenishment);
		}
	}
	
	public void removeOrder(
			DispOrder clusterDispOrder,
			RemarkEntity remarkEntity,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled) {
		
		clusterDispOrder.getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		clusterDispOrder.getPharmOrder().loadDmInfo();
		
		MedOrder clusterMedOrder = clusterDispOrder.getPharmOrder().getMedOrder();
		
		// for delete vetted MOE order remark, duplicate order is checked in unvetOrder()
		checkDuplicateMedOrder(clusterMedOrder.getId(), clusterMedOrder.getOrderNum());		
		
		MedOrder medOrder = null;
		DispOrder dispOrder = null;
		if (clusterMedOrder.getDocType() == MedOrderDocType.Manual) {
			dispOrder = em.find(DispOrder.class, clusterDispOrder.getId());
			dispOrder.loadChild();
			medOrder = dispOrder.getPharmOrder().getMedOrder();
		} else {
			logger.info("removeOrder (pms-corp): medOrderId=#0, orderNum=#1", clusterMedOrder.getId(), clusterMedOrder.getOrderNum());
			
			medOrder = em.find(MedOrder.class, clusterMedOrder.getId());
			medOrder.loadChild();
			dispOrder = new DispOrder();
			PharmOrder pharmOrder = new PharmOrder();
			dispOrder.setPharmOrder(pharmOrder);
			pharmOrder.setMedOrder(medOrder);
		}
		
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			medOrder.markDeleted();
		} else {
			medOrder.setRemarkStatus(clusterMedOrder.getRemarkStatus());
			medOrder.setRemarkText(remarkEntity.getRemarkText());
			medOrder.setRemarkCreateDate(remarkEntity.getRemarkCreateDate());
			medOrder.setRemarkCreateUser(remarkEntity.getRemarkCreateUser());
			medOrder.setRemarkConfirmDate(null);
			medOrder.setRemarkConfirmUser(null);
		}
		
		medOrder.setCmsUpdateDate(clusterMedOrder.getCmsUpdateDate());
		
		medOrder.setIsUnvetPharmRemarkDeleteItem(clusterMedOrder.getIsUnvetPharmRemarkDeleteItem());

		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			if (dispOrder.getPrevStatus() == DispOrderStatus.Issued) { 
				corpCddhService.removePatientProfileCddh("removeOrder", dispOrder);
			}
		}
		
		em.flush();
		
		// load cmsExtraXml for sending message to CMS (because it is lazy)
		// (must be loaded after em.flush, otherwise local managed MedOrder will be overwritten by old MedOrder
		//  of corp DB (only affect MedOrder merge case but not persist case, so only affect delete vetted MOE order)
		medOrder.getCmsExtraXml();
		
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		
		em.clear();		
		
		// clear all DmInfo before sending to OG and DA
		dispOrder.getPharmOrder().clearDmInfo();
		
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			if (fcsPersistenceEnabled) {
				fcsManager.voidFcsSfiInvoice(dispOrder.getInvoiceList());
			}
		}
		
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			if (dispOrder.getPrevStatus() == DispOrderStatus.Issued) {	// disp order status is preserved in clusterDispOrder 
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, dispOrder);
				
			} else {
				// if current order is not issued, search for the first issued disp order in previous order chain to
				// send back EPR delete message 
				// (do nothing if no issued disp order in previous order chain)
				int searchCount = 0;
				
				DispOrder prevDispOrder = null;
				if (dispOrder.getPrevDispOrderId() != null) {
					prevDispOrder = em.find(DispOrder.class, dispOrder.getPrevDispOrderId());
				}
				
				while (prevDispOrder != null && prevDispOrder.getIssueDate() == null && searchCount < PREV_DISP_ORDER_SEARCH_LIMIT) {
					searchCount++;
					
					if (prevDispOrder.getPrevDispOrderId() != null) {
						prevDispOrder = em.find(DispOrder.class, prevDispOrder.getPrevDispOrderId());
					} else {
						prevDispOrder = null;
					}
				}
				
				if (searchCount == PREV_DISP_ORDER_SEARCH_LIMIT) {
					throw new UnsupportedOperationException("Search prev disp order for sending EPR delete fails, disp order num = " + dispOrder.getDispOrderNum());
				}
				
				if (prevDispOrder != null) {
					prevDispOrder.loadChild();
					ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, prevDispOrder);
				}
			}
		}
		
		if (legacyPersistenceEnabled) {
			if (medOrder.getDocType() == MedOrderDocType.Manual) {
				if (dispOrder.getPrevStatus() == DispOrderStatus.Issued) {	// disp order status is preserved in clusterDispOrder 
					corpCddhService.removeLegacyCddhDispOrder(dispOrder);
				}
			}
		}
		
		// send MedOrder to CMM if it is subscribed by CMM
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in removeOrder, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
		}
		
		if (!medOrder.isDhOrder()) {
			// replace MOI list which contain all item
			medOrder.setMedOrderItemList(getMedOrderItemToOg(medOrder));

			OpTrxType opTrxType = null;
			if (medOrder.getDocType() == MedOrderDocType.Normal) {
				opTrxType = checkOpMoeOrderTrxTypeAndIncludeItemFlag(medOrder, false);
			} else {
				opTrxType = OpTrxType.OpDeleteManualOrder;
			}
			
			ogServiceDispatcher.sendOpOrderToCms(opTrxType, dispOrder);
		}
	}
	
	public void updateFcsSfiInvoiceForceProceedInfo(Invoice sfiInvoice, Boolean fcsPersistenceEnabled){
		sfiInvoice.getDispOrder().getPharmOrder().clearDmInfo();	// for version 3.0.8 compatibility
		
		fcsManager.updateFcsSfiInvoiceForceProceedInfo(sfiInvoice, fcsPersistenceEnabled);		
	}
	
	@Deprecated // 1.0.5.x
	public void updateFcsSfiInvoiceForceProceedInfo(Invoice invoice) {
		this.updateFcsSfiInvoiceForceProceedInfo(invoice, true);
	}
	
	private void updateEndVettingDeleteItemRemarkStatus(PharmOrder pharmOrder, Long prevPharmOrderId, Boolean remarkFlag) {
		// note: updated remark item status of old MedOrderItem is NOT synchronized to cluster
		
		MedOrder medOrder = pharmOrder.getMedOrder();
		if (remarkFlag && medOrder.getDocType() == MedOrderDocType.Normal) {
			MedOrder deleteMedOrder = em.find(MedOrder.class, medOrder.getId());

			HashMap<Integer, MedOrderItem> remarkItemStatusUpdatedMoiHm = new HashMap<Integer, MedOrderItem>();	// skip updating remark item status to old version  
			
			HashMap<Integer, MedOrderItem> moiOrgItemNumHm = new HashMap<Integer, MedOrderItem>();
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getStatus() != MedOrderItemStatus.SysDeleted) {
					moiOrgItemNumHm.put(medOrderItem.getOrgItemNum(), medOrderItem);
				}
			}

			for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
				boolean markOldItemDelete = false;
				boolean markPreviousItemDelete = false;
				boolean markPreviousItemOldVersion = false;
				boolean remarkItemStatusUpdated = false;
				
				if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.NoRemark ||
						deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.Confirmed) {
					MedOrderItem medOrderItem = moiOrgItemNumHm.get(deleteMedOrderItem.getOrgItemNum());
					
					if (medOrderItem != null) {
						if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.NoRemark &&
								! deleteMedOrderItem.getItemNum().equals(medOrderItem.getItemNum())) {
							// pharm remark modify/delete and then reverse
							markOldItemDelete = true;		 	
						} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete &&
								! deleteMedOrderItem.getItemNum().equals(medOrderItem.getItemNum())) {
							// pharm remark modify/delete and then reverse and then delete
							markOldItemDelete = true;		 	
						}
					}
					
				} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedAdd) {
					MedOrderItem medOrderItem = moiOrgItemNumHm.get(deleteMedOrderItem.getOrgItemNum());
					
					if (medOrderItem == null) {
						// reverse item add remark
						markOldItemDelete = true;
					} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedAdd &&
								! deleteMedOrderItem.getItemNum().equals(medOrderItem.getItemNum()) ) {
						// if modify pharmacy remark add item, mark previous remarked item to delete
						markOldItemDelete = true;
					}
					
				} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
					MedOrderItem medOrderItem = moiOrgItemNumHm.get(deleteMedOrderItem.getOrgItemNum());

					if (medOrderItem != null) {
						if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.NoRemark) {
							// reverse item modify remark
							// (since a new item is created for original item, mark previous item and previous original item to delete)
							markOldItemDelete = true;			 	
							markPreviousItemDelete = true;		 
						} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify && 
									! deleteMedOrderItem.getItemNum().equals(medOrderItem.getItemNum())) {
							// create modify remark again in current order, mark previous remarked item to delete 
							markOldItemDelete = true;
						} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete) {
							// delete an item with modify remark, or reverse modify and then delete
							markOldItemDelete = true;

							MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, deleteMedOrderItem.getPrevMoItemId());
							if ( ! prevMedOrderItem.getItemNum().equals(medOrderItem.getItemNum()) ) {
								// reverse modify and then delete
								markPreviousItemDelete = true;		 
							} else {
								// delete an item with modify remark
								markPreviousItemOldVersion = true;
							}
						}
					}
					
				} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete) {
					MedOrderItem medOrderItem = moiOrgItemNumHm.get(deleteMedOrderItem.getOrgItemNum());
					
					if (medOrderItem != null) {
						if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.NoRemark) {
							// reverse item delete remark
							markOldItemDelete = true;		 	
						} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete &&
									! deleteMedOrderItem.getItemNum().equals(medOrderItem.getItemNum())) {
							// reverse delete and then delete again
							markOldItemDelete = true;		 	
						}
					}
					
				} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal && 
								deleteMedOrderItem.getStatus() == MedOrderItemStatus.SysDeleted) {
					// skip marking remark item status to old version for confirmed delete remark item 
					// which will be removed from latest version of order in this save
					remarkItemStatusUpdated = true;
				}
				
				if (markOldItemDelete) {
					deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);	// mark the added item to delete
					remarkItemStatusUpdated = true;
				}
				
				if (markPreviousItemDelete) {
					MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, deleteMedOrderItem.getPrevMoItemId());
					prevMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
				}
				
				if (markPreviousItemOldVersion) {
					MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, deleteMedOrderItem.getPrevMoItemId());
					prevMedOrderItem.setRemarkItemStatus(RemarkItemStatus.OldVersion);
				}
				
				if (remarkItemStatusUpdated) {
					remarkItemStatusUpdatedMoiHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
				}
			}			

			HashMap<Long, MedOrderItem> deleteMoiIdHm = new HashMap<Long, MedOrderItem>();
			for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
				if (deleteMedOrderItem.getStatus() != MedOrderItemStatus.SysDeleted) {
					deleteMoiIdHm.put(deleteMedOrderItem.getId(), deleteMedOrderItem);
				}
			}
			
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
					// mark remarkItemStatus of item update original item (must be in deleteMedOrder)
					// (also update for reverse item delete remark and then create item modify remark) 
					MedOrderItem deleteMedOrderItem = deleteMoiIdHm.get(medOrderItem.getPrevMoItemId());
					boolean remarkItemStatusUpdated = false;
					
					if (deleteMedOrderItem != null) {
						deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.UnconfirmedOrginial);
						remarkItemStatusUpdated = true;
					}

					if (remarkItemStatusUpdated) {
						remarkItemStatusUpdatedMoiHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
					}
				}
			}
			
			for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
				if ( ! remarkItemStatusUpdatedMoiHm.containsKey(deleteMedOrderItem.getItemNum())) {
					// if remark item status in previous order item is not updated, mark to old version
					deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.OldVersion);
				}
			}
		}
		
		em.flush();
		em.clear();
	}	

	private void updatePrevItemRemarkForUnvetByCms(MedOrder deleteMedOrder) {
		for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
			deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.OldVersion);
		}		
		
		em.flush();
		em.clear();
	}
	
	private void fallbackPrevItemRemarkForUnvetByPms(MedOrder deleteMedOrder) {
		// note: updated remark item status of old MedOrderItem is NOT synchronized to cluster
		
		HashMap<Integer, MedOrderItem> remarkItemStatusUpdatedMoiHm = new HashMap<Integer, MedOrderItem>();  
		
		for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
			if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedAdd) {
				// mark delete for item of item add remark
				deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
				remarkItemStatusUpdatedMoiHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
				
			} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
				// mark delete for item of item edit remark
				deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
				remarkItemStatusUpdatedMoiHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
				
				// mark old version for previous item of item edit remark
				// (the item is copied to the new unvet order with no remark status)
				MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, deleteMedOrderItem.getPrevMoItemId());
				prevMedOrderItem.setRemarkItemStatus(RemarkItemStatus.OldVersion);
				
			} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.Confirmed &&
						deleteMedOrderItem.getPrevMoItemId() != null) {
				// mark delete for previous items of confirmed item modify remark
				MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, deleteMedOrderItem.getPrevMoItemId());
				
				while ( prevMedOrderItem != null &&
						(prevMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOldOriginal ||
						 prevMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal) ) {
					
					prevMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
					
					if (prevMedOrderItem.getPrevMoItemId() != null) {	
						prevMedOrderItem = em.find(MedOrderItem.class, prevMedOrderItem.getPrevMoItemId());
					} else {
						prevMedOrderItem = null;
					}
				}
				
			} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal &&
						deleteMedOrderItem.getStatus() == MedOrderItemStatus.SysDeleted) {
				// mark delete for item of confirmed item delete remark
				deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
				remarkItemStatusUpdatedMoiHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
			}
		}
		
		for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
			if ( ! remarkItemStatusUpdatedMoiHm.containsKey(deleteMedOrderItem.getItemNum())) {
				// if remark item status in previous order item is not updated, mark to old version
				deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.OldVersion);
			}
		}
		
        em.flush();
        em.clear();
	}
	
	private void fallbackCurrentItemRemarkForUnvetByPms(MedOrder medOrder) {
		// note: updated remark item status of old MedOrderItem is NOT synchronized to cluster
		List<MedOrderItem> pendingRemoveList = new ArrayList<MedOrderItem>(); 
		List<MedOrderItem> pendingAddList = new ArrayList<MedOrderItem>(); 

		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedAdd) {
				// unconfirmed remark item insert
				pendingRemoveList.add(medOrderItem);
				
			} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
				// unconfirmed remark item update
                MedOrderItem prevMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
                prevMedOrderItem.getMedOrderItemAlertList().size();
                em.flush();
                em.clear();

                // TODO: additional code which ensure entity detach is effective, necessary?
				prevMedOrderItem.setId(null);
				em.flush();
				em.clear();
				
				prevMedOrderItem.setStatus(MedOrderItemStatus.Outstanding);
				
				clearMedOrderItemPharmRemark(prevMedOrderItem);
                
				pendingRemoveList.add(medOrderItem);
                pendingAddList.add(prevMedOrderItem);
				
			} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete) {
				clearMedOrderItemPharmRemark(medOrderItem);
				
			} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.Confirmed) {
				// confirmed item add or modify remark
				clearMedOrderItemPharmRemark(medOrderItem);
				
			} else if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal) {
				// confirmed item delete remark  
				// (because MedOrderItem.status already change from SysDelete to Outstanding, cannot be used to check)
				// (if current order item has remarkItemStatus ConfirmedOriginal in current order, the item must be confirm deleted)
				pendingRemoveList.add(medOrderItem);
			}
		}
		
		for (MedOrderItem medOrderItem : pendingRemoveList) {
			medOrder.removeDeletedMedOrderItem(medOrderItem);
		}
		for (MedOrderItem medOrderItem : pendingAddList) {
			medOrder.addMedOrderItem(medOrderItem);
		}
	}
	
	private void clearMedOrderItemPharmRemark(MedOrderItem medOrderItem) {
		medOrderItem.setRemarkItemStatus(RemarkItemStatus.NoRemark);
		
		medOrderItem.setPrevMoItemId(null);
		medOrderItem.setRemarkCreateDate(null);
		medOrderItem.setRemarkCreateUser(null);
		medOrderItem.setRemarkText(null);
		medOrderItem.setRemarkConfirmDate(null);
		medOrderItem.setRemarkConfirmUser(null);
	}
	
	private boolean isTicketUsed(DispOrder dispOrder) {
		
		StringBuilder sb = new StringBuilder(
				"select o from DispOrder o" + // 20120213 index check : DispOrder.ticket : FK_DISP_ORDER_03
				" where o.status <> :dispOrderStatus" +
				" and o.ticket.id = :ticketId");
		
		String orderNum = dispOrder.getPharmOrder().getMedOrder().getOrderNum();
		if (orderNum != null) {
			sb.append(" and o.pharmOrder.medOrder.orderNum <> :orderNum");
		}

		Query query = em.createQuery(sb.toString());
		query.setParameter("dispOrderStatus", DispOrderStatus.SysDeleted);
		query.setParameter("ticketId",        dispOrder.getTicket().getId());
		
		if (orderNum != null) {
			query.setParameter("orderNum", orderNum);
		}
		
		return !query.getResultList().isEmpty();
	}
	
	private void checkDuplicateMedOrder(Long medOrderId, String orderNum) {
		if (medOrderId != null) {
			// ID is null for first save of manual order
			int count = ((Long) em.createQuery(
					"select count(o) from MedOrder o" + // 20150206 index check : MedOrder.orderNum : I_MED_ORDER_01
					" where o.orderNum = :orderNum" +
					" and o.id <> :medOrderId" +
					" and o.status <> :status")
					.setParameter("orderNum", orderNum)
					.setParameter("medOrderId", medOrderId)
					.setParameter("status", MedOrderStatus.SysDeleted)
					.getSingleResult()).intValue();
			
			if (count > 0) {
				throw new UnsupportedOperationException("Duplicate active MedOrder, order num = " + orderNum);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<MedOrderItem> getMedOrderItemToOg(MedOrder medOrder) {
		List<MedOrderItem> medOrderItemList = em.createQuery( 
					"select o from MedOrderItem o" + // 20120213 index check : MedOrder.orderNum : I_MED_ORDER_01
					" where o.medOrder.orderNum = :orderNum" +
					" and o.remarkItemStatus <> :remarkItemStatus" +
					" order by o.itemNum")
					.setParameter("orderNum", medOrder.getOrderNum())
					.setParameter("remarkItemStatus", RemarkItemStatus.OldVersion)
					.getResultList();
		
		for (MedOrderItem medOrderItem : medOrderItemList) {
			medOrderItem.recalTotalDurationInDay();
			for (MedOrderItemAlert medOrderItemAlert : medOrderItem.getMedOrderItemAlertList()) {
				medOrderItemAlert.getMedOrderItem();
			}
		}
		
		em.clear();
		
		// sorting by org item num, item num (group by org item num and sort by item num for shifting remark date of confirmed item
    	Collections.sort(medOrderItemList, new MedOrderItemRemarkComparator());
		
    	// shift remark date of confirmed item 
    	// (because remark date is in previous item in CMS but remark date is in current item in PMS) 
		for (int i=0; i<=medOrderItemList.size()-2; i++) {
			// loop until 2nd last MedOrderItem
			MedOrderItem currentMedOrderItem = medOrderItemList.get(i);
			MedOrderItem nextMedOrderItem = medOrderItemList.get(i+1);
			
			if (currentMedOrderItem.getOrgItemNum().intValue() == nextMedOrderItem.getOrgItemNum().intValue()) {
				switch (currentMedOrderItem.getRemarkItemStatus()) {
					case ConfirmedOldOriginal: 
					case ConfirmedOriginal:
						currentMedOrderItem.setRemarkText(nextMedOrderItem.getRemarkText());
						currentMedOrderItem.setRemarkCreateDate(nextMedOrderItem.getRemarkCreateDate());
						currentMedOrderItem.setRemarkCreateUser(nextMedOrderItem.getRemarkCreateUser());
						currentMedOrderItem.setRemarkConfirmDate(nextMedOrderItem.getRemarkConfirmDate());
						currentMedOrderItem.setRemarkConfirmUser(nextMedOrderItem.getRemarkConfirmUser());
						nextMedOrderItem.setRemarkText(null);
						nextMedOrderItem.setRemarkCreateDate(null);
						nextMedOrderItem.setRemarkCreateUser(null);
						nextMedOrderItem.setRemarkConfirmDate(null);
						nextMedOrderItem.setRemarkConfirmUser(null);
						break;
				}
			}
		}

		// resume original sorting (by item num)
    	Collections.sort(medOrderItemList, new MedOrderItemComparator());
		
		return medOrderItemList;
	}
	
	private OpTrxType checkOpMoeOrderTrxTypeAndIncludeItemFlag(MedOrder medOrder, boolean forceOpVetOrderWithoutRemark) {
		// note: include item flag only applies to OP order. For discharge order, VO6 always exclude item while VO7 always include item  

		if (forceOpVetOrderWithoutRemark) {
			// force to send VO1/VO6 for one-stop action not affecting item (allow modify / uncollect / reverse delete order remark) 
			medOrder.setIncludeItemFlag(Boolean.FALSE);
			return OpTrxType.OpVetOrderWithoutRemark;
		}
		
		boolean orderItemChanged = checkOrderItemChanged(medOrder);
		
		OpTrxType opTrxType;
		if (orderItemChanged || medOrder.getRemarkStatus() != RemarkStatus.None) {
			opTrxType = OpTrxType.OpVetOrderWithRemark;
		} else {
			opTrxType = OpTrxType.OpVetOrderWithoutRemark;
		}
		
		medOrder.setIncludeItemFlag(orderItemChanged);
		
		return opTrxType;
	}
	
	private boolean checkOrderItemChanged(MedOrder medOrder) {
		// send VO2 if unvet MOE order with delete item pharm remark 
		// (for unvet of delete order pharm remark, MedOrder.remarkStatus is not NONE and is handled above)
		if (medOrder.getIsUnvetPharmRemarkDeleteItem() != null && medOrder.getIsUnvetPharmRemarkDeleteItem()) {
			return true; 
		}
		
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			if (medOrderItem.getRemarkItemStatus() != RemarkItemStatus.OldVersion && 
					medOrderItem.getRemarkItemStatus() != RemarkItemStatus.NoRemark) {
				return true;
			}
		}
		
		return false;
	}
	
	public MedCase saveMedCase(MedCase remoteMedCase) {
		return orderManager.saveMedCase(remoteMedCase);
	}

	public Patient savePatient(Patient remotePatient) {
		return orderManager.savePatient(remotePatient);
	}
	
	public void invalidateSession(Workstore workstore) {
		pmsSubscriberProxy.invalidateSession(workstore);
	}	

	public void purgeMsWorkstoreDrug(Workstore workstore) {
		pmsSubscriberProxy.purgeMsWorkstoreDrug(workstore);
		asaSubscriberProxy.purgeMsWorkstoreDrug(workstore);
	}
	
	@SuppressWarnings("unchecked")
	public List<WardStock> retrieveWardStockList(Ward ward) {
		return em.createQuery(
				"select o from WardStock o" + // 20120214 index check : WardStock.ward : FK_WARD_STOCK_01
				" where o.ward = :ward" +
				" and o.status = :status")
				.setParameter("ward", ward)
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}

	public boolean saveDispOrderListWithVoidInvoiceListForIp(List<DispOrder> dispOrderList,
															 Boolean legacyPersistenceEnabled, 
															 Boolean fcsPersistenceEnabled,
															 Boolean directLabelPrintFlag,
															 List<Invoice> tempInvoiceList,
															 List<Invoice> voidInvoiceList){
		return medProfileManager.saveDispOrderListForIp(dispOrderList, legacyPersistenceEnabled, fcsPersistenceEnabled, directLabelPrintFlag, tempInvoiceList, voidInvoiceList);
	}
	
	@Override
	public boolean saveDispOrderListForIp(List<DispOrder> dispOrderList, Boolean legacyPersistenceEnabled) {
		return medProfileManager.saveDispOrderListForIp(dispOrderList, legacyPersistenceEnabled, false, true, null, null);
	}
	
	@SuppressWarnings("unchecked")
	public List<DispOrderItem> retrieveDispOrderItemListByDeliveryItemId(List<Long> deliveryItemIdList) {
		return em.createQuery(
				"select o from DispOrderItem o" + // 20120214 index check : DispOrderItem.deliveryItemId : I_DISP_ORDER_ITEM_01
				" where o.deliveryItemId in :deliveryItemIdList" +
				" and o.dispOrder.status = :status" +
				" and o.dispOrder.dayEndStatus = :dayEndStatus" +
				" and o.dispOrder.batchProcessingFlag = :batchProcessingFlag")
				.setParameter("deliveryItemIdList", deliveryItemIdList)
				.setParameter("status", DispOrderStatus.Issued)
				.setParameter("dayEndStatus", DispOrderDayEndStatus.None)
				.setParameter("batchProcessingFlag", Boolean.FALSE)
				.getResultList();
	}
	
	public boolean updateDispOrderItemStatusToDeletedByTpnRequestIdList(List<Long> tpnRequestIdList, Boolean legacyPersistenceEnabled){
		return medProfileManager.updateDispOrderItemStatusToDeletedByTpnRequestIdList(tpnRequestIdList, legacyPersistenceEnabled);
	}

	public boolean updateDispOrderItemStatusToDeleted(List<Long> deliveryItemIdList, Boolean legacyPersistenceEnabled){
		return medProfileManager.updateDispOrderItemStatusToDeleted(deliveryItemIdList, legacyPersistenceEnabled);
	}
	
	public List<DayEndFileExtractRpt> retrieveDayEndFileExtractRptList(String hospCode, String workstoreCode, Date batchDate) {
		return dayEndRptManager.retrieveDayEndFileExtractRptList(hospCode, workstoreCode, batchDate);
	}	

	public List<MpWorkloadStat> retrieveMpWorkloadStatRptList(Workstore workstore, Date startDate, Date endDate) {
		return dayEndRptManager.retrieveMpWorkloadStatRptList(workstore, startDate, endDate);
	}
	
	private static class MedOrderItemRemarkComparator implements Comparator<MedOrderItem>, Serializable {
		private static final long serialVersionUID = 1L;

		public int compare(MedOrderItem moi1, MedOrderItem moi2) {
			int compareResult = 0;
			
			compareResult = moi1.getOrgItemNum().compareTo(moi2.getOrgItemNum());
			if (compareResult != 0) {
				return compareResult;
			}
			
			return moi1.getItemNum().compareTo(moi2.getItemNum()); 
		}
	}
	
	private static class MedOrderItemComparator implements Comparator<MedOrderItem>, Serializable {
		private static final long serialVersionUID = 1L;

		public int compare(MedOrderItem moi1, MedOrderItem moi2) {
			return moi1.getItemNum().compareTo(moi2.getItemNum());
		}
	}
	
	public List<CorporateProp> retrieveCorporatePropList() {
		return propManager.retrieveCorporatePropList();
	}
	
	public void updateCorporatePropList(List<PropInfoItem> corporatePropInfoList) {
		if ( !corporatePropInfoList.isEmpty() ) {
			List<CorporateProp> corpPropList = new ArrayList<CorporateProp>();
			for ( PropInfoItem corpPropInfo : corporatePropInfoList ) {
				CorporateProp corpProp = em.find(CorporateProp.class, corpPropInfo.getPropItemId());
				corpProp.setValue(corpPropInfo.getValue());
				corpPropList.add(corpProp);
			}
			propManager.updateCorporatePropList(corpPropList, "#4034");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateCorporatePropByPropMaint(List<CorporateProp> corporatePropList) {
		if ( !corporatePropList.isEmpty() ) {
			List<CorporateProp> updateCorpPropList = new ArrayList<CorporateProp>();
			for ( CorporateProp corporateProp : corporatePropList ) {

				List<CorporateProp> corpCorporatePropList = em.createQuery(
						"select o from CorporateProp o" + // 20120831 index check : CorporateProp.prop : UI_CORPORATE_PROP_01
						" where o.prop = :prop" )
						.setParameter("prop", corporateProp.getProp())
						.getResultList();
				
				CorporateProp corpCorporateProp = null;
				if ( !corpCorporatePropList.isEmpty() ) {
					corpCorporateProp = corpCorporatePropList.get(0);
					corpCorporateProp.setValue(corporateProp.getValue());
					updateCorpPropList.add(corpCorporateProp);
				}
			}
			
			if ( !updateCorpPropList.isEmpty() ) {
				propManager.updateCorporatePropList(updateCorpPropList, "#4029");
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateWorkstoreProp(List<WorkstoreProp> workstorePropList, boolean isOperationModeMaint) {
		if ( workstorePropList.isEmpty() ) {
			return;
		}
		boolean updatePrnDuration = false;
		boolean updatePrnPercent = false;
		
		Set<Workstore> prnMsgWorkstoreSet = new HashSet<Workstore>();
		
		for ( WorkstoreProp workstoreProp : workstorePropList ) {
			
			List<WorkstoreProp> updateWorkstorePropList = em.createQuery(
															"select o from WorkstoreProp o" + // 20120831 index check : WorkstoreProp.workstore,prop : UI_WORKSTORE_PROP_01
															" where o.prop = :prop" +
															" and o.workstore = :workstore")
															.setParameter("prop", workstoreProp.getProp())
															.setParameter("workstore", workstoreProp.getWorkstore())
															.getResultList();
			
			WorkstoreProp updateWorkstoreProp = null;
			if ( !updateWorkstorePropList.isEmpty() ) {
				updateWorkstoreProp = updateWorkstorePropList.get(0);
				updateWorkstoreProp.setValue(workstoreProp.getValue());
				em.merge(updateWorkstoreProp);
				if ( isOperationModeMaint ) {
					auditLogger.log("#4030 Operation Mode Maintenance|Update Workstore Property|HospCode [#0]|WorktoreCode [#1]|PropName [#2]|OrgValue [#3]|NewValue [#4]|", 
										updateWorkstoreProp.getWorkstore().getHospCode(), 
										updateWorkstoreProp.getWorkstore().getWorkstoreCode(), 
										updateWorkstoreProp.getProp().getName(), 
										updateWorkstoreProp.getOrgValue(), 
										updateWorkstoreProp.getValue());
				} else {
					auditLogger.log("#4031 Property Maintenance|Update Workstore Property|HospCode [#0]|WorktoreCode [#1]|PropName [#2]|OrgValue [#3]|NewValue [#4]|", 
										updateWorkstoreProp.getWorkstore().getHospCode(), 
										updateWorkstoreProp.getWorkstore().getWorkstoreCode(), 
										updateWorkstoreProp.getProp().getName(), 
										updateWorkstoreProp.getOrgValue(), 
										updateWorkstoreProp.getValue());
				}
				if ( StringUtils.equals(updateWorkstoreProp.getProp().getName(), VETTING_REGIMEN_PRN_DURATION.getName()) ) {
					updatePrnDuration = true;
					prnMsgWorkstoreSet.add(updateWorkstoreProp.getWorkstore());
				} else if ( StringUtils.equals(updateWorkstoreProp.getProp().getName(), VETTING_REGIMEN_PRN_PERCENT.getName()) ) {
					updatePrnPercent = true;
					prnMsgWorkstoreSet.add(updateWorkstoreProp.getWorkstore());
				}
			}
		}
		em.flush();	
		
		if ( ( updatePrnDuration || updatePrnPercent ) &&  !prnMsgWorkstoreSet.isEmpty() ) {
			List<String> prnPropNameList = new ArrayList<String>();
			prnPropNameList.add(VETTING_REGIMEN_PRN_DURATION.getName());
			prnPropNameList.add(VETTING_REGIMEN_PRN_PERCENT.getName());
			
			for ( Workstore workstore : prnMsgWorkstoreSet ) {
				List<WorkstoreProp> prnWorkstorePropList = em.createQuery(
												"select o from WorkstoreProp o" + // 20120831 index check : WorkstoreProp.workstore,prop : UI_WORKSTORE_PROP_01
												" where o.prop.name in :prnPropNameList" +
												" and o.workstore = :workstore")
												.setParameter("prnPropNameList", prnPropNameList)
												.setParameter("workstore", workstore)
												.getResultList();
				
				if ( !prnWorkstorePropList.isEmpty() ) {
					Workstore targetWorkstore = prnWorkstorePropList.get(0).getWorkstore();
					PrnPropertyResult prnPropertyResult = new PrnPropertyResult();
					prnPropertyResult.setHospCode(targetWorkstore.getHospCode());
					prnPropertyResult.setWorkstoreCode(targetWorkstore.getWorkstoreCode());
					
					for ( WorkstoreProp prnProp : prnWorkstorePropList ) {
						if ( prnProp.getProp().getName().equals(VETTING_REGIMEN_PRN_DURATION.getName()) ) {
							prnPropertyResult.setPrnDuration(Integer.valueOf(prnProp.getValue()));
							if ( updatePrnDuration ) {
								prnPropertyResult.setUpdateUser(prnProp.getUpdateUser());
								prnPropertyResult.setUpdateDate(prnProp.getUpdateDate());
							}
						} else if ( prnProp.getProp().getName().equals(VETTING_REGIMEN_PRN_PERCENT.getName()) ) {
							prnPropertyResult.setPrnPercent(Integer.valueOf(prnProp.getValue()));
							if ( updatePrnPercent ) {
								prnPropertyResult.setUpdateUser(prnProp.getUpdateUser());
								prnPropertyResult.setUpdateDate(prnProp.getUpdateDate());
							}
						}
					}
					ogServiceDispatcher.sendPrnPropertyToCms(OpTrxType.OpUpdatePrnProperty, prnPropertyResult);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateHospitalProp(List<HospitalProp> hospitalPropList) {
		if ( hospitalPropList.isEmpty() ) {
			return;
		}
		
		for ( HospitalProp hospitalProp : hospitalPropList ) {
			List<HospitalProp> updateHospitalPropList = em.createQuery(
					"select o from HospitalProp o" + // 20120831 index check : HospitalProp.hospital,prop : UI_HOSPITAL_PROP_01
					" where o.prop = :prop" +
					" and o.hospital = :hospital")
					.setParameter("prop", hospitalProp.getProp())
					.setParameter("hospital", hospitalProp.getHospital())
					.getResultList();
			
			if ( !updateHospitalPropList.isEmpty() ) {
				HospitalProp updateHospitalProp = updateHospitalPropList.get(0);
				updateHospitalProp.setValue(hospitalProp.getValue());
				em.merge(updateHospitalProp);
				auditLogger.log("#4032 Property Maintenance|Update Hospital Property|HospCode [#0]|PropName [#1]|OrgValue [#2]|NewValue [#3]|", 
									updateHospitalProp.getHospital().getHospCode(), 
									updateHospitalProp.getProp().getName(), 
									updateHospitalProp.getOrgValue(), 
									updateHospitalProp.getValue());
			}
		}
		em.flush();
	}
	
	//uncollect
	@SuppressWarnings("unchecked")
	public void updateUncollectOrderList(List<UncollectOrder> updateUncollectOrderList, Workstore workstore, String uncollectUser) {
		List<Invoice> uncollectInvoiceList = new ArrayList<Invoice>();
		
		for ( UncollectOrder uncollectOrder : updateUncollectOrderList) {
			List<DispOrder> dispOrderList = em.createQuery(
					"select o from DispOrder o" + // 20120306 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
					" where o.ticket.workstore = :workstore" + 
					" and o.ticket.orderType = :ticketOrderType" +
					" and o.ticket.ticketDate = :ticketDate" +
					" and o.ticket.ticketNum = :ticketNum" +
					" and o.dayEndStatus = :dayEndStatus" +
					" and o.adminStatus = :dispOrderAdminStatus" +
					" and o.status not in :status") 
					.setParameter("workstore", workstore)
					.setParameter("ticketOrderType", OrderType.OutPatient)
					.setParameter("ticketDate", uncollectOrder.getTicketDate())
					.setParameter("ticketNum", uncollectOrder.getTicketNum())
					.setParameter("dayEndStatus", DispOrderDayEndStatus.Completed)
					.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
					.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
					.setHint(QueryHints.BATCH, "o.ticket")
					.getResultList();

			if ( !dispOrderList.isEmpty() ) {
				DispOrder dispOrder = dispOrderList.get(0);
				if ( uncollectOrder.isMarkDelete() ) {
					dispOrder.setUncollectFlag(false);
					dispOrder.setUncollectDate(null);
					dispOrder.setUncollectUser(null);
				} else {
					dispOrder.setUncollectFlag(true);
					dispOrder.setUncollectDate(new Date());
					dispOrder.setUncollectUser(uncollectUser);
				}
				
				if( !dispOrder.getInvoiceList().isEmpty() ){
					for(Invoice invoice : dispOrder.getInvoiceList() ){
						if( InvoiceDocType.Sfi == invoice.getDocType() ){
							uncollectInvoiceList.add(invoice);
							break;
						}
					}
				}
			}
		}
		em.flush();
		
		fcsManager.updateFcsSfiInvoiceUncollectInfoList(uncollectInvoiceList);
	}
	
	public void updateCapdVoucherStatusToIssue(List<CapdVoucher> capdVoucherList){
		for ( CapdVoucher capdVoucher : capdVoucherList ) {
			CapdVoucher cv = em.find(CapdVoucher.class, capdVoucher.getId());
			if ( cv != null ) {
				cv.setStatus(CapdVoucherStatus.Issued);
				em.merge(cv);
			}
		}
		em.flush();
	}
	
	public List<PmsSessionInfo> retrievePmsSessionInfoList(String requestId, Workstore workstore) {
		return corpPmsSessionInfoCoordinator.retrievePmsSessionInfoList(requestId, workstore);
	}

	public List<Invoice> retrieveInvoiceByInvoiceNumWoPurchaseRequest(String invoiceNum, String hospCode) {
		return invoiceManager.retrieveInvoiceByInvoiceNumWoPurchaseRequest(invoiceNum, hospCode);
	}

	public List<Invoice> retrieveInvoiceListWoPurchaseRequest(String hkid, String caseNum, String hospCode) {
		return invoiceManager.retrieveInvoiceListWoPurchaseRequest(hkid, caseNum, hospCode);
	}
	
	public void voidInvoiceByInvoiceNum(List<String> invoiceNumList) {
		List<Invoice> invoiceList = medProfileManager.retrieveUpdateVoidInvoiceList(invoiceNumList);
		fcsManager.voidFcsSfiInvoice(invoiceList);
	}
	
	public String retrieveChargeOrderNum(String hospCode, String patHospCode){
		return medProfileManager.retrieveChargeOrderNum(hospCode, patHospCode);
	}
	
	public void updateFcsSfiInvoiceStatus(List<String> invoiceNumList){
		List<Invoice> invoiceList = medProfileManager.retrieveActiveInvoiceByInvoiceNum(invoiceNumList);
		for ( Invoice invoice : invoiceList ) {
			fcsManager.updateFcsSfiInvoiceStatus(invoice.getDispOrder());
		}
	}
	
	public Map<Long, String> retrieveInvoiceNumByDeliveryItemId(List<Long> deliveryItemIdList){
		return invoiceManager.retrieveInvoiceNumByDeliveryItemId(deliveryItemIdList);
	}
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceNum(String invoiceNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){
		return invoiceManager.retrieveSfiInvoiceItemListByInvoiceNum(invoiceNum, includeVoidInvoice, orderType, workstore, patType);
	}
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceManager.retrieveSfiInvoiceItemListByInvoiceDate(invoiceDate, includeVoidInvoice, orderType, workstore, patType);
	}
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByCaseNum(String caseNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceManager.retrieveSfiInvoiceItemListByCaseNum(caseNum, includeVoidInvoice, orderType, workstore, patType);
	}	
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByHkid(String hkid, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceManager.retrieveSfiInvoiceItemListByHkid(hkid, includeVoidInvoice, orderType, workstore, patType);
	}	
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){
		return invoiceManager.retrieveSfiInvoiceItemListByExtactionPeriod(dayRange, includeVoidInvoice, orderType, workstore, patType);
	}
	
	public Invoice retrieveInvoiceByInvoiceId(Long invoiceId){
		return invoiceManager.retrieveInvoiceByInvoiceId(invoiceId);
	}
	
	public void updateInvoicePaymentStatus(List<Long> invoiceIdList, Map<String, FcsPaymentStatus> fcsPaymentStatusMap){
		invoiceManager.updateInvoicePaymentStatus(invoiceIdList, fcsPaymentStatusMap);
	}

	public void reverseUncollectAndEndDispensing(DispOrder clusterDispOrder,
													DispInfo dispInfo,
													Boolean legacyPersistenceEnabled,
													Boolean legacyFcsPersistenceEnabled, 
													Invoice sfiInvoice) {
		updateAdminStatus(clusterDispOrder, DispOrderAdminStatus.Normal, PharmOrderAdminStatus.Normal, legacyPersistenceEnabled, legacyFcsPersistenceEnabled, null);
		endDispensing(clusterDispOrder.getId(), dispInfo, legacyFcsPersistenceEnabled, legacyPersistenceEnabled, sfiInvoice);
	}
	
	public List<OneStopOrderItemSummary> retrieveOrderByCaseNum(String caseNum)
	{
		return oneStopManager.retrieveOrderByCaseNum(caseNum);
	}

	public List<FmHospitalMapping> retrieveFmHospitalMappingList()
	{
		return fmHospitalMappingManager.retrieveFmHospitalMappingList();
	}
	
	public PatientSfiProfile retrievePatientSfiProfile(String hkid) {
		if ( StringUtils.isBlank(hkid) ) {
			return null;
		}
		return em.find(PatientSfiProfile.class, hkid);
	}
}
