package hk.org.ha.model.pms.corp.batch.worker.reftable.phs;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.phs.PhsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Scope(ScopeType.SESSION)
@Name("phsDataBean")
public class PhsDataBean implements PhsDataLocal{

	private Logger logger = null;
		
	@In
	private PhsDataHelper phsDataHelper;
	
	@In
    private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	@In
	private PhsServiceJmsRemote phsServiceProxy;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
    public void initBatch(BatchWorkerInfo info, Logger logger) {
		this.logger = logger;
	}
	
	public void beginChunk(Chunk arg0) throws Exception {
		
	}
	
	public void processRecord(Record arg0) throws Exception {
    	Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);

        List<PatientCat> patientCatListForCorp = phsServiceProxy.retrievePatientCatForPms();
        List<PatientCat> patientCatListForPms = new ArrayList<PatientCat>(patientCatListForCorp);
        phsDataHelper.updatePatientCatForPms(em, patientCatListForCorp);
        pmsSubscriberProxy.updatePatientCatForPms(patientCatListForPms, new Date());
        
        List<Ward> wardListForCorp = phsServiceProxy.retrieveWardForPms();
        List<Ward> wardListForPms = new ArrayList<Ward>(wardListForCorp);
        phsDataHelper.updateWardForPms(em, wardListForCorp);
        pmsSubscriberProxy.updateWardForPms(wardListForPms, new Date());
        
        List<Specialty> specialtyListForCorp = phsServiceProxy.retrieveSpecialtyForPms();
        List<Specialty> specialtyListForPms = new ArrayList<Specialty>(specialtyListForCorp);
        phsDataHelper.updateSpecialtyForPms(em, specialtyListForCorp);
        pmsSubscriberProxy.updateSpecialtyForPms(specialtyListForPms, new Date());
        
        List<CapdSupplierItem> capdSupplierItemListForCorp = phsServiceProxy.retrieveCapdSupplierItemForPms();
        List<CapdSupplierItem> capdSupplierItemListForPms = new ArrayList<CapdSupplierItem>(capdSupplierItemListForCorp);
        phsDataHelper.updateCapdSupplierItemForPms(em, capdSupplierItemListForCorp);
        pmsSubscriberProxy.updateCapdSupplierItemForPms(capdSupplierItemListForPms, new Date());

		Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	

	@Remove
	public void destroyBatch() {
		
	}

	public void endChunk() throws Exception {
		
	}

}
