package hk.org.ha.model.pms.corp.biz.medprofile;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.moe.MoeException;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.biz.TimerProp;
import hk.org.ha.model.pms.corp.biz.order.CorporateTaskManagerLocal;
import hk.org.ha.model.pms.corp.vo.medprofile.MoeMsgCount;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.service.biz.pms.og.interfaces.OgCorpServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.ipmoe.IpmoeServiceJmsRemote;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("mpMsgCountTrigger")
@MeasureCalls
public class MpMsgCountTriggerBean implements MpMsgCountTriggerLocal {

	private static final String TRIGGER_MP_MSG_COUNT_TRX = "triggerMpMsgCountTrx";
	
	@Logger
	private Log logger;

	@In
	private IpmoeServiceJmsRemote ipmoeServiceProxy;
	
	@In
	private OgCorpServiceJmsRemote ogCorpServiceProxy;
	
	@In
	private CorporateTaskManagerLocal corporateTaskManager;	

	@In
	private TimerProp timerProp;
	
	@In
	private ApplicationProp applicationProp;
	
	SimpleDateFormat loggerDateFormatter = new SimpleDateFormat("yyyyMMdd.HHmmssSSS");
	
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy.MM.dd HH:mm");
	
	private List<MoeMsgCount> retrieveMoeMsgCount(List<String> patHospCodeList, String trxDate) {
		try {
			return ipmoeServiceProxy.retrieveMoeMsgCount(patHospCodeList, trxDate);
		} catch (MoeException e) {
			logger.error("retrieveMoeMsgCount error", e);
			return null;
		}
	}
	
	private List<MoeMsgCount> retrieveOgMsgCount(List<String> patHospCodeList, Date srcTrxDate, Date destTrxDate) {
		Date fromDate = DateUtils.truncate(srcTrxDate, Calendar.HOUR);
		Date toDate = DateUtils.truncate(destTrxDate, Calendar.HOUR);
		logger.info("retrieveOgMsgCount - fromDate:#0, toDate:#1", fromDate, toDate);
		return ogCorpServiceProxy.retrieveMoeMsgCountByPatHospcodeList(patHospCodeList, fromDate, toDate);
	}

	private void triggerMpMsgCount() {
		triggerMpMsgCount(new DateTime());
	}
	
	@SuppressWarnings("unchecked")
	public void triggerMpMsgCount(DateTime pastInDateTime) {		
		StringBuilder strBuilder;
		StringBuilder resultErrorStrBuilder = new StringBuilder();
		DateTime oneHourBeforeDateTime = pastInDateTime.minusHours(1);
		List<String> patHospCodeList = applicationProp.getMoeMsgCountPatHospCodeList();
		logger.info("triggerMpMsgCount - patHospCodeList:#0, currentDateTime:#1, oneHourPastDateTime:#2", 
				StringUtils.join(patHospCodeList.toArray(),","), 
				loggerDateFormatter.format(pastInDateTime.toDate()), 
				loggerDateFormatter.format(oneHourBeforeDateTime.toDate()));
		
		List<MoeMsgCount> moeMsgCountList = retrieveMoeMsgCount(patHospCodeList, dateFormatter.format(oneHourBeforeDateTime.toDate()));
		
		if (moeMsgCountList == null) {
			logger.info("mpMsgCountTrigger has been terminated.");
			return;
		}
		
		List<MoeMsgCount> ogMsgCountList = retrieveOgMsgCount(patHospCodeList, oneHourBeforeDateTime.toDate(), pastInDateTime.toDate());
		
		logger.info("triggerMpMsgCount - size of moeMsgCountList:#0, size of ogMsgCountList:#1", moeMsgCountList.size(), ogMsgCountList.size());
		
		List<String> moeMsgCountStringInfoList = new ArrayList<String>();
		List<String> ogMsgCountStringInfoList = new ArrayList<String>();
		
		for (MoeMsgCount msgCount:moeMsgCountList) {
			if (MpTrxType.dataValueOf(msgCount.getTrxCode()) != null && msgCount.getCount() > 0) {
				strBuilder = new StringBuilder();
				strBuilder.append("Patient hospital code:")
				.append(msgCount.getHospitalCode())
				.append(", Transaction code:")				
				.append(msgCount.getTrxCode())
				.append(", Message count:")
				.append(msgCount.getCount());
				moeMsgCountStringInfoList.add(strBuilder.toString());
			}
		}
		Collections.sort(moeMsgCountStringInfoList);
		
		for (MoeMsgCount msgCount:ogMsgCountList) {
			strBuilder = new StringBuilder();
			strBuilder.append("Patient hospital code:")
			.append(msgCount.getHospitalCode())
			.append(", Transaction code:")				
			.append(msgCount.getTrxCode())
			.append(", Message count:")
			.append(msgCount.getCount());
			ogMsgCountStringInfoList.add(strBuilder.toString());
		}
		Collections.sort(ogMsgCountStringInfoList);
		
		List<String> extraMoeMsgList = (List<String>)CollectionUtils.subtract(moeMsgCountStringInfoList, ogMsgCountStringInfoList);
		List<String> extraOgMsgList = (List<String>)CollectionUtils.subtract(ogMsgCountStringInfoList, moeMsgCountStringInfoList);
		
		if (extraMoeMsgList != null && extraMoeMsgList.size() > 0) {
			strBuilder = new StringBuilder();	
			strBuilder.append("Additional or unmatched message count from CMS:\n")
			.append(StringUtils.join(extraMoeMsgList.toArray(),"\n"));
			resultErrorStrBuilder.append(strBuilder.toString());
		}
		
		if (extraOgMsgList != null && extraOgMsgList.size() > 0) {
			strBuilder = new StringBuilder();
			strBuilder.append("Additional or unmatched message count from PMS-OG:\n")
			.append(StringUtils.join(extraOgMsgList.toArray(),"\n"));
			resultErrorStrBuilder.append(strBuilder.toString());
		}
		
		if (resultErrorStrBuilder.length() > 0) {
			logger.fatal(resultErrorStrBuilder.toString());	
		}
	}
	
	public void triggerMessageCountByTimer(Long duration, Long intervalDuration) {
		try {	
			if (timerProp.isActivate()) 
			{	
				if (corporateTaskManager.lock(
						TRIGGER_MP_MSG_COUNT_TRX, 
						new Date(), 
						intervalDuration.longValue())) {			
					triggerMpMsgCount();
				} else {
					logger.info("triggerMessageCountByTimer has been run previously within #0ms, bypass.", intervalDuration);
				}
			} else {
				logger.warn("triggerMessageCountByTimer temporary disabled by " + TimerProp.TIMER_PROPERTIES);
			}
		} catch (Exception e) {
			logger.error(e,e);
		}
	}
}
