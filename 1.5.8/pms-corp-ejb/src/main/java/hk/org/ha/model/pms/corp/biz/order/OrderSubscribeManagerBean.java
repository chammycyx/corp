package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.order.OrderSubscribe;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("orderSubscribeManager")
@MeasureCalls
public class OrderSubscribeManagerBean implements OrderSubscribeManagerLocal {
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@Override
	public Boolean unsubscribeMedOrder(String orderNum, String system) {
		OrderSubscribe orderSubscribe = retrieveOrderSubscribe(orderNum, system);
		
		if (orderSubscribe == null) {
			return false;
		} else {
			orderSubscribe.setStatus(RecordStatus.Delete);
			return true;
		}
	}

	@Override
	public void subscribeMedOrder(String orderNum, String system) {
		OrderSubscribe orderSubscribe = retrieveOrderSubscribe(orderNum, system);
		
		if (orderSubscribe == null) {
			orderSubscribe = createOrderSubscribe(orderNum, system);
			em.persist(orderSubscribe);
		} else {
			orderSubscribe.setStatus(RecordStatus.Active);
		}
	}

	@SuppressWarnings("unchecked")
	public OrderSubscribe retrieveOrderSubscribe(String orderNum, String system, RecordStatus status) {
		List<OrderSubscribe> orderSubscribeList = em.createQuery(
				"select o from OrderSubscribe o " + // 20150206 index check : OrderSubscribe.orderNum,system : UI_ORDER_SUBSCRIBE_01
				"where o.orderNum = :orderNum " +
				"and o.system = :system " +
				"and o.status = :status ")
				.setParameter("orderNum", orderNum)
				.setParameter("system", system)
				.setParameter("status", status)
				.getResultList();
		
		return orderSubscribeList.isEmpty() ? null : orderSubscribeList.get(0);
	}
	
	@SuppressWarnings("unchecked")
	private OrderSubscribe retrieveOrderSubscribe(String orderNum, String system) {
		List<OrderSubscribe> orderSubscribeList = em.createQuery(
				"select o from OrderSubscribe o " + // 20150206 index check : OrderSubscribe.orderNum,system : UI_ORDER_SUBSCRIBE_01
				"where o.orderNum = :orderNum " +
				"and o.system = :system")
				.setParameter("orderNum", orderNum)
				.setParameter("system", system)
				.getResultList();
		
		return orderSubscribeList.isEmpty() ? null : orderSubscribeList.get(0);
	}
	
	private OrderSubscribe createOrderSubscribe(String orderNum, String system) {
		OrderSubscribe orderSubscribe = new OrderSubscribe();
		orderSubscribe.setOrderNum(orderNum);
		orderSubscribe.setSystem(system);
		orderSubscribe.setStatus(RecordStatus.Active);
		return orderSubscribe;
	}	
}
