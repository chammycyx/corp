package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("corpPmsSessionInfoCoordinator")
@MeasureCalls
public class CorpPmsSessionInfoCoordinator implements CorpPmsSessionInfoCoordinatorLocal {

	@In
	private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	private static Map<String, List<PmsSessionInfo>> pmsSessionInfoRequestMap = new HashMap<String, List<PmsSessionInfo>>();	
	
	private long waitTime = 1000L; 
	
	public long getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(long waitTime) {
		this.waitTime = waitTime;
	}

	@Override
	public void consolidatePmsSessionInfo(String requestId, List<PmsSessionInfo> pmsSessionInfoList) {
		synchronized (pmsSessionInfoRequestMap) {
			List<PmsSessionInfo> list = pmsSessionInfoRequestMap.get(requestId);
			if (list != null) {
				list.addAll(pmsSessionInfoList);
			}
		}
		
	}

	@Override
	public List<PmsSessionInfo> retrievePmsSessionInfoList(String requestId, Workstore workstore) {
		
		synchronized (pmsSessionInfoRequestMap) {
			pmsSessionInfoRequestMap.put(requestId, new ArrayList<PmsSessionInfo>());
		}
		
		
		pmsSubscriberProxy.requestPmsSessionInfo(requestId, workstore);

		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
		}

		synchronized (pmsSessionInfoRequestMap) {
			return pmsSessionInfoRequestMap.remove(requestId);
		}
	}
}
