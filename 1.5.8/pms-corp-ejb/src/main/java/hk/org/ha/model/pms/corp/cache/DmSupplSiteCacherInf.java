package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;

import java.util.List;

public interface DmSupplSiteCacherInf extends BaseCacherInf {

	DmSupplSite getSupplSiteBySiteCodeSupplSiteEng(String siteCode, String supplSiteEng);
	
	List<DmSupplSite> getSupplSiteList();
	
	List<DmSupplSite> getSupplSiteListBySiteCode(String prefixSiteCode);
	
	List<DmSupplSite> getSupplSiteListBySupplSiteEng(String prefixSupplSiteEng);

}
