package hk.org.ha.model.pms.corp.vo.report;

import java.io.Serializable;

public class FmReportOrder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private String year;
	
	private String eisSpecCode;
	
	private String orderNum;
	
	private Integer sfiDrugCnt;
	
	private String clusterCode;
	
	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getEisSpecCode() {
		return eisSpecCode;
	}

	public void setEisSpecCode(String eisSpecCode) {
		this.eisSpecCode = eisSpecCode;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public void setSfiDrugCnt(Integer sfiDrugCnt) {
		this.sfiDrugCnt = sfiDrugCnt;
	}

	public Integer getSfiDrugCnt() {
		if (sfiDrugCnt == null) {
			sfiDrugCnt = 0;
		}
		
		return sfiDrugCnt;
	}

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}
	
}
