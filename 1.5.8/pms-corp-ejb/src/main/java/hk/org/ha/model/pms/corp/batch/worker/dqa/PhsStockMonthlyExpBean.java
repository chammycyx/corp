package hk.org.ha.model.pms.corp.batch.worker.dqa;

import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.service.biz.pms.dqa.interfaces.DqaBatchServiceJmsRemote;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Scope(ScopeType.SESSION)
@Name("phsStockMonthlyExpBean")
public class PhsStockMonthlyExpBean implements PhsStockMonthlyExpLocal {

	private Logger logger = null;
	
	private String jobId = null;
	
	@In
	private DqaBatchServiceJmsRemote dqaBatchServiceProxy;
	
	@Override
	public void beginChunk(Chunk arg0) throws Exception {
	}

	@Override
	@Remove
	public void destroyBatch() {
	}

	@Override
	public void endChunk() throws Exception {
	}

	@Override
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		this.jobId = info.getJobId();
		logger.info("initBatch "+info);
	}

	@Override
	public void processRecord(Record record) throws Exception 
	{
    	Date startDatetime = new Date();
        logger.info("jobId: "+jobId+" - process record start @ " + startDatetime);

        Boolean jobSucceed = dqaBatchServiceProxy.importPhsStockMonthlyExp();
        
        if(!jobSucceed)
        {
        	throw new Exception("Failed to import PhsStockMonthlyExp. Please check DQA logs for details.");
        }
        
		Date endDatetime = new Date();
		
        logger.info("jobId: "+jobId+" - process record complete @ " + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");

	}

}
