package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.refill.RefillScheduleStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

@AutoCreate
@Name("orderHelper")
@Scope(ScopeType.APPLICATION)
public class OrderHelper {
		
    public MedOrder retrieveMedOrder(EntityManager em, String orderNum) {
    	return retrieveMedOrder(em, orderNum, Arrays.asList(MedOrderStatus.SysDeleted, MedOrderStatus.Deleted));
    }
  
	@SuppressWarnings("unchecked")
	public MedOrder retrieveMedOrder(EntityManager em, String orderNum, List<MedOrderStatus> excludeStatusList) {
        
		List<MedOrder> medOrderList = em.createQuery(
				"select o from MedOrder o" + // 20120214 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :orderNum" +
				" and o.status not in :status")
				.setParameter("orderNum", orderNum)
				.setParameter("status", excludeStatusList)
				.getResultList();

        return (!medOrderList.isEmpty()) ? medOrderList.get(0) : null;          
	}
	
	public void lockMedOrder(EntityManager em, String orderNum) {
		em.createQuery(
				"select o.id from MedOrder o" + // 20120214 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :orderNum" +
				" and o.status not in :status")
				.setParameter("orderNum", orderNum)
				.setParameter("status", Arrays.asList(MedOrderStatus.SysDeleted))
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
	}
	
	public void lockMedOrder(EntityManager em, Long medOrderId) {
		em.createQuery(
				"select o.id from MedOrder o" + // 20150206 index check : MedOrder.id : PK_MED_ORDER
				" where o.id in :medOrderId")
				.setParameter("medOrderId", Arrays.asList(medOrderId))
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public PharmOrder retrievePharmOrder(EntityManager em, String orderNum) {
		List<PharmOrder> pharmOrderList = em.createQuery(
				"select o from PharmOrder o" + // 20120214 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.medOrder.orderNum = :orderNum" +
				" and o.status not in :status")
				.setParameter("orderNum", orderNum)
				.setParameter("status", Arrays.asList(PharmOrderStatus.SysDeleted))
				.getResultList();
		
		PharmOrder pharmOrder = null;		
		if ( ! pharmOrderList.isEmpty()) {
			pharmOrder = pharmOrderList.get(0);
			pharmOrder.loadChild();
		} else {
			MedOrder medOrder = retrieveMedOrder(em, orderNum);
			if (medOrder != null) {
				medOrder.loadChild();
				pharmOrder = new PharmOrder();
				pharmOrder.setMedOrder(medOrder);
			}			
		}

		return pharmOrder;		
	}

	public Ticket retrieveTicket(EntityManager em, Ticket ticket) {
		return em.find(Ticket.class, ticket.getId());
	}
	
	public Workstore retrieveWorkstore(EntityManager em, Workstore workstore) {
		
		return em.find(Workstore.class, workstore.getId());
	}
	
	public Ticket saveTicket(EntityManager em, Ticket remoteTicket) {
		return saveTicket(em, remoteTicket, false);
	}

	public Ticket saveTicket(EntityManager em, Ticket remoteTicket, boolean allowCreate) {		
		if (remoteTicket == null) {
			return null;
		}

		Ticket localTicket = null;

		if (remoteTicket.getId() != null) {
			localTicket = em.find(Ticket.class, remoteTicket.getId());
		} else if (!allowCreate) {
			throw new UnsupportedOperationException();
		}
		
		if (localTicket == null) {
			localTicket = remoteTicket;
			em.persist(localTicket);
		}

		return localTicket;		
	}

	public MedCase saveMedCase(EntityManager em, MedCase remoteMedCase, boolean allowCreate) {
		if (remoteMedCase == null) {
			return null;
		}

		MedCase localMedCase = null;
		if (remoteMedCase.getId() != null) {
			localMedCase = em.find(MedCase.class, remoteMedCase.getId());
		} else if (!allowCreate) {
			throw new UnsupportedOperationException();
		}
		
		if (localMedCase == null) 
		{ 
			// insert MedCase
			localMedCase = remoteMedCase;
			em.persist(localMedCase);
		}
		else 
		{ 
			// update MedCase
			localMedCase.updateByMedCase(remoteMedCase);
		}
		
		return localMedCase;
		
	}

	public Patient savePatient(EntityManager em, Patient remotePatient) {
		if (remotePatient == null) {
			return null;
		}
	
		Patient localPatient = this.retrievePatient(em, remotePatient);
		if (localPatient == null) { // insert Patient
			localPatient = remotePatient;
			em.persist(localPatient);
		}
		else { // update Patient
			localPatient.updateByPatient(remotePatient);
		}
	
		return localPatient;
		
	}
	
	@SuppressWarnings("unchecked")
	private Patient retrievePatient(EntityManager em, Patient patient) {
		
		if (patient == null) {
			return null;
		}
	
		if (patient.getId() != null) {
			return em.find(Patient.class, patient.getId());
		}
		else if (patient.getPatKey() != null) {
			List<Patient> patientList = em.createQuery(
					"select o from Patient o" + // 20120307 index check : Patient.patKey : I_PATIENT_01
					" where o.patKey = :patKey" + 
					" order by o.id")
					.setParameter("patKey", patient.getPatKey())
					.getResultList();
	
			if (!patientList.isEmpty()) {
				return patientList.get(0);
			}
		}
		return null;
	}	
	
	public MedOrder insertMedOrder(EntityManager em, MedOrder medOrder, boolean allowCreate, boolean includeTicket) {
		
		// patient is saved in another transaction and set to null before calling this function
		if (medOrder.getPatient() != null) {
			throw new IllegalArgumentException("Patient should be null, order num = " + medOrder.getOrderNum());
		}
		
		if (includeTicket && medOrder.getTicket() != null) {
			medOrder.setTicket(this.retrieveTicket(em, medOrder.getTicket()));
		} else {
			medOrder.setTicket(null);
		}

		medOrder.setWorkstore(this.retrieveWorkstore(em, medOrder.getWorkstore()));

		medOrder.setMedCase(this.saveMedCase(em, medOrder.getMedCase(), allowCreate));
		
		em.persist(medOrder);
		return medOrder;		
	}
	
	public List<DispOrder> markOrderSysDeleted(
			EntityManager em,
			PharmOrder pharmOrder, 
			Long prevPharmOrderId,
			Boolean remarkFlag) {
		
		List<DispOrder> delDispOrderList = new ArrayList<DispOrder>();
		
		MedOrder medOrder = pharmOrder.getMedOrder();

		// new manual order
		if (medOrder.getId() == null) {
			return delDispOrderList;
		}

		if (remarkFlag) 
		{ 
			// remark, manual order			
			MedOrder deleteMedOrder = em.find(MedOrder.class, medOrder.getId());
			if (deleteMedOrder == null) { // unvet dh order in cluster
				return delDispOrderList;
			}
			this.loadOrderList(em, deleteMedOrder);
			delDispOrderList.addAll(deleteMedOrder.markSysDeleted());				
		}
		else if (medOrder.getStatus() == MedOrderStatus.Complete) 
		{ 
			if (prevPharmOrderId != null) {
				// vetted order
				PharmOrder deletePharmOrder = em.find(PharmOrder.class, prevPharmOrderId);
				this.loadDispOrderList(em, deletePharmOrder);
				delDispOrderList.addAll(deletePharmOrder.markSysDeleted());
			} else {
				throw new RuntimeException(
						"Missing previous pharmOrder for vetted order!" +
						" orderNum:" + medOrder.getOrderNum() + 
						" medOrderId:" + medOrder.getId());
			}
		}

		em.flush();
		em.clear();
				
		return delDispOrderList;
	}	
	
	public void markOrderSysDeletedForRefillSchedule(
			EntityManager em,
			PharmOrder pharmOrder, 
			Long prevPharmOrderId,
			Boolean remarkFlag) {
		
		// note: after calling markOrderSysDeleted() in pms-pms, OrderManagerBean must call this function to mark 
		//       RefillSchedule status to sysDeleted
		
		MedOrder medOrder = pharmOrder.getMedOrder();

		// new manual order
		if (medOrder.getId() == null) {
			return;
		}
		
		if (remarkFlag) 
		{ 
			// remark, manual order			
			MedOrder deleteMedOrder = em.find(MedOrder.class, medOrder.getId());
			if (deleteMedOrder == null) { // unvet dh order in cluster
				return;
			}
			this.loadOrderList(em, deleteMedOrder);
			markSysDeletedForRefillScheduleByMedOrder(em, deleteMedOrder);
		}
		else if (medOrder.getStatus() == MedOrderStatus.Complete) 
		{ 
			if (prevPharmOrderId != null) {
				// vetted order
				PharmOrder deletePharmOrder = em.find(PharmOrder.class, prevPharmOrderId);
				this.loadDispOrderList(em, deletePharmOrder);
				markSysDeletedForRefillScheduleByPharmOrder(em, deletePharmOrder);
			} else {
				throw new RuntimeException(
						"Missing previous pharmOrder for vetted order!" +
						" orderNum:" + medOrder.getOrderNum() + 
						" medOrderId:" + medOrder.getId());
			}
		}
		
		em.flush();
		em.clear();
	}
	
	private void markSysDeletedForRefillScheduleByMedOrder(EntityManager em, MedOrder medOrder) {
		for (PharmOrder pharmOrder : medOrder.getPharmOrderList()) {
			markSysDeletedForRefillScheduleByPharmOrder(em, pharmOrder);
		}
	}
	
	private void markSysDeletedForRefillScheduleByPharmOrder(EntityManager em, PharmOrder pharmOrder) {
		String userName = "system";
		if (Contexts.isSessionContextActive()) {
			userName = Identity.instance().getCredentials().getUsername();
		} 
		
		em.createQuery(
				"update RefillSchedule o" + 
				" set o.status = :status," +
				" o.updateUser = :updateUser," +
				" o.updateDate = :updateDate," +
				" o.version = o.version + 1" +
				" where o.pharmOrder.id = :pharmOrderId ")
				.setParameter("status", RefillScheduleStatus.SysDeleted)
				.setParameter("updateUser", userName)
				.setParameter("updateDate", new Date())
				.setParameter("pharmOrderId", pharmOrder.getId())
				.executeUpdate();
	}
	
	public void loadOrderList(EntityManager em, MedOrder medOrder) 
	{		
		loadPharmOrderList(em, medOrder);
		for (PharmOrder pharmOrder : medOrder.getPharmOrderList()) {
			this.loadDispOrderList(em, pharmOrder);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadPharmOrderList(EntityManager em, MedOrder medOrder) 
	{		
		List<PharmOrder> pharmOrderList = em.createQuery(
				"select o from PharmOrder o" + // 20120214 index check : PharmOrder.medOrder : FK_MED_ORDER_01
				" where o.medOrder = :medOrder")
				.setParameter("medOrder", medOrder)
				.getResultList();
		
		medOrder.setPharmOrderList(pharmOrderList);
	}

	@SuppressWarnings("unchecked")
	public void loadDispOrderList(EntityManager em, PharmOrder pharmOrder) 
	{
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120214 index check : DispOrder.pharmOrder : FK_DISP_ORDER_02
				" where o.pharmOrder = :pharmOrder")
				.setParameter("pharmOrder", pharmOrder)
				.getResultList();
		
		pharmOrder.setDispOrderList(dispOrderList);		
	}

	@SuppressWarnings("unchecked")
	public List<MedOrderItem> retrieveMedOrderItemByItemNum(EntityManager em, String orderNum, List<Integer> medOrderItemNumList) {
		if (medOrderItemNumList.isEmpty()) {
			return new ArrayList<MedOrderItem>();
		}
		
		return em.createQuery(
				"select o from MedOrderItem o" + // 20120214 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.medOrder.orderNum = :orderNum" + 
				" and o.itemNum in :itemNumList" +
				" and o.remarkItemStatus <> :remarkItemStatus")
				.setParameter("orderNum", orderNum)
				.setParameter("itemNumList", medOrderItemNumList)
				.setParameter("remarkItemStatus", RemarkItemStatus.OldVersion)
				.getResultList();
	}
	
	public Set<DispOrder> retrieveDispOrderList(List<Invoice> voidInvoiceList) {
		Set<DispOrder> dispOrderSet = new HashSet<DispOrder>();
		for (Invoice invoice: voidInvoiceList) {
			DispOrder dispOrder = invoice.getDispOrder();
			// TODO: do u really need to load everything here?
			dispOrder.loadChild();
			dispOrderSet.add(dispOrder);
		}
		return dispOrderSet;
	}
	
	public List<Invoice> markSfiInvoiceSysDeleted(EntityManager em, List<Invoice> voidInvoiceList, List<Invoice> invoiceList){
		List<Invoice> updateVoidInvoiceList = new ArrayList<Invoice>();		
		Invoice sfiInvoice = null;
		
		for( Invoice invoice : invoiceList ){
			if( InvoiceDocType.Sfi == invoice.getDocType() ){
				sfiInvoice = invoice;
				break;
			}
		}
		
		for( Invoice voidInvoice : voidInvoiceList ){
			if( sfiInvoice != null && InvoiceDocType.Sfi == voidInvoice.getDocType() && StringUtils.equals(sfiInvoice.getInvoiceNum(), voidInvoice.getInvoiceNum()) ){
				Invoice prevVoidInvoice = em.find(Invoice.class, voidInvoice.getId());
				prevVoidInvoice.setStatus(InvoiceStatus.SysDeleted);
				em.flush();
				em.clear();
				
				updateVoidInvoiceList.add(prevVoidInvoice);
			}else{
				updateVoidInvoiceList.add(voidInvoice);
			}
		}	
		return updateVoidInvoiceList;
	}
}
