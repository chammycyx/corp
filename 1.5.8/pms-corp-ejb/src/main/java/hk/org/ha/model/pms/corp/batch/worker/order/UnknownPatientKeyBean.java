package hk.org.ha.model.pms.corp.batch.worker.order;
import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_UNKNOWNPATIENT_DURATION;
import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.OrderHelper;
import hk.org.ha.model.pms.corp.pas.PasServiceWrapper;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PatientStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.drools.util.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;


/**
 * Session Bean implementation class PatientEpisodeServiceBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("unknownPatientKeyBean")
public class UnknownPatientKeyBean implements UnknownPatientKeyLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private OrderHelper orderHelper;
	
	@In
	private CorpCddhServiceLocal corpCddhService;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	private Date orderStartDate = null;
	private Date orderEndDate = null;
	private Logger logger = null;
	
	public void beginChunk(Chunk arg0) throws Exception {
	}

	public void endChunk() throws Exception {
		
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		BatchParameters params = info.getBatchParameters();
		
		Date batchDate = null;
		if (params.getDate("batchDate") == null) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
		
		orderStartDate = dtBatchDate.minusDays(BATCH_UNKNOWNPATIENT_DURATION.get()).toDate();
		orderEndDate = new DateMidnight(batchDate).toDateTime().plusDays(1).minus(1).toDate();
	}
	
	@SuppressWarnings("unchecked")
	private void updatePatient(MedOrder medOrder, Patient patient) {
		List<Patient> patListByPatKey = em.createQuery(
				"select o from Patient o " + // 20120307 index check : Patient.patKey : I_PATIENT_01
				" where o.patKey = :patKey ")
				.setParameter("patKey", patient.getPatKey())
				.getResultList();
		
		if (!patListByPatKey.isEmpty()) {
			medOrder.getPatient().setStatus(PatientStatus.Inactive);	// update status of old patient after delink
			medOrder.setPatient(patListByPatKey.get(0));
		} else {
			medOrder.getPatient().setStatus(PatientStatus.Inactive);	// update status of old patient after delink
			medOrder.setPatient(orderHelper.savePatient(em, patient)); // create new patient if not found
		}
	}
	
	private void updatePatientByHkid(String hkid, MedOrder medOrder) throws PasException, UnreachableException {
		Patient pasPatient = pasServiceWrapper.retrievePasPatientByHkid(hkid, StringUtils.EMPTY, StringUtils.EMPTY);
		updatePatient(medOrder, pasPatient);
	}
	
	private void updatePatientByCaseNum(String patHospCode, String caseNum, MedOrder medOrder) throws PasException, UnreachableException {
		Patient pasPatient = pasServiceWrapper.retrievePasPatientByCaseNum(patHospCode, caseNum);
		updatePatient(medOrder, pasPatient);
	}
	
	@SuppressWarnings("unchecked")
	public void processRecord(Record record) throws Exception {
		
		String hospCode = record.getString("HOSP_CODE");
		String workstoreCode = record.getString("WORKSTORE_CODE");
		
		Date startDatetime = new Date();
		logger.info("process record start for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + "]... @" + startDatetime);
		
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20140822 index check : MedOrder.workstore,orderType,orderDate : I_MED_ORDER_06
				" where o.pharmOrder.medOrder.workstore.hospCode = :hospCode" +
				" and o.pharmOrder.medOrder.workstore.workstoreCode = :workstoreCode" + 
				" and o.pharmOrder.medOrder.orderType = :orderType" +
				" and o.pharmOrder.medOrder.orderDate between :orderStartDate and :orderEndDate" +
				" and o.pharmOrder.medOrder.status not in :status" +
				" and o.pharmOrder.medOrder.patient.patKey is null" +
				" and o.pharmOrder.medOrder.patient.hkid is not null")
				.setParameter("hospCode", hospCode)
				.setParameter("workstoreCode", workstoreCode)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("orderStartDate", orderStartDate)
				.setParameter("orderEndDate", orderEndDate)
				.setParameter("status", Arrays.asList(MedOrderStatus.SysDeleted, MedOrderStatus.Deleted))
				.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient")
				.getResultList();

		// convert distinct MedOrder to list
		Set<MedOrder> medOrderSet = new HashSet<MedOrder>();
		List<MedOrder> medOrderList = new ArrayList<MedOrder>();
		for (DispOrder dispOrder : dispOrderList) {
			medOrderSet.add(dispOrder.getPharmOrder().getMedOrder());
		}
		medOrderList.addAll(medOrderSet);

		logger.info("get MedOrder from [" + orderStartDate + "] to [" + orderEndDate + "] for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + 
				"], count = " + medOrderList.size());
		
		for (MedOrder medOrder: medOrderList) {
			Patient moExceptionPatient = medOrder.getPatient();
			
			List<Patient> patList = em.createQuery(
					"select o from Patient o " + // 20120307 index check : Patient.hkid : I_PATIENT_02
					" where o.hkid = :hkid " +
					" and o.name = :name " +
					" and o.sex = :sex " +
					" and o.patKey is not null")
					.setParameter("hkid", moExceptionPatient.getHkid())
					.setParameter("name", moExceptionPatient.getName())
					.setParameter("sex", moExceptionPatient.getSex())
					.getResultList();
			
			if (patList.size() == 1) { // patient found in local db
				Patient newPatient = patList.get(0);
				medOrder.getPatient().setStatus(PatientStatus.Inactive);	// update status of old patient after delink
				medOrder.setPatient(newPatient);
				
			} else {
				if (medOrder.getMedCase() != null && medOrder.getMedCase().getCaseNum() != null) {
					try {
						if (logger.isDebugEnabled()) {
							logger.debug("check patient by case number: orderNum=" + medOrder.getOrderNum() + ", patHospCode="+ medOrder.getPatHospCode() +", caseNum=" + medOrder.getMedCase().getCaseNum());
						}
						updatePatientByCaseNum(medOrder.getPatHospCode(), medOrder.getMedCase().getCaseNum(), medOrder);
						
					} catch (PasException retrievePasPatientByHkidException) {
						logger.info("Patient not found in retrievePasPatientByHkid for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + 
								"]: orderNum=" + medOrder.getOrderNum() + ", patHospCode=" + medOrder.getPatHospCode() + ", caseNum=" + medOrder.getMedCase().getCaseNum());
					}
					
				} else {
					try{
						if (logger.isDebugEnabled()) {
						logger.debug("check patient by hkid: orderNum=" + medOrder.getOrderNum() + ", patKey=" + moExceptionPatient.getPatKey());
						}
						updatePatientByHkid(moExceptionPatient.getHkid(), medOrder);
						
					} catch (PasException retrievePasPatientByCaseNumException) {
						logger.info("Patient not found in retrievePasPatientByCaseNum for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + 
								"]: orderNum=" + medOrder.getOrderNum() + ", patKey=" + moExceptionPatient.getPatKey());
					}
				}
			}
		}
		
		// update patient patient profile
		// (note: must be before updating DO patient, because old patient id (in DO) is required here)
		Map<String, Pair<Long, Long>> movePatientIdPairByOrderNumMap = new HashMap<String, Pair<Long, Long>>();
		for (DispOrder dispOrder : dispOrderList) {
			Pair<Long, Long> movePatientIdPair = new Pair<Long, Long>(dispOrder.getPatient().getId(), dispOrder.getPharmOrder().getMedOrder().getPatient().getId());
			movePatientIdPairByOrderNumMap.put(dispOrder.getPharmOrder().getMedOrder().getOrderNum(), movePatientIdPair);
		}
		corpCddhService.updatePatientProfilePatientId(movePatientIdPairByOrderNumMap);
		
		// copy patKey and hkid to DispOrder
		// (DispOrder columns is used for creating SQL index) 
		for (DispOrder dispOrder : dispOrderList) {
			dispOrder.setPatient(dispOrder.getPharmOrder().getMedOrder().getPatient());
		}

		// note: do not sync updated Patient of MedOrder to cluster because it is used in CORP for CDDH only,
		//       and CMS will update the Patient when saving order
		
		Date endDatetime = new Date();
        logger.info("process record complete for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + "]..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
}
