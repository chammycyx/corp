package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItem;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItemCriteria;
import hk.org.ha.model.pms.corp.vo.moe.ChargeRule;
import hk.org.ha.model.pms.corp.vo.moe.OrderInfo;
import hk.org.ha.model.pms.corp.vo.moe.OrderInfoSearchCriteria;
import hk.org.ha.model.pms.corp.vo.moe.PmsOrderHdr;
import hk.org.ha.model.pms.corp.vo.moe.PrnDuration;
import hk.org.ha.model.pms.corp.vo.moe.RoutePrn;
import hk.org.ha.model.pms.corp.vo.moe.WardStock;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PharmacyService {

	PmsOrderHdr retrievePmsOrderHdr(OrderInfoSearchCriteria criteria);

	List<RoutePrn> retrieveRoutePrnList(String dispHospCode, String dispWorkstore);

	PrnDuration retrievePrnDuration(String dispHospCode, String dispWorkstore);

	ChargeRule retrieveChargeRule(String dispHospCode, String dispWorkstore);
	
	boolean allowDeleteMpItem(String patHospCode, Long ordNum, Integer itemNum);
	
	List<WardStock> retrieveWardStockList(String patHospCode);
	
	OrderInfo retrieveOrderInfo(String patHospCode, Long trxId);
	
	List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria);
	
	List<CddhDispenseItem> retrieveCddhDispenseItemList(CddhDispenseItemCriteria criteria);
}
