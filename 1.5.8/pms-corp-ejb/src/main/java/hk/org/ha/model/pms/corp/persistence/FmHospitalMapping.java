package hk.org.ha.model.pms.corp.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "FM_HOSPITAL_MAPPING")
public class FmHospitalMapping extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;
	
	@Column(name = "FROM_CLUSTER_CODE", nullable = false, length = 3)
	private String fromClusterCode;
	
	@Column(name = "TO_CLUSTER_CODE", nullable = false, length = 3)
	private String toClusterCode;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getFromClusterCode() {
		return fromClusterCode;
	}

	public void setFromClusterCode(String fromClusterCode) {
		this.fromClusterCode = fromClusterCode;
	}

	public String getToClusterCode() {
		return toClusterCode;
	}

	public void setToClusterCode(String toClusterCode) {
		this.toClusterCode = toClusterCode;
	}
	
	
}
