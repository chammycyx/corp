package hk.org.ha.model.pms.corp.vo.report;

import hk.org.ha.model.pms.udt.vetting.ActionStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.FmStatusOption;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FmReportInfo implements Serializable {
	
	private final transient Log logger = LogFactory.getLog(getClass());
	
	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private String year;
	
	private String month;
	
	private String eisSpecCode;
	
	private String pasSpecCode;
	
	private String pasSubSpecCode;
	
	private String specCode;
	
	private String patKey;
	
	private String caseNum;
		
	private Date orderDate;
	
	private Integer orderNum;
	
	private String refNum;
	
	private String orderStatus;
	
	private String orderSubtype;
	
	private ActionStatus actionStatus;
	
	private String orderDesc;
	
	private String displayName;
	
	private String formDesc;
	
	private String routeDesc;
	
	private String saltProperty;
	
	private String strength;
	
	private String itemCode;
	
	private String haSold;
	
	private String fmStatus;
	
	private String sfiCateogry;
	
	private String bnfNum;
	
	private String bnfDesc;
	
	private String privatePat;
	
	private String payCode;
	
	private String prescOption;
	
	private String coInd;
	
	private String coIndDesc;
	
	private String suInd;
	
	private String suIndDesc;
	
	private String otherMessage;
	
	private String doctorCode;
		
	private String authCode;
	
	private String medicalOfficerName;
	
	private String rank;
		
	private String formCode;
	
	private Integer itemNum;
	
	private String volText;
		
	private String imisSpecialty;
	
	private Integer generalDrugCnt;
	
	private Integer specialDrugCnt;
	
	private Integer specialAndExistPat;
	
	private Integer specialAndOther;
	
	private Integer specialAndIndicated;
	
	private Integer specialAndNonformInd;
	
	private Integer sfi;
	
	private Integer namedPatient;
	
	private Integer sfiSn;
	
	private Integer refOrderNum;
	
	private Integer refItemNum;
	
	private String haIssue;
		
	private String tItemcode;
	
	private Integer batchNum;
	
	private String clusterCode;
	
	private Boolean addToDetailReport;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getEisSpecCode() {
		return eisSpecCode;
	}

	public void setEisSpecCode(String eisSpecCode) {
		this.eisSpecCode = eisSpecCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderSubtype() {
		return orderSubtype;
	}

	public void setOrderSubtype(String orderSubtype) {
		this.orderSubtype = orderSubtype;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getSaltProperty() {
		return saltProperty;
	}

	public void setSaltProperty(String saltProperty) {
		this.saltProperty = saltProperty;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getHaSold() {
		return haSold;
	}

	public void setHaSold(String haSold) {
		this.haSold = haSold;
	}

	public String getFmStatus() {
		return fmStatus;
	}
	
	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public String getSfiCateogry() {
		return sfiCateogry;
	}

	public void setSfiCateogry(String sfiCateogry) {
		this.sfiCateogry = sfiCateogry;
	}

	public String getBnfNum() {
		return bnfNum;
	}

	public void setBnfNum(String bnfNum) {
		this.bnfNum = bnfNum;
	}

	public String getBnfDesc() {
		return bnfDesc;
	}

	public void setBnfDesc(String bnfDesc) {
		this.bnfDesc = bnfDesc;
	}

	public String getPrivatePat() {
		return privatePat;
	}

	public void setPrivatePat(String privatePat) {
		this.privatePat = privatePat;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getPrescOption() {
		return prescOption;
	}

	public void setPrescOption(String prescOption) {
		this.prescOption = prescOption;
	}
	
	public String getCoInd() {
		return coInd;
	}

	public void setCoInd(String coInd) {
		this.coInd = coInd;
	}

	public String getCoIndDesc() {
		return coIndDesc;
	}

	public void setCoIndDesc(String coIndDesc) {
		this.coIndDesc = coIndDesc;
	}

	public String getSuInd() {
		return suInd;
	}

	public void setSuInd(String suInd) {
		this.suInd = suInd;
	}

	public String getSuIndDesc() {
		return suIndDesc;
	}

	public void setSuIndDesc(String suIndDesc) {
		this.suIndDesc = suIndDesc;
	}

	public String getOtherMessage() {
		return otherMessage;
	}

	public void setOtherMessage(String otherMessage) {
		this.otherMessage = otherMessage;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getMedicalOfficerName() {
		return medicalOfficerName;
	}

	public void setMedicalOfficerName(String medicalOfficerName) {
		this.medicalOfficerName = medicalOfficerName;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getVolText() {
		return volText;
	}

	public void setVolText(String volText) {
		this.volText = volText;
	}

	public String getImisSpecialty() {
		return imisSpecialty;
	}

	public void setImisSpecialty(String imisSpecialty) {
		this.imisSpecialty = imisSpecialty;
	}

	public Integer getGeneralDrugCnt() {
		return generalDrugCnt;
	}

	public void setGeneralDrugCnt(Integer generalDrugCnt) {
		this.generalDrugCnt = generalDrugCnt;
	}

	public Integer getSpecialDrugCnt() {
		return specialDrugCnt;
	}

	public void setSpecialDrugCnt(Integer specialDrugCnt) {
		this.specialDrugCnt = specialDrugCnt;
	}

	public Integer getSpecialAndExistPat() {
		return specialAndExistPat;
	}

	public void setSpecialAndExistPat(Integer specialAndExistPat) {
		this.specialAndExistPat = specialAndExistPat;
	}

	public Integer getSpecialAndOther() {
		return specialAndOther;
	}

	public void setSpecialAndOther(Integer specialAndOther) {
		this.specialAndOther = specialAndOther;
	}

	public Integer getSpecialAndIndicated() {
		return specialAndIndicated;
	}

	public void setSpecialAndIndicated(Integer specialAndIndicated) {
		this.specialAndIndicated = specialAndIndicated;
	}

	public Integer getSpecialAndNonformInd() {
		return specialAndNonformInd;
	}

	public void setSpecialAndNonformInd(Integer specialAndNonformInd) {
		this.specialAndNonformInd = specialAndNonformInd;
	}

	public Integer getSfi() {
		return sfi;
	}

	public void setSfi(Integer sfi) {
		this.sfi = sfi;
	}

	public Integer getNamedPatient() {
		return namedPatient;
	}

	public void setNamedPatient(Integer namedPatient) {
		this.namedPatient = namedPatient;
	}

	public Integer getSfiSn() {
		return sfiSn;
	}

	public void setSfiSn(Integer sfiSn) {
		this.sfiSn = sfiSn;
	}

	public Integer getRefOrderNum() {
		return refOrderNum;
	}

	public void setRefOrderNum(Integer refOrderNum) {
		this.refOrderNum = refOrderNum;
	}

	public Integer getRefItemNum() {
		return refItemNum;
	}

	public void setRefItemNum(Integer refItemNum) {
		this.refItemNum = refItemNum;
	}

	public String getHaIssue() {
		return haIssue;
	}

	public void setHaIssue(String haIssue) {
		this.haIssue = haIssue;
	}

	public String gettItemcode() {
		return tItemcode;
	}

	public void settItemcode(String tItemcode) {
		this.tItemcode = tItemcode;
	}

	public Integer getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(Integer batchNum) {
		this.batchNum = batchNum;
	}

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}
	
	public String getYearMonth() {
		return this.getYear() + this.getMonth();
	}
	
	public void setPrescOption(FmStatusOption fmStatusOption) {
		setPrescOption(fmStatusOption.getDataValue());
	}
	
	public String getPrescOptionDesc() {
		if (!StringUtils.isBlank(getPrescOption()))
		{
			return FmStatusOption.dataValueOf(getPrescOption()).getDisplayValue();
		}
		
		return getPrescOption();
	}
	
	public String getFmStatusDesc() {
		if (getFmStatus() == null)
		{
			logger.info("order num " + orderNum + " with itemNum " + itemNum + " and displayName " + displayName + " fmStatus is null");
			return "";
		}
		
		return FmStatus.dataValueOf(getFmStatus()).getDisplayValue();
	}

	public void setAddToDetailReport(Boolean addToDetailReport) {
		this.addToDetailReport = addToDetailReport;
	}

	public Boolean getAddToDetailReport() {
		return addToDetailReport;
	}
	
}
