package hk.org.ha.model.pms.corp.batch.worker.reftable;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsRptServiceJmsRemote;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Scope(ScopeType.SESSION)
@Name("followUpWarningBean")
public class FollowUpWarningBean implements FollowUpWarningLocal {

	private Logger logger = null;

    @In
    private DmsRptServiceJmsRemote dmsRptServiceProxy;
    
    @In
    private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
    public void initBatch(BatchWorkerInfo info, Logger logger) {
		this.logger = logger;
	}

	public void beginChunk(Chunk chunk) {
		
	}

	public void processRecord(Record arg0) {
    	Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);
		
		List<String> itemCodeList = dmsRptServiceProxy.retrieveFollowUpWarningItemCode();
		Date updateTime = new Date();
		pmsSubscriberProxy.updateFollowUpWarningItemCode(itemCodeList, updateTime);
		
		Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	@Remove
	public void destroyBatch() {
		
	}

	public void endChunk() throws Exception {
		
	}
	
}
