package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.sys.SysProfile;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("corpStarter")
@Scope(ScopeType.APPLICATION)
public class CorpStarter {

	@In
	private CorpStartupLocal corpStartup;
		
	@In
	private SysProfile sysProfile;
	
	private int delayTime = 180000;
		
	public void setDelayTime(int delayTime) {
		this.delayTime = delayTime;
	}

	public int getDelayTime() {
		return delayTime;
	}

	@Create
	public void create() {
		
		// Don't start on Testing Phase
		if (sysProfile.isTesting()) {
			return;
		}
				
		corpStartup.startup(Long.valueOf(getDelayTime()));
	}

}
