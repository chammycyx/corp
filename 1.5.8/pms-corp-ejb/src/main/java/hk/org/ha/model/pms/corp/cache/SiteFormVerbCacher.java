package hk.org.ha.model.pms.corp.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.SiteFormVerb;
import hk.org.ha.model.pms.dms.vo.SiteFormVerbSearchCriteria;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("siteFormVerbCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class SiteFormVerbCacher extends BaseCacher implements SiteFormVerbCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	private Map<String, InnerCacher> cacherMap = new HashMap<String, InnerCacher>();	
	
	@Override
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}	
	
	public List<SiteFormVerb> getSiteFormVerbList() {		
		return (List<SiteFormVerb>) this.getCacher(null).getAll();
	}

	public List<SiteFormVerb> getSiteFormVerbListByFormCode(String formCode) {
		return (List<SiteFormVerb>) this.getCacher(formCode).getAll();		
	}

	public FormVerb getDefaultFormVerbByFormCode(String formCode) {
		List<SiteFormVerb> siteFormVerbList = (List<SiteFormVerb>) this.getCacher(formCode).getAll();
		if ( siteFormVerbList == null ) {
			return null;			
		}
		for (SiteFormVerb siteFormVerb : siteFormVerbList) {
			for (FormVerb formVerb : siteFormVerb.getFormVerbs()) {
				if (formVerb.getRank() == 1) {
					return formVerb;
				}
			}
		}		
		return null;
	}
	
	private InnerCacher getCacher(String formCode) {
		synchronized (this) { 
			InnerCacher cacher = cacherMap.get( formCode );
			
			if ( cacher == null ) {
				SiteFormVerbSearchCriteria criteria = new SiteFormVerbSearchCriteria();
				criteria.setFormCode(formCode);
				cacher = new InnerCacher(criteria, this.getExpireTime());
				cacherMap.put( formCode, cacher );
			}
									
			return cacher;
		}
	}
	
	private class InnerCacher extends AbstractCacher<String, SiteFormVerb>
	{				
		private SiteFormVerbSearchCriteria criteria=null;
		
        public InnerCacher(SiteFormVerbSearchCriteria criteria, int expireTime) {
            super(expireTime);
            this.criteria = criteria;
        }
		
		@Override
		public SiteFormVerb create(String formCode) {			
			throw new UnsupportedOperationException();
		}

		@Override
		public Collection<SiteFormVerb> createAll() {
			return dmsPmsServiceProxy.retrieveSiteFormVerb(criteria);
		}

		@Override
		public String retrieveKey(SiteFormVerb siteFormVerb) {
			return null;
		}		
	}
	
	public static SiteFormVerbCacherInf instance()
	{
		if (!Contexts.isApplicationContextActive())
		{
			throw new IllegalStateException("No active application scope");
		}
		return (SiteFormVerbCacherInf) Component.getInstance("siteFormVerbCacher", ScopeType.APPLICATION);
	}
	
}
