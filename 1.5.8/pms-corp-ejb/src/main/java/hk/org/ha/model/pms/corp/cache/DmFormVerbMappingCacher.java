package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmFormVerbMappingCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmFormVerbMappingCacher extends BaseCacher implements DmFormVerbMappingCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	
	
	private InnerCacher cacher;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	

    public DmFormVerbMapping getDmFormVerbMappingByFormCodeSiteCode(String formCode, String siteCode) {
    	//TODO : DmFormVerbMapping use composite key
    	String mappingKey = formCode + siteCode;
		return (DmFormVerbMapping) this.getCacher().get(mappingKey);
	}

	public List<DmFormVerbMapping> getDmFormVerbMappingList() {
		return (List<DmFormVerbMapping>) this.getCacher().getAll();
	}

	public List<DmFormVerbMapping> getDmFormVerbMappingListByFormCode(String prefixFormCode) {
		List<DmFormVerbMapping> ret = new ArrayList<DmFormVerbMapping>();
		boolean found = false;
		for (DmFormVerbMapping dmFormVerbMapping : getDmFormVerbMappingList()){
			if(dmFormVerbMapping.getFormCode().equals(prefixFormCode)){
				ret.add(dmFormVerbMapping);
				found = true;
			} else {
				if (found) {
					break;
				}
			}
		}
		return ret;
	}

	public List<DmFormVerbMapping> getDmFormVerbMappingListBySiteCode(String prefixSiteCode) {
		List<DmFormVerbMapping> ret = new ArrayList<DmFormVerbMapping>();
		for (DmFormVerbMapping dmFormVerbMapping : getDmFormVerbMappingList()){
			if(dmFormVerbMapping.getSiteCode().equals(prefixSiteCode)){
				ret.add(dmFormVerbMapping);
			}
		}
		return ret;
	}


	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmFormVerbMapping> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmFormVerbMapping create(String mappingKey) {
			this.getAll();
			return this.internalGet(mappingKey);
		}

		@Override
		public Collection<DmFormVerbMapping> createAll() {
			return dmsPmsServiceProxy.retrieveDmFormVerbMappingList();
		}

		@Override
		public String retrieveKey(DmFormVerbMapping dmFormVerbMapping) {
			return dmFormVerbMapping.getFormCode() + dmFormVerbMapping.getSiteCode();
		}
	}

	public static DmFormVerbMappingCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmFormVerbMappingCacherInf) Component.getInstance("dmFormVerbMappingCacher",
				ScopeType.APPLICATION);
	}
}
