package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AspDispManagerLocal {
	public void filterAndSend(String hospCode, String workstoreCode, List<DispOrderItem> dispOrderItemList, Date batchDate) throws RuntimeException;
}
