package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DrugDispensaryInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String dispensingInstId;
	
	@XmlElement
	private String dispensaryCode;
	
	@XmlElement
	private String dispensingInstName;
	
	@XmlElement(name="DrugDispensaryOrder")
	private List<DrugDispensaryOrder> drugDispensaryOrderList;

	public String getDispensingInstId() {
		return dispensingInstId;
	}

	public void setDispensingInstId(String dispensingInstId) {
		this.dispensingInstId = dispensingInstId;
	}

	public String getDispensaryCode() {
		return dispensaryCode;
	}

	public void setDispensaryCode(String dispensaryCode) {
		this.dispensaryCode = dispensaryCode;
	}

	public String getDispensingInstName() {
		return dispensingInstName;
	}

	public void setDispensingInstName(String dispensingInstName) {
		this.dispensingInstName = dispensingInstName;
	}

	public List<DrugDispensaryOrder> getDrugDispensaryOrderList() {
		return drugDispensaryOrderList;
	}

	public void setDrugDispensaryOrderList(List<DrugDispensaryOrder> drugDispensaryOrderList) {
		this.drugDispensaryOrderList = drugDispensaryOrderList;
	}

}
