package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmFrequency;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmFrequencyCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmFrequencyCacher extends BaseCacher implements DmFrequencyCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

    private InnerCacher cacher;	
	    
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
    
	public DmFrequency getDmFrequencyByFreqCode(String freqCode) {
		return (DmFrequency) this.getCacher().get(freqCode);
	}
	
	public List<DmFrequency> getDmFrequencyList() {
		return (List<DmFrequency>) this.getCacher().getAll();
	}
	
	public List<DmFrequency> getDmFrequencyListByFreqCode(String prefixFreqCode) {
		List<DmFrequency> ret = new ArrayList<DmFrequency>();
		for (DmFrequency dmFrequency : getDmFrequencyList()) {
			if (dmFrequency.getFreqCode().startsWith(prefixFreqCode)) {
				ret.add(dmFrequency);
			}
		}
		return ret;
	}
	
	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmFrequency>
	{		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }
		
		@Override
		public DmFrequency create(String freqCode) {
			this.getAll();
			return this.internalGet(freqCode);
		}

		@Override
		public Collection<DmFrequency> createAll() {
			return dmsPmsServiceProxy.retrieveDmFrequencyList();
		}

		@Override
		public String retrieveKey(DmFrequency dmFrequency) {
			return dmFrequency.getFreqCode();
		}		
	}
	
	public static DmFrequencyCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmFrequencyCacherInf) Component.getInstance("dmFrequencyCacher", ScopeType.APPLICATION);
    }	
}
