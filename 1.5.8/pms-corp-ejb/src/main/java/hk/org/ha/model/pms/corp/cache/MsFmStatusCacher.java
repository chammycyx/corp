package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("msFmStatusCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class MsFmStatusCacher extends BaseCacher implements MsFmStatusCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
	
    public MsFmStatus getMsFmStatusByFmStatus(String fmStatus) {
		return (MsFmStatus) this.getCacher().get(fmStatus);
	}

	public List<MsFmStatus> getMsFmStatusList() {
		return (List<MsFmStatus>) this.getCacher().getAll();
	}
		   
	public List<MsFmStatus> getMsFmStatusListByFmStatus(String prefixFmStatus) {
		List<MsFmStatus> ret = new ArrayList<MsFmStatus>();
		boolean found = false;
		for (MsFmStatus msFmStatus : getMsFmStatusList()) {
			if (msFmStatus.getFmStatus().startsWith(prefixFmStatus)) {
				ret.add(msFmStatus);
				found = true;
			} else {
				if (found) {
					break;
				}
			}
		}
		return ret;
	}

	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, MsFmStatus> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public MsFmStatus create(String fmStatus) {
			this.getAll();			
			return this.internalGet(fmStatus);
		}

		@Override
		public Collection<MsFmStatus> createAll() {
			return dmsPmsServiceProxy.retrieveMsFmStatusList();
		}

		@Override
		public String retrieveKey(MsFmStatus msFmStatus) {
			return msFmStatus.getFmStatus();
		}
	}

	public static MsFmStatusCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (MsFmStatusCacherInf) Component.getInstance("msFmStatusCacher",
				ScopeType.APPLICATION);
	}
}
