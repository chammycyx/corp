package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.vo.report.FmReportInfo;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Fluid;
import hk.org.ha.model.pms.vo.rx.RxDrug;

public interface FmRptConverterInf {
	
	void create();
	
	void convertFmRxDrugReportData(FmReportInfo fmReportInfo, MedOrderFm medOrderFm, RxDrug rxDrug);
	
	void convertFmCapdRxDrugReportData(FmReportInfo fmReportInfo, CapdRxDrug capdRxDrug);
	
	void convertFmFluidReportData(FmReportInfo fmReportInfo, Fluid fluid);
	
	void destroy();
}