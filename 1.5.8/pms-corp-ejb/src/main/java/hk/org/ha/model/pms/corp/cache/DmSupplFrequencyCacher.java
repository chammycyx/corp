package hk.org.ha.model.pms.corp.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.udt.vetting.SupplFreqSort;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmSupplFrequencyCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmSupplFrequencyCacher extends BaseCacher implements DmSupplFrequencyCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	

	private Map<StringArray, InnerCacher> cacherMap = new HashMap<StringArray, InnerCacher>();
	
	@Override
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}	
	
	public DmSupplFrequency getDmSupplFrequencyBySupplFreqCode( String supplFreqCode ) {
		return this.getCacher( "ALL", SupplFreqSort.Rank ).get( supplFreqCode );
	}
	
	public List<DmSupplFrequency> getDmSupplFrequencyList( String regimenCode, SupplFreqSort sortSeq ) {
		return getDmSupplFrequency(regimenCode, sortSeq, false);
	}
	
	public List<DmSupplFrequency> getDmSupplFrequencySuspendList( String regimenCode, SupplFreqSort sortSeq ) {
		return getDmSupplFrequency(regimenCode, sortSeq, true);
	}

	private List<DmSupplFrequency> getDmSupplFrequency( String regimenCode, SupplFreqSort sortSeq, boolean isSuspended ) {
		List<DmSupplFrequency> resultList = new ArrayList<DmSupplFrequency>();
		for (DmSupplFrequency dmSupplFrequency : this.getCacher( regimenCode, sortSeq ).getAll()) {
			if ( isSuspended == "Y".equals(dmSupplFrequency.getSuspend()) ) {
				resultList.add(dmSupplFrequency);
			}			
		}
		return resultList;
	}

	private InnerCacher getCacher( String regimenCode, SupplFreqSort sortSeq ) {
		synchronized (this) { 
			StringArray key = new StringArray(regimenCode, sortSeq.toString());
			InnerCacher cacher = cacherMap.get(key);
			if (cacher == null) {
				cacher = new InnerCacher(regimenCode, sortSeq, this.getExpireTime());
				cacherMap.put(key, cacher);
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmSupplFrequency>
	{
		private String regimenCode;
		
		private SupplFreqSort sortSeq;
		
		public InnerCacher(String regimenCode, SupplFreqSort sortSeq, int expireTime) {
			super(expireTime);  
			this.regimenCode = regimenCode;
			this.sortSeq = sortSeq;
		}

		@Override
		public DmSupplFrequency create(String supplFreqCode) {
			this.getAll();
			return this.internalGet(supplFreqCode);
		}

		@Override
		public Collection<DmSupplFrequency> createAll() {	
			List<DmSupplFrequency> list;
			if ( "ALL".equals(regimenCode) ) {
				list = dmsPmsServiceProxy.retrieveDmSupplFrequencyList();
			} else {
				list = dmsPmsServiceProxy.retrieveDmSupplFrequencyListByRegimenCode( regimenCode );
			}
			if (sortSeq == SupplFreqSort.Rank) {
				Collections.sort(list, new DmSupplFreqComparatorByRank());
			} else if (sortSeq == SupplFreqSort.SupplFreqDesc) {
				Collections.sort(list, new DmSupplFreqComparatorByDesc());
			}			
			return list;			
		}

		@Override
		public String retrieveKey(DmSupplFrequency dmSupplFrequency) {
			return dmSupplFrequency.getSupplFreqCode();
		}		
	}
	
	public static DmSupplFrequencyCacherInf instance() {
		if (!Contexts.isApplicationContextActive())
		{
			throw new IllegalStateException("No active application scope");
		}
		return (DmSupplFrequencyCacherInf) Component.getInstance("dmSupplFrequencyCacher", ScopeType.APPLICATION);
	}
	
	public static class DmSupplFreqComparatorByRank implements Comparator<DmSupplFrequency>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( DmSupplFrequency f1, DmSupplFrequency f2 ) {
			return f1.getRank().compareTo( f2.getRank() );
		}
	}
	
	public static class DmSupplFreqComparatorByDesc implements Comparator<DmSupplFrequency>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( DmSupplFrequency f1, DmSupplFrequency f2 ) {
			return f1.getSupplFreqDesc().compareTo( f2.getSupplFreqDesc() );
		}
	}

}
