package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmMoeProperty;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmDrugCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmDrugCacher extends BaseCacher implements DmDrugCacherInf {

	private String formularyStatus = "1,2,3,4,5,6";
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
		
	private InnerDmDrugCacher dmDrugCacher;

	private Map<WorkstorePK, InnerMsWorkstoreDrugCacher> msWorkstoreDrugCacherMap = new HashMap<WorkstorePK, InnerMsWorkstoreDrugCacher>();	

	private Map<StringArray, List<DmDrug>> displayNameFormSaltFmStatusMap = new HashMap<StringArray, List<DmDrug>>();	

	private Map<StringArray, List<DmDrug>> drugKeyMap = new HashMap<StringArray, List<DmDrug>>();	
	
	private Map<StringArray, List<DmDrug>> drugKeyFmStatusMap = new HashMap<StringArray, List<DmDrug>>();	
	
	private Map<StringArray, List<DmDrug>> displayNameFmStatusMap = new HashMap<StringArray, List<DmDrug>>();
	
	private Map<String, List<DmDrug>> displayNameMap = new HashMap<String, List<DmDrug>>();
	
	private Map<String, List<DmDrug>> diluentCodeMap = new HashMap<String, List<DmDrug>>();

	private List<DmDrug> chestItemList = new ArrayList<DmDrug>();
	
	//internal use
	private List<DmDrug> cachedDmDrugList;
		
	//>> For Test Case
	private List<String> itemCodeList = new ArrayList<String>();

	public List<String> getItemCodeList() {
		return itemCodeList;
	}

	public void setItemCodeList(List<String> itemCodeList) {
		this.itemCodeList = itemCodeList;
	}
	//<< For Test Case

	@Override
	public void clear() {
		this.clearDmDrugCache();
		this.clearMsWorkstoreDrugCache();
	}
	
	public void clearDmDrugCache() {
		synchronized (this) {
			this.getDmDrugCacher().clear();
			cachedDmDrugList = null;
			displayNameFormSaltFmStatusMap.clear();
			drugKeyMap.clear();
			drugKeyFmStatusMap.clear();
			displayNameFmStatusMap.clear();
			displayNameMap.clear();
			diluentCodeMap.clear();
		}
	}
					
	public void clearMsWorkstoreDrugCache() {
		synchronized (this) {
			msWorkstoreDrugCacherMap.clear();
		}
	}

	public void clearMsWorkstoreDrugCache(Workstore workstore) {
		synchronized (this) {
			msWorkstoreDrugCacherMap.remove(workstore.getId());
		}
	}
	
	public void initDmDrugCache(List<DmDrug> dmDrugList) {
		synchronized (this) {
			this.clearDmDrugCache();
			this.initDmDrugList(dmDrugList);
		}
	}
	
	public List<MsWorkstoreDrug> getMsWorkstoreDrugList(Workstore workstore) 
	{
		return (List<MsWorkstoreDrug>) this.getMsWorkstoreDrugCacher(workstore).getAll();
	}
	
	public MsWorkstoreDrug getMsWorkstoreDrug(Workstore workstore, String itemCode) 
	{
		return this.getMsWorkstoreDrugCacher(workstore).get(itemCode);
	}

	public DmDrug getDmDrug(String itemCode) 
	{
		return (DmDrug) this.getDmDrugCacher().get(itemCode);
	}

	public List<DmDrug> getDmDrugList(Workstore workstore, String prefixItemCode) 
	{
		return this.getDmDrugList(workstore, prefixItemCode, null, null);
	}
	
	public List<DmDrug> getDmDrugList(Workstore workstore, String prefixItemCode, Boolean suspend, Boolean fdn) 
	{
		List<DmDrug> ret = new ArrayList<DmDrug>();
		for (DmDrug dmDrug : getDmDrugList()) 
		{
			MsWorkstoreDrug msWorkstoreDrug = getMsWorkstoreDrug(workstore, dmDrug.getItemCode());
			if (dmDrug.getItemCode().startsWith(prefixItemCode) && 
					msWorkstoreDrug != null)
			{
				if (suspend != null) {
					if (( !suspend.booleanValue() == msWorkstoreDrug.isActive() )
							|| (suspend.booleanValue() == msWorkstoreDrug.isInActive())) {
						ret.add(dmDrug);
					}
				} else if (fdn != null) {
					if (( !fdn.booleanValue() == "N".equals(dmDrug.getDmCarsProperty().getFdn()) )							
							|| (fdn.booleanValue() == "Y".equals(dmDrug.getDmCarsProperty().getFdn()))) {
						ret.add(dmDrug);
					}
				} else {
					ret.add(dmDrug);
				}
			}
		}
		return ret;
	}
	
	public List<DmDrug> getDmDrugListByFullDrugDesc(Workstore workstore, String prefixFullDrugDesc) 
	{
		List<DmDrug> ret = new ArrayList<DmDrug>();
		for (DmDrug dmDrug : getDmDrugList()) {
			MsWorkstoreDrug msWorkstoreDrug = getMsWorkstoreDrug(workstore, dmDrug.getItemCode());
			if (msWorkstoreDrug != null && 
					dmDrug.getFullDrugDesc().startsWith(prefixFullDrugDesc)) 
			{
				ret.add(dmDrug);
			}
		}
		return ret;
	}
	
	public List<DmDrug> getDmDrugListBySolvent(Workstore workstore) 
	{
		List<DmDrug> ret = new ArrayList<DmDrug>();		
		for (DmDrug dmDrug : getDmDrugList()) {
			MsWorkstoreDrug msWorkstoreDrug = getMsWorkstoreDrug(workstore, dmDrug.getItemCode());
			if (msWorkstoreDrug != null && 
					dmDrug.getDmMoeProperty().getSolventItem().equals("Y") ) {
				ret.add( dmDrug );
			}			
		}
		return ret;
	}
		
	public List<DmDrug> getDmDrugListByDisplayNameFormSaltFmStatus(Workstore workstore, String displayName, String formCode, String saltProperty, String fmStatus) {		
		this.getDmDrugList();
		return getWorkstoreDmDrugList(
				workstore,
				displayNameFormSaltFmStatusMap.get(new StringArray(
						displayName, 
						formCode, 
						saltProperty, 
						fmStatus)));
	}

	public List<DmDrug> getDmDrugListByDrugKey(Integer drugKey) {		
		this.getDmDrugList();
		return drugKeyMap.get(new StringArray(
					String.valueOf(drugKey)));
	}
	
	public List<DmDrug> getDmDrugListByDrugKey(Workstore workstore, Integer drugKey) {		
		this.getDmDrugList();
		return getWorkstoreDmDrugList(
				workstore,
				drugKeyMap.get(new StringArray(
						String.valueOf(drugKey))));
	}

	public List<DmDrug> getDmDrugListByDrugKeyFmStatus(Workstore workstore, Integer drugKey, String fmStatus) {		
		this.getDmDrugList();
		return getWorkstoreDmDrugList(
				workstore,
				drugKeyFmStatusMap.get(new StringArray(
						String.valueOf(drugKey),
						fmStatus)));
	}
	
	public List<DmDrug> getDmDrugListByDisplayNameFmStatus(Workstore workstore, String displayName, String fmStatus) {		
		this.getDmDrugList();
		return getWorkstoreDmDrugList(
				workstore,
				displayNameFmStatusMap.get(new StringArray(
						displayName,
						fmStatus)));
	}

	public List<DmDrug> getDmDrugListByDisplayName(Workstore workstore, String displayName) {		
		this.getDmDrugList();
		return getWorkstoreDmDrugList(
				workstore,
				displayNameMap.get(displayName));
	}
	
	public List<DmDrug> getDmDrugListByDiluentCode(Workstore workstore, String diluentCode) {		
		this.getDmDrugList();
		return getWorkstoreDmDrugList(
				workstore,
				diluentCodeMap.get(diluentCode));
	}
	
	public List<DmDrug> getChestItemList(Workstore workstore, String prefixItemCode) {		
		this.getDmDrugList();
		List<DmDrug> ret = new ArrayList<DmDrug>();		
		
		if (chestItemList != null ) {
			for (DmDrug dmDrug : chestItemList) {
				MsWorkstoreDrug msWorkstoreDrug = getMsWorkstoreDrug(workstore,dmDrug.getItemCode());
				if (msWorkstoreDrug != null && 
						dmDrug.getItemCode().startsWith(prefixItemCode)) {
					ret.add(dmDrug);
				}
			}
		} 
		return ret;
	}
	
	private List<DmDrug> getWorkstoreDmDrugList(Workstore workstore, List<DmDrug> dmDrugList) {
		List<DmDrug> ret = new ArrayList<DmDrug>();		

		if (dmDrugList != null ) {
			for (DmDrug dmDrug : dmDrugList) {
				MsWorkstoreDrug msWorkstoreDrug = getMsWorkstoreDrug(workstore,dmDrug.getItemCode());
				if (msWorkstoreDrug != null) {
					ret.add(dmDrug);
				}
			}
		} 
		return ret;
	}
	
	public List<DmDrug> getDmDrugList() 
	{
		return initDmDrugList(null);
	}
	
	private List<DmDrug> initDmDrugList(List<DmDrug> dmDrugList) 
	{
		synchronized (this)
		{
			InnerDmDrugCacher innerDmDrugCacher = this.getDmDrugCacher();
			innerDmDrugCacher.setDmDrugCollection(dmDrugList);
			
			List<DmDrug> _dmDrugList = (List<DmDrug>) innerDmDrugCacher.getAll();

			// check whether the Full DmDrugList expire
			if (cachedDmDrugList == null || (cachedDmDrugList != _dmDrugList)) 
			{
				// if yes just clear everything
				msWorkstoreDrugCacherMap.clear();

				// hold the reference
				cachedDmDrugList = _dmDrugList;
				
				this.buildDisplayNameFormSaltFmStatusMap(cachedDmDrugList);
				this.buildDrugKeyMap(cachedDmDrugList);
				this.buildDrugKeyFmStatusMap(cachedDmDrugList);
				this.buildDisplayNameFmStatusMap(cachedDmDrugList);
				this.buildDisplayNameMap(cachedDmDrugList);
				this.buildDiluentCodeMap(cachedDmDrugList);
				this.buildChestItemList(cachedDmDrugList);
			}			
			return cachedDmDrugList;
		}
		
	}
	
	
	private void buildDisplayNameFormSaltFmStatusMap(List<DmDrug> dmDrugList) 
	{
		displayNameFormSaltFmStatusMap.clear();

		for (DmDrug dmDrug : dmDrugList) {
			StringArray key = new StringArray(
					dmDrug.getDmDrugProperty().getDisplayname(), 
					dmDrug.getDmDrugProperty().getFormCode(),
					dmDrug.getDmDrugProperty().getSaltProperty(),
					dmDrug.getPmsFmStatus().getFmStatus());
			List<DmDrug> drugList = displayNameFormSaltFmStatusMap.get(key);
			if (drugList == null) {
				drugList = new ArrayList<DmDrug>();
				displayNameFormSaltFmStatusMap.put(key, drugList);
			}
			drugList.add(dmDrug);
		}
	}

	private void buildDrugKeyMap(List<DmDrug> dmDrugList)
	{
		drugKeyMap.clear();
		for (DmDrug dmDrug : dmDrugList) {
			StringArray key = new StringArray(
					String.valueOf(dmDrug.getDrugKey()));
			List<DmDrug> drugList = drugKeyMap.get(key);
			if (drugList == null) {
				drugList = new ArrayList<DmDrug>();
				drugKeyMap.put(key, drugList);
			}
			drugList.add(dmDrug);
		}
	}

	private void buildDrugKeyFmStatusMap(List<DmDrug> dmDrugList)
	{
		drugKeyFmStatusMap.clear();
		
		for (DmDrug dmDrug : dmDrugList) {
			StringArray key = new StringArray(
					String.valueOf(dmDrug.getDrugKey()), 
					dmDrug.getPmsFmStatus().getFmStatus());
			List<DmDrug> drugList = drugKeyFmStatusMap.get(key);
			if (drugList == null) {
				drugList = new ArrayList<DmDrug>();
				drugKeyFmStatusMap.put(key, drugList);
			}
			drugList.add(dmDrug);
		}
	}
	
	private void buildDisplayNameFmStatusMap(List<DmDrug> dmDrugList)
	{
		displayNameFmStatusMap.clear();
		
		for (DmDrug dmDrug : dmDrugList) {
			StringArray key = new StringArray(
					dmDrug.getDmDrugProperty().getDisplayname(), 
					dmDrug.getPmsFmStatus().getFmStatus());
			List<DmDrug> drugList = displayNameFmStatusMap.get(key);
			if (drugList == null) {
				drugList = new ArrayList<DmDrug>();
				displayNameFmStatusMap.put(key, drugList);
			}
			drugList.add(dmDrug);
		}
	}
	
	private void buildDisplayNameMap(List<DmDrug> dmDrugList)
	{
		displayNameMap.clear();
		
		for (DmDrug dmDrug : dmDrugList) {
			String key = dmDrug.getDmDrugProperty().getDisplayname();
			List<DmDrug> drugList = displayNameMap.get(key);
			if (drugList == null) {
				drugList = new ArrayList<DmDrug>();
				displayNameMap.put(key, drugList);
			}
			drugList.add(dmDrug);
		}
	}
	
	private void buildDiluentCodeMap(List<DmDrug> dmDrugList)
	{
		diluentCodeMap.clear();
		
		for (DmDrug dmDrug : dmDrugList) {
			if (dmDrug.getDmMoeProperty() != null && !StringUtils.isBlank(dmDrug.getDmMoeProperty().getDiluentCode()))
			{
				String key = dmDrug.getDmMoeProperty().getDiluentCode();
				List<DmDrug> drugList = diluentCodeMap.get(key);
				if (drugList == null) {
					drugList = new ArrayList<DmDrug>();
					diluentCodeMap.put(key, drugList);
				}
				
				drugList.add(dmDrug);
			}
		}
	}
	
	private void buildChestItemList(List<DmDrug> dmDrugList){
		chestItemList.clear();
		
		for (DmDrug dmDrug : dmDrugList) {
			DmMoeProperty dmMoeProperty = dmDrug.getDmMoeProperty();
			if ( dmMoeProperty != null && dmMoeProperty.getDrugScope() > 100 && 
					isFormularyItem(dmMoeProperty.getFormularyStatus()) && 
					StringUtils.isNotBlank(dmMoeProperty.getDispenseDosageUnit())) {
				chestItemList.add(dmDrug);
			}
		}
	}
	
	private boolean isFormularyItem(String status){
		String[] formularyStatusArray = formularyStatus.split(",");
		for ( String s : formularyStatusArray ) {
			if ( StringUtils.equals(status, s) ) {
				return true;
			}
		}
		return false;
	}
	
    private InnerMsWorkstoreDrugCacher getMsWorkstoreDrugCacher(Workstore workstore) {
        synchronized (this) {
        	  WorkstorePK key = workstore.getId();
        	  InnerMsWorkstoreDrugCacher cacher = msWorkstoreDrugCacherMap.get(key);
              if (cacher == null) {
                    cacher = new InnerMsWorkstoreDrugCacher(key);
                    msWorkstoreDrugCacherMap.put(key, cacher);
              }
              return cacher;
        }
    }

	private InnerDmDrugCacher getDmDrugCacher() {
		synchronized (this) {
			if (dmDrugCacher == null) {
				dmDrugCacher = new InnerDmDrugCacher(this.getExpireTime());
			}
			return dmDrugCacher;			
		}
	}

	private class InnerDmDrugCacher extends AbstractCacher<String, DmDrug>
	{		
		private Collection<DmDrug> dmDrugList;
		
		public void setDmDrugCollection(Collection<DmDrug> dmDrugList) {
			this.dmDrugList = dmDrugList;
		}
		
		public InnerDmDrugCacher(int expireTime) {
			super(expireTime);
		}

		public DmDrug create(String itemCode) {	
			this.getAll();
			return this.internalGet(itemCode);
		}

		public Collection<DmDrug> createAll() {			
			
			if (dmDrugList != null) {
				Collection<DmDrug> tempDmDrugList = dmDrugList;
				dmDrugList = null;
				return tempDmDrugList;
			} else {
				if (!itemCodeList.isEmpty()) {
					return dmsPmsServiceProxy.retrieveDmDrugListByItemCode(itemCodeList);				
				} else {			
					return dmsPmsServiceProxy.retrieveDmDrugList();
				}
			}
		}

		public String retrieveKey(DmDrug dmDrug) {
			return dmDrug.getItemCode();
		}		
	}
	
	private class InnerMsWorkstoreDrugCacher extends AbstractCacher<String, MsWorkstoreDrug>
	{		
		private WorkstorePK workstorePK = null;
		
		public InnerMsWorkstoreDrugCacher(WorkstorePK workstorePK) {
			super(Integer.MAX_VALUE);
			this.workstorePK = workstorePK;
		}

		public MsWorkstoreDrug create(String itemCode) {	
			this.getAll();
			return this.internalGet(itemCode);
		}

		public Collection<MsWorkstoreDrug> createAll() {	
			
			if (!itemCodeList.isEmpty()) {
				return dmsPmsServiceProxy.retrieveMsWorkstoreDrugListByItemCode(
						workstorePK.getHospCode(), 
						workstorePK.getWorkstoreCode(),
						itemCodeList);
			} else {
				return dmsPmsServiceProxy.retrieveMsWorkstoreDrugList(
					workstorePK.getHospCode(), 
					workstorePK.getWorkstoreCode());
			}
		}

		public String retrieveKey(MsWorkstoreDrug msWorkstoreDrug) {
			return msWorkstoreDrug.getCompId().getItemCode();
		}		
	}	

	public static DmDrugCacherInf instance()
	{
		if (!Contexts.isApplicationContextActive())
		{
			throw new IllegalStateException("No active application scope");
		}
		return (DmDrugCacherInf) Component.getInstance("dmDrugCacher", ScopeType.APPLICATION);
	}
	
}
