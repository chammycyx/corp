package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmAdminTime;

import java.util.List;

public interface DmAdminTimeCacherInf extends BaseCacherInf {
  
    DmAdminTime getDmAdminTimeByAdminTimeCode(String adminTimeCode);
	
	List<DmAdminTime> getDmAdminTimeList();
	
	List<DmAdminTime> getDmAdminTimeListByAdminTimeCode(String prefixAdminTimeCode);
}
