package hk.org.ha.model.pms.corp.pas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("pasServiceWrapper")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class PasServiceWrapper {
	
	@In
	public PasServiceJmsRemote pasServiceProxy;
		
	public Patient retrievePasPatientByHkid(String hkid, String startDatetime, String endDatetime) throws PasException, UnreachableException {
		Patient patient = pasServiceProxy.retrievePasPatientByHkid(hkid, startDatetime, endDatetime);
		checkPatient(patient);
		return patient;
	}

	public Patient retrievePasPatientByPatKey(String patKey, String startDatetime, String endDatetime) throws PasException, UnreachableException {
		Patient patient =  pasServiceProxy.retrievePasPatientByPatKey(patKey, startDatetime, endDatetime);
		checkPatient(patient);
		return patient;
	}

	public Patient retrievePasPatientByCaseNum(String patHospCode, String caseNum) throws PasException, UnreachableException {
		Patient patient =  pasServiceProxy.retrievePasPatientByCaseNum(patHospCode, caseNum);
		checkPatient(patient);
		return patient;
	}
	
	public Patient retrievePasPatientCaseListByHkid(String hospCode, String hkid, String startDate, String endDate) throws PasException, UnreachableException {
		Patient patient =  pasServiceProxy.retrievePasPatientCaseListByHkid(hospCode, hkid, startDate, endDate);
		checkPatient(patient);
		return patient;
	}
	
	private void checkPatient(Patient patient) throws PasException { 
		if (patient == null) {
			throw new PasException(new Exception("No patient found from PAS service."));
		}
	}
}
