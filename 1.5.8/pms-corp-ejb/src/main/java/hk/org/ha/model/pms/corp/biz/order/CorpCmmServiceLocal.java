package hk.org.ha.model.pms.corp.biz.order;

import javax.ejb.Local;

import hk.org.ha.service.biz.pms.corp.interfaces.CorpCmmServiceJmsRemote;

@Local
public interface CorpCmmServiceLocal extends CorpCmmServiceJmsRemote {

}
