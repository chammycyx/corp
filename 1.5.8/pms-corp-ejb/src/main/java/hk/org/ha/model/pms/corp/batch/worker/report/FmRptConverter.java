package hk.org.ha.model.pms.corp.batch.worker.report;

import javax.ejb.Remove;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.vo.report.FmReportCriteria;
import hk.org.ha.model.pms.corp.vo.report.FmReportInfo;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Fluid;
import hk.org.ha.model.pms.vo.rx.RxDrug;

import org.drools.RuleBase;
import org.drools.StatelessSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("fmRptConverter")
@MeasureCalls
public class FmRptConverter implements FmRptConverterInf {
	
	@In(create=true)
	private RuleBase fmReportRxDrugConversionRuleBase;
	
	@In(create=true)
	private RuleBase fmReportCapdRxDrugConversionRuleBase;
	
	@In(create=true)
	private RuleBase fmReportFluidConversionRuleBase;
	
	private transient StatelessSession fmReportRxDrugConversionMemory = null;
	private transient StatelessSession fmReportCapdRxDrugConversionMemory = null;
	private transient StatelessSession fmReportFluidConversionMemory = null;
	
	@Create
	public void create() {
		if (fmReportRxDrugConversionRuleBase != null) {
			fmReportRxDrugConversionMemory = fmReportRxDrugConversionRuleBase.newStatelessSession();
		}
		
		if (fmReportCapdRxDrugConversionRuleBase != null) {
			fmReportCapdRxDrugConversionMemory = fmReportCapdRxDrugConversionRuleBase.newStatelessSession();
		}
		
		if (fmReportFluidConversionRuleBase != null) {
			fmReportFluidConversionMemory = fmReportFluidConversionRuleBase.newStatelessSession();
		}
	}
	
	public void convertFmRxDrugReportData(FmReportInfo fmReportInfo, MedOrderFm medOrderFm, RxDrug rxDrug) {
		FmReportCriteria fmReportCriteria = new FmReportCriteria();
		fmReportCriteria.setFmReportInfo(fmReportInfo);
		fmReportCriteria.setMedOrderFm(medOrderFm);
		fmReportCriteria.setRxDrug(rxDrug);
		fmReportRxDrugConversionMemory.execute(fmReportCriteria);
	}
	
	public void convertFmCapdRxDrugReportData(FmReportInfo fmReportInfo, CapdRxDrug capdRxDrug) {
		FmReportCriteria fmReportCriteria = new FmReportCriteria();
		fmReportCriteria.setFmReportInfo(fmReportInfo);
		fmReportCriteria.setCapdRxDrug(capdRxDrug);
		fmReportCapdRxDrugConversionMemory.execute(fmReportCriteria);
	}
	
	public void convertFmFluidReportData(FmReportInfo fmReportInfo, Fluid fluid) {
		FmReportCriteria fmReportCriteria = new FmReportCriteria();
		fmReportCriteria.setFmReportInfo(fmReportInfo);
		fmReportCriteria.setFluid(fluid);
		fmReportFluidConversionMemory.execute(fmReportCriteria);
	}
	
	@Remove
	public void destroy() 
	{
		if (fmReportRxDrugConversionMemory != null) {
			fmReportRxDrugConversionMemory = null;
		}
		
		if (fmReportCapdRxDrugConversionMemory != null) {
			fmReportCapdRxDrugConversionMemory = null;
		}
		
		if (fmReportFluidConversionMemory != null) {
			fmReportFluidConversionMemory = null;
		}
	}
}
