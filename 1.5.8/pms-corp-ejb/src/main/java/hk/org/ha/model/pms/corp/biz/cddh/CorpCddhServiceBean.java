package hk.org.ha.model.pms.corp.biz.cddh;

import static hk.org.ha.model.pms.corp.prop.Prop.CDDH_EXTRAINFO_SOURCE;
import static hk.org.ha.model.pms.corp.prop.Prop.CDDH_LEGACY_ENQUIRY_ENABLED;
import static hk.org.ha.model.pms.corp.prop.Prop.CDDH_TRXDATE_MIN;
import static hk.org.ha.model.pms.corp.prop.Prop.ORDER_ALLOWUPDATE_DURATION;
import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_ORDER_RECON_ENDRX_RETAIN_DURATION;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmAdminTimeCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDailyFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFormVerbMappingCacherInf;
import hk.org.ha.model.pms.corp.cache.DmFrequencyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSiteCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSupplFrequencyCacherInf;
import hk.org.ha.model.pms.corp.persistence.PatientProfile;
import hk.org.ha.model.pms.corp.util.TimeStamp;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugAliasname;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.DmFrequency;
import hk.org.ha.model.pms.dms.persistence.DmIngredient;
import hk.org.ha.model.pms.dms.persistence.DmMdsIngred;
import hk.org.ha.model.pms.persistence.corp.CddhSpecialtyMapping;
import hk.org.ha.model.pms.persistence.corp.CddhSpecialtyMappingPK;
import hk.org.ha.model.pms.persistence.corp.CddhWorkstoreGroup;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.udt.cddh.ItemDescFilterMode;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.util.StopWatcher;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.model.pms.vo.cddh.PatientCddh;
import hk.org.ha.model.pms.vo.cddh.PatientCddhDispItem;
import hk.org.ha.model.pms.vo.cddh.PatientCddhDose;
import hk.org.ha.model.pms.vo.cddh.PatientCddhDoseGroup;
import hk.org.ha.model.pms.vo.cddh.PatientCddhPharmItem;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhRepServiceJmsRemote;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Session Bean implementation class CorpCddhServiceBean
 */
@AutoCreate
@Stateless
@Name("corpCddhService")
@MeasureCalls
public class CorpCddhServiceBean implements CorpCddhServiceLocal {
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@PersistenceContext(unitName="PMSCOR1_PMS_RPT")
	private EntityManager emReport;
	
	@In
	private DaCddhServiceJmsRemote daCddhServiceProxy;
	
	@In
	private DaCddhRepServiceJmsRemote daCddhRepServiceProxy;
	
	@In
	private DaCddhServiceJmsRemote daCddhService2Proxy;
	
	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@In
	private DmSupplFrequencyCacherInf dmSupplFrequencyCacher;
	
	@In
	private DmFormVerbMappingCacherInf dmFormVerbMappingCacher;
	
	@In
	private DmSiteCacherInf dmSiteCacher;
	
	@In
	private DmAdminTimeCacherInf dmAdminTimeCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmFormCacherInf dmFormCacher;
	
	@In
	private DmFrequencyCacherInf dmFrequencyCacher;
	
	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	private static final int PAT_HOSP_CODE_LENGTH = 3;
	private static final char SPACE = ' ';
	private static final String DOSAGE_UNIT_5ML_SPOONFUL = "5ML SPOONFUL(S)";
	
	public CorpCddhServiceBean() {
    }
    
    public void createLegacyCddhRecordForIp(List<DispOrder> dispOrderList){
    	for (DispOrder dispOrder : dispOrderList) { 
			dispOrder.getPharmOrder();
			dispOrder.getPharmOrder().getMedOrder().getPatient();
			dispOrder.getPharmOrder().getMedCase();
    	}
		daCddhRepServiceProxy.createCddhRecordForIp(dispOrderList);
    }
    
    public void createLegacyCddhRecord(DispOrder dispOrder){
		dispOrder.getPharmOrder();
		dispOrder.getPharmOrder().getMedOrder().getPatient();
		dispOrder.getPharmOrder().getMedCase();
		daCddhRepServiceProxy.createCddhRecord(dispOrder);
    }
    
    public void updateLegacyCddhDispStatus(DispOrder dispOrder, Date prevIssueDate) {
    	daCddhRepServiceProxy.updateDispStatus(dispOrder, prevIssueDate);
    }
    
    public void removeLegacyCddhDispOrder(DispOrder dispOrder) {
		dispOrder.getPharmOrder();
		dispOrder.getPharmOrder().getMedOrder().getPatient();
		dispOrder.getPharmOrder().getMedCase();
		daCddhRepServiceProxy.removeOutpatDispOrder(dispOrder);
		
		auditLogger.log("#4027 CDDH|Delete Order|HospCode [#0]|DispOrderNum [#1]|DispDate [#2]|DispOrderId [#3]|", dispOrder.getWorkstore().getHospCode(), dispOrder.getDispOrderNum(), dispOrder.getDispDate(), dispOrder.getId());
    }
    
    @SuppressWarnings("unchecked")
	public List<DispOrder> retrieveDispOrderLikePatName(CddhCriteria cddhCriteria){
    	List<DispOrder> dispOrderList = new ArrayList<DispOrder>();
    	
    	if(StringUtils.isNotBlank(cddhCriteria.getPatientName())){

			List<Patient> pasPatList = cddhCriteria.getPatList(); // from cluster
			
	    	logger.info("CDDH retrieve by patient name: patName=#0", cddhCriteria.getPatientName());
			
			for (Patient patient: pasPatList) {

				List<DispOrder> patientDispOrderList = emReport.createQuery(
						"select o from DispOrder o" + // 20150422 index check : Patient.patKey : I_PATIENT_01
						" where o.patient.patKey = :patKey" +
						" and o.status = :status")
						.setParameter("patKey", patient.getPatKey())
						.setParameter("status", DispOrderStatus.Issued)
						.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient")
						.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder")
						.setHint(QueryHints.BATCH, "o.workstore.hospital.hospitalCluster")
						.getResultList();
				
				if (logger.isDebugEnabled()) {
					logger.debug("retrieveDispOrderLikePatName: patDispOrderListSize=#0", patientDispOrderList.size());
				}
	    		
				for (DispOrder dispOrder: patientDispOrderList) {
					PharmOrder pharmOrder = dispOrder.getPharmOrder();
					dispOrder.getTicket();
					dispOrder.getWorkstore().getHospital().getHospitalCluster();
					pharmOrder.getMedCase();
					pharmOrder.getMedOrder().getPatient();
				}
				
				emReport.clear();

				// reset patient info provided by opas
				for (DispOrder dispOrder: patientDispOrderList) {
					Patient doPatient = dispOrder.getPharmOrder().getMedOrder().getPatient();
					doPatient.setName(patient.getName());
					doPatient.setDob(patient.getDob());
					doPatient.setPhone(patient.getPhone());
					doPatient.setPatKey(patient.getPatKey());
					doPatient.setHkid(StringUtils.trim(patient.getHkid()));
				}
				
				dispOrderList.addAll(patientDispOrderList);
			}
			
			if (cddhCriteria.isLegacyCddhIncluded()) {
				List<DispOrder> legacyDispOrderList = daCddhServiceProxy.retrieveDispOrderLikePatName(cddhCriteria, pasPatList);
				dispOrderList.addAll(legacyDispOrderList);
				if (logger.isDebugEnabled()) {
					logger.debug("retrieveDispOrderLikePatName: legacyDispOrderListSize=#0", legacyDispOrderList.size());
				}
			}

    	} else if(StringUtils.isNotBlank(cddhCriteria.getNameOnLabel())){
    		
	    	Date sqlDobEnd = null;
	    	if(cddhCriteria.getLabelDob() != null){
	    		sqlDobEnd = (new DateTime(cddhCriteria.getLabelDob())).plusDays(1).toDate();
	    	}
	    	
	    	Date sqlDispDateEnd = null;
	    	if(cddhCriteria.getLabelDispDateEnd() != null){
	    		sqlDispDateEnd = (new DateTime(cddhCriteria.getLabelDispDateEnd())).plusDays(1).toDate();
	    	}
	    	
	    	if (StringUtils.isBlank(cddhCriteria.getLabelHospcode())){
	    	    cddhCriteria.setLabelHospcode(null);
	    	}

	    	Date createDateStart = cddhCriteria.getLabelDispDateStart() == null ? null : new DateMidnight(cddhCriteria.getLabelDispDateStart()).minusDays(ORDER_ALLOWUPDATE_DURATION.get()).toDate();
	    	Date createDateEnd = sqlDispDateEnd == null ? null : new DateMidnight(sqlDispDateEnd).plusDays(ORDER_ALLOWUPDATE_DURATION.get() + 1).toDate();
	    	
	    	logger.info("CDDH retrieve by name on label: nameOnLabel=#0,dobStart=#1,dobEnd=#2,dispDateStart=#3,dispDateEnd=#4", cddhCriteria.getNameOnLabel(), cddhCriteria.getLabelDob(), sqlDobEnd, cddhCriteria.getLabelDispDateStart(), sqlDispDateEnd);
	    	
	    	StringBuilder sql = new StringBuilder();
	    	sql.append("select o from DispOrder o"); // 20120313 index check : PharmOrder.name : I_PHARM_ORDER_02
	    	sql.append(" where o.status = :status");
	    	sql.append(" and o.pharmOrder.name = :patName");
	    	if (cddhCriteria.getLabelHospcode() != null) {
		    	sql.append(" and o.workstore.hospCode = :hospCode");
	    	}
	    	if (cddhCriteria.getLabelDob() != null) {
		    	sql.append(" and o.pharmOrder.medOrder.patient.dob >= :dobStart and o.pharmOrder.medOrder.patient.dob < :dobEnd");
	    	}
	    	if (cddhCriteria.getLabelDispDateStart() != null) {
	    		sql.append(" and o.issueDate >= :dispDateStart and o.createDate >= :createDateStart");
	    	}
	    	if (sqlDispDateEnd != null) {
	    		sql.append(" and o.issueDate < :dispDateEnd and o.createDate < :createDateEnd");
	    	}
	    	
	    	Query query = emReport.createQuery(sql.toString());
			query.setParameter("status", DispOrderStatus.Issued);
	    	query.setParameter("patName", cddhCriteria.getNameOnLabel());
	    	if (cddhCriteria.getLabelHospcode() != null) {
				query.setParameter("hospCode", cddhCriteria.getLabelHospcode());
	    	}
	    	if (cddhCriteria.getLabelDob() != null) {
		    	query.setParameter("dobStart", cddhCriteria.getLabelDob());
				query.setParameter("dobEnd", sqlDobEnd);
	    	}
	    	if (cddhCriteria.getLabelDispDateStart() != null) {
				query.setParameter("dispDateStart", cddhCriteria.getLabelDispDateStart());
				query.setParameter("createDateStart", createDateStart);
	    	}
	    	if (sqlDispDateEnd != null) {
	    		query.setParameter("dispDateEnd", sqlDispDateEnd);
	    		query.setParameter("createDateEnd", createDateEnd);
	    	}
			query.setHint(QueryHints.BATCH, "o.ticket");
			query.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient");
			query.setHint(QueryHints.BATCH, "o.pharmOrder.medCase");
			dispOrderList = query.getResultList();
	    	
	    	for(DispOrder dispOrder: dispOrderList){
				dispOrder.getTicket();
				dispOrder.getPharmOrder().getMedCase();
				MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
				Patient moPatient = medOrder.getPatient();
				
				// for revamped data only
				if (StringUtils.isBlank(moPatient.getPatKey())) {
					dispOrder.setCddhExceptionFlag(Boolean.TRUE);
				}
			}
	    	
	    	if (cddhCriteria.isLegacyCddhIncluded()) {
				// update start disp date and end disp date if not specified (include display all)
		    	if (cddhCriteria.getLabelDispDateStart() == null) {
		    		cddhCriteria.setLabelDispDateStart(CDDH_TRXDATE_MIN.get().toDate());
		    	}
		    	if (cddhCriteria.getLabelDispDateEnd() == null) {
		    		cddhCriteria.setLabelDispDateEnd(DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH));
		    	}
		    	
	    		List<DispOrder> legacyDispOrder = daCddhServiceProxy.retrieveDispOrderLikePatName(cddhCriteria, new ArrayList<Patient>());
	    		dispOrderList.addAll(legacyDispOrder);
	    	}
    	}
		
    	return dispOrderList;
    }
    
	private boolean proxy2ServiceRequired(Date startDate, Date endDate) {
		if (startDate == null) {
			return true;
			
		} else {
			DateTime start = new DateTime(startDate.getTime());
			DateTime end;
			
			if (endDate == null) {
				end = new DateTime(new Date().getTime());
			} else {
				end = new DateTime(endDate.getTime());
			}
			
			Months months = Months.monthsBetween(start, end);
			
			if (months.getMonths() > 12) {
				return true;
			} else { 
				return false;
			}
		}
	}
    
	private List<DispOrderItem> retrieveDispOrderItemFromPmsRevamp(CddhCriteria cddhCriteria) {
		if (cddhCriteria.isExcept()) {
    		return retrieveExceptionDispOrderItem(cddhCriteria);
    	} else {
    		return retrieveDispOrderItemByPatKey(cddhCriteria);
    	}
	}
	
	private List<DispOrderItem> retrieveDispOrderItemFromLegacy(CddhCriteria cddhCriteria) {
		if (cddhCriteria.isLegacyCddhIncluded()) {
			if ( ! cddhCriteria.isExcept()) {
				// update start disp date and end disp date if not specified (include display all)
		    	if (cddhCriteria.getDispDateStart() == null) {
		    		cddhCriteria.setDispDateStart(CDDH_TRXDATE_MIN.get().toDate());
		    	}
		    	if (cddhCriteria.getDispDateEnd() == null) {
		    		cddhCriteria.setDispDateEnd(DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH));
		    	}
			}
			
    		if (proxy2ServiceRequired(cddhCriteria.getDispDateStart(), cddhCriteria.getDispDateEnd())) {
    			return daCddhService2Proxy.retrieveDaDispOrderItem(cddhCriteria);
    		} else {
    			return daCddhServiceProxy.retrieveDaDispOrderItem(cddhCriteria);
    		}
    	} else {
    		return new ArrayList<DispOrderItem>();
    	}
	}
	
	private void setLegacyIpRegimenDoseGroupProperty(List<DoseGroup> doseGroupList) {
		if (doseGroupList.size() > 1) {
			for (DoseGroup doseGroup: doseGroupList) {
				Dose dose = doseGroup.getDoseList().get(0);
				DmFrequency dmFrequency = dmFrequencyCacher.getDmFrequencyByFreqCode(dose.getDailyFreq().getCode());
				if (dmFrequency != null) {
					switch(dmFrequency.getPeriod()) {
					case 1:
						doseGroup.setDurationUnit(RegimenDurationUnit.Day);
						break;
					case 7:
						doseGroup.setDurationUnit(RegimenDurationUnit.Week);
						break;
					case 28:
						doseGroup.setDurationUnit(RegimenDurationUnit.Month);
						break;
					}
				}
			}
		}
	}
	
	private void setLegacyIpDoseProperty(Dose dose, DmForm dmForm, DmFrequency dmFrequency, String itemCatCode, Integer duration, boolean multiDose) {
		if (dmForm != null) {
			dose.setFormVerb(dmForm.getVerb());
		} else {
			dose.setFormVerb(StringUtils.EMPTY);
		}
		
		if (!multiDose) {
			String dmFreqDesc = StringUtils.EMPTY;
			if (dmFrequency != null) {
				dmFreqDesc = dmFrequency.getFreqDescEng();
			}
			
			String dbfreqDesc = dose.getDmDailyFrequency().getLabelFreqBlk1Eng();
			
			dose.getDmDailyFrequency().setLabelFreqBlk1Eng(dmFreqDesc);
			
			if (!"PN".equals(itemCatCode) && !"CI".equals(itemCatCode) && !"CY".equals(itemCatCode)) {
				if (dmForm != null) {
					if (DOSAGE_UNIT_5ML_SPOONFUL.equals(dmForm.getNoun())) {
						dose.setDosageUnit("X " + dmForm.getNoun());
					} else {
						dose.setDosageUnit(dmForm.getNoun());
					}
				} else {
					dose.setDosageUnit(StringUtils.EMPTY);
				}
				
				// reset a new dosage and freq desc
				if (StringUtils.isNotBlank(dbfreqDesc) && dbfreqDesc.length() >= 14 && " on".equals(dbfreqDesc.substring(0, 3))) {
					// recalculate dosage
					if (dmForm != null && dose.getLegacyDosage() != null && duration > 0) { // skip checking dmForm != null?
						BigDecimal newDosage = new BigDecimal(dose.getLegacyDosage()).divide(new BigDecimal(duration), 4, RoundingMode.HALF_UP);
						DecimalFormat decimalFormat = new DecimalFormat("#######.####");
						dose.setLegacyDosage(decimalFormat.format(newDosage));
					}
					
					DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");					
					
					// adjust freq desc
					DateTime endDate = dateFormatter.parseDateTime(dbfreqDesc.substring(4)).plusDays(duration.intValue() - 1);
					String newFreqDesc = new StringBuilder(dmFreqDesc)
												.append(" from")
												.append(SPACE)
												.append(dbfreqDesc.substring(4, 14))
												.append(SPACE)
												.append("to")
												.append(SPACE)
												.append(dateFormatter.print(endDate))
												.toString();
					
					dose.getDmDailyFrequency().setLabelFreqBlk1Eng(newFreqDesc);
				}
			}
		} else {
			if (dmForm != null) {
				if (DOSAGE_UNIT_5ML_SPOONFUL.equals(dmForm.getNoun())) {
					dose.setDosageUnit("X " + dmForm.getNoun());
				} else {
					dose.setDosageUnit(dmForm.getNoun());
				}
			} else {
				dose.setDosageUnit(StringUtils.EMPTY);
			}
		}
	}
	
	private Integer setLegacyIpRegimenProperty(Regimen regimen, String formCode, String itemCatCode, boolean multiDose) {
		DmForm dmForm = dmFormCacher.getFormByFormCodeByAllUsageType(formCode);
		List<DoseGroup> doseGroupList = regimen.getDoseGroupList();
		
		// determine regimen duration unit
		boolean samePeriod = true;
		Integer period = null;
		
		for (DoseGroup doseGroup: doseGroupList) {
			for (Dose dose: doseGroup.getDoseList()) {
				DmFrequency dmFrequency = dmFrequencyCacher.getDmFrequencyByFreqCode(dose.getDailyFreq().getCode());
				setLegacyIpDoseProperty (dose, dmForm, dmFrequency, itemCatCode, regimen.getDuration(), multiDose);
				
				if (dmFrequency == null) {
					samePeriod = false;
				} else if (period == null) {
					period = dmFrequency.getPeriod();
				} else {
					if (period.compareTo(dmFrequency.getPeriod()) != 0) {
						samePeriod = false;
					}
				}
			}
		}
		
		if (period == null) {
			samePeriod = false;
		}
		
		if (samePeriod) {
			switch(period) {
			case 1:
				regimen.setDurationUnit(RegimenDurationUnit.Day);
				break;
			case 7:
				regimen.setDurationUnit(RegimenDurationUnit.Week);
				break;
			case 28:
			case 30:
				regimen.setDurationUnit(RegimenDurationUnit.Month);
			}
		} else {
			regimen.setDurationUnit(null);
			period = null;
		}
		
		// set duration unit for each dose group for step up down
		setLegacyIpRegimenDoseGroupProperty(regimen.getDoseGroupList());
		
		return period;
	}
	
	private void setLegacyIpDispOrderItemProperty(DispOrderItem dispOrderItem) {
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		Regimen regimen = pharmOrderItem.getRegimen();
		
		// item desc
		DmForm dmForm = dmFormCacher.getFormByFormCodeByAllUsageType(pharmOrderItem.getFormCode());
		if (dmForm != null) {
			pharmOrderItem.setFormLabelDesc(dmForm.getFormDescEng());
		}
		
		dispOrderItem.setCddhDrugDesc(DmDrug.buildFullDrugDesc(
				pharmOrderItem.getDrugName(), 
				pharmOrderItem.getFormCode(), 
				pharmOrderItem.getFormLabelDesc(), 
				pharmOrderItem.getStrength(), 
				pharmOrderItem.getVolumeText()));
		
		// instruction text
		Integer period = setLegacyIpRegimenProperty(
				regimen, 
				pharmOrderItem.getFormCode(),
				dispOrderItem.getItemCatCode(),
				dispOrderItem.isLegacyMultiDose());
		
		if (!dispOrderItem.isLegacyMultiDose()) {
			Dose dose = regimen.getDoseGroupList().get(0).getDoseList().get(0);
			DmFrequency dmFrequency = dmFrequencyCacher.getDmFrequencyByFreqCode(dose.getDailyFreq().getCode());

			if (dmFrequency == null && dose.getLegacyDosage() != null) {
				dispOrderItem.setSingleDosageValue(StringUtils.EMPTY);
			} else {
				List<String> dosageTextList = new ArrayList<String>();
				dosageTextList.add(dose.getFormVerb());
				if (dose.getLegacyDosage() != null) {
					dosageTextList.add(dose.getLegacyDosage());
					dosageTextList.add(dose.getDosageUnit());
				}
				dosageTextList.add(dose.getDmDailyFrequency().getLabelFreqBlk1Eng());
				dosageTextList.remove(StringUtils.EMPTY);
				
				dispOrderItem.setSingleDosageValue(StringUtils.join(dosageTextList.toArray(), SPACE));
			}
		}
		
		// set end date by default
		dispOrderItem.setEndDate(dispOrderItem.getDispOrder().getIssueDate());
		if (!dispOrderItem.isLegacyMultiDose() && period != null) {
			DateTime startDate = new DateTime(dispOrderItem.getStartDate());
			dispOrderItem.setEndDate(startDate.plusDays(regimen.getDuration() * period - 1).toDate());
		}
	}
	
	private List<DispOrderItem> filterDispOrderItemList(List<DispOrderItem> dispOrderItemList, CddhCriteria cddhCriteria) {
		List<DispOrderItem> dispOrderItemFilterList = new ArrayList<DispOrderItem>();
		
		for (DispOrderItem dispOrderItem: dispOrderItemList) {
			boolean filter = false;
			
			if (dispOrderItem.isLegacy()) {
				DmDrug dmDrug = dmDrugCacher.getDmDrug(dispOrderItem.getPharmOrderItem().getItemCode());
				
				if (cddhCriteria.getDangerDrugFlag() != YesNoBlankFlag.Blank) {
					if (!StringUtils.equals(cddhCriteria.getDangerDrugFlag().getDataValue(), dmDrug.getDangerousDrug())) {
						filter = true;
					}
				}
			}
			
			// filter by drug desc/ingredient
			if (StringUtils.isNotBlank(cddhCriteria.getItemDesc())) {
				DmDrug dmDrug = dmDrugCacher.getDmDrug(dispOrderItem.getPharmOrderItem().getItemCode());
				DmDrugProperty dmDrugProperty = dmDrug.getDmDrugProperty();
				List<DmMdsIngred> dmMdsIngredList = dmDrug.getDmMdsIngredList();
				List<DmDrugAliasname> dmDrugAliasnameList = dmDrug.getDmDrugAliasnames();
				
				if (cddhCriteria.getItemDescFilterMode() == ItemDescFilterMode.Contain) {
					boolean containDesc = false;
					if (StringUtils.contains(dmDrug.getDrugName(), cddhCriteria.getItemDesc())) {
						containDesc = true;
					} 
					
					if (dmDrugProperty != null) {
						if (StringUtils.contains(dmDrugProperty.getDisplayname(), cddhCriteria.getItemDesc())) {
							containDesc = true;
						}
					}
					
					if (dmMdsIngredList != null) {
						for (DmMdsIngred dmMdsIngred: dmMdsIngredList) {
							DmIngredient dmIngred = dmMdsIngred.getDmIngredient();
							if (dmIngred != null) {
								if (StringUtils.contains(dmIngred.getIngredientDesc(), cddhCriteria.getItemDesc())) {
									containDesc = true;
								}
							}
						}
					}
					
					if (dmDrugAliasnameList != null) {
						for (DmDrugAliasname dmDrugAliasname: dmDrugAliasnameList) {
							if (StringUtils.contains(dmDrugAliasname.getCompId().getAliasname(), cddhCriteria.getItemDesc())) {
								containDesc = true;
							}
						}
					}
					
					if (!containDesc) {
						filter = true;
					}
				}
				
				if (cddhCriteria.getItemDescFilterMode() == ItemDescFilterMode.BeginWith) {
					boolean containDesc = false;
					if (dmDrug.getDrugName() != null && dmDrug.getDrugName().startsWith(cddhCriteria.getItemDesc())) {
						containDesc = true;
					} else if (dmDrugProperty != null) {
						if (dmDrugProperty.getDisplayname() != null && dmDrugProperty.getDisplayname().startsWith(cddhCriteria.getItemDesc())) {
							containDesc = true;
						}
					} else if (dmMdsIngredList != null) {
						for (DmMdsIngred dmMdsIngred: dmMdsIngredList) {
							DmIngredient dmIngred = dmMdsIngred.getDmIngredient();
							if (dmIngred != null) {
								if (dmIngred.getIngredientDesc() != null && dmIngred.getIngredientDesc().startsWith(cddhCriteria.getItemDesc())) {
									containDesc = true;
								}
							}
						}
					} else if (dmDrugAliasnameList != null) {
						for (DmDrugAliasname dmDrugAliasname: dmDrugAliasnameList) {
							if (dmDrugAliasname.getCompId().getAliasname() != null && dmDrugAliasname.getCompId().getAliasname().startsWith(cddhCriteria.getItemDesc())) {
								containDesc = true;
							}
						}
					}
					
					if (!containDesc) {
						filter = true;
					}
				}
			}
			
			if (filter) {
				dispOrderItemFilterList.add(dispOrderItem);
			}
		}
		
		dispOrderItemList.removeAll(dispOrderItemFilterList);
		return dispOrderItemFilterList;
	}
	
	private void constructDispOrderItemList(List<DispOrderItem> dispOrderItemList, CddhCriteria cddhCriteria, TimeStamp timeStamp) {
		// cache CddhSpecialtyMapping query result, especially for null CddhSpecialtyMapping case 
		Map<CddhSpecialtyMappingPK, CddhSpecialtyMapping> cddhSpecialtyMappingMap = new HashMap<CddhSpecialtyMappingPK, CddhSpecialtyMapping>();

		// cache Workstore query result, especially for null Workstore case 
		Map<WorkstorePK, Workstore> workstoreMap = new HashMap<WorkstorePK, Workstore>();
		
		int i = 0;
		
		for(DispOrderItem dispOrderItem: dispOrderItemList){
			BaseLabel baseLabel = dispOrderItem.getBaseLabel();
			if (baseLabel != null) {
				PrintOption printOption = new PrintOption();
				printOption.setPrintLang(PrintLang.Eng);
				baseLabel.setPrintOption(printOption);
			}
			
    		DispOrder dispOrder = dispOrderItem.getDispOrder();
    		dispOrder.getTicket();
    		
    		Workstore doWorkstore = dispOrder.getWorkstore();
    		doWorkstore.getCddhWorkstoreGroup();
    		
    		PharmOrder pharmOrder = dispOrder.getPharmOrder();
			pharmOrder.getMedCase();
			MedOrder medOrder = pharmOrder.getMedOrder();
			medOrder.getPatient();
			
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
			DmDrug poiDmDrug = pharmOrderItem.getDmDrug();
			Regimen regimen = pharmOrderItem.getRegimen();
			pharmOrderItem.getMedOrderItem();
			
			CddhSpecialtyMapping cddhSpecialtyMapping = null;
			if ( ! StringUtils.isBlank(dispOrder.getSpecCode()) ) {
				CddhSpecialtyMappingPK cddhSpecialtyMappingPK = new CddhSpecialtyMappingPK(doWorkstore.getHospCode(), dispOrder.getSpecCode());				
				if ( cddhSpecialtyMappingMap.containsKey(cddhSpecialtyMappingPK) ) {
					cddhSpecialtyMapping = cddhSpecialtyMappingMap.get(cddhSpecialtyMappingPK);
				} else {
					cddhSpecialtyMapping = emReport.find(CddhSpecialtyMapping.class, cddhSpecialtyMappingPK);
					if (cddhSpecialtyMapping != null && cddhSpecialtyMapping.getStatus() == RecordStatus.Active ) {
						cddhSpecialtyMappingMap.put(cddhSpecialtyMappingPK, cddhSpecialtyMapping);
					} else {
						cddhSpecialtyMapping = null;
					}
				}
				if (cddhSpecialtyMapping != null) {
					dispOrder.setCddhSpecCode(cddhSpecialtyMapping.getCddhSpecCode());
				}
			}
			dispOrder.setCddhSpecialtyMapping(cddhSpecialtyMapping);
			
			if (dispOrderItem.isLegacy()) {
				DmDrug dmDrug = dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode());
				if (dmDrug != null) {
					poiDmDrug.setCommodityGroup(dmDrug.getCommodityGroup());
					poiDmDrug.setCommodityType(dmDrug.getCommodityType());
				}
				
				if (medOrder.getOrderType() == OrderType.InPatient) {
					setLegacyIpDispOrderItemProperty(dispOrderItem);
				} // legacy inpat end
				
				if (regimen.getDoseGroupList().isEmpty()) {
					DoseGroup doseGroup = new DoseGroup();
					Dose dose = new Dose();
					dose.setDailyFreq(new Freq());
					
					doseGroup.getDoseList().add(dose);
					regimen.getDoseGroupList().add(doseGroup);
				}
				
				// retrieve hospital display code
				Workstore revampWorkstore = null;
				WorkstorePK workstorePK = new WorkstorePK(doWorkstore.getHospCode(), doWorkstore.getWorkstoreCode());				
				if ( workstoreMap.containsKey(workstorePK) ) {
					revampWorkstore = workstoreMap.get(workstorePK);
				} else {
					revampWorkstore = emReport.find(Workstore.class, workstorePK);			
					workstoreMap.put(workstorePK, revampWorkstore);
				}
				if (revampWorkstore != null) {
					doWorkstore.setCddhWorkstoreGroup(revampWorkstore.getCddhWorkstoreGroup());
				}
				
			} else {
				dispOrderItem.setItemCatCode(getItemCatCode(
						medOrder.getOrderType(), 
						pharmOrderItem.getCapdVoucherFlag(), 
						pharmOrderItem.getWardStockFlag(), 
						pharmOrderItem.getDangerDrugFlag(), 
						dispOrderItem.getReDispFlag()));
				
				if (baseLabel != null) {
					if (StringUtils.isNotBlank(baseLabel.getItemDesc()) && baseLabel.getItemDesc().contains(DOSAGE_UNIT_5ML_SPOONFUL)) {
						baseLabel.setItemDesc(StringUtils.replace(baseLabel.getItemDesc(), DOSAGE_UNIT_5ML_SPOONFUL, "X " + DOSAGE_UNIT_5ML_SPOONFUL));
					}
					
					dispOrderItem.setCddhDrugDesc(baseLabel.getItemDesc());
				}
				
				dispOrderItem.setEpisodeType(CDDH_EXTRAINFO_SOURCE.get());

				if (regimen != null) {
					if (regimen.getDoseGroupList().size() > 1 || regimen.getDoseGroupList().get(0).getDoseList().size() > 1) {
						dispOrderItem.setSingleDosageValue("With Multiple Dosage Instruction");
					} else {
						if (baseLabel != null) {
							dispOrderItem.setSingleDosageValue(baseLabel.getInstructionText());
						}
					}
				}
			}
			
			// access right check part 1
			if (dispOrder.getCddhRemarkStatus() == null) {
				String displayHospCode = doWorkstore.getHospCode();
				if (doWorkstore.getCddhWorkstoreGroup() != null) {
					displayHospCode = doWorkstore.getCddhWorkstoreGroup().getCddhWorkstoreGroupCode();
				}
				dispOrder.setCddhRemarkStatus(checkDispOrderItemRemarkAccess(cddhCriteria.getLoginHospCode(), displayHospCode));
			}
			
			if (++i % 100 == 0) {
				timeStamp.stampTime("after " + i + " item processed");			
			}
		}
		
		timeStamp.stampTime("total " + i + " item completed");			
		
		filterDispOrderItemList(dispOrderItemList, cddhCriteria);
		
		timeStamp.stampTime("after filter");
	}
	
	private String getItemCatCode(OrderType orderType, Boolean capdVoucherFlag, Boolean wardStockFlag, Boolean dangerDrugFlag, Boolean reDispFlag) {
		switch (orderType) {
		case OutPatient:
			if (capdVoucherFlag) {
				return "CA";
			} else {
				return "N";
			}
		case InPatient:
			if (dangerDrugFlag && reDispFlag) {
				return "DR";
			} else if (dangerDrugFlag && !reDispFlag) {
				return "DD";
			} else if (wardStockFlag && !dangerDrugFlag && reDispFlag) {
				return "WR";
			} else if (wardStockFlag && !dangerDrugFlag && !reDispFlag) {
				return "W";
			} else if (!wardStockFlag && !dangerDrugFlag && !reDispFlag) {
				return "N";
			}
		}
		return StringUtils.EMPTY;
	}
	
	public List<DispOrderItem> retrieveDispOrderItem(CddhCriteria cddhCriteria) {
    	if( cddhCriteria.getRetrieveFromPmsFlag() ){
    		DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    		String fromDate = null;
    		if( cddhCriteria.getDispDateStart() != null ){
    			fromDate = dateFormatter.print(cddhCriteria.getDispDateStart().getTime());
    		}
    		
    		String toDate = null;
    		if( cddhCriteria.getDispDateEnd() != null ){
    			toDate = dateFormatter.print(cddhCriteria.getDispDateEnd().getTime());
    		}
    		
    		auditLogger.log("#4028 {methodName:\"retrieveDispOrderItem"
					+"\", hospCode:\""+cddhCriteria.getLoginHospCode()
	    			+"\", workstoreCode:\""+cddhCriteria.getLoginWorkstoreCode()
	    			+"\", userCode:\""+cddhCriteria.getUserCode()
	    			+"\", workstationCode:\""+cddhCriteria.getWorkstationCode()
	    			+"\", dispDateFr:\""+fromDate
	    			+"\", dispDateTo:\""+toDate	    			
	    			+"\", patKey:\""+cddhCriteria.getPatKey()
	    			+"\", caseNum:\""+cddhCriteria.getCaseNum()
	    			+"\", enquiryType:\"E"
	    			+"\", enquirySystem:\"PMS\"}");
    	}
		
    	List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
    	
    	TimeStamp timeStamp = new TimeStamp("CorpCddhServiceBean.retrieveDispOrderItem");
    	
    	if (StopWatcher.isDebugEnabled()) {
        	StopWatcher.reset();
    		StopWatcher.start();
    		StopWatcher.suspend();
    	}
    	
    	dispOrderItemList.addAll(retrieveDispOrderItemFromPmsRevamp(cddhCriteria));
    	
    	timeStamp.stampTime("after retrieve revamp");
    	
    	dispOrderItemList.addAll(retrieveDispOrderItemFromLegacy(cddhCriteria));
    	
    	timeStamp.stampTime("after retrieve legacy");
    	
    	constructDispOrderItemList(dispOrderItemList, cddhCriteria, timeStamp);
    	
    	timeStamp.stampTime("after conversion");

		// avoid sending xml string to pms-pms for performance
    	emReport.clear();
    	for (DispOrderItem dispOrderItem : dispOrderItemList) {
    		dispOrderItem.getPharmOrderItem().getMedOrderItem().setRxItemXml(null);
    		dispOrderItem.getPharmOrderItem().setRegimenXml(null);
    		dispOrderItem.setLabelXml(null);
    	}
    	
    	timeStamp.stampTime("after remove xml");
    	
    	timeStamp.writeLog(logger);

    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.stop();
        	logger.info("CDDH retrieve total post load time: patKey=#0,dispDateStart=#1,dispDateEnd=#2,postLoadTime=#3", cddhCriteria.getPatKey(), cddhCriteria.getDispDateStart(), cddhCriteria.getDispDateEnd(), StopWatcher.getTime());
    	}
    	
    	return dispOrderItemList;
    }
    
    @SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveDispOrderItemByPatKey(CddhCriteria cddhCriteria){
		// reset the dispensed end date for sql use
    	Date sqlDispDateEnd = null;
    	if(cddhCriteria.getDispDateEnd() != null){
    		sqlDispDateEnd = (new DateTime(cddhCriteria.getDispDateEnd())).plusDays(1).toDate();
    	}
    	
    	if(StringUtils.isBlank(cddhCriteria.getItemCode())){
    		cddhCriteria.setItemCode(null);
    	}
    	
    	Boolean dangerDrugFlag = null;
    	if(cddhCriteria.getDangerDrugFlag() != null){
	    	if(cddhCriteria.getDangerDrugFlag().equals(YesNoBlankFlag.Yes)){
	    		dangerDrugFlag = true;
	    	} else if(cddhCriteria.getDangerDrugFlag().equals(YesNoBlankFlag.No)){
	    		dangerDrugFlag = false;
	    	}
    	}
    	
    	Date createDateStart = cddhCriteria.getDispDateStart() == null ? null : new DateMidnight(cddhCriteria.getDispDateStart()).minusDays(ORDER_ALLOWUPDATE_DURATION.get()).toDate();
    	Date createDateEnd = sqlDispDateEnd == null ? null : new DateMidnight(sqlDispDateEnd).plusDays(ORDER_ALLOWUPDATE_DURATION.get()).toDate();
    	
    	logger.info("CDDH retrieve by patient key: patKey=#0,dispDateStart=#1,dispDateEnd=#2", cddhCriteria.getPatKey(), cddhCriteria.getDispDateStart(), sqlDispDateEnd);

    	StringBuilder sql = new StringBuilder();
    	sql.append("select o from DispOrderItem o"); // 20171024 index check : DispOrder.patient,issueDate : I_DISP_ORDER_18
    	sql.append(" where o.dispOrder.patient.patKey = :patKey");
    	sql.append(" and o.dispOrder.status = :status");
    	sql.append(" and o.dispOrder.adminStatus <> :adminStatus");
    	if (cddhCriteria.getDispDateStart() != null) {
    		sql.append(" and o.dispOrder.issueDate >= :dispDateStart and o.dispOrder.createDate >= :createDateStart and o.createDate >= :createDateStart");
    	}
    	if (sqlDispDateEnd != null) {
    		sql.append(" and o.dispOrder.issueDate < :dispDateEnd and o.dispOrder.createDate < :createDateEnd and o.createDate < :createDateEnd");
    	}
    	if (cddhCriteria.getItemCode() != null) {
    		sql.append(" and o.pharmOrderItem.itemCode = :itemCode");
    	}
    	if (dangerDrugFlag != null) {
    		sql.append(" and o.pharmOrderItem.dangerDrugFlag = :dangerDrugFlag");
    	}
    	
		Query query = emReport.createQuery(sql.toString());
		query.setParameter("patKey", cddhCriteria.getPatKey());
		query.setParameter("status", DispOrderStatus.Issued);
		query.setParameter("adminStatus", DispOrderAdminStatus.Suspended);
		
		if (cddhCriteria.getDispDateStart() != null) {
			query.setParameter("dispDateStart", cddhCriteria.getDispDateStart());
			query.setParameter("createDateStart", createDateStart);
		}
    	if (sqlDispDateEnd != null) {
    		query.setParameter("dispDateEnd", sqlDispDateEnd);
			query.setParameter("createDateEnd", createDateEnd);
    	}
    	if (cddhCriteria.getItemCode() != null) {
    		query.setParameter("itemCode", cddhCriteria.getItemCode());
    	}
    	if (dangerDrugFlag != null) {
    		query.setParameter("dangerDrugFlag", dangerDrugFlag);
    	}
				
    	this.addHintsForDispOrderItem(query);
    	
    	return query.getResultList();
    }
    
    private void addHintsForDispOrderItem(Query query) {
    	addHintsForDispOrderItem(query, false);
    }
    
    private void addHintsForDispOrderItem(Query query, boolean withInvoice) {    	
		query.setHint(QueryHints.FETCH, "o.pharmOrderItem.pharmOrder.medCase");
		query.setHint(QueryHints.FETCH, "o.pharmOrderItem.medOrderItem.medOrder");
		query.setHint(QueryHints.BATCH, "o.dispOrder.ticket");
    	if (withInvoice) {
        	query.setHint(QueryHints.BATCH, "o.dispOrder.invoiceList.invoiceItemList");
    	}
    }
    
    @SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveExceptionDispOrderItem(CddhCriteria cddhCriteria){
    	// reset the dispensed end date for sql use
    	Date sqlPatListDispDateEnd = null;
    	if(cddhCriteria.getPatListDispDate() != null){
    		sqlPatListDispDateEnd = (new DateTime(cddhCriteria.getPatListDispDate())).plusDays(1).toDate();
    	}
    	
    	if(StringUtils.isBlank(cddhCriteria.getItemCode())){
    		cddhCriteria.setItemCode(null);
    	}
    	
    	Boolean dangerDrugFlag = null;
    	if(cddhCriteria.getDangerDrugFlag() != null){
	    	if(cddhCriteria.getDangerDrugFlag()==YesNoBlankFlag.Yes){
	    		dangerDrugFlag = true;
	    	} 
	    	
	    	if(cddhCriteria.getDangerDrugFlag()==YesNoBlankFlag.No){
	    		dangerDrugFlag = false;
	    	}
    	}
    	
    	logger.info("CDDH retrieve by disp order id: dispOrderId=#0,dispHospCode=#1,patName=#2,dispDateStart=#3,dispDateEnd=#4", cddhCriteria.getDispOrderId(), cddhCriteria.getDispHospCode(), cddhCriteria.getDispOrderPatName(), cddhCriteria.getPatListDispDate(), sqlPatListDispDateEnd);
    	
    	List<DispOrderItem> dispOrderItemList = null;

    	Date createDateStart = cddhCriteria.getPatListDispDate() == null ? null : new DateMidnight(cddhCriteria.getPatListDispDate()).minusDays(ORDER_ALLOWUPDATE_DURATION.get()).toDate();
    	Date createDateEnd = sqlPatListDispDateEnd == null ? null : new DateMidnight(sqlPatListDispDateEnd).plusDays(ORDER_ALLOWUPDATE_DURATION.get()).toDate();

    	StringBuilder sql = new StringBuilder();
    	sql.append("select o from DispOrderItem o"); // 20171024 index check : DispOrderItem.dispOrder : FK_DISP_ORDER_ITEM_01
    	sql.append(" where o.dispOrder.id = :dispOrderId");
    	sql.append(" and o.dispOrder.workstore.hospCode = :dispHospCode");
    	sql.append(" and o.dispOrder.pharmOrder.name = :patName");
    	sql.append(" and o.dispOrder.status = :status");
    	sql.append(" and o.dispOrder.adminStatus <> :adminStatus");
    	sql.append(" and o.dispOrder.issueDate >= :dispDateStart and o.dispOrder.issueDate < :dispDateEnd");
    	sql.append(" and o.dispOrder.createDate >= :createDateStart and o.dispOrder.createDate < :createDateEnd");
    	sql.append(" and o.createDate >= :createDateStart and o.createDate < :createDateEnd");
    	if (cddhCriteria.getItemCode() != null) {
        	sql.append(" and o.pharmOrderItem.itemCode = :itemCode");
    	}
    	if (dangerDrugFlag != null) {
    		sql.append(" and o.pharmOrderItem.dangerDrugFlag = :dangerDrugFlag");
    	}

    	Query query = emReport.createQuery(sql.toString());
    	query.setParameter("dispOrderId", cddhCriteria.getDispOrderId());
    	query.setParameter("dispHospCode", cddhCriteria.getDispHospCode());
    	query.setParameter("patName", cddhCriteria.getDispOrderPatName());
    	query.setParameter("status", DispOrderStatus.Issued);
    	query.setParameter("adminStatus", DispOrderAdminStatus.Suspended);
    	query.setParameter("dispDateStart", cddhCriteria.getPatListDispDate());
    	query.setParameter("dispDateEnd", sqlPatListDispDateEnd);
    	query.setParameter("createDateStart", createDateStart);
    	query.setParameter("createDateEnd", createDateEnd);
    	if (cddhCriteria.getItemCode() != null) {
    		query.setParameter("itemCode", cddhCriteria.getItemCode());
    	}
    	if (dangerDrugFlag != null) {
    		query.setParameter("dangerDrugFlag", dangerDrugFlag);
    	}

    	this.addHintsForDispOrderItem(query);
    	
    	dispOrderItemList = query.getResultList();
    	return dispOrderItemList;
    }
    
    private boolean checkWorkstoreGroupExist(String loginHospCode, String workstoreGroupCode) {
    	Hospital hospital = emReport.find(Hospital.class, loginHospCode);
		List<CddhWorkstoreGroup> cddhWorkstoreGroupList = hospital.getCddhWorkstoreGroupList();
		for (CddhWorkstoreGroup cddhWorkstoreGroup: cddhWorkstoreGroupList){
			if (cddhWorkstoreGroup.getStatus() == RecordStatus.Active && 
					cddhWorkstoreGroup.getCddhWorkstoreGroupCode().equals(workstoreGroupCode)) {
				return true;
			}
		}
		return false;
    }
    
    public CddhRemarkStatus checkDispOrderItemRemarkAccess(Long dispOrderId, String loginHospCode, String displayHospCode){
    	DispOrder dispOrder = emReport.find(DispOrder.class, dispOrderId);
    	if (dispOrder != null) {
	    	if (dispOrder.getBatchProcessingFlag()) {
	    		return CddhRemarkStatus.DayEndProcessing;
	    	}
    	}
    	return CddhRemarkStatus.Success;
    }
    
    private CddhRemarkStatus checkDispOrderItemRemarkAccess(String loginHospCode, String displayHospCode){
    	if (loginHospCode == null) {
        	return CddhRemarkStatus.AccessDenied;
    	}
    	
    	if (loginHospCode.equals(displayHospCode)) {
    		return CddhRemarkStatus.Success;
    	} else {
    		if (checkWorkstoreGroupExist(loginHospCode, displayHospCode )) {
    			return CddhRemarkStatus.Success;
    		}
    	}
    	
    	return CddhRemarkStatus.AccessDenied;
    }
    
    public void updateDispOrderItemRemark(DispOrderItem dispOrderItem, Boolean legacyPersistenceEnabled){
    	if (!dispOrderItem.isLegacy()) {
	    	DispOrderItem managedDispOrderItem = em.find(DispOrderItem.class, dispOrderItem.getId());
	    	managedDispOrderItem.setRemarkType(dispOrderItem.getRemarkType());
	    	managedDispOrderItem.setRemarkText(dispOrderItem.getRemarkText());
	    	managedDispOrderItem.setRemarkUpdateDate(dispOrderItem.getRemarkUpdateDate());
	    	managedDispOrderItem.setRemarkUpdateUser(dispOrderItem.getRemarkUpdateUser());
	    	managedDispOrderItem.setVersion(dispOrderItem.getVersion());
	    	em.merge(managedDispOrderItem);
	    	
	    	if (legacyPersistenceEnabled) {
	    		daCddhRepServiceProxy.updateRemark(dispOrderItem);
	    	}
    	} else {
    		daCddhRepServiceProxy.updateRemark(dispOrderItem);
    	}
    }
    
    public void deleteDispOrderItemRemark(DispOrderItem dispOrderItem, Boolean legacyPersistenceEnabled){
    	if (!dispOrderItem.isLegacy()) {
	    	DispOrderItem managedDispOrderItem = em.find(DispOrderItem.class, dispOrderItem.getId());
	    	managedDispOrderItem.getPharmOrderItem();
	    	managedDispOrderItem.setRemarkType(DispOrderItemRemarkType.Empty);
	    	managedDispOrderItem.setRemarkText(null);
	    	managedDispOrderItem.setRemarkUpdateDate(null);
	    	managedDispOrderItem.setRemarkUpdateUser(null);
	    	managedDispOrderItem.setVersion(dispOrderItem.getVersion());
	    	em.merge(managedDispOrderItem);
	    	
	    	if (legacyPersistenceEnabled) {
	    		daCddhRepServiceProxy.updateRemark(managedDispOrderItem);
	    	}
    	} else {
    		dispOrderItem.setRemarkType(DispOrderItemRemarkType.Empty);
    		dispOrderItem.setRemarkText(null);
    		daCddhRepServiceProxy.updateRemark(dispOrderItem);
    	}
    }
    
    @SuppressWarnings("unchecked")
	public List<DispOrderItem> retrieveDispOrderItemForCms(CddhDispenseInfoCriteria cddhDispenseInfoCriteria) {
    	String orderNum = StringUtils.rightPad(cddhDispenseInfoCriteria.getPatHospCode(), PAT_HOSP_CODE_LENGTH) + cddhDispenseInfoCriteria.getMoeOrderNum();
    	
    	Query query = emReport.createQuery(
    			"select o from DispOrderItem o" + // 20120313 index check : MedOrder.orderNum : I_MED_ORDER_01
    			" where o.dispOrder.pharmOrder.medOrder.orderNum = :orderNum" +
    			" and (o.dispOrder.pharmOrder.medOrder.docType = :normalOrder" +
    			" or (o.dispOrder.pharmOrder.medOrder.docType = :manualOrder" +
    			" and o.dispOrder.workstore.hospCode = :dispHospCode))" +
    			" and o.dispOrder.status = :status" +
    			" and o.dispOrder.adminStatus <> :adminStatus")
			.setParameter("orderNum", orderNum)
			.setParameter("normalOrder", MedOrderDocType.Normal)
			.setParameter("manualOrder", MedOrderDocType.Manual)
			.setParameter("dispHospCode", cddhDispenseInfoCriteria.getDispHospCode())
			.setParameter("status", DispOrderStatus.Issued)
			.setParameter("adminStatus", DispOrderAdminStatus.Suspended);
    	
    	this.addHintsForDispOrderItem(query, true);
    	
    	// get and build the dispOrderItem.invoiceItemList link
    	List<DispOrderItem> dispOrderItemList = query.getResultList();    	
    	for (DispOrderItem dispOrderItem : dispOrderItemList) {
    		dispOrderItem.getDispOrder().getTicket();
    		for (Invoice invoice : dispOrderItem.getDispOrder().getInvoiceList()) {
    			invoice.loadChild();
    		}
    	}
    	    	
    	// legacy
    	int legacyCount = 0;
    	if (CDDH_LEGACY_ENQUIRY_ENABLED.get()) {
    		List<DispOrderItem> legacyDispOrderItemList = daCddhServiceProxy.retrieveDaDispOrderItemForCms(cddhDispenseInfoCriteria);
    		legacyCount = legacyDispOrderItemList.size();
    		dispOrderItemList.addAll(legacyDispOrderItemList);
    	}
    	
    	logger.info("retrieveDispOrderItemForCms: orderNum=#0, dispHospCode=#1, legacyResultCount=#2, totalResultCount=#3", orderNum, cddhDispenseInfoCriteria.getDispHospCode(), legacyCount, dispOrderItemList.size());
    	return dispOrderItemList;
    }
    
    public void createPatientProfileCddh(String action, DispOrder dispOrder) {
    	createPatientProfileCddh(action, dispOrder, null);
    }
    
    @SuppressWarnings("unchecked")
    public void createPatientProfileCddh(String action, DispOrder dispOrder, PatientProfile inPatientProfile) {
    	MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
    	PharmOrder pharmOrder = dispOrder.getPharmOrder();
    	
    	if (medOrder.getPatient() == null) {
    		return;
    	}
    	
    	if (medOrder.getOrderType() == OrderType.InPatient) {
    		return;
    	}
    	
    	PatientProfile patientProfile = null;
    	if (inPatientProfile != null) {
    		// reuse of managed patient profile if any
    		patientProfile = inPatientProfile;
    	} else {
	    	List<PatientProfile> patientProfileList = em.createQuery(
					"select o from PatientProfile o" + // 20171024 index check : PatientProfile.patientId : PK_PATIENT_PROFILE
					" where o.patientId in :patientId")
					.setParameter("patientId", Arrays.asList(medOrder.getPatient().getId()))
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
	    	
	    	if ( ! patientProfileList.isEmpty()) {
	    		patientProfile = patientProfileList.get(0);
	    		
	    	} else {
	    		patientProfile = new PatientProfile();
	    		patientProfile.setPatientId(medOrder.getPatient().getId());
	    		patientProfile.setPatientCddh(new PatientCddh());
	    		patientProfile.setPatient(medOrder.getPatient());
	    		em.persist(patientProfile);
	    	}    		
    	}
    	
    	Map<Long, PatientCddhPharmItem> patientCddhPharmItemByPoiIdMap = new HashMap<Long, PatientCddhPharmItem>();
		for (PatientCddhPharmItem patientCddhPharmItem : patientProfile.getPatientCddh().getPatientCddhPharmItemList()) {
			patientCddhPharmItemByPoiIdMap.put(patientCddhPharmItem.getPharmOrderItemId(), patientCddhPharmItem);
		}
    	
    	for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
    		PatientCddhPharmItem patientCddhPharmItem;
    		
    		if (patientCddhPharmItemByPoiIdMap.containsKey(dispOrderItem.getPharmOrderItem().getId())) {
    			patientCddhPharmItem = patientCddhPharmItemByPoiIdMap.get(dispOrderItem.getPharmOrderItem().getId());
    			
    		} else {
    			patientCddhPharmItem = new PatientCddhPharmItem();
    			patientProfile.getPatientCddh().addPatientCddhPharmItem(patientCddhPharmItem);
    			
    			patientCddhPharmItem.setPharmOrderItemId(dispOrderItem.getPharmOrderItem().getId());
    			patientCddhPharmItem.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
    			patientCddhPharmItem.setOrderNum(medOrder.getOrderNum());
    			patientCddhPharmItem.setCaseNum(pharmOrder.getMedCase().getCaseNum());
    			patientCddhPharmItem.setMedOrderId(medOrder.getId());
    			patientCddhPharmItem.setPharmOrderId(pharmOrder.getId());
    			patientCddhPharmItem.setFmStatus(dispOrderItem.getPharmOrderItem().getFmStatus());
    			patientCddhPharmItem.setRegimenType(dispOrderItem.getPharmOrderItem().getRegimen().getType());
    			
    			for (DoseGroup doseGroup : dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList()) {
					PatientCddhDoseGroup patientCddhDoseGroup = new PatientCddhDoseGroup();
        			patientCddhPharmItem.addPatientCddhDoseGroup(patientCddhDoseGroup);
					
					patientCddhDoseGroup.setStartDate(doseGroup.getStartDate());
					patientCddhDoseGroup.setEndDate(doseGroup.getEndDate());
        			
    				for (Dose dose : doseGroup.getDoseList()) {
    					PatientCddhDose patientCddhDose = new PatientCddhDose();
    					patientCddhDoseGroup.addPatientCddhDose(patientCddhDose);
    					
    					patientCddhDose.setDosage(dose.getDosage());
    					patientCddhDose.setDosageUnit( StringUtils.trimToNull(dose.getDosageUnit()) );
    					patientCddhDose.setPrn(dose.getPrn());
    					patientCddhDose.setSiteCode(StringUtils.trimToNull(dose.getSiteCode()));
    					patientCddhDose.setSupplSiteDesc( StringUtils.trimToNull(dose.getSupplSiteDesc()) );	
    					patientCddhDose.setDailyFreq(dose.getDailyFreq());
    					if (patientCddhDose.getDailyFreq() != null) {
    						patientCddhDose.getDailyFreq().setDesc(StringUtils.trimToNull(patientCddhDose.getDailyFreq().getDesc()));
    					}
    					patientCddhDose.setSupplFreq(dose.getSupplFreq());
    					if (patientCddhDose.getSupplFreq() != null) {
    						patientCddhDose.getSupplFreq().setDesc(StringUtils.trimToNull(patientCddhDose.getSupplFreq().getDesc()));
    					}
    				}
    			}
    		}
    		
    		PatientCddhDispItem patientCddhDispItem = new PatientCddhDispItem();
    		patientCddhPharmItem.addPatientCddhDispItem(patientCddhDispItem);
    		
    		patientCddhDispItem.setDispOrderId(dispOrder.getId());
    		patientCddhDispItem.setDispOrderItemId(dispOrderItem.getId());
    		patientCddhDispItem.setStartDate(dispOrderItem.getStartDate());
    		patientCddhDispItem.setEndDate(dispOrderItem.getEndDate());
    		patientCddhDispItem.setAdminStatus(dispOrder.getAdminStatus());
    	}

        housekeepPatientProfile(patientProfile);
    	
    	Collections.sort(patientProfile.getPatientCddh().getPatientCddhPharmItemList(), new PatientCddhPharmItemComparator());
    	
    	patientProfile.preSave();
    	
		auditLogger.log("#4116 CDDH|Create PatientProfile Item|PatientId [#0]|Action [#1]|DispOrderId [#2]|CddhXml [#3]|", patientProfile.getPatientId(), action, dispOrder.getId(), patientProfile.getCddhXml()); 
    }
    
    @SuppressWarnings("unchecked")
    public PatientProfile removePatientProfileCddh(String action, DispOrder dispOrder) {
    	MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
    	
    	List<PatientProfile> patientProfileList = em.createQuery(
				"select o from PatientProfile o" + // 20171024 index check : PatientProfile.patientId : PK_PATIENT_PROFILE
				" where o.patientId in :patientId")
				.setParameter("patientId", Arrays.asList(medOrder.getPatient().getId()))
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();

    	if (patientProfileList.isEmpty()) {
    		return null;
    	}
    	
    	PatientProfile patientProfile = patientProfileList.get(0);
    	
    	Map<Long, PatientCddhPharmItem> patientCddhPharmItemByPoiIdMap = new HashMap<Long, PatientCddhPharmItem>();
		for (PatientCddhPharmItem patientCddhPharmItem : patientProfile.getPatientCddh().getPatientCddhPharmItemList()) {
			patientCddhPharmItemByPoiIdMap.put(patientCddhPharmItem.getPharmOrderItemId(), patientCddhPharmItem);
		}
    	
    	for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
    		if (patientCddhPharmItemByPoiIdMap.containsKey(dispOrderItem.getPharmOrderItem().getId())) {
    			PatientCddhPharmItem patientCddhPharmItem = patientCddhPharmItemByPoiIdMap.get(dispOrderItem.getPharmOrderItem().getId());

    			PatientCddhDispItem delPatientCddhDispItem = null;
    	    	for (PatientCddhDispItem patientCddhDispItem : patientCddhPharmItem.getPatientCddhDispItemList()) {
    	    		if (patientCddhDispItem.getDispOrderItemId().equals(dispOrderItem.getId())) {
    	    			delPatientCddhDispItem = patientCddhDispItem;
    	    			break;
    	    		}
    	    	}
    	    	
    	    	if (delPatientCddhDispItem != null) {
    	    		if (patientCddhPharmItem.getPatientCddhDispItemList().size() == 1) {
    	    			// only disp item to be deleted is linked to the pharm item, so delete whole pharm item
    	    			patientProfile.getPatientCddh().getPatientCddhPharmItemList().remove(patientCddhPharmItem);
    	    		} else {
        	    		// other disp item are still linked to the pharm item, so only delete the target disp item
        	    		patientCddhPharmItem.getPatientCddhDispItemList().remove(delPatientCddhDispItem);
    	    		}
    	    	}
    		}
    	}

    	patientProfile.preSave();
    	
		auditLogger.log("#4117 CDDH|Delete PatientProfile Item|PatientId [#0]|Action [#1]|DispOrderId [#2]|CddhXml [#3]|", patientProfile.getPatientId(), action, dispOrder.getId(), patientProfile.getCddhXml()); 
		
    	return patientProfile;
    }
    
    @SuppressWarnings("unchecked")
    public void updatePatientProfileCddhAdminStatus(DispOrder dispOrder) {
    	MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
    	
    	List<PatientProfile> patientProfileList = em.createQuery(
				"select o from PatientProfile o" + // 20171024 index check : PatientProfile.patientId : PK_PATIENT_PROFILE
				" where o.patientId in :patientId")
				.setParameter("patientId", Arrays.asList(medOrder.getPatient().getId()))
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
    	
    	if (patientProfileList.isEmpty()) {
    		return;
    	}
    	
    	PatientProfile patientProfile = patientProfileList.get(0);
    	
    	Map<Long, PatientCddhPharmItem> patientCddhPharmItemByPoiIdMap = new HashMap<Long, PatientCddhPharmItem>();
		for (PatientCddhPharmItem patientCddhPharmItem : patientProfile.getPatientCddh().getPatientCddhPharmItemList()) {
			patientCddhPharmItemByPoiIdMap.put(patientCddhPharmItem.getPharmOrderItemId(), patientCddhPharmItem);
		}
    	
    	for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
    		if (patientCddhPharmItemByPoiIdMap.containsKey(dispOrderItem.getPharmOrderItem().getId())) {
    			PatientCddhPharmItem patientCddhPharmItem = patientCddhPharmItemByPoiIdMap.get(dispOrderItem.getPharmOrderItem().getId());
    			
    	    	for (PatientCddhDispItem patientCddhDispItem : patientCddhPharmItem.getPatientCddhDispItemList()) {
    	    		if (patientCddhDispItem.getDispOrderItemId().equals(dispOrderItem.getId())) {
    	    			patientCddhDispItem.setAdminStatus(dispOrder.getAdminStatus());
    	    			break;
    	    		}
    	    	}
    		}
    	}
    	
    	patientProfile.preSave();
    }
    
    @SuppressWarnings("unchecked")
    public void updatePatientProfilePatientId(Map<String, Pair<Long, Long>> movePatientIdPairByOrderNumMap) {
    	if (movePatientIdPairByOrderNumMap.keySet().isEmpty()) {
    		return;
    	}

    	Set<Long> allPatientIdSet = new HashSet<Long>();
    	for (String orderNum : movePatientIdPairByOrderNumMap.keySet()) {
    		Pair<Long, Long> movePatientIdPair = movePatientIdPairByOrderNumMap.get(orderNum);
    		
			allPatientIdSet.add(movePatientIdPair.getFirst());	// add source patient id
			allPatientIdSet.add(movePatientIdPair.getSecond());	// add target patient id
    	}
    	List<Long> allPatientIdList = new ArrayList<Long>();
    	allPatientIdList.addAll(allPatientIdSet);
    	
		List<PatientProfile> patientProfileList = QueryUtils.splitExecute(em.createQuery(
				"select o from PatientProfile o" + // 20171024 index check : PatientProfile.patientId : PK_PATIENT_PROFILE
				" where o.patientId in :patientId"),
				"patientId", allPatientIdList);
		
		Map<Long, PatientProfile> patientProfileByPatientIdMap = new HashMap<Long, PatientProfile>();
		for (PatientProfile patientProfile : patientProfileList) {
			patientProfileByPatientIdMap.put(patientProfile.getPatientId(), patientProfile);
		}
    	
    	for (String orderNum : movePatientIdPairByOrderNumMap.keySet()) {
    		Pair<Long, Long> movePatientIdPair = movePatientIdPairByOrderNumMap.get(orderNum);
    		
    		Long srcPatientId = movePatientIdPair.getFirst();
    		Long targetPatientId = movePatientIdPair.getSecond();
    		
    		if (srcPatientId == null) {
    			// for safety
    			continue;
    		}
    		
    		if (targetPatientId == null) {
    			// for safety
    			continue;
    		}
    		
    		if (srcPatientId == targetPatientId) {
    			// for safety
    			continue;
    		}
    		
        	PatientProfile srcPatientProfile = null;
			if (patientProfileByPatientIdMap.containsKey(srcPatientId)) {
				srcPatientProfile = patientProfileByPatientIdMap.get(srcPatientId);
			}

    		if (srcPatientProfile == null) {
        		// for order saved before patient profile table created, there is no patient record in patient profile table 
    			continue;
    		}
			
        	PatientProfile targetPatientProfile = null;
			if (patientProfileByPatientIdMap.containsKey(targetPatientId)) {
				targetPatientProfile = patientProfileByPatientIdMap.get(targetPatientId);
				
			} else {
    			targetPatientProfile = new PatientProfile();
    			targetPatientProfile.setPatientId(targetPatientId);
    			targetPatientProfile.setPatientCddh(new PatientCddh());
    			// no need to set patient since it is not insertable
	    		em.persist(targetPatientProfile);
	    		
	    		patientProfileByPatientIdMap.put(targetPatientProfile.getPatientId(), targetPatientProfile);
	    		patientProfileList.add(targetPatientProfile);
    		}
    		
			// move patient cddh from source patient profile to target patient profile
    		List<PatientCddhPharmItem> srcPatientCddhPharmItemList = srcPatientProfile.getPatientCddh().getPatientCddhPharmItemList();
    		
    		List<PatientCddhPharmItem> movePatientCddhPharmItemList = new ArrayList<PatientCddhPharmItem>();
    		for (PatientCddhPharmItem srcPatientCddhPharmItem : srcPatientCddhPharmItemList) {
    			if (orderNum.equals(srcPatientCddhPharmItem.getOrderNum())) {
    				movePatientCddhPharmItemList.add(srcPatientCddhPharmItem);
    			}
    		}
    		
    		targetPatientProfile.getPatientCddh().getPatientCddhPharmItemList().addAll(movePatientCddhPharmItemList);
    		
    		srcPatientProfile.getPatientCddh().getPatientCddhPharmItemList().removeAll(movePatientCddhPharmItemList);
    		
    		auditLogger.log("#4118 CDDH|Move PatientProfile Patient|OrderNum [#0]|SourcePatientId [#1]|TargetPatientId [#2]|", orderNum, srcPatientProfile.getPatientId(), targetPatientProfile.getPatientId()); 
		}

    	// sort and marshall all source and target patient profile
		for (PatientProfile patientProfile : patientProfileList) {
        	Collections.sort(patientProfile.getPatientCddh().getPatientCddhPharmItemList(), new PatientCddhPharmItemComparator());
        	
        	patientProfile.preSave();
		}
    }
    
    private void housekeepPatientProfile(PatientProfile patientProfile) {
    	if (patientProfile == null) {
    		return;
    	}
    	
    	Date currentDate = new Date();
		Date todayWithoutTime = DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH);
    	
		Integer retainDuration = VETTING_ORDER_RECON_ENDRX_RETAIN_DURATION.get();
		Date retainEndDate = new DateTime(todayWithoutTime).minusDays(retainDuration).toDate();
		
		List<PatientCddhPharmItem> delPatientCddhPharmItemList = new ArrayList<PatientCddhPharmItem>();

		for (PatientCddhPharmItem patientCddhPharmItem : patientProfile.getPatientCddh().getPatientCddhPharmItemList()) {
			List<PatientCddhDispItem> delPatientCddhDispItemList = new ArrayList<PatientCddhDispItem>();
			
	    	for (PatientCddhDispItem patientCddhDispItem : patientCddhPharmItem.getPatientCddhDispItemList()) {
				if (patientCddhDispItem.getEndDate().compareTo(retainEndDate) < 0) {
					delPatientCddhDispItemList.add(patientCddhDispItem);
	    		}
	    	}
	    	
	    	if ( ! delPatientCddhDispItemList.isEmpty()) {
	    		patientCddhPharmItem.getPatientCddhDispItemList().removeAll(delPatientCddhDispItemList);
	    		
	    		if (patientCddhPharmItem.getPatientCddhDispItemList().isEmpty()) {
	    			delPatientCddhPharmItemList.add(patientCddhPharmItem);
	    		}
	    	}
		}
		
		patientProfile.getPatientCddh().getPatientCddhPharmItemList().removeAll(delPatientCddhPharmItemList);
    }
    
	private static class PatientCddhPharmItemComparator implements Comparator<PatientCddhPharmItem>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PatientCddhPharmItem patientCddhPharmItem1, PatientCddhPharmItem patientCddhPharmItem2) {
			return new CompareToBuilder()
							.append( patientCddhPharmItem1.getItemCode(), patientCddhPharmItem2.getItemCode() )
							.append( patientCddhPharmItem1.getPharmOrderItemId(), patientCddhPharmItem2.getPharmOrderItemId() )
							.toComparison();
		}
    }
}
