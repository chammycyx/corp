package hk.org.ha.model.pms.corp.batch.worker.dayend;

import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ENABLED;
import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.vo.disp.DayEndDispOrder;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * Session Bean implementation class ExportUncollectTrxBean
 */

@Stateful
@Scope(ScopeType.SESSION)
@Name("uncollectTrxBean")
public class UncollectTrxBean implements UncollectTrxLocal {
	
	private static final String DOT = ".";
	private static final String SLASH = "/";
	private static final String MONTH_DIR_NAME = "month";
	private static final String COUNT_FILE_EXTENSION = "cnt";
	
	private Date batchDate = null;
	private Date startDate = null;
	
	private Logger logger = null;
	
	// for building directory
	private String jobId = null;
	
	private String year = null;
	private String month = null;
	
	@In
	private DayEndOrderManagerLocal dayEndOrderManager;
	
	@In
	private ApplicationProp applicationProp;

    /**
     * Default constructor. 
     */
    public UncollectTrxBean() {
        // TODO Auto-generated constructor stub
    }

	public void beginChunk(Chunk chunk) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
		
	}
    
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if (params.getDate("batchDate") == null) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
			year = DateTimeFormat.forPattern("yyyy").print(new DateTime(batchDate));
			month = DateTimeFormat.forPattern("MM").print(new DateTime(batchDate));
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
        
        Integer dayEndDuration = BATCH_DAYEND_ORDER_DURATION.get();
        
		if (dayEndDuration == null) {
			throw new Exception("corporate property : batch.dayEnd.order.duration is null");
		} else {
			DateTime dt = new DateTime(batchDate);
			startDate = dt.minusDays(dayEndDuration).toDate();
		}
	}
	
	private boolean checkHospFileExist(File dir, List<String> fileNameList, Date batchDate, String hospCode) {
		for (String fileName: fileNameList) {
			File file = getFile(dir, fileName, batchDate, hospCode, null);
			if (file.exists()) {
				return true;
			}
		}
		return false;
	}
	
	public void processRecord(Record record) throws Exception {
		
		String hospCode = record.getString("HOSP_CODE");
		
		Date startDatetime = new Date();
		logger.info("process record start for hospCode [" + hospCode + "]... @" + startDatetime);

		Hospital hospital = new Hospital(hospCode);
		if ( ! BATCH_DAYEND_ENABLED.get(hospital)) {
			logger.error("Day end is disabled for hospCode = [" + hospCode + "]");
			return;
		}
		
		BatchJobState batchJobState = dayEndOrderManager.retrieveBatchJobState(jobId, batchDate, hospCode, null);
		if (batchJobState != null) {
			logger.info("Job " + jobId + " is already done for batchDate [" + batchDate + "], hospCode [" + hospCode + "]");
			return;
		}
		
		List<String> fcsFileList = Arrays.asList(applicationProp.getFcsUncollectTrxFile());
		
		// get directories
		File workFcsDir = getDirectory(hospCode, jobId, DataDirType.Work);
		File outFcsDir = getOutDoneDirectory(jobId, DataDirType.Out, null, hospCode, true);
		File errorFcsDir = getDirectory(hospCode, jobId, DataDirType.Error);

		// check whether hospital file exist in OUT folder 
		// (since file is unknown file one when entry does not exists in BatchJobState, exception is thrown)
		if (checkHospFileExist(outFcsDir, fcsFileList, batchDate, hospCode)) {
			throw new UnsupportedOperationException("Uncollect hosp file of date [" + batchDate + "] already exists in OUT folder for hospCode = [" + hospCode + "] but job is not yet done");
		}
		
		File uncollectTrxFile = getFile(workFcsDir, applicationProp.getFcsUncollectTrxFile(), batchDate, hospCode, null);
		File uncollectCountTrxFile = getFile(workFcsDir, applicationProp.getFcsUncollectTrxFile(), batchDate, hospCode, null, COUNT_FILE_EXTENSION);
		
		List<DayEndDispOrder> dayEndDispOrderListForDayEnd = null;
		List<DayEndDispOrder> dayEndDispOrderListForNonDayEnd = null;
		
		try {
			// new transaction is used because CDDH needs to know Processing status, and   
			// pessimistic lock is used in the function
			dayEndOrderManager.preProcessUncollect(
					logger,
					hospCode, 
					startDate,
					batchDate,
					applicationProp.getFcsUncollectTrxFile());

			// no new transaction because file generation takes long time
			// (day end status will fallback to NONE if there is exception)
			List<List<DayEndDispOrder>> dayEndDispOrderListGroup = dayEndOrderManager.processUncollect(
					logger,
					hospCode, 
					startDate,
					batchDate,
					uncollectTrxFile,
					uncollectCountTrxFile);
			
			dayEndDispOrderListForDayEnd = dayEndDispOrderListGroup.get(0);
			dayEndDispOrderListForNonDayEnd = dayEndDispOrderListGroup.get(1);
			
			// mark day end status back to None for non day end order and to Uncollect for day end order
			// (new transaction is used to guarantee update is committed before file move) 
			dayEndOrderManager.postProcessUncollect(
					logger,
					hospCode, 
					dayEndDispOrderListForDayEnd, 
					dayEndDispOrderListForNonDayEnd,
					"[" + applicationProp.getFcsUncollectTrxFile() + "]");
			
		} catch (Exception e) {
			moveFileToDirectory(uncollectTrxFile, errorFcsDir, startDatetime);
			moveFileToDirectory(uncollectCountTrxFile, errorFcsDir, startDatetime);
			
			// do not fallback day end status if postUncollect throws exception, because if 
			// marking Complete status failure in DB, mark None status will also fail
			
			throw e;
		}
		
		FileUtils.moveFileToDirectory(uncollectTrxFile, outFcsDir, false);
		FileUtils.moveFileToDirectory(uncollectCountTrxFile, outFcsDir, false);

		// save to log table when day end job is done
		dayEndOrderManager.saveBatchJobState(jobId, batchDate, hospCode, null);
		
		Date endDatetime = new Date();
        logger.info("process record complete for hospCode [" + hospCode + "]..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	private void moveFileToDirectory(File file, File destDir, Date currentDate) throws IOException {
		if (file != null && file.exists()) {
			FileUtils.moveFile(file, getFile(destDir, file.getName(), null, null, currentDate));
		}
	}
	
	private File getDirectory(
			String hospCode,
			String jobId,
			DataDirType dataDirType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + "/" + 
				jobId + "/" + 
				dataDirType.getDataValue() + "/" + 
				hospCode);
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}
	
	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
			logger.debug("UncollectTrxBean - createDirectory: create path status: " + created);
		}
		
		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("UncollectTrxBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("UncollectTrxBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("UncollectTrxBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}

	private File getOutDoneDirectory(
			String jobId,
			DataDirType dataDirType,
			String systemType,
			String hospCode,
			boolean createDir) throws Exception {
		
		StringBuilder basicPath = new StringBuilder();
		basicPath.append(applicationProp.getBatchDataDir())
						.append(SLASH).append(jobId)
						.append(SLASH).append(dataDirType.getDataValue());
		if (systemType != null) {
			basicPath.append(SLASH).append(systemType);
		}
		basicPath.append(SLASH).append(MONTH_DIR_NAME);
		
		return getOutDoneDirectory(basicPath.toString(), hospCode, createDir);
	}
	
	private File getOutDoneDirectory(
			String basicPath,
			String hospCode,
			boolean createDir) throws Exception {
		
		File basicDir = new File(basicPath);
		if ( ! basicDir.exists()) {
			throw new Exception("UncollectTrxBean: date directory could not be created as parent directory " + basicDir.getParent() + " not exists.");
		}

		StringBuilder destPath = new StringBuilder();
		destPath.append(basicPath)
						.append(SLASH).append(year)
						.append(SLASH).append(month)
						.append(SLASH).append(hospCode);
		
		File destDir = new File(destPath.toString());
		if ( ! destDir.exists()) {
			if (createDir) {
				destDir.mkdirs();
				if ( ! destDir.exists()) {
					throw new Exception("UncollectTrxBean: directory " + destDir.getPath() + " could not be found and created. Please check (eg. Access Right...).");
				}
			} else {
				throw new Exception("UncollectTrxBean: directory " + destDir.getPath() + " could not be found. Please check (eg. Access Right...).");
			}
		}
		
		return destDir;
	}
	
	private File getFile(File dir, String fileName, Date batchDate, String hospCode, Date currentDate) {
		return getFile(dir, fileName, batchDate, hospCode, currentDate, null);
	}
	
	private File getFile(File dir, String fileName, Date batchDate, String hospCode, Date currentDate, String fileExtension) {
		DateTimeFormatter sdf = DateTimeFormat.forPattern("yyyyMMdd");
		DateTimeFormatter sdt = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(sdf.print(new DateTime(batchDate)));
		}
		
		if (hospCode != null) {
			finalFileName.append(DOT)
			 .append(hospCode);
		}
		
		if (currentDate != null) {
			finalFileName.append(DOT)
						 .append(sdt.print(new DateTime(currentDate)));
		}
		
		if (fileExtension != null) {
			finalFileName.append(DOT)
						 .append(fileExtension);
		}
		
		File file = new File(
				    dir + "/" + 
					finalFileName );
		
		return file;
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
}
