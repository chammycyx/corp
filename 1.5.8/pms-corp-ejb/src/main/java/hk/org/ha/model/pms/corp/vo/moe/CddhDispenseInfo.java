package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;
import java.util.Date;

public class CddhDispenseInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer dispOrderNum;

	private Integer dispItemNum;
	
	private Integer moeItemNum;

	private Integer orgMoeItemNum;

	private Date updateDate;

	private String line1;

	private String line2;

	private String line3;

	private Integer actionStatusRank;

	public Integer getDispOrderNum() {
		return dispOrderNum;
	}

	public void setDispOrderNum(Integer dispOrderNum) {
		this.dispOrderNum = dispOrderNum;
	}

	public Integer getDispItemNum() {
		return dispItemNum;
	}

	public void setDispItemNum(Integer dispItemNum) {
		this.dispItemNum = dispItemNum;
	}

	public Integer getMoeItemNum() {
		return moeItemNum;
	}

	public void setMoeItemNum(Integer moeItemNum) {
		this.moeItemNum = moeItemNum;
	}

	public Integer getOrgMoeItemNum() {
		return orgMoeItemNum;
	}

	public void setOrgMoeItemNum(Integer orgMoeItemNum) {
		this.orgMoeItemNum = orgMoeItemNum;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getLine3() {
		return line3;
	}

	public void setLine3(String line3) {
		this.line3 = line3;
	}

	public Integer getActionStatusRank() {
		return actionStatusRank;
	}

	public void setActionStatusRank(Integer actionStatusRank) {
		this.actionStatusRank = actionStatusRank;
	}
}
