package hk.org.ha.model.pms.corp.batch.worker.order;
import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_PATIENTEPISODE_LASTTRXDATE;
import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_PATIENTEPISODE_LIMIT;
import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.OrderHelper;
import hk.org.ha.model.pms.corp.pas.PasServiceWrapper;
import hk.org.ha.model.pms.corp.persistence.order.PatientEpisode;
import hk.org.ha.model.pms.corp.udt.order.PatientEpisodeStatus;
import hk.org.ha.model.pms.corp.udt.order.PatientEpisodeType;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;


/**
 * Session Bean implementation class PatientEpisodeServiceBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("patientEpisodeBean")
public class PatientEpisodeBean implements PatientEpisodeLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private PasServiceWrapper pasServiceWrapper;
	
	@In
	private PasServiceJmsRemote pasServiceProxy;
	
	@In
	private OrderHelper orderHelper;
	
	@In
	private CorpCddhServiceLocal corpCddhService;
	
	private Logger logger = null;
	
	private Date latestTranDate = null;
	private Integer recordNum = null;
	
	
	public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		//BatchParameters params = info.getBatchParameters();
		
        
	}

	private CorporateProp retrieveProp(String name) {
		CorporateProp corporateProp = (CorporateProp) em.createQuery(
				"select o from CorporateProp o" + // 20120213 index check : Prop.name : UI_PROP_01
				" where o.prop.name = :name") 
				.setParameter("name", name)
				.getSingleResult();
		return corporateProp;
	}
	
	private void retrievePatEpisodeProp() throws ParseException {
		latestTranDate = BATCH_PATIENTEPISODE_LASTTRXDATE.get().toDate();
		recordNum = BATCH_PATIENTEPISODE_LIMIT.get();
	}
	
	private List<PatientEpisode> retrievePatEpisodeList(Date batchDate) throws PasException {
		List<PatientEpisode> patEpisodeList = pasServiceProxy.retrievePatientEpisodeByTrxDate(latestTranDate, recordNum);
		savePatientEpisode(patEpisodeList, batchDate);
		return patEpisodeList;
	}
	
	private void savePatientEpisode(List<PatientEpisode> patEpisodeList, Date batchDate) {
		for (PatientEpisode patEpisode: patEpisodeList) {
			patEpisode.setStatus(PatientEpisodeStatus.None);
			patEpisode.setBatchDate(batchDate);
			em.persist(patEpisode);
		}
	}
	
	@SuppressWarnings("unchecked")
	private Patient retrievePatientByPatKey(String patKey) throws PasException, UnreachableException {
		Patient patient = null;
		
		List<Patient> patList = em.createQuery(
				"select o from Patient o" + // 20120213 index check : Patient.patKey : I_PATIENT_01
				" where o.patKey = :patKey")
				.setParameter("patKey", patKey)
				.getResultList();
		
		if (!patList.isEmpty()) {
			patient = patList.get(0);
		}
		
		if (patient == null) {
			Patient pasPatient = pasServiceWrapper.retrievePasPatientByPatKey(patKey, "", "");
			patient = orderHelper.savePatient(em, pasPatient);
		}
		
		return patient;
	}
	
	private boolean isSamePatient(PatientEpisode patEpisode) {
		if (patEpisode.getPrevPatKey().equals(patEpisode.getPatKey()) &&
				patEpisode.getPrevHkid().equals(patEpisode.getHkid())) {
			return true;
		} else {
			return false;		
		}		
	}
	
	private List<MedOrder> convertDistinctMedOrderList(List<DispOrder> dispOrderList) {
		List<MedOrder> medOrderList = new ArrayList<MedOrder>();
		
		Set<MedOrder> medOrderSet = new HashSet<MedOrder>();
		for (DispOrder dispOrder : dispOrderList) {
			medOrderSet.add(dispOrder.getPharmOrder().getMedOrder());
		}
		medOrderList.addAll(medOrderSet);
		
		return medOrderList; 
	}
	
	@SuppressWarnings("unchecked")
	public void processRecord(Record record) throws Exception {
		
		Date batchDate = new Date();
		logger.info("process record start... @" + batchDate);
		
		retrievePatEpisodeProp();
		Date maxTrxDate = latestTranDate;
		
		List<PatientEpisode> patEpisodeList = retrievePatEpisodeList(batchDate);
		
		for (PatientEpisode patEpisode: patEpisodeList) {
			if (patEpisode.getTrxDate() != null && maxTrxDate.compareTo(patEpisode.getTrxDate()) < 0) {
				maxTrxDate = patEpisode.getTrxDate();
			}
			
			if (isSamePatient(patEpisode)) {
				patEpisode.setStatus(PatientEpisodeStatus.Completed);
			} else {
		    	StringBuilder patIdSqlSb = new StringBuilder();
		    	
		    	patIdSqlSb.append("select o from DispOrder o") // 20120213 index check : Patient.patKey : I_PATIENT_01
			    			.append(" where o.pharmOrder.medOrder.patient.patKey = :prevPatKey")
			    			.append(" and o.pharmOrder.medOrder.patient.hkid = :prevHkid")
			    			.append(" and o.pharmOrder.medOrder.status not in :status");
		    	
		    	if (patEpisode.getType() == PatientEpisodeType.MergeHkid) {
		    		patIdSqlSb.append(" and o.pharmOrder.medOrder.patHospCode = :patHospCode");
		    	} else if (patEpisode.getType() == PatientEpisodeType.MoveEpisode) {
		    		patIdSqlSb.append(" and o.pharmOrder.medOrder.patHospCode = :patHospCode");
		    		patIdSqlSb.append(" and o.pharmOrder.medCase.caseNum = :caseNum");
		    	}
		    	
		    	Query patIdQuery = em.createQuery(patIdSqlSb.toString())
									.setParameter("prevPatKey", patEpisode.getPrevPatKey())
									.setParameter("prevHkid", patEpisode.getPrevHkid())
									.setParameter("status", Arrays.asList(MedOrderStatus.SysDeleted, MedOrderStatus.Deleted));
		    	if (patEpisode.getType() == PatientEpisodeType.MergeHkid) {
		    		patIdQuery.setParameter("patHospCode", patEpisode.getPatHospCode());
		    	} else if (patEpisode.getType() == PatientEpisodeType.MoveEpisode) {
		    		patIdQuery.setParameter("patHospCode", patEpisode.getPatHospCode());
		    		patIdQuery.setParameter("caseNum", patEpisode.getCaseNum());
		    	}

	    		patIdQuery.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient");
		    	
		    	List<DispOrder> dispOrderList = patIdQuery.getResultList();
		    	
				List<MedOrder> medOrderList = convertDistinctMedOrderList(dispOrderList);
				
				if (!medOrderList.isEmpty()) {
					
					try {
						Patient newPatient = retrievePatientByPatKey(patEpisode.getPatKey());
						
						if (newPatient == null) { // pas is down, then it may be null
							throw new PasException(new Exception());
						}
						
						newPatient.setHkid(StringUtils.trim(patEpisode.getHkid()));
						for (MedOrder medOrder: medOrderList) {
							if (newPatient != medOrder.getPatient()){
								medOrder.setPrevPatientId(medOrder.getPatient().getId());
								medOrder.setPatient(newPatient);
							}
						}

						// update patient patient profile
						// (note: must be before updating DO patient, because old patient id (in DO) is required here)
						Map<String, Pair<Long, Long>> movePatientIdPairByOrderNumMap = new HashMap<String, Pair<Long, Long>>();
						for (DispOrder dispOrder : dispOrderList) {
							Pair<Long, Long> movePatientIdPair = new Pair<Long, Long>(dispOrder.getPatient().getId(), dispOrder.getPharmOrder().getMedOrder().getPatient().getId());
							movePatientIdPairByOrderNumMap.put(dispOrder.getPharmOrder().getMedOrder().getOrderNum(), movePatientIdPair);
						}
						corpCddhService.updatePatientProfilePatientId(movePatientIdPairByOrderNumMap);
						
						// copy patKey and hkid to DispOrder
						// (DispOrder columns is used for creating SQL index) 
						for (DispOrder dispOrder : dispOrderList) {
							dispOrder.setPatient(dispOrder.getPharmOrder().getMedOrder().getPatient());
						}
						
						patEpisode.setStatus(PatientEpisodeStatus.Completed);
						
					} catch (PasException e) {
						patEpisode.setStatus(PatientEpisodeStatus.PasException);
						if (logger.isDebugEnabled()) {
							logger.debug("No patient information if found from OPAS by patient key: " + patEpisode.getPatKey());
						}
					} catch (UnreachableException e) {
						patEpisode.setStatus(PatientEpisodeStatus.PasException);
						if (logger.isDebugEnabled()) {
							logger.debug("OPAS is unreachable.");
						}
					}
				} else { // exceptional case, by prevPatId
					List<Patient> prevPatientList = em.createQuery(
							"select o from Patient o" + // 20120213 index check : Patient.patKey : I_PATIENT_01
							" where o.patKey = :prevPatKey" + 
							" and o.hkid = :prevHkid") 
							.setParameter("prevPatKey", patEpisode.getPrevPatKey())
							.setParameter("prevHkid", patEpisode.getPrevHkid())
							.getResultList();
					
					if (!prevPatientList.isEmpty()) {
						Patient prevPatient = prevPatientList.get(0);
						
				    	StringBuilder prevPatIdSqlSb = new StringBuilder();
				    	
				    	prevPatIdSqlSb.append("select o from DispOrder o") // 20120213 index check : MedOrder.prevPatientId : I_MED_ORDER_03
						    			.append(" where o.pharmOrder.medOrder.prevPatientId = :patId")
						    			.append(" and o.pharmOrder.medOrder.status not in :status");
						
				    	if (patEpisode.getType() == PatientEpisodeType.MergeHkid) {
				    		prevPatIdSqlSb.append(" and o.pharmOrder.medOrder.patHospCode = :patHospCode");
				    	} else if (patEpisode.getType() == PatientEpisodeType.MoveEpisode) {
				    		prevPatIdSqlSb.append(" and o.pharmOrder.medOrder.patHospCode = :patHospCode");
				    		prevPatIdSqlSb.append(" and o.pharmOrder.medCase.caseNum = :caseNum");
				    	}
				    	
				    	Query prevPatIdQuery = em.createQuery(prevPatIdSqlSb.toString())
												.setParameter("patId", prevPatient.getId())
												.setParameter("status", Arrays.asList(MedOrderStatus.SysDeleted, MedOrderStatus.Deleted));
				    	if (patEpisode.getType() == PatientEpisodeType.MergeHkid) {
				    		prevPatIdQuery.setParameter("patHospCode", patEpisode.getPatHospCode());
				    	} else if (patEpisode.getType() == PatientEpisodeType.MoveEpisode) {
				    		prevPatIdQuery.setParameter("patHospCode", patEpisode.getPatHospCode());
				    		prevPatIdQuery.setParameter("caseNum", patEpisode.getCaseNum());
				    	}
				    	
				    	prevPatIdQuery.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient");
				    	
				    	List<DispOrder> dispOrderListByPrevPatientId = prevPatIdQuery.getResultList();
				    	
						List<MedOrder> medOrderListByPrevPatientId = convertDistinctMedOrderList(dispOrderListByPrevPatientId);
						
						if (!medOrderListByPrevPatientId.isEmpty()) {
							try {
								Patient newPatient = retrievePatientByPatKey(patEpisode.getPatKey());
								if (newPatient == null) { // pas is down, then it may be null
									throw new PasException(new Exception());
								}
								
								newPatient.setHkid(StringUtils.trim(patEpisode.getHkid()));
								
								for (MedOrder medOrder: medOrderListByPrevPatientId) {
									if (newPatient != medOrder.getPatient()) {
										medOrder.setPrevPatientId(medOrder.getPatient().getId());
										medOrder.setPatient(newPatient);
									}
								}
								
								// update patient patient profile
								// (note: must be before updating DO patient, because old patient id (in DO) is required here)
								Map<String, Pair<Long, Long>> movePatientIdPairByOrderNumMap = new HashMap<String, Pair<Long, Long>>();
								for (DispOrder dispOrder : dispOrderListByPrevPatientId) {
									Pair<Long, Long> movePatientIdPair = new Pair<Long, Long>(dispOrder.getPatient().getId(), dispOrder.getPharmOrder().getMedOrder().getPatient().getId());
									movePatientIdPairByOrderNumMap.put(dispOrder.getPharmOrder().getMedOrder().getOrderNum(), movePatientIdPair);
								}
								corpCddhService.updatePatientProfilePatientId(movePatientIdPairByOrderNumMap);
								
								// copy patKey and hkid to DispOrder
								// (DispOrder columns is used for creating SQL index) 
								for (DispOrder dispOrder : dispOrderListByPrevPatientId) {
									dispOrder.setPatient(dispOrder.getPharmOrder().getMedOrder().getPatient());
								}
								
								patEpisode.setStatus(PatientEpisodeStatus.Completed);
								
							} catch (PasException e) {
								patEpisode.setStatus(PatientEpisodeStatus.PasException);
								if (logger.isDebugEnabled()) {
									logger.debug("No patient information if found from OPAS by patient key: " + patEpisode.getPatKey());
								}
							}
							
						} else { // exception case, by orgPatId
								
					    	StringBuilder orgPatIdSqlSb = new StringBuilder();
							
					    	orgPatIdSqlSb.append("select o from DispOrder o") // 20120213 index check : MedOrder.orgPatientId : I_MED_ORDER_04
							    			.append(" where o.pharmOrder.medOrder.orgPatientId = :patId")
							    			.append(" and o.pharmOrder.medOrder.status not in :status");
							
					    	if (patEpisode.getType() == PatientEpisodeType.MergeHkid) {
					    		orgPatIdSqlSb.append(" and o.pharmOrder.medOrder.patHospCode = :patHospCode");
					    	} else if (patEpisode.getType() == PatientEpisodeType.MoveEpisode) {
					    		orgPatIdSqlSb.append(" and o.pharmOrder.medOrder.patHospCode = :patHospCode");
					    		orgPatIdSqlSb.append(" and o.pharmOrder.medCase.caseNum = :caseNum");
					    	}
							
					    	Query orgPatIdQuery = em.createQuery(orgPatIdSqlSb.toString())
													.setParameter("patId", prevPatient.getId())
													.setParameter("status", Arrays.asList(MedOrderStatus.SysDeleted, MedOrderStatus.Deleted));
					    	if (patEpisode.getType() == PatientEpisodeType.MergeHkid) {
					    		orgPatIdQuery.setParameter("patHospCode", patEpisode.getPatHospCode());
					    	} else if (patEpisode.getType() == PatientEpisodeType.MoveEpisode) {
					    		orgPatIdQuery.setParameter("patHospCode", patEpisode.getPatHospCode());
					    		orgPatIdQuery.setParameter("caseNum", patEpisode.getCaseNum());
					    	}
					    	
					    	orgPatIdQuery.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder.patient");
					    	
					    	List<DispOrder> dispOrderListByOrgPatientId = orgPatIdQuery.getResultList();
					    	
							List<MedOrder> medOrderListByOrgPatientId = convertDistinctMedOrderList(dispOrderListByOrgPatientId);
							
							if (!medOrderListByOrgPatientId.isEmpty()) {
								try {
									Patient newPatient = retrievePatientByPatKey(patEpisode.getPatKey());
									if (newPatient == null) { // pas is down, then it may be null
										throw new PasException(new Exception());
									}
									
									newPatient.setHkid(StringUtils.trim(patEpisode.getHkid()));
									
									for (MedOrder medOrder: medOrderListByOrgPatientId) {
										if (newPatient != medOrder.getPatient()) {
											medOrder.setPrevPatientId(medOrder.getPatient().getId());
											medOrder.setPatient(newPatient);
										}
									}
									
									// update patient patient profile
									// (note: must be before updating DO patient, because old patient id (in DO) is required here)
									Map<String, Pair<Long, Long>> movePatientIdPairByOrderNumMap = new HashMap<String, Pair<Long, Long>>();
									for (DispOrder dispOrder : dispOrderListByOrgPatientId) {
										Pair<Long, Long> movePatientIdPair = new Pair<Long, Long>(dispOrder.getPatient().getId(), dispOrder.getPharmOrder().getMedOrder().getPatient().getId());
										movePatientIdPairByOrderNumMap.put(dispOrder.getPharmOrder().getMedOrder().getOrderNum(), movePatientIdPair);
									}
									corpCddhService.updatePatientProfilePatientId(movePatientIdPairByOrderNumMap);
									
									// copy patKey and hkid to DispOrder
									// (DispOrder columns is used for creating SQL index) 
									for (DispOrder dispOrder : dispOrderListByOrgPatientId) {
										dispOrder.setPatient(dispOrder.getPharmOrder().getMedOrder().getPatient());
									}
									
									patEpisode.setStatus(PatientEpisodeStatus.Completed);
									
								} catch (PasException e) {
									patEpisode.setStatus(PatientEpisodeStatus.PasException);
									if (logger.isDebugEnabled()) {
										logger.debug("No patient information if found from OPAS by patient key: " + patEpisode.getPatKey());
									}
								}
							} else {
								patEpisode.setStatus(PatientEpisodeStatus.NoRxAssociated);
							}
						}
					} else {
						patEpisode.setStatus(PatientEpisodeStatus.NoRxAssociated);
					}
				}
			}
		}
		
		// note: do not sync updated Patient of MedOrder to cluster because it is used in CORP for CDDH only,
		//       and CMS will update the Patient when saving order
		
		updatePatEpisodeProp(maxTrxDate);
		
		Date endDatetime = new Date();
        logger.info("process record complete..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - batchDate.getTime()) + "ms)");
	}

	private void updatePatEpisodeProp(Date maxDate) {
		CorporateProp lastTrxDateProp = retrieveProp("batch.patientEpisode.lastTrxDate");
		lastTrxDateProp.setValue(DateTimeFormat.forPattern(
				lastTrxDateProp.getProp().getValidation()).print(new DateTime(maxDate)));
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
}
