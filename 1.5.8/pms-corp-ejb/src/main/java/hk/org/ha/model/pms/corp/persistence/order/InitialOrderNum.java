package hk.org.ha.model.pms.corp.persistence.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INITIAL_ORDER_NUM")
public class InitialOrderNum {

	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
    private String patHospCode;

	@Column(name = "ORDER_NUM", nullable = false)
	private Integer orderNum;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
}
