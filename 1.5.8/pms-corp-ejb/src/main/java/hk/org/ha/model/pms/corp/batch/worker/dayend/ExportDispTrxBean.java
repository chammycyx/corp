package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.vo.label.BaseLabel;

import java.io.BufferedWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;


/** 
 * Session Bean implementation class ExportDispTrxBean
 */
@AutoCreate
@Stateless
@Name("exportDispTrx")
@MeasureCalls
public class ExportDispTrxBean implements ExportDispTrxLocal {

	private static final char SPACE = ' ';			
	private static final int ITEM_NUM_LENGTH = 3;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
	private DateFormat timeFormat = new SimpleDateFormat("HHmmss");
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
    public String getPhsFormatHkid(String hkid) {
		if (hkid == null) {
			return StringUtils.EMPTY;
		}
		
		if (hkid.length() == 8) {
			StringBuilder formatHkid = new StringBuilder();
			formatHkid.append(SPACE);
			formatHkid.append(hkid);
			return formatHkid.toString();
		} else {
			return hkid;
		}
	}
    
	public String getPhsFormatCaseNum(MedCase medCase){
		if (medCase == null || medCase.getCaseNum() == null) {
			return StringUtils.EMPTY;
		}
		
		if (medCase.getCaseNum().length() == 11) {
			StringBuilder formatCaseNum = new StringBuilder();
			formatCaseNum.append(SPACE);
			formatCaseNum.append(medCase.getCaseNum());
			return formatCaseNum.toString();
		} else {
			return medCase.getCaseNum();
		}
	}
	
	private String getPasBedNum(MedCase medCase){
		if (medCase == null || medCase.getPasBedNum() == null) {
			return StringUtils.EMPTY;
		}
		
		return StringUtils.defaultString(medCase.getPasBedNum());
	}
	
	private String getPasSubSpecCode(MedCase medCase) {
		if (medCase == null) {
			return StringUtils.EMPTY;
		}
		
		return StringUtils.defaultString(medCase.getPasSubSpecCode());
	}
	
	private String getDispOrderIdforHospTrx(Integer dispOrderNum) {
		if (dispOrderNum == null) {
			return StringUtils.EMPTY;
		}
		
		return StringUtils.right(dispOrderNum.toString(), 5);
	}
	
	public String getTicketNum(Ticket ticket) {
		
		if (ticket == null) {
			return StringUtils.EMPTY;
		}
		
		return ticket.getTicketNum();
	}
	
	private String getTicketIssueDate(Ticket ticket) {
		
		if (ticket == null) {
			return StringUtils.EMPTY;
		}
		
		return dateFormat.format(ticket.getCreateDate());
	}
	
	private String getTicketIssueTime(Ticket ticket) {
		
		if (ticket == null) {
			return StringUtils.EMPTY;
		}
		
		return timeFormat.format(ticket.getCreateDate());
	}
	
	private String getDateString(Date date) {
		
		if (date == null) {
			return StringUtils.EMPTY;
		}
		
		return dateFormat.format(date);
	}
	
	private String getTimeString(Date date) {
		
		if (date == null) {
			return StringUtils.EMPTY;
		}
		
		return timeFormat.format(date);
	}
	
	private String getSrcId(MedOrderDocType medOrderDocType) {
		if (medOrderDocType == null) {
			return StringUtils.EMPTY;
		}
		
		if (medOrderDocType == MedOrderDocType.Normal) {
			return "13";
		} else {
			return "03";
		}
	}
	
	private String getDispLabelItemDesc(BaseLabel baseLabel) {
		if (baseLabel == null) {
			return StringUtils.EMPTY;
		}
		
		return StringUtils.defaultString(baseLabel.getItemDesc());
	}
	
	private String getUnitPrice(BigDecimal unitPrice) {
		if (unitPrice == null) {
			return StringUtils.EMPTY;
		}
		
		return unitPrice.setScale(4, BigDecimal.ROUND_DOWN).toString();
	}

	private String getDrugGroup(String itemCode) {
		if (itemCode == null) {
			return StringUtils.EMPTY;
		}
		
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		
		if (dmDrug == null || dmDrug.getDrugGroup() == null) {
			return StringUtils.EMPTY;
		}
		
		return dmDrug.getDrugGroup();
	}
	
	private String getPrintLang(PrintLang printLang) {
		
		if (printLang == null) {
			return StringUtils.EMPTY;
		}
		
		if (printLang == PrintLang.Eng) {
			return "E";
		} else {
			return "C";
		}
	}
	
	public void createDispHdrInterface(
			int countOutTrx,
			int countInTrx,
			BufferedWriter edsHdrWriter) throws Exception {

    	edsHdrWriter.write(
    			String.format(
	        			"%1$9d%2$9d%3$9s%4$9s%5$9s", 
	        			countInTrx, 
	        			countOutTrx, 
	        			StringUtils.EMPTY, 
	        			StringUtils.EMPTY, 
	        			StringUtils.EMPTY));
    	edsHdrWriter.newLine();
	}
	
	private String convertToString(Integer integer) {
		if (integer == null) {
			return StringUtils.EMPTY;
		}
		
		return integer.toString();
	}
	
	public Boolean validTransaction(DispOrderItem dispOrderItem, List<String> exceptItemList) throws Exception {
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		Workstore workstore = dispOrder.getWorkstore();
		
		if (dispOrder.getStatus() != DispOrderStatus.Issued) {
			return Boolean.FALSE;
		}
		
		if (exceptItemList.contains(pharmOrderItem.getItemCode())) {
			return Boolean.FALSE;
		}
		
		if (dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
			// for IPMOE ward stock or danger drug item with with reDispFlag = N, DOI status 
			// is DispOrderItemStatus.KeepRecord and will be filtered out
			return Boolean.FALSE;
		}
		
		if (pharmOrderItem.getCapdVoucherFlag()) {
			return Boolean.FALSE;
		}
		
		// check item num length
		if (pharmOrderItem.getItemNum().toString().length() > ITEM_NUM_LENGTH) {
			throw new Exception("ExportDispTrxBean: itemNum in pharmOrderItem is over 3 digits: " +
					"dispHospCode="+ workstore.getHospCode() + ", " +
					"dispOrderNum=" + dispOrder.getDispOrderNum() + ", " +
					"itemNum=" + pharmOrderItem.getItemNum() + ", " +
					"dispOrderItemId=" + dispOrderItem.getId());
		}
		
		return Boolean.TRUE;
	}
	
	private void createDispTrxInterface(
			String hospCode,
			String workstoreCode,
			List<DispOrderItem> dispOrderItemList,
			BufferedWriter hospTrxWriter,
			List<String> exceptItemList) throws Exception {
		
        for (DispOrderItem dispOrderItem: dispOrderItemList) {
        	DispOrder dispOrder = dispOrderItem.getDispOrder();
        	PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
        	PharmOrder pharmOrder = dispOrder.getPharmOrder();
        	
        	if (validTransaction(dispOrderItem, exceptItemList)) {
        		
            	MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
            	Workstore workstore = dispOrder.getWorkstore();	
            	MedOrder medOrder = pharmOrder.getMedOrder();
            	MedCase medCase = pharmOrder.getMedCase();
            	Patient patient = pharmOrder.getPatient();
            	BaseLabel baseLabel = dispOrderItem.getBaseLabel();
            	
            	// create PMS trx file (HospTrx)
            	StringBuilder transactionLineForHospTrx = new StringBuilder();
            	transactionLineForHospTrx.append(String.format("%-1s", StringUtils.EMPTY))
	        				   .append(String.format("%5.5s", getDispOrderIdforHospTrx(dispOrder.getDispOrderNum()))) // require right 5 only
	        				   .append(String.format("%3.3s", pharmOrderItem.getItemNum().toString()))
	        				   .append(dateFormat.format(dispOrder.getTicket().getTicketDate()))
	        				   .append(String.format("%-3.3s", StringUtils.defaultString(workstore.getHospCode())))
	        				   .append(String.format("%-12.12s", StringUtils.defaultString(dispOrderItem.getCreateUser())))
	        				   .append(String.format("%-25.25s", StringUtils.defaultString(patient.getName()))) // TOTEST
	        				   .append(String.format("%-1.1s", medOrder.getOrderType().getDataValue())) // TOTEST: to refer to medOrder.docType
	        				   .append(String.format("%-12.12s", getPhsFormatCaseNum(medCase)))
	        				   .append(String.format("%-10.10s", getTicketNum(dispOrder.getTicket())))
	        				   .append(String.format("%-4.4s", StringUtils.defaultString(dispOrder.getWardCode())))
	        				   .append(String.format("%-5.5s", getPasBedNum(medCase)))
	        				   .append(String.format("%-4.4s", StringUtils.defaultString(dispOrderItem.getChargeSpecCode())))
	        				   .append(String.format("%-6.6s", pharmOrderItem.getItemCode()))
	        				   .append(String.format("%-13.13s", StringUtils.EMPTY))
	        				   .append(String.format("%-3.3s", convertToString(dispOrderItem.getDurationInDay())))
	        				   .append(String.format("%6d", dispOrderItem.getDispQty().intValue()))
	        				   .append(String.format("%1.1s", convertToString(medOrderItem.getNumOfLabel())))
	        				   .append(String.format("%1.1s", getPrintLang(dispOrder.getPrintLang())))
	        				   .append(String.format("%1.1s", StringUtils.EMPTY))
	        				   .append(String.format("%-59.59s", getDispLabelItemDesc(baseLabel)))
	        				   .append(String.format("%-3.3s", StringUtils.defaultString(pharmOrderItem.getFormCode())))
	        				   .append(String.format("%-12.12s", StringUtils.defaultString(pharmOrderItem.getStrength())))
	        				   .append(String.format("%-4.4s", StringUtils.defaultString(pharmOrderItem.getBaseUnit())))
	        				   .append(String.format("%-6.6s", StringUtils.EMPTY))
	        				   .append(String.format("%-2.2s", StringUtils.defaultString(dispOrder.getPatCatCode())))
	        				   .append(String.format("%-4.4s", StringUtils.defaultString(workstore.getWorkstoreCode())))
	        				   .append(String.format("%-7.7s", StringUtils.EMPTY))
	        				   .append(String.format("%-2.2s", StringUtils.defaultString(dispOrderItem.getWarnCode1())))
	        				   .append(String.format("%-2.2s", StringUtils.defaultString(dispOrderItem.getWarnCode2())))
	        				   .append(String.format("%-12.12s", getPhsFormatHkid(patient.getHkid())))
	        				   .append(timeFormat.format(dispOrderItem.getUpdateDate()))
	        				   .append(String.format("%11.11s", getUnitPrice(dispOrderItem.getUnitPrice())))
	        				   .append(String.format("%-5.5s", getDrugGroup(pharmOrderItem.getItemCode())))
	        				   .append(String.format("%-12.12s", StringUtils.defaultString(pharmOrder.getDoctorCode())))
	        				   .append(String.format("%-4.4s", getPasSubSpecCode(medCase)))
	        				   .append(String.format("%-4.4s", StringUtils.EMPTY))
	        				   .append(String.format("%-12.12s", StringUtils.defaultString(patient.getOtherDoc())))
	        				   .append("A")
	        				   .append("I")
	        				   .append(String.format("%-2s", StringUtils.EMPTY))
	        				   .append(String.format("%-2.2s", getSrcId(medOrder.getDocType())))
	        				   .append(String.format("%-6.6s", getTicketIssueDate(dispOrder.getTicket())))
	        				   .append(String.format("%-6.6s", getTicketIssueTime(dispOrder.getTicket())))
	        				   .append(dateFormat.format(dispOrderItem.getCreateDate()))
	        				   .append(timeFormat.format(dispOrderItem.getCreateDate()))
	        				   .append(String.format("%-12.12s", StringUtils.EMPTY))
	        				   .append(String.format("%-6.6s", getDateString(dispOrderItem.getPickDate())))
	        				   .append(String.format("%-6.6s", getTimeString(dispOrderItem.getPickDate())))
	        				   .append(String.format("%-6.6s", getDateString(dispOrderItem.getAssembleDate())))
	        				   .append(String.format("%-6.6s", getTimeString(dispOrderItem.getAssembleDate())))
	        				   .append(String.format("%-6.6s", getDateString(dispOrder.getCheckDate())))
	        				   .append(String.format("%-6.6s", getTimeString(dispOrder.getCheckDate())))
	        				   .append(String.format("%-6.6s", getDateString(dispOrder.getIssueDate())))
	        				   .append(String.format("%-6.6s", getTimeString(dispOrder.getIssueDate())));
	        	
	        	hospTrxWriter.write(transactionLineForHospTrx.toString());
	        	hospTrxWriter.newLine();
        	}
        }
	}

	public void process(String hospCode, 
    		String workstoreCode,
    		List<DispOrderItem> dispOrderItemList,
    		BufferedWriter hospTrxWriter,
    		List<String> exceptItemList) throws Exception {
		
		createDispTrxInterface(hospCode, workstoreCode, dispOrderItemList, hospTrxWriter, exceptItemList);
	}
}
