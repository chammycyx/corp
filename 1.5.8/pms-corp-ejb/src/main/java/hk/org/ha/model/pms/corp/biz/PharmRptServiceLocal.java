package hk.org.ha.model.pms.corp.biz;
import hk.org.ha.service.biz.pms.corp.interfaces.PharmRptServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface PharmRptServiceLocal extends PharmRptServiceJmsRemote {

}
