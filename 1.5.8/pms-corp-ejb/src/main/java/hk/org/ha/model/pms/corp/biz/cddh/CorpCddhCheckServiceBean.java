package hk.org.ha.model.pms.corp.biz.cddh;

import static hk.org.ha.model.pms.corp.prop.Prop.LABEL_DISPLABEL_LARGELAYOUT_DELTACHANGEINDICATOR_VISIBLE;
import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_ORDER_RECON_STARTDATE_DURATION;
import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_ORDER_RECON_ENDRX_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.PatientProfile;
import hk.org.ha.model.pms.da.vo.cddh.DuplicateItemCheckCriteria;
import hk.org.ha.model.pms.da.vo.cddh.DuplicateItemCheckResult;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.DupChkDrugType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.udt.vetting.RegimenType;
import hk.org.ha.model.pms.vo.cddh.PatientCddhDispItem;
import hk.org.ha.model.pms.vo.cddh.PatientCddhDose;
import hk.org.ha.model.pms.vo.cddh.PatientCddhDoseGroup;
import hk.org.ha.model.pms.vo.cddh.PatientCddhPharmItem;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhCheckServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("corpCddhCheckService")
@MeasureCalls
public class CorpCddhCheckServiceBean implements CorpCddhCheckServiceLocal {

	private static final String IN_PATIENT_CASE_PREFIX = "HN";
	
	private static final int DUPLICATE_CHECK_DAY_IN = 14;
	
	private static final int DUPLICATE_CHECK_DAY_OUT = 3;
	
	private static final DispOrderItemComparator dispOrderItemComparator = new DispOrderItemComparator();
	
	private static final String YES_FLAG = "Y";
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In(required = false)
	private DaCddhCheckServiceJmsRemote daCddhCheckServiceProxy;
	
	@SuppressWarnings("unchecked")
	public Map<Integer, List<DispOrderItem>> retrieveDuplicateDispensedItem(
			String hkid,
			String caseNumber,
			String patHospCode,
			String orderNum,
			Map<Integer, List<PharmOrderItem>> pharmOrderItemMap,
			Boolean cddhLegacyEnquiryEnabled) {
		
		// retrieve friend club hospCode list
		List<String> hospCodeList = em.createQuery(
				"select o.hospCode from HospitalMapping o" + // 20120214 index check : none
				" where o.patHospCode = :patHospCode" +
				" and o.status = :status")
				.setParameter("patHospCode", patHospCode)
				.setParameter("status", RecordStatus.Active)
				.getResultList();
		
		if (!hospCodeList.contains(patHospCode)) {
			hospCodeList.add(patHospCode);
		}
		
		// check caseNum is in/out patient
		int pastDay;
		String caseNum;
		if (caseNumber != null && caseNumber.startsWith(IN_PATIENT_CASE_PREFIX)) { // in-patient case
			pastDay = DUPLICATE_CHECK_DAY_IN;
			caseNum = caseNumber; 
		}
		else { // out-patient case
			pastDay = DUPLICATE_CHECK_DAY_OUT;
			caseNum = null; // does not check caseNum
		}

		// start, end date
		DateTime dt = new DateTime();
		Date startDate = dt.minusDays(pastDay).toDate();
		Date endDate = dt.plusDays(1).toDate();

		List<List<DispOrderItem>> legacyDuplicateItemList = new ArrayList<List<DispOrderItem>>();

		if (cddhLegacyEnquiryEnabled) {

			List<DuplicateItemCheckCriteria> duplicateItemCheckCriteriaList = new ArrayList<DuplicateItemCheckCriteria>();
			
			for (Map.Entry<Integer, List<PharmOrderItem>> entry : pharmOrderItemMap.entrySet()) {
				
				Set<String> itemCodeSet    = new HashSet<String>();
				Set<String> displayNameSet = new HashSet<String>();
				
				for (PharmOrderItem pharmOrderItem : entry.getValue()) {
					
					if (pharmOrderItem.getDupChkDrugType() == DupChkDrugType.Oral) {
						if (displayNameSet.contains(pharmOrderItem.getDupChkDisplayName())) {
							continue;
						}
						displayNameSet.add(pharmOrderItem.getDupChkDisplayName());
					}
					else {
						if (itemCodeSet.contains(pharmOrderItem.getItemCode())) {
							continue;
						}
						itemCodeSet.add(pharmOrderItem.getItemCode());
					}
					
					duplicateItemCheckCriteriaList.add(new DuplicateItemCheckCriteria(
							pharmOrderItem.getDupChkDrugType(),
							pharmOrderItem.getItemCode(),
							pharmOrderItem.getDupChkDisplayName(),
							hospCodeList,
							hkid,
							caseNum,
							startDate,
							endDate,
							orderNum,
							patHospCode
						));
				}
			}

			for (DuplicateItemCheckResult duplicateItemCheckResult : daCddhCheckServiceProxy.retrieveDispOrderItemListByDispOrderForDuplicateItemCheck(duplicateItemCheckCriteriaList)) {
				legacyDuplicateItemList.add(duplicateItemCheckResult.getDispOrderItemList());
			}
		}
		
		Map<Integer, List<DispOrderItem>> duplicateItemListMap = new HashMap<Integer, List<DispOrderItem>>();
		
		for (Map.Entry<Integer, List<PharmOrderItem>> entry : pharmOrderItemMap.entrySet()) {
		
			Integer orgItemNum = entry.getKey();
			
			duplicateItemListMap.put(orgItemNum, new ArrayList<DispOrderItem>());

			Set<String> itemCodeSet    = new HashSet<String>();
			Set<String> displayNameSet = new HashSet<String>();
			
			// retrieve dispOrderItem list
			for (PharmOrderItem pharmOrderItem : entry.getValue()) {

				if (pharmOrderItem.getDupChkDrugType() == DupChkDrugType.Oral) {
					if (displayNameSet.contains(pharmOrderItem.getDupChkDisplayName())) {
						continue;
					}
					displayNameSet.add(pharmOrderItem.getDupChkDisplayName());
				}
				else {
					if (itemCodeSet.contains(pharmOrderItem.getItemCode())) {
						continue;
					}
					itemCodeSet.add(pharmOrderItem.getItemCode());
				}
				
				Set<DispOrderItem> dispOrderItemSet = new HashSet<DispOrderItem>();
				
				// from revamp
				dispOrderItemSet.addAll(retrieveDispOrderItemListForDuplicateItemCheck(
						pharmOrderItem.getDupChkDrugType(),
						pharmOrderItem.getItemCode(),
						pharmOrderItem.getDupChkDisplayName(),
						hospCodeList,
						hkid,
						caseNum,
						startDate,
						endDate,
						orderNum
				));
				
				for (DispOrderItem doi : dispOrderItemSet) {
					doi.setCddhDrugDesc(doi.getBaseLabel().getItemDesc());
				}
				
				if (cddhLegacyEnquiryEnabled) {
					dispOrderItemSet.addAll(legacyDuplicateItemList.get(0));
					legacyDuplicateItemList.remove(0);
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug("orgItemNum:#0, size:#1", orgItemNum, dispOrderItemSet.size());
				}
				
				List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>(dispOrderItemSet);
				Collections.sort(dispOrderItemList, dispOrderItemComparator); // sort by ticketDate

				if (!dispOrderItemList.isEmpty()) {
					dispOrderItemList.get(0).setSplitFlag(YES_FLAG);
				}
				
				duplicateItemListMap.get(orgItemNum).addAll(dispOrderItemList);
			}
		}
		
		for (List<DispOrderItem> dispOrderItemList : duplicateItemListMap.values()) {
			for (DispOrderItem dispOrderItem : dispOrderItemList) {
				dispOrderItem.getDispOrder().loadChild();
			}
		}

		return duplicateItemListMap;
	}

	@SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveDispOrderItemListForDuplicateItemCheck (
			DupChkDrugType dupChkDrugType,
			String itemCode,
			String dupChkDisplayName,
			List<String> hospCodeList,
			String hkid,
			String caseNum,
			Date startDate,
			Date endDate,
			String orderNum) {

		StringBuilder sb = new StringBuilder(
				"select o from DispOrderItem o" + // 20150422 index check : DispOrder.hospCode,ticketDate,patient : I_DISP_ORDER_19
				" where o.dispOrder.ticketDate between :startDate and :endDate" +
				" and o.dispOrder.patient.hkid = :hkid" +
				" and o.dispOrder.hospCode in :hospCode " +
				" and o.dispOrder.status = :dispOrderStatus" +
				" and o.dispOrder.adminStatus = :dispOrderAdminStatus" +
				" and o.dispOrder.pharmOrder.medOrder.prescType in :prescType"
			);
	
		if (dupChkDrugType == DupChkDrugType.Oral) {
			sb.append(" and o.pharmOrderItem.dupChkDrugType = :dupChkDrugType");
			sb.append(" and o.pharmOrderItem.dupChkDisplayName = :dupChkDisplayName");
		}
		else {
			sb.append(" and o.pharmOrderItem.itemCode = :itemCode");
		}
	
		if (caseNum != null) {
			sb.append(" and o.dispOrder.pharmOrder.medCase.caseNum = :caseNum");
		}
	
		if (orderNum != null) {
			sb.append(" and o.dispOrder.pharmOrder.medOrder.orderNum <> :orderNum");
		}
		
		sb.append(" order by o.dispOrder.ticketDate desc, o.pharmOrderItem.itemNum");
	
		Query query = em.createQuery(sb.toString());

		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setParameter("hkid", hkid);
		query.setParameter("hospCode", hospCodeList);
		query.setParameter("dispOrderStatus", DispOrderStatus.Issued);
		query.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal);
		query.setParameter("prescType", MedOrderPrescType.ALL_OUT_PATIENT);
		
	
		if (dupChkDrugType == DupChkDrugType.Oral) {
			query.setParameter("dupChkDrugType", DupChkDrugType.Oral);
			query.setParameter("dupChkDisplayName", dupChkDisplayName);
		}
		else {
			query.setParameter("itemCode", itemCode);
		}
	
		if (caseNum != null) {
			query.setParameter("caseNum", caseNum);
		}
	
		if (orderNum != null) {
			query.setParameter("orderNum", orderNum);
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("sb=#0", sb.toString());
		}
		
		return query.getResultList();
	}
	
    public Map<Long, DeltaChangeType> updateDeltaChangeTypeForLabel(DispOrder dispOrder) {

    	if ( ! LABEL_DISPLABEL_LARGELAYOUT_DELTACHANGEINDICATOR_VISIBLE.get(dispOrder.getWorkstore(), Boolean.FALSE)) {
    		return new HashMap<Long, DeltaChangeType>();
    	}
    	
    	MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
    	
    	if (medOrder.getPatient() == null || medOrder.getPatient().getPatKey() == null) {
    		// skip unknown patient order
    		return new HashMap<Long, DeltaChangeType>();
    	}

    	PatientProfile patientProfile = em.find(PatientProfile.class, medOrder.getPatient().getId());

    	// group patient cddh pharm item by item code exists in disp order 
    	Set<String> dispOrderItemCodeSet = new HashSet<String>();
    	for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
    		dispOrderItemCodeSet.add(dispOrderItem.getPharmOrderItem().getItemCode());
    	}    	

		Map<String, List<PatientCddhPharmItem>> patientCddhPharmItemListByItemCodeMap = new HashMap<String, List<PatientCddhPharmItem>>();
    	
    	if (patientProfile != null) {
    		// note: patient profile is null when a new patient issue first order in EDS mode, since patient profile record
    		//       is inserted during issue
			for (PatientCddhPharmItem patientCddhPharmItem : patientProfile.getPatientCddh().getPatientCddhPharmItemList()) {
				if (dispOrderItemCodeSet.contains(patientCddhPharmItem.getItemCode())) {
					List<PatientCddhPharmItem> patientCddhPharmItemList;
					if (patientCddhPharmItemListByItemCodeMap.containsKey(patientCddhPharmItem.getItemCode())) {
						patientCddhPharmItemList = patientCddhPharmItemListByItemCodeMap.get(patientCddhPharmItem.getItemCode());
					} else {
						patientCddhPharmItemList = new ArrayList<PatientCddhPharmItem>();
						patientCddhPharmItemListByItemCodeMap.put(patientCddhPharmItem.getItemCode(), patientCddhPharmItemList);
					}
					patientCddhPharmItemList.add(patientCddhPharmItem);
				}
			}
    	}
		
		// determine delta change type and update to disp order item
		Map<Long, DeltaChangeType> deltaChangeTypeMap = new HashMap<Long, DeltaChangeType>();
		
    	for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
    		if (dispOrderItem.getPrintFlag()) {
    			continue;
    		}
    		
    		if (FmStatus.FreeTextEntryItem.getDataValue().equals(dispOrderItem.getPharmOrderItem().getFmStatus())) {
    			continue;
    		}
    		
    		if (dispOrder.getPharmOrder().getChestFlag() && dispOrderItem.getPharmOrderItem().getChestFlag()) {
    			continue;
    		}
    		
    		if (dispOrderItem.getPharmOrderItem().getRegimen().getType() == RegimenType.StepUpDown) {
    			continue;
    		}
    		
			List<PatientCddhPharmItem> patientCddhPharmItemList = patientCddhPharmItemListByItemCodeMap.get(dispOrderItem.getPharmOrderItem().getItemCode());
			if (patientCddhPharmItemList == null) {
				patientCddhPharmItemList = new ArrayList<PatientCddhPharmItem>();
			}
			
			boolean stepUpDownExist = false; 
			for (PatientCddhPharmItem patientCddhPharmItem : patientCddhPharmItemList) {
				if (patientCddhPharmItem.getRegimenType() == RegimenType.StepUpDown) {
					stepUpDownExist = true;
					break;
				}
			}
			if (stepUpDownExist) {
				continue;
			}

			DeltaChangeType deltaChangeType = compareItemChangeByCddh(dispOrder, dispOrderItem, patientCddhPharmItemList);

			String deltaChangeTypeStr = null;
			if (deltaChangeType != null) {
				deltaChangeTypeStr = deltaChangeType.getDataValue();
			}
			dispOrderItem.setDeltaChangeType(deltaChangeTypeStr);
			
			// return delta change type map to sync to PMS 
			deltaChangeTypeMap.put(dispOrderItem.getId(), deltaChangeType);
		}
    	
    	return deltaChangeTypeMap;
    }
    
    private DeltaChangeType compareItemChangeByCddh(DispOrder dispOrder, DispOrderItem dispOrderItem, List<PatientCddhPharmItem> patientCddhPharmItemList) {
		if (patientCddhPharmItemList.isEmpty()) {
			return DeltaChangeType.NewItem;
		}
		
		Set<DeltaChangeType> deltaChangeTypeSet = new HashSet<DeltaChangeType>();
		
		for (PatientCddhPharmItem patientCddhPharmItem : patientCddhPharmItemList) {
			DeltaChangeType deltaChangeType = compareItemChangeByCddhItem(dispOrder, dispOrderItem, patientCddhPharmItem);
			
			if (deltaChangeType != null) {
				deltaChangeTypeSet.add(deltaChangeType);
			}
		}

		if (deltaChangeTypeSet.isEmpty()) {
			// if all CDDH record are free text item or belongs to current saving DO or out of reference period, 
			// comparison is skipped and treat as new item 
			return DeltaChangeType.NewItem;
		} else if (deltaChangeTypeSet.size() == 1) {
			DeltaChangeType retDeltaChangeType = null;
			for (DeltaChangeType deltaChangeType : deltaChangeTypeSet) {
				retDeltaChangeType = deltaChangeType;
			}
			return retDeltaChangeType;
		} else {
			deltaChangeTypeSet.remove(DeltaChangeType.NoChange);

			if (deltaChangeTypeSet.size() == 1) {
				DeltaChangeType retDeltaChangeType = null;
				for (DeltaChangeType deltaChangeType : deltaChangeTypeSet) {
					retDeltaChangeType = deltaChangeType;
				}
				return retDeltaChangeType;
			} else {
				return DeltaChangeType.MultipleChange;
			}
		}
    }
    
    private DeltaChangeType compareItemChangeByCddhItem(DispOrder dispOrder, DispOrderItem dispOrderItem, PatientCddhPharmItem patientCddhPharmItem) {
    	Date currentDate = new Date();
		Date todayWithoutTime = DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH);

    	boolean withinRefPeriodDispItemExist = false;; 
    	for (PatientCddhDispItem patientCddhDispItem : patientCddhPharmItem.getPatientCddhDispItemList()) {
        	if (patientCddhDispItem.getDispOrderId() != dispOrder.getId()) {
        		// skip CDDH record of current saving order 
        		// (note: for refill order, regimen of base order DOI and DOI of other refill of same refill schedule are
        		//        used for comparsion, so DO ID instead of PO ID is used for checking)
        		
        		Integer startDateDuration = VETTING_ORDER_RECON_STARTDATE_DURATION.get(dispOrder.getWorkstore());
        		Date minStartDate = new DateTime(todayWithoutTime).minusDays(startDateDuration).toDate();
        		
        		Integer endDateDuration = VETTING_ORDER_RECON_ENDRX_DURATION.get(dispOrder.getWorkstore());
        		Date minEndDate = new DateTime(todayWithoutTime).minusDays(endDateDuration).toDate();
        		
        		if (patientCddhDispItem.getStartDate().compareTo(minStartDate) >= 0 && 
        				patientCddhDispItem.getStartDate().compareTo(todayWithoutTime) <= 0 &&
    					patientCddhDispItem.getEndDate().compareTo(minEndDate) > 0) {
        			// start date and end date need to be within reference period
            		withinRefPeriodDispItemExist = true;
            		break;
        		}
        	}
    	}
    	if ( ! withinRefPeriodDispItemExist) {
    		// if no within reference period CDDH item which not belongs to current saving DO exists, skip compare 
    		return null;
    	}

    	boolean nonSuspendDispItemExist = false;
    	for (PatientCddhDispItem patientCddhDispItem : patientCddhPharmItem.getPatientCddhDispItemList()) {
        	if (patientCddhDispItem.getDispOrderId() != dispOrder.getId()) {
        		if (patientCddhDispItem.getAdminStatus() == DispOrderAdminStatus.Normal) {
        			nonSuspendDispItemExist = true;
        			break;
        		}
        	}
    	}
    	if ( ! nonSuspendDispItemExist) {
    		return null;
    	}

		if (FmStatus.FreeTextEntryItem.getDataValue().equals(patientCddhPharmItem.getFmStatus())) {
			// for safety only
			// if saving item is free text, this function is not entered; for other saving item, since item code
			// is not free text item code (NOT 01), patient cddh pharm item list does not contain free text item code (NOT 01)
			return null;
    	}
    	
		if (dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().size() != patientCddhPharmItem.getPatientCddhDoseGroupList().size()) {
			// for safety, comparison is skipped if either order or CDDH item is step up down item, so both dose group size must be 1 in this function
			return DeltaChangeType.MultipleChange;
		}

		Set<DeltaChangeType> deltaChangeTypeSet = new HashSet<DeltaChangeType>();
		
		for (int i=0; i<dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().size(); i++) {
			DoseGroup doseGroup = dispOrderItem.getPharmOrderItem().getRegimen().getDoseGroupList().get(i);
			PatientCddhDoseGroup patientCddhDoseGroup = patientCddhPharmItem.getPatientCddhDoseGroupList().get(i);
			
			if (doseGroup.getDoseList().size() != patientCddhDoseGroup.getPatientCddhDoseList().size()) {
				return DeltaChangeType.MultipleChange;
			}
			
			for (int j=0; j<doseGroup.getDoseList().size(); j++) {
				Dose dose = doseGroup.getDoseList().get(j);
				PatientCddhDose patientCddhDose = patientCddhDoseGroup.getPatientCddhDoseList().get(j);

				DeltaChangeType deltaChangeType = compareItemChangeByCddhItemDose(dose, patientCddhDose);

				if (deltaChangeType != null) {
					deltaChangeTypeSet.add(deltaChangeType);
				}
			}
		}
		
		if (deltaChangeTypeSet.isEmpty()) {
			// for safety, no dose is skipped to compare up to now
			return DeltaChangeType.NewItem;
		} else if (deltaChangeTypeSet.size() == 1) {
			DeltaChangeType retDeltaChangeType = null;
			for (DeltaChangeType deltaChangeType : deltaChangeTypeSet) {
				retDeltaChangeType = deltaChangeType;
			}
			return retDeltaChangeType;
		} else {
			deltaChangeTypeSet.remove(DeltaChangeType.NoChange);

			if (deltaChangeTypeSet.size() == 1) {
				DeltaChangeType retDeltaChangeType = null;
				for (DeltaChangeType deltaChangeType : deltaChangeTypeSet) {
					retDeltaChangeType = deltaChangeType;
				}
				return retDeltaChangeType;
			} else {
				return DeltaChangeType.MultipleChange;
			}
		}
    }
    
    private DeltaChangeType compareItemChangeByCddhItemDose(Dose dose, PatientCddhDose patientCddhDose) {
		int dosageCompareResult = new CompareToBuilder()
										.append( dose.getDosage(), patientCddhDose.getDosage() )
										.append( StringUtils.trimToNull(dose.getDosageUnit()), patientCddhDose.getDosageUnit() )
										.toComparison();

		String doseDailyFreqDesc = null; 
		String patientCddhDoseDailyFreqDesc = null;
		if (dose.getDailyFreq() != null) {
			doseDailyFreqDesc = dose.getDailyFreq().getDesc();
		}
		if (patientCddhDose.getDailyFreq() != null) {
			patientCddhDoseDailyFreqDesc = patientCddhDose.getDailyFreq().getDesc();
		}

		String doseSupplFreqDesc = null; 
		String patientCddhDoseSupplFreqDesc = null;
		if (dose.getSupplFreq() != null) {
			doseSupplFreqDesc = dose.getSupplFreq().getDesc();
		}
		if (patientCddhDose.getSupplFreq() != null) {
			patientCddhDoseSupplFreqDesc = patientCddhDose.getSupplFreq().getDesc();
		}

		int freqCompareResult = new CompareToBuilder()
									.append( StringUtils.trimToNull(doseDailyFreqDesc), patientCddhDoseDailyFreqDesc )
									.append( StringUtils.trimToNull(doseSupplFreqDesc), patientCddhDoseSupplFreqDesc )
									.toComparison();

		int otherCompareResult = new CompareToBuilder()
										.append( dose.getPrn(), patientCddhDose.getPrn() )
										.append( StringUtils.trimToNull(dose.getSiteCode()), patientCddhDose.getSiteCode() )
										.append( StringUtils.trimToNull(dose.getSupplSiteDesc()), patientCddhDose.getSupplSiteDesc() )
										.toComparison();

		if (dosageCompareResult == 0 && freqCompareResult == 0 && otherCompareResult == 0) {
			return DeltaChangeType.NoChange;
		} else if (dosageCompareResult != 0 && freqCompareResult == 0 && otherCompareResult == 0) {
			return DeltaChangeType.DosageChanged;
		} else if (dosageCompareResult == 0 && freqCompareResult != 0 && otherCompareResult == 0) {
			return DeltaChangeType.FrequencyChanged;
		} else {
			return DeltaChangeType.MultipleChange;
		}
    }
    
	private static class DispOrderItemComparator implements Comparator<DispOrderItem>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare(DispOrderItem item1, DispOrderItem item2) {
			
			Date date1 = item1.getDispOrder().getTicket().getTicketDate();
			Date date2 = item2.getDispOrder().getTicket().getTicketDate();
			
			if (!date1.equals(date2)) {
				return date2.compareTo(date1);
			}
			
			if (!item1.getPharmOrderItem().getItemNum().equals(item2.getPharmOrderItem().getItemNum())) {
				return item1.getPharmOrderItem().getItemNum().compareTo(item2.getPharmOrderItem().getItemNum());
			}
			
			return 0;
		}
		
	}

}
