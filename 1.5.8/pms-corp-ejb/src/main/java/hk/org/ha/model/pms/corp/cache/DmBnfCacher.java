package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmBnf;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmBnfCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmBnfCacher extends BaseCacher implements DmBnfCacherInf {
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;	
	
	private List<DmBnf> cachedDmBnfList;
	    
	private Map<String, DmBnf> dmBnfMap = new HashMap<String, DmBnf>();
	
	@Override
	public void clear() {
		synchronized (this) {		
			cacher = null;
			cachedDmBnfList = null;
			dmBnfMap.clear();
		}
	}
	
	public String getDmBnfDesc(String bnfNum) {
		DmBnf dmBnf = this.getDmBnf(bnfNum);
		if (dmBnf != null) {
			return dmBnf.getBnfDesc();
		} else {
			return "";
		}
	}
	
	public DmBnf getDmBnf(String bnfNum) {
		this.getDmBnfList();

		return dmBnfMap.get(bnfNum);
	}
	
	private List<DmBnf> getDmBnfList() 
	{
		synchronized (this)
		{
			List<DmBnf> dmDrugBnfList = (List<DmBnf>) this.getCacher().getAll();

			// check whether the Full DmDrugList expire
			if (cachedDmBnfList == null || (cachedDmBnfList != dmDrugBnfList)) 
			{
				// hold the reference
				cachedDmBnfList = dmDrugBnfList;
				
				this.buildDmBnfKeyMap(cachedDmBnfList);
			}			
		
			return cachedDmBnfList;
		}		
	}
	
	private void buildDmBnfKeyMap(List<DmBnf> dmBnfList)
	{
		dmBnfMap.clear();
		
		for (DmBnf dmBnf : dmBnfList) {
			String key = dmBnf.getBnfNo();
			dmBnfMap.put(key, dmBnf);
		}		
	}
			
    private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
  }

	private class InnerCacher extends AbstractCacher<String, DmBnf>
	{		      
        public InnerCacher(int expireTime) {
              super(expireTime);              
        }
		
		@Override
		public DmBnf create(String bnfNo) {
			this.getAll();
			return this.internalGet(bnfNo);
		}

		@Override
		public Collection<DmBnf> createAll() {
			return dmsPmsServiceProxy.retrieveDmBnfList();
		}

		@Override
		public String retrieveKey(DmBnf dmBnf) {
			return dmBnf.getBnfNo();
		}		
	}
	
	public static DmBnfCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmBnfCacherInf) Component.getInstance("dmBnfCacher", ScopeType.APPLICATION);
    }

		
}
