package hk.org.ha.model.pms.corp.biz.order;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.da.exception.fcs.DaFcsException;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.charging.FcsSfiInvoiceActionType;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsMpSfiInvoiceInfo;
import hk.org.ha.model.pms.vo.charging.FcsSfiInvoice;
import hk.org.ha.model.pms.vo.charging.FcsSfiInvoiceForceProceedInfo;
import hk.org.ha.model.pms.vo.charging.FcsSfiInvoiceInfo;
import hk.org.ha.model.pms.vo.charging.FcsSfiInvoiceItem;
import hk.org.ha.model.pms.vo.charging.FcsSfiInvoiceUncollectInfo;
import hk.org.ha.service.pms.da.interfaces.fcs.DaFcsRepServiceJmsRemote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("fcsManager")
@MeasureCalls
public class FcsManagerBean implements FcsManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@In
	private DaFcsRepServiceJmsRemote daFcsRepServiceProxy;
	
	public void saveFcsSfiInvoice(List<Invoice> voidInvoiceList, List<Invoice> newInvoiceList){
		Invoice newSfiInvoice = null;
		Invoice voidSfiInvoice = null;
		
		for (Invoice invoice : newInvoiceList) {
			if (invoice.getDocType() == InvoiceDocType.Sfi) {
				newSfiInvoice = invoice;
			}
		}
		
		for (Invoice invoice : voidInvoiceList) {
			if (invoice.getDocType() == InvoiceDocType.Sfi) {
				voidSfiInvoice = invoice;
			}
		}
		
		// check exceptional case
		if (newSfiInvoice != null && voidSfiInvoice != null &&
			StringUtils.equals(newSfiInvoice.getInvoiceNum(), voidSfiInvoice.getInvoiceNum())) {
			//no change in SFI Invoice content after order change
			this.updateFcsSfiInvoiceForceProceedInfo(newSfiInvoice, true);
		} else if (newSfiInvoice != null) {
			// new SFI invoice on new order or after order change
			this.insertFcsSfiInvoice(newSfiInvoice);
		} else if (voidSfiInvoice != null) {
			// no SFI Invoice after order change
			this.updateFcsSfiInvoiceStatus(voidSfiInvoice, FcsSfiInvoiceActionType.VOID);
		}
	}
	
	private void insertFcsSfiInvoice(Invoice invoice) {
		
		Invoice prevInvoice = null;

		if (invoice.getPrevInvoiceId() != null) {
			prevInvoice = em.find(Invoice.class, invoice.getPrevInvoiceId());
		}
		
		Invoice orgInvoice = null;
		
		if (invoice.getOrgInvoiceId() != null) {
			orgInvoice = em.find(Invoice.class, invoice.getOrgInvoiceId());
		}
		
		FcsSfiInvoice fcsSfiInvoice = new FcsSfiInvoice(invoice, prevInvoice, orgInvoice);

		List<FcsSfiInvoiceItem> list = new ArrayList<FcsSfiInvoiceItem>();		
		for (InvoiceItem invoiceItem : invoice.getInvoiceItemList()) {
			FcsSfiInvoiceItem fcsSfiInvoiceItem = new FcsSfiInvoiceItem(invoiceItem);
			list.add(fcsSfiInvoiceItem);
		}
		fcsSfiInvoice.setFcsSfiInvoiceItemList(list);
		
		try {
			daFcsRepServiceProxy.saveSfiInvoice(fcsSfiInvoice);
		} catch (DaFcsException e) {
			logger.error("calling fcs error" , e);
		}
	}
	
	public void saveFcsSfiInvoiceListForIp(List<Invoice> newInvoiceList, Boolean directLabelPrintFlag){
		if( newInvoiceList == null || newInvoiceList.isEmpty() ){
			return;
		}
		
		if( directLabelPrintFlag ){
			for (Invoice newInvoice: newInvoiceList) {
				if (newInvoice.getDocType() == InvoiceDocType.Sfi) {
					insertFcsSfiInvoice(newInvoice);
				}
			}
		}else{
			for (Invoice updateInvoice: newInvoiceList) {
				if (updateInvoice.getDocType() == InvoiceDocType.Sfi) {
					updateFcsMpSfiInvoiceInfoForIp(updateInvoice);
				}
			}
		}
	}
	
	public void voidFcsSfiInvoice(List<Invoice> voidInvoiceList){
		if( voidInvoiceList == null || voidInvoiceList.isEmpty() ){
			return;
		}
		
		for (Invoice voidInvoice: voidInvoiceList) {
			if (voidInvoice.getDocType() == InvoiceDocType.Sfi) {
				this.updateFcsSfiInvoiceStatus(voidInvoice, FcsSfiInvoiceActionType.VOID);
			}
		}
	}
	
	public void updateFcsSfiInvoiceStatus(DispOrder dispOrder){
		if( dispOrder.getOrderType() == OrderType.OutPatient ){
			if( DispOrderStatus.Issued == dispOrder.getStatus() ){
				FcsSfiInvoiceActionType actionType = getFcsSfiInvoiceActionType(dispOrder.getStatus(), dispOrder.getAdminStatus());
		
				List<Invoice> invoiceList = dispOrder.getInvoiceList();
				for (Invoice invoice: invoiceList) {
					if (invoice.getStatus() != InvoiceStatus.Void && invoice.getDocType() == InvoiceDocType.Sfi) {
						this.updateFcsSfiInvoiceStatus(invoice, actionType);
					}
				}
			}
		}else if ( dispOrder.getOrderType() == OrderType.InPatient ){
			if( DispOrderAdminStatus.Suspended == dispOrder.getAdminStatus() ){
				dispOrder.setAdminStatus(DispOrderAdminStatus.Normal);
			}
			
			FcsSfiInvoiceActionType actionType = getFcsSfiInvoiceActionType(DispOrderStatus.Issued, dispOrder.getAdminStatus());
			List<Invoice> invoiceList = dispOrder.getInvoiceList();
			for (Invoice invoice: invoiceList) {
				if (invoice.getStatus() != InvoiceStatus.Void && invoice.getDocType() == InvoiceDocType.Sfi) {
					this.updateFcsSfiInvoiceStatus(invoice, actionType);
				}
			}
		}
	}
	
	public void updateFcsSfiInvoiceForceProceedInfo(Invoice invoice, Boolean fcsPersistenceEnabled) {
		Invoice corpInvoice = em.find(Invoice.class, invoice.getId());
		corpInvoice.setForceProceedFlag(invoice.getForceProceedFlag());
		corpInvoice.setForceProceedDate(invoice.getForceProceedDate());
		corpInvoice.setForceProceedUser(invoice.getForceProceedUser());
		corpInvoice.setForceProceedCode(invoice.getForceProceedCode());
		corpInvoice.setManualReceiptNum(invoice.getManualReceiptNum());
		corpInvoice.setManualInvoiceNum(invoice.getManualInvoiceNum());

		if( fcsPersistenceEnabled ){
			FcsSfiInvoiceForceProceedInfo fcsSfiInvoiceForceProceedInfo = new FcsSfiInvoiceForceProceedInfo(invoice);
			FcsSfiInvoiceActionType actionType = getFcsSfiInvoiceActionType(corpInvoice.getDispOrder().getStatus(), corpInvoice.getDispOrder().getAdminStatus());
			try {
				if( actionType == FcsSfiInvoiceActionType.ISSUE ){
					daFcsRepServiceProxy.updateSfiInvoiceForceProceedInfoDispStatus(fcsSfiInvoiceForceProceedInfo);
				}else{
					daFcsRepServiceProxy.updateSfiInvoiceForceProceedInfo(fcsSfiInvoiceForceProceedInfo);
				}
			} catch (DaFcsException e) {
				logger.error("calling fcs error" , e);
			}
		}
	}
	
	private void updateFcsSfiInvoiceStatus(Invoice invoice, FcsSfiInvoiceActionType type) {
		FcsSfiInvoiceInfo fcsSfiInvoiceInfo = new FcsSfiInvoiceInfo(invoice);		
		try {
			daFcsRepServiceProxy.updateSfiInvoiceStatus(fcsSfiInvoiceInfo, type);
		} catch (DaFcsException e) {
			logger.error("calling fcs error" , e);
		}
	}
	
	private FcsSfiInvoiceActionType getFcsSfiInvoiceActionType(DispOrderStatus dispOrderStatus, DispOrderAdminStatus dispOrderAdminStatus){
		FcsSfiInvoiceActionType actionType = null;		
		if( dispOrderStatus == DispOrderStatus.Issued ){
			actionType = FcsSfiInvoiceActionType.ISSUE;
			if( dispOrderAdminStatus == DispOrderAdminStatus.Suspended ){
				actionType = FcsSfiInvoiceActionType.SUSPEND;
			}
		}
		return actionType;
	}
	
	public void updateFcsSfiInvoiceUncollectInfoList(List<Invoice> uncollectInvoiceList){
		List<FcsSfiInvoiceUncollectInfo> uncollectInfoList = new ArrayList<FcsSfiInvoiceUncollectInfo>();
		List<FcsSfiInvoiceUncollectInfo> revUncollectInfoList = new ArrayList<FcsSfiInvoiceUncollectInfo>();
		
		for( Invoice uncollectInvoice : uncollectInvoiceList ){
			FcsSfiInvoiceUncollectInfo uncollectInvoiceInfo = new FcsSfiInvoiceUncollectInfo(uncollectInvoice);
			uncollectInvoiceInfo.setUncollectUser(uncollectInvoice.getDispOrder().getUncollectUser());
			
			if( uncollectInvoice.getDispOrder().getUncollectUser() != null ){
				uncollectInfoList.add(uncollectInvoiceInfo);
			}else{
				revUncollectInfoList.add(uncollectInvoiceInfo);
			}
		}
		
		if( !uncollectInfoList.isEmpty() || !revUncollectInfoList.isEmpty() ){
			try {
				daFcsRepServiceProxy.updateSfiInvoiceUncollectInfoList(uncollectInfoList, revUncollectInfoList);
			} catch (DaFcsException e) {
				logger.error("calling fcs error" , e);
			}
		}
	}

	private void updateFcsMpSfiInvoiceInfoForIp(Invoice invoice) {
		FcsMpSfiInvoiceInfo fcsMpSfiInvoiceInfo = new FcsMpSfiInvoiceInfo(invoice);
		try {
			daFcsRepServiceProxy.updateMpSfiInvoiceInfoForIp(fcsMpSfiInvoiceInfo);
		} catch (DaFcsException e) {
			logger.error("calling fcs error" , e);
		}
	}
	
}
