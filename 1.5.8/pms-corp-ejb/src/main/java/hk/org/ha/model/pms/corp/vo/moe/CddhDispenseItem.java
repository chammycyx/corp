package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CddhDispenseItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Date dispDate;
	
	private Date issueDate;
	
	private Integer dispOrderNum;
	
	private Integer itemNum;
	
	private String itemCode;
	
	private String actionStatus;
	
	private String fmStatus;
	
	private String itemCatCode;	
	
	private String drugName;
	
	private String tradeName;
	
	private String durationText;	
	
	private String dispQtyText;
	
	private String singleDoseInstruction;

	private String multipleDoseInstruction;
	
	private String hospCode;
	
	private String workstoreCode;
	
	private String specCode;
	
	private String wardCode;
	
	private String cddhSpecCode;
	
	private String ticketNum;
	
	private String episodeType;
	
	private String refNum;
	
	private String doctorCode;
	
	private String doctorName;
	
	private String patType;
	
	private String caseNum;
	
	private Boolean multipleDoseFlag; 
	
	private BigDecimal unitPrice;
	
	private String warnCode1;
	
	private String warnCode2;

	private String warnCode3;

	private String warnCode4;
	
	private String warnDesc1;

	private String warnDesc2;
	
	private String warnDesc3;

	private String warnDesc4;
	
	private Boolean dangerDrugFlag;
	
	private String commodityTypeText;
	
	private String remarkText;	
	
	private String mpRemarkText;
	
	private String overrideSeqNum;
	
	private String overrideDesc;

	private String overrideRemark;
	
	private String userCode;
	
	private String remarkUpdateUser;	
	
	private Date remarkUpdateDate;

	public Date getDispDate() {
		return dispDate;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = dispDate;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Integer getDispOrderNum() {
		return dispOrderNum;
	}

	public void setDispOrderNum(Integer dispOrderNum) {
		this.dispOrderNum = dispOrderNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getFmStatus() {
		return fmStatus;
	}

	public void setFmStatus(String fmStatus) {
		this.fmStatus = fmStatus;
	}

	public String getItemCatCode() {
		return itemCatCode;
	}

	public void setItemCatCode(String itemCatCode) {
		this.itemCatCode = itemCatCode;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getDurationText() {
		return durationText;
	}

	public void setDurationText(String durationText) {
		this.durationText = durationText;
	}

	public String getDispQtyText() {
		return dispQtyText;
	}

	public void setDispQtyText(String dispQtyText) {
		this.dispQtyText = dispQtyText;
	}

	public String getSingleDoseInstruction() {
		return singleDoseInstruction;
	}

	public void setSingleDoseInstruction(String singleDoseInstruction) {
		this.singleDoseInstruction = singleDoseInstruction;
	}

	public String getMultipleDoseInstruction() {
		return multipleDoseInstruction;
	}

	public void setMultipleDoseInstruction(String multipleDoseInstruction) {
		this.multipleDoseInstruction = multipleDoseInstruction;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getCddhSpecCode() {
		return cddhSpecCode;
	}

	public void setCddhSpecCode(String cddhSpecCode) {
		this.cddhSpecCode = cddhSpecCode;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getEpisodeType() {
		return episodeType;
	}

	public void setEpisodeType(String episodeType) {
		this.episodeType = episodeType;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getPatType() {
		return patType;
	}

	public void setPatType(String patType) {
		this.patType = patType;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Boolean getMultipleDoseFlag() {
		return multipleDoseFlag;
	}

	public void setMultipleDoseFlag(Boolean multipleDoseFlag) {
		this.multipleDoseFlag = multipleDoseFlag;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public String getWarnDesc1() {
		return warnDesc1;
	}

	public void setWarnDesc1(String warnDesc1) {
		this.warnDesc1 = warnDesc1;
	}

	public String getWarnDesc2() {
		return warnDesc2;
	}

	public void setWarnDesc2(String warnDesc2) {
		this.warnDesc2 = warnDesc2;
	}

	public String getWarnDesc3() {
		return warnDesc3;
	}

	public void setWarnDesc3(String warnDesc3) {
		this.warnDesc3 = warnDesc3;
	}

	public String getWarnDesc4() {
		return warnDesc4;
	}

	public void setWarnDesc4(String warnDesc4) {
		this.warnDesc4 = warnDesc4;
	}

	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}
	
	public String getCommodityTypeText() {
		return commodityTypeText;
	}

	public void setCommodityTypeText(String commodityTypeText) {
		this.commodityTypeText = commodityTypeText;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getMpRemarkText() {
		return mpRemarkText;
	}

	public void setMpRemarkText(String mpRemarkText) {
		this.mpRemarkText = mpRemarkText;
	}

	public String getOverrideSeqNum() {
		return overrideSeqNum;
	}

	public void setOverrideSeqNum(String overrideSeqNum) {
		this.overrideSeqNum = overrideSeqNum;
	}

	public String getOverrideDesc() {
		return overrideDesc;
	}

	public void setOverrideDesc(String overrideDesc) {
		this.overrideDesc = overrideDesc;
	}

	public String getOverrideRemark() {
		return overrideRemark;
	}

	public void setOverrideRemark(String overrideRemark) {
		this.overrideRemark = overrideRemark;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getRemarkUpdateUser() {
		return remarkUpdateUser;
	}

	public void setRemarkUpdateUser(String remarkUpdateUser) {
		this.remarkUpdateUser = remarkUpdateUser;
	}

	public Date getRemarkUpdateDate() {
		return remarkUpdateDate;
	}

	public void setRemarkUpdateDate(Date remarkUpdateDate) {
		this.remarkUpdateDate = remarkUpdateDate;
	} 
}
