package hk.org.ha.model.pms.corp.biz;

import java.util.List;

import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItem;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItemCriteria;
import hk.org.ha.model.pms.corp.vo.moe.ChargeRule;
import hk.org.ha.model.pms.corp.vo.moe.OrderInfo;
import hk.org.ha.model.pms.corp.vo.moe.OrderInfoSearchCriteria;
import hk.org.ha.model.pms.corp.vo.moe.PmsOrderHdr;
import hk.org.ha.model.pms.corp.vo.moe.PrnDuration;
import hk.org.ha.model.pms.corp.vo.moe.RoutePrn;
import hk.org.ha.model.pms.corp.vo.moe.WardStock;
import hk.org.ha.service.biz.pms.corp.interfaces.PharmacyService;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import org.jboss.seam.Component;

/**
 * Session Bean implementation class PharmacyServiceBean
 */
@WebService
@Stateless(mappedName="PharmacyService")
public class PharmacyServiceBean implements PharmacyService {

	@WebMethod
	public PmsOrderHdr retrievePmsOrderHdr(OrderInfoSearchCriteria criteria) {
		return this.getPharmService().retrievePmsOrderHdr(criteria);
	}

	@WebMethod
	public List<RoutePrn> retrieveRoutePrnList(String dispHospCode,
			String dispWorkstore) {
		return this.getPharmService().retrieveRoutePrnList(dispHospCode, dispWorkstore);
	}

	@WebMethod
	public PrnDuration retrievePrnDuration(String dispHospCode,
			String dispWorkstore) {
		return this.getPharmService().retrievePrnDuration(dispHospCode, dispWorkstore);
	}

	@WebMethod
	public ChargeRule retrieveChargeRule(String dispHospCode,
			String dispWorkstore) {
		return this.getPharmService().retrieveChargeRule(dispHospCode, dispWorkstore);
	}

	@WebMethod
	public boolean allowDeleteMpItem(String patHospCode, Long ordNum,
			Integer itemNum) {
		return this.getPharmService().allowDeleteMpItem(patHospCode, ordNum, itemNum);
	}

	@WebMethod
	public List<WardStock> retrieveWardStockList(String patHospCode) {
		return this.getPharmService().retrieveWardStockList(patHospCode);
	}

	@WebMethod
	public OrderInfo retrieveOrderInfo(String patHospCode, Long trxId) {
		return this.getPharmService().retrieveOrderInfo(patHospCode, trxId);
	}
	
	@WebMethod
	public List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria) {
		return this.getPharmRptService().retrieveCddhDispenseInfoList(criteria);
	}

	@WebMethod
	public List<CddhDispenseItem> retrieveCddhDispenseItemList(CddhDispenseItemCriteria criteria) {
		return this.getPharmRptService().retrieveCddhDispenseItemList(criteria);
	}
	
	private PharmServiceLocal getPharmService() {
		return (PharmServiceLocal) lookupComponent(PharmServiceBean.class);
	}	

	private PharmRptServiceLocal getPharmRptService() {
		return (PharmRptServiceLocal) lookupComponent(PharmRptServiceBean.class);
	}	
	
	@SuppressWarnings("unchecked")
	private Object lookupComponent(Class clazz) {		
		return Component.getInstance(clazz, true);
	}     	
}
