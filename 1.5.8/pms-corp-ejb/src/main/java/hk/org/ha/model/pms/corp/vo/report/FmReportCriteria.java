
package hk.org.ha.model.pms.corp.vo.report;

import java.io.Serializable;

import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
import hk.org.ha.model.pms.vo.rx.Fluid;
import hk.org.ha.model.pms.vo.rx.RxDrug;

public class FmReportCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private FmReportInfo fmReportInfo;
	
	private MedOrderFm medOrderFm;
	
	private RxDrug rxDrug;

	private CapdRxDrug capdRxDrug;
	
	private Fluid fluid;
	
	public FmReportInfo getFmReportInfo() {
		return fmReportInfo;
	}

	public void setFmReportInfo(FmReportInfo fmReportInfo) {
		this.fmReportInfo = fmReportInfo;
	}

	public MedOrderFm getMedOrderFm() {
		return medOrderFm;
	}

	public void setMedOrderFm(MedOrderFm medOrderFm) {
		this.medOrderFm = medOrderFm;
	}

	public RxDrug getRxDrug() {
		return rxDrug;
	}

	public void setRxDrug(RxDrug rxDrug) {
		this.rxDrug = rxDrug;
	}
	
	public CapdRxDrug getCapdRxDrug() {
		return capdRxDrug;
	}

	public void setCapdRxDrug(CapdRxDrug capdRxDrug) {
		this.capdRxDrug = capdRxDrug;
	}

	public void setFluid(Fluid fluid) {
		this.fluid = fluid;
	}

	public Fluid getFluid() {
		return fluid;
	}
	
}
