package hk.org.ha.model.pms.corp.persistence.phs;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;
import hk.org.ha.fmk.pms.entity.UpdateDate;
import hk.org.ha.fmk.pms.entity.UpdateUser;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


@Entity
@Table(name = "capd_voucher")
public class CapdVoucher implements Serializable {
	
    @Id
    @Column(name = "institution_code", nullable = false)
    private String institutionCode;
    
    @Column(name = "store_code", nullable = false)
    private String storeCode;
    
    @Id
    @Column(name = "voucher_num", nullable = false)
    private String voucherNum;
    
    @Id
    @Column(name = "voucher_line_num", nullable = false)
    private Integer voucherLineNum;
    
    @Column(name = "voucher_manual_num")
    private String voucherManualNum;

    @Column(name = "voucher_line_status", nullable = false)
    private String voucherLineStatus;
    
    @Column(name = "voucher_date", nullable = false)
	@Temporal(TemporalType.DATE)
    private Date voucherDate;
    
    @Column(name = "supplier_code", nullable = false)
    private String supplierCode;
    
    @Column(name = "item_code", nullable = false)
    private String itemCode;
    
    @Column(name = "request_qty", nullable = false)
    private Integer requestQty;
    
    @Column(name = "pat_class", nullable = false)
    private String patClass;
    
    @Column(name = "pat_type")
    private String patType;
    
    @Column(name = "category")
    private String category;
    
    @Column(name = "first_gen_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date firstGenDate;
    
    @Column(name = "pat_id", nullable = false)
    private String patId;
    
    @Column(name = "case_num", nullable = false)
    private String caseNum;
    
    @Column(name = "pat_name", nullable = false)
    private String patName;
    
    @Column(name = "dispense_order_num")
    private Integer dispenseOrderNum;
    
    @Column(name = "dispense_line_num", nullable = false)
    private Integer dispenseLineNum;
    
    @Column(name = "dispense_ticket_num", nullable = false)
    private String dispenseTicketNum;
    
    @Column(name = "delivery_note_num")
    private String deliveryNoteNum;
    
    @Column(name = "delivery_note_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date deliveryNoteDate;
    
    @Column(name = "match_qty")
    private Integer matchQty;
    
    @Column(name = "order_num", nullable = false)
    private String orderNum;
    
    @Column(name = "line_num")
    private Integer lineNum;
    
    @Column(name = "contract_num")
    private String contractNum;
    
    @Column(name = "contract_suffix")
    private String contractSuffix;
    
    @Column(name = "currency_code")
    private String currencyCode;
    
    @Column(name = "exchange_method")
    private String exchangeMethod;
    
    @Column(name = "exchange_rate")
    private Double exchangeRate;
    
    @Column(name = "last_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date lastDate;
    
    @Column(name = "close_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date closeDate;
    
    @Column(name = "reference_num")
    private String referenceNum;
    
    @Column(name = "error_msg_code")
    private String errorMsgCode;
    
    @Column(name = "seq1", nullable = false)
    private Integer seq1;
    
    @Column(name = "seq2", nullable = false)
    private Integer seq2;
    
	@CreateUser
    @Column(name = "create_user", nullable = false)
    private String createUser;
    
	@CreateDate
    @Column(name = "create_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
	@UpdateUser
    @Column(name = "update_user", nullable = false)
    private String updateUser;
    
	@UpdateDate
    @Column(name = "update_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Version
    @Column(name = "update_version", nullable = false)
    private Integer updateVersion;
    
    @Column(name = "record_status", nullable = false)
    private String recordStatus;
    
    @Column(name = "transmit_flag")
    private String transmitFlag;
    
    @Column(name = "transmit_batch_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date transmitBatchDate;
    
	public CapdVoucher() {
    }

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getVoucherNum() {
		return voucherNum;
	}

	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}

	public Integer getVoucherLineNum() {
		return voucherLineNum;
	}

	public void setVoucherLineNum(Integer voucherLineNum) {
		this.voucherLineNum = voucherLineNum;
	}

	public String getVoucherManualNum() {
		return voucherManualNum;
	}

	public void setVoucherManualNum(String voucherManualNum) {
		this.voucherManualNum = voucherManualNum;
	}

	public String getVoucherLineStatus() {
		return voucherLineStatus;
	}

	public void setVoucherLineStatus(String voucherLineStatus) {
		this.voucherLineStatus = voucherLineStatus;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getRequestQty() {
		return requestQty;
	}

	public void setRequestQty(Integer requestQty) {
		this.requestQty = requestQty;
	}

	public String getPatClass() {
		return patClass;
	}

	public void setPatClass(String patClass) {
		this.patClass = patClass;
	}

	public String getPatType() {
		return patType;
	}

	public void setPatType(String patType) {
		this.patType = patType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getFirstGenDate() {
		return firstGenDate;
	}

	public void setFirstGenDate(Date firstGenDate) {
		this.firstGenDate = firstGenDate;
	}

	public String getPatId() {
		return patId;
	}

	public void setPatId(String patId) {
		this.patId = patId;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public Integer getDispenseOrderNum() {
		return dispenseOrderNum;
	}

	public void setDispenseOrderNum(Integer dispenseOrderNum) {
		this.dispenseOrderNum = dispenseOrderNum;
	}

	public Integer getDispenseLineNum() {
		return dispenseLineNum;
	}

	public void setDispenseLineNum(Integer dispenseLineNum) {
		this.dispenseLineNum = dispenseLineNum;
	}

	public String getDispenseTicketNum() {
		return dispenseTicketNum;
	}

	public void setDispenseTicketNum(String dispenseTicketNum) {
		this.dispenseTicketNum = dispenseTicketNum;
	}

	public String getDeliveryNoteNum() {
		return deliveryNoteNum;
	}

	public void setDeliveryNoteNum(String deliveryNoteNum) {
		this.deliveryNoteNum = deliveryNoteNum;
	}

	public Date getDeliveryNoteDate() {
		return deliveryNoteDate;
	}

	public void setDeliveryNoteDate(Date deliveryNoteDate) {
		this.deliveryNoteDate = deliveryNoteDate;
	}

	public Integer getMatchQty() {
		return matchQty;
	}

	public void setMatchQty(Integer matchQty) {
		this.matchQty = matchQty;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getLineNum() {
		return lineNum;
	}

	public void setLineNum(Integer lineNum) {
		this.lineNum = lineNum;
	}

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getContractSuffix() {
		return contractSuffix;
	}

	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getExchangeMethod() {
		return exchangeMethod;
	}

	public void setExchangeMethod(String exchangeMethod) {
		this.exchangeMethod = exchangeMethod;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public String getReferenceNum() {
		return referenceNum;
	}

	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

	public String getErrorMsgCode() {
		return errorMsgCode;
	}

	public void setErrorMsgCode(String errorMsgCode) {
		this.errorMsgCode = errorMsgCode;
	}

	public Integer getSeq1() {
		return seq1;
	}

	public void setSeq1(Integer seq1) {
		this.seq1 = seq1;
	}

	public Integer getSeq2() {
		return seq2;
	}

	public void setSeq2(Integer seq2) {
		this.seq2 = seq2;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getUpdateVersion() {
		return updateVersion;
	}

	public void setUpdateVersion(Integer updateVersion) {
		this.updateVersion = updateVersion;
	}

	public String getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getTransmitFlag() {
		return transmitFlag;
	}

	public void setTransmitFlag(String transmitFlag) {
		this.transmitFlag = transmitFlag;
	}

	public Date getTransmitBatchDate() {
		return transmitBatchDate;
	}

	public void setTransmitBatchDate(Date transmitBatchDate) {
		this.transmitBatchDate = transmitBatchDate;
	}
}
