package hk.org.ha.model.pms.corp.biz.order;
import hk.org.ha.model.pms.corp.persistence.order.OrderSubscribe;
import hk.org.ha.model.pms.udt.RecordStatus;

import javax.ejb.Local;

@Local
public interface OrderSubscribeManagerLocal {

	public Boolean unsubscribeMedOrder(String orderNum, String system);
		
	public void subscribeMedOrder(String orderNum, String system);
		
	public OrderSubscribe retrieveOrderSubscribe(String orderNum, String system, RecordStatus status);
}
