package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmRoute;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmRouteCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmRouteCacher extends BaseCacher implements DmRouteCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
	
    public DmRoute getRouteByRouteCode(String routeCode) {
		return (DmRoute) this.getCacher().get(routeCode);
	}

	public List<DmRoute> getRouteList() {
		return (List<DmRoute>) this.getCacher().getAll();
	}

	public List<String> getDistinctRouteDesc() {
		HashMap<String, Boolean> routeDescMap = new HashMap<String, Boolean>();
		List<String> retRouteDescList = new ArrayList<String>();
		for (DmRoute dmRoute : getRouteList()) {
			String fullRouteDesc;
			if (dmRoute.getFullRouteDesc() == null) {
				fullRouteDesc = "";
			} else {
				fullRouteDesc = dmRoute.getFullRouteDesc();
			}

			if ( ("B".equals(dmRoute.getUsageType()) || "N".equals(dmRoute.getUsageType())) && 
				 ( ! routeDescMap.containsKey(fullRouteDesc)) ) {
				retRouteDescList.add(fullRouteDesc);
				routeDescMap.put(fullRouteDesc, Boolean.TRUE);
			}
		}
		return retRouteDescList;
	}
	
	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmRoute> {
		
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmRoute create(String routeCode) {
			this.getAll();
			return this.internalGet(routeCode);
		}

		@Override
		public Collection<DmRoute> createAll() {
			return dmsPmsServiceProxy.retrieveDmRouteList();
		}

		@Override
		public String retrieveKey(DmRoute dmRoute) {
			return dmRoute.getRouteCode();
		}
	}

	public static DmRouteCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmRouteCacherInf) Component.getInstance("dmRouteCacher",
				ScopeType.APPLICATION);
	}

}
