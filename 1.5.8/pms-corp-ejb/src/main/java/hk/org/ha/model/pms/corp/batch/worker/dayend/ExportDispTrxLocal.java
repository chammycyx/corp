package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Ticket;

import java.io.BufferedWriter;
import java.util.List;

import javax.ejb.Local;

@Local
public interface ExportDispTrxLocal {
	void process(String hospCode, 
			String workstoreCode, 
			List<DispOrderItem> dispOrderItemList,
			BufferedWriter hospTrxWriter,
			List<String> exceptItemList) throws Exception;
	
	Boolean validTransaction(DispOrderItem dispOrderItem, List<String> exceptItemList) throws Exception;

	String getPhsFormatCaseNum(MedCase medCase);
	
	String getTicketNum(Ticket ticket);
	
    String getPhsFormatHkid(String hkid);
}
