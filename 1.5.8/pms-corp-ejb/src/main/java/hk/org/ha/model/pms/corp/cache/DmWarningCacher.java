package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmBnf;
import hk.org.ha.model.pms.dms.persistence.DmDrugBnf;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmWarningCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmWarningCacher extends BaseCacher implements DmWarningCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

    private InnerCacher cacher;	
    
    private List<DmWarning> cachedDmWarningList;
    
    private List<DmWarning> cachedDmWarningListWithSuspend;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
			cachedDmWarningList = null;
			cachedDmWarningListWithSuspend = null;
		}
	}	
    
	public DmWarning getDmWarningByWarnCode(String warnCode) {
		DmWarning dmWarning = (DmWarning) this.getCacher().get(warnCode);
		if ( dmWarning == null || "Y".equals(dmWarning.getSuspend()) ) {
			return null;
		}
		return dmWarning;
	}

	public DmWarning getDmWarningByWarnCodeWithSuspend(String warnCode) {
		return (DmWarning) this.getCacher().get(warnCode);
	}
	
	public List<DmWarning> getDmWarningListByWarnCode(String prefixWarnCode) {
		List<DmWarning> ret = new ArrayList<DmWarning>();
		for (DmWarning dmWarning : getDmWarningList()) {
			if (dmWarning.getWarnCode().startsWith(prefixWarnCode)) {
				ret.add(dmWarning);
			}
		}
		return ret;
	}
	
	public List<DmWarning> getDmWarningListWithSuspend() 
	{
		return cachedDmWarningListWithSuspend;
	}

	public List<DmWarning> getDmWarningList() 
	{
		synchronized (this)
		{
			List<DmWarning> _dmWarningList = (List<DmWarning>) this.getCacher().getAll();

			// check whether the Full DmWarningList expire
			if (cachedDmWarningListWithSuspend == null || (cachedDmWarningListWithSuspend != _dmWarningList)) 
			{
				// hold the reference
				cachedDmWarningListWithSuspend = _dmWarningList;
				
				this.buildDmWarningList(cachedDmWarningListWithSuspend);
			}
		
			return cachedDmWarningList;
		}		
	}	
	
	private void buildDmWarningList(List<DmWarning> dmWarningListWithSuspend) {
		cachedDmWarningList = new ArrayList<DmWarning>();
		for (DmWarning dmWarning : dmWarningListWithSuspend) {
			if ( "N".equals(dmWarning.getSuspend()) ) {
				cachedDmWarningList.add(dmWarning);
			}
		}
	}
	
    private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmWarning>
	{		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }
		
		@Override
		public DmWarning create(String warnCode) {
			this.getAll();
			return this.internalGet(warnCode);
		}

		@Override
		public Collection<DmWarning> createAll() {
			return dmsPmsServiceProxy.retrieveDmWarningListWithSuspend();
		}

		@Override
		public String retrieveKey(DmWarning dmWarning) {
			return dmWarning.getWarnCode();
		}		
	}
	
	public static DmWarningCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmWarningCacherInf) Component.getInstance("dmWarningCacher", ScopeType.APPLICATION);
    }	
}
