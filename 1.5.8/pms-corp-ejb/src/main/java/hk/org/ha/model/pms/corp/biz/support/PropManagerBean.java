package hk.org.ha.model.pms.corp.biz.support;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("propManager")
@MeasureCalls
public class PropManagerBean implements PropManagerLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private PropHelper propHelper;

	public List<CorporateProp> retrieveCorporatePropList() {
		return propHelper.retrieveCorporatePropList(em);
	}

	public void updateCorporatePropList(List<CorporateProp> corporatePropList, String auditLoggerMessageId) {
		propHelper.updateCorporatePropList(em, corporatePropList, auditLoggerMessageId);
	}
}
