package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmWarning;

import java.util.List;

public interface DmWarningCacherInf extends BaseCacherInf {

	DmWarning getDmWarningByWarnCode(String warnCode);
	
	List<DmWarning> getDmWarningList();
	
	List<DmWarning> getDmWarningListByWarnCode(String prefixWarnCode);
	
	DmWarning getDmWarningByWarnCodeWithSuspend(String warnCode);
	
	List<DmWarning> getDmWarningListWithSuspend();
}
