package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.persistence.report.MpWorkloadStat;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface DayEndRptManagerLocal {
	
	List<DayEndFileExtractRpt> retrieveDayEndFileExtractRptList(String hospCode, String workstoreCode, Date batchDate);
	
	List<MpWorkloadStat> retrieveMpWorkloadStatRptList(Workstore workstore, Date startDate, Date endDate);
}
