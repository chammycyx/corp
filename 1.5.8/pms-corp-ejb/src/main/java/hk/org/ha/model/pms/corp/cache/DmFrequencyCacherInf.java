package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmFrequency;

import java.util.List;

public interface DmFrequencyCacherInf extends BaseCacherInf {

	DmFrequency getDmFrequencyByFreqCode(String freqCode);
	
	List<DmFrequency> getDmFrequencyList();
	
	List<DmFrequency> getDmFrequencyListByFreqCode(String prefixFreqCode);
}
