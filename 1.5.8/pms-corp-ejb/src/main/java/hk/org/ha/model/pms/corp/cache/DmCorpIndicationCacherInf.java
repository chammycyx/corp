package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmCorpIndication;

public interface DmCorpIndicationCacherInf extends BaseCacherInf {
	DmCorpIndication getDmCorpIndication(String corpIndicationCode);	
	String getDmCorpIndicationDesc(String corpIndicationCode);
}
