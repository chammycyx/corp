package hk.org.ha.model.pms.corp.batch.worker.downtime;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.ManualInvoiceNumRange;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.dms.persistence.DmAdminTime;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmForm;
import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dtDbBean")
public class DtDbBean implements DtDbLocal {
        
	private Logger logger = null;
	private Date batchDate = null;
	private Database db = null;
	
	private String jobId = null;

	private static final String PMS_DT_PREFIX = "pms-dt-";
	private static final String TEMPLATE_PREFIX = "template";
	private static final String REFDATA_PREFIX = "refdata";
	private static final String ACCDE_EXT = "accde";
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
    @In
    private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private ApplicationProp applicationProp;

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		
		this.logger = logger;

		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if ( null == params.getDate("batchDate") ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = new DateTime(params.getDate("batchDate")).plusDays(1).toDate();
		}
		
    }
    
    public void beginChunk(Chunk chunk) {
    }

    public void processRecord(Record record) throws Exception {

    	Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);
        
        File templateDir = getDirectory();
        File outDir = getDirectory(jobId, DataDirType.Out);
        File workDir = getDirectory(jobId, DataDirType.Work);
        File errorDir = getDirectory(jobId, DataDirType.Error);
        
		File templateFile = getFile(templateDir, PMS_DT_PREFIX + 
											TEMPLATE_PREFIX + 
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		File workFile = getFile(workDir, PMS_DT_PREFIX +
											dateFormat.format(batchDate) +  
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		File outFile = getFile(outDir, PMS_DT_PREFIX +
											dateFormat.format(batchDate) +  
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		File errorFile = getFile(errorDir, PMS_DT_PREFIX +
											dateFormat.format(batchDate) + 
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		
		FileUtils.copyFile(templateFile, workFile);
    	
		try {
			try {
		        db = Database.open(workFile);
	        
		        updateAccdbDrugTable();
		        updateAccdbFreqTable();
		        updateAccdbSupplFreqTable();
		        updateAccdbRegimenTable();
		        updateAccdbSiteTable();
		        updateAccdbSupplSiteTable();
		        updateAccdbAdminTimeTable();
		        updateAccdbWarningTable();
		        updateAccdbFormTable();
		        updateAccdbFormVerbMappingTable();
		        updateAccdbFormDosageUnitMappingTable();
		        updateAccdbWorkstorePropTable();
		        updateAccdbHospitalPropTable();
		        updateAccdbManualInvoiceNumRangeTable();

			} finally {
				db.close();
			}

		} catch (Exception e) {
			FileUtils.copyFile(workFile, errorFile);
			FileUtils.deleteQuietly(workFile);
			throw e;
		}

		FileUtils.copyFile(workFile, outFile);
		FileUtils.deleteQuietly(workFile);
		
    	Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
        
    }

    public void endChunk() {
    }
    
    @Remove
    public void destroyBatch() {
    }
	
    private void updateAccdbDrugTable() throws IOException{
    	
		List<DmDrug> drugs = dmsPmsServiceProxy.retrieveDmDrugList();	
		
		Table drugsTable = db.getTable("Drug");
		
		List<Object[]> drugList = new ArrayList<Object[]>();
		Map<String, Object> drugRowMap = new HashMap<String, Object>();
		
		for ( Iterator<DmDrug> iterator = drugs.iterator(); iterator.hasNext(); ) {
			DmDrug dmDrug = iterator.next();
			
			drugRowMap.put("itemCode", dmDrug.getItemCode());
			drugRowMap.put("fullDrugDesc",dmDrug.getFullDrugDesc());
			drugRowMap.put("drugName",dmDrug.getDrugName());
			drugRowMap.put("formCode",dmDrug.getFormCode());
			drugRowMap.put("baseUnit",dmDrug.getBaseUnit());
			drugRowMap.put("dispenseDosageUnit",dmDrug.getDmMoeProperty().getDispenseDosageUnit());
			drugRowMap.put("strength",dmDrug.getStrength());
			drugRowMap.put("volumeValue",dmDrug.getVolumeValue());
			drugRowMap.put("volumeUnit",dmDrug.getVolumeUnit());
			drugRowMap.put("warnCode1",dmDrug.getDmMoeProperty().getWarnCode1());
			drugRowMap.put("warnCode2",dmDrug.getDmMoeProperty().getWarnCode2());
			drugRowMap.put("warnCode3",dmDrug.getDmMoeProperty().getWarnCode3());
			drugRowMap.put("warnCode4",dmDrug.getDmMoeProperty().getWarnCode4());
			drugRowMap.put("baseunitToDduRatio",dmDrug.getDmMoeProperty().getBaseunitToDduRatio());
			drugRowMap.put("dosageCompul",dmDrug.getDmDrugProperty().getDosageCompul());
			drugRowMap.put("adminTimeCode",dmDrug.getDmDrugProperty().getAdminTimeCode());
			drugRowMap.put("strengthCompul",dmDrug.getDmDrugProperty().getStrengthCompul());
			
			if (dmDrug.getDrugChargeInfo() != null) {
				
				if (dmDrug.getDrugChargeInfo().getCorpDrugPrice() == null){
					drugRowMap.put("corpDrugPrice",0.0);
				}else{
					drugRowMap.put("corpDrugPrice",dmDrug.getDrugChargeInfo().getCorpDrugPrice());
				}
				
				if (dmDrug.getDrugChargeInfo().getPrivateMarkupFactor() == null){
					drugRowMap.put("privateMarkupFactor",1.0);
				}else{
					drugRowMap.put("privateMarkupFactor",dmDrug.getDrugChargeInfo().getPrivateMarkupFactor());
				}
				
				if (dmDrug.getDrugChargeInfo().getPublicMarkupFactor() == null){
					drugRowMap.put("publicMarkupFactor",1.0);
				}else{
					drugRowMap.put("publicMarkupFactor",dmDrug.getDrugChargeInfo().getPublicMarkupFactor());
				}
				
				drugRowMap.put("privateHandleCharge",dmDrug.getDrugChargeInfo().getPrivateHandleCharge());
				drugRowMap.put("publicHandleCharge",dmDrug.getDrugChargeInfo().getPublicHandleCharge());
				drugRowMap.put("lifeStyleDrugFlag",dmDrug.getDrugChargeInfo().isLifestyleDrug());
				drugRowMap.put("oncologyDrugFlag",dmDrug.getDrugChargeInfo().isOncologyDrug());
			}
			
			drugRowMap.put("fmStatus",dmDrug.getPmsFmStatus().getFmStatus());
			drugRowMap.put("drugKey", dmDrug.getDrugKey());
			
			drugList.add(drugsTable.asRow(drugRowMap));
		}
		
		drugsTable.addRows(drugList);
		db.flush();
		
    }
    
    private void updateAccdbFreqTable() throws IOException{

		List<DmDailyFrequency> frequencies = dmsPmsServiceProxy.retrieveDmDailyFrequencyList();
		
		Table freqTable = db.getTable("Frequency");
		
		List<Object[]> freqList = new ArrayList<Object[]>();
		Map<String, Object> freqRowMap = new HashMap<String, Object>();
		
		for ( Iterator<DmDailyFrequency> iterator = frequencies.iterator(); iterator.hasNext(); ) {
			DmDailyFrequency frequency = iterator.next();
			
			freqRowMap.put("dailyFreqCode", frequency.getDailyFreqCode());
			freqRowMap.put("dailyFreqDesc", frequency.getDailyFreqDesc());
			freqRowMap.put("dailyFreqBlk1", frequency.getDailyFreqBlk1());
			freqRowMap.put("dailyFreqVal1", frequency.getDailyFreqVal1());
			freqRowMap.put("dailyFreqBlk2", frequency.getDailyFreqBlk2());
			freqRowMap.put("labelFreqBlk1Eng", frequency.getLabelFreqBlk1Eng());
			freqRowMap.put("labelFreqVal1Eng", frequency.getLabelFreqVal1Eng());
			freqRowMap.put("labelFreqBlk2Eng", frequency.getLabelFreqBlk2Eng());
			freqRowMap.put("labelFreqBlk1Chi", frequency.getLabelFreqBlk1Chi());
			freqRowMap.put("labelFreqVal1Chi", frequency.getLabelFreqVal1Chi());
			freqRowMap.put("labelFreqBlk2Chi", frequency.getLabelFreqBlk2Chi());
			freqRowMap.put("lowerLimit", frequency.getLowerLimit());
			freqRowMap.put("upperLimit", frequency.getUpperLimit());
			freqRowMap.put("numOfInputValue", frequency.getNumOfInputValue());
			freqRowMap.put("rank", frequency.getRank());
			freqRowMap.put("multiplier", frequency.getMultiplier());
			freqRowMap.put("multiplierType", frequency.getMultiplierType());
			freqRowMap.put("suspend", frequency.getSuspend());
			freqRowMap.put("suspendDate", frequency.getSuspendDate());

			freqList.add(freqTable.asRow(freqRowMap));
		}
		
		freqTable.addRows(freqList);
		db.flush();
    	
    }
    
    private void updateAccdbSupplFreqTable() throws IOException{

		List<DmSupplFrequency> supplFrequencies = dmsPmsServiceProxy.retrieveDmSupplFrequencyList();

		Table supplFreqTable = db.getTable("SupplFrequency");
		
		List<Object[]> supplfreqList = new ArrayList<Object[]>();
		Map<String, Object> supplFreqRowMap = new HashMap<String, Object>();
		
		for ( Iterator<DmSupplFrequency> iterator = supplFrequencies.iterator(); iterator.hasNext(); ) {
			DmSupplFrequency supplFrequency = iterator.next();

			supplFreqRowMap.put("supplFreqCode", supplFrequency.getSupplFreqCode());
			supplFreqRowMap.put("supplFreqDesc", supplFrequency.getSupplFreqDesc());
			supplFreqRowMap.put("supplFreqBlk1", supplFrequency.getSupplFreqBlk1());
			supplFreqRowMap.put("supplFreqVal1", supplFrequency.getSupplFreqVal1());
			supplFreqRowMap.put("supplFreqBlk2", supplFrequency.getSupplFreqBlk2());
			supplFreqRowMap.put("supplFreqVal2", supplFrequency.getSupplFreqVal2());
			supplFreqRowMap.put("supplFreqBlk3", supplFrequency.getSupplFreqBlk3());
			supplFreqRowMap.put("labelSupplFreqBlk1Eng", supplFrequency.getLabelFreqBlk1Eng());
			supplFreqRowMap.put("labelSupplFreqVal1Eng", supplFrequency.getLabelFreqVal1Eng());
			supplFreqRowMap.put("labelSupplFreqBlk2Eng", supplFrequency.getLabelFreqBlk2Eng());
			supplFreqRowMap.put("labelSupplFreqVal2Eng", supplFrequency.getLabelFreqVal2Eng());
			supplFreqRowMap.put("labelSupplFreqBlk3Eng", supplFrequency.getLabelFreqBlk3Eng());
			supplFreqRowMap.put("labelSupplFreqBlk1Chi", supplFrequency.getLabelFreqBlk1Chi());
			supplFreqRowMap.put("labelSupplFreqVal1Chi", supplFrequency.getLabelFreqVal1Chi());
			supplFreqRowMap.put("labelSupplFreqBlk2Chi", supplFrequency.getLabelFreqBlk2Chi());
			supplFreqRowMap.put("labelSupplFreqVal2Chi", supplFrequency.getLabelFreqVal2Chi());
			supplFreqRowMap.put("labelSupplFreqBlk3Chi", supplFrequency.getLabelFreqBlk3Chi());
			supplFreqRowMap.put("lowerLimit", supplFrequency.getLowerLimit());
			supplFreqRowMap.put("upperLimit", supplFrequency.getUpperLimit());
			supplFreqRowMap.put("numOfInputValue", supplFrequency.getNumOfInputValue());
			supplFreqRowMap.put("rank", supplFrequency.getRank());
			supplFreqRowMap.put("regimenCode", supplFrequency.getRegimenCode());
			supplFreqRowMap.put("multiplier", supplFrequency.getMultiplier());
			supplFreqRowMap.put("multiplierType", supplFrequency.getMultiplierType());

			supplfreqList.add(supplFreqTable.asRow(supplFreqRowMap));
		}
		
		supplFreqTable.addRows(supplfreqList);
		db.flush();
    	
    }
    
    private void updateAccdbRegimenTable() throws IOException{

		List<DmRegimen> regimens = dmsPmsServiceProxy.retrieveDmRegimenList();	

		Table regimenTable = db.getTable("Regimen");
		
		List<Object[]> regimenList = new ArrayList<Object[]>();
		Map<String, Object> regimenRowMap = new HashMap<String, Object>();

		for ( Iterator<DmRegimen> iterator = regimens.iterator(); iterator.hasNext(); ) {
			DmRegimen regimen = iterator.next();

			regimenRowMap.put("regimenCode", regimen.getRegimenCode());
			regimenRowMap.put("regimenDesc", regimen.getRegimenDesc());
			regimenRowMap.put("regimenMultiplier", regimen.getRegimenMultiplier());
			regimenRowMap.put("regimenIndex", regimen.getRegimenIndex());

			regimenList.add(regimenTable.asRow(regimenRowMap));
		}
		
		regimenTable.addRows(regimenList);
		db.flush();
    	
    }
    
    private void updateAccdbSiteTable() throws IOException{

		List<DmSite> sites = dmsPmsServiceProxy.retrieveDmSiteList();	

		Table siteTable = db.getTable("Site");
		
		List<Object[]> siteList = new ArrayList<Object[]>();
		Map<String, Object> siteRowMap = new HashMap<String, Object>();

		for ( Iterator<DmSite> iterator = sites.iterator(); iterator.hasNext(); ) {
			DmSite site = iterator.next();

			siteRowMap.put("siteCode", site.getSiteCode());
			siteRowMap.put("siteCategoryCode", site.getSiteCategoryCode());
			siteRowMap.put("siteDescChi", site.getSiteDescChi());
			siteRowMap.put("siteDescEng", site.getSiteDescEng());
			siteRowMap.put("usageType", site.getUsageType());

			siteList.add(siteTable.asRow(siteRowMap));
		}
		
		siteTable.addRows(siteList);
		db.flush();
    	
    }
    
    private void updateAccdbSupplSiteTable() throws IOException{

		List<DmSupplSite> supplSites = dmsPmsServiceProxy.retrieveDmSupplSiteList();	

		Table supplSiteTable = db.getTable("SupplSite");
		
		List<Object[]> siteList = new ArrayList<Object[]>();
		Map<String, Object> siteRowMap = new HashMap<String, Object>();

		for ( Iterator<DmSupplSite> iterator = supplSites.iterator(); iterator.hasNext(); ) {
			DmSupplSite supplSite = iterator.next();

			siteRowMap.put("siteCode", supplSite.getCompId().getSiteCode());
			siteRowMap.put("supplSiteEng", supplSite.getCompId().getSupplSiteEng());
			siteRowMap.put("supplSiteChi", supplSite.getSupplSiteChi());
			siteRowMap.put("multiplier", supplSite.getMultiplier());
			siteRowMap.put("rank", supplSite.getRank());

			siteList.add(supplSiteTable.asRow(siteRowMap));
		}
		
		supplSiteTable.addRows(siteList);
		db.flush();
    	
    }
    
    private void updateAccdbAdminTimeTable() throws IOException{

		List<DmAdminTime> adminTimes = dmsPmsServiceProxy.retrieveDmAdminTimeList();	

		Table adminTimeTable = db.getTable("AdminTime");
		
		List<Object[]> adminTimeList = new ArrayList<Object[]>();
		Map<String, Object> adminTimeRowMap = new HashMap<String, Object>();

		for ( Iterator<DmAdminTime> iterator = adminTimes.iterator(); iterator.hasNext(); ) {
			DmAdminTime adminTime = iterator.next();

			adminTimeRowMap.put("adminTimeCode", adminTime.getAdminTimeCode());
			adminTimeRowMap.put("adminTimeChi", adminTime.getAdminTimeChi());
			adminTimeRowMap.put("adminTimeEng", adminTime.getAdminTimeEng());

			adminTimeList.add(adminTimeTable.asRow(adminTimeRowMap));
		}
		
		adminTimeTable.addRows(adminTimeList);
		db.flush();
    	
    }
    
    private void updateAccdbWarningTable() throws IOException{

		List<DmWarning> warnings = dmsPmsServiceProxy.retrieveDmWarningList();	

		Table warningTable = db.getTable("Warning");
		
		List<Object[]> warningList = new ArrayList<Object[]>();
		Map<String, Object> warningRowMap = new HashMap<String, Object>();

		for ( Iterator<DmWarning> iterator = warnings.iterator(); iterator.hasNext(); ) {
			DmWarning warning = iterator.next();
			if ( warning.getUsageType().equals("O")) {
				continue;
			}

			warningRowMap.put("warnCode", warning.getWarnCode());
			warningRowMap.put("warnMessageEng", warning.getWarnMessageEng());
			warningRowMap.put("warnMessageChi", warning.getWarnMessageChi());
			warningRowMap.put("warnCatCode", warning.getWarnCatCode());
			warningRowMap.put("warnCatDesc", warning.getDmWarningCategory().getWarnCatDesc());

			warningList.add(warningTable.asRow(warningRowMap));
		}
		
		warningTable.addRows(warningList);
		db.flush();
		
    }
    
    private void updateAccdbFormTable() throws IOException{

		List<DmForm> forms = dmsPmsServiceProxy.retrieveDmFormList();	

		Table formTable = db.getTable("Form");
		
		List<Object[]> formList = new ArrayList<Object[]>();
		Map<String, Object> formRowMap = new HashMap<String, Object>();

		for ( Iterator<DmForm> iterator = forms.iterator(); iterator.hasNext(); ) {
			DmForm form = iterator.next();

			formRowMap.put("formCode", form.getFormCode());
			formRowMap.put("labelDesc", form.getLabelDesc());
			formRowMap.put("noun", form.getNoun());
			formRowMap.put("nounChi", form.getNounChi());

			formList.add(formTable.asRow(formRowMap));
		}
		
		formTable.addRows(formList);
		db.flush();
		
    }
    
    private void updateAccdbFormVerbMappingTable() throws IOException{

		List<DmFormVerbMapping> formVerbMappings = dmsPmsServiceProxy.retrieveDmFormVerbMappingList();	

		Table formVerbMappingTable = db.getTable("FormVerbMapping");
		
		List<Object[]> formVerbMappingList = new ArrayList<Object[]>();
		Map<String, Object> formVerbMappingRowMap = new HashMap<String, Object>();

		for ( Iterator<DmFormVerbMapping> iterator = formVerbMappings.iterator(); iterator.hasNext(); ) {
			DmFormVerbMapping formVerbMapping = iterator.next();

			formVerbMappingRowMap.put("formCode", formVerbMapping.getFormCode());
			formVerbMappingRowMap.put("siteCode", formVerbMapping.getSiteCode());
			formVerbMappingRowMap.put("verbEng", formVerbMapping.getVerbEng());
			formVerbMappingRowMap.put("verbChi", formVerbMapping.getVerbChi());
			formVerbMappingRowMap.put("supplementarySite", formVerbMapping.getSupplementarySite());
			formVerbMappingRowMap.put("fullRouteDesc", formVerbMapping.getDmForm().getDmRoute().getFullRouteDesc());
			formVerbMappingRowMap.put("rank", formVerbMapping.getRank());

			formVerbMappingList.add(formVerbMappingTable.asRow(formVerbMappingRowMap));
		}
		
		formVerbMappingTable.addRows(formVerbMappingList);
		db.flush();
		
    }
    
    private void updateAccdbFormDosageUnitMappingTable() throws IOException{

		List<DmFormDosageUnitMapping> formDosageUnitMappings = dmsPmsServiceProxy.retrieveDmFormDosageUnitMappingList();	

		Table formDosageUnitMappingTable = db.getTable("FormDosageUnitMapping");
		
		List<Object[]> formDosageUnitMappingList = new ArrayList<Object[]>();
		Map<String, Object> formDosageUnitMappingRowMap = new HashMap<String, Object>();

		for ( Iterator<DmFormDosageUnitMapping> iterator = formDosageUnitMappings.iterator(); iterator.hasNext(); ) {
			DmFormDosageUnitMapping formDosageUnitMapping = iterator.next();

			formDosageUnitMappingRowMap.put("formCode", formDosageUnitMapping.getCompId().getFormCode());
			formDosageUnitMappingRowMap.put("dosageUnit", formDosageUnitMapping.getCompId().getDosageUnit());
			formDosageUnitMappingRowMap.put("dosageUnitEng", formDosageUnitMapping.getDosageUnitEng());
			formDosageUnitMappingRowMap.put("dosageUnitChi", formDosageUnitMapping.getDosageUnitChi());

			formDosageUnitMappingList.add(formDosageUnitMappingTable.asRow(formDosageUnitMappingRowMap));
		}
		
		formDosageUnitMappingTable.addRows(formDosageUnitMappingList);
		db.flush();
		
    }
    
    @SuppressWarnings("unchecked")
	private void updateAccdbWorkstorePropTable() throws IOException{

		Table workstorePropTable = db.getTable("WorkstoreProp");
		List<Object[]> workstorePropList = new ArrayList<Object[]>();
		Map<String, Object> workstorePropRowMap = new HashMap<String, Object>();
		
		List<WorkstoreProp> phsWorkstorePropList = em.createQuery(
				"select o from WorkstoreProp o") // 20120307 index check : none
				.getResultList();
	
		for (WorkstoreProp workstoreProp : phsWorkstorePropList) {
			workstorePropRowMap.put("name",workstoreProp.getProp().getName());
			workstorePropRowMap.put("value",workstoreProp.getValue());
			workstorePropRowMap.put("description",workstoreProp.getProp().getDescription());
			workstorePropRowMap.put("hospCode",workstoreProp.getWorkstore().getHospCode());
			workstorePropRowMap.put("workstoreCode",workstoreProp.getWorkstore().getWorkstoreCode());
			workstorePropList.add(workstorePropTable.asRow(workstorePropRowMap));
		}
		
		workstorePropTable.addRows(workstorePropList);
		db.flush();
	
    }
    
    @SuppressWarnings("unchecked")
	private void updateAccdbHospitalPropTable() throws IOException{

		Table hospitalPropTable = db.getTable("HospitalProp");
		List<Object[]> hospitalPropList = new ArrayList<Object[]>();
		Map<String, Object> hospitalPropRowMap = new HashMap<String, Object>();
		
		List<HospitalProp> phsHospitalPropList = em.createQuery(
				"select o from HospitalProp o") // 20120307 index check : none
				.getResultList();
	
		for (HospitalProp hospitalProp : phsHospitalPropList) {
			hospitalPropRowMap.put("name",hospitalProp.getProp().getName());
			hospitalPropRowMap.put("value",hospitalProp.getValue());
			hospitalPropRowMap.put("description",hospitalProp.getProp().getDescription());
			hospitalPropRowMap.put("hospCode",hospitalProp.getHospital().getHospCode());
			hospitalPropList.add(hospitalPropTable.asRow(hospitalPropRowMap));
		}
		
		hospitalPropTable.addRows(hospitalPropList);
		db.flush();
	
    }
    
    @SuppressWarnings("unchecked")
	private void updateAccdbManualInvoiceNumRangeTable() throws IOException{

		Table manualInvoiceNumRangeTable = db.getTable("ManualInvoiceNumRange");
		List<Object[]> manualInvoiceNumRangeList = new ArrayList<Object[]>();
		Map<String, Object> manualInvoiceNumRangeRowMap = new HashMap<String, Object>();
		
		List<ManualInvoiceNumRange> phsManualInvoiceNumRangeList = em.createQuery(
				"select o from ManualInvoiceNumRange o") // 20120307 index check : none
				.getResultList();
	
		for (ManualInvoiceNumRange manualInvoiceNumRange : phsManualInvoiceNumRangeList) {
			manualInvoiceNumRangeRowMap.put("hostName",manualInvoiceNumRange.getHostName());
			manualInvoiceNumRangeRowMap.put("hospCode",manualInvoiceNumRange.getHospCode());
			manualInvoiceNumRangeRowMap.put("minNum",manualInvoiceNumRange.getMinNum());
			manualInvoiceNumRangeRowMap.put("maxNum",manualInvoiceNumRange.getMaxNum());
			manualInvoiceNumRangeRowMap.put("currNum",manualInvoiceNumRange.getMinNum());
			manualInvoiceNumRangeList.add(manualInvoiceNumRangeTable.asRow(manualInvoiceNumRangeRowMap));
		}
		
		manualInvoiceNumRangeTable.addRows(manualInvoiceNumRangeList);
		db.flush();
	
    }

	private File getDirectory() throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + "/" + 
				jobId);
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}
    
	private File getDirectory(String jobId,
								DataDirType dataDirType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + "/" + 
				jobId + "/" + 
				dataDirType.getDataValue());
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}
	
	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
			logger.debug("DtDbBean - createDirectory: create path status: " + created);
		}
		
		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("DtDbBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("DtDbBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("DtDbBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}	
	
	private File getFile(
			File dir, 
			String fileName) {;
		
		File file = new File(
				    dir + "/" + 
				    fileName );
		
		return file;
	}

}
