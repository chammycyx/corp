package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.List;

public interface DmMedicalOfficerCacherInf extends BaseCacherInf {

	DmMedicalOfficer getDmMedicalOfficerByMedicalOfficerCode(Workstore workstore, String medicalOfficerCode);
	
	List<DmMedicalOfficer> getDmMedicalOfficerList(Workstore workstore);
	
	List<DmMedicalOfficer> getDmMedicalOfficerListByMedicalOfficerCode(Workstore workstore, String prefixMedicalOfficerCode);

}
