package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmFormDosageUnitMappingCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmFormDosageUnitMappingCacher extends BaseCacher implements DmFormDosageUnitMappingCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	
	
	private InnerCacher cacher;

	private DmFormDosageUnitMappingComparatorByDosageUnit comparator = new DmFormDosageUnitMappingComparatorByDosageUnit();
	
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	

    public DmFormDosageUnitMapping getDmFormDosageUnitMappingByFormCodeDosageUnit(String formCode, String dosageUnit) {
    	String mappingKey = formCode + dosageUnit;
		return (DmFormDosageUnitMapping) this.getCacher().get(mappingKey);
	}

	public List<DmFormDosageUnitMapping> getDmFormDosageUnitMappingList() {
		return (List<DmFormDosageUnitMapping>) this.getCacher().getAll();
	}

	public List<DmFormDosageUnitMapping> getDmFormDosageUnitMappingListByFormCode(String formCode) {		
		List<DmFormDosageUnitMapping> ret = new ArrayList<DmFormDosageUnitMapping>();
		for (DmFormDosageUnitMapping dmFormDosageUnitMapping : getDmFormDosageUnitMappingList()){
			if(dmFormDosageUnitMapping.getCompId().getFormCode().equals(formCode)){
				ret.add(dmFormDosageUnitMapping);				
			}
		}
		Collections.sort(ret,comparator);		
		return ret;
	}

	public List<DmFormDosageUnitMapping> getDmFormDosageUnitMappingListByDosageUnit(String prefixDosageUnit) {
		List<DmFormDosageUnitMapping> ret = new ArrayList<DmFormDosageUnitMapping>();
		for (DmFormDosageUnitMapping dmFormDosageUnitMapping : getDmFormDosageUnitMappingList()){
			if(dmFormDosageUnitMapping.getCompId().getDosageUnit().startsWith(prefixDosageUnit)){
				ret.add(dmFormDosageUnitMapping);
			}
		}
		return ret;
	}


	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmFormDosageUnitMapping> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmFormDosageUnitMapping create(String mappingKey) {
			this.getAll();
			return this.internalGet(mappingKey);
		}

		@Override
		public Collection<DmFormDosageUnitMapping> createAll() {
			return dmsPmsServiceProxy.retrieveDmFormDosageUnitMappingList();
		}

		@Override
		public String retrieveKey(DmFormDosageUnitMapping dmFormDosageUnitMapping) {
			return dmFormDosageUnitMapping.getCompId().getFormCode() + dmFormDosageUnitMapping.getCompId().getDosageUnit();
		}
	}

	public static DmFormDosageUnitMappingCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmFormDosageUnitMappingCacherInf) Component.getInstance("dmFormDosageUnitMappingCacher",
				ScopeType.APPLICATION);
	}
	
	public static class DmFormDosageUnitMappingComparatorByDosageUnit implements Comparator<DmFormDosageUnitMapping>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( DmFormDosageUnitMapping f1, DmFormDosageUnitMapping f2 ) {
			return f1.getCompId().getDosageUnit().compareToIgnoreCase( f2.getCompId().getDosageUnit() );
		}
	}
}
