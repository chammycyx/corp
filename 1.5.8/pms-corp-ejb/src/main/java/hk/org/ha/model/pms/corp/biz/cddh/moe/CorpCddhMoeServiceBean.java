package hk.org.ha.model.pms.corp.biz.cddh.moe;

import static hk.org.ha.model.pms.corp.prop.Prop.CDDH_LEGACY_ENQUIRY_ENABLED;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhServiceLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.util.TimeStamp;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItem;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItemCriteria;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.Gender;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.cddh.ItemDescFilterMode;
import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.label.Instruction;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Session Bean implementation class CorpCddhMoeServiceBean
 */
@AutoCreate
@Stateless
@Name("corpCddhMoeService")
@MeasureCalls
public class CorpCddhMoeServiceBean implements CorpCddhMoeServiceLocal {
	
	@In
	private CorpCddhServiceLocal corpCddhService;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@Logger
	private Log logger;
	
	@In
	private AuditLogger auditLogger;
	
	private static final String QUANTITY_DECIMAL_PATTERN = "#######";
	private static final String COST_DECIMAL_PATTERN = "######.###";
	private static final String COMMA = ", ";
	private static final char SPACE = ' ';
	private static final String YES_FLAG = "Y";
	private static final String NO_FLAG = "N";
	
    public CorpCddhMoeServiceBean() {
    }
    
    private String decimalFormat(String pattern, BigDecimal value) {
    	DecimalFormat decimalFormatter = new DecimalFormat(pattern);
    	return decimalFormatter.format(value);
    }
    
    private String getLine3Text(DispOrderItem dispOrderItem) {
    	Workstore workstore = dispOrderItem.getDispOrder().getWorkstore();
    	PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
    	Regimen regimen = pharmOrderItem.getRegimen();
    	
    	// start date text
    	Date startDate = dispOrderItem.getStartDate();
    	Date dispDate = DateUtils.truncate(dispOrderItem.getStartDate(), Calendar.DAY_OF_MONTH);
    	DateTimeFormatter simpleDateFormat = DateTimeFormat.forPattern("dd/MM/yyyy");
    	
    	String startDateText = StringUtils.EMPTY;
    	if (startDate != null) {
    		if (startDate.compareTo(dispDate) != 0) {
    			startDateText = "Start on " + simpleDateFormat.print(new DateTime(startDate)) + COMMA;
    		}
    	}
    	
    	// update date text
    	String updateDateText = StringUtils.EMPTY;
    	Date updateDate = dispOrderItem.getUpdateDate();
    	
    	if (updateDate != null) {
    		updateDateText = "On " + simpleDateFormat.print(new DateTime(updateDate)) + COMMA;
    	}
    	
    	// hospCode text
    	String hospCodeText = "by : " + workstore.getHospCode() + COMMA;
    	
    	// duration text
    	String durationText = StringUtils.EMPTY;
    	if (dispOrderItem.isLegacy()) {
    		durationText = "Duration : " + regimen.getDuration() + SPACE + regimen.getDurationUnit() + COMMA;
    	} else {
    		durationText = "Duration : " + dispOrderItem.getDurationInDay() + SPACE + RegimenDurationUnit.Day.getDisplayValue() + COMMA;
    	}
    	
    	// quantity text
    	String quanText = StringUtils.EMPTY;
    	quanText = "Quantity : " + decimalFormat(QUANTITY_DECIMAL_PATTERN, dispOrderItem.getDispQty()) + SPACE + pharmOrderItem.getBaseUnit() + COMMA;
    	
    	// cost text
    	BigDecimal expenditure = dispOrderItem.getDispQty().multiply(dispOrderItem.getUnitPrice());
    	String costText = StringUtils.EMPTY;
    	costText = "Cost : $" + decimalFormat(COST_DECIMAL_PATTERN, expenditure);
    	
    	StringBuilder text = new StringBuilder();
    	text.append(startDateText)
    		.append(updateDateText)
    	    .append(hospCodeText)
    	    .append(durationText)
    	    .append(quanText)
    	    .append(costText);
    	
    	return text.toString();
    }
 
    private void fillSplitFlag(List<DispOrderItem> dispOrderItemList) {
    	// fill in split flag
    	for (DispOrderItem dispOrderItem: dispOrderItemList) {
    		if (!dispOrderItem.isLegacy()) {
    			DispOrder dispOrder = dispOrderItem.getDispOrder();
    			
    			List<InvoiceItem> invoiceItemList = dispOrderItem.getInvoiceItemList();
        		InvoiceItem invoiceItem = null;
        		Invoice invoice = null;
    			if (invoiceItemList != null && !invoiceItemList.isEmpty()) {
    				invoiceItem = invoiceItemList.get(0);
    				invoice = invoiceItem.getInvoice();
    			}
    			
    			// split flag
    			if (invoice != null && invoice.getDocType() == InvoiceDocType.Sfi) { // sfi
    				dispOrderItem.setSplitFlag(NO_FLAG);
    			} else {// standard or keep record
    				if (dispOrder.getRefillFlag()) {
    					dispOrderItem.setSplitFlag(YES_FLAG);
    				} else {
    					dispOrderItem.setSplitFlag(NO_FLAG);
    				}
    			}
    		}
    	}
    }
    
    public List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria) {
    	logger.info("CDDH retrieve of CMS by MOE order num: orderNum=#0, dispHospCode=#1", criteria.getMoeOrderNum(), criteria.getDispHospCode());

    	auditLogger.log("#4025 {methodName:\"retrieveCddhDispenseInfoList\", hospCode:\""+criteria.getHospCode()
    			+"\", workstationCode:\""+criteria.getWorkstationCode()
    			+"\", userCode:\""+criteria.getUserCode()
    			+"\", patKey:\""+criteria.getPatKey()
    			+"\", caseNum:\""+criteria.getCaseNum()
    			+"\", enquiryType:\""+criteria.getEnquiryType()
    			+"\", userRank:\""+criteria.getUserRank()
    			+"\", enquirySystem:\""+criteria.getEnquirySystem()+"\"}");
    	
    	List<CddhDispenseInfo> dispInfoList = new ArrayList<CddhDispenseInfo>();
    	
    	TimeStamp timeStamp = new TimeStamp("CorpCddhMoeServiceBean.retrieveCddhDispenseInfoList");
    	
    	List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItemForCms(criteria);
    	
    	timeStamp.stampTime("after retrieve all");
    	
    	fillSplitFlag(dispOrderItemList);
    	
    	timeStamp.stampTime("after filter");
    	
		int i = 0;
		
    	for (DispOrderItem dispOrderItem: dispOrderItemList) {
    		DispOrder dispOrder = dispOrderItem.getDispOrder();
    		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
    		MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
    		BaseLabel baseLabel = dispOrderItem.getBaseLabel();
    		baseLabel.setPrintOption(new PrintOption(PrintType.Normal, PrintLang.Eng));
    		
    		CddhDispenseInfo dispInfo = new CddhDispenseInfo();
    		dispInfo.setDispOrderNum(dispOrder.getDispOrderNum());
    		dispInfo.setDispItemNum(pharmOrderItem.getItemNum());
    		dispInfo.setMoeItemNum(medOrderItem.getItemNum());
    		dispInfo.setOrgMoeItemNum(medOrderItem.getOrgItemNum());
    		dispInfo.setUpdateDate(dispOrderItem.getUpdateDate());
    		dispInfo.setLine1(baseLabel.getItemDesc());
    		dispInfo.setLine2(baseLabel.getInstructionText());
    		dispInfo.setLine3(getLine3Text(dispOrderItem));
    		dispInfo.setActionStatusRank(Integer.valueOf(pharmOrderItem.getActionStatus().getRank()));
    		
    		if (logger.isDebugEnabled()) {
	    		logger.debug("CorpCddhMoeServiceBean.retrieveCddhDispenseInfoList: " +
	    				"dispOrderNum=#0, " +
	    				"dispItemNum=#1, " +
	    				"moeItemNum=#2, " +
	    				"orgMoeItemNum=#3, " +
	    				"updateDate=#4",
	    				dispInfo.getDispOrderNum(),
	    				dispInfo.getDispItemNum(),
	    				dispInfo.getMoeItemNum(),
	    				dispInfo.getOrgMoeItemNum(),
	    				dispInfo.getUpdateDate());
	    		
	    		logger.debug("CorpCddhMoeServiceBean.retrieveCddhDispenseInfoList: " +
	    				"line1=#0, " +
	    				"line2=#1, " +
	    				"line3=#2, " +
	    				"actionStatusRank=#3",
	    				dispInfo.getLine1(),
	    				dispInfo.getLine2(),
	    				dispInfo.getLine3(),
	    				dispInfo.getActionStatusRank());
    		}
    		
    		dispInfoList.add( dispInfo );
    		
			if (++i % 100 == 0) {
				timeStamp.stampTime("after " + i + " item processed");			
			}
    	}
    	
		timeStamp.stampTime("total " + i + " item completed");
		
    	timeStamp.writeLog(logger);
		
    	return dispInfoList;
    }
    
	public List<CddhDispenseItem> retrieveCddhDispenseItemList(CddhDispenseItemCriteria criteria) {
    	logger.info("CDDH retrieve of CMS by patient key: patKey=#0, dispDateStart=#1, dispDateEnd=#2", criteria.getPatKey(), criteria.getDispDateStart(), criteria.getDispDateEnd());
		
    	auditLogger.log("#4026 {methodName:\"retrieveCddhDispenseItemList\", hospCode:\""+criteria.getHospCode()
    			+"\", workstationCode:\""+criteria.getWorkstationCode()
    			+"\", userCode:\""+criteria.getUserCode()
    			+"\", patKey:\""+criteria.getPatKey()
    			+"\", caseNum:\""+criteria.getCaseNum()
    			+"\", enquiryType:\""+criteria.getEnquiryType()
    			+"\", userRank:\""+criteria.getUserRank()
    			+"\", enquirySystem:\""+criteria.getEnquirySystem()+"\"}");

		List<CddhDispenseItem> cddhDispenseItemList = new ArrayList<CddhDispenseItem>();
		
    	TimeStamp timeStamp = new TimeStamp("CorpCddhMoeServiceBean.retrieveCddhDispenseItemList");    	
		
		validateCddhDispenseItemCriteria(criteria.getPatKey(), criteria.getDispDateStart(), criteria.getDispDateEnd());
		
		CddhCriteria cddhCriteria = new CddhCriteria();
		
		cddhCriteria.setPatKey(criteria.getPatKey());
		try {
			DateFormat dispDateFormat = new SimpleDateFormat("yyyyMMdd");
			cddhCriteria.setDispDateStart( dispDateFormat.parse(criteria.getDispDateStart()) );
			cddhCriteria.setDispDateEnd( dispDateFormat.parse(criteria.getDispDateEnd()) );
		} catch (ParseException e) {
		}
		
		cddhCriteria.setLegacyCddhIncluded(CDDH_LEGACY_ENQUIRY_ENABLED.get(false));
		// do not set hkpmiEnabled as in pms-pms, because it is no more used
		cddhCriteria.setLoginHospCode(null);	// no need to check access right for CMS call
		cddhCriteria.setDangerDrugFlag(YesNoBlankFlag.Blank);
		cddhCriteria.setGender(Gender.Male);
		cddhCriteria.setItemDescFilterMode(ItemDescFilterMode.BeginWith);
		cddhCriteria.setRetrieveFromPmsFlag(false);
		
		List<DispOrderItem> dispOrderItemList = corpCddhService.retrieveDispOrderItem(cddhCriteria);
		
    	timeStamp.stampTime("after retrieve all");
    	
    	// sorting DispOrderItemComparator
    	Collections.sort(dispOrderItemList, new DispOrderItemComparator());

    	timeStamp.stampTime("after sort");
    	
    	logger.info("retrieveCddhDispenseItemList: patKey=#0, dispDateStart=#1, dispDateEnd=#2, resultCount=#3", criteria.getPatKey(), criteria.getDispDateStart(), criteria.getDispDateEnd(), dispOrderItemList.size());
    	

		int i = 0;
		
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			CddhDispenseItem cddhDispenseItem = new CddhDispenseItem();
			
			MedOrder medOrder = dispOrderItem.getDispOrder().getPharmOrder().getMedOrder();
			PharmOrder pharmOrder = dispOrderItem.getDispOrder().getPharmOrder();
			DispOrder dispOrder = dispOrderItem.getDispOrder();
			PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();

			if (dispOrder.getTicket() == null) {
				// ticket may be null because of batch join, so just skip it
				logger.info("retrieveCddhDispenseItemList: dispOrderItem skipped due to batch join, id = " + dispOrderItem.getId());
				continue;
			}
			
			if (pharmOrderItem.getDmDrug() == null && pharmOrderItem.getItemCode() != null) {
	    		pharmOrderItem.setDmDrug(dmDrugCacher.getDmDrug(pharmOrderItem.getItemCode()));
	    	}
			
			cddhDispenseItem.setDispDate(dispOrder.getDispDate());
			cddhDispenseItem.setIssueDate(dispOrder.getIssueDate());
			cddhDispenseItem.setDispOrderNum(dispOrder.getDispOrderNum());
			cddhDispenseItem.setItemNum(pharmOrderItem.getItemNum());
			cddhDispenseItem.setItemCode(pharmOrderItem.getItemCode());
			cddhDispenseItem.setActionStatus(pharmOrderItem.getActionStatus().getDataValue());
			cddhDispenseItem.setFmStatus(pharmOrderItem.getFmStatus());
			cddhDispenseItem.setItemCatCode(dispOrderItem.getItemCatCode());
			cddhDispenseItem.setDrugName(dispOrderItem.getCddhDrugDesc());
			cddhDispenseItem.setTradeName(pharmOrderItem.getTradeName());
			
			if (dispOrderItem.isLegacy()) {
				if (pharmOrderItem.getRegimen().getDuration() != null && pharmOrderItem.getRegimen().getDuration() > 0 &&
						pharmOrderItem.getRegimen().getDurationUnit() != null) {
					StringBuilder durationTextSb = new StringBuilder();
					durationTextSb.append(pharmOrderItem.getRegimen().getDuration());
					durationTextSb.append(SPACE);
					durationTextSb.append(pharmOrderItem.getRegimen().getDurationUnit().getDisplayValue());
					cddhDispenseItem.setDurationText(durationTextSb.toString());
				}
			} else {
				if (dispOrderItem.getDurationInDay() != null && dispOrderItem.getDurationInDay() > 0) {
					StringBuilder durationTextSb = new StringBuilder();
					durationTextSb.append(dispOrderItem.getDurationInDay());
					durationTextSb.append(SPACE);
					durationTextSb.append(RegimenDurationUnit.Day.getDisplayValue());
					cddhDispenseItem.setDurationText(durationTextSb.toString());
				}
			}
			
			StringBuilder dispQtySb = new StringBuilder();
			dispQtySb.append(dispOrderItem.getDispQty());
			dispQtySb.append(SPACE);
			if (pharmOrderItem.getBaseUnit() != null) {
				dispQtySb.append(pharmOrderItem.getBaseUnit());
			}
			cddhDispenseItem.setDispQtyText(dispQtySb.toString());
			
			if (dispOrder.getWorkstore().getCddhWorkstoreGroup() != null) {
				cddhDispenseItem.setHospCode(dispOrder.getWorkstore().getCddhWorkstoreGroup().getCddhWorkstoreGroupCode());
			} else {
				cddhDispenseItem.setHospCode(dispOrder.getWorkstore().getHospCode());
			}
			cddhDispenseItem.setWorkstoreCode(dispOrder.getWorkstore().getWorkstoreCode());
			cddhDispenseItem.setSpecCode(dispOrderItem.getChargeSpecCode());
			cddhDispenseItem.setWardCode(pharmOrder.getWardCode());
			if (dispOrder.getCddhSpecialtyMapping() != null) {
				cddhDispenseItem.setCddhSpecCode(dispOrder.getCddhSpecialtyMapping().getCddhSpecCode());
			} else {
				cddhDispenseItem.setCddhSpecCode(null);
			}
			cddhDispenseItem.setTicketNum(dispOrder.getTicket().getTicketNum());
			cddhDispenseItem.setEpisodeType(dispOrderItem.getEpisodeType());
			if (medOrder.getDocType() == MedOrderDocType.Normal) {
				cddhDispenseItem.setRefNum(medOrder.getRefNum());
			}
			cddhDispenseItem.setDoctorCode(pharmOrder.getDoctorCode());
			cddhDispenseItem.setDoctorName(pharmOrder.getDoctorName());
			cddhDispenseItem.setPatType(medOrder.getOrderType().getDataValue());
			cddhDispenseItem.setCaseNum(pharmOrder.getMedCase().getCaseNum());
			cddhDispenseItem.setMultipleDoseFlag( convertMultiDoseFlag(pharmOrderItem, dispOrderItem) );
			if (dispOrderItem.getUnitPrice() != null) {
				cddhDispenseItem.setUnitPrice(dispOrderItem.getUnitPrice());
			} else {
				cddhDispenseItem.setUnitPrice(BigDecimal.valueOf(0));
			}
			cddhDispenseItem.setWarnCode1(dispOrderItem.getWarnCode1());
			cddhDispenseItem.setWarnCode2(dispOrderItem.getWarnCode2());
			cddhDispenseItem.setWarnCode3(dispOrderItem.getWarnCode3());
			cddhDispenseItem.setWarnCode4(dispOrderItem.getWarnCode4());
			cddhDispenseItem.setWarnDesc1(dispOrderItem.getWarnDesc1());
			cddhDispenseItem.setWarnDesc2(dispOrderItem.getWarnDesc2());
			cddhDispenseItem.setWarnDesc3(dispOrderItem.getWarnDesc3());
			cddhDispenseItem.setWarnDesc4(dispOrderItem.getWarnDesc4());
			cddhDispenseItem.setDangerDrugFlag(pharmOrderItem.getDangerDrugFlag());
			if (pharmOrderItem.getDmDrug() != null) {
				StringBuilder commodityTypeSb = new StringBuilder();
				commodityTypeSb.append(pharmOrderItem.getDmDrug().getCommodityGroup());
				commodityTypeSb.append(pharmOrderItem.getDmDrug().getCommodityType());
				cddhDispenseItem.setCommodityTypeText(commodityTypeSb.toString());
			}
			
			if (dispOrderItem.getRemarkType() != DispOrderItemRemarkType.Empty) {
				StringBuilder remarkTextSb = new StringBuilder();
				if (dispOrderItem.getRemarkType() != DispOrderItemRemarkType.Others) {
					remarkTextSb.append(dispOrderItem.getRemarkType().getDisplayValue());
				}
				if (dispOrderItem.getRemarkText() != null) {
					if (remarkTextSb.length() > 0) {
						remarkTextSb.append(SPACE);
					}
					remarkTextSb.append(dispOrderItem.getRemarkText());
				}
				cddhDispenseItem.setRemarkText(remarkTextSb.toString());
			}
			
			cddhDispenseItem.setMpRemarkText(pharmOrderItem.getRemarkText());
			cddhDispenseItem.setOverrideSeqNum(dispOrderItem.getOverrideSeqNum());
			if (dispOrderItem.isLegacy()) {
				cddhDispenseItem.setOverrideDesc(dispOrderItem.getOverrideDesc());
				cddhDispenseItem.setOverrideRemark(dispOrderItem.getOverrideRemark());
			}
			cddhDispenseItem.setUserCode(dispOrderItem.getCreateUser());
			cddhDispenseItem.setRemarkUpdateUser(dispOrderItem.getRemarkUpdateUser());
			cddhDispenseItem.setRemarkUpdateDate(dispOrderItem.getRemarkUpdateDate());
			
			cddhDispenseItem.setSingleDoseInstruction(dispOrderItem.getSingleDosageValue());
			
			if (convertMultiDoseFlag(pharmOrderItem, dispOrderItem)) {
				if (dispOrderItem.isLegacy()) {
					InstructionBuilder instructionBuilder = InstructionBuilder.instance();
		    		List<Instruction> instructionList = instructionBuilder.buildInstructionListForDispLabel(dispOrderItem);
	
		    		BaseLabel tempBaseLabel = new DispLabel();
		    		tempBaseLabel.setInstructionList(instructionList);
		    		tempBaseLabel.setPrintOption(new PrintOption(PrintType.Normal, PrintLang.Eng));
		    		tempBaseLabel.setItemCode(pharmOrderItem.getItemCode());
		    		tempBaseLabel.setItemDesc(DmDrug.buildFullDrugDesc(
			    			pharmOrderItem.getDrugName(), 
			    			pharmOrderItem.getFormCode(), 
			    			pharmOrderItem.getFormLabelDesc(), 
			    			pharmOrderItem.getStrength(), 
			    			pharmOrderItem.getVolumeText()));
		    		
		    		dispOrderItem.setBaseLabel(tempBaseLabel);
				}
	    		
	    		BaseLabel baseLabel = dispOrderItem.getBaseLabel();
	    		baseLabel.setPrintOption(new PrintOption(PrintType.Normal, PrintLang.Eng));
				cddhDispenseItem.setMultipleDoseInstruction(baseLabel.getInstructionText());
			}
			
			cddhDispenseItemList.add(cddhDispenseItem);
			
			if (++i % 100 == 0) {
				timeStamp.stampTime("after " + i + " item processed");			
			}
		}
		
		timeStamp.stampTime("total " + i + " item completed");
		
    	timeStamp.writeLog(logger);
		
		return cddhDispenseItemList;
	} 
	
	private void validateCddhDispenseItemCriteria(String patKey, String dispDateStart, String dispDateEnd) {
		if (StringUtils.isBlank(patKey)) {
			throw new IllegalArgumentException("Patient Key is empty");
		} 
		
		if (StringUtils.isBlank(dispDateStart)) {
			throw new IllegalArgumentException("DispDateStart is empty");
		}

		if (StringUtils.isBlank(dispDateEnd)) {
			throw new IllegalArgumentException("DispDateEnd is empty");
		}

		DateFormat dispDateFormat = new SimpleDateFormat("yyyyMMdd");
		
		try {
			dispDateFormat.parse(dispDateStart);
		} catch (ParseException e) {
			throw new IllegalArgumentException("Invalid DispDateStart");
		}

		try {
			dispDateFormat.parse(dispDateEnd);
		} catch (ParseException e) {
			throw new IllegalArgumentException("Invalid DispDateEnd");
		}
	}

	private Boolean convertMultiDoseFlag(PharmOrderItem pharmOrderItem, DispOrderItem dispOrderItem) {
		if (dispOrderItem.isLegacy()) {
			return dispOrderItem.isLegacyMultiDose();
			
		} else {
			Regimen regimen = pharmOrderItem.getRegimen();
			
			if (regimen != null){
				if (regimen.getDoseGroupList().size() > 1) { // step up down
					return Boolean.TRUE;
				} else if (regimen.getDoseGroupList().get(0).getDoseList().size() > 1) {
					return Boolean.TRUE;
				}
			}
			
			return Boolean.FALSE;
		}
	}

    // Comparator for Cddh DispOrderItem sort by date
    private static class DispOrderItemComparator implements Comparator<DispOrderItem>, Serializable {

    	private static final long serialVersionUID = 1L;

    	@Override
		public int compare(DispOrderItem dispOrderItem1, DispOrderItem dispOrderItem2) {
			int compareResult = 0;
			Date issueDate1 = DateUtils.truncate(dispOrderItem1.getDispOrder().getIssueDate(), Calendar.DAY_OF_MONTH);
			Date issueDate2 = DateUtils.truncate(dispOrderItem2.getDispOrder().getIssueDate(), Calendar.DAY_OF_MONTH);
			compareResult = issueDate2.compareTo(issueDate1);

			if (compareResult == 0) {
				Ticket ticket1 = dispOrderItem1.getDispOrder().getTicket();
				Ticket ticket2 = dispOrderItem2.getDispOrder().getTicket();
				if (ticket1 != null && ticket2 != null) {
					compareResult = StringUtils.trimToEmpty(ticket2.getTicketNum()).compareTo(StringUtils.trimToEmpty(ticket1.getTicketNum()));
				}
				
				if (compareResult == 0) {
					compareResult = StringUtils.trimToEmpty(dispOrderItem1.getCddhDrugDesc()).compareTo(StringUtils.trimToEmpty(dispOrderItem2.getCddhDrugDesc()));
						
					if (compareResult == 0) {
						compareResult = dispOrderItem1.getDispQty().compareTo(dispOrderItem2.getDispQty());
					}
				}
			}
			return compareResult;
		}
    }	
}
