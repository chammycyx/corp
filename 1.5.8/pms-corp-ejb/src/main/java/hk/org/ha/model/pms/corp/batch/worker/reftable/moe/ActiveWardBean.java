package hk.org.ha.model.pms.corp.batch.worker.reftable.moe;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.persistence.corp.ActiveWard;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.ipmoe.IpmoeServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Scope(ScopeType.SESSION)
@Name("activeWardBean")
public class ActiveWardBean implements ActiveWardLocal{

	private Logger logger = null;

	@In
	private ActiveWardHelper activeWardHelper;
	
	@In
	private IpmoeServiceJmsRemote ipmoeServiceProxy;
	
	@In
    private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	@In
    private ActiveMpHospitalManagerLocal activeMpHospitalManager;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
    public void initBatch(BatchWorkerInfo info, Logger logger) {
		this.logger = logger;
	}
	
	public void beginChunk(Chunk arg0) throws Exception {
		
	}
	
	public void processRecord(Record arg0) throws Exception {
    	Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);

        List<String> patHospCodeList = activeMpHospitalManager.retrieveActiveMpPatHospCodeList();
        
		List<String> errorPatHospCodeList = new ArrayList<String>();
		
        //retrieve all work store group data from cms
        List<ActiveWard> activeWardList = new ArrayList<ActiveWard>();
        for(String patHospCode : patHospCodeList){
        	List<ActiveWard> list = ipmoeServiceProxy.retrieveActiveWard( patHospCode );
        	// empty list is valid case, only null is exception
    		if( list != null ){
    			activeWardList.addAll(list);	
    		}else{
    			errorPatHospCodeList.add( patHospCode );
    		}
        }
        
        List<ActiveWard> activeWardListForCorp = new ArrayList<ActiveWard>(activeWardList);
        List<ActiveWard> activeWardListForPms = new ArrayList<ActiveWard>(activeWardList);
        
        activeWardHelper.updateActiveWardForPms(em, activeWardListForCorp, errorPatHospCodeList);
        pmsSubscriberProxy.updateActiveWardForPms(activeWardListForPms, errorPatHospCodeList, new Date());

		Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	@Remove
	public void destroyBatch() {
		
	}

	public void endChunk() throws Exception {
		
	}

}
