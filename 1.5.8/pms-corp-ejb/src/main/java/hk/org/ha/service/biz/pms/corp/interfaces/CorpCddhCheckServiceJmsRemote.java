package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;

import java.util.List;
import java.util.Map;

public interface CorpCddhCheckServiceJmsRemote {

	Map<Integer, List<DispOrderItem>> retrieveDuplicateDispensedItem (
			String hkid,
			String caseNum,
			String patHospCode,
			String orderNum,
			Map<Integer, List<PharmOrderItem>> pharmOrderItemMap,
			Boolean legacyPersistenceEnabled);

}
