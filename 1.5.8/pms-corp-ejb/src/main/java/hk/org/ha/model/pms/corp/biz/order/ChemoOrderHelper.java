package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.rx.AdminRate;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseFluid;
import hk.org.ha.model.pms.vo.rx.Freq;
import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvFluid;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.OralRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("chemoOrderHelper")
@Scope(ScopeType.APPLICATION)
public class ChemoOrderHelper {
		
	public boolean compareChemoItemSameGroup(MedProfileMoItem medProfileMoItem1, MedProfileMoItem medProfileMoItem2) {
		int result = new CompareToBuilder()
							.append( medProfileMoItem1.getCmmFormulaCode(), medProfileMoItem2.getCmmFormulaCode() )
							// note: skip grouping by protocol code and protocol version, because they should be the same for IP items in same CP1 
							.append( medProfileMoItem1.getChemoCategory(), medProfileMoItem2.getChemoCategory() )
							.append( medProfileMoItem1.getCycle(), medProfileMoItem2.getCycle() )
							// since chemo has no CAPD, compare rxItemType will distinguish oral, injection, iv items
							.append( medProfileMoItem1.getRxItemType(), medProfileMoItem2.getRxItemType() )	
							.toComparison();
		
		if (result != 0) {
			return false;
		}
		
		if (medProfileMoItem1.getRxItemType() == RxItemType.Oral) {
			OralRxDrug oralRxDrug1 = (OralRxDrug)medProfileMoItem1.getRxItem();
			OralRxDrug oralRxDrug2 = (OralRxDrug)medProfileMoItem2.getRxItem();
			Regimen regimen1 = oralRxDrug1.getRegimen(); 
			Regimen regimen2 = oralRxDrug2.getRegimen();

			// compare IpMedProfileOral
			result = new CompareToBuilder()
							.append( regimen1.getType(), regimen2.getType() )
							// note: skip compare multDose as it does not map to moi
							.append( oralRxDrug1.getVolume(), oralRxDrug2.getVolume() )
							.append( oralRxDrug1.getVolumeUnit(), oralRxDrug2.getVolumeUnit() )
							// note: skip compare volText as it does not map to moi
							.append( oralRxDrug1.getItemCode(), oralRxDrug2.getItemCode() )
							.append( oralRxDrug1.getTradeName(), oralRxDrug2.getTradeName() )
							.append( oralRxDrug1.getStrength(), oralRxDrug2.getStrength() )
							.append( oralRxDrug1.getFormCode(), oralRxDrug2.getFormCode() )
							.append( oralRxDrug1.getRouteCode(), oralRxDrug2.getRouteCode() )
							.append( oralRxDrug1.getRouteDesc(), oralRxDrug2.getRouteDesc() )
							.append( oralRxDrug1.getFormDesc(), oralRxDrug2.getFormDesc() )
							.append( oralRxDrug1.getSaltProperty(), oralRxDrug2.getSaltProperty() )
							.append( oralRxDrug1.getExtraInfo(), oralRxDrug2.getExtraInfo() )
							.append( oralRxDrug1.getSpecialInstruction(), oralRxDrug2.getSpecialInstruction() )
							.append( oralRxDrug1.getActionStatus(), oralRxDrug2.getActionStatus() )
							.append( oralRxDrug1.getDisplayName(), oralRxDrug2.getDisplayName() )
							.append( oralRxDrug1.getAliasName(), oralRxDrug2.getAliasName() )
							// TODO: add strengthValue and strengthUnit after confirmation
							.toComparison();

			if (result != 0) {
				return false;
			}
			
			if (regimen1.getDoseGroupList().size() != regimen2.getDoseGroupList().size()) {
				return false;
			}

			for (int i=0; i<regimen1.getDoseGroupList().size(); i++) {
				if (regimen1.getDoseGroupList().get(i).getDoseList().size() != regimen2.getDoseGroupList().get(i).getDoseList().size()) {
					return false;
				}
			}
			
			for (int i=0; i<regimen1.getDoseGroupList().size(); i++) {
				for (int j=0; j<regimen1.getDoseGroupList().get(i).getDoseList().size(); j++) {
					Dose dose1 = regimen1.getDoseGroupList().get(i).getDoseList().get(j);
					Dose dose2 = regimen2.getDoseGroupList().get(i).getDoseList().get(j);
					
					// compare IpMedMultDoseOral
					result = new CompareToBuilder()
									.append( dose1.getDosage(), dose2.getDosage() )
									.append( dose1.getDosageEnd(), dose2.getDosageEnd() )
									.append( dose1.getDosageUnit(), dose2.getDosageUnit() )
									// freq and suppl freq is compared below by function
									.append( dose1.getPrn(), dose2.getPrn() )
									.append( dose1.getPrnPercent(), dose2.getPrnPercent() )
									.append( dose1.getSiteCode(), dose2.getSiteCode() )
									.append( dose1.getSupplSiteDesc(), dose2.getSupplSiteDesc() )
									.append( dose1.getSiteDesc(), dose2.getSiteDesc() )
									.append( dose1.getIpmoeDesc(), dose2.getIpmoeDesc() )
									// note: skip compare item code because it is already compared 
									.append( dose1.getDispQty(), dose2.getDispQty() )
									.toComparison();
					
					if (result != 0) {
						return false;
					}
					
					if ( ! compareFreq(dose1.getDailyFreq(), dose2.getDailyFreq())) {
						return false;
					}
						    	  
					if ( ! compareFreq(dose1.getSupplFreq(), dose2.getSupplFreq())) {
						return false;
					}
				}
			}
			
		} else if (medProfileMoItem1.getRxItemType() == RxItemType.Injection) {
			InjectionRxDrug injRxDrug1 = (InjectionRxDrug)medProfileMoItem1.getRxItem();
			InjectionRxDrug injRxDrug2 = (InjectionRxDrug)medProfileMoItem2.getRxItem();
			Regimen regimen1 = injRxDrug1.getRegimen(); 
			Regimen regimen2 = injRxDrug2.getRegimen();

			// compare IpMedProfileInjection
			result = new CompareToBuilder()
							.append( regimen1.getType(), regimen2.getType() )
							// note: skip compare multDose as it does not map to moi
							.append( injRxDrug1.getVolume(), injRxDrug2.getVolume() )
							.append( injRxDrug1.getVolumeUnit(), injRxDrug2.getVolumeUnit() )
							// note: skip compare volText as it does not map to moi
							.append( injRxDrug1.getItemCode(), injRxDrug2.getItemCode() )
							.append( injRxDrug1.getTradeName(), injRxDrug2.getTradeName() )
							.append( injRxDrug1.getStrength(), injRxDrug2.getStrength() )
							.append( injRxDrug1.getFormCode(), injRxDrug2.getFormCode() )
							.append( injRxDrug1.getRouteCode(), injRxDrug2.getRouteCode() )
							.append( injRxDrug1.getRouteDesc(), injRxDrug2.getRouteDesc() )
							.append( injRxDrug1.getFormDesc(), injRxDrug2.getFormDesc() )
							.append( injRxDrug1.getSaltProperty(), injRxDrug2.getSaltProperty() )
							.append( injRxDrug1.getExtraInfo(), injRxDrug2.getExtraInfo() )
							.append( injRxDrug1.getSpecialInstruction(), injRxDrug2.getSpecialInstruction() )
							.append( injRxDrug1.getActionStatus(), injRxDrug2.getActionStatus() )
							.append( injRxDrug1.getDisplayName(), injRxDrug2.getDisplayName() )
							// note: ipFullDesc is compared by dose.ipmoeDesc
							.append( injRxDrug1.getAliasName(), injRxDrug2.getAliasName() )
							// note: siteCode is compared by dose.siteCode
							// note: supSiteDesc is compared by dose.supplSiteDesc
							// note: siteDesc is compared by dose.siteDesc
							// TODO: add strengthValue and strengthUnit after confirmation
							.append( injRxDrug1.getNoDiluentRequireFlag(), injRxDrug2.getNoDiluentRequireFlag() )
							.toComparison();

			if (result != 0) {
				return false;
			}

			if (regimen1.getDoseGroupList().size() != regimen2.getDoseGroupList().size()) {
				return false;
			}

			for (int i=0; i<regimen1.getDoseGroupList().size(); i++) {
				if (regimen1.getDoseGroupList().get(i).getDoseList().size() != regimen2.getDoseGroupList().get(i).getDoseList().size()) {
					return false;
				}
			}
			
			for (int i=0; i<regimen1.getDoseGroupList().size(); i++) {
				for (int j=0; j<regimen1.getDoseGroupList().get(i).getDoseList().size(); j++) {
					Dose dose1 = regimen1.getDoseGroupList().get(i).getDoseList().get(j);
					Dose dose2 = regimen2.getDoseGroupList().get(i).getDoseList().get(j);
					AdminRate startAdminRate1 = dose1.getStartAdminRate();
					AdminRate startAdminRate2 = dose2.getStartAdminRate();
					AdminRate endAdminRate1 = dose1.getEndAdminRate();
					AdminRate endAdminRate2 = dose2.getEndAdminRate();
					DoseFluid doseFluid1 = dose1.getDoseFluid();
					DoseFluid doseFluid2 = dose2.getDoseFluid();
					
					// compare IpMedMultDoseInjection
					result = new CompareToBuilder()
									.append( dose1.getDosage(), dose2.getDosage() )
									.append( dose1.getDosageEnd(), dose2.getDosageEnd() )
									.append( dose1.getDosageUnit(), dose2.getDosageUnit() )
									.append( startAdminRate1.getRateFormat(), startAdminRate2.getRateFormat() )
									.append( startAdminRate1.getRateDuration(), startAdminRate2.getRateDuration() )
									.append( startAdminRate1.getRateDurationUnit(), startAdminRate2.getRateDurationUnit() )
									.append( endAdminRate1.getRateDuration(), endAdminRate2.getRateDuration() )
									.append( endAdminRate1.getRateDurationUnit(), endAdminRate2.getRateDurationUnit() )
									.append( startAdminRate1.getRateVolume(), startAdminRate2.getRateVolume() )
									.append( startAdminRate1.getRateVolumeUnit(), startAdminRate2.getRateVolumeUnit() )
									.append( startAdminRate1.getRateVolumePerUnit(), startAdminRate2.getRateVolumePerUnit() )
									.append( endAdminRate1.getRateVolume(), endAdminRate2.getRateVolume() )
									.append( endAdminRate1.getRateVolumeUnit(), endAdminRate2.getRateVolumeUnit() )
									.append( endAdminRate1.getRateVolumePerUnit(), endAdminRate2.getRateVolumePerUnit() )
									// freq and suppl freq is compared below by function
									.append( dose1.getPrn(), dose2.getPrn() )
									.append( dose1.getPrnPercent(), dose2.getPrnPercent() )
									// note: following fields are mapped from ipMedProfileInjection
									.append( dose1.getIpmoeDesc(), dose2.getIpmoeDesc() )
									.append( dose1.getSiteCode(), dose2.getSiteCode() )
									.append( dose1.getSupplSiteDesc(), dose2.getSupplSiteDesc() )
									.append( dose1.getSiteDesc(), dose2.getSiteDesc() )
									.append( dose1.getDispQty(), dose2.getDispQty() )
									.toComparison();
					
					if (result != 0) {
						return false;
					}
					
					if ( ! compareFreq(dose1.getDailyFreq(), dose2.getDailyFreq())) {
						return false;
					}
						    	  
					if ( ! compareFreq(dose1.getSupplFreq(), dose2.getSupplFreq())) {
						return false;
					}
					
					// compare IpMedFluid
					if ( (doseFluid1 != null && doseFluid2 == null) ||
							(doseFluid1 == null && doseFluid2 != null) ) {
						return false;
					}
					
					if (doseFluid1 != null && doseFluid2 != null) {
						result = new CompareToBuilder()
										.append( doseFluid1.getDiluentName(), doseFluid2.getDiluentName() )
										.append( doseFluid1.getDiluentAbbr(), doseFluid2.getDiluentAbbr() )
										.append( doseFluid1.getFluidVolume(), doseFluid2.getFluidVolume() )
										.append( doseFluid1.getFluidVolumeUnit(), doseFluid2.getFluidVolumeUnit() )
										.append( doseFluid1.getFluidQty(), doseFluid2.getFluidQty() )
										.append( doseFluid1.getDilutionMethod(), doseFluid2.getDilutionMethod() )
										.append( doseFluid1.getActionStatus(), doseFluid2.getActionStatus() )
										.toComparison();
						
						if (result != 0) {
							return false;
						}
					}
				}
			}
			
		} else if (medProfileMoItem1.getRxItemType() == RxItemType.Iv) {
			IvRxDrug ivRxDrug1 = (IvRxDrug)medProfileMoItem1.getRxItem();
			IvRxDrug ivRxDrug2 = (IvRxDrug)medProfileMoItem2.getRxItem();
			AdminRate startAdminRate1 = ivRxDrug1.getStartAdminRate();
			AdminRate startAdminRate2 = ivRxDrug2.getStartAdminRate();
			AdminRate endAdminRate1 = ivRxDrug1.getEndAdminRate();
			AdminRate endAdminRate2 = ivRxDrug2.getEndAdminRate();

			// compare IpMedProfileIv
			result = new CompareToBuilder()
							.append( ivRxDrug1.getLineCode(), ivRxDrug2.getLineCode() )
							.append( ivRxDrug1.getLineDesc(), ivRxDrug2.getLineDesc() )
							.append( ivRxDrug1.getLineRemark(), ivRxDrug2.getLineRemark() )
							.append( startAdminRate1.getRateFormat(), startAdminRate2.getRateFormat() )
							.append( startAdminRate1.getRateDuration(), startAdminRate2.getRateDuration() )
							.append( startAdminRate1.getRateDurationUnit(), startAdminRate2.getRateDurationUnit() )
							.append( startAdminRate1.getRateVolume(), startAdminRate2.getRateVolume() )
							.append( startAdminRate1.getRateVolumeUnit(), startAdminRate2.getRateVolumeUnit() )
							.append( startAdminRate1.getRateVolumePerUnit(), startAdminRate2.getRateVolumePerUnit() )
							// note: skip compare endDateFormat as it does not map to moi
							.append( ivRxDrug1.getDuration(), ivRxDrug2.getDuration() )
							.append( ivRxDrug1.getDurationUnit(), ivRxDrug2.getDurationUnit() )
							.append( ivRxDrug1.getSpecialInstruction(), ivRxDrug2.getSpecialInstruction() )
							// note: skip compare drugAdditive as it does not map to moi
							// freq is compared below by function
							.append( ivRxDrug1.getIpmoeDesc(), ivRxDrug2.getIpmoeDesc() )
							.append( ivRxDrug1.getSiteCode(), ivRxDrug2.getSiteCode() )
							.append( ivRxDrug1.getSupplSiteDesc(), ivRxDrug2.getSupplSiteDesc() )
							.append( ivRxDrug1.getSiteDesc(), ivRxDrug2.getSiteDesc() )
							.append( ivRxDrug1.getDispQty(), ivRxDrug2.getDispQty() )
							.append( endAdminRate1.getRateDuration(), endAdminRate2.getRateDuration() )
							.append( endAdminRate1.getRateDurationUnit(), endAdminRate2.getRateDurationUnit() )
							.append( endAdminRate1.getRateVolume(), endAdminRate2.getRateVolume() )
							.append( endAdminRate1.getRateVolumeUnit(), endAdminRate2.getRateVolumeUnit() )
							.append( endAdminRate1.getRateVolumePerUnit(), endAdminRate2.getRateVolumePerUnit() )
							.toComparison();
			
			if (result != 0) {
				return false;
			}
			
			if ( ! compareFreq(ivRxDrug1.getDailyFreq(), ivRxDrug2.getDailyFreq())) {
				return false;
			}

			// compare IpMedAdditiveIv
			if ( (ivRxDrug1.getIvAdditiveList() != null && ivRxDrug2.getIvAdditiveList() == null) ||
					(ivRxDrug1.getIvAdditiveList() == null && ivRxDrug2.getIvAdditiveList() != null) ) {
				return false;
			}
			
			if (ivRxDrug1.getIvAdditiveList().size() != ivRxDrug2.getIvAdditiveList().size()) {
				return false;
			}
				
			if (ivRxDrug1.getIvAdditiveList() != null && ivRxDrug1.getIvAdditiveList().size() > 0 && 
					ivRxDrug2.getIvAdditiveList() != null && ivRxDrug2.getIvAdditiveList().size() > 0) {
				Map<Integer, IvAdditive> ivAdditive2Map = new HashMap<Integer, IvAdditive>(); 
				for (IvAdditive ivAdditive2 : ivRxDrug2.getIvAdditiveList()) {
					ivAdditive2Map.put(ivAdditive2.getAdditiveNum(), ivAdditive2);
				}
				
				for (IvAdditive ivAdditive1 : ivRxDrug1.getIvAdditiveList()) {
					IvAdditive ivAdditive2 = ivAdditive2Map.get(ivAdditive1.getAdditiveNum());
					
					if (ivAdditive2 == null) {
						return false;
					}
	
					result = new CompareToBuilder()
									.append( ivAdditive1.getVolume(), ivAdditive2.getVolume() )
									.append( ivAdditive1.getVolumeUnit(), ivAdditive2.getVolumeUnit() )
									// note: skip compare volText as it does not map to moi
									.append( ivAdditive1.getItemCode(), ivAdditive2.getItemCode() )
									.append( ivAdditive1.getTradeName(), ivAdditive2.getTradeName() )
									.append( ivAdditive1.getStrength(), ivAdditive2.getStrength() )
									.append( ivAdditive1.getBaseUnit(), ivAdditive2.getBaseUnit() )
									.append( ivAdditive1.getFormCode(), ivAdditive2.getFormCode() )
									.append( ivAdditive1.getRouteCode(), ivAdditive2.getRouteCode() )
									.append( ivAdditive1.getRouteDesc(), ivAdditive2.getRouteDesc() )
									.append( ivAdditive1.getFormDesc(), ivAdditive2.getFormDesc() )
									.append( ivAdditive1.getSaltProperty(), ivAdditive2.getSaltProperty() )
									.append( ivAdditive1.getExtraInfo(), ivAdditive2.getExtraInfo() )
									.append( ivAdditive1.getDosage(), ivAdditive2.getDosage() )
									.append( ivAdditive1.getDosageUnit(), ivAdditive2.getDosageUnit() )
									.append( ivAdditive1.getActionStatus(), ivAdditive2.getActionStatus() )
									.append( ivAdditive1.getFmStatus(), ivAdditive2.getFmStatus() )
									.append( ivAdditive1.getDisplayName(), ivAdditive2.getDisplayName() )
									// note: skip compare doseRuleCode as it does not map to moi
									// note: skip compare dosageCompute as it does not map to moi
									// note: skip compare ipFullDesc as it does not map to moi
									.append( ivAdditive1.getAliasName(), ivAdditive2.getAliasName() )
									// note: skip compare siteCode as it does not map to moi
									// note: skip compare supSiteDesc as it does not map to moi
									// note: skip compare siteDesc as it does not map to moi
									.append( ivAdditive1.getDosageEnd(), ivAdditive2.getDosageEnd() )
									// TODO: add strengthValue and strengthUnit after confirmation
									.append( ivAdditive1.getNoDiluentRequireFlag(), ivAdditive2.getNoDiluentRequireFlag() )
									.toComparison();
					
					if (result != 0) {
						return false;
					}
				}
			}

			// compare IpMedFluid
			if ( (ivRxDrug1.getIvFluidList() != null && ivRxDrug2.getIvFluidList() == null) ||
					(ivRxDrug1.getIvFluidList() == null && ivRxDrug2.getIvFluidList() != null) ) {
				return false;
			}
			
			if (ivRxDrug1.getIvFluidList().size() != ivRxDrug2.getIvFluidList().size()) {
				return false;
			}
				
			if (ivRxDrug1.getIvFluidList() != null && ivRxDrug1.getIvFluidList().size() > 0 &&
					ivRxDrug2.getIvFluidList() != null && ivRxDrug2.getIvFluidList().size() > 0) {
				Map<Integer, IvFluid> ivFluid2Map = new HashMap<Integer, IvFluid>(); 
				for (IvFluid ivFluid2 : ivRxDrug2.getIvFluidList()) {
					ivFluid2Map.put(ivFluid2.getFluidNum(), ivFluid2);
				}
				
				for (IvFluid ivFluid1 : ivRxDrug1.getIvFluidList()) {
					IvFluid ivFluid2 = ivFluid2Map.get(ivFluid1.getFluidNum());
					
					if (ivFluid2 == null) {
						return false;
					}
	
					result = new CompareToBuilder()
									.append( ivFluid1.getDiluentName(), ivFluid2.getDiluentName() )
									.append( ivFluid1.getDiluentAbbr(), ivFluid2.getDiluentAbbr() )
									.append( ivFluid1.getFluidVolume(), ivFluid2.getFluidVolume() )
									.append( ivFluid1.getFluidVolumeUnit(), ivFluid2.getFluidVolumeUnit() )
									.append( ivFluid1.getFluidQty(), ivFluid2.getFluidQty() )
									.append( ivFluid1.getDilutionMethod(), ivFluid2.getDilutionMethod() )
									.append( ivFluid1.getActionStatus(), ivFluid2.getActionStatus() )
									.toComparison();
					
					if (result != 0) {
						return false;
					}
				}
			}
			
		} else {
			throw new UnsupportedOperationException("Unsupported item type for chemo order, rxItemType = " + medProfileMoItem1.getRxItemType());
		}
		
		return true;
	}
	
	private boolean compareFreq(Freq oldFreq, Freq newFreq) {
		boolean oldFreqCodeExist = false;
		boolean newFreqCodeExist = false;
		if (oldFreq != null && oldFreq.getCode() != null) {
			oldFreqCodeExist = true;
		}
		if (newFreq != null && newFreq.getCode() != null) {
			newFreqCodeExist = true;
		}
		if ( ! oldFreqCodeExist && ! newFreqCodeExist) {
			return true;
		}
		if (oldFreqCodeExist != newFreqCodeExist) {
			return false;
		}
		
		if ( ! StringUtils.equals(oldFreq.getCode(), newFreq.getCode()) ) {
			return false;
		}

		if ( ! "E".equals(oldFreq.getMultiplierType()) || ! "E".equals(newFreq.getMultiplierType()) ) {
			if ( ! StringUtils.equals(oldFreq.getDesc(), newFreq.getDesc()) ) {
				return false;
			}
		}
		
		boolean oldFreqParamExist = false;
		boolean newFreqParamExist = false;
		if (oldFreq.getParam() != null && oldFreq.getParam().length > 0) {
			oldFreqParamExist = true;
		}
		if (newFreq.getParam() != null && newFreq.getParam().length > 0) {
			newFreqParamExist = true;
		}		
		if ( ! oldFreqParamExist && ! newFreqParamExist) {
			return true;
		}
		if (oldFreqParamExist != newFreqParamExist) {
			return false;
		}		
		
		if (oldFreq.getParam().length != newFreq.getParam().length) {
			return false;
		}
		
		for (int i=0; i<newFreq.getParam().length; i++) {
			if ( ! StringUtils.equals(oldFreq.getParam()[i], newFreq.getParam()[i]) ) {
				return false;
			}
		}
		
		return true;
	}
	
	public MedProfileMoItem extractMedProfileMoItemFromMedProfileOrder(MedProfileOrder medProfileOrder) {
		if (medProfileOrder.getType() == MedProfileOrderType.Modify) {
			return medProfileOrder.getMedProfileMoItemList().get(1);
		} else {
			return medProfileOrder.getMedProfileMoItemList().get(0);
		}
	}
}
