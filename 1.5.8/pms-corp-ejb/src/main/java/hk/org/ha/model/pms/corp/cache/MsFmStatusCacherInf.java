package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.MsFmStatus;

import java.util.List;

public interface MsFmStatusCacherInf extends BaseCacherInf {

    MsFmStatus getMsFmStatusByFmStatus(String fmStatus);
	
	List<MsFmStatus> getMsFmStatusList();
	
	List<MsFmStatus> getMsFmStatusListByFmStatus(String prefixFmStatus);

}
