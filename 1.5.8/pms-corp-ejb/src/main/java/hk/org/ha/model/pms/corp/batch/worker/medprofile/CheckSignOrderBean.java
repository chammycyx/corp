package hk.org.ha.model.pms.corp.batch.worker.medprofile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.medprofile.CorpPmsSignOrderCoordinatorLocal;
import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;
import hk.org.ha.service.pms.asa.interfaces.AsaServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("checkSignOrderBean")
public class CheckSignOrderBean implements CheckSignOrderLocal {
	
	private static final String LABEL_PATTERN = "%1$-9s";
	private static final String COUNT_PATTERN = "%1$-9d";
	
	private Logger logger = null;
	
	private Date batchDate;
	
	@In
	private AsaServiceJmsRemote asaServiceProxy;
	
	@In
	private CorpPmsSignOrderCoordinatorLocal corpPmsSignOrderCoordinator;
	
    public CheckSignOrderBean() {
    }

    public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);

		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
	}
	
	public void processRecord(Record record) throws Exception {
		List<SignatureCountResult> scResultList = new ArrayList<SignatureCountResult>();
		List<SignatureCountResult> pmsScResultList = corpPmsSignOrderCoordinator.retrievePmsSignatureCountResultList(UUID.randomUUID().toString(), batchDate);		
		StringBuffer resultStr = new StringBuffer();
		SignatureCountResult signatureCountResult;
		Date orderDate = batchDate;
		boolean isMatchFlag = true;
		
		logger.debug("batchDate | "+batchDate);
		logger.debug("size of pmsScResultList | "+pmsScResultList.size());
		
		for (SignatureCountResult pmsSignatureCountResult : pmsScResultList) {
			
			scResultList = asaServiceProxy.retrieveSignatureCountByHospCodeDate(pmsSignatureCountResult.getHospitalCode(), pmsSignatureCountResult.getHospitalCode(), batchDate);	
			if ( !scResultList.isEmpty() ) {
				
				signatureCountResult = scResultList.get(0);
				
				if ( pmsSignatureCountResult.getCount() != signatureCountResult.getCount() ) {
					resultStr.append(String.format(LABEL_PATTERN,pmsSignatureCountResult.getHospitalCode()))
					.append(String.format(COUNT_PATTERN,signatureCountResult.getCount()))
					.append(String.format(COUNT_PATTERN,pmsSignatureCountResult.getCount()))
					.append("NOT Match")
					.append('\n');
					isMatchFlag = false;
				} else {
					resultStr.append(String.format(LABEL_PATTERN,pmsSignatureCountResult.getHospitalCode()))
					.append(String.format(COUNT_PATTERN,signatureCountResult.getCount()))
					.append(String.format(COUNT_PATTERN,pmsSignatureCountResult.getCount()))
					.append('\n');
				}
			} else {
				resultStr.append(String.format(LABEL_PATTERN,pmsSignatureCountResult.getHospitalCode()))
				.append(String.format(COUNT_PATTERN,0))
				.append(String.format(COUNT_PATTERN,pmsSignatureCountResult.getCount()))
				.append("NOT Match")
				.append('\n');
			}
		}
	
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern("dd/MM/yyyy");
		if ( !isMatchFlag ) {
			logger.error("IPMOE - Signed order count checking: Warning! Count NOT match\nOrder Date: "
					+dateFormat.format(orderDate)
					+"\n"
					+"Hospital Signed   Received Status\n"
					+resultStr.toString());
		} else {
			logger.info("IPMOE - Signed order count checking: Count match (Order Date: "
					+ dateFormat.format(orderDate)
					+ ")");
		}
	}

	@Remove
	public void destroyBatch() {
	}
}
