package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.report.FmReportType;

import java.util.Arrays;
import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

/**
 * Session Bean implementation class DayendMainBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("fmRptCorpStatBean")
@MeasureCalls
public class FmRptCorpStatBean implements FmRptCorpStatLocal {
		
	@In
	private FmRptManagerLocal fmRptManager;
	
	@In
	private FmRptFilesManagerLocal fmRptFilesManager;
		
	private Logger logger = null;
	private String jobId = null;
	private DateTime startDate = null;
	
	private Date batchDate;
	private DateTime batchStartDate = null;
	
    /**
     * Default constructor. 
     */
    public FmRptCorpStatBean() {
        // TODO Auto-generated constructor stub
    }

	public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void endChunk() throws Exception {
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        } else {
        	batchDate = params.getDate("batchDate");
        	batchStartDate = new DateTime(batchDate).dayOfMonth().withMinimumValue();
        }
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
	
	public void processRecord(Record record) throws Exception {
		startDate = new DateTime();
		logger.info("Generate Corp Statistic Report with batch date " + batchStartDate + " start ");
		long startTime;
		long generateMonthlyCorpSpecStatReportTime;
		long generateMonthlyCorpStatReportTime;
		long generateYearlyAccumulatedCorpStatReportTime;
		
		try {
			startTime = fmRptManager.processBeginTime();
	    	fmRptManager.generateMonthlyCorpSpecStatReport(batchStartDate, jobId);
	    	generateMonthlyCorpSpecStatReportTime = fmRptManager.totalPorcessTime(startTime);
	    	
	    	startTime = fmRptManager.processBeginTime();
	    	fmRptManager.generateMonthlyCorpStatReport(batchStartDate, jobId);
	    	generateMonthlyCorpStatReportTime = fmRptManager.totalPorcessTime(startTime);
	    	
	    	startTime = fmRptManager.processBeginTime();
	    	fmRptManager.generateYearlyAccumulatedCorpStatReport(batchStartDate, jobId);
	    	generateYearlyAccumulatedCorpStatReportTime = fmRptManager.totalPorcessTime(startTime);
	    	
	    	fmRptFilesManager.moveToDestDirectory(batchStartDate, "ALL", "ALL", jobId, startDate,
	    									 	  Arrays.asList(FmReportType.MonthlyCorpSpecStat, FmReportType.MonthlyCorpStat, 
					   											FmReportType.YearlyAccumulatedCorpStat));
	    	
	    	logger.info("Generate cluster statistic report for corporate completed" +
    				"\n1. generateMonthlyCorpSpecStatReport time used: " + generateMonthlyCorpSpecStatReportTime +
    				"\n2. generateMonthlyCorpStatReport time used: " + generateMonthlyCorpStatReportTime + 
    				"\n3. generateYearlyAccumulatedCorpStatReport time used: " + generateYearlyAccumulatedCorpStatReportTime);
		} catch (Exception e) {
			logger.info(jobId + " Fail. Move files to error folder");
    		fmRptFilesManager.moveToErrorDirectory(jobId, startDate);
    		throw e;
		}
    }
}
