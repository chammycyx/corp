package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;

public class CddhDispenseInfoCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	private String patHospCode;
	private String dispHospCode;
	private Integer moeOrderNum;
	private String hospCode;
	private String workstationCode;
	private String userCode;
	private String patKey;
	private String caseNum;
	private String enquiryType;
	private String userRank;
	private String enquirySystem;

	public String getDispHospCode() {
		return dispHospCode;
	}
	public void setDispHospcode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}
	public String getPatHospCode() {
		return patHospCode;
	}
	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}
	public Integer getMoeOrderNum() {
		return moeOrderNum;
	}
	public void setMoeOrderNum(Integer moeOrderNum) {
		this.moeOrderNum = moeOrderNum;
	}
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public String getWorkstationCode() {
		return workstationCode;
	}
	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getPatKey() {
		return patKey;
	}
	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getEnquiryType() {
		return enquiryType;
	}
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}
	public String getUserRank() {
		return userRank;
	}
	public void setUserRank(String userRank) {
		this.userRank = userRank;
	}
	public String getEnquirySystem() {
		return enquirySystem;
	}
	public void setEnquirySystem(String enquirySystem) {
		this.enquirySystem = enquirySystem;
	}
}
