package hk.org.ha.model.pms.corp.batch.worker.reftable.moe;

import static hk.org.ha.model.pms.corp.prop.Prop.MEDPROFILE_ENABLED;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.YesNoFlag;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;


/**
 * Session Bean implementation class ExportDispTrxBean
 */
@AutoCreate
@Stateless
@Name("activeMpHospitalManager")
@MeasureCalls
public class ActiveMpHospitalManagerBean implements ActiveMpHospitalManagerLocal {
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
    private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@SuppressWarnings("unchecked")
	public List<String> retrieveActiveMpPatHospCodeList(){
		List<String> patHospCodeList = new ArrayList<String>();
		List<WorkstoreProp> workstorePropList = em.createQuery(
													"select o from WorkstoreProp o" + // 20160616 index check : Prop.name : UI_PROP_01
													" where o.prop.name = :propName" +
													" and o.value = :value")
													.setParameter("propName", MEDPROFILE_ENABLED.getName())
													.setParameter("value", YesNoFlag.Yes.getDataValue())
													.getResultList();
		
		if ( !workstorePropList.isEmpty() ) {
			List<Pair<String, String>> workstoreList = new ArrayList<Pair<String, String>>();
			for ( WorkstoreProp workstoreProp : workstorePropList ) {
				Workstore wortkstore = workstoreProp.getWorkstore();
				workstoreList.add(new Pair(wortkstore.getHospCode(), wortkstore.getWorkstoreCode()));
			}
			patHospCodeList.addAll(dmsPmsServiceProxy.retrieveIpSpecMappingPatHospCodeListByDispHospCodeWorkstore(workstoreList));
		}
		return patHospCodeList;
	}
}
