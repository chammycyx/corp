package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.udt.disp.DispOrderBatchProcessingType;
import hk.org.ha.model.pms.vo.disp.DayEndDispOrder;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.apache.log4j.Logger;

import com.jcraft.jsch.SftpException;

@Local
public interface DayEndOrderManagerLocal {
	
	void preProcessDayEnd(
			Logger logger,
			String hospCode, 
			String workstoreCode, 
			Date startDate,
			Date batchDate);

	void preProcessUncollect(
			Logger logger,
			String hospCode, 
			Date startDate,
			Date batchDate,
			String fcsUncollectTrxFile);
	
	void preProcessSendSuspend(
			Logger logger,
			String hospCode,
			String workstoreCode, 
			Date startDate,
			Date batchDate);
	
	List<DayEndDispOrder> processDayEnd(
			Logger logger,
			String hospCode,
			String workstoreCode,
			Date startDate,
			Date batchDate,
			File chargeTrx,
			File dlTrx,
			List<String> exceptItemList) throws Exception;
	
	List<List<DayEndDispOrder>> processUncollect(
			Logger logger,
			String hospCode, 
			Date startDate,
			Date batchDate,
			File uncollectTrxFile,
			File uncollectCountTrxFile) throws Exception;
	
	List<List<DayEndDispOrder>> processSendSuspend(
			Logger logger,
			String hospCode, 
			String workstoreCode,
			Date startDate,
			Date batchDate) throws Exception;
	
	void postProcessDayEnd(
			Logger logger,
			String hospCode,
			String workstoreCode,			
			Date batchDate,
			List<DayEndDispOrder> dayEndDispOrderList,
			String batchRemark) throws Exception;
	
	void postProcessUncollect(
			Logger logger,
			String hospCode,
			List<DayEndDispOrder> dayEndDispOrderIdList,
			List<DayEndDispOrder> nonDayEndDispOrderIdList,
			String batchRemark) throws Exception;
	
	void postProcessSendSuspend(
			Logger logger,
			String hospCode,
			String workstoreCode,
			List<DayEndDispOrder> dayEndDispOrderListForSuspend,
			List<DayEndDispOrder> dayEndDispOrderListForUnsuspend,
			String batchRemarkSuspend,
			String batchRemarkUnSuspend) throws Exception;
		
	void fallbackDayEndStatusType(
			Logger logger,
			String hospCode, 
			String workstoreCode,
			List<DayEndDispOrder> dayEndDispOrderList,
			DispOrderBatchProcessingType batchProcessingType) throws Exception;
	
	BatchJobState retrieveBatchJobState(
			String jobId, 
			Date batchDate, 
			String hospCode, 
			String workstoreCode);
	
	void saveBatchJobState(
			String jobId, 
			Date batchDate, 
			String hospCode, 
			String workstoreCode);
	
	void uploadFcsFile(
			File chargingFtpSourceFile, 
			File chargingFtpSourceCountFile, 
			File uncollectFtpSourceFile, 
			File uncollectFtpSourceCountFile,  
			String jobId, 
			Date batchDate, 
			String hospCode) throws SftpException;
	
	void uploadPmsFile(
			File ftpSourceFile, 
			String pmsTargetPath, 
			String ftpTargetFileName,
			Logger logger, 
			String jobId, 
			Date batchDate, 
			String hospCode) throws SftpException, IOException;

}
