package hk.org.ha.model.pms.corp.biz.order;

import javax.ejb.Local;

@Local
public interface SeqNumServiceLocal  {

	String retrieveChargeOrderNum(String hospCode, String patHospCode);

	String retrieveOrderNum(String hospCode, String patHospCode);

	String retrieveRefNum();

	String retrieveNextRefNum(String refNum);
	
	Integer retrieveDispOrderNum();
}
