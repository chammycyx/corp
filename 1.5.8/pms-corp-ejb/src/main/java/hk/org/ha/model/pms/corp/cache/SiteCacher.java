package hk.org.ha.model.pms.corp.cache;

import java.util.Collection;
import java.util.List;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.Site;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("siteCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class SiteCacher extends BaseCacher implements SiteCacherInf {
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	
	
	private InnerCacher cacher;
	
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}
	
	public List<Site> getSiteList() {
		return (List<Site>) this.getCacher().getAll();
	}	

	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}
	
	private class InnerCacher extends AbstractCacher<String, Site> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public Site create(String siteCode) {
			this.getAll();
			return this.internalGet(siteCode);
		}

		@Override
		public Collection<Site> createAll() {
			return dmsPmsServiceProxy.retrieveFullRouteSite();
		}

		@Override
		public String retrieveKey(Site site) {
			return site.getSiteCode();
		}
	}

	public static SiteCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (SiteCacherInf) Component.getInstance("siteCacher",
				ScopeType.APPLICATION);
	}
}
