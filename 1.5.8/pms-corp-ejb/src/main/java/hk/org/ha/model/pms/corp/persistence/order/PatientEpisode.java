package hk.org.ha.model.pms.corp.persistence.order;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.corp.udt.order.PatientEpisodeStatus;
import hk.org.ha.model.pms.corp.udt.order.PatientEpisodeType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "PATIENT_EPISODE")
public class PatientEpisode extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "patientEpisodeSeq")
	@SequenceGenerator(name = "patientEpisodeSeq", sequenceName = "SQ_PATIENT_EPISODE", initialValue = 100000000)
	private Long id;
	
	@Column(name = "BATCH_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date batchDate;
	
	@Column(name = "TRX_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date trxDate;
	
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;

	@Column(name = "PAT_KEY", nullable = false, length = 8)
	private String patKey;
	
	@Column(name = "PREV_PAT_KEY", nullable = false, length = 8)
	private String prevPatKey;
	
	@Column(name = "HKID", nullable = false, length = 12)
	private String hkid;
	
	@Column(name = "PREV_HKID", nullable = false, length = 12)
	private String prevHkid;
	
	@Column(name = "CASE_NUM", length = 12)
	private String caseNum;
			
	@Converter(name = "PatientEpisode.type", converterClass = PatientEpisodeType.Converter.class)
    @Convert("PatientEpisode.type")
	@Column(name="TYPE", nullable = false, length = 3)
	private PatientEpisodeType type;
	
	@Converter(name = "PatientEpisode.status", converterClass = PatientEpisodeStatus.Converter.class)
    @Convert("PatientEpisode.status")
	@Column(name="STATUS", length = 1, nullable = false)
	private PatientEpisodeStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getBatchDate() {
		if(batchDate == null) {
			return null;
		} else  {
			return new Date(batchDate.getTime());
		}
	}

	public void setBatchDate(Date batchDate) {
		if(batchDate == null) {
			this.batchDate = null;
		} else {
			this.batchDate = new Date(batchDate.getTime());
		}
	}

	public Date getTrxDate() {
		if(trxDate == null) {
			return null;
		} else {
			return new Date(trxDate.getTime());
		}
	}

	public void setTrxDate(Date trxDate) {
		if(trxDate == null) {
			this.trxDate = null;
		} else {
			this.trxDate = new Date(trxDate.getTime());
		}
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getPrevPatKey() {
		return prevPatKey;
	}

	public void setPrevPatKey(String prevPatKey) {
		this.prevPatKey = prevPatKey;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getPrevHkid() {
		return prevHkid;
	}

	public void setPrevHkid(String prevHkid) {
		this.prevHkid = prevHkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public PatientEpisodeStatus getStatus() {
		return status;
	}

	public void setStatus(PatientEpisodeStatus status) {
		this.status = status;
	}

	public PatientEpisodeType getType() {
		return type;
	}

	public void setType(PatientEpisodeType type) {
		this.type = type;
	}
}
