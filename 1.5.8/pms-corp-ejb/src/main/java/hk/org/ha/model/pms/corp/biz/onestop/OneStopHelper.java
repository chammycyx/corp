package hk.org.ha.model.pms.corp.biz.onestop;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderStatus;
import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.patient.DhPatient;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("oneStopHelper")
@Scope(ScopeType.APPLICATION)
public class OneStopHelper {
	
	@Logger
	private Log logger;
	
	private static final Pattern dohCaseNumPattern = Pattern.compile("^DHP|CIM[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$"); 
//	private static final Pattern dohCaseNumPattern2 = Pattern.compile("^CIM[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$"); 
		
	public OneStopOrderItemSummary convertSummaryFromUnvetOrder(MedOrder medOrder) 
	{
		OneStopOrderItemSummary oneStopOrderItemSummary = new OneStopOrderItemSummary();
		oneStopOrderItemSummary.setId(medOrder.getId());
		oneStopOrderItemSummary.setOneStopOrderType(convertOneStopOrderType(medOrder));
		oneStopOrderItemSummary.setRefNum(convertRefNum(medOrder));
		oneStopOrderItemSummary.setPatHospCode(medOrder.getPatHospCode());		
		oneStopOrderItemSummary.setDoctorCode(medOrder.getDoctorCode());
		oneStopOrderItemSummary.setDoctorName(medOrder.getDoctorName());
		oneStopOrderItemSummary.setPrescType(medOrder.getPrescType());
		oneStopOrderItemSummary.setOrderNum(convertOrderNum(medOrder));
		oneStopOrderItemSummary.setOrderDate(medOrder.getOrderDate());
		oneStopOrderItemSummary.setOrignalWks(medOrder.getWorkstore().getWorkstoreCode());
		oneStopOrderItemSummary.setMedOrderVersion(medOrder.getVersion());
		oneStopOrderItemSummary.setPrivateFlag(medOrder.getMedCase().getPasPatInd() == MedCasePasPatInd.Normal?false:true);
		oneStopOrderItemSummary.setPreVetUrgentFlag(medOrder.getUrgentFlag());
		oneStopOrderItemSummary.setMedOrderPhsSpecCode(medOrder.getSpecCode());
		oneStopOrderItemSummary.setMedOrderPhsWardCode(medOrder.getWardCode());
		oneStopOrderItemSummary.setPatType(convertPatType(medOrder.getMedCase()));		
		oneStopOrderItemSummary.setMedCase(cloneMedCase(medOrder));		
		
		if (medOrder.isDhOrder()) {
			convertSummaryFromUnvetDhOrder(oneStopOrderItemSummary, medOrder);
		}
								
		return oneStopOrderItemSummary;
	}	
	
	private void convertSummaryFromUnvetDhOrder(OneStopOrderItemSummary oneStopOrderItemSummary, MedOrder medOrder)
	{
		medOrder.getDhPatient();			
		Patient patient = new Patient();
		if (!isDhPatientCannotBeConvert(medOrder.getDhPatient()))
		{
			patient.updateByPatientEntity(medOrder.getDhPatient());	
		}
		else
		{
			patient.setHkid(((medOrder.getDhPatient().getHkid() != null && medOrder.getDhPatient().getHkid().length() > 9) ? "" : medOrder.getDhPatient().getHkid()));
			oneStopOrderItemSummary.setIsCannotConvertDhPatient(true);
		}
				
		oneStopOrderItemSummary.setPatient(patient);		
		oneStopOrderItemSummary.setIsLatestDhOrder(medOrder.getDhLatestFlag());
		
		if (medOrder.getStatus() == MedOrderStatus.Deleted) {
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Cancelled);
		}
		else
		{
			oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Unvet);
		}
	}
	
	private Boolean isDhPatientCannotBeConvert(DhPatient dhPatient)
	{
		if (dhPatient.getHkid() != null && dhPatient.getHkid().length() > 9)
		{
			return true;
		}
		else if (dhPatient.getOtherDoc() != null && dhPatient.getOtherDoc().length() > 11)
		{
			return true;
		}
		else if (dhPatient.getName().length() > 48)
		{
			return true;
		}
		else if (dhPatient.getNameChi() != null && dhPatient.getNameChi().length() > 6)
		{
			return true;
		}
		
		return false;
	}
	
	public OneStopOrderItemSummary convertSummaryFromVettedOrder(DispOrder dispOrder, PharmOrder pharmOrder, MedOrder medOrder) 
	{	
		OneStopOrderItemSummary oneStopOrderItemSummary = new OneStopOrderItemSummary();
		oneStopOrderItemSummary.setId(medOrder.getId());
		oneStopOrderItemSummary.setPatType(pharmOrder.getPatType());
		oneStopOrderItemSummary.setPatHospCode(medOrder.getPatHospCode());
		oneStopOrderItemSummary.setPhsSpecCode(pharmOrder.getSpecCode());
		oneStopOrderItemSummary.setOrignalPhsSpecCode(pharmOrder.getSpecCode());
		oneStopOrderItemSummary.setPhsWardCode(pharmOrder.getWardCode());
		oneStopOrderItemSummary.setPatCatCode(pharmOrder.getPatCatCode());
		oneStopOrderItemSummary.setPrescType(medOrder.getPrescType());
		oneStopOrderItemSummary.setOrderDate(medOrder.getOrderDate());
		oneStopOrderItemSummary.setOrignalWks(medOrder.getWorkstore().getWorkstoreCode());
		oneStopOrderItemSummary.setDocType(medOrder.getDocType());
		oneStopOrderItemSummary.setHasPostComment(medOrder.getCommentFlag());
		oneStopOrderItemSummary.setMedCase(pharmOrder.getMedCase());
		oneStopOrderItemSummary.setOneStopOrderType(OneStopOrderType.DispOrder);
		oneStopOrderItemSummary.setPrivateFlag(pharmOrder.getPrivateFlag());
		oneStopOrderItemSummary.setMedOrderPhsSpecCode(medOrder.getSpecCode());
		oneStopOrderItemSummary.setMedOrderPhsWardCode(medOrder.getWardCode());
		oneStopOrderItemSummary.setOrderNum(convertOrderNum(medOrder));
		oneStopOrderItemSummary.setRefNum(convertRefNum(medOrder));
		oneStopOrderItemSummary.setDispOrderId(dispOrder.getId());
		oneStopOrderItemSummary.setDispOrderStatus(dispOrder.getStatus());
		oneStopOrderItemSummary.setDispWorkStore(dispOrder.getWorkstore().getWorkstoreCode());
		oneStopOrderItemSummary.setDispHospCode(dispOrder.getWorkstore().getHospCode());
		oneStopOrderItemSummary.setTicketDate(dispOrder.getTicket().getTicketDate());
		oneStopOrderItemSummary.setTicketNum(dispOrder.getTicket().getTicketNum());
		oneStopOrderItemSummary.setTicketWks(dispOrder.getWorkstore().getWorkstoreCode());
		oneStopOrderItemSummary.setTicketHospCode(dispOrder.getWorkstore().getHospCode());
		oneStopOrderItemSummary.setDispDate(dispOrder.getDispDate());
		oneStopOrderItemSummary.setPatient(pharmOrder.getPatient());
		oneStopOrderItemSummary.setDoctorCode(pharmOrder.getDoctorCode());
		oneStopOrderItemSummary.setDoctorName(pharmOrder.getDoctorName());
		oneStopOrderItemSummary.setReceiptNum(pharmOrder.getReceiptNum());

		if (medOrder.isDhOrder())
		{
			oneStopOrderItemSummary.setIsLatestDhOrder(medOrder.getDhLatestFlag());
			if (medOrder.getStatus() == MedOrderStatus.Deleted)
			{
				oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Cancelled);
			}
			else
			{
				oneStopOrderItemSummary.setOneStopOrderStatus(OneStopOrderStatus.Vetted);
			}			
		}
		
		return oneStopOrderItemSummary;
	}
	
	private MedCase cloneMedCase(MedOrder medOrder)
	{
		MedCase cloneMedCase = null;
		try {			
			cloneMedCase = (MedCase) BeanUtils.cloneBean(medOrder.getMedCase());
			cloneMedCase.setId(null);
		} catch (IllegalAccessException e) {
			logger.debug("IllegalAccessException: #0", e);
		} catch (InstantiationException e) {
			logger.debug("InstantiationException: #0", e);
		} catch (InvocationTargetException e) {
			logger.debug("InvocationTargetException: #0", e);
		} catch (NoSuchMethodException e) {
			logger.debug("NoSuchMethodException: #0", e);
		}
		
		return cloneMedCase;
	}
	
	private String convertOrderNum(MedOrder medOrder)
	{
		if (medOrder.isDhOrder())
		{
			return medOrder.getMedCase().getCaseNum(); 
		}
		
		return medOrder.getOrderNum();
	}
	
	private String convertRefNum(MedOrder medOrder)
	{
		String refNum = medOrder.getRefNum();
		if (medOrder.isDhOrder())
		{
			if (StringUtils.isBlank(refNum) || refNum.length() == 1) 
			{
				String str = medOrder.getMedCase().getCaseNum().substring(medOrder.getMedCase().getCaseNum().length() - 4);
				if ( StringUtils.isNotBlank(refNum) ) {
					str += "-" + refNum;
				}
				return str;
			}
			else {
				return refNum;
			}
		}
				
		return refNum;
	}
	
	private OneStopOrderType convertOneStopOrderType(MedOrder medOrder) {
		if (medOrder.isDhOrder()) 
		{
			return OneStopOrderType.DhOrder;
		}
		
		return OneStopOrderType.MedOrder;
	}
	
	private PharmOrderPatType convertPatType(MedCase medCase)
	{
		if (isDohCase(medCase.getCaseNum()))
		{
			return PharmOrderPatType.Doh;
		}
		
		if (medCase.getPasPatInd() == MedCasePasPatInd.Normal)
		{
			if (medCase.getPasPayCode() != null && medCase.getPasPayCode().startsWith("NE"))
			{
				return PharmOrderPatType.Nep;
			}
			else
			{
				return PharmOrderPatType.Public;
			}
		}
		else
		{
			return PharmOrderPatType.Private;
		}
	}
	
	public Boolean isDohCase(String caseNum) 
	{
		if (dohCaseNumPattern.matcher(caseNum).find()) 
		{
			return true;
		}
		
		return false;
	}	
}
