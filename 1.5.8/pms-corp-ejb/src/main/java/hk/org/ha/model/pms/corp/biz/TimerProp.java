package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.util.PropertiesHelper;

import java.io.IOException;
import java.util.Properties;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("timerProp")
@Scope(ScopeType.APPLICATION)
public class TimerProp {
		
	public static final String TIMER_PROPERTIES = "timer.properties";
	public static final String ACTIVATE = "activate";
	
	@Logger
	private Log logger;	

	public boolean isActivate() {
		try {
			Properties prop = PropertiesHelper.getProperties(TIMER_PROPERTIES, Thread.currentThread().getContextClassLoader(), 0);
			return Boolean.valueOf(prop.getProperty(ACTIVATE, Boolean.FALSE.toString()));
		} catch (IOException e) {
			logger.error("Unexpected error!", e);
			return false;
		}
	}
}
