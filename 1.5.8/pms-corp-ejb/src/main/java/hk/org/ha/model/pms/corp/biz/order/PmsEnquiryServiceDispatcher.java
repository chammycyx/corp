package hk.org.ha.model.pms.corp.biz.order;

import java.util.List;

import hk.org.ha.model.pms.corp.vo.moe.DispenseStatusInfo;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.service.biz.pms.interfaces.PmsEnquiryServiceJmsRemote;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("pmsEnquiryServiceDispatcher")
@Scope(ScopeType.APPLICATION)
public class PmsEnquiryServiceDispatcher implements PmsEnquiryServiceJmsRemote {
			
	private String componentNamePattern;
	
	public String getComponentNamePattern() {
		return componentNamePattern;
	}

	public void setComponentNamePattern(String componentNamePattern) {
		this.componentNamePattern = componentNamePattern;
	}

	public PmsEnquiryServiceJmsRemote getProxy(String clusterCode) {
		
		String componentName = componentNamePattern.replace("{clusterCode}",  StringUtils.capitalize(clusterCode.toLowerCase()));
				
		PmsEnquiryServiceJmsRemote jmsRemote = (PmsEnquiryServiceJmsRemote) Component.getInstance(componentName);
		
		if (jmsRemote == null) {
			throw new UnsupportedOperationException("Component " + componentName + " not found");
		}
		return jmsRemote;
	}
	
	public PmsEnquiryServiceJmsRemote getProxy(Workstore workstore) {
		
		String clusterCode = workstore.getHospital().getHospitalCluster().getClusterCode();	
		
		return getProxy(clusterCode);
	}

	@Override
	public boolean allowDeleteMpItem(String clusterCode, String orderNum, Integer itemNum) {
		
		PmsEnquiryServiceJmsRemote jmsRemote = this.getProxy(clusterCode);
		
		return jmsRemote.allowDeleteMpItem(clusterCode, orderNum, itemNum);
	}

	public List<DispenseStatusInfo> retrieveDispenseStatusInfoList(String clusterCode, String orderNum, Integer itemNum) {

		PmsEnquiryServiceJmsRemote jmsRemote = this.getProxy(clusterCode);
		
		return jmsRemote.retrieveDispenseStatusInfoList(clusterCode, orderNum, itemNum);
	}
}
