package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.cddh.moe.CorpCddhMoeServiceLocal;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItem;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItemCriteria;

import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("pharmRptService")
@MeasureCalls
public class PharmRptServiceBean implements PharmRptServiceLocal {

	@In
	private CorpCddhMoeServiceLocal corpCddhMoeService;
	

	public List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria) {
		return corpCddhMoeService.retrieveCddhDispenseInfoList(criteria);
	}
	
	public List<CddhDispenseItem> retrieveCddhDispenseItemList(CddhDispenseItemCriteria criteria) {
		return corpCddhMoeService.retrieveCddhDispenseItemList(criteria);
	}
}
