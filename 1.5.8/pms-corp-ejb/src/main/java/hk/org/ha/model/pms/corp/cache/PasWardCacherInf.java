package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.vo.patient.PasWard;

import java.util.List;

public interface PasWardCacherInf extends BaseCacherInf{

	List<PasWard> getPasWardList(String patHospCode);
}
