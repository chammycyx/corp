package hk.org.ha.model.pms.corp.biz.resend;

import hk.org.ha.fmk.pms.jsf.AbstractEnumSelectable;
import hk.org.ha.model.pms.udt.disp.DispTrxType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;


@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("dispTrxType")
public class DispTrxTypeBean extends AbstractEnumSelectable<DispTrxType> {
	// for the enum DispTrxType to be used by JSF
	
	public Class<DispTrxType> getEnumClass() {
		return DispTrxType.class;
	}
}



