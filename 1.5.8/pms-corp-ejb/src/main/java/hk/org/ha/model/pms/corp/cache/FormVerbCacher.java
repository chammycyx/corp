package hk.org.ha.model.pms.corp.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.Site;
import hk.org.ha.model.pms.dms.vo.SiteFormVerbSearchCriteria;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

@AutoCreate
@Name("formVerbCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class FormVerbCacher extends BaseCacher implements FormVerbCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	private Map<String, InnerCacher> cacherMap = new HashMap<String, InnerCacher>();	
	
	@Override
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}	
	
	public FormVerb getDefaultFormVerbByFormCode(String formCode) {
		List<FormVerb> formVerbList = (List<FormVerb>) this.getCacher(formCode).getAll();
		if ( formVerbList == null ) {
			return null;			
		}
		for (FormVerb formVerb : formVerbList) {			
			if (formVerb.getRank() == 1) {
				return formVerb;
			}			
		}		
		return null;
	}

	public List<FormVerb> getFormVerbList() {
		return (List<FormVerb>) this.getCacher(null).getAll();
	}

	public List<FormVerb> getFormVerbListByFormCode(String formCode) {		
		return (List<FormVerb>) this.getCacher(formCode).getAll();
	}

	private InnerCacher getCacher(String formCode) {
		synchronized (this) { 
			InnerCacher cacher = cacherMap.get( formCode );
			
			if ( cacher == null ) {
				SiteFormVerbSearchCriteria criteria = new SiteFormVerbSearchCriteria();
				criteria.setFormCode(formCode);
				cacher = new InnerCacher(criteria, this.getExpireTime());
				cacherMap.put( formCode, cacher );
			}
									
			return cacher;
		}
	}
	
	private class InnerCacher extends AbstractCacher<String, FormVerb>
	{				
		private SiteFormVerbSearchCriteria criteria=null;
		
		private List<FormVerb> convertSiteToFormVerb(List<Site> siteList) {
			List<FormVerb> resultList = new ArrayList<FormVerb>();
			FormVerb formVerb;
			for ( Site site : siteList ) {
				formVerb = new FormVerb();
				formVerb.setSiteCode(site.getSiteCode());
				resultList.add(formVerb);
			}
			return resultList;
		}
		
        public InnerCacher(SiteFormVerbSearchCriteria criteria, int expireTime) {
            super(expireTime);
            this.criteria = criteria;
        }
		
		@Override
		public FormVerb create(String formCode) {			
			throw new UnsupportedOperationException();
		}

		@Override
		public Collection<FormVerb> createAll() {
			if ( StringUtils.isEmpty(criteria.getFormCode()) ) {
				return convertSiteToFormVerb(dmsPmsServiceProxy.retrieveFullRouteSite());
			} else {
				return dmsPmsServiceProxy.retrieveRouteSiteFormVerb(criteria);
			}
		}

		@Override
		public String retrieveKey(FormVerb formVerb) {
			return null;
		}		
	}
	
	public static FormVerbCacherInf instance()
	{
		if (!Contexts.isApplicationContextActive())
		{
			throw new IllegalStateException("No active application scope");
		}
		return (FormVerbCacherInf) Component.getInstance("formVerbCacher", ScopeType.APPLICATION);
	}
}
