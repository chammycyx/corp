package hk.org.ha.model.pms.corp.biz.onestop;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopRefillItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("oneStopManager")
@Scope(ScopeType.STATELESS)
public class OneStopManagerBean implements OneStopManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	private static final String STATUS = "status";
	private static final String STATUS_1 = "status1";
	private static final String CASE_NUM = "caseNum";
	private static final String REFILL_FLAG = "refillFlag";
	private static final String DOC_TYPE = "docType";
	private static final String PHARM_ORDER_STATUS = "pharmOrderStatus";
	private static final String ORDER_TYPE = "orderType";
	private static final String PRESC_TYPE = "prescType";
	
	@In
	private OneStopHelper oneStopHelper;
	
	/**
	 * For DH Order Only!
	 */
	@SuppressWarnings("unchecked")
	public List<OneStopOrderItemSummary> retrieveOrderByCaseNum(String caseNum) 
	{	
		List<OneStopOrderItemSummary> oneStopOrderItemSummaryList = new ArrayList<OneStopOrderItemSummary>();
		List<OneStopOrderItemSummary> caseNumUnvetList = new ArrayList<OneStopOrderItemSummary>();
		List<OneStopOrderItemSummary> caseNumVettedList = new ArrayList<OneStopOrderItemSummary>();
				
		List<MedOrder> medOrderList = (List<MedOrder>) em.createQuery(
				"select o from MedOrder o" + // 20160616 index check : MedOrder.medCase : I_MED_CASE_01
				" where o.medCase.caseNum = :caseNum" +
				" and o.docType = :docType" +
				" and o.orderType = :orderType" +
				" and o.vetDate is null" +
				" and (o.status in :status or (o.status = :status1 and o.ticket is not null))")
				.setParameter(CASE_NUM, caseNum)
				.setParameter(DOC_TYPE, MedOrderDocType.Manual)
				.setParameter(ORDER_TYPE, OrderType.OutPatient)
				.setParameter(STATUS, MedOrderStatus.Outstanding_Deleted_Withhold_PreVet)
				.setParameter(STATUS_1, MedOrderStatus.Deleted)
				.getResultList();
		
		for (MedOrder medOrder : medOrderList) 
		{
			if (medOrder.getPharmOrderList().isEmpty()) 
			{
				caseNumUnvetList.add(oneStopHelper.convertSummaryFromUnvetOrder(medOrder));
			}
		}
		
		List<DispOrder> dispMoeOrderList =  (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20160616 index check : MedOrder.medCase : I_MED_CASE_01
						" where o.pharmOrder.medCase.caseNum = :caseNum" +
						" and o.pharmOrder.medOrder.status in :status" +
						" and o.refillFlag = :refillFlag" +
						" and o.pharmOrder.medOrder.docType = :docType" +
						" and o.pharmOrder.status <> :pharmOrderStatus" +
						" and o.pharmOrder.medOrder.orderType = :orderType" +
						" and o.pharmOrder.medOrder.prescType = :prescType")
						.setParameter(CASE_NUM, caseNum)
						.setParameter(STATUS, MedOrderStatus.Complete_Deleted)
						.setParameter(REFILL_FLAG, Boolean.FALSE)
						.setParameter(DOC_TYPE, MedOrderDocType.Manual)
						.setParameter(PHARM_ORDER_STATUS, PharmOrderStatus.SysDeleted)
						.setParameter(ORDER_TYPE, OrderType.OutPatient)
						.setParameter(PRESC_TYPE, MedOrderPrescType.Dh)
						.getResultList();
				
		for (DispOrder dispOrder : dispMoeOrderList) 
		{
			caseNumVettedList.add(oneStopHelper.convertSummaryFromVettedOrder(dispOrder, dispOrder.getPharmOrder(), dispOrder.getPharmOrder().getMedOrder()));
		}
		
		oneStopOrderItemSummaryList.addAll(caseNumUnvetList);
		oneStopOrderItemSummaryList.addAll(caseNumVettedList);
		return oneStopOrderItemSummaryList;
	}
		
}
