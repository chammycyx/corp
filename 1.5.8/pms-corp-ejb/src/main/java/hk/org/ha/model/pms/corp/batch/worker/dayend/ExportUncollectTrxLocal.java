package hk.org.ha.model.pms.corp.batch.worker.dayend;
import hk.org.ha.model.pms.persistence.disp.DispOrder;

import java.io.BufferedWriter;
import java.util.List;

import javax.ejb.Local;

@Local
public interface ExportUncollectTrxLocal {
	void createUncollectCount(
			int uncollectCount,
			BufferedWriter uncollectCountWriter) throws Exception;
	
	int process(
			List<DispOrder> dispOrderList, 
			BufferedWriter outWriter) throws Exception;
}
