package hk.org.ha.model.pms.corp.biz;

import static hk.org.ha.model.pms.corp.prop.Prop.CHARGING_STANDARD_CHARGEABLEUNIT;
import static hk.org.ha.model.pms.corp.prop.Prop.CHARGING_STANDARD_REFILL_PARTIALPAYMENT_ENABLED;
import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_REGIMEN_PRN_DURATION;
import static hk.org.ha.model.pms.corp.prop.Prop.VETTING_REGIMEN_PRN_PERCENT;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.cddh.moe.CorpCddhMoeServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.OrderManagerLocal;
import hk.org.ha.model.pms.corp.biz.order.OwnershipServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.PmsEnquiryServiceDispatcher;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.PasWardCacherInf;
import hk.org.ha.model.pms.corp.vo.medprofile.MoeMsgCount;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.ChargeRule;
import hk.org.ha.model.pms.corp.vo.moe.DispenseStatusInfo;
import hk.org.ha.model.pms.corp.vo.moe.OrderInfo;
import hk.org.ha.model.pms.corp.vo.moe.OrderInfoSearchCriteria;
import hk.org.ha.model.pms.corp.vo.moe.PmsOrderHdr;
import hk.org.ha.model.pms.corp.vo.moe.PrnDuration;
import hk.org.ha.model.pms.corp.vo.moe.RoutePrn;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.vo.PmsMoeIpSpecMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.vo.patient.PasWard;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.og.interfaces.OgCorpServiceJmsRemote;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("pharmService")
@MeasureCalls
public class PharmServiceBean implements PharmServiceLocal {

	@Logger
	private Log logger;
	

	@In
	private CorpCddhMoeServiceLocal corpCddhMoeService;
	
	@In
	private OrderManagerLocal orderManager;
	
	@In
	private OwnershipServiceLocal ownershipService;
	
	@In
	private PmsEnquiryServiceDispatcher pmsEnquiryServiceDispatcher;

	@In
    private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private PasWardCacherInf pasWardCacher;
	
	@In
	private OgCorpServiceJmsRemote ogCorpServiceProxy;
	
	private static final Map<String, Long> lastWardStockListRetrieveTime = Collections.synchronizedMap(new HashMap<String, Long>());

	private long wardStockListCooldownTime = 300000L;
	
	public static final String TRX_DATE_PATTERN = "yyyyMMdd.HH";
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
    public long getWardStockListCooldownTime() {
		return wardStockListCooldownTime;
	}

	public void setWardStockListCooldownTime(long wardStockListCooldownTime) {
		this.wardStockListCooldownTime = wardStockListCooldownTime;
	}

	public PmsOrderHdr retrievePmsOrderHdr(OrderInfoSearchCriteria criteria) {
    	if (logger.isDebugEnabled()) {
    		logger.debug("retrievePmsOrderHdr() - call with patHospCode: [#0], orderNum: [#1]", criteria.getPatHospCode(), criteria.getOrdNum());
    	}

    	try {
	    	PmsOrderHdr pmsOrderHdr = new PmsOrderHdr();
	    	
    		// get order from corp
	    	String orderNum = this.getPmsFormatOrderNum(criteria.getPatHospCode(), criteria.getOrdNum().toString());
	    	
	    	Boolean orderLocked = ownershipService.checkOwnershipLocked(orderNum);
	    	
    		PharmOrder pharmOrder = orderManager.retrievePharmOrder(orderNum);
    		
	    	if (pharmOrder == null) {
	    		// return empty object if order not found 
	    		return pmsOrderHdr;
	    	}
	    	
    		MedOrder medOrder = pharmOrder.getMedOrder();
	    	
			if (medOrder.getDocType() == MedOrderDocType.Normal) { 
				pmsOrderHdr.setMoeOrder("Y");
			} else {
				pmsOrderHdr.setMoeOrder("N");
			}
			if (pharmOrder.getId() != null) {
				if (pharmOrder.getAdminStatus() == PharmOrderAdminStatus.Revoke) {
					pmsOrderHdr.setUncollect("Y");
				} else {
					pmsOrderHdr.setUncollect("N");
				}
			} else {
				pmsOrderHdr.setUncollect("N");	// default value of unvet order
			}
			if (medOrder.getStatus() == MedOrderStatus.MoeSuspended) {
				pmsOrderHdr.setSuspend("Y");		
			} else {
				pmsOrderHdr.setSuspend("N");		// not related to PMS suspend
			}
			if (medOrder.getStatus() == MedOrderStatus.Outstanding || medOrder.getStatus() == MedOrderStatus.PreVet ||
				medOrder.getStatus() == MedOrderStatus.Withhold) {
				if (orderLocked) {
					pmsOrderHdr.setOrdStatus("PO");
				} else {
					pmsOrderHdr.setOrdStatus("O");
				}
			} else if (medOrder.getStatus() == MedOrderStatus.Complete) {
				if (orderLocked) {
					pmsOrderHdr.setOrdStatus("PV");
				} else {
					pmsOrderHdr.setOrdStatus("V");
				}
			} else if (medOrder.getStatus() == MedOrderStatus.Deleted) {
				if (pharmOrder.getId() == null) {
					pmsOrderHdr.setOrdStatus("CO");
				} else {
					pmsOrderHdr.setOrdStatus("CV");
				}
			}
			if (pharmOrder.getId() != null) {
				if (pharmOrder.getAdminStatus() == PharmOrderAdminStatus.AllowModify) {
					pmsOrderHdr.setAllowModify("Y");
				} else {
					pmsOrderHdr.setAllowModify("N");
				}
			} else {
				pmsOrderHdr.setAllowModify("Y");		// default value of unvet order
			}
			if (pharmOrder.getId() != null) {
				pmsOrderHdr.setAllowPostComment(pharmOrder.getAllowCommentType().getDataValue());
			} else {
				pmsOrderHdr.setAllowPostComment("N");	// default value of unvet order
			}
			if (medOrder.getDocType() == MedOrderDocType.Manual) {
				if (medOrder.getPrescType() == MedOrderPrescType.Out) {
					pmsOrderHdr.setOrdSubtype("O");
				} else {
					pmsOrderHdr.setOrdSubtype("I");
				} 
			} else {
				pmsOrderHdr.setOrdSubtype(medOrder.getCmsOrderSubType().getDataValue());
			}
			pmsOrderHdr.setLastUpdDate(medOrder.getCmsUpdateDate());
			if (medOrder.getRemarkCreateDate() != null) {	// not check by remarkStatus as it includes item remark
				pmsOrderHdr.setRemarkStatus("Y");
			}
			
	    	return pmsOrderHdr;
	    	
    	} catch (RuntimeException e) {
    		StringBuilder errorStr = new StringBuilder();
    		errorStr.append("retrievePmsOrderHdr() - getting PMS order info fails - ");
    		errorStr.append("[action=get_order_info");
    		errorStr.append("|patHospCode=");
    		errorStr.append(criteria.getPatHospCode());
    		errorStr.append("|ordNum=");
    		errorStr.append(criteria.getOrdNum());
    		errorStr.append("|dispHospCode=");
    		errorStr.append(criteria.getDispHospCode());
    		errorStr.append("|dispWorkstore=");
    		errorStr.append(criteria.getDispWorkstore());
    		errorStr.append("]");
    		
    		logger.error(errorStr.toString(), e);
    		throw e;
    	}
	}
    
	public List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria) {
		return corpCddhMoeService.retrieveCddhDispenseInfoList(criteria);
	}
	
	public String getPmsFormatOrderNum(String patHospcode, String ordNum){
		StringBuilder formatOrdNum = new StringBuilder();		
		if ( "0".equals(ordNum) ){
			return ordNum;
		}
		formatOrdNum.append( StringUtils.trimToNull(patHospcode) );
		if ( patHospcode.length() == 2 ){
			formatOrdNum.append(" ");
		}
		formatOrdNum.append(ordNum);			
		return formatOrdNum.toString();
	}	
	
	@SuppressWarnings("unchecked")
    public List<RoutePrn> retrieveRoutePrnList( String dispHospCode, String dispWorkstore ){
		return em.createQuery(
				"select new hk.org.ha.model.pms.corp.vo.moe.RoutePrn(o.routeCode, o.routeDesc)" + // 20120831 index check : PrnRoute.workstore : FK_PRN_ROUTE_01
				" from PrnRoute o" +
				" where o.hospCode = :hospCode" +
				" and o.workstoreCode = :workstoreCode")
    			.setParameter("hospCode", dispHospCode)
    			.setParameter("workstoreCode", dispWorkstore)
    			.getResultList();
    }
	
	public PrnDuration retrievePrnDuration( String dispHospCode, String dispWorkstore ){
		
		if( StringUtils.isNotBlank(dispHospCode) && StringUtils.isNotBlank(dispWorkstore) ){
			Workstore workstore = new Workstore(dispHospCode, dispWorkstore);
			
			PrnDuration prnDuration = new PrnDuration();
			prnDuration.setDispHospCode(dispHospCode);
			prnDuration.setDispWorkstore(dispWorkstore);
			prnDuration.setPrnDuration( VETTING_REGIMEN_PRN_DURATION.get(workstore) );
			Integer prnPercent = VETTING_REGIMEN_PRN_PERCENT.get(workstore);
			if( prnPercent != null ){
				prnDuration.setPrnPercent( prnPercent.doubleValue() / 100  );
			}
			
			return prnDuration;
		}else{
			return null;
		}
    }

    public ChargeRule retrieveChargeRule( String dispHospCode, String dispWorkstore ){
    	
    	if( StringUtils.isNotBlank(dispHospCode) && StringUtils.isNotBlank(dispWorkstore) ){
			Workstore workstore = new Workstore(dispHospCode, dispWorkstore);
			
			ChargeRule chargeRule = new ChargeRule();
			chargeRule.setDispHospCode(dispHospCode);
			chargeRule.setDispWorkstore(dispWorkstore);
			chargeRule.setChargeSplitInd( CHARGING_STANDARD_REFILL_PARTIALPAYMENT_ENABLED.get(workstore, false) );
			chargeRule.setChargeDaysPerUnit( CHARGING_STANDARD_CHARGEABLEUNIT.get(0) );
			
			return chargeRule;
		}else{
			return null;
		}
    }

	public boolean allowDeleteMpItem(String patHospCode, Long ordNum, Integer itemNum) {

		if (patHospCode == null || ordNum == null || itemNum == null) {
			throw new IllegalArgumentException();
		}
		String orderNum = this.formatPmsOrderNum(patHospCode, ordNum);		
		String clusterCode = orderManager.retrieveClusterCodeByPatHospCode(patHospCode);		
		return pmsEnquiryServiceDispatcher.allowDeleteMpItem(clusterCode, orderNum, itemNum);
	}
	
	private String formatPmsOrderNum(String patHospCode, Long ordNum) {
		
		StringBuilder formatOrdNum = new StringBuilder();		
		formatOrdNum.append( patHospCode.trim() );
		if ( patHospCode.length() == 2 ){
			formatOrdNum.append(" ");
		}
		formatOrdNum.append(ordNum);			
		return formatOrdNum.toString();
	}	
	
	
	@SuppressWarnings("unchecked")
	public List<hk.org.ha.model.pms.corp.vo.moe.WardStock> retrieveWardStockList(String patHospCode){

		Long lastRetrieveTime = lastWardStockListRetrieveTime.get(patHospCode);

		if (lastRetrieveTime != null) {
			if (System.currentTimeMillis() - wardStockListCooldownTime < lastRetrieveTime.longValue()) {
				throw new UnsupportedOperationException("Error : retrieve within a cool down period " + wardStockListCooldownTime + "ms for " + patHospCode);
			}
		}

		DateMidnight today = new DateMidnight();

		Map<String, hk.org.ha.model.pms.corp.vo.moe.WardStock> resultMap = new LinkedHashMap<String, hk.org.ha.model.pms.corp.vo.moe.WardStock>();
		List<PasWard> pasWardList = pasWardCacher.getPasWardList(patHospCode);
		List<String> pasWardStrList = new ArrayList<String>();
		Map<String, List<PmsMoeIpSpecMapping>> pmsMoeIpSpecMappingPasWardMap = new HashMap<String, List<PmsMoeIpSpecMapping>>();
		Set<String> instCodeSet = new HashSet<String>();
		List<WardStock> wardStockFullList = new ArrayList<WardStock>();

		for ( PasWard pasWard : pasWardList ) {
			pasWardStrList.add(pasWard.getPasWardCode());
		}
		
		List<PmsMoeIpSpecMapping> pmsMoeIpSpecMappingFullList = dmsPmsServiceProxy.retrievePmsMoeIpSpecialtyMappingByWardList(patHospCode, pasWardStrList);
		String defaultInstCode = StringUtils.EMPTY;
		for ( PmsMoeIpSpecMapping pmsMoeIpSpecMapping : pmsMoeIpSpecMappingFullList) {	
			if ( !pmsMoeIpSpecMappingPasWardMap.containsKey(pmsMoeIpSpecMapping.getPasWardCode()) ) {
				pmsMoeIpSpecMappingPasWardMap.put(pmsMoeIpSpecMapping.getPasWardCode(), new ArrayList<PmsMoeIpSpecMapping>());
			}
			pmsMoeIpSpecMappingPasWardMap.get(pmsMoeIpSpecMapping.getPasWardCode()).add(pmsMoeIpSpecMapping);
			instCodeSet.add(pmsMoeIpSpecMapping.getInstCode());
			if ( "*".equals(pmsMoeIpSpecMapping.getPasWardCode()) ) {
				defaultInstCode = pmsMoeIpSpecMapping.getInstCode();
			}
		}
		
		
		if ( !instCodeSet.isEmpty() ) {
			wardStockFullList = em.createQuery(
									"select o" +	// 20140605 index check : WardStock.instCode : I_WARD_STOCK_01
									" from  WardStock o" +
									" where o.instCode in :instCodeList" +
									" and   o.status = :status")
									.setParameter("instCodeList", new ArrayList<String>(instCodeSet))
									.setParameter("status", RecordStatus.Active)
									.setHint(QueryHints.BATCH, "o.ward")
									.getResultList();
		}
		
		Map<String, List<WardStock>> wardStockMap = new HashMap<String, List<WardStock>>();
		for ( WardStock wardStock : wardStockFullList ) {
			if ( !wardStockMap.containsKey(wardStock.getWardCode()) ) {
				wardStockMap.put(wardStock.getWardCode(), new ArrayList<hk.org.ha.model.pms.persistence.phs.WardStock>());
			}
			wardStockMap.get(wardStock.getWardCode()).add(wardStock);
		}
		
		for ( PasWard pasWard : pasWardList ) {
			List<PmsMoeIpSpecMapping> pmsMoeIpSpecMappingList = pmsMoeIpSpecMappingPasWardMap.get(pasWard.getPasWardCode());
			if ( pmsMoeIpSpecMappingList != null && !pmsMoeIpSpecMappingList.isEmpty() ) {
				for ( PmsMoeIpSpecMapping pmsMoeIpSpecMapping : pmsMoeIpSpecMappingList ) {
					List<WardStock> wardStockList = wardStockMap.get(pmsMoeIpSpecMapping.getPhsWardCode());
					if ( wardStockList != null ) {
						for ( WardStock wardStock : wardStockList ) {
							if ( StringUtils.equals(pmsMoeIpSpecMapping.getInstCode(), wardStock.getInstCode()) ) {
								String key = buildWardStockKey(patHospCode, wardStock.getItemCode(), pmsMoeIpSpecMapping.getPasWardCode());
								if ( !resultMap.containsKey(key) ) {
									hk.org.ha.model.pms.corp.vo.moe.WardStock newWardStock = buildWardStock(patHospCode, wardStock.getItemCode(), pmsMoeIpSpecMapping.getPasWardCode(), today);
									resultMap.put(key, newWardStock);
								}
							}
						}
					}
				}
			} else {
				List<WardStock> wardStockList = wardStockMap.get(pasWard.getPasWardCode());
				if ( wardStockList != null ) {
					for ( WardStock wardStock : wardStockList ) {
						if ( StringUtils.equals(defaultInstCode, wardStock.getInstCode()) ) {
							String key = buildWardStockKey(patHospCode, wardStock.getItemCode(), pasWard.getPasWardCode());
							if ( !resultMap.containsKey(key) ) {
								hk.org.ha.model.pms.corp.vo.moe.WardStock newWardStock = buildWardStock(patHospCode, wardStock.getItemCode(), pasWard.getPasWardCode(), today);
								resultMap.put(key, newWardStock);
							}
						}
					}
				}
			}
		}
		
		lastWardStockListRetrieveTime.put(patHospCode, Long.valueOf(System.currentTimeMillis()));
		
		return new ArrayList<hk.org.ha.model.pms.corp.vo.moe.WardStock>(resultMap.values());
	}
	
	private String buildWardStockKey(String patHospCode, String itemCode, String pasWardCode) {
		List<String> keyList = new ArrayList<String>();
		keyList.add(String.valueOf(patHospCode));
		keyList.add(String.valueOf(itemCode));
		keyList.add(String.valueOf(pasWardCode));
		return StringUtils.join(keyList.toArray(), "|");
	}
	
	private hk.org.ha.model.pms.corp.vo.moe.WardStock buildWardStock(String patHospCode, String itemCode, String pasWardCode, DateMidnight updateDate){
		hk.org.ha.model.pms.corp.vo.moe.WardStock returnWardStock = new hk.org.ha.model.pms.corp.vo.moe.WardStock(itemCode, null);
		returnWardStock.setWard(pasWardCode);
		DmDrug dmDrug = dmDrugCacher.getDmDrug(returnWardStock.getItemCode());
		if(dmDrug!= null){
			DmDrugProperty dmDrugProperty = dmDrug.getDmDrugProperty();
			returnWardStock.setDisplayname(dmDrugProperty.getDisplayname());
			returnWardStock.setFormCode(dmDrugProperty.getFormCode());
			returnWardStock.setSaltProperty(dmDrugProperty.getSaltProperty());
		}
		returnWardStock.setPatHospCode(patHospCode);
		returnWardStock.setUpdateDate(updateDate.toDate());
		return returnWardStock;
	}

	
	@SuppressWarnings("unchecked")
	public OrderInfo retrieveOrderInfo(String patHospCode, Long trxId) {
		
		List<OrderInfo> orderInfoList = em.createQuery(
				"select new " + OrderInfo.class.getName() + "(i.pharmOrderItem.medOrderItem.medOrder.patHospCode," + // 20130829 index check : DispOrderItem.deliveryItemId : I_DISP_ORDER_ITEM_01
				" i.pharmOrderItem.medOrderItem.medOrder.orderNum, i.pharmOrderItem.medOrderItem.medOrder.medCase.caseNum," +
				" i.pharmOrderItem.medOrderItem.itemNum, i.pharmOrderItem.medOrderItem.modifyFlag) from DispOrderItem i" +
				" where i.deliveryItemId = :deliveryItemId" +
				" and i.pharmOrderItem.medOrderItem.medOrder.patHospCode = :patHospCode" +
				" and i.pharmOrderItem.medOrderItem.orgItemNum < 10000 " +
				" order by i.deliveryItemId")
				.setParameter("patHospCode", patHospCode)
				.setParameter("deliveryItemId", trxId)
				.getResultList();

		return orderInfoList.isEmpty() ? null : orderInfoList.get(0); 
	}
	
	public List<MoeMsgCount> retrieveCmsMoeMessageCount(String patHospCode, String trxDate) {
		MoeMsgCount moeMsgCount;
		DateTime endDate, startDate;
		List<String> patHospCodeList = new ArrayList<String>();
		List<MoeMsgCount> resultList = new ArrayList<MoeMsgCount>();
		SimpleDateFormat dateFormat = new SimpleDateFormat(TRX_DATE_PATTERN);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
		patHospCodeList.add(patHospCode);
		
		try {
			startDate = new DateTime(dateFormat.parse(trxDate));
			endDate = startDate.plusHours(1);
			List<MoeMsgCount> moeMsgCountList = ogCorpServiceProxy.retrieveCmsMoeMsgCount(patHospCodeList, startDate.toDate(), endDate.toDate());
			
			Map<String, MoeMsgCount> trxCountMap = new HashMap<String, MoeMsgCount>();
			for (MpTrxType type:MpTrxType.getPmsToCmsTrxType()) {
				moeMsgCount = new MoeMsgCount();
				moeMsgCount.setHospitalCode(patHospCode);
				moeMsgCount.setTrxCode(type.getDataValue());
				moeMsgCount.setCount(0);
				trxCountMap.put(moeMsgCount.getHospitalCode()+moeMsgCount.getTrxCode(), moeMsgCount);
			}
			for (MoeMsgCount msgCount:moeMsgCountList) {
				trxCountMap.put(msgCount.getHospitalCode()+msgCount.getTrxCode(), msgCount);
			}
			
			resultList.addAll(trxCountMap.values());
		} catch (ParseException e) {
			logger.error("Invalid date format["+TRX_DATE_PATTERN+"]",e);
		}
		
		return resultList;
	}

	public List<DispenseStatusInfo> retrieveDispenseStatusInfoList(String patHospCode, Long ordNum, Integer itemNum) {
		
		if (patHospCode == null || ordNum == null || itemNum == null) {
			throw new IllegalArgumentException();
		}
		String orderNum = this.formatPmsOrderNum(patHospCode, ordNum);		
		String clusterCode = orderManager.retrieveClusterCodeByPatHospCode(patHospCode);		
		return pmsEnquiryServiceDispatcher.retrieveDispenseStatusInfoList(clusterCode, orderNum, itemNum);
	}
}
