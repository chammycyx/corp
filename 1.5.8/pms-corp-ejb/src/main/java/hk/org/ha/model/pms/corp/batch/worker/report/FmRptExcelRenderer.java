package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.udt.batch.worker.FmRptTemplateType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.Renderer;

@AutoCreate
@Name("fmRptExcelRenderer")
@Scope(ScopeType.APPLICATION)
@Synchronized(timeout=3600000)
public class FmRptExcelRenderer implements FmRptExcelRendererInf {

	@In(create = true)
	private Renderer renderer;
	
	public void retrieveFmTemplateReport(FmRptTemplateType fmRptTemplateType) {
		switch(fmRptTemplateType) {
			case FMDATAREPORT:
				renderer.render("/fmDataReport.xhtml");
				break;
			case FMHOSPSPECSTATREPORT:
				renderer.render("/fmHospSpecStatReport.xhtml");
				break;
			case FMHOSPSTATREPORT:
				renderer.render("/fmHospStatReport.xhtml");
				break;
			case FMYEARLYSTATREPORT:
				renderer.render("/fmYearlyStatReport.xhtml");
				break;
		}
	}
}
