package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("pharmRestService")
@MeasureCalls
public class PharmRestServiceBean implements PharmRestServiceLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	@Override
	public boolean initMsWorkstoreDrugCache(String hospCode, String workstoreCode) {		
		Workstore workstore = em.find(Workstore.class, new WorkstorePK(hospCode, workstoreCode));
		if (workstore != null) {
			pmsSubscriberProxy.purgeMsWorkstoreDrug(workstore);
			return true;
		}
		return false;
	}

}
