package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.persistence.disp.Patient;

import javax.ejb.Local;

@Local
public interface PatientManagerLocal  {

	Patient savePatient(Patient remotePatient);
}
