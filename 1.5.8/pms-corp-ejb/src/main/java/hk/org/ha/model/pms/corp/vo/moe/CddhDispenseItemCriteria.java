package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;

public class CddhDispenseItemCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	private String patKey;
	
	private String dispDateStart;
	
	private String dispDateEnd;
	
	private String hospCode;
	
	private String workstationCode;
	
	private String userCode;
	
	private String caseNum;
	
	private String enquiryType;
	
	private String userRank;
	
	private String enquirySystem;

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getDispDateStart() {
		return dispDateStart;
	}

	public void setDispDateStart(String dispDateStart) {
		this.dispDateStart = dispDateStart;
	}

	public String getDispDateEnd() {
		return dispDateEnd;
	}

	public void setDispDateEnd(String dispDateEnd) {
		this.dispDateEnd = dispDateEnd;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstationCode() {
		return workstationCode;
	}

	public void setWorkstationCode(String workstationCode) {
		this.workstationCode = workstationCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getEnquiryType() {
		return enquiryType;
	}

	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}

	public String getUserRank() {
		return userRank;
	}

	public void setUserRank(String userRank) {
		this.userRank = userRank;
	}

	public String getEnquirySystem() {
		return enquirySystem;
	}

	public void setEnquirySystem(String enquirySystem) {
		this.enquirySystem = enquirySystem;
	}
}
