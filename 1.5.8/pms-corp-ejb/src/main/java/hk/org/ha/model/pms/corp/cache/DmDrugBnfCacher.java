package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrugBnf;
import hk.org.ha.model.pms.dms.persistence.DmDrugBnfPK;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmDrugBnfCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmDrugBnfCacher extends BaseCacher implements DmDrugBnfCacherInf {
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;
	
	private List<DmDrugBnf> cachedDmDrugBnfList;
	
	private Map<String, List<DmDrugBnf>> dmDrugBnfMap = new HashMap<String, List<DmDrugBnf>>();
	
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
			cachedDmDrugBnfList = null;
			dmDrugBnfMap.clear();
		}
	}	
	
	public String getDmDrugBnfNum(String itemCode) {
		this.getDmDrugBnfList();

		if (!StringUtils.isBlank(itemCode)) {
			List<DmDrugBnf> dmDrugBnfList = dmDrugBnfMap.get(itemCode);
			for (DmDrugBnf dmDrugBnf : dmDrugBnfList) {
				if ("Y".equals(dmDrugBnf.getPrincipal())) {
					return dmDrugBnf.getCompId().getBnfNo();
				}
			}	
		}
		
		return "";
	}
		
	private List<DmDrugBnf> getDmDrugBnfList() 
	{
		synchronized (this)
		{
			List<DmDrugBnf> dmDrugBnfList = (List<DmDrugBnf>) this.getCacher().getAll();

			// check whether the Full DmDrugList expire
			if (cachedDmDrugBnfList == null || (cachedDmDrugBnfList != dmDrugBnfList)) 
			{
				// hold the reference
				cachedDmDrugBnfList = dmDrugBnfList;
				
				this.buildDmDrugBnfKeyMap(cachedDmDrugBnfList);
			}			
			return cachedDmDrugBnfList;
		}
		
	}
	
	private void buildDmDrugBnfKeyMap(List<DmDrugBnf> dmDrugBnfList)
	{
		dmDrugBnfMap.clear();
		
		for (DmDrugBnf dmDrugBnf : dmDrugBnfList) {
			String key = dmDrugBnf.getCompId().getItemCode();
			List<DmDrugBnf> drugBnfList = dmDrugBnfMap.get(key);
			if (drugBnfList == null) {	
				drugBnfList = new ArrayList<DmDrugBnf>();
				dmDrugBnfMap.put(key, drugBnfList);
			}
			drugBnfList.add(dmDrugBnf);
		}
		
	}
		
    private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
  }

	private class InnerCacher extends AbstractCacher<DmDrugBnfPK, DmDrugBnf>
	{		      
        public InnerCacher(int expireTime) {
              super(expireTime);              
        }
		
		@Override
		public DmDrugBnf create(DmDrugBnfPK key) {
			this.getAll();
			return this.internalGet(key);
		}

		@Override
		public Collection<DmDrugBnf> createAll() {
			return dmsPmsServiceProxy.retrieveDmDrugBnfList();
		}

		@Override
		public DmDrugBnfPK retrieveKey(DmDrugBnf dmDrugBnf) {
			return dmDrugBnf.getCompId();
		}		
	}
	
	public static DmDrugBnfCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmDrugBnfCacherInf) Component.getInstance("dmDrugBnfCacher", ScopeType.APPLICATION);
    }
}
