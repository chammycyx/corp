package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.fmk.pms.remote.annotations.Marshaller;
import hk.org.ha.fmk.pms.remote.annotations.MarshallerType;
import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;
import hk.org.ha.model.pms.corp.persistence.charging.PatientSfiProfile;
import hk.org.ha.model.pms.corp.persistence.report.MpWorkloadStat;
import hk.org.ha.model.pms.corp.vo.EndVettingResult;
import hk.org.ha.model.pms.corp.vo.StartVettingResult;
import hk.org.ha.model.pms.corp.vo.UnvetOrderResult;
import hk.org.ha.model.pms.persistence.RemarkEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.reftable.CorporateProp;
import hk.org.ha.model.pms.persistence.reftable.HospitalProp;
import hk.org.ha.model.pms.persistence.reftable.WorkstoreProp;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.disp.PharmOrderAdminStatus;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.onestop.AllowCommentType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
import hk.org.ha.model.pms.vo.checkissue.DispInfo;
import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
import hk.org.ha.model.pms.vo.report.DayEndFileExtractRpt;
import hk.org.ha.model.pms.vo.support.PropInfoItem;
import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface CorpPmsServiceJmsRemote {
	
	//obsolete after pms-pms version 3.4.2
	StartVettingResult startVetting(
			String orderNum, 
			Long medOrderId, 
			String hospCode);
	
	//start using in pms-pms verions 3.4.2
	StartVettingResult startVetting(
			String orderNum, 
			Long medOrderId, 
			String hospCode, 
			String hkid);

	//obsolete after pms-pms version 3.4.0.6
	EndVettingResult endVetting(
			DispOrder dispOrder, 
			Long prevPharmOrderId, 
			String hospCode, 
			Boolean remarkFlag,
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled);

	//start using in pms-pms verions 3.4.0.6
	EndVettingResult endVetting(
			DispOrder dispOrder, 
			Long prevPharmOrderId, 
			String hospCode, 
			Boolean remarkFlag,
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled,
			Boolean dhEnabled);

	void cancelVetting(
			String orderNum, 
			String hospCode);

	//obsolete after pms-pms verions 3.4.2.4
	EndVettingResult refillDispensing(
			DispOrder dispOrder, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled,
			List<MedOrderItem> confirmDiscontinueMoiList,
			String hospCodeForOwnerShip);

	EndVettingResult refillDispensing(
			DispOrder dispOrder, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled,
			List<MedOrderItem> confirmDiscontinueMoiList,
			String hospCodeForOwnerShip,
			Map<Long, Boolean> largeLayoutFlagMap);
	
	void cancelDispensing(
			Long dispOrderId, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled);
	
	void cancelDispensing(
			Long dispOrderId, 
			Boolean legacyPersistenceEnabled, 
			Boolean fcsPersistenceEnabled,
			String hospCodeForOwnerShip);
	
	void endDispensing(Long dispOrderId, 
			DispInfo dispInfo,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled, 
			Invoice sfiInvoice);
	
	/*start using in pms-pms version 3.0.3.2*/
	void endDispensing(
			Long dispOrderId, 
			DispInfo dispInfo,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled);
	
	void endDispensingForBatchIssue(
			List<DispInfo> dispInfoList, 
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled);
	
	UnvetOrderResult unvetOrder(
			MedOrder medOrder,
			Long prevMedOrderId, 
			Boolean fcsPersistenceEnabled, 
			Boolean legacyPersistenceEnabled,
			Boolean removeOrderByCms,
			Boolean removeOrderByPms);

	//obsolete after pms-pms version 3.4.2.4
	void updateDispOrderAdminStatus(
			Long dispOrderId, 
			DispOrderAdminStatus dispOrderAdminStatus,
			Date issueDate,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled);
	
	void updateDispOrderAdminStatus(
			Long dispOrderId, 
			DispOrderAdminStatus dispOrderAdminStatus,
			Date issueDate,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled,
			Map<Long, Boolean> largeLayoutFlagMap);
	
	void updateDispOrderAdminStatusAndForceProceed(Long dispOrderId, 
			DispOrderAdminStatus dispOrderAdminStatus,
			Date issueDate,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled, 
			Invoice invoice);

	void reversePharmacyRemark(MedOrder medOrder);
	
	//obsolete after pms-pms verions 3.4.2.4
	void updateAdminStatus(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled);

	void updateAdminStatus(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Boolean legacyFcsPersistenceEnabled,
			Map<Long, Boolean> largeLayoutFlagMap);
	
	//obsolete after pms-pms verions 3.4.2.4
	void updateAdminStatusForceProceed(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Invoice invoice, 
			Boolean legacyFcsPersistenceEnabled);

	void updateAdminStatusForceProceed(
			DispOrder clusterDispOrder, 
			DispOrderAdminStatus dispOrderAdminStatus, 
			PharmOrderAdminStatus pharmOrderAdminStatus,
			Boolean legacyPersistenceEnabled,
			Invoice invoice, 
			Boolean legacyFcsPersistenceEnabled,
			Map<Long, Boolean> largeLayoutFlagMap);
	
	void updateAllowCommentType(
			DispOrder clusterDispOrder, 
			AllowCommentType allowCommentType);
	
	void removeOrder(
			DispOrder clusterDispOrder, 
			RemarkEntity remarkEntity,
			Boolean fcsPersistenceEnabled,
			Boolean legacyPersistenceEnabled);
	
	void updateFcsSfiInvoiceForceProceedInfo(
			Invoice sfiInvoice,
			Boolean fcsPersistenceEnabled
	);

	void invalidateSession(Workstore workstore);

	void purgeMsWorkstoreDrug(Workstore workstore);

	// IPMOE
	void sendMedProfileOrder(MpTrxType mpTrxType, List<MedProfileMoItem> medProfileMoItemList);
	
	void sendMedProfileReplenishment(MpTrxType mpTrxType, List<Replenishment> replenishmentList);
	
	MedCase saveMedCase(MedCase remoteMedCase);

	Patient savePatient(Patient remotePatient);
	
	List<WardStock> retrieveWardStockList(Ward ward);
	
	//obsolete after pms-pms 3.2.2
	boolean saveDispOrderListForIp(List<DispOrder> dispOrderList, Boolean legacyPersistenceEnabled);
	
	//start using in pms-pms 3.2.2
	@Marshaller(MarshallerType.JAVA)
	boolean saveDispOrderListWithVoidInvoiceListForIp(List<DispOrder> dispOrderList,
														 Boolean legacyPersistenceEnabled, 
														 Boolean fcsPersistenceEnabled,
														 Boolean directLabelPrintFlag,
														 List<Invoice> tempInvoiceList,
														 List<Invoice> voidInvoiceList);
	
	List<DispOrderItem> retrieveDispOrderItemListByDeliveryItemId(List<Long> deliveryItemIdList);
	
	boolean updateDispOrderItemStatusToDeletedByTpnRequestIdList(List<Long> tpnRequestIdList, Boolean legacyPersistenceEnabled);
	
	boolean updateDispOrderItemStatusToDeleted(List<Long> deliveryItemIdList, Boolean legacyPersistenceEnabled);
	
	// report
	List<DayEndFileExtractRpt> retrieveDayEndFileExtractRptList(String hospCode, String workstoreCode, Date batchDate);

	List<MpWorkloadStat> retrieveMpWorkloadStatRptList(Workstore workstore, Date startDate, Date endDate);
		
	//Property Maintenance
	List<CorporateProp> retrieveCorporatePropList();
	
	void updateCorporatePropList(List<PropInfoItem> corporatePropInfoList);
	
	void updateWorkstoreProp(List<WorkstoreProp> workstorePropList, boolean isOperationModeMaint);
	
	void updateCorporatePropByPropMaint(List<CorporateProp> corporatePropList);
	
	void updateHospitalProp(List<HospitalProp> hospitalPropList);
	
	//uncollect
	void updateUncollectOrderList(List<UncollectOrder> updateUncollectOrderList, Workstore workstore, String uncollectUser);
	
	void updateCapdVoucherStatusToIssue(List<CapdVoucher> capdVoucherList);
	
	List<PmsSessionInfo> retrievePmsSessionInfoList(String requestId, Workstore workstore);
	
	//MpSfiInvoice
	List<Invoice> retrieveInvoiceListWoPurchaseRequest(String hkid, String caseNum, String hospCode);
	
	List<Invoice> retrieveInvoiceByInvoiceNumWoPurchaseRequest(String invoiceNum, String hospCode);
	
	void voidInvoiceByInvoiceNum(List<String> invoiceNumList);
	
	String retrieveChargeOrderNum(String hospCode, String patHospCode);
	
	//Send out
	void updateFcsSfiInvoiceStatus(List<String> invoiceNumList);
	
	//Label Delete
	Map<Long, String> retrieveInvoiceNumByDeliveryItemId(List<Long> deliveryItemIdList);
	
	//MpSfiInvoiceEnq
	List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceNum(String invoiceNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByCaseNum(String caseNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByHkid(String hkid, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	Invoice retrieveInvoiceByInvoiceId(Long invoiceId);
	
	void updateInvoicePaymentStatus(List<Long> invoiceIdList, Map<String, FcsPaymentStatus> fcsPaymentStatusMap);
	
	//Check & Issue Uncollect
	void reverseUncollectAndEndDispensing(DispOrder clusterDispOrder,
											DispInfo dispInfo,
											Boolean legacyPersistenceEnabled,
											Boolean legacyFcsPersistenceEnabled, 
											Invoice sfiInvoice);
	
	List<OneStopOrderItemSummary> retrieveOrderByCaseNum(String caseNum);
	
	List<FmHospitalMapping> retrieveFmHospitalMappingList();
	
	PatientSfiProfile retrievePatientSfiProfile(String hkid);
}
