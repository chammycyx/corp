package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "eai")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChecklistEai implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="TransactionId")
	private String transactionId;
	
	@XmlElement(name="FileList")
	private ChecklistFileList fileList;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public ChecklistFileList getFileList() {
		return fileList;
	}

	public void setFileList(ChecklistFileList fileList) {
		this.fileList = fileList;
	}
}
