package hk.org.ha.model.pms.corp.batch.worker.reftable.moe;

import hk.org.ha.model.pms.persistence.corp.WardAdminFreq;
import hk.org.ha.model.pms.persistence.corp.WardAdminFreqPK;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("wardAdminFreqHelper")
@Scope(ScopeType.APPLICATION)
public class WardAdminFreqHelper {
	
	@SuppressWarnings("unchecked")
	public void updateWardAdminFreqForPms(EntityManager em, List<WardAdminFreq> wardAdminFreqList, List<String> errorPatHospCodeList) {
		List<WardAdminFreq> origList = em.createQuery(" SELECT o FROM WardAdminFreq o ").getResultList();
		Map<WardAdminFreqPK, WardAdminFreq> wardAdminFreqMap = new HashMap<WardAdminFreqPK, WardAdminFreq>();
		for(WardAdminFreq wardAdminFreq : origList){
			WardAdminFreqPK wardAdminFreqPK = new WardAdminFreqPK(wardAdminFreq.getPatHospCode(), wardAdminFreq.getPasWardCode(), wardAdminFreq.getFreqCode());
			wardAdminFreqMap.put(wardAdminFreqPK, wardAdminFreq);			
		}
		for(WardAdminFreq wardAdminFreq : wardAdminFreqList)
		{
			WardAdminFreqPK key = wardAdminFreq.getId();
			WardAdminFreq origWardAdminFreq = wardAdminFreqMap.get(key);
			if(origWardAdminFreq != null)
			{
				origWardAdminFreq.setStatus(wardAdminFreq.getStatus());
			}
			else
			{
				em.persist(wardAdminFreq);
			}
			wardAdminFreqMap.remove(key);
		}
		for(WardAdminFreq wardAdminFreq : wardAdminFreqMap.values())
		{
			if( !errorPatHospCodeList.contains(wardAdminFreq.getPatHospCode()) ){
				wardAdminFreq.setStatus(RecordStatus.Delete);
			}
		}	
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public void updateWardAdminFreqForPms(EntityManager em, List<WardAdminFreq> wardAdminFreqList) {
		List<WardAdminFreq> origList = em.createQuery(" SELECT o FROM WardAdminFreq o ").getResultList();
		Map<WardAdminFreqPK, WardAdminFreq> wardAdminFreqMap = new HashMap<WardAdminFreqPK, WardAdminFreq>();
		for(WardAdminFreq wardAdminFreq : origList){
			WardAdminFreqPK wardAdminFreqPK = new WardAdminFreqPK(wardAdminFreq.getPatHospCode(), wardAdminFreq.getPasWardCode(), wardAdminFreq.getFreqCode());
			wardAdminFreqMap.put(wardAdminFreqPK, wardAdminFreq);			
		}
		Set<String> hospCodeSet = new HashSet<String>();
		for(WardAdminFreq wardAdminFreq : wardAdminFreqList)
		{
			WardAdminFreqPK key = wardAdminFreq.getId();
			WardAdminFreq origWardAdminFreq = wardAdminFreqMap.get(key);
			if(origWardAdminFreq != null)
			{
				origWardAdminFreq.setStatus(wardAdminFreq.getStatus());
			}
			else
			{
				em.persist(wardAdminFreq);
			}
			wardAdminFreqMap.remove(key);
			hospCodeSet.add(wardAdminFreq.getPatHospCode());
		}
		for(WardAdminFreq wardAdminFreq : wardAdminFreqMap.values())
		{
			if ( hospCodeSet.contains(wardAdminFreq.getPatHospCode()) ) {
				wardAdminFreq.setStatus(RecordStatus.Delete);
			}
		}	
		em.flush();
	}
}
