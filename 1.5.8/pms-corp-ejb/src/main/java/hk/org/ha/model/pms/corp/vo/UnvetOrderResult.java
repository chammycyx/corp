package hk.org.ha.model.pms.corp.vo;

import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class UnvetOrderResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private MedOrder medOrder;
	
	private MedOrderStatus medOrderStatus;

	private Map<Long, MedOrderItemStatus> medOrderItemStatusMap;
	
	public UnvetOrderResult() {
		medOrderItemStatusMap = new HashMap<Long, MedOrderItemStatus>();
	}

	public UnvetOrderResult(MedOrder medOrder) {
		this();
		this.medOrder = medOrder; 
		medOrderStatus = medOrder.getStatus();
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
			medOrderItemStatusMap.put(medOrderItem.getId(), medOrderItem.getStatus());
		}
	}

	public MedOrder getMedOrder() {
		return medOrder;
	}

	public void setMedOrder(MedOrder medOrder) {
		this.medOrder = medOrder;
	}

	public MedOrderStatus getMedOrderStatus() {
		return medOrderStatus;
	}

	public void setMedOrderStatus(MedOrderStatus medOrderStatus) {
		this.medOrderStatus = medOrderStatus;
	}

	public Map<Long, MedOrderItemStatus> getMedOrderItemStatusMap() {
		return medOrderItemStatusMap;
	}

	public void setMedOrderItemStatusMap(
			Map<Long, MedOrderItemStatus> medOrderItemStatusMap) {
		this.medOrderItemStatusMap = medOrderItemStatusMap;
	}
}
