package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.phs.DpTransaction;
import hk.org.ha.model.pms.corp.persistence.phs.PhsStoreMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;


/** 
 * Session Bean implementation class PhsTrxBean
 */
@AutoCreate
@Stateless
@Name("phsTrx")
@MeasureCalls
public class PhsTrxBean implements PhsTrxLocal {

	private static final int ITEM_NUM_LENGTH = 3;
	
	@PersistenceContext(unitName="PMSCMS1_DM")
	private EntityManager em;
	
	@In
	private ExportDispTrxLocal exportDispTrx;
	
	public void processNonCapdItem(
			String hospCode, 
    		String workstoreCode,
    		Date batchDate,
    		List<DispOrderItem> dispOrderItemList,
    		List<String> exceptItemList) throws Exception {
		
		// convert institution code from workstore 
		Workstore tempWorkstore = new Workstore(hospCode, workstoreCode);
		PhsStoreMapping phsStoreMapping = retrieveInstitutionCodeByWorkstore(tempWorkstore);
		if (phsStoreMapping == null) {
			throw new UnsupportedOperationException("Instiution not found in PHS DB phs_store_mapping table, hospCode = " + hospCode + ", workstoreCode = " + workstoreCode);
		}

		
        for (DispOrderItem dispOrderItem: dispOrderItemList) {
        	DispOrder dispOrder = dispOrderItem.getDispOrder();
        	PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
        	PharmOrder pharmOrder = dispOrder.getPharmOrder();
        	
        	if (exportDispTrx.validTransaction(dispOrderItem, exceptItemList)) {
        		
            	Workstore workstore = dispOrder.getWorkstore();	
            	MedOrder medOrder = pharmOrder.getMedOrder();
            	MedCase medCase = pharmOrder.getMedCase();
            	Patient patient = pharmOrder.getPatient();

        		
            	DpTransaction dpTransaction = new DpTransaction();
            	
            	dpTransaction.setInstitutionCode(phsStoreMapping.getInstitutionCode());
            	dpTransaction.setLocationCode(phsStoreMapping.getLocationCode()); 
            	
            	dpTransaction.setPmsHospCode(workstore.getHospCode());
            	dpTransaction.setPmsWorkStore(workstore.getWorkstoreCode());
            	
                dpTransaction.setDispenseDate(dispOrder.getDispDate()); 
				dpTransaction.setDispenseTicketNum(exportDispTrx.getTicketNum(dispOrder.getTicket()));
				dpTransaction.setDispenseOrderNum(dispOrder.getDispOrderNum()); 
				dpTransaction.setDispenseLineNum(pharmOrderItem.getItemNum());
				dpTransaction.setItemCode(pharmOrderItem.getItemCode()); 
				dpTransaction.setDispenseQty(dispOrderItem.getDispQty().intValue());
				dpTransaction.setPatType(medOrder.getOrderType().getDataValue()); 
				dpTransaction.setDispenseStatus("I");
				dpTransaction.setWardCode(dispOrder.getWardCode());
				dpTransaction.setSpecialtyCode(dispOrderItem.getChargeSpecCode());
				dpTransaction.setDurationInDays(dispOrderItem.getDurationInDay());
				dpTransaction.setPatCatCode(dispOrder.getPatCatCode());
				dpTransaction.setPatName( StringUtils.left(StringUtils.upperCase(patient.getName()), 25) ); 
                // space is NOT required at the beginning of HKID, because the HKID is NOT sync to ERP
				dpTransaction.setPatId(patient.getHkid());
				dpTransaction.setCaseNum(exportDispTrx.getPhsFormatCaseNum(medCase));
				dpTransaction.setPassportNum(patient.getOtherDoc()); 
				dpTransaction.setMoCode(pharmOrder.getDoctorCode());
				dpTransaction.setTransactionDate(batchDate);
				dpTransaction.setUnitPrice(null); // unitPrice is set by PHS ERP job
				dpTransaction.setSpecialtyPercentage(100); 
				
				dpTransaction.setRecordStatus("P");
				
				dpTransaction.setInfileBatchNum(null);
				
            	em.persist(dpTransaction);
        	}
		}
	}
	
    public void processCapdItem(
    		String hospCode, 
    		String workstoreCode,
    		List<DispOrderItem> dispOrderItemList,
    		List<String> exceptItemList) throws Exception {

        for (DispOrderItem dispOrderItem: dispOrderItemList) {
        	PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
        	DispOrder dispOrder = dispOrderItem.getDispOrder();
        	PharmOrder pharmOrder = dispOrder.getPharmOrder();
        	MedOrder medOrder = pharmOrder.getMedOrder();
        	
        	if (validCapdTransaction(dispOrderItem, exceptItemList)) {
        		
            	Workstore workstore = dispOrder.getWorkstore();
            	MedCase medCase = pharmOrder.getMedCase();
            	Patient patient = pharmOrder.getPatient();

            	List<CapdVoucherItem> capdVoucherItemList = dispOrderItem.getCapdVoucherItemList();
            	
            	for(CapdVoucherItem capdVoucherItem : capdVoucherItemList) {
	            	
            		CapdVoucher capdVoucher = capdVoucherItem.getCapdVoucher();
	            	
            		if (capdVoucher.getStatus() == CapdVoucherStatus.Issued) {
    	        		PhsStoreMapping phsStoreMapping = retrieveInstitutionCodeByWorkstore(workstore);
    	        		if (phsStoreMapping == null) {
    	        			throw new UnsupportedOperationException("Instiution not found in PHS DB phs_store_mapping table, hospCode = " + hospCode + ", workstoreCode = " + workstoreCode);
    	        		}

    	        		
    	        		// note: before PHS migration, record is inserted to CapdTransaction table first for duplication check, and then copy 
    	        		//       to CapdVoucher by sp in import CAPD trx job
    	        		//       after PHS migration, it is not possible to insert duplicate CAPD trx, so record is directly inserted to CapdVoucher 
    	        		hk.org.ha.model.pms.corp.persistence.phs.CapdVoucher capdVoucherPhs = new hk.org.ha.model.pms.corp.persistence.phs.CapdVoucher();

                        capdVoucherPhs.setInstitutionCode(phsStoreMapping.getInstitutionCode());
                        capdVoucherPhs.setStoreCode(phsStoreMapping.getLocationCode());

                        capdVoucherPhs.setVoucherNum(capdVoucher.getVoucherNum());
                        capdVoucherPhs.setVoucherLineNum(pharmOrderItem.getItemNum());
                        // note: space is required at the beginning of manual voucher number
                        if (capdVoucher.getManualVoucherNum() != null) {
                        	capdVoucherPhs.setVoucherManualNum(String.format("%11.11s", capdVoucher.getManualVoucherNum()));
                        }
                        capdVoucherPhs.setVoucherLineStatus("N");
                        capdVoucherPhs.setVoucherDate(dispOrder.getDispDate());
                        
                        capdVoucherPhs.setSupplierCode(capdVoucher.getSupplierCode());
                        capdVoucherPhs.setItemCode(pharmOrderItem.getItemCode());
                        capdVoucherPhs.setRequestQty(capdVoucherItem.getQty());
                        capdVoucherPhs.setPatClass("C");
                        capdVoucherPhs.setPatType(medOrder.getOrderType().getDataValue());
                        capdVoucherPhs.setCategory(pharmOrder.getPatCatCode());
                        capdVoucherPhs.setFirstGenDate(null);
                        
                        // note: space is required at the beginning of HKID, because the HKID is sync to ERP
                        capdVoucherPhs.setPatId(exportDispTrx.getPhsFormatHkid(patient.getHkid()));	 
                        capdVoucherPhs.setCaseNum(exportDispTrx.getPhsFormatCaseNum(medCase)); 
                        capdVoucherPhs.setPatName(patient.getName());

                        capdVoucherPhs.setDispenseOrderNum(dispOrder.getDispOrderNum());
                        capdVoucherPhs.setDispenseLineNum(pharmOrderItem.getItemNum());
                        capdVoucherPhs.setDispenseTicketNum(exportDispTrx.getTicketNum(dispOrder.getTicket()));
                        
                        capdVoucherPhs.setRecordStatus("A");
                        
                        em.persist(capdVoucherPhs);
            		}
            	}
        	}
        }
	}
	
	private Boolean validCapdTransaction(DispOrderItem dispOrderItem, List<String> exceptItemList) throws Exception {
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		Workstore workstore = dispOrder.getWorkstore();
		
		if (!pharmOrderItem.getCapdVoucherFlag() ||
			dispOrder.getStatus() != DispOrderStatus.Issued || 
			medOrder.getOrderType() != OrderType.OutPatient) {
			
			return Boolean.FALSE;
		}
		
		if (exceptItemList.contains(pharmOrderItem.getItemCode())) {
			return Boolean.FALSE;
		}
		
		// check item num length
		if (pharmOrderItem.getItemNum().toString().length() > ITEM_NUM_LENGTH) {
			throw new Exception("PhsTrxBean: itemNum in CAPD pharmOrderItem is over 3 digits: " +
					"dispHospCode="+ workstore.getHospCode() + ", " +
					"dispOrderNum=" + dispOrder.getDispOrderNum() + ", " +
					"itemNum=" + pharmOrderItem.getItemNum() + ", " +
					"dispOrderItemId=" + dispOrderItem.getId());
		}
		
		return Boolean.TRUE;
	}
	
	@SuppressWarnings("unchecked")
	private PhsStoreMapping retrieveInstitutionCodeByWorkstore(Workstore workstore) {
		List<PhsStoreMapping> phsStoreMappingList = em.createQuery(
				"select o from PhsStoreMapping o" +  // 20170418 index check : none
				" where o.pmsHospCode = :pmsHospCode" + 
				" and o.pmsWorkStore = :pmsWorkStore" + 
				" and o.suspend = 'N'") 
				.setParameter("pmsHospCode", workstore.getHospCode())
				.setParameter("pmsWorkStore", workstore.getWorkstoreCode())
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		if (phsStoreMappingList.isEmpty()) {
			return null;
		} else {
			// only 1 record for each workstore
			return phsStoreMappingList.get(0);
		}
	}
	
	private String convertToString(Integer integer) {
		if (integer == null) {
			return StringUtils.EMPTY;
		}
		
		return integer.toString();
	}
	
    private static String rtrim(String value) {
    	return StringUtils.stripEnd(value, null);
    }
}
