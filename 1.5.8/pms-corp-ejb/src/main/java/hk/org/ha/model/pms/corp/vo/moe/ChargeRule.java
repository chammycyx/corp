package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;

public class ChargeRule implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String dispHospCode;

	private String dispWorkstore;
	
	private Boolean chargeSplitInd;

	private Integer chargeDaysPerUnit;
	
    public ChargeRule() {
    }

	public String getDispHospCode() {
		return dispHospCode;
	}

	public void setDispHospCode(String dispHospCode) {
		this.dispHospCode = dispHospCode;
	}

	public String getDispWorkstore() {
		return dispWorkstore;
	}

	public void setDispWorkstore(String dispWorkstore) {
		this.dispWorkstore = dispWorkstore;
	}

	public Boolean getChargeSplitInd() {
		return chargeSplitInd;
	}

	public void setChargeSplitInd(Boolean chargeSplitInd) {
		this.chargeSplitInd = chargeSplitInd;
	}

	public Integer getChargeDaysPerUnit() {
		return chargeDaysPerUnit;
	}

	public void setChargeDaysPerUnit(Integer chargeDaysPerUnit) {
		this.chargeDaysPerUnit = chargeDaysPerUnit;
	}

}