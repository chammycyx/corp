package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;

import java.io.BufferedWriter;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Session Bean implementation class ExportUncollectTrxBean
 */
@AutoCreate
@Stateless
@Name("exportUncollectTrx")
@MeasureCalls
public class ExportUncollectTrxBean implements ExportUncollectTrxLocal {
	
	private final static char SPACE = ' ';
	
    /**
     * Default constructor. 
     */
    public ExportUncollectTrxBean() {
        // TODO Auto-generated constructor stub
    }
	
    private String getPhsFormatHkid(String hkid) {
		if (hkid == null) {
			return StringUtils.EMPTY;
		}
		
		if (hkid.length() == 8) {
			StringBuilder formatHkid = new StringBuilder();
			formatHkid.append(SPACE);
			formatHkid.append(hkid);
			return formatHkid.toString();
		} else {
			return hkid;
		}
	}
	
    private String getPhsFormatCaseNum(MedCase medCase){
		if (medCase == null || medCase.getCaseNum() == null) {
			return StringUtils.EMPTY;
		}
		
		if (medCase.getCaseNum().length() == 11) {
			StringBuilder formatCaseNum = new StringBuilder();
			formatCaseNum.append(SPACE);
			formatCaseNum.append(medCase.getCaseNum());
			return formatCaseNum.toString();
		} else {
			return medCase.getCaseNum();
		}
	}
    
	private String getPasApptSeq(MedCase medCase) {
		if (medCase == null || medCase.getPasApptSeq() == null) {
			return StringUtils.EMPTY;
		}
		
		return StringUtils.defaultString(medCase.getPasApptSeq().toString());
	}
	
	private String getPhsOrderNumFormat(String orderNum) {
		if (orderNum != null) {
			return orderNum.substring(3);
		}
		return StringUtils.EMPTY;
	}
	
	public void createUncollectCount(
			int uncollectCount,
			BufferedWriter uncollectCountWriter) throws Exception {
		
		uncollectCountWriter.write(String.valueOf(uncollectCount));
		uncollectCountWriter.newLine();
	}
	
	public int process(
			List<DispOrder> dispOrderList, 
			BufferedWriter outWriter) throws Exception {
		
		int recCount = 0;
		
		for(DispOrder dispOrder: dispOrderList) {
			PharmOrder pharmOrder = dispOrder.getPharmOrder();
			MedOrder medOrder = pharmOrder.getMedOrder();
			MedCase medCase = pharmOrder.getMedCase();
			Workstore workstore = dispOrder.getWorkstore();
			Patient patient = pharmOrder.getPatient();
			
			StringBuilder transactionLine = new StringBuilder();
	        transactionLine.append(String.format("%-3.3s", StringUtils.defaultString(medOrder.getPatHospCode())))
	        				   .append(String.format("%-3.3s", StringUtils.defaultString(workstore.getHospCode())))
	        				   .append(String.format("%-12.12s", getPhsFormatHkid(patient.getHkid())))
	        				   .append(String.format("%-12.12s", getPhsFormatCaseNum(medCase)))
	        				   .append(String.format("%9.9s", getPasApptSeq(medCase)))
	        				   .append(String.format("%-20.20s", StringUtils.defaultString(pharmOrder.getReceiptNum())))
	        				   .append(String.format("%9.9s", getPhsOrderNumFormat(medOrder.getOrderNum())))
	        				   .append(String.format("%09d", dispOrder.getDispOrderNum()))
	        				   .append(DateTimeFormat.forPattern("yyyyMMdd").print(new DateTime(dispOrder.getDispDate())));
	        				  
	        outWriter.write(transactionLine.toString());
	        outWriter.newLine();
	        
	        recCount++;
		}
		
		return recCount;
	}
}
