package hk.org.ha.model.pms.corp.persistence.report;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class DayEndRptStatPK {
	
	private String hospCode;
	private String workstoreCode;
	private Date batchDate;
	
	public DayEndRptStatPK(){
	}
	
	public DayEndRptStatPK(String hospCode, String workstoreCode, Date batchDate){
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.batchDate = batchDate;
	}
	
	public String getHospCode() {
		return hospCode;
	}
	
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	
	public String getWorkstoreCode() {
		return workstoreCode;
	}
	
	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
