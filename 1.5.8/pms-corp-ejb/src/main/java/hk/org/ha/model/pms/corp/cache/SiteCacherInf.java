package hk.org.ha.model.pms.corp.cache;
import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.vo.Site;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SiteCacherInf extends BaseCacherInf {
	List<Site> getSiteList();
}
