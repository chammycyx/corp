package hk.org.ha.model.pms.corp.batch.worker.order;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.vo.dh.EaiSftpMessage;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.corp.vo.dh.ChecklistEai;
import hk.org.ha.model.pms.corp.vo.dh.ChecklistFile;
import hk.org.ha.model.pms.corp.vo.dh.ChecklistFileList;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.service.pms.asa.interfaces.AsaServiceJmsRemote;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

@Stateful
@Scope(ScopeType.SESSION)
@Name("upldDhFileBean")
@MeasureCalls
public class UpldDhFileBean implements UpldDhFileLocal {

	private static final String JAXB_CONTEXT = "hk.org.ha.model.pms.corp.vo.dh";

	private static final String SLASH = "/";

	private static final String DOT = ".";

	private static final String UNDERSCORE = "_";
	
	private static final String MONTH_DIR_NAME = "month";
	
	private static final String DAY_DIR_NAME = "day";
	
	private static final String TMP_EXTENSION = ".tmp";
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	private Logger logger = null;

	private Date batchDate;

	private String year = null;
	private String month = null;
	private String day = null;
	
	private String checklistFileName = null;
	
	private String ftpTargetPath = null;
	
	@In
	private ApplicationProp applicationProp;
	
	@In
	private AsaServiceJmsRemote asaServiceProxy;
	
	public void beginChunk(Chunk chunk) throws Exception {
	}

	public void endChunk() throws Exception {
	}

	@Remove
	public void destroyBatch() {
	}

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        }
		
    	batchDate = params.getDate("batchDate");
		year = DateTimeFormat.forPattern("yyyy").print(new DateTime(batchDate));
		month = DateTimeFormat.forPattern("MM").print(new DateTime(batchDate));
		day = DateTimeFormat.forPattern("dd").print(new DateTime(batchDate));
		
		checklistFileName = applicationProp.getDhDispChecklistFile() + UNDERSCORE + dateFormat.format(batchDate) + DOT + "xml";
		
		ftpTargetPath = applicationProp.getSftpPmsRootDir() + SLASH + applicationProp.getDhDispDataSubDir() + SLASH + DAY_DIR_NAME + SLASH + year + SLASH + month + SLASH + day; 
	}

	public void processRecord(Record record) throws Exception {
		Date startDatetime = new Date();
		logger.info("process record start... @" + startDatetime);

		File outDir = getOutDoneDirectory(applicationProp.getDhDispTrxDir(), DataDirType.Out, null, null, true);
		
		DateTimeFormatter sdt = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		File errorDir = getDirectory(null, applicationProp.getDhDispTrxDir(), DataDirType.Error);
		File errorChecklistFile = new File(errorDir + SLASH + checklistFileName + DOT + sdt.print(new DateTime(new Date())) );
		
		List<File> outDispTrxFileList = retrieveDhDispTrxFileList();
		
		File workChecklistFile = null;
		
		try {
			workChecklistFile = constructWorkChecklistFile(outDispTrxFileList);

			uploadFile(outDispTrxFileList, workChecklistFile);
			
			notifyEai();
			
		} catch (Exception e) {
			if (workChecklistFile != null && workChecklistFile.exists()) {
				FileUtils.moveFile(workChecklistFile, errorChecklistFile);
			}
			
			throw e;
		}
		
		FileUtils.moveFileToDirectory(workChecklistFile, outDir, false);
		
		Date endDatetime = new Date();
        logger.info("process record complete..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	private void uploadFile(List<File> outDispTrxFileList, File workChecklistFile) throws Exception {
		ChannelSftp channelSftp = getChannelSftp();
		
		try {
			channelSftp.cd(ftpTargetPath);
			if (logger.isDebugEnabled()) {
				logger.debug("dhDispTrxBean - processRecord: changed path to " + ftpTargetPath);
			}
		} catch (SftpException e) {
			createRemoteDir(ftpTargetPath, channelSftp);
			channelSftp.cd(ftpTargetPath);
			logger.info("dhDispTrxBean - processRecord: The following directory created: " + ftpTargetPath);
			if (logger.isDebugEnabled()) {
				logger.debug("dhDispTrxBean - processRecord: changed path to " + ftpTargetPath);
			}
		}
		
		// upload checklist file
		ftpFile(workChecklistFile, workChecklistFile.getName(), channelSftp);
		logger.info(" DH checklist file SFTP source : " + workChecklistFile.getPath());
		logger.info(" DH checklist file SFTP destination : " + workChecklistFile.getName());
		
		// upload dispensed trx file
		for (File outDispTrxFile : outDispTrxFileList) {
			ftpFile(outDispTrxFile, outDispTrxFile.getName(), channelSftp);
			logger.info("DH trx file SFTP source : " + outDispTrxFile.getPath());
			logger.info("DH trx file SFTP destination : " + outDispTrxFile.getName());
		}
	}

	@SuppressWarnings("unchecked")
	private List<File> retrieveDhDispTrxFileList() throws Exception {
		List<String> hospCodeList = em.createQuery(
				"select o.hospCode from Hospital o" + // 20170418 index check : none
				" where o.status = :status")
				.setParameter("status", RecordStatus.Active)
				.getResultList();
		
		List<File> outFileList = new ArrayList<File>();
		
		for (String hospCode : hospCodeList) {
			File outDir = getOutDoneDirectory(applicationProp.getDhDispTrxDir(), DataDirType.Out, null, hospCode, true);
			File outFile = getFile(outDir, applicationProp.getDhDispTrxFile(), batchDate, hospCode, "xml");

			if ( ! outFile.exists()) {
				throw new UnsupportedOperationException("Disp trx file of date [" + batchDate + "] not found in OUT folder for hospCode = [" + hospCode + "]");
			}
			
			outFileList.add(outFile);
		}
		
		return outFileList;
	}

	private File constructWorkChecklistFile(List<File> dispTrxFileList) throws Exception {
		File workDir = getDirectory(null, applicationProp.getDhDispTrxDir(), DataDirType.Work);
		File workFile = new File(workDir + SLASH + checklistFileName);
		
		File outDir = getOutDoneDirectory(applicationProp.getDhDispTrxDir(), DataDirType.Out, null, null, true);
		File outFile = new File(outDir + SLASH + checklistFileName);

		if (outFile.exists()) {
			if ( ! outFile.delete()) {
				throw new UnsupportedOperationException("Old disp checklist file " + outFile.getAbsolutePath() + " cannot be removed");
			}
			logger.info("Old disp checklist file " + outFile.getAbsolutePath() + " exists and is removed to allow re-run");
		}
		
		List<ChecklistFile> checkListFileList = new ArrayList<ChecklistFile>();

		for (File dispTrxFile : dispTrxFileList) {
			ChecklistFile checklistFile = new ChecklistFile();
			checklistFile.setFileName(dispTrxFile.getName());
			checklistFile.setChecksum(getHexString(dispTrxFile));
			checkListFileList.add(checklistFile);
		}
		
		ChecklistEai eai = new ChecklistEai();
		eai.setTransactionId(dateFormat.format(batchDate));
		eai.setFileList(new ChecklistFileList(checkListFileList));
		
		FileUtils.writeStringToFile(workFile, JaxbWrapper.instance(JAXB_CONTEXT).marshall(eai));

		return workFile;
	}
	
	private String getHexString(File file) throws Exception {
		FileInputStream fileInputStream = new FileInputStream(file);
		String hexString = DigestUtils.sha256Hex(fileInputStream);
		fileInputStream.close();
		return hexString;
	}
	
	private ChannelSftp getChannelSftp() {
		Context context = Contexts.getEventContext();
		context.set("sftp_pms_host", applicationProp.getSftpPmsHost());
		context.set("sftp_pms_username", applicationProp.getSftpPmsUsername());
		context.set("sftp_pms_password", applicationProp.getSftpPmsPassword());
		context.set("sftp_pms_rootDir", applicationProp.getSftpPmsRootDir() + SLASH + applicationProp.getDhDispDataSubDir());
		return (ChannelSftp) Component.getInstance("pmsChannelSftp");
	}

	private void createRemoteDir(String directory, ChannelSftp c) throws IOException, SftpException {
        synchronized (DhDispTrxBean.class) {
			try {
	            SftpATTRS att = c.stat(directory);
	            if (att != null) {
	                if (att.isDir()) {
	                    return;
	                }
	            }
	        } catch (SftpException ex) {
	            if (directory.indexOf(SLASH) != -1) {
	                createRemoteDir(directory.substring(0, directory.lastIndexOf(SLASH)), c);
	            }
	            c.mkdir(directory);
	        }
        }
    }

	private void ftpFile(File sourceFile, String targetFileName, ChannelSftp sftp) throws SftpException {
		sftp.put(sourceFile.getPath(), targetFileName + TMP_EXTENSION);
		renameFtpTmpFileToNormal(targetFileName, sftp);
	}
	
	private void renameFtpTmpFileToNormal(String fileName, ChannelSftp sftp) throws SftpException {
		try {
			sftp.rename(fileName + TMP_EXTENSION, fileName);
		} catch (SftpException e) {
			sftp.rm(fileName);
			sftp.rename(fileName + TMP_EXTENSION, fileName);
		}
	}
	
	private void notifyEai() {
		EaiSftpMessage message = new EaiSftpMessage();
		message.setTransactionId(dateFormat.format(batchDate));
		message.setFilePath(ftpTargetPath);
		message.setCheckListFileName(checklistFileName);
		message.setZipFileName(dateFormat.format(batchDate));
		
		asaServiceProxy.submitEaiSftpJob(message);
	}

	private File getDirectory(
			String hospCode,
			String jobId,
			DataDirType dataDirType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + SLASH + 
				jobId + SLASH + 
				dataDirType.getDataValue()
				+ (hospCode == null ? "" : SLASH + hospCode));
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}

	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
			logger.debug("DhDispTrxBean - createDirectory: create path status: " + created);
		}

		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("DhDispTrxBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("DhDispTrxBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("DhDispTrxBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}
	
	private File getOutDoneDirectory(
			String jobId,
			DataDirType dataDirType,
			String systemType,
			String hospCode,
			boolean createDir) throws Exception {
		
		StringBuilder basicPath = new StringBuilder();
		basicPath.append(applicationProp.getBatchDataDir())
						.append(SLASH).append(jobId)
						.append(SLASH).append(dataDirType.getDataValue());
		if (systemType != null) {
			basicPath.append(SLASH).append(systemType);
		}
		basicPath.append(SLASH).append(MONTH_DIR_NAME);
		
		return getOutDoneDirectory(basicPath.toString(), hospCode, createDir);
	}
	
	private File getOutDoneDirectory(
			String basicPath,
			String hospCode,
			boolean createDir) throws Exception {
		
		File basicDir = new File(basicPath);
		if ( ! basicDir.exists()) {
			throw new Exception("DhDispTrxBean: date directory could not be created as parent directory " + basicDir.getParent() + " not exists.");
		}

		StringBuilder destPath = new StringBuilder();
		destPath.append(basicPath)
						.append(SLASH).append(year)
						.append(SLASH).append(month);

		if (hospCode != null) {
			destPath.append(SLASH).append(hospCode);
		}
		
		File destDir = new File(destPath.toString());
		if ( ! destDir.exists()) {
			if (createDir) {
				destDir.mkdirs();
				if ( ! destDir.exists()) {
					throw new Exception("DhDispTrxBean: directory " + destDir.getPath() + " could not be found and created. Please check (eg. Access Right...).");
				}
			} else {
				throw new Exception("DhDispTrxBean: directory " + destDir.getPath() + " could not be found. Please check (eg. Access Right...).");
			}
		}
		
		return destDir;
	}
	
	private File getFile(File dir, String fileName, Date batchDate, String hospCode, String fileExtension) {

		DateTimeFormatter sdf = DateTimeFormat.forPattern("yyyyMMdd");
		
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(sdf.print(new DateTime(batchDate)));
		}
		
		if (hospCode != null) {
			finalFileName.append(DOT)
			 .append(hospCode);
		}
		
		if (fileExtension != null) {
			finalFileName.append(DOT)
						 .append(fileExtension);
		}
		
		File file = new File(
				    dir + SLASH + 
					finalFileName );
		
		return file;
	}
}
