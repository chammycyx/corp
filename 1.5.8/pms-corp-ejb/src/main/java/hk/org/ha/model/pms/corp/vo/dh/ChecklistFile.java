package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ChecklistFile implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="FileName")
	private String fileName;
	
	@XmlElement(name="Checksum")
	private String checksum;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
}
