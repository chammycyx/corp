package hk.org.ha.model.pms.corp.batch.worker.reftable.moe;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ActiveMpHospitalManagerLocal {
	List<String> retrieveActiveMpPatHospCodeList();
}
