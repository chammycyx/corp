package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.service.biz.pms.corp.interfaces.CorpPmsServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface CorpPmsServiceLocal extends CorpPmsServiceJmsRemote {
}
