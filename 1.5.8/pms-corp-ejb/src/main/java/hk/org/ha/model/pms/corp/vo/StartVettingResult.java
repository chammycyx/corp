package hk.org.ha.model.pms.corp.vo;

import hk.org.ha.model.pms.corp.persistence.charging.PatientSfiProfile;
import hk.org.ha.model.pms.persistence.disp.MedOrder;

import java.io.Serializable;

public class StartVettingResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private MedOrder medOrder;
	
	private Boolean successFlag;
	
	private Boolean updateFlag;
	
	private String messageCode;
	
	private String[] messageParam;
	
	private PatientSfiProfile patientSfiProfile;
	
	public StartVettingResult() {
		successFlag = Boolean.TRUE;
		updateFlag  = Boolean.FALSE;
		messageParam = new String[]{};
	}
	
	public MedOrder getMedOrder() {
		return medOrder;
	}

	public void setMedOrder(MedOrder medOrder) {
		this.medOrder = medOrder;
	}

	public Boolean getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}

	public Boolean getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(Boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String[] getMessageParam() {
		return messageParam;
	}

	public void setMessageParam(String[] messageParam) {
		this.messageParam = messageParam;
	}

	public PatientSfiProfile getPatientSfiProfile() {
		return patientSfiProfile;
	}

	public void setPatientSfiProfile(PatientSfiProfile patientSfiProfile) {
		this.patientSfiProfile = patientSfiProfile;
	}
}
