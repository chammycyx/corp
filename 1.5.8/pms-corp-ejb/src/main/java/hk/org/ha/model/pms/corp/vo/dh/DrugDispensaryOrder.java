package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DrugDispensaryOrder implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String prscrbOrderNum;
	
	@XmlElement
	private String verNum;
	
	@XmlElement
	private Long pmiNum;
	
	@XmlElement
	private String sex;
	
	@XmlElement
	private Date dob;
	
	@XmlElement
	private Date dispensedDate;
	
	@XmlElement(name="DrugDispensaryDetail")
	private List<DrugDispensaryDetail> drugDispensaryDetailList;

	public String getPrscrbOrderNum() {
		return prscrbOrderNum;
	}

	public void setPrscrbOrderNum(String prscrbOrderNum) {
		this.prscrbOrderNum = prscrbOrderNum;
	}

	public String getVerNum() {
		return verNum;
	}

	public void setVerNum(String verNum) {
		this.verNum = verNum;
	}

	public Long getPmiNum() {
		return pmiNum;
	}

	public void setPmiNum(Long pmiNum) {
		this.pmiNum = pmiNum;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getDispensedDate() {
		return dispensedDate;
	}

	public void setDispensedDate(Date dispensedDate) {
		this.dispensedDate = dispensedDate;
	}

	public List<DrugDispensaryDetail> getDrugDispensaryDetailList() {
		return drugDispensaryDetailList;
	}

	public void setDrugDispensaryDetailList(List<DrugDispensaryDetail> drugDispensaryDetailList) {
		this.drugDispensaryDetailList = drugDispensaryDetailList;
	}
}
