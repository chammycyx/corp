package hk.org.ha.model.pms.corp.biz.charging;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface InvoiceManagerLocal {
	
	Invoice retrieveInvoiceByInvoiceId(Long invoiceId);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceNum(String invoiceNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByCaseNum(String caseNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByHkid(String hkid, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	List<InvoiceItem> retrieveSfiInvoiceItemListByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType);
	
	void updateInvoicePaymentStatus(List<Long> invoiceIdList, Map<String, FcsPaymentStatus> fcsPaymentStatusMap);
	
	List<Invoice> retrieveInvoiceByInvoiceNumWoPurchaseRequest(String invoiceNum, String hospCode);
	
	List<Invoice> retrieveInvoiceListWoPurchaseRequest(String hkid, String caseNum, String hospCode);
	
	Map<Long, String> retrieveInvoiceNumByDeliveryItemId(List<Long> deliveryItemIdList);
}
