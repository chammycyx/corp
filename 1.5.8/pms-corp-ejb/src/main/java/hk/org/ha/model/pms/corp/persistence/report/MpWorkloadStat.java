package hk.org.ha.model.pms.corp.persistence.report;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.annotations.Customizer;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;

@Entity
@Table(name = "MP_WORKLOAD_STAT")
@Customizer(AuditCustomizer.class)
public class MpWorkloadStat extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mpWorkloadStatSeq")
	@SequenceGenerator(name = "mpWorkloadStatSeq", sequenceName = "SQ_MP_WORKLOAD_STAT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DAY_END_BATCH_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dayEndBatchDate;
	
	@Column(name = "ITEM_COUNT", nullable = false)
	private Integer itemCount;
	
	@Column(name = "SFI_ITEM_COUNT", nullable = false)
	private Integer sfiItemCount;

	@Column(name = "SAFETY_NET_ITEM_COUNT", nullable = false)
	private Integer safetyNetItemCount;

	@Column(name = "WARD_STOCK_ITEM_COUNT", nullable = false)
	private Integer wardStockItemCount;
	
	@Column(name = "DANGER_DRUG_ITEM_COUNT", nullable = false)
	private Integer dangerDrugItemCount;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDayEndBatchDate() {
		if (dayEndBatchDate == null) {
			return null;
		}
		else {
			return new Date(dayEndBatchDate.getTime());
		}
	}

	public void setDayEndBatchDate(Date dayEndBatchDate) {
		if (dayEndBatchDate == null) {
			this.dayEndBatchDate = null;
		}
		else {
			this.dayEndBatchDate = new Date(dayEndBatchDate.getTime());
		}
	}

	public Integer getItemCount() {
		return itemCount;
	}

	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}

	public Integer getSfiItemCount() {
		return sfiItemCount;
	}

	public void setSfiItemCount(Integer sfiItemCount) {
		this.sfiItemCount = sfiItemCount;
	}

	public Integer getSafetyNetItemCount() {
		return safetyNetItemCount;
	}

	public void setSafetyNetItemCount(Integer safetyNetItemCount) {
		this.safetyNetItemCount = safetyNetItemCount;
	}

	public Integer getWardStockItemCount() {
		return wardStockItemCount;
	}

	public void setWardStockItemCount(Integer wardStockItemCount) {
		this.wardStockItemCount = wardStockItemCount;
	}

	public Integer getDangerDrugItemCount() {
		return dangerDrugItemCount;
	}

	public void setDangerDrugItemCount(Integer dangerDrugItemCount) {
		this.dangerDrugItemCount = dangerDrugItemCount;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

}
