package hk.org.ha.model.pms.corp.biz.order;

import static hk.org.ha.model.pms.corp.prop.Prop.ALERT_HLA_DHORDER_OVERRIDE_REASON;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.InitialOrderNum;
import hk.org.ha.model.pms.corp.persistence.order.OrderSubscribe;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.HospitalCluster;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.model.pms.udt.alert.mds.AlertType;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.disp.RemarkStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.FmStatus;
import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
import hk.org.ha.model.pms.vo.chemo.ChemoItem;
import hk.org.ha.model.pms.vo.rx.CapdItem;
import hk.org.ha.model.pms.vo.rx.Dose;
import hk.org.ha.model.pms.vo.rx.DoseGroup;
import hk.org.ha.model.pms.vo.rx.IvAdditive;
import hk.org.ha.model.pms.vo.rx.IvRxDrug;
import hk.org.ha.model.pms.vo.rx.Regimen;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.service.biz.pms.asp.interfaces.AspServiceJmsRemote;
import hk.org.ha.service.biz.pms.cmm.interfaces.CmmServiceJmsRemote;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;
import hk.org.ha.service.biz.pms.interfaces.PmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("corpOrderService")
@MeasureCalls
public class CorpOrderServiceBean implements CorpOrderServiceLocal {

	private static final String JAXB_CONTEXT_MO = "hk.org.ha.model.pms.persistence.disp";

	private static final String JAXB_CONTEXT_MP_MO = "hk.org.ha.model.pms.persistence.medprofile";
	
	@Logger
	private Log logger;

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@In
	private OrderManagerLocal orderManager;
	
	@In
	private OwnershipServiceLocal ownershipService;
	
	@In
	private OrderSubscribeManagerLocal orderSubscribeManager;
	
	@In
    private PmsServiceJmsRemote pmsServiceDispatcher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private ChemoOrderHelper chemoOrderHelper;
	
	@In
	private ApplicationProp applicationProp;
	
	private final static String UNCHECKED = "unchecked";

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";
	
	private CmmServiceJmsRemote getCmmServiceProxy() {
		return (CmmServiceJmsRemote) Component.getInstance("cmmServiceProxy", false);
	}	
	
	public void receiveMedOrder(OpTrxType opTrxType, MedOrder remoteMedOrder) {
		
		if (opTrxType == null && remoteMedOrder.isDhOrder()) {
			receiveDhOrder(remoteMedOrder);
			return;
		}

		switch (opTrxType) {
			case OpDiscontinue:
			case OpCancelDiscontinue:
				receiveDiscontinue(opTrxType, remoteMedOrder);
				return;
				
			default:
				switch (opTrxType.getMessageType()) {
					case NewOrder:
						receiveNewOrder(remoteMedOrder);
						return;
					case UpdateOrder:
						receiveUpdateOrder(opTrxType, remoteMedOrder);
						return;
					case ConfirmRemark:
						receiveConfirmRemark(remoteMedOrder);
						return;
				}
				break;
		}
		
		throw new UnsupportedOperationException("OP order incorrect transaction type, transaction type = " + opTrxType);
	}
	
	private void receiveDhOrder(MedOrder remoteMedOrder) {
		// no need to implement duplication check in pms-pms, because duplication order is already rejected in pms-corp
		if (retrieveMedOrderByCaseNumRefNum(remoteMedOrder.getMedCase().getCaseNum(), remoteMedOrder.getRefNum()) != null ) {
			throw new UnsupportedOperationException("Duplicate order " + remoteMedOrder.getOrderNum());
		}
		
		if (ownershipService.retrieveAndLockOwnership(remoteMedOrder.getMedCase().getCaseNum()) == null) {
			// insert Ownership
			ownershipService.insertOwnership(remoteMedOrder.getMedCase().getCaseNum());
			ownershipService.retrieveAndLockOwnership(remoteMedOrder.getMedCase().getCaseNum());
		}
		
		for (MedOrderItem moi : remoteMedOrder.getMedOrderItemList()) {
			for (MedOrderItemAlert moia : moi.getMedOrderItemAlertList()) {
				if (moia.getAlertType() == AlertType.OpHla && moia.getOverrideReason().isEmpty()) {
					moia.setOverrideReason(Arrays.asList(ALERT_HLA_DHORDER_OVERRIDE_REASON.get()));
				}
			}
		}

		// insert MedOrder
		MedOrder medOrder = orderManager.insertMedOrder(remoteMedOrder);
		
		markDhLatestFlag(medOrder.getMedCase().getCaseNum(), medOrder.getOrderNum());

		// (note: no need to set in pms-pms, because updated medOrder is sent to pms-pms)
		em.flush();
		medOrder.setOrgMedOrderId(medOrder.getId());
	}

	public void receiveNewOrder(MedOrder remoteMedOrder) {		
		MedOrder prevMedOrder = null;
		Boolean unvetRequired = false;

		// no need to implement duplication check in pms-pms, because duplication order is already rejected in pms-corp
		if ( orderManager.retrieveMedOrder(remoteMedOrder.getOrderNum(), Arrays.asList(MedOrderStatus.SysDeleted)) != null ) {
			throw new UnsupportedOperationException("Duplicate order " + remoteMedOrder.getOrderNum());
		}
		
		// insert MedOrder
		MedOrder medOrder = orderManager.insertMedOrder(remoteMedOrder);
		
		// (note: no need to set in pms-pms, because updated medOrder is sent to pms-pms)
		em.flush();
		medOrder.setOrgMedOrderId(medOrder.getId());

		// insert Ownership
		ownershipService.insertOwnership(medOrder.getOrderNum());

		if (medOrder.getPrevOrderNum() != null) {

			// lock the order by pessimistic lock
			orderManager.lockMedOrder(medOrder.getPrevOrderNum());
			
			prevMedOrder = orderManager.retrieveMedOrder(medOrder.getPrevOrderNum(), Arrays.asList(MedOrderStatus.SysDeleted));
			
			if (prevMedOrder == null) {
				Integer initOrderNum = retreiveMoeRevampInitialOrderNum(medOrder.getPatHospCode());
				if (initOrderNum == null) {
					throw new UnsupportedOperationException("No entry in table initial_order_num for order sequence checking for previous order " + medOrder.getPrevOrderNum() + " of order " + medOrder.getOrderNum());
				} else {
					if (Integer.valueOf(medOrder.getPrevOrderNum().substring(3)) >= initOrderNum) {
						medOrder.setIsRevampPrevOrder(Boolean.TRUE);
					} else {
						medOrder.setIsRevampPrevOrder(Boolean.FALSE);
					}
				}

				if (medOrder.getIsRevampPrevOrder()) {
					throw new UnsupportedOperationException("Unable to process outdated order message: Previous order " + medOrder.getPrevOrderNum() + " not found for new order " + medOrder.getOrderNum());
				} else {
					logger.warn("Legacy order message received: Previous order #0 is legacy order for new order #1", medOrder.getPrevOrderNum(), medOrder.getOrderNum());
				}
				
			} else {	
				prevMedOrder.loadChild();
				
				// carry forward vet before flag
				// (note: no need to set in pms-pms, because updated medOrder is sent to pms-pms)
				medOrder.setVetBeforeFlag(prevMedOrder.getVetBeforeFlag());
				
				if (prevMedOrder.getVetDate() != null) {
					// vetted order
					DispOrder prevDispOrder = retreiveNonRefillDispOrder(prevMedOrder.getOrderNum());

					if (prevDispOrder.getDayEndStatus() != DispOrderDayEndStatus.None) {
						// if pass day-end, mark the MO to delete
						// otherwise cancel and unvet order in pms
						// (it is not possible that day-end is processing for an allow modify order, so no need to check 
						//  batchProcessingFlag)
						prevMedOrder.markDeleted(MedOrderStatus.Deleted, false);
						
						// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
					} else {
						unvetRequired = true;
					}
					
				} else {
					// unvet order
					prevMedOrder.markDeleted(MedOrderStatus.Deleted, false);
					
					// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
				}
			}
		}

		em.flush();
		
		if (prevMedOrder != null) {
			// send new MedOrder to CMM if the previous order is subscribed by CMM
			OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(prevMedOrder.getOrderNum(), "CMM", RecordStatus.Active);
			
			if (cmmOrderSubscribe != null) {
				orderSubscribeManager.subscribeMedOrder(medOrder.getOrderNum(), "CMM");
				
				JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
				String medOrderXml = moJaxbWrapper.marshall(medOrder);
				this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
				
				logger.info("send MedOrder to CMM complete in receiveNewOrder, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
						medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId());
				
				if ( ! unvetRequired) {
					// send previous MedOrder to CMM if unvet NOT required 
					// (if unvet required, CMM message is sent after unvet) 
					String prevMedOrderXml = moJaxbWrapper.marshall(prevMedOrder);
					this.getCmmServiceProxy().receiveMedOrder(prevMedOrderXml);

					logger.info("send MedOrder to CMM complete in receiveNewOrder (cancel prev), medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3", 
							prevMedOrder.getOrderNum(), prevMedOrder.getPrevOrderNum(), prevMedOrder.getId(), prevMedOrder.getPrevMedOrderId());
				}
			}
		}

		em.flush();
		
		// send order to pms-pms
		pmsServiceDispatcher.receiveNewOrder(medOrder);
	}
	
	public void receiveUpdateOrder(OpTrxType opTrxType, MedOrder remoteMedOrder) {
		// dose not check ownership for update order, because the update order message cannot be skipped even the order is locked in PMS
		
		Boolean unvetRequired = false;
		
		String orderNum = remoteMedOrder.getOrderNum();
		
		// lock the order by pessimistic lock
		orderManager.lockMedOrder(orderNum);
		
		// retrieve order
		MedOrder medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));
		if (medOrder == null) {
			Integer initOrderNum = retreiveMoeRevampInitialOrderNum(remoteMedOrder.getPatHospCode());
			if (initOrderNum == null) {
				throw new UnsupportedOperationException("No entry in table initial_order_num for order sequence checking for update order " + remoteMedOrder.getOrderNum() + " (trx code = " + opTrxType.getDataValue() + ")");
			} else {
				if (Integer.valueOf(remoteMedOrder.getOrderNum().substring(3)) >= initOrderNum) {
					throw new UnsupportedOperationException("Unable to process outdated order message: Update order " + remoteMedOrder.getOrderNum() + " (trx code = " + opTrxType.getDataValue() + ") not found");
				} else {
					// if order number is legacy, ignore it (no need to implement this checking in pms-pms, because legacy update order is already ignored in pms-corp) 
					logger.warn("Legacy order message received: Update order #0 (trx code = #1) is legacy order", remoteMedOrder.getOrderNum(), opTrxType.getDataValue());
					return;
				}
			}
		}
		
        medOrder.loadChild();
		
		if (opTrxType == OpTrxType.OpDeleteMoeOrder) {
			if (medOrder.getVetDate() != null) {
				DispOrder dispOrder = retreiveNonRefillDispOrder(medOrder.getOrderNum());

				if (dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None) {
					// if pass day-end, mark the MO to delete
					// otherwise cancel and unvet order in pms
					// (it is not possible that day-end is processing for an allow modify order, so no need to check 
					//  batchProcessingFlag)
					medOrder.markDeleted(MedOrderStatus.Deleted, false);
					
					// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
				} else {
					unvetRequired = true;
				}
			} else {
				// unvet order
				medOrder.markDeleted(MedOrderStatus.Deleted, false);
				
				// note: no need to mark RefillSchedule to SysDeleted, since the mark MedOrder deleted call is not cascade
			}
			
		} else if (opTrxType == OpTrxType.OpChangePrescType) {
			if (medOrder.getStatus() != MedOrderStatus.Complete) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is received when order status is #2", medOrder.getOrderNum(), opTrxType.getDataValue(), medOrder.getStatus());
				return;
			}
			if ( medOrder.getPrescTypeUpdateDate() != null && remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getPrescTypeUpdateDate()) <= 0 ) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is out of date, cms update date = #2, order presc type update date = #3", 
						medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrder.getCmsUpdateDate(), medOrder.getPrescTypeUpdateDate());
				return;
			}
			
			if ( medOrder.getPrescTypeUpdateDate() == null || remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getPrescTypeUpdateDate()) > 0 ) {
				medOrder.setPrescType(remoteMedOrder.getPrescType());
				medOrder.setPrescTypeUpdateDate(remoteMedOrder.getCmsUpdateDate());
			}
			
		} else if (opTrxType == OpTrxType.OpAddPostDispComment) {
			Map<Integer, MedOrderItem> medOrderItemHm = createMedOrderItemHash(medOrder.getMedOrderItemList());
			
			if (medOrder.getStatus() != MedOrderStatus.Complete) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is received when order status is #2", medOrder.getOrderNum(), opTrxType.getDataValue(), medOrder.getStatus());
				return;
			}
			
			for (MedOrderItem remoteMedOrderItem : remoteMedOrder.getMedOrderItemList()) {
				if (medOrderItemHm.containsKey(remoteMedOrderItem.getItemNum())) {
					MedOrderItem tempMedOrderItem = medOrderItemHm.get(remoteMedOrderItem.getItemNum());

					if ( tempMedOrderItem.getCommentDate() == null || (remoteMedOrderItem.getCommentDate() != null && 
							remoteMedOrderItem.getCommentDate().compareTo(tempMedOrderItem.getCommentDate()) > 0) ) {
						medOrderItemHm.get(remoteMedOrderItem.getItemNum()).setCommentText(remoteMedOrderItem.getCommentText());
						medOrderItemHm.get(remoteMedOrderItem.getItemNum()).setCommentDate(remoteMedOrderItem.getCommentDate());
						medOrderItemHm.get(remoteMedOrderItem.getItemNum()).setCommentUser(remoteMedOrderItem.getCommentUser());
					} else {
						logger.warn("Unable to process outdated order message item: Update order #0 (trx code = #1) item num = #2 is out of date, message item comment date = #3, " + 
								"local item comment date = #4", medOrder.getOrderNum(), opTrxType.getDataValue(), tempMedOrderItem.getItemNum(), remoteMedOrderItem.getCommentDate(), tempMedOrderItem.getCommentDate());
					}
				}
			}
			
			boolean commentExist = false;
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getCommentText() != null) {
					commentExist = true;
					break;
				}
			}
			medOrder.setCommentFlag(commentExist); 
			
			if ( medOrder.getPrescTypeUpdateDate() == null || remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getPrescTypeUpdateDate()) > 0 ) {
				medOrder.setPrescType(remoteMedOrder.getPrescType());
				medOrder.setPrescTypeUpdateDate(remoteMedOrder.getCmsUpdateDate());
			}
			
		} else if (opTrxType == OpTrxType.OpResumeAdvanceOrder) {
			if (medOrder.getStatus() != MedOrderStatus.MoeSuspended) {
				logger.warn("Unable to process outdated order message: Update order #0 (trx code = #1) is received when order status is #2", medOrder.getOrderNum(), opTrxType.getDataValue(), medOrder.getStatus());
				return;
			}
			
			medOrder.setStatus(MedOrderStatus.Outstanding);
		}

		if ( remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getCmsUpdateDate()) > 0 ) {
			medOrder.setCmsUpdateDate(remoteMedOrder.getCmsUpdateDate());
		}
		
		em.flush();

		// send MedOrder to CMM if it is subscribed by CMM and unvet NOT required
		// (if unvet required, CMM message is sent after unvet) 
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		if (cmmOrderSubscribe != null && ! unvetRequired) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in receiveUpdateOrder, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3, trxCode=#4", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId(), opTrxType.getDataValue());
		}
		
		// send order to pms-pms
		// (send remoteMedOrder to keep message cmsUpdateDate and commentDate for checking in pms-pms)
		remoteMedOrder.setWorkstore(medOrder.getWorkstore());
		
		// send order to pms-pms
		pmsServiceDispatcher.receiveUpdateOrder(opTrxType, remoteMedOrder);
	}
	
	public void receiveConfirmRemark(MedOrder remoteMedOrder) {
		// dose not check ownership for confirm remark, because the confirm remark message cannot be skipped even the order is locked in PMS
		
		String orderNum = remoteMedOrder.getOrderNum();

		// lock the order by pessimistic lock
		orderManager.lockMedOrder(orderNum);
		
		// retrieve order
		MedOrder medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));

		if (medOrder == null) {
			Integer initOrderNum = retreiveMoeRevampInitialOrderNum(remoteMedOrder.getPatHospCode());
			if (initOrderNum == null) {
				throw new UnsupportedOperationException("No entry in table initial_order_num for order sequence checking for confirm remark " + remoteMedOrder.getOrderNum());
			} else {
				if (Integer.valueOf(remoteMedOrder.getOrderNum().substring(3)) >= initOrderNum) {
					throw new UnsupportedOperationException("Unable to process outdated order message: Confirm remark " + remoteMedOrder.getOrderNum() + " not found");
				} else {
					// if order number is legacy, ignore it (no need to implement this checking in pms-pms, because legacy confirm remark is already ignored in pms-corp) 
					logger.warn("Legacy order message received: Confirm remark #0 is legacy order", remoteMedOrder.getOrderNum());
					return;
				}
			}
		}

		if (remoteMedOrder.getStatus() == MedOrderStatus.Deleted) {
			// confirm order delete remark 
			if (medOrder.getStatus() != MedOrderStatus.Outstanding && medOrder.getStatus() != MedOrderStatus.PreVet &&
					medOrder.getStatus() != MedOrderStatus.Withhold) {
				logger.warn("Unable to process outdated order message: Confirm remark (order) #0 is received when order status is #1", medOrder.getOrderNum(), medOrder.getStatus());
				return;
			}
			if (medOrder.getRemarkCreateDate() == null) {
				logger.warn("Unable to process outdated order message: Confirm remark #0 is received but order has no delete order remark", medOrder.getOrderNum());
				return;
			}
			
			medOrder.setStatus(remoteMedOrder.getStatus());
			
			medOrder.setRemarkConfirmDate(remoteMedOrder.getRemarkConfirmDate());
			medOrder.setRemarkConfirmUser(remoteMedOrder.getRemarkConfirmUser());
			medOrder.setRemarkStatus(RemarkStatus.Confirm);
			
		} else {
			// confirm item delete remark

			if (medOrder.getStatus() != MedOrderStatus.Complete) {
				logger.warn("Unable to process outdated order message: Confirm remark (item) #0 is received when order status is #1", medOrder.getOrderNum(), medOrder.getStatus());
				return;
			}
			
			medOrder.loadChild();
			orderManager.loadOrderList(medOrder);		

			// get local medOrderItem map
			Map<Integer, MedOrderItem> medOrderItemHm = createMedOrderItemHash(medOrder.getMedOrderItemList());
			
			// get remote MedOrderItem list (exclude dummy MedOrderItem which is included in CMS chargeDtl only but not exists in ordDtl)
			List<MedOrderItem> remoteMedOrderItemList = new ArrayList<MedOrderItem>();
			for (MedOrderItem remoteMedOrderItem : remoteMedOrder.getMedOrderItemList()) {
				if (remoteMedOrderItem.getOrgItemNum() != null) {
					remoteMedOrderItemList.add(remoteMedOrderItem);
				}
			}
			
			// get dummy remote MedOrderItem list which is included in CMS chargeDtl only but not exists in ordDtl
			List<MedOrderItem> remoteChargeDtlItemList = new ArrayList<MedOrderItem>();
			for (MedOrderItem remoteMedOrderItem : remoteMedOrder.getMedOrderItemList()) {
				if (remoteMedOrderItem.getOrgItemNum() == null) {
					remoteChargeDtlItemList.add(remoteMedOrderItem);
				}
			}
			
			// group by org item num
			Map<Integer, List<MedOrderItem>> remoteMedOrderItemGroupHm = new HashMap<Integer, List<MedOrderItem>>();
			for (MedOrderItem remoteMedOrderItem : remoteMedOrderItemList) {
				List<MedOrderItem> remoteMedOrderItemListByOrgItemNum = null;
				
				if (remoteMedOrderItemGroupHm.containsKey(remoteMedOrderItem.getOrgItemNum())) {
					remoteMedOrderItemListByOrgItemNum = remoteMedOrderItemGroupHm.get(remoteMedOrderItem.getOrgItemNum());
				} else {
					remoteMedOrderItemListByOrgItemNum = new ArrayList<MedOrderItem>();
					remoteMedOrderItemGroupHm.put(remoteMedOrderItem.getOrgItemNum(), remoteMedOrderItemListByOrgItemNum);
				}
				
				remoteMedOrderItemListByOrgItemNum.add(remoteMedOrderItem);
			}
				
			// declare list to store all MOI which is not in current order (to update their status later) 
			List<Integer> otherMedOrderItemNumList = new ArrayList<Integer>();
			
			// declare list to store all confirm delete MOI (to remove their DDIM linkage later) 
			List<Long> deleteItemNumList = new ArrayList<Long>();

			// declare list to store all confirm original (L) item for confirm modify and delete (to remove FM later) 
			List<MedOrderItem> remoteConfirmOriginalItemList = new ArrayList<MedOrderItem>();
			
			
			// update pharm remark confirm (C) item of current order
			for (Map.Entry<Integer, List<MedOrderItem>> entry : remoteMedOrderItemGroupHm.entrySet()) {
				Integer orgItemNum = entry.getKey();
				List<MedOrderItem> remoteMedOrderItemListByOrgItemNum = entry.getValue();

				// search for the item to confirm
				MedOrderItem remoteConfirmMedOrderItem = null;
				MedOrderItem localConfirmMedOrderItem = null;
				for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
					if ( medOrderItemHm.containsKey(tempMedOrderItem.getItemNum()) ) {
						remoteConfirmMedOrderItem = tempMedOrderItem;
						localConfirmMedOrderItem = medOrderItemHm.get(tempMedOrderItem.getItemNum());
						break;
					}
				}
				
				if (remoteConfirmMedOrderItem == null && localConfirmMedOrderItem == null) {
					logger.warn("Unable to process outdated order message item: Confirm remark #0 is received but the item is removed " + 
							"from order, org item num = #1", orderNum, orgItemNum);
					continue;
				}

				switch (localConfirmMedOrderItem.getRemarkItemStatus()) {
					case UnconfirmedAdd:
					case UnconfirmedModify:
						if (remoteConfirmMedOrderItem.getRemarkItemStatus() != RemarkItemStatus.Confirmed) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed add/modify " + 
									"but the message item remark status is not Confirmed, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}
						
						// ignore item if there is no confirm original (L) item
						MedOrderItem remoteConfirmOriginalMedOrderItem = null;
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal) {
								remoteConfirmOriginalMedOrderItem = tempMedOrderItem;
								break;
							}
						}
						if (remoteConfirmOriginalMedOrderItem == null) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed add/modify " +
									"but there is no original item (with item remark status L) in message, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}
						
						// if pharm remark modify, add confirm original (L) item to list (to remove FM later)
						if (localConfirmMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
							remoteConfirmOriginalItemList.add(remoteConfirmOriginalMedOrderItem);
						}
						
						// update remark info for confirmed (C) item
						localConfirmMedOrderItem.setRemarkConfirmDate(remoteConfirmOriginalMedOrderItem.getRemarkConfirmDate());
						localConfirmMedOrderItem.setRemarkConfirmUser(remoteConfirmOriginalMedOrderItem.getRemarkConfirmUser());
						localConfirmMedOrderItem.setRemarkItemStatus(remoteConfirmMedOrderItem.getRemarkItemStatus());
						
						// for confirm original (L) and confirm old original (H) item, add to list to update remark status in other order later
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal || 
									tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOldOriginal) {
								otherMedOrderItemNumList.add(tempMedOrderItem.getItemNum()); 
							}
						}
						
						break;
						
					case UnconfirmedDelete:
						if (remoteConfirmMedOrderItem.getRemarkItemStatus() != RemarkItemStatus.ConfirmedOriginal) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed delete " + 
									"but the message item remark status is not L, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}

						// ignore item if there is confirm (C) item
						MedOrderItem remoteRemarkStautsConfirmMoi = null;
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.Confirmed) {
								remoteRemarkStautsConfirmMoi = tempMedOrderItem;
								break;
							}
						}
						if (remoteRemarkStautsConfirmMoi != null) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received when local item is unconfirmed delete " +
									"but the message is confirm modify item (both remark status L/C exists in message), item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
							continue;
						}
						
						// update remark info for confirmed (L) item
						localConfirmMedOrderItem.setRemarkConfirmDate(remoteConfirmMedOrderItem.getRemarkConfirmDate());
						localConfirmMedOrderItem.setRemarkConfirmUser(remoteConfirmMedOrderItem.getRemarkConfirmUser());
						localConfirmMedOrderItem.setRemarkItemStatus(remoteConfirmMedOrderItem.getRemarkItemStatus());

						// update MOI status for confirmed (L) item
						localConfirmMedOrderItem.setStatus(MedOrderItemStatus.SysDeleted);
						
						// add to pharm remark delete MOI list (to remove DDIM linkage later)
						deleteItemNumList.add(Long.valueOf(remoteConfirmMedOrderItem.getItemNum().intValue()));

						// add pharm remark delete (L) item to list (to remove FM later)
						remoteConfirmOriginalItemList.add(remoteConfirmMedOrderItem);
						
						// for confirm old original (H) item, add to list to update remark status in other order later
						for (MedOrderItem tempMedOrderItem : remoteMedOrderItemListByOrgItemNum) {
							if (tempMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOldOriginal) {
								otherMedOrderItemNumList.add(tempMedOrderItem.getItemNum()); 
							}
						}
						
						break;
						
					default:
						if (localConfirmMedOrderItem != null && remoteConfirmMedOrderItem != null) {
							logger.warn("Unable to process outdated order message item: Confirm remark #0 is received but local item remark status " + 
									"is not unconfirmed add/modify/delete, item num = #1, org item num = #2, message item remark status = #3, " +
									"local item remark status = #4", orderNum, localConfirmMedOrderItem.getItemNum(), orgItemNum, 
									remoteConfirmMedOrderItem.getRemarkItemStatus(), localConfirmMedOrderItem.getRemarkItemStatus());
						}
						continue;
				}
			}
			
			
			// update pharm remark confirm original (L) and confirm old original (H) item of other order
			List<MedOrderItem> otherMedOrderItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, otherMedOrderItemNumList);
			Map<Integer, MedOrderItem> remoteMedOrderItemHm = createMedOrderItemHash(remoteMedOrderItemList);
			for (MedOrderItem otherMedOrderItem : otherMedOrderItemList) {
				if (remoteMedOrderItemHm.containsKey(otherMedOrderItem.getItemNum())) {
					otherMedOrderItem.setRemarkItemStatus(remoteMedOrderItemHm.get(otherMedOrderItem.getItemNum()).getRemarkItemStatus());
				}
			}
			
			
			// update cmsConfirmChargeFlag and cmsConfirmUnitOfCharge of MedOrderItem and CapdItem for all items included in Confirm Remark
			// (note: the 2 confirm charge variables are used to sent back to CMS, and resume charge variables in unvet)
			// (note: there are 3 kinds of items that need to update confirm charge variables:
			//        1. pharmacy remark items
			//        2. other items with same displayname, form, salt with remark items, and CMS send both ordDtl and chargeDtl for that item
			//        3. other items with same displayname, form, salt with remark items, and CMS send only chargeDtl for that item)
			// -----------------------
			// update confirm charge variables of type 1 and type 2 item
			List<Integer> remoteMedOrderItemNumList = new ArrayList<Integer>(); 
			for (MedOrderItem remoteMedOrderItem : remoteMedOrderItemList) {
				remoteMedOrderItemNumList.add(remoteMedOrderItem.getItemNum());
			}
			List<MedOrderItem> localMedOrderItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, remoteMedOrderItemNumList);
			for (MedOrderItem localMedOrderItem : localMedOrderItemList) {
				if (remoteMedOrderItemHm.containsKey(localMedOrderItem.getItemNum())) {
					MedOrderItem remoteMedOrderItem = remoteMedOrderItemHm.get(localMedOrderItem.getItemNum());
					
					localMedOrderItem.setCmsConfirmChargeFlag(remoteMedOrderItem.getCmsConfirmChargeFlag());
					localMedOrderItem.setCmsConfirmUnitOfCharge(remoteMedOrderItem.getCmsConfirmUnitOfCharge());
					
					if (localMedOrderItem.getRxItemType() == RxItemType.Capd) { 
						for (int i=0; i<remoteMedOrderItem.getCapdRxDrug().getCapdItemList().size(); i++) {
							CapdItem remoteCapdItem = remoteMedOrderItem.getCapdRxDrug().getCapdItemList().get(i);
							CapdItem localCapdItem = localMedOrderItem.getCapdRxDrug().getCapdItemList().get(i);
							localCapdItem.setCmsConfirmChargeFlag(remoteCapdItem.getCmsConfirmChargeFlag());
							localCapdItem.setCmsConfirmUnitOfCharge(remoteCapdItem.getCmsConfirmUnitOfCharge());
						}
					}
				}
			}

			// update confirm charge variables of type 3 item
			List<Integer> remoteChargeDtlItemNumList = new ArrayList<Integer>();
			Map<Integer, MedOrderItem> remoteChargeDtlItemHm = new HashMap<Integer, MedOrderItem>();
			for (MedOrderItem remoteChargeDtlItem : remoteChargeDtlItemList) {
				remoteChargeDtlItemNumList.add(remoteChargeDtlItem.getItemNum());
				remoteChargeDtlItemHm.put(remoteChargeDtlItem.getItemNum(), remoteChargeDtlItem);
			}			
			List<MedOrderItem> localChargeDtlItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, remoteChargeDtlItemNumList);
			for (MedOrderItem localChargeDtlItem : localChargeDtlItemList) {
				if (remoteChargeDtlItemHm.containsKey(localChargeDtlItem.getItemNum())) {
					MedOrderItem remoteChargeDtlItem = remoteChargeDtlItemHm.get(localChargeDtlItem.getItemNum());
					
					localChargeDtlItem.setCmsConfirmChargeFlag(remoteChargeDtlItem.getCmsConfirmChargeFlag());
					localChargeDtlItem.setCmsConfirmUnitOfCharge(remoteChargeDtlItem.getCmsConfirmUnitOfCharge());
				}
			}			
			
			
			// delete ddim for confirmed delete (L) item
			for (Long deleteItemNum : deleteItemNumList) {
				for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
					Set<MedOrderItemAlert> deleteMoiAlertSet = new HashSet<MedOrderItemAlert>();
					for (MedOrderItemAlert moiAlert : moi.getDdiAlertList()) {
						AlertDdim alertDdim = (AlertDdim) moiAlert.getAlert();
						Long tempOrderNum = Long.valueOf(medOrder.getOrderNum().substring(3));
						if ((deleteItemNum.equals(alertDdim.getItemNum1()) && tempOrderNum.equals(alertDdim.getOrdNo1()))
						 || (deleteItemNum.equals(alertDdim.getItemNum2()) && tempOrderNum.equals(alertDdim.getOrdNo2()))) {
							deleteMoiAlertSet.add(moiAlert);
						}
					}
					
					moi.getMedOrderItemAlertList().removeAll(deleteMoiAlertSet);
					moi.markAlertFlag();
					
					if (deleteMoiAlertSet.size() > 0 && medOrder.isMpDischarge()) {
						// update drug order html for discharge order line display
						JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
						String rxItemXml = rxJaxbWrapper.marshall(moi.getRxItem());
						ConvertParam convertParam = new ConvertParam();
						convertParam.setOrderType("D");
						List<String> rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml( new ArrayList<String>(Arrays.asList(rxItemXml)), convertParam );
						RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXmlList.get(0));
						if (rxItem == null) {
							throw new UnsupportedOperationException("Cannot unmarshall updated RxItem after calling DMS formatting engine, orderNum = " + orderNum + ", itemNum = " + moi.getItemNum());
						}
						moi.setRxItem(rxItem);
					}
				}
			}
			
			
			// get local confirm original item from remote confirm original item
			List<Integer> remoteConfirmOriginalItemNumList = new ArrayList<Integer>();
			for (MedOrderItem tempItem : remoteConfirmOriginalItemList) {
				remoteConfirmOriginalItemNumList.add(tempItem.getItemNum());
			}
			List<MedOrderItem> localConfirmOriginalItemList = orderManager.retrieveMedOrderItemByItemNum(orderNum, remoteConfirmOriginalItemNumList);
			
			// delete unused FM for confirmed delete and confirmed replace item, and set prev FM ind for those items			
			Set<MedOrderFm> checkRemoveMedOrderFmSet = new HashSet<MedOrderFm>();
			for (MedOrderItem remoteConfirmOriginalItem : remoteConfirmOriginalItemList) {
				remoteConfirmOriginalItem.loadDmInfo();
				List<MedOrderFm> medOrderFmList = retrieveLocalMedOrderFmListByMedOrderItem(remoteConfirmOriginalItem, medOrder.getMedOrderFmList());
				if (medOrderFmList.size() > 0) {
					checkRemoveMedOrderFmSet.addAll(medOrderFmList);					
					for (MedOrderItem localConfirmOriginalItem : localConfirmOriginalItemList) {
						if (remoteConfirmOriginalItem.getItemNum().equals(localConfirmOriginalItem.getItemNum())) {
							// if the item has any FM, mark the flag to show corresponding FM indicator in pharmacy remark old item part
							localConfirmOriginalItem.setConfirmPrevFmIndFlag(Boolean.TRUE);
						}
					}
				}
			}
			if ( ! checkRemoveMedOrderFmSet.isEmpty()) {
				Map<Long, MedOrderFm> usedMedOrderFmMap = new HashMap<Long, MedOrderFm>();
				for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
					if (medOrderItem.getStatus() != MedOrderItemStatus.SysDeleted) {
						medOrderItem.loadDmInfo();
						List<MedOrderFm> medOrderFmList = retrieveLocalMedOrderFmListByMedOrderItem(medOrderItem, medOrder.getMedOrderFmList());
						for (MedOrderFm medOrderFm : medOrderFmList) {
							if ( ! usedMedOrderFmMap.containsKey(medOrderFm.getId())) {
								usedMedOrderFmMap.put(medOrderFm.getId(), medOrderFm);
							}
						}
					}
				}
				List<MedOrderFm> unusedMedOrderFmList = new ArrayList<MedOrderFm>();
				for (MedOrderFm medOrderFm : checkRemoveMedOrderFmSet) {
					if ( ! usedMedOrderFmMap.containsKey(medOrderFm.getId())) {
						unusedMedOrderFmList.add(medOrderFm);
					}
				}
				medOrder.getMedOrderFmList().removeAll(unusedMedOrderFmList);
			}
			
			
			// update MedOrder info
			boolean allConfirm = true;
			medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));
			
			medOrder.setItemRemarkConfirmedFlag(Boolean.TRUE);	// set to status that at least one item has been confirmed
			
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getRemarkStatus() == RemarkStatus.Unconfirm) {
					allConfirm = false;
					break;
				}
			}
			if (allConfirm) {
				// update remark status
				medOrder.setRemarkStatus(RemarkStatus.Confirm);
			}
			
			if (remoteMedOrder.getMaxItemNum() > medOrder.getMaxItemNum()) {
				// update maximum item number for remark item add
				// (does not always replace maximum item number because only confirmed item is included in confirm remark) 
				medOrder.setMaxItemNum(remoteMedOrder.getMaxItemNum());
			}
		}

		if ( remoteMedOrder.getCmsUpdateDate().compareTo(medOrder.getCmsUpdateDate()) > 0 ) {
			medOrder.setCmsUpdateDate(remoteMedOrder.getCmsUpdateDate());
		}
		
		em.flush();

		// send MedOrder to CMM if it is subscribed by CMM
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in receiveConfirmRemark, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3, status=#4", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId(), medOrder.getStatus());
		}
		
		// send order to pms-pms
		// (send remoteMedOrder to include ConfirmedOldOriginal, ConfirmedOriginal items which are not in latest version of order)
		remoteMedOrder.setWorkstore(medOrder.getWorkstore());
		
		pmsServiceDispatcher.receiveConfirmRemark(remoteMedOrder);
	}
	
	public void receiveDiscontinue(OpTrxType opTrxType, MedOrder remoteMedOrder) {
		// dose not check ownership for discontinue order, because the confirm remark message cannot be skipped even the order is locked in PMS
		
		String orderNum = remoteMedOrder.getOrderNum();

		// lock the order by pessimistic lock
		orderManager.lockMedOrder(orderNum);
		
		// retrieve order
		MedOrder medOrder = orderManager.retrieveMedOrder(orderNum, Arrays.asList(MedOrderStatus.SysDeleted));
		if (medOrder == null) {
			Integer initOrderNum = retreiveMoeRevampInitialOrderNum(remoteMedOrder.getPatHospCode());
			if (initOrderNum == null) {
				throw new UnsupportedOperationException("No entry in table initial_order_num for order sequence checking for discontinue order " + remoteMedOrder.getOrderNum() + " (trx code = " + opTrxType.getDataValue() + ")");
			} else {
				if (Integer.valueOf(remoteMedOrder.getOrderNum().substring(3)) >= initOrderNum) {
					throw new UnsupportedOperationException("Unable to process outdated order message: Discontinue order " + remoteMedOrder.getOrderNum() + " (trx code = " + opTrxType.getDataValue() + ") not found");
				} else {
					// if order number is legacy, ignore it (no need to implement this checking in pms-pms, because legacy discontinue order is already ignored in pms-corp) 
					logger.warn("Legacy order message received: Discontinue order #0 (trx code = #1) is legacy order", remoteMedOrder.getOrderNum(), opTrxType.getDataValue());
					return;
				}
			}
		}
		
        medOrder.loadChild();

        
		// check whether the cluster is enabled
		PropMap discontinueClusterCodePropMap = applicationProp.getDiscontinueClusterCodePropMap();
		String clusterCode = medOrder.getWorkstore().getHospital().getHospitalCluster().getClusterCode();
		if ( ! discontinueClusterCodePropMap.getValueAsBool(clusterCode, false)) {
			throw new UnsupportedOperationException("Cluster is not enabled to receive discontinue message, order = " + remoteMedOrder.getOrderNum() + ", trx code = " + opTrxType.getDataValue() + 
					", hosp code = " + medOrder.getWorkstore().getHospCode() + ", workstore code = " + medOrder.getWorkstore().getWorkstoreCode() + ", cluster code = " + clusterCode);
		}
		
		MedOrderItem remoteMedOrderItem = remoteMedOrder.getMedOrderItemList().get(0);
		MedOrderItem medOrderItem = medOrder.getMedOrderItemByItemNum(remoteMedOrderItem.getItemNum());
		
		if (medOrderItem == null) {
			logger.warn("Unable to process outdated order message item: Discontinue order #0 (trx code = #1) item num = #2 item not found, discontinue date = #3", 
					medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrderItem.getItemNum(), remoteMedOrderItem.getDiscontinueDate());
			return;
		}

		if (medOrderItem.getDiscontinueStatus() == DiscontinueStatus.Confirmed) {
			logger.info("Unable to process outdated order message item: Discontinue order #0 (trx code = #1) item num = #2 is already confirmed discontinue, discontinue date = #3", 
					medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrderItem.getItemNum(), remoteMedOrderItem.getDiscontinueDate());
			return;
		}
		
		RxItem rxItem = medOrderItem.getRxItem();
		
		if (rxItem.getDiscontinueDate() != null && remoteMedOrderItem.getDiscontinueDate().compareTo(rxItem.getDiscontinueDate()) <= 0) {
			logger.warn("Unable to process outdated order message item: Discontinue order #0 (trx code = #1) item num = #2 is out of date, message discontinue date = #3, local discontinue date = #4", 
					medOrder.getOrderNum(), opTrxType.getDataValue(), remoteMedOrderItem.getItemNum(), remoteMedOrderItem.getDiscontinueDate(), rxItem.getDiscontinueDate());
			return;
		}

		
		medOrderItem.setDiscontinueStatus(remoteMedOrderItem.getDiscontinueStatus());
		
		if (opTrxType == OpTrxType.OpDiscontinue) {
			rxItem.setDiscontinueFlag(Boolean.TRUE);

			rxItem.setDiscontinueReason( remoteMedOrderItem.getDiscontinueReason() );
			rxItem.setDiscontinueHospCode( remoteMedOrderItem.getDiscontinueHospCode() );
			
		} else if (opTrxType == OpTrxType.OpCancelDiscontinue) {
			rxItem.setDiscontinueFlag(Boolean.FALSE);
			
			rxItem.setDiscontinueReason(null);
			rxItem.setDiscontinueHospCode(null);
		}
		rxItem.setDiscontinueDate( remoteMedOrderItem.getDiscontinueDate() );
		
		em.flush();

		// send MedOrder to CMM if it is subscribed by CMM
		OrderSubscribe cmmOrderSubscribe = orderSubscribeManager.retrieveOrderSubscribe(medOrder.getOrderNum(), "CMM", RecordStatus.Active);
		if (cmmOrderSubscribe != null) {
			JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
			String medOrderXml = moJaxbWrapper.marshall(medOrder);
			this.getCmmServiceProxy().receiveMedOrder(medOrderXml);
			
			logger.info("send MedOrder to CMM complete in receiveDiscontinue, medOrderNum=#0, prevMedOrderNum=#1, medOrderId=#2, prevMedOrderId=#3, trxCode=#4", 
					medOrder.getOrderNum(), medOrder.getPrevOrderNum(), medOrder.getId(), medOrder.getPrevMedOrderId(), opTrxType.getDataValue());
		}
		
		// send order to pms-pms
		// (send remoteMedOrder to sync with other CMS order receiving)
		remoteMedOrder.setWorkstore(medOrder.getWorkstore());
		
		pmsServiceDispatcher.receiveDiscontinue(opTrxType, remoteMedOrder);
	}
	
	public void receiveMedProfileOrder(MedProfileOrder medProfileOrder) {
		updateMedProfileOrderInfo(medProfileOrder);
		
		pmsServiceDispatcher.receiveMedProfileOrder(medProfileOrder);
	}
	
	private void updateMedProfileOrderInfo(MedProfileOrder medProfileOrder) {
		if (medProfileOrder.getWorkstore() != null) {
			Workstore loadWorkstore = orderManager.retrieveWorkstore(medProfileOrder.getWorkstore()); 
			medProfileOrder.setWorkstore(loadWorkstore);
		} else {
			HospitalCluster hospitalCluster = new HospitalCluster();
			hospitalCluster.setClusterCode(orderManager.retrieveClusterCodeByPatHospCode(medProfileOrder.getPatHospCode()));

			Hospital hospital = new Hospital();
			hospital.setHospitalCluster(hospitalCluster);
			Workstore workstore = new Workstore();
			workstore.setHospital(hospital);
			medProfileOrder.setWorkstore(workstore);
		}
		
		// clear type enum for compatibility of receiving new IP message type in old pms
		// the type enum will be converted from typeString in pms
		medProfileOrder.setType(null);
	}

	public void receiveChemoOrder(MedOrder medOrder, List<MedProfileOrder> medProfileOrderList) {
		if (medProfileOrderList != null) {
			// group chemo item before sending to PMS
			switch (medProfileOrderList.get(0).getType()) {	// group for CP1 possible order type
				case Create: 
				case Add: 
				case Modify: 
				case Delete: 
				case ModifyMds: 
					medProfileOrderList = groupChemoOrder(medProfileOrderList);
					break;
				default:
					break;
			}

			// set workstore and cluster info to order
			for (MedProfileOrder medProfileOrder : medProfileOrderList) {
				updateMedProfileOrderInfo(medProfileOrder);
			}
		}

		// send order to cmm
		sendChemoOrderToCmm(medProfileOrderList);
		
		// send order to pms
		pmsServiceDispatcher.receiveChemoOrder(medOrder, medProfileOrderList);
	}
	
	private List<MedProfileOrder> groupChemoOrder(List<MedProfileOrder> medProfileOrderList) {
		List<MedProfileOrder> groupedMedProfileOrderList = new ArrayList<MedProfileOrder>();
		
		for (MedProfileOrder rawMedProfileOrder : medProfileOrderList) {
			MedProfileMoItem sameGroupMedProfileMoItem = null;

			MedProfileMoItem rawMedProfileMoItem = chemoOrderHelper.extractMedProfileMoItemFromMedProfileOrder(rawMedProfileOrder);
			
			// for new item, search for item which can be grouped with current item
			if (rawMedProfileOrder.getType() == MedProfileOrderType.Create || rawMedProfileOrder.getType() == MedProfileOrderType.Add) {
				for (MedProfileOrder groupedMedProfileOrder : groupedMedProfileOrderList) {
					MedProfileMoItem groupedMedProfileMoItem = chemoOrderHelper.extractMedProfileMoItemFromMedProfileOrder(groupedMedProfileOrder);
					
					if (chemoOrderHelper.compareChemoItemSameGroup(rawMedProfileMoItem, groupedMedProfileMoItem)) {
						// merge raw item to this grouped item
						sameGroupMedProfileMoItem = groupedMedProfileMoItem;
						break;
					}
				}
			}
				
			if (sameGroupMedProfileMoItem == null) {
				// for 1st MOI of new group
				groupedMedProfileOrderList.add(rawMedProfileOrder);
				
			} else {
				// for MOI under same group
				
				// merge mo and moi but keep distinct info of current item in chemoItem 
				ChemoItem rawChemoItem = rawMedProfileMoItem.getChemoInfo().getChemoItemList().get(0);
				sameGroupMedProfileMoItem.getChemoInfo().getChemoItemList().add(rawChemoItem);

				// aggregate start date and end date in MOI
				if (rawMedProfileMoItem.getStartDate().compareTo(sameGroupMedProfileMoItem.getStartDate()) < 0) {
					sameGroupMedProfileMoItem.setStartDate(rawMedProfileMoItem.getStartDate());
				}
				if (rawMedProfileMoItem.getEndDate() == null) {
					// if one of the chemo item has no end date, the grouped chemo item should have no end date
					sameGroupMedProfileMoItem.setEndDate(null);
				} else { 
					if ( sameGroupMedProfileMoItem.getEndDate() != null && 
							rawMedProfileMoItem.getEndDate().compareTo(sameGroupMedProfileMoItem.getEndDate()) > 0) {
						sameGroupMedProfileMoItem.setEndDate(rawMedProfileMoItem.getEndDate());
					}
				}
				
				if (rawMedProfileMoItem.getRxItemType() == RxItemType.Oral || rawMedProfileMoItem.getRxItemType() == RxItemType.Injection) {
					RxDrug rawRxDrug = (RxDrug)rawMedProfileMoItem.getRxItem();
					RxDrug sameGroupRxDrug = (RxDrug)sameGroupMedProfileMoItem.getRxItem();
					Regimen rawRegimen = rawRxDrug.getRegimen(); 
					Regimen sameGroupRegimen = sameGroupRxDrug.getRegimen();
					if (rawRegimen.getDoseGroupList().size() > 1) {
						throw new UnsupportedOperationException("Step up/down item is not supported for chemo order");
					}
					DoseGroup rawDoseGroup = rawRegimen.getDoseGroupList().get(0);
					DoseGroup sameGroupDoseGroup = sameGroupRegimen.getDoseGroupList().get(0);
					
					// aggregate start date and end date in DoseGroup
					if (rawDoseGroup.getStartDate().compareTo(sameGroupDoseGroup.getStartDate()) < 0) {
						sameGroupDoseGroup.setStartDate(rawDoseGroup.getStartDate());
					}
					if (rawDoseGroup.getEndDate() == null) {
						// if one of the chemo item doseGroup has no end date, the grouped chemo item doseGroup should have no end date
						sameGroupDoseGroup.setEndDate(null);
					} else { 
						if ( sameGroupDoseGroup.getEndDate() != null && 
								rawDoseGroup.getEndDate().compareTo(sameGroupDoseGroup.getEndDate()) > 0) {
							sameGroupDoseGroup.setEndDate(rawDoseGroup.getEndDate());
						}
					}
					
					// aggregate disp qty in Dose
					for (int i=0; i<rawDoseGroup.getDoseList().size(); i++) {
						Dose rawDose = rawDoseGroup.getDoseList().get(i);
						Dose sameGroupDose = sameGroupDoseGroup.getDoseList().get(i);
						
						if (rawDose.getDispQty() != null && sameGroupDose.getDispQty() != null) {
							// note: since disp qty is checked in chemo item grouping, disp qty of raw dose and 
							//       grouped dose should be both null or both not null at the same time
							sameGroupDose.setDispQty(sameGroupDose.getDispQty() + rawDose.getDispQty());
						}
					}
					
				} else if (rawMedProfileMoItem.getRxItemType() == RxItemType.Iv) {
					IvRxDrug rawIvRxDrug = (IvRxDrug)rawMedProfileMoItem.getRxItem();
					IvRxDrug sameGroupIvRxDrug = (IvRxDrug)sameGroupMedProfileMoItem.getRxItem();
					
					// aggregate start date and end date in IvRxDrug
					if (rawIvRxDrug.getStartDate().compareTo(sameGroupIvRxDrug.getStartDate()) < 0) {
						sameGroupIvRxDrug.setStartDate(rawIvRxDrug.getStartDate());
					}
					if (rawIvRxDrug.getEndDate() == null) {
						// if one of the chemo item doseGroup has no end date, the grouped chemo item doseGroup should have no end date
						sameGroupIvRxDrug.setEndDate(null);
					} else { 
						if ( sameGroupIvRxDrug.getEndDate() != null && 
								rawIvRxDrug.getEndDate().compareTo(sameGroupIvRxDrug.getEndDate()) > 0) {
							sameGroupIvRxDrug.setEndDate(rawIvRxDrug.getEndDate());
						}
					}

					// aggregate disp qty in IvRxDrug
					if (rawIvRxDrug.getDispQty() != null && sameGroupIvRxDrug.getDispQty() != null) {
						// note: since disp qty is checked in chemo item grouping, disp qty of raw dose and 
						//       grouped dose should be both null or both not null at the same time
						sameGroupIvRxDrug.setDispQty(sameGroupIvRxDrug.getDispQty() + rawIvRxDrug.getDispQty());
					}
					
				} else {
					throw new UnsupportedOperationException("Unsupported item type for chemo order, rxItemType = " + rawMedProfileMoItem.getRxItemType());
				}
			}
		}
		
		return groupedMedProfileOrderList;
	}

	private void sendChemoOrderToCmm(List<MedProfileOrder> medProfileOrderList) {
		JaxbWrapper<MedProfileOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MP_MO);
		List<String> medProfileOrderXmlList = new ArrayList<String>();
		for (MedProfileOrder medProfileOrder : medProfileOrderList) {
			for (MedProfileMoItem medProfileMoItem : medProfileOrder.getMedProfileMoItemList()) {
				// rxItemXml and chemoXml is required by cmm 
				medProfileMoItem.preSave();
			}
			medProfileOrderXmlList.add(moJaxbWrapper.marshall(medProfileOrder));
		}
		this.getCmmServiceProxy().receiveMedProfileOrderList(medProfileOrderXmlList);
		
		for (MedProfileOrder medProfileOrder : medProfileOrderList) {
			for (MedProfileMoItem medProfileMoItem : medProfileOrder.getMedProfileMoItemList()) {
				// clear rxItemXml and chemoXml after send to cmm to reduce message size 
				medProfileMoItem.clearXml();
			}
		}
	}
	
	public void receivePatientTrx(MedProfileOrder medProfileOrder) {
		HospitalCluster hospitalCluster = new HospitalCluster();
		hospitalCluster.setClusterCode(orderManager.retrieveClusterCodeByPatHospCode(medProfileOrder.getPatHospCode()));

		Hospital hospital = new Hospital();
		hospital.setHospitalCluster(hospitalCluster);
		Workstore workstore = new Workstore();
		workstore.setHospital(hospital);
		medProfileOrder.setWorkstore(workstore);
		
		MedProfileOrderType type = medProfileOrder.getType();
		
		// clear type enum for compatibility of receiving new IP message type in old pms
		// the type enum will be converted from typeString in pms
		medProfileOrder.setType(null);

		// send pat trx active HN association message to cmm
		if (type == MedProfileOrderType.PatientRegistrationMp) {
			List<MedProfileOrder> medProfileOrderList = new ArrayList<MedProfileOrder>();
			medProfileOrderList.add(medProfileOrder);
			sendChemoOrderToCmm(medProfileOrderList);
		}
		
		// send message to pms
		pmsServiceDispatcher.receiveMedProfileOrder(medProfileOrder);

		// send message to asp 
		if(applicationProp.isAspRollout() 
				&& isAspHosp(medProfileOrder.getPatHospCode()) 
				&& (MedProfileOrderType.Discharge.equals(type) || MedProfileOrderType.CancelDischarge.equals(type)))
		{
			medProfileOrder.setType(type);
			this.getAspServiceProxy().receivePatientTrx(medProfileOrder);
		}
	}
	
	@SuppressWarnings("unchecked")
	private boolean isAspHosp(String patHospCode)
	{
		List<AspHosp> aspHospList = em.createQuery(
				"select o from AspHosp o, HospitalMapping h"+ // 20171024 index check : AspHosp.hospCode : PK_ASP_HOSP
				" where o.hospCode = h.hospCode"+
				" and h.patHospCode = :patHospCode"
				)
				.setParameter("patHospCode", patHospCode)
				.getResultList();
		
		return !aspHospList.isEmpty();
	}
	
	private AspServiceJmsRemote getAspServiceProxy()
	{
		return (AspServiceJmsRemote)Component.getInstance("aspServiceProxy", false);
	}
	
	
	private Integer retreiveMoeRevampInitialOrderNum(String patHospCode) {
		InitialOrderNum initialOrderNum = em.find(InitialOrderNum.class, patHospCode);
		
		if (initialOrderNum == null) {
			return null;
		} else {
			return initialOrderNum.getOrderNum();
		}		
	}
	
	@SuppressWarnings(UNCHECKED)
	private DispOrder retreiveNonRefillDispOrder(String orderNum) {
		List<DispOrder> dispOrderList = (List<DispOrder>) em.createQuery(
				"select o from DispOrder o" + // 20120303 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.pharmOrder.medOrder.orderNum = :orderNum" +
				" and o.status not in :status" +
				" and o.refillFlag = :refillFlag")
				.setParameter("orderNum", orderNum)
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.setParameter("refillFlag", Boolean.FALSE)
				.getResultList();

		if (dispOrderList.isEmpty()) {
			throw new UnsupportedOperationException("Vetted order with no non-refill DispOrder, orderNum = " + orderNum);
		} else {
			return dispOrderList.get(0);
		}
	}
	
	private Map<Integer, MedOrderItem> createMedOrderItemHash(List<MedOrderItem> medOrderItemList) {
		HashMap<Integer, MedOrderItem> medOrderItemHm = new HashMap<Integer, MedOrderItem>();
		
		for (MedOrderItem medOrderItem : medOrderItemList) {
			medOrderItemHm.put(medOrderItem.getItemNum(), medOrderItem);
		}
		
		return medOrderItemHm;
	}
	
	public List<MedOrderFm> retrieveLocalMedOrderFmListByMedOrderItem(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList) {
		List<MedOrderFm> foundMedOrderFmList = new ArrayList<MedOrderFm>();

		if (medOrderItem.getRxItemType() == RxItemType.Capd || FmStatus.dataValueOf(medOrderItem.getFmStatus()) == FmStatus.FreeTextEntryItem) {
			return foundMedOrderFmList;
		}
		
		MedOrderFm foundMedOrderFm = null;
		
		switch (medOrderItem.getRxItemType()) {
			case Oral:
			case Injection:
				foundMedOrderFm = retrieveLocalMedOrderFmByRxDrug(medOrderItem.getRxDrug(), medOrderFmList);
				if (foundMedOrderFm != null) {
					foundMedOrderFmList.add(foundMedOrderFm);
				}
				break;
				
			case Iv:
				for (IvAdditive ivAdditive : medOrderItem.getIvRxDrug().getIvAdditiveList()) {
					foundMedOrderFm = retrieveLocalMedOrderFmByRxDrug(ivAdditive, medOrderFmList);
					if (foundMedOrderFm != null) {
						foundMedOrderFmList.add(foundMedOrderFm);
					}
				}
				break;
		}
		
		return foundMedOrderFmList;
	}
	
	public MedOrderFm retrieveLocalMedOrderFmByRxDrug(RxDrug rxDrug, List<MedOrderFm> medOrderFmList) {
		for (MedOrderFm medOrderFm : medOrderFmList) {
			if (rxDrug.getStrengthCompulsory()) {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) && 
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) &&
						StringUtils.equals(medOrderFm.getItemCode(), rxDrug.getItemCode()) ) {
					return medOrderFm;
				}
			} else {
				if ( StringUtils.equals(medOrderFm.getDisplayName(), rxDrug.getDisplayName()) &&
						StringUtils.equals(medOrderFm.getFormCode(), rxDrug.getFormCode()) &&
						StringUtils.equals(medOrderFm.getSaltProperty(), rxDrug.getSaltProperty()) &&
						medOrderFm.getFmStatus() == FmStatus.dataValueOf(rxDrug.getFmStatus()) ) {
					return medOrderFm;
				}
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void markDhLatestFlag(String caseNum, String orderNum) {

		List<MedOrder> medOrderList = em.createQuery(
				"select o from MedOrder o " + // 20160616 index check : MedOrder.medCase : I_MED_CASE_01
				"where o.medCase.caseNum = :caseNum "
			)
			.setParameter("caseNum", caseNum)
			.getResultList();
		
		int latestVersion = 0;
		boolean isLatestOrder = true;
		
		for (MedOrder mo : medOrderList) {
			if (mo.getDhOrderVersion().intValue() > latestVersion) {
				latestVersion = mo.getDhOrderVersion().intValue();
				isLatestOrder = orderNum.equals(mo.getOrderNum());
			}
		}

		for (MedOrder mo : medOrderList) {
			mo.setDhLatestFlag(mo.getDhOrderVersion().intValue() == latestVersion);
		}
		
		if (isLatestOrder) {
			String hospCode = ownershipService.retrieveOwnership(caseNum).getOwner();
			if (hospCode != null) {
				pmsServiceDispatcher.markDhLatestFlag(em.find(Hospital.class, hospCode).getHospitalCluster().getClusterCode(), caseNum);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private Object retrieveMedOrderByCaseNumRefNum(String caseNum, String refNum) {

		List<MedOrder> medOrderList;
		
		if (refNum == null) {
			medOrderList = em.createQuery(
					"select o from MedOrder o " + // 20160616 index check : MedOrder.medCase : I_MED_CASE_01
					"where o.medCase.caseNum = :caseNum " +
					"and o.refNum is null "
				)
				.setParameter("caseNum", caseNum)
				.getResultList();
		}
		else {
			medOrderList = em.createQuery(
					"select o from MedOrder o " + // 20160616 index check : MedOrder.medCase : I_MED_CASE_01
					"where o.medCase.caseNum = :caseNum " +
					"and o.refNum = :refNum "
				)
				.setParameter("caseNum", caseNum)
				.setParameter("refNum", refNum)
				.getResultList();
		}
		
		return medOrderList.isEmpty() ? null : medOrderList.get(0);
	}
}
