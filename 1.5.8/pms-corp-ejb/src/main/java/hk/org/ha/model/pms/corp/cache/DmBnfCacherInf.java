package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;

public interface DmBnfCacherInf extends BaseCacherInf {

	String getDmBnfDesc(String bnfNum);

}
