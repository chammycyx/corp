package hk.org.ha.model.pms.corp.biz.medprofile;
import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;
import org.jboss.seam.annotations.async.IntervalDuration;
import org.joda.time.DateTime;

@Local
public interface MpMsgCountTriggerLocal {
	@Asynchronous	
	void triggerMessageCountByTimer(@Duration Long duration, @IntervalDuration Long intervalDuration);
	void triggerMpMsgCount(DateTime currentDateTime);
}
