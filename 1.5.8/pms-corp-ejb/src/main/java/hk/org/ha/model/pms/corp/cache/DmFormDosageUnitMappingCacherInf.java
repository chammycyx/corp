package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;

import java.util.List;

public interface DmFormDosageUnitMappingCacherInf extends BaseCacherInf {

	DmFormDosageUnitMapping getDmFormDosageUnitMappingByFormCodeDosageUnit(String formCode, String dosageUnit);
	
	List<DmFormDosageUnitMapping> getDmFormDosageUnitMappingList();
	
	List<DmFormDosageUnitMapping> getDmFormDosageUnitMappingListByFormCode(String prefixFormCode);
	
	List<DmFormDosageUnitMapping> getDmFormDosageUnitMappingListByDosageUnit(String prefixDosageUnit);

}
