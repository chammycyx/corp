package hk.org.ha.model.pms.corp.batch.worker.charging;

import hk.org.ha.model.pms.corp.persistence.charging.PatientSfiProfile;
import hk.org.ha.model.pms.vo.charging.PatientSfi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("patientSfiProfileHelper")
@Scope(ScopeType.APPLICATION)
public class PatientSfiProfileHelper {
	@SuppressWarnings("unchecked")
	public void updatePatientSfiProfile(EntityManager em, List<PatientSfi> patientSfiList) {
		List<PatientSfiProfile> orgList = em.createQuery(
				"select o from PatientSfiProfile o") // 20171024 index check : none
				.getResultList();
		Map<String, PatientSfiProfile> patientSfiProfileMap = new HashMap<String, PatientSfiProfile>();
		
		for ( PatientSfiProfile patientSfiProfile : orgList ) {
			patientSfiProfileMap.put(patientSfiProfile.getHkid(), patientSfiProfile);
		}
		
		for ( PatientSfi patientSfi : patientSfiList ) {
			String hkid = patientSfi.getHkid();
			if ( patientSfiProfileMap.containsKey(hkid) ) {
				PatientSfiProfile patientSfiProfile = patientSfiProfileMap.get(hkid);
				patientSfiProfile.setPatientSfi(patientSfi);
				patientSfiProfile.preSave();
				
				patientSfiProfileMap.remove(hkid);
			} else {
				PatientSfiProfile patientSfiProfile = new PatientSfiProfile();
				patientSfiProfile.setHkid(hkid);
				patientSfiProfile.setPatientSfi(patientSfi);
				em.persist(patientSfiProfile);
			}
		}
		
		for ( PatientSfiProfile patientSfiProfile : patientSfiProfileMap.values() ) {
			em.remove(patientSfiProfile);
		}
		em.flush();
	}
}
