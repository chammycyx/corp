package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.udt.vetting.DailyFreqSort;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmDailyFrequencyCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmDailyFrequencyCacher extends BaseCacher implements DmDailyFrequencyCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;	

	private Map<DailyFreqSort, InnerCacher> cacherMap = new HashMap<DailyFreqSort, InnerCacher >();
				
	@Override
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}
	
	public DmDailyFrequency getDmDailyFrequencyByDailyFreqCode( String dailyFreqCode ) {
		return this.getCacher( DailyFreqSort.Rank ).get( dailyFreqCode );
	}
	
	public List<DmDailyFrequency> getDmDailyFrequencyList( DailyFreqSort sortSeq ) {
		return getDmDailyFrequency(sortSeq, false);
	}

	public List<DmDailyFrequency> getDmDailyFrequencySuspendList( DailyFreqSort sortSeq ) {
		return getDmDailyFrequency(sortSeq, true);
	}
	
	private List<DmDailyFrequency> getDmDailyFrequency(DailyFreqSort sortSeq, boolean isSuspended) {
		List<DmDailyFrequency> resultList = new ArrayList<DmDailyFrequency>();
		for ( DmDailyFrequency dmDailyFrequency : this.getCacher( sortSeq ).getAll()) {
			if (isSuspended == "Y".equals(dmDailyFrequency.getSuspend())) {
				resultList.add(dmDailyFrequency);
			}
		}
		return resultList;
	}
	
	private InnerCacher getCacher(DailyFreqSort sortSeq) {
		synchronized (this) { 
			InnerCacher cacher = cacherMap.get( sortSeq );
			
			if ( cacher == null ) {
				cacher = new InnerCacher(sortSeq, this.getExpireTime() );
				cacherMap.put( sortSeq, cacher );
			}
									
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmDailyFrequency>
	{		     
		private DailyFreqSort sortSeq;		
		
		public InnerCacher(DailyFreqSort sortSeq, int expireTime) {
			super(expireTime);    
			this.sortSeq = sortSeq;
		}

		@Override
		public DmDailyFrequency create( String dailyFreqCode ) {
			this.getAll();
			return this.internalGet(dailyFreqCode);
		}

		@Override
		public Collection<DmDailyFrequency> createAll() {			
			List<DmDailyFrequency> list = dmsPmsServiceProxy.retrieveDmDailyFrequencyList();
			if (sortSeq == DailyFreqSort.Rank) {
				Collections.sort(list, new DmDailyFreqComparatorByRank());
			} else if (sortSeq == DailyFreqSort.DailyFreqDesc) {
				Collections.sort(list, new DmDailyFreqComparatorByDesc());
			}
			return list;
		}

		@Override
		public String retrieveKey(DmDailyFrequency dmDailyFrequency) {
			return dmDailyFrequency.getDailyFreqCode();
		}		
	}

	public static DmDailyFrequencyCacherInf instance()
	{
		if (!Contexts.isApplicationContextActive())
		{
			throw new IllegalStateException("No active application scope");
		}
		return (DmDailyFrequencyCacherInf) Component.getInstance("dmDailyFrequencyCacher", ScopeType.APPLICATION);
	}
	
	public static class DmDailyFreqComparatorByRank implements Comparator<DmDailyFrequency>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( DmDailyFrequency f1, DmDailyFrequency f2 ) {
			return f1.getRank().compareTo( f2.getRank() );
		}
	}
	
	public static class DmDailyFreqComparatorByDesc implements Comparator<DmDailyFrequency>, Serializable {

		private static final long serialVersionUID = 1L;

		public int compare( DmDailyFrequency f1, DmDailyFrequency f2 ) {
			return f1.getDailyFreqDesc().compareTo( f2.getDailyFreqDesc() );
		}
	}
}
