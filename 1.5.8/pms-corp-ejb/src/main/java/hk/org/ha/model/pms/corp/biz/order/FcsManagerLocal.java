package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;

import java.util.List;

import javax.ejb.Local;

@Local
public interface FcsManagerLocal  {
	void saveFcsSfiInvoice(List<Invoice> voidInvoiceList, List<Invoice> newInvoiceList);
	void updateFcsSfiInvoiceStatus(DispOrder dispOrder);
	void saveFcsSfiInvoiceListForIp(List<Invoice> newInvoiceList, Boolean directLabelPrintFlag);
	void voidFcsSfiInvoice(List<Invoice> voidInvoiceList);
	void updateFcsSfiInvoiceForceProceedInfo(Invoice invoice, Boolean fcsPersistenceEnabled);
	void updateFcsSfiInvoiceUncollectInfoList(List<Invoice> uncollectInvoiceList);
}
