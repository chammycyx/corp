package hk.org.ha.model.pms.corp.batch.worker.asp;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.HospitalMappingManagerLocal;
import hk.org.ha.service.biz.pms.asp.interfaces.AspBatchServiceJmsRemote;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("aspDispRptBean")
public class AspDispRptBean implements AspDispRptLocal {

	private Logger logger = null;
	
	private String jobId = null;
	
	private Date batchDate;
	
	private String hospCode;
	
	@In
	AspBatchServiceJmsRemote aspBatchServiceProxy;
	
	@In
	HospitalMappingManagerLocal hospitalMappingManager;
	
	@Override
	public void beginChunk(Chunk arg0) throws Exception {
	}

	@Override
	public void endChunk() throws Exception {
	}

	@Remove
	public void destroyBatch() {

	}

	@Override
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch "+info);
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if(params.getDate("batchDate") == null)
		{
			throw new Exception("batch parameter : batchDate is null");
		}
		else
		{
			batchDate = params.getDate("batchDate");
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }

	}

	@Override
	public void processRecord(Record record) throws Exception {
		Date startDatetime = new Date();
		try
		{
			hospCode = record.getString("HOSP_CODE");
			String g6pdCode = record.getString("G6PD_CODE");
			
			logger.info("jobId: "+jobId+" - process record start for hospCode [" + hospCode + "] ... @" + startDatetime);
			
			List<String> patHospCodeList = hospitalMappingManager.retrievePatHospCodeList(hospCode);
			
			aspBatchServiceProxy.generatePendingAspDispRpt(batchDate, hospCode, g6pdCode, patHospCodeList);
		}
		catch (Exception e)
		{
			logger.error("jobId: "+jobId+" - Batch job failed. Batch date: "+batchDate+", hospital: "+hospCode);
			throw e;
		}
		
		Date endDatetime = new Date();
        logger.info("jobId: "+jobId+" - process record complete for hospCode [" + hospCode + "] ..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}

}
