package hk.org.ha.model.pms.corp.biz;


import hk.org.ha.service.biz.pms.corp.interfaces.CorpMaintServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface CorpMaintServiceLocal extends CorpMaintServiceJmsRemote {
}
