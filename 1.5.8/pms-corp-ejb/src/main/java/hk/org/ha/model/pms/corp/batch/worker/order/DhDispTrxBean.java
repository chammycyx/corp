package hk.org.ha.model.pms.corp.batch.worker.order;

import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.batch.worker.dayend.DayEndOrderManagerLocal;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.corp.vo.dh.DrugDispensary;
import hk.org.ha.model.pms.corp.vo.dh.DrugDispensaryDetail;
import hk.org.ha.model.pms.corp.vo.dh.DrugDispensaryInfo;
import hk.org.ha.model.pms.corp.vo.dh.DrugDispensaryOrder;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dhDispTrxBean")
@MeasureCalls
public class DhDispTrxBean implements DhDispTrxLocal {

	private static final int CREATE_DATE_BUFFER = 30;

	private static final String JAXB_CONTEXT = "hk.org.ha.model.pms.corp.vo.dh";

	private static final String SLASH = "/";
	
	private static final String DOT = ".";

	private static final String MONTH_DIR_NAME = "month";
	
	private Logger logger = null;
	
	private String jobDir = null;
	
	private String year = null;
	private String month = null;

	private Date batchDate;
	
	@In
	private ApplicationProp applicationProp;

	@In
	private DayEndOrderManagerLocal dayEndOrderManager;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {

		this.logger = logger;
		logger.info("initBatch " + info);
		
		String jobId = info.getJobId();
		String lastToken = jobId.substring(jobId.lastIndexOf("_")+1);
		if ("HOSP".equals(lastToken)) {
			jobDir = jobId.substring(0, jobId.lastIndexOf("_"));
		} else {
			jobDir = jobId;
		}
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        }
    	batchDate = params.getDate("batchDate");
		year = DateTimeFormat.forPattern("yyyy").print(new DateTime(batchDate));
		month = DateTimeFormat.forPattern("MM").print(new DateTime(batchDate));
	}

	public void beginChunk(Chunk arg0) throws Exception {
	}

	public void processRecord(Record record) throws Exception {
		
		String hospCode = record.getString("HOSP_CODE");

		Date startDatetime = new Date();
		logger.info("process record start for hospCode [" + hospCode + "]... @" + startDatetime);
		
		BatchJobState batchJobState = dayEndOrderManager.retrieveBatchJobState(jobDir, batchDate, hospCode, null);
		if (batchJobState != null) {
			logger.info("Job " + jobDir + " is already done for batchDate [" + batchDate + "], hospCode [" + hospCode + "]");
			return;
		}
		
		List<DispOrderItem> dispOrderItemList = retrieveDhItem(hospCode);

		DrugDispensary drugDispensary = constructDrugDispensary(dispOrderItemList, hospCode);

		File workDir = getDirectory(null, jobDir, DataDirType.Work);
		File workFile = getFile(workDir, applicationProp.getDhDispTrxFile(), batchDate, hospCode, "xml");
			
		File outDir = getOutDoneDirectory(jobDir, DataDirType.Out, null, hospCode, true);
		File outFile = getFile(outDir, applicationProp.getDhDispTrxFile(), batchDate, hospCode, "xml");

		DateTimeFormatter sdt = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		File errorDir = getDirectory(null, jobDir, DataDirType.Error);
		File errorFile = new File(errorDir + SLASH + outFile.getName() + DOT + sdt.print(new DateTime(new Date())));
		
		if (outFile.exists()) {
			throw new UnsupportedOperationException("File already exists in OUT folder for batchDate = [" + batchDate + "] hospCode = [" + hospCode + "] but job is not yet done");
		}

		try {
			FileUtils.writeStringToFile(workFile, JaxbWrapper.instance(JAXB_CONTEXT).marshall(drugDispensary));
			
		} catch (Exception e) {
			if (workFile.exists()) {
				FileUtils.moveFile(workFile, errorFile);
			}
			
			throw e;
		}

		FileUtils.moveFileToDirectory(workFile, outDir, false);
		
		// save to log table when day end job is done
		dayEndOrderManager.saveBatchJobState(jobDir, batchDate, hospCode, null);
		
		Date endDatetime = new Date();
        logger.info("process record complete for hospCode [" + hospCode + "]..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	public void endChunk() throws Exception {
	}
	
	@Remove
	public void destroyBatch() {
	}

	private DrugDispensary constructDrugDispensary(List<DispOrderItem> dispOrderItemList, String hospCode) {
		
		if (dispOrderItemList.isEmpty()) {
			DrugDispensary drugDispensary = new DrugDispensary();
			drugDispensary.setDispensingInstId(hospCode);
			return drugDispensary;
		}

		Map<Workstore, List<DispOrderItem>> itemByWorkstoreMap = new LinkedHashMap<Workstore, List<DispOrderItem>>();
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			Workstore workstore = dispOrderItem.getDispOrder().getWorkstore();
			List<DispOrderItem> list = itemByWorkstoreMap.containsKey(workstore) ? itemByWorkstoreMap.get(workstore) : new ArrayList<DispOrderItem>();
			list.add(dispOrderItem);
			itemByWorkstoreMap.put(workstore, list);
		}
		
		List<DrugDispensaryInfo> drugDispensaryInfoList = new ArrayList<DrugDispensaryInfo>();
		for (Entry<Workstore, List<DispOrderItem>> e : itemByWorkstoreMap.entrySet()) {
			drugDispensaryInfoList.add(constructDrugDispensaryInfo(e.getKey(), e.getValue()));
		}
		
		DrugDispensary drugDispensary = new DrugDispensary();
		drugDispensary.setDispensingInstId(dispOrderItemList.get(0).getDispOrder().getHospCode());
		drugDispensary.setDrugDispensaryInfoList(drugDispensaryInfoList);
		return drugDispensary;
	}

	private DrugDispensaryInfo constructDrugDispensaryInfo(Workstore workstore, List<DispOrderItem> dispOrderItemList) {
		
		DrugDispensaryInfo drugDispensaryInfo = new DrugDispensaryInfo();
		drugDispensaryInfo.setDispensingInstId(workstore.getHospCode());
		drugDispensaryInfo.setDispensaryCode(workstore.getWorkstoreCode());
		drugDispensaryInfo.setDispensingInstName(dispOrderItemList.get(0).getBaseLabel().getHospName());
		
		List<DrugDispensaryOrder> drugDispensaryOrderList = new ArrayList<DrugDispensaryOrder>();
		Map<DispOrder, List<DispOrderItem>> itemByDispOrderMap = new LinkedHashMap<DispOrder, List<DispOrderItem>>();
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			List<DispOrderItem> list = itemByDispOrderMap.containsKey(dispOrderItem.getDispOrder()) ? itemByDispOrderMap.get(dispOrderItem.getDispOrder()) : new ArrayList<DispOrderItem>();
			list.add(dispOrderItem);
			itemByDispOrderMap.put(dispOrderItem.getDispOrder(), list);
		}
		
		for (Entry<DispOrder, List<DispOrderItem>> e : itemByDispOrderMap.entrySet()) {
			drugDispensaryOrderList.add(constructDrugDispensaryOrder(e.getKey(), e.getValue()));
		}

		drugDispensaryInfo.setDrugDispensaryOrderList(drugDispensaryOrderList);
		return drugDispensaryInfo;
	}

	private DrugDispensaryOrder constructDrugDispensaryOrder(DispOrder dispOrder, List<DispOrderItem> dispOrderItemList) {
		DrugDispensaryOrder drugDispensaryOrder = new DrugDispensaryOrder();
		drugDispensaryOrder.setPrscrbOrderNum(dispOrder.getPharmOrder().getMedOrder().getMedCase().getCaseNum());
		
		if (StringUtils.isNotBlank(dispOrder.getPharmOrder().getMedOrder().getRefNum())) {
			drugDispensaryOrder.setVerNum(StringUtils.right(dispOrder.getPharmOrder().getMedOrder().getMedCase().getCaseNum(), 4) + '-' + dispOrder.getPharmOrder().getMedOrder().getRefNum());
		}
		
		drugDispensaryOrder.setPmiNum(dispOrder.getPharmOrder().getMedOrder().getDhPatient().getPmiNum());
		drugDispensaryOrder.setSex(null);
		drugDispensaryOrder.setDob(null);
		drugDispensaryOrder.setDispensedDate(dispOrder.getIssueDate());
		
		List<DrugDispensaryDetail> drugDispensaryDetailList = new ArrayList<DrugDispensaryDetail>();
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			drugDispensaryDetailList.add(constructDrugDispensaryDetail(dispOrderItem));
		}
		
    	Collections.sort(drugDispensaryDetailList, new DrugDispensaryDetailComparator());
    	
		drugDispensaryOrder.setDrugDispensaryDetailList(drugDispensaryDetailList);
		return drugDispensaryOrder;
	}

	private DrugDispensaryDetail constructDrugDispensaryDetail(DispOrderItem dispOrderItem) {
		DrugDispensaryDetail drugDispensaryDetail = new DrugDispensaryDetail();
		drugDispensaryDetail.setDrugItemNum(dispOrderItem.getPharmOrderItem().getItemNum());
		drugDispensaryDetail.setDspnsRecordNum(dispOrderItem.getDispOrder().getDispOrderNum() + StringUtils.leftPad(dispOrderItem.getPharmOrderItem().getItemNum().toString(), 2, '0'));
		drugDispensaryDetail.setAction(null);
		drugDispensaryDetail.setItemCode(dispOrderItem.getPharmOrderItem().getItemCode());
		drugDispensaryDetail.setUniversalTradeName(dispOrderItem.getPharmOrderItem().getTradeName());
		drugDispensaryDetail.setGenericName(dispOrderItem.getPharmOrderItem().getDupChkDisplayName());
		drugDispensaryDetail.setItemDesc(dispOrderItem.getBaseLabel().getItemDesc());
		drugDispensaryDetail.setHkRegNum(null);
		drugDispensaryDetail.setProdNum(null);
		drugDispensaryDetail.setDispensedQty(dispOrderItem.getDispQty());
		drugDispensaryDetail.setDispensedUnitName(dispOrderItem.getPharmOrderItem().getBaseUnit());
		drugDispensaryDetail.setStrength(dispOrderItem.getPharmOrderItem().getStrength());
		drugDispensaryDetail.setBatchNum(null);
		drugDispensaryDetail.setExpiryDate(null);
		drugDispensaryDetail.setDispensedDrugRecogTerm(null);
		drugDispensaryDetail.setDispensedDrugIDRecogTerm(null);
		drugDispensaryDetail.setDispensedDrugDescRecogTerm(null);
		return drugDispensaryDetail;
	}

	@SuppressWarnings("unchecked")
	private List<DispOrderItem> retrieveDhItem(String hospCode) {
		// since DispOrder table is partitioned by createDate, createDate range is applied to SQL to ensure only 1-2 
		// partition is searched for performance tuning
        Integer dayEndDuration = BATCH_DAYEND_ORDER_DURATION.get();
		if (dayEndDuration == null) {
			throw new UnsupportedOperationException("corporate property : batch.dayEnd.order.duration is null");
		}
		Date createDateStart = new DateMidnight(batchDate).minusDays(dayEndDuration).minusDays(CREATE_DATE_BUFFER).toDate();
		Date createDateEnd = new DateMidnight(batchDate).toDateTime().plusDays(1).minus(1).toDate();
		
		List<DispOrderItem> dispOrderItemList = em.createQuery(
				"select o from DispOrderItem o" + // 20170418 index check : DispOrder.hospCode,dayEndBatchDate : I_DISP_ORDER_08
				" where o.dispOrder.dayEndStatus = :dayEndStatus" +
				" and o.dispOrder.hospCode = :hospCode" +
				" and o.dispOrder.dayEndBatchDate = :batchDate" +
				" and o.dispOrder.createDate between :createDateStart and :createDateEnd" +
				" and o.createDate between :createDateStart and :createDateEnd" +
				" and o.dispOrder.pharmOrder.medOrder.prescType = :prescType")
				.setParameter("dayEndStatus", DispOrderDayEndStatus.Completed)
				.setParameter("hospCode", hospCode)
				.setParameter("batchDate", batchDate)
				.setParameter("createDateStart", createDateStart)
				.setParameter("createDateEnd", createDateEnd)
				.setParameter("prescType", MedOrderPrescType.Dh)
				.setHint(QueryHints.FETCH, "o.pharmOrderItem.medOrderItem")
				.setHint(QueryHints.FETCH, "o.dispOrder.pharmOrder.medOrder.medCase")
				.getResultList();

		return dispOrderItemList;
	}
	
	private File getFile(File dir, String fileName, Date batchDate, String hospCode, String fileExtension) {

		DateTimeFormatter sdf = DateTimeFormat.forPattern("yyyyMMdd");
		
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(sdf.print(new DateTime(batchDate)));
		}
		
		if (hospCode != null) {
			finalFileName.append(DOT)
			 .append(hospCode);
		}
		
		if (fileExtension != null) {
			finalFileName.append(DOT)
						 .append(fileExtension);
		}
		
		File file = new File(
				    dir + SLASH + 
					finalFileName );
		
		return file;
	}
	
	private File getDirectory(
			String hospCode,
			String jobId,
			DataDirType dataDirType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + SLASH + 
				jobId + SLASH + 
				dataDirType.getDataValue()
				+ (hospCode == null ? "" : SLASH + hospCode));
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}

	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
			logger.debug("DhDispTrxBean - createDirectory: create path status: " + created);
		}

		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("DhDispTrxBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("DhDispTrxBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("DhDispTrxBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}
	
	private File getOutDoneDirectory(
			String jobId,
			DataDirType dataDirType,
			String systemType,
			String hospCode,
			boolean createDir) throws Exception {
		
		StringBuilder basicPath = new StringBuilder();
		basicPath.append(applicationProp.getBatchDataDir())
						.append(SLASH).append(jobId)
						.append(SLASH).append(dataDirType.getDataValue());
		if (systemType != null) {
			basicPath.append(SLASH).append(systemType);
		}
		basicPath.append(SLASH).append(MONTH_DIR_NAME);
		
		return getOutDoneDirectory(basicPath.toString(), hospCode, createDir);
	}
	
	private File getOutDoneDirectory(
			String basicPath,
			String hospCode,
			boolean createDir) throws Exception {
		
		File basicDir = new File(basicPath);
		if ( ! basicDir.exists()) {
			throw new Exception("DhDispTrxBean: date directory could not be created as parent directory " + basicDir.getParent() + " not exists.");
		}

		StringBuilder destPath = new StringBuilder();
		destPath.append(basicPath)
						.append(SLASH).append(year)
						.append(SLASH).append(month);

		if (hospCode != null) {
			destPath.append(SLASH).append(hospCode);
		}
		
		File destDir = new File(destPath.toString());
		if ( ! destDir.exists()) {
			if (createDir) {
				destDir.mkdirs();
				if ( ! destDir.exists()) {
					throw new Exception("DhDispTrxBean: directory " + destDir.getPath() + " could not be found and created. Please check (eg. Access Right...).");
				}
			} else {
				throw new Exception("DhDispTrxBean: directory " + destDir.getPath() + " could not be found. Please check (eg. Access Right...).");
			}
		}
		
		return destDir;
	}
	
	private static class DrugDispensaryDetailComparator implements Comparator<DrugDispensaryDetail>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(DrugDispensaryDetail drugDispensaryDetail1, DrugDispensaryDetail drugDispensaryDetail2) {
			return new CompareToBuilder()
							.append( drugDispensaryDetail1.getDspnsRecordNum(), drugDispensaryDetail2.getDspnsRecordNum() )
							.toComparison();
		}
    }
}
