package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.model.pms.corp.biz.medprofile.MpMsgCountTriggerLocal;
import hk.org.ha.model.pms.corp.prop.Prop;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("corpStartup")
public class CorpStartupBean implements CorpStartupLocal {

	private static final long ONE_MINUTE_MILLI = 60000L;
	
	@Logger
	private Log logger;
	
	@In
	private MpMsgCountTriggerLocal mpMsgCountTrigger;
	
	@Override
	public void startup(Long delayDuration) {		
		startUpTriggerMpMessageCount();
	}
	
	private void startUpTriggerMpMessageCount() {
		int interval = Prop.MEDPROFILE_MSGCOUNT_INTERVAL.get(60).intValue();//default is 1 hr as interval time
		if (interval > 0 && Prop.MEDPROFILE_MSGCOUNT_ENABLED.get().booleanValue()) {
			Long intervalInMilli = Long.valueOf(interval * ONE_MINUTE_MILLI);
			mpMsgCountTrigger.triggerMessageCountByTimer(5 * ONE_MINUTE_MILLI, intervalInMilli);
		} else {
			logger.warn("startUpTriggerMpMessageCount timer disabled.");
		}
	}
}
