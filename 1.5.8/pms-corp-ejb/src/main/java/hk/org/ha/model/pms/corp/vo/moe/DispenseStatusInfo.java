package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;
import java.util.Date;

public class DispenseStatusInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemCode;
	
	private String itemDesc;
	
	private Date lastDeliveryDate;
	
	private Date dueDate;
	
	private String processingRefill;

	public DispenseStatusInfo() {
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public Date getLastDeliveryDate() {
		return lastDeliveryDate;
	}

	public void setLastDeliveryDate(Date lastDeliveryDate) {
		this.lastDeliveryDate = lastDeliveryDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getProcessingRefill() {
		return processingRefill;
	}

	public void setProcessingRefill(String processingRefill) {
		this.processingRefill = processingRefill;
	}
}
