package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.fmk.pms.batch.worker.listener.BatchWorkerListener;

import javax.ejb.Local;

@Local
public interface UncollectTrxLocal extends BatchWorkerListener {

}
