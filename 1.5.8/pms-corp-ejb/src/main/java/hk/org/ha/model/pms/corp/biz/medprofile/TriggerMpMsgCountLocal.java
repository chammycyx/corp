package hk.org.ha.model.pms.corp.biz.medprofile;
import javax.ejb.Local;

@Local
public interface TriggerMpMsgCountLocal {
	String getCheckDateTime();
	void setCheckDateTime(String value);
	void triggerMsgCountService();
	
	boolean getIsSuccess();
	String getSuccessMessage();
	void setSuccessMessage(String successMessage);
	
	boolean getHasError();
	String getErrorMessage();
	void setErrorMessage(String errorMessage);
	
	void clearScreen();
	void destroy();
}
