package hk.org.ha.model.pms.corp.batch.worker.dayend;

import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ENABLED;
import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.vo.disp.DayEndDispOrder;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
/**
 * Session Bean implementation class DayEndBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("dayEndBean")
public class DayEndBean implements DayEndLocal {
	
	private static final String FCS = "FCS";
	private static final String PMS = "PMS";
	private static final String DOT = ".";
	private static final String SLASH = "/";
	
	@In
	private DayEndOrderManagerLocal dayEndOrderManager;

	@In
	private ApplicationProp applicationProp;

	private Logger logger = null;

	private Date startDate = null;
	private Date batchDate = null;

	// for building directory
	private String jobDir = null;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
	private String exceptItemList = null;
	
    public DayEndBean() {
        
    }

	public void beginChunk(Chunk chunk) throws Exception {
		
	}

	public void endChunk() throws Exception {
		
	}

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		String jobId = info.getJobId();
		String lastToken = jobId.substring(jobId.lastIndexOf("_")+1);
		if ("HOSP".equals(lastToken)) {
			jobDir = jobId.substring(0, jobId.lastIndexOf("_"));
		} else {
			jobDir = jobId;
		}
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
		
        Integer dayEndDuration = BATCH_DAYEND_ORDER_DURATION.get();
        
		if (dayEndDuration == null) {
			throw new Exception("corporate property : batch.dayEnd.order.duration is null");
		} else {
			DateTime dt = new DateTime(batchDate);
			startDate = dt.minusDays(dayEndDuration).toDate();
		}
	}
	
	public void processRecord(Record record) throws Exception {

		String hospCode = record.getString("HOSP_CODE");
		String workstoreCode = record.getString("WORKSTORE_CODE");
		
		Date startDatetime = new Date();
		logger.info("process record start for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + "]... @" + startDatetime);

		Hospital hospital = new Hospital(hospCode);
		if ( ! BATCH_DAYEND_ENABLED.get(hospital)) {
			logger.error("Day end is disabled for hospCode = [" + hospCode + "] workstoreCode [" + workstoreCode + "]");
			return;
		}
		
		BatchJobState batchJobState = dayEndOrderManager.retrieveBatchJobState(jobDir, batchDate, hospCode, workstoreCode);
		if (batchJobState != null) {
			logger.info("Job " + jobDir + " is already done for batchDate [" + batchDate + "], hospCode [" + hospCode + "], workstoreCode [" + workstoreCode + "]");
			return;
		}
		
		List<String> fcsFileList = Arrays.asList(applicationProp.getFcsChargingTrxFile());
		List<String> pmsFileList = Arrays.asList(applicationProp.getDnldTrxFile());

		// get in directories of post day end job
		File postDayEndInFcsDir = getDirectory(hospCode, applicationProp.getPostDayendDir(), DataDirType.In, FCS);
		File postDayEndInPmsDir = getDirectory(hospCode, applicationProp.getPostDayendDir(), DataDirType.In, PMS);
		
		// check whether workstore file exist in post day-end IN folder
		// (since file is unknown file one when entry does not exists in BatchJobState, exception is thrown)
		if (checkWorkstoreFileExist(postDayEndInFcsDir, fcsFileList, workstoreCode, batchDate)) {
			throw new UnsupportedOperationException("FCS workstore file of date [" + batchDate + "] already exists in post day-end IN folder for hospCode = [" + hospCode + "] workstoreCode [" + workstoreCode + "] but job is not yet done");
		}
		if (checkWorkstoreFileExist(postDayEndInPmsDir, pmsFileList, workstoreCode, batchDate)) {
			throw new UnsupportedOperationException("PMS workstore file of date [" + batchDate + "] already exists in post day-end IN folder for hospCode = [" + hospCode + "] workstoreCode [" + workstoreCode + "] but job is not yet done");
		}
		
		// get work and error directories for job use
		File workFcsDir = getDirectory(hospCode, jobDir, DataDirType.Work, FCS);
		File workPmsDir = getDirectory(hospCode, jobDir, DataDirType.Work, PMS);
				
		File errorFcsDir = getDirectory(hospCode, jobDir, DataDirType.Error, FCS);
		File errorPmsDir = getDirectory(hospCode, jobDir, DataDirType.Error, PMS);
		
		// init day end files for the batch date, eg. DISP-TRX.WKS1.20120102
		File chargeTrx = getFile(workFcsDir, applicationProp.getFcsChargingTrxFile(), workstoreCode, batchDate, null);
		File dlTrx = getFile(workPmsDir, applicationProp.getDnldTrxFile(), workstoreCode, batchDate, null);
		
		List<DayEndDispOrder> dayEndDispOrderList = null;
		
		try {
			// new transaction is used because CDDH needs to know Processing status, and   
			// pessimistic lock is used in the function
			dayEndOrderManager.preProcessDayEnd(
					logger,
					hospCode, 
					workstoreCode, 
					startDate, 
					batchDate);
			
			// no new transaction because file generation takes long time
			// (day end status will fallback to NONE if there is exception)
			dayEndDispOrderList = dayEndOrderManager.processDayEnd(
					logger,
					hospCode, 
					workstoreCode,
					startDate,
					batchDate,
					chargeTrx, 
					dlTrx,
					Arrays.asList(exceptItemList.split(",")));
			
			// mark day end status to Complete
			// (new transaction is used to guarantee update is committed before file move) 
			dayEndOrderManager.postProcessDayEnd(
					logger,
					hospCode, 
					workstoreCode,
					batchDate, 
					dayEndDispOrderList,
					"[" +
					StringUtils.join(
							Arrays.asList( 
									applicationProp.getFcsChargingTrxFile(), 
									applicationProp.getDnldTrxFile()).toArray(),  
							"][") + 
					"]");
			
		} catch (Exception e) {
			moveDayEndFileToErrorDirectory(chargeTrx, dlTrx, errorFcsDir, errorPmsDir, startDatetime);
			
			// do not fallback day end status if postProcessDayEnd throws exception,  because if 
			// marking Complete status failure in DB, mark None status will also fail
			
			throw e;
		}

		// move completed day end files to post day end 'in' directory
		// (No exception handling because assuming move file have no chance to fail)
		FileUtils.moveFileToDirectory(chargeTrx, postDayEndInFcsDir, false);
		FileUtils.moveFileToDirectory(dlTrx, postDayEndInPmsDir, false);
		
		// save to log table when day end job is done
		dayEndOrderManager.saveBatchJobState(jobDir, batchDate, hospCode, workstoreCode);
		
		Date endDatetime = new Date();
        logger.info("process record complete for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + "]..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	private void moveDayEndFileToErrorDirectory(File chargeTrx, File dlTrx, File errorFcsDir,
			File errorPmsDir, Date startDatetime) throws Exception {
		moveFileToDirectory(chargeTrx, errorFcsDir, startDatetime);
		moveFileToDirectory(dlTrx, errorPmsDir, startDatetime);
	}
	
	private boolean checkWorkstoreFileExist(File dir, List<String> fileNameList, String workstoreCode, Date batchDate) {
		for (String fileName: fileNameList) {
			File file = getFile(dir, fileName, workstoreCode, batchDate, null);
			if (file.exists()) {
				return true;
			}
		}
		return false;
	}
	
	private File getDirectory(
			String hospCode, 
			String jobId,
			DataDirType dataDirType, 
			String systemType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + SLASH + 
				jobId + SLASH + 
				dataDirType.getDataValue() + SLASH + 
				systemType + SLASH + 
				hospCode);
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
			if (logger.isDebugEnabled()) {
		logger.debug("DayEndBean: get directory " + directory.getPath());
			}		
		return directory;
	}
	
	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
		logger.debug("DayEndBean - createDirectory: create path status: " + created);
		}
		
		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("DayEndBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("DayEndBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
			logger.debug("DayEndBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}
	
	private File getFile(
			File dir, 
			String fileName,
			String workstoreCode,
			Date batchDate,
			Date currentDate) {
		
		DateTimeFormatter sdt = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (workstoreCode != null) {
			finalFileName.append(DOT)
						 .append(workstoreCode);
		}
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(dateFormat.format(batchDate));
		}
		
		if (currentDate != null) {
			finalFileName.append(DOT)
						 .append(sdt.print(new DateTime(currentDate)));
		}
		
		File file = new File(
				    dir + SLASH + 
					finalFileName );
		
		if (logger.isDebugEnabled()) {
		logger.debug("DayEndBean: get file " + file.getPath());
		}
		
		return file;
	}
	
	private void moveFileToDirectory(File file, File destDir, Date currentDate) throws IOException {
		if (file != null && file.exists()) {
			FileUtils.moveFile(file, getFile(destDir, file.getName(), null, null, currentDate));
		}
	}
	
	@Remove
	public void destroyBatch() {
	}
}
