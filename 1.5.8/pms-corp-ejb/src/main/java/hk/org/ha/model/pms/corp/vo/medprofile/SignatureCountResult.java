package hk.org.ha.model.pms.corp.vo.medprofile;

import java.io.Serializable;
import java.util.Date;

public class SignatureCountResult implements Serializable {
	private static final long serialVersionUID = 1L;
	private String hospitalCode;
	private Date signedDate;
	private long count;
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}
	public Date getSignedDate() {
		return signedDate;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public long getCount() {
		return count;
	}
}
