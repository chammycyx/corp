package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.fmk.pms.remote.annotations.Marshaller;
import hk.org.ha.fmk.pms.remote.annotations.MarshallerType;
import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;

import java.util.List;

import org.jboss.seam.annotations.async.Asynchronous;
import org.osoa.sca.annotations.OneWay;

public interface CorpSubscriberJmsRemote {

	@OneWay
	@Asynchronous
	void initCache();
		
	@OneWay
	@Asynchronous
	@Marshaller(MarshallerType.JAVA)
	void initCacheWithDmDrugList(List<DmDrug> dmDrugList);
	
	@OneWay
	@Asynchronous
	void initJmsServiceProxy();
	
	@OneWay
	@Asynchronous
	void responsePmsSessionInfo(String requestId, List<PmsSessionInfo> pmsSessionInfoList);	
	
	@OneWay
	@Asynchronous
	void responsePmsSignatureCountResult(String requestId, List<SignatureCountResult> signatureCountResultList);
}
