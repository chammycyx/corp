package hk.org.ha.model.pms.corp.biz;
import java.util.Date;

import hk.org.ha.model.pms.persistence.reftable.CorporateProp;

import javax.ejb.Local;

@Local
public interface CorporatePropManagerLocal {
	CorporateProp retrieveProp(String name);
	void updatePatTrxTriggerProp(Date maxDate);
}
