package hk.org.ha.model.pms.corp.batch.worker.asp;

import javax.ejb.Local;

import hk.org.ha.fmk.pms.batch.worker.listener.BatchWorkerListener;

@Local
public interface AspLegacyDispItemLocal extends BatchWorkerListener {

}
