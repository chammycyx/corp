package hk.org.ha.model.pms.corp.biz.medprofile;
import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface CorpPmsSignOrderCoordinatorLocal {
	void consolidatePmsSignatureCountResult(String requestId, List<SignatureCountResult> signatureCountResultList);
	List<SignatureCountResult> retrievePmsSignatureCountResultList(String requestId, Date batchDate);
}
