package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;

import java.util.List;

public interface DmRegimenCacherInf extends BaseCacherInf {

	DmRegimen getRegimenByRegimenCode(String regimenCode);
	
	List<DmRegimen> getRegimenList();
	
	List<DmRegimen> getRegimenListByRegimenCode(String prefixRegimenCode);

}
