package hk.org.ha.model.pms.corp.persistence.order;

public class ItemBinPK {
	
	private String hospCode;
	private String workstoreCode;
	private String itemCode;
	private String binNum;
	
	public ItemBinPK(){
	}
	
	public ItemBinPK(String hospCode, String workstoreCode, String itemCode, String binNum){
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.itemCode = itemCode;
		this.binNum = binNum;
	}
	
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public String getWorkstoreCode() {
		return workstoreCode;
	}
	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBinNum() {
		return binNum;
	}
	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((binNum == null) ? 0 : binNum.hashCode());
		result = prime * result
				+ ((hospCode == null) ? 0 : hospCode.hashCode());
		result = prime * result
				+ ((itemCode == null) ? 0 : itemCode.hashCode());
		result = prime * result
				+ ((workstoreCode == null) ? 0 : workstoreCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		ItemBinPK other = (ItemBinPK) obj;
		
		if (binNum == null) {
			if (other.binNum != null) {
				return false;
			}
		} else if (!binNum.equals(other.binNum)) {
			return false;
		}
		
		if (hospCode == null) {
			if (other.hospCode != null) {
				return false;
			}
		} else if (!hospCode.equals(other.hospCode)) {
			return false;
		}
		
		if (itemCode == null) {
			if (other.itemCode != null) {
				return false;
			}
		} else if (!itemCode.equals(other.itemCode)) {
			return false;
		}
		
		if (workstoreCode == null) {
			if (other.workstoreCode != null) {
				return false;
			}
		} else if (!workstoreCode.equals(other.workstoreCode)) {
			return false;
		}
		
		return true;
	}

}
