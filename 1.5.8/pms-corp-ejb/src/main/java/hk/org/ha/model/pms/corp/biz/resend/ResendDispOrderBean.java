package hk.org.ha.model.pms.corp.biz.resend;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.DispTrxType;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.service.biz.pms.og.interfaces.OgServiceJmsRemote;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.auth.Subject;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import weblogic.security.Security;
import weblogic.security.principal.WLSUserImpl;

@Stateful
@Scope(ScopeType.SESSION)
@Name("resendDispOrder")
public class ResendDispOrderBean implements ResendDispOrderLocal {

	@In
	private AuditLogger auditLogger;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@In
	private OgServiceJmsRemote ogServiceDispatcher;
    
	private String userName;
	
	private String dispOrderNum;

	private DispTrxType dispTrxType;
	
	private String successMessage;
	
	private String errorMessage;
	

    // for JSF binding start
	public String getUserName() {
		Subject subject = Security.getCurrentSubject();  
		Set<Principal> allPrincipals = subject.getPrincipals();
		for (Principal principal : allPrincipals) {  
			if (principal instanceof WLSUserImpl) {  
				return principal.getName();  
			}              
		}
		
		return "";
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDispOrderNum() {
		return dispOrderNum;
	}

	public void setDispOrderNum(String dispOrderNum) {
		this.dispOrderNum = dispOrderNum;
	}

	public DispTrxType getDispTrxType() {
		return dispTrxType;
	}

	public void setDispTrxType(DispTrxType dispTrxType) {
		this.dispTrxType = dispTrxType;
	}

	public boolean getIsSuccess() {
		return (!StringUtils.isBlank(successMessage));
	}
	
	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public boolean getHasError() {
		return (!StringUtils.isBlank(errorMessage));
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
    // for JSF binding end

	
    @Override
	public void resendDispOrderMessage() {
		// --------- pre process before search ------------
		successMessage = null;
		errorMessage = null;
		// ------------------------------------------------

		String inDispOrderNumStr = StringUtils.trimToEmpty(dispOrderNum);
		validateSearchCriteria(inDispOrderNumStr, dispTrxType);
		if (errorMessage != null) {
			return;
		}
		
		Integer inDispOrderNum = Integer.valueOf(inDispOrderNumStr);
		sendEprMessage(inDispOrderNum, dispTrxType);
		if (errorMessage != null) {
			return;
		}
	}
    
    private void validateSearchCriteria(String dispOrderNum, DispTrxType dispTrxType) {
		if (StringUtils.isBlank(dispOrderNum)) {
			errorMessage = "Please input dispensing order no.";
			return;
		}
		
    	if ( ! StringUtils.isNumeric(dispOrderNum)){	// must check not blank before
			errorMessage = "Invalid dispensing order no.";
			return;
    	}
    	
		if (dispTrxType == DispTrxType.Blank) {
			errorMessage = "Please input dispensing message type";
			return;
		}
    }
    
	@SuppressWarnings("unchecked")
    private void sendEprMessage(Integer dispOrderNum, DispTrxType dispTrxType) {
		List<DispOrder> dispOrderList = em.createQuery( 
				"select o from DispOrder o" + // 20130829 index check : DispOrder.dispOrderNum : I_DISP_ORDER_10
				" where o.dispOrderNum = :dispOrderNum")
				.setParameter("dispOrderNum", dispOrderNum)
				.getResultList();
		
		if (dispOrderList.size() == 0) {
			errorMessage = "Dispensing order not found";
			return;
		}
		
		DispOrder dispOrder = dispOrderList.get(0);
		dispOrder.loadChild();
		
		switch (dispTrxType) {
			case DispCreate:
				if (dispOrder.getStatus() != DispOrderStatus.Issued) {
					errorMessage = "Invalid dispensing order status [" + dispOrder.getStatus() + "] for sending CREATE message";
					return; 
				}
				if (dispOrder.getPrevDispOrderNum() != null) {
					errorMessage = "CREATE message cannot be sent for update dispensing order";
					return; 
				}
				break;
				
			case DispUpdate:
				if (dispOrder.getStatus() != DispOrderStatus.Issued) {
					errorMessage = "Invalid dispensing order status [" + dispOrder.getStatus() + "] for sending UPDATE message";
					return; 
				}
				if (dispOrder.getPrevDispOrderNum() == null) {
					errorMessage = "UPDATE message cannot be sent for new dispensing order";
					return; 
				}
				break;
				
			case DispDelete:
				boolean dispOrderStatusOk = false;
				boolean medOrderStatusOk = false;
				
				if (dispOrder.getStatus() == DispOrderStatus.SysDeleted) {
					// for MOE order unvet case
					dispOrderStatusOk = true;
				} else if (dispOrder.getStatus() == DispOrderStatus.Deleted) { 
					// for manual order delete case
					dispOrderStatusOk = true;
				} else if (dispOrder.getPharmOrder().getMedOrder().getStatus() == MedOrderStatus.Deleted) {
					// for day end completed MOE order delete case 
					medOrderStatusOk = true;
				}
				
				if ( ! medOrderStatusOk && ! dispOrderStatusOk) {
					errorMessage = "Invalid MOE order status [" + dispOrder.getPharmOrder().getMedOrder().getStatus() 
									+ "] and dispensing order status [" + dispOrder.getStatus() + "] for sending DELETE message";
					return;
				}
				
				break;
				
			case DispSuspend:
				if (dispOrder.getStatus() != DispOrderStatus.Issued) {
					errorMessage = "Invalid dispensing order status [" + dispOrder.getStatus() + "] for sending SUSPEND message";
					return; 
				}
				if (dispOrder.getAdminStatus() != DispOrderAdminStatus.Suspended) {
					errorMessage = "Invalid suspend status for sending SUSPEND message";
					return; 
				}
				break;
				
			case DispUnsuspend:
				if (dispOrder.getStatus() != DispOrderStatus.Issued) {
					errorMessage = "Invalid dispensing order status [" + dispOrder.getStatus() + "] for sending UNSUSPEND message";
					return; 
				}
				if (dispOrder.getAdminStatus() != DispOrderAdminStatus.Normal) {
					errorMessage = "Invalid suspend status for sending UNSUSPEND message";
					return; 
				}
				break;
		}
		
		// set label desc for EPR CREATE/UPDATE and CDDH create
		dispOrder.getPharmOrder().loadDmInfo();
		InstructionBuilder builder = InstructionBuilder.instance();
		dispOrder = builder.buildLabelDesc(dispOrder);
		
		// clear all DmInfo before sending to OG
		dispOrder.getPharmOrder().clearDmInfo();
		
		ogServiceDispatcher.sendOrderToEpr(dispTrxType, dispOrder);
		
		// --------- post process after send message ------------
		successMessage = createSuccessMessage(dispOrder, dispTrxType);
		auditLogger.log("#4033 Send Epr Message Success|Dispensing Message[#0]|DispOrderNum[#1]|UserName[#2]|", dispTrxType.getDataValue(), dispOrder.getDispOrderNum(), getUserName());
		// --------------------------------------------------------
    }
	
	private String createSuccessMessage(DispOrder dispOrder, DispTrxType dispTrxType) {
		StringBuilder message = new StringBuilder();
		
		message.append("Dispensing message [");
		message.append(dispTrxType.getDataValue());
		message.append("] for order number [");
		message.append(dispOrder.getDispOrderNum());
		message.append("] is sent to EPR");
		
		return message.toString();
	}

    @Override
	public void clearScreen() {
    	dispOrderNum = null;
    	dispTrxType = null;
    	
		successMessage = null;
		errorMessage = null;
	}
    
	@Remove
	public void destroy() 
	{
	}	
}
