package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.udt.batch.worker.FmRptTemplateType;

public interface FmRptExcelRendererInf {
	void retrieveFmTemplateReport(FmRptTemplateType fmRptTemplateType);
}
