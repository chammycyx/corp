package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmSiteCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmSiteCacher extends BaseCacher implements DmSiteCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
	
    public DmSite getSiteBySiteCode(String siteCode) {
		return (DmSite) this.getCacher().get(siteCode);
	}

	public List<DmSite> getSiteList() {
		return (List<DmSite>) this.getCacher().getAll();
	}	

	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmSite> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmSite create(String siteCode) {
			this.getAll();
			return this.internalGet(siteCode);
		}

		@Override
		public Collection<DmSite> createAll() {
			return dmsPmsServiceProxy.retrieveDmSiteList();
		}

		@Override
		public String retrieveKey(DmSite dmSite) {
			return dmSite.getSiteCode();
		}
	}

	public static DmSiteCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmSiteCacherInf) Component.getInstance("dmSiteCacher",
				ScopeType.APPLICATION);
	}
}
