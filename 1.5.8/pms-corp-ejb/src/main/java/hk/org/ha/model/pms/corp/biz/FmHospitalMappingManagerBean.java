package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("fmHospitalMappingManager")
@MeasureCalls
public class FmHospitalMappingManagerBean implements FmHospitalMappingManagerLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS_RPT")
	private EntityManager emReport;
	
	@SuppressWarnings("unchecked")
	@Override
	@CacheResult(timeout = "60000")
	public FmHospitalMapping retrieveFmHospitalMapping(String patHospCode) {
		List<FmHospitalMapping> resultList = emReport.createQuery(
				" select o from FmHospitalMapping o"+
				" where o.patHospCode = :patHospCode"
				)
				.setParameter("patHospCode", patHospCode)
				.getResultList();
		
		if(!resultList.isEmpty())
		{
			return resultList.get(0);
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@CacheResult(timeout = "60000")
	public List<FmHospitalMapping> retrieveFmHospitalMappingList() {
		return emReport.createQuery(
				" select o from FmHospitalMapping o"+
				" order by o.patHospCode"
				)
				.getResultList();
	}
}
