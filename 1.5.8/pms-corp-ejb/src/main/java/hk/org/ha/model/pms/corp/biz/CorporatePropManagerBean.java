package hk.org.ha.model.pms.corp.biz;

import java.util.Date;

import hk.org.ha.model.pms.persistence.reftable.CorporateProp;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

@AutoCreate
@Stateless
@Name("corporatePropManager")
@Scope(ScopeType.STATELESS)
public class CorporatePropManagerBean implements CorporatePropManagerLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	public CorporateProp retrieveProp(String name) {
		CorporateProp corporateProp = (CorporateProp) em.createQuery(
				"select o from CorporateProp o" + // 20120213 index check : Prop.name : UI_PROP_01
				" where o.prop.name = :name") 
				.setParameter("name", name)
				.getSingleResult();
		return corporateProp;
	}
	
	public void updatePatTrxTriggerProp(Date maxDate) {
		CorporateProp lastTrxDateProp = retrieveProp("batch.patientTrx.lastTrxDate");
		lastTrxDateProp.setValue(DateTimeFormat.forPattern(
				lastTrxDateProp.getProp().getValidation()).print(new DateTime(maxDate)));
		em.merge(lastTrxDateProp);
		em.flush();
	}	
}
