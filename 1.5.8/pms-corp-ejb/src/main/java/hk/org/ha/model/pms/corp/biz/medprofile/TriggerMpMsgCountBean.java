package hk.org.ha.model.pms.corp.biz.medprofile;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("triggerMpMsgCount")
public class TriggerMpMsgCountBean implements TriggerMpMsgCountLocal {

	@In
	private MpMsgCountTriggerLocal mpMsgCountTrigger;
	
	private String checkDateTime;
	
	private String successMessage;
	
	private String errorMessage;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public boolean getIsSuccess() {
		return (!StringUtils.isBlank(successMessage));
	}
	
	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public boolean getHasError() {
		return (!StringUtils.isBlank(errorMessage));
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public void setCheckDateTime(String value) {
		checkDateTime = value;
	}
	
	public String getCheckDateTime() {
		return checkDateTime;
	}
	
	public void triggerMsgCountService() {
		
		try {
			setSuccessMessage(null);
			setErrorMessage(null);
			mpMsgCountTrigger.triggerMpMsgCount(new DateTime(dateFormat.parse(checkDateTime)));
			setSuccessMessage("Successfully triggered!!");
		} catch (ParseException e) { 
			setErrorMessage("Wrong format of Check Date Time!");
		}				
	}
	
	public void clearScreen() {
		successMessage = null;
		errorMessage = null;
	}
	
	@Remove
	public void destroy() 
	{
	}
}
