package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.util.PropertiesHelper;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("applicationProp")
@Scope(ScopeType.APPLICATION)
public class ApplicationProp {
	
	private static final String APPLICATION_PROPERTIES = "application.properties";
	private static final long RELOAD_INTERVAL = 120000L;
	public static final String MOE_MSG_COUNT_PAT_HOSP_CODE_LIST = "pms.moe-msg-count.pat-hosp-code-list";
	public static final String DISCONTINUE_CLUSTER_CODE_LIST = "pms.discontinue.cluster-code-list";
	private static final String BATCH_DATA_DIR = "batch.data-dir";
	private static final String SFTP_DOWNTIME_HOST = "sftp.downtime.host";
	private static final String SFTP_DOWNTIME_USERNAME = "sftp.downtime.username";
	private static final String SFTP_DOWNTIME_PASSWORD = "sftp.downtime.password";
	private static final String SFTP_DOWNTIME_ROOT_DIR = "sftp.downtime.root-dir";
	private static final String SFTP_PMS_HOST = "sftp.pms.host";
	private static final String SFTP_PMS_USERNAME = "sftp.pms.username";
	private static final String SFTP_PMS_PASSWORD = "sftp.pms.password";
	private static final String SFTP_PMS_ROOT_DIR = "sftp.pms.root-dir";
	private static final String SFTP_FCS_HOST = "sftp.fcs.host";
	private static final String SFTP_FCS_USERNAME = "sftp.fcs.username";
	private static final String SFTP_FCS_PASSWORD = "sftp.fcs.password";
	private static final String SFTP_FCS_ROOT_DIR = "sftp.fcs.root-dir";
	private static final String POST_DAYEND_DIR = "post.dayend-dir";
	private static final String DNLD_TRX_FILE = "dnld-trx.file";
	private static final String DNLD_TRX_DATA_SUB_DIR = "dnld-trx.data-sub-dir";
	private static final String FCS_CHARGING_TRX_FILE = "fcs.charging.trx-file";
	private static final String FCS_UNCOLLECT_TRX_FILE = "fcs.uncollect.trx-file";
	private static final String DH_DISP_CHECKLIST_FILE = "dh.disp.checklist-file";
	private static final String DH_DISP_TRX_FILE = "dh.disp.trx-file";
	private static final String PMS_TRX_DIR = "pms.trx-dir";
	private static final String FCS_CHARGING_TRX_DIR = "fcs.charging.trx-dir";
	private static final String FCS_UNCOLLECT_TRX_DIR = "fcs.uncollect.trx-dir";
	private static final String DH_DISP_TRX_DIR = "dh.disp.trx-dir";
	private static final String FM_RPT_FILE_DELIMITER = "fm-rpt.file.delimiter";
	private static final String FM_RPT_MONTHLY_SUMMARY_FILE = "fm-rpt.monthly.summary.file";
	private static final String FM_RPT_MONTHLY_SUMMARY_SPECIALTY_FILE = "fm-rpt.monthly.summary-specialty.file";
	private static final String FM_RPT_MONTHLY_DETAIL_FILE = "fm-rpt.monthly.detail.file";
	private static final String FM_RPT_YEARLY_SUMMARY_FILE = "fm-rpt.yearly.summary.file";
	private static final String FM_RPT_DATA_SUB_DIR = "fm-rpt.data-sub-dir";
	private static final String DH_DISP_DATA_SUB_DIR = "dh.disp.data-sub-dir";
	private static final String ASP_ROLLOUT = "asp.rollout";
	
	public Properties getProperties() {
		try {
			return PropertiesHelper.getProperties(APPLICATION_PROPERTIES, Thread.currentThread().getContextClassLoader(), RELOAD_INTERVAL);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<String> getMoeMsgCountPatHospCodeList() {
		return Arrays.asList(getProperties().getProperty(MOE_MSG_COUNT_PAT_HOSP_CODE_LIST, "").split(","));
	}		
	
	public PropMap getDiscontinueClusterCodePropMap() {
		List<String> clusterCodeList = Arrays.asList(getProperties().getProperty(DISCONTINUE_CLUSTER_CODE_LIST, "").split(","));
		PropMap propMap = new PropMap();
		for (String clusterCode:clusterCodeList) {
			if (!StringUtils.isEmpty(clusterCode)) {
				propMap.add(StringUtils.trim(clusterCode),"true");
			}
		}
		return propMap;
	}

	public String getBatchDataDir() {
		return getProperties().getProperty(BATCH_DATA_DIR, "");
	}

	public String getSftpDowntimeHost() {
		return getProperties().getProperty(SFTP_DOWNTIME_HOST, "");
	}

	public String getSftpDowntimeUsername() {
		return getProperties().getProperty(SFTP_DOWNTIME_USERNAME, "");
	}

	public String getSftpDowntimePassword() {
		return getProperties().getProperty(SFTP_DOWNTIME_PASSWORD, "");
	}

	public String getSftpDowntimeRootDir() {
		return getProperties().getProperty(SFTP_DOWNTIME_ROOT_DIR, "");
	}

	public String getSftpPmsHost() {
		return getProperties().getProperty(SFTP_PMS_HOST, "");
	}

	public String getSftpPmsUsername() {
		return getProperties().getProperty(SFTP_PMS_USERNAME, "");
	}

	public String getSftpPmsPassword() {
		return getProperties().getProperty(SFTP_PMS_PASSWORD, "");
	}

	public String getSftpPmsRootDir() {
		return getProperties().getProperty(SFTP_PMS_ROOT_DIR, "");
	}

	public String getSftpFcsHost() {
		return getProperties().getProperty(SFTP_FCS_HOST, "");
	}

	public String getSftpFcsUsername() {
		return getProperties().getProperty(SFTP_FCS_USERNAME, "");
	}

	public String getSftpFcsPassword() {
		return getProperties().getProperty(SFTP_FCS_PASSWORD, "");
	}

	public String getSftpFcsRootDir() {
		return getProperties().getProperty(SFTP_FCS_ROOT_DIR, "");
	}

	public String getPostDayendDir() {
		return getProperties().getProperty(POST_DAYEND_DIR, "");
	}

	public String getDnldTrxFile() {
		return getProperties().getProperty(DNLD_TRX_FILE, "");
	}

	public String getDnldTrxDataSubDir() {
		return getProperties().getProperty(DNLD_TRX_DATA_SUB_DIR, "");
	}

	public String getFcsChargingTrxFile() {
		return getProperties().getProperty(FCS_CHARGING_TRX_FILE, "");
	}

	public String getFcsUncollectTrxFile() {
		return getProperties().getProperty(FCS_UNCOLLECT_TRX_FILE, "");
	}

	public String getDhDispChecklistFile() {
		return getProperties().getProperty(DH_DISP_CHECKLIST_FILE, "");
	}
	
	public String getDhDispTrxFile() {
		return getProperties().getProperty(DH_DISP_TRX_FILE, "");
	}

	public String getPmsTrxDir() {
		return getProperties().getProperty(PMS_TRX_DIR, "");
	}

	public String getFcsChargingTrxDir() {
		return getProperties().getProperty(FCS_CHARGING_TRX_DIR, "");
	}

	public String getFcsUncollectTrxDir() {
		return getProperties().getProperty(FCS_UNCOLLECT_TRX_DIR, "");
	}

	public String getDhDispTrxDir() {
		return getProperties().getProperty(DH_DISP_TRX_DIR, "");
	}

	public String getFmRptFileDelimiter() {
		return getProperties().getProperty(FM_RPT_FILE_DELIMITER, "");
	}

	public String getFmRptMonthlySummaryFile() {
		return getProperties().getProperty(FM_RPT_MONTHLY_SUMMARY_FILE, "");
	}

	public String getFmRptMonthlySummarySpecialtyFile() {
		return getProperties().getProperty(FM_RPT_MONTHLY_SUMMARY_SPECIALTY_FILE, "");
	}

	public String getFmRptMonthlyDetailFile() {
		return getProperties().getProperty(FM_RPT_MONTHLY_DETAIL_FILE, "");
	}

	public String getFmRptYearlySummaryFile() {
		return getProperties().getProperty(FM_RPT_YEARLY_SUMMARY_FILE, "");
	}

	public String getFmRptDataSubDir() {
		return getProperties().getProperty(FM_RPT_DATA_SUB_DIR, "");
	}
	
	public String getDhDispDataSubDir() {
		return getProperties().getProperty(DH_DISP_DATA_SUB_DIR, "");
	}

	public boolean isAspRollout() {
		try{
			return Boolean.valueOf(getProperties().getProperty(ASP_ROLLOUT, Boolean.FALSE.toString()));
		} catch (Exception e) {
			return false;
		}
	}
}
