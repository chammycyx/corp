package hk.org.ha.model.pms.corp.persistence.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import hk.org.ha.model.pms.persistence.BaseEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

@Entity
@Table(name = "ORDER_SUBSCRIBE")
public class OrderSubscribe extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderSubscribeSeq")
	@SequenceGenerator(name = "orderSubscribeSeq", sequenceName = "SQ_ORDER_SUBSCRIBE", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ORDER_NUM", nullable = false, length = 12)
	private String orderNum;
	
	@Column(name = "SYSTEM", nullable = false, length = 3)
	private String system;
	
    @Converter(name = "OrderSubscribe.status", converterClass = RecordStatus.Converter.class)
    @Convert("OrderSubscribe.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
}
