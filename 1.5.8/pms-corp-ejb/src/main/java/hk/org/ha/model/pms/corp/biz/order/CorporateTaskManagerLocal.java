package hk.org.ha.model.pms.corp.biz.order;
import java.util.Date;

import javax.ejb.Local;

@Local
public interface CorporateTaskManagerLocal {

	boolean lock(String taskName, Date date);
	
	boolean lock(String taskName, Date date, long graceTime);
}
