package hk.org.ha.model.pms.corp.udt.batch.worker;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DataDirType implements StringValuedEnum {
	Work("work", "Work"),
	In("in","In"),
	Out("out","Out"),
	Done("done", "Done"),
	Error("error","Error");
	
    private final String dataValue;
    private final String displayValue;

    DataDirType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DataDirType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DataDirType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DataDirType> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DataDirType> getEnumClass() {
    		return DataDirType.class;
    	}
    }
}



