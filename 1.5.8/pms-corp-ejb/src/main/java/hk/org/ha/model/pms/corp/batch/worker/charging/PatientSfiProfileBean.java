package hk.org.ha.model.pms.corp.batch.worker.charging;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.vo.charging.PatientSfi;
import hk.org.ha.service.pms.asa.interfaces.AsaServiceJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Stateful
@Scope(ScopeType.SESSION)
@Name("patientSfiProfileBean")
public class PatientSfiProfileBean implements PatientSfiProfileLocal{

	private Logger logger = null;

	@In
	private PatientSfiProfileHelper patientSfiProfileHelper;
	
	@In
	private AsaServiceJmsRemote asaServiceProxy;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
    public void initBatch(BatchWorkerInfo info, Logger logger) {
		this.logger = logger;
	}
	
	public void beginChunk(Chunk arg0) throws Exception {
		
	}
	
	public void processRecord(Record arg0) throws Exception {
    	Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);
        
        List<PatientSfi> patientSfiList = asaServiceProxy.retrievePatientSfi();        
        patientSfiProfileHelper.updatePatientSfiProfile(em, patientSfiList);

		Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	@Remove
	public void destroyBatch() {
		
	}

	public void endChunk() throws Exception {
		
	}

}
