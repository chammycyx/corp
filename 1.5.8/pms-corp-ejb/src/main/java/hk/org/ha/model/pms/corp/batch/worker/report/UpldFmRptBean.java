package hk.org.ha.model.pms.corp.batch.worker.report;

import java.util.Date;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

/**
 * Session Bean implementation class DayendMainBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("upldFmRptBean")
@MeasureCalls
public class UpldFmRptBean implements UpldFmRptLocal {
	
	@In
	private FmRptManagerLocal fmRptManager;
	
	private Logger logger = null;
	
	private Date batchDate;
	private DateTime batchStartDate = null;
	
    /**
     * Default constructor. 
     */
    public UpldFmRptBean() {
        // TODO Auto-generated constructor stub
    }

	public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        } else {
        	batchDate = params.getDate("batchDate");
        	batchStartDate = new DateTime(batchDate).dayOfMonth().withMinimumValue();
        }
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
	}
	
	public void processRecord(Record record) throws Exception {
    	logger.info("Upload Report to server with batch date " + batchStartDate + " start");
    	try {
    		long startTime = fmRptManager.processBeginTime();
    		fmRptManager.uploadReports(batchStartDate);
    		logger.info("Upload FM reports completed. Time used: " + (fmRptManager.totalPorcessTime(startTime)));
    	} catch (Exception e) {
    		logger.info("upload data fail");
    		throw e;
    	}
    }
}