package hk.org.ha.model.pms.corp.biz.medprofile;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.InstructionBuilder;
import hk.org.ha.model.pms.biz.reftable.WorkstoreKeyable;
import hk.org.ha.model.pms.corp.biz.cddh.CorpCddhServiceLocal;
import hk.org.ha.model.pms.corp.biz.order.FcsManagerLocal;
import hk.org.ha.model.pms.corp.biz.order.OrderHelper;
import hk.org.ha.model.pms.corp.biz.order.SeqNumServiceLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.DispTrxType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.service.biz.pms.og.interfaces.OgServiceJmsRemote;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("medProfileManager")
@MeasureCalls
public class MedProfileManagerBean implements MedProfileManagerLocal {

	private static final int DISP_ORDER_FLUSH_LIMIT = 50;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@Logger
	private Log logger;
	
	@In
	private OrderHelper orderHelper;
	
	@In
	private FcsManagerLocal fcsManager;
	
	@In
	private SeqNumServiceLocal seqNumService;
	
	@In
	private OgServiceJmsRemote ogServiceDispatcher;
	
	@In
	private CorpCddhServiceLocal corpCddhService;

	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
	private static final String MP_SFI_INVOICE_NUM = "0000";
	private static final String MP_SFI_INVOICE_DATE_STR = "2000-01-01";

	@Override
	public boolean saveDispOrderListForIp(	List<DispOrder> dispOrderList, 
											Boolean legacyPersistenceEnabled,
											Boolean fcsPersistenceEnabled,
											Boolean directLabelPrintFlag,
											List<Invoice> tempInvoiceList,
											List<Invoice> voidInvoiceList) {
		
		int dispOrderCount = 0;
		int dispOrderItemCount = 0;
		Date today = new Date();
		
		long startTime = System.currentTimeMillis();
		
		List<Long> dispOrderIdList = new ArrayList<Long>();

		// persist new MedCase in every issue to keep the snapshot of MedCase
		for(DispOrder dispOrder:dispOrderList) {
			MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
			MedCase medProfileMedCase = medOrder.getMedCase();
			medProfileMedCase.setId(null);
		}
		
		for(DispOrder dispOrder:dispOrderList) {
			Workstore workstore = orderHelper.retrieveWorkstore(em, dispOrder.getWorkstore());
			
			MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
			
			MedCase medProfileMedCase = medOrder.getMedCase();
			MedCase newMedCase = orderHelper.saveMedCase(em, medProfileMedCase, true);
			
			medOrder.setPatient(orderHelper.savePatient(em, medOrder.getPatient()));
			medOrder.setMedCase(newMedCase);
			medOrder.setWorkstore(workstore);
			em.persist(medOrder);
			
			PharmOrder pharmOrder = dispOrder.getPharmOrder();
			
			String otherDoc = pharmOrder.getOtherDoc();
			pharmOrder.setPatient(orderHelper.savePatient(em, medOrder.getPatient()));
			pharmOrder.setOtherDoc(otherDoc);
			
			pharmOrder.setMedCase(newMedCase);
			pharmOrder.setWorkstore(workstore);
			em.persist(pharmOrder);
			
			dispOrder.setWorkstore(workstore);

			List<Invoice> invoiceList = dispOrder.getInvoiceList();
			if ( dispOrder.getTicket() != null ) { 
				dispOrder.getTicket().setCreateDate(today);
				dispOrder.setTicketCreateDate(today);
				dispOrder.getTicket().setWorkstore(workstore);
				dispOrder.setTicket(orderHelper.saveTicket(em, dispOrder.getTicket(), true));
			} else if ( !invoiceList.isEmpty() || dispOrder.isTpnRequest() ) { //for MpSfiInvoice(private patient)
				Ticket ticket = retrieveMpSfiInvoiceTicket(workstore);
				dispOrder.setTicket(ticket);
				dispOrder.setTicketDate(ticket.getTicketDate());
				dispOrder.setTicketNum(ticket.getTicketNum());
				dispOrder.setTicketCreateDate(ticket.getCreateDate());
			}
			
			Integer dispOrderNum = seqNumService.retrieveDispOrderNum();
			dispOrder.setDispOrderNum(dispOrderNum);
			dispOrder.setOrgDispOrderNum(dispOrderNum);
			
			dispOrder.setPatient(medOrder.getPatient());
			dispOrder.setVetDate(today);			
			
			em.persist(dispOrder);
			
			if ( !invoiceList.isEmpty() ) {
				for ( Invoice invoice : invoiceList ) {
					invoice.setWorkstore(orderHelper.retrieveWorkstore(em, invoice.getWorkstore()));
					em.persist(invoice);
				}
			}
 
			em.flush();
			
			dispOrderCount++;
			dispOrderItemCount += dispOrder.getDispOrderItemList().size();
			if( dispOrderCount%DISP_ORDER_FLUSH_LIMIT == 0 ){
				em.flush();
				logger.info("saveDispOrderListForIp saveDispOrder dispOrderCount:#0 dispOrderItemCount:#1 timeused:#2", DISP_ORDER_FLUSH_LIMIT, dispOrderItemCount, System.currentTimeMillis()-startTime);
				startTime = System.currentTimeMillis();
				dispOrderItemCount = 0;
			}			
		}
		
		em.flush();
		logger.info("saveDispOrderListForIp saveDispOrder dispOrderCount:#0 dispOrderItemCount:#1 timeused:#2", dispOrderList.size()%DISP_ORDER_FLUSH_LIMIT, dispOrderItemCount, System.currentTimeMillis()-startTime);
		startTime = System.currentTimeMillis();
		
		for(DispOrder dispOrder : dispOrderList){
			dispOrderIdList.add(dispOrder.getId());
		}
		
		QueryUtils.splitUpdate(em.createQuery(
				"update DispOrder o" + // 20150206 index check : DispOrder.id : PK_DISP_ORDER
				" set o.orgDispOrderId = o.id" +
				" where o.id in :dispOrderIdList"), "dispOrderIdList", dispOrderIdList);

		logger.info("saveDispOrderListForIp setOrgDispOrderId timeused:#0", System.currentTimeMillis()-startTime);
	
		// set label desc for EPR CREATE/UPDATE and CDDH create
		for (DispOrder dispOrder : dispOrderList) {
			dispOrder.getPharmOrder().loadDmInfo();
			InstructionBuilder builder = InstructionBuilder.instance();
			dispOrder = builder.buildLabelDesc(dispOrder);
		}
		
		// clear all DmInfo before sending to OG and DA
		for (DispOrder dispOrder : dispOrderList) {
			dispOrder.getPharmOrder().clearDmInfo();
		}
		
		for(DispOrder dispOrder:dispOrderList) {
			if ( dispOrder.getStatus() != DispOrderStatus.SysDeleted ) { //skip MpSfiInvoice(private patient) order
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispCreate, dispOrder);
			}
		}
		
		if (legacyPersistenceEnabled) {
			if (logger.isDebugEnabled()) {
				logger.debug("saveDispOrderListForIp: to be persist into legacy CDDH.");
			}
			corpCddhService.createLegacyCddhRecordForIp(dispOrderList);
		}
		
		//FCS Handling
		if( fcsPersistenceEnabled ){
			List<Invoice> newInvoiceList = new ArrayList<Invoice>();			
			for( DispOrder dispOrder : dispOrderList ){
				if( dispOrder.getInvoiceList() != null ){
					newInvoiceList.addAll(dispOrder.getInvoiceList());
				}
			}
			
			if( tempInvoiceList != null ){
				newInvoiceList.addAll(tempInvoiceList);
			}
			
			fcsManager.saveFcsSfiInvoiceListForIp(newInvoiceList, directLabelPrintFlag);

			if( voidInvoiceList != null ){
				List<String> invoiceNumList = new ArrayList<String>();
				for( Invoice voidInvoice : voidInvoiceList ){
					invoiceNumList.add(voidInvoice.getInvoiceNum());
				}
				retrieveUpdateVoidInvoiceList(invoiceNumList);
				
				fcsManager.voidFcsSfiInvoice(voidInvoiceList);
			}
		}
		
		return false;
	}

	public boolean updateDispOrderItemStatusToDeletedByTpnRequestIdList(List<Long> tpnRequestIdList, Boolean legacyPersistenceEnabled){
		return updateDispOrderItemStatusToDeleted(tpnRequestIdList, legacyPersistenceEnabled, true);
	}

	public boolean updateDispOrderItemStatusToDeleted(List<Long> deliveryItemIdList, Boolean legacyPersistenceEnabled){
		return updateDispOrderItemStatusToDeleted(deliveryItemIdList, legacyPersistenceEnabled, false);
	}

	@SuppressWarnings("unchecked")
	private boolean updateDispOrderItemStatusToDeleted(List<Long> idList, Boolean legacyPersistenceEnabled, boolean isTpnFlag) {
		
		String idColumnName = isTpnFlag ? "tpnRequestId" : "deliveryItemId";
		
		List<Long> dispOrderIdList = em.createQuery(
				"select distinct o.dispOrder.id from DispOrderItem o" + // 20160616 index check : DispOrderItem.deliveryItemId DispOrderItem.tpnRequestId : I_DISP_ORDER_ITEM_01 I_DISP_ORDER_ITEM_02
				" where o." + idColumnName + " in :idList" +
				" and o.dispOrder.status = :status")
				.setParameter("idList", idList)
				.setParameter("status", DispOrderStatus.Issued)
				.getResultList();
		
		if ( !dispOrderIdList.isEmpty() ) {
			
			List<DispOrder> dispOrderList = em.createQuery(
					"select o from DispOrder o" + // 20120214 index check : DispOrder.id : PK_DISP_ORDER
					" where o.id in :dispOrderIdList")
					.setParameter("dispOrderIdList", dispOrderIdList)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
					.getResultList();
			
			for ( DispOrder dispOrder : dispOrderList ) {
				if ( dispOrder.getDayEndStatus() != DispOrderDayEndStatus.None ||
						dispOrder.getBatchProcessingFlag() ) {
					return false;
				}
			}
			
			markDispOrderItemDelete(dispOrderList, idList, isTpnFlag);
			
			// mark disp order to delete if all items are deleted  
			Pair<List<DispOrder>, List<Invoice>> deletedDispOrderPair = markDispOrderDelete(dispOrderList);
			List<DispOrder> deletedDispOrderList = deletedDispOrderPair.getFirst();
			List<Invoice> voidInvoiceList = deletedDispOrderPair.getSecond();
			
			em.flush();
			
			// get disp order for update which there is still some undeleted items  
			List<DispOrder> updatePrevDispOrderList = retrieveUpdatePrevDispOrderList(dispOrderList, deletedDispOrderList);
			
			List<DispOrder> updateNewDispOrderList = retrieveUpdateNewDispOrderList(updatePrevDispOrderList);

			saveUpdateDispOrderList(updateNewDispOrderList);

			for (DispOrder updatePrevDispOrder : updatePrevDispOrderList) {
				updatePrevDispOrder.markSysDeleted();
			}
			
			em.flush();

			for (DispOrder deletedDispOrder : deletedDispOrderList) {
				deletedDispOrder.loadHeader();
			}
			
			// for updatePrevDispOrderList and updateNewDispOrderList, dispOrder.loadChild is called
			// in retrieveUpdateNewDispOrderList(), so no need to call deletedDispOrder.loadHeader()
			
			// set label desc for EPR CREATE/UPDATE and CDDH create
			for (DispOrder updateNewDispOrder : updateNewDispOrderList) {
				updateNewDispOrder.getPharmOrder().loadDmInfo();
				InstructionBuilder builder = InstructionBuilder.instance();
				updateNewDispOrder = builder.buildLabelDesc(updateNewDispOrder);
			}			
			
			// clear all DmInfo before sending to OG and DA
			for (DispOrder deletedDispOrder : deletedDispOrderList) {
				deletedDispOrder.getPharmOrder().clearDmInfo();
			}
			for (DispOrder updatePrevDispOrder : updatePrevDispOrderList) {
				updatePrevDispOrder.getPharmOrder().clearDmInfo();
			}
			for (DispOrder updateNewDispOrder : updateNewDispOrderList) {
				updateNewDispOrder.getPharmOrder().clearDmInfo();
			}
			
			// update CDDH and send EPR message
			for (DispOrder deletedDispOrder : deletedDispOrderList) {
				if (legacyPersistenceEnabled){
					corpCddhService.removeLegacyCddhDispOrder(deletedDispOrder);
				}
										
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispDelete, deletedDispOrder);
			}

			for (DispOrder updatePrevDispOrder : updatePrevDispOrderList) {
				if (legacyPersistenceEnabled){
					corpCddhService.removeLegacyCddhDispOrder(updatePrevDispOrder);
				}
			}
			
			for (DispOrder updateNewDispOrder : updateNewDispOrderList) {
				if (legacyPersistenceEnabled) {
					corpCddhService.createLegacyCddhRecord(updateNewDispOrder);
				}
				
				ogServiceDispatcher.sendOrderToEpr(DispTrxType.DispUpdate, updateNewDispOrder);
			}
			
			fcsManager.voidFcsSfiInvoice(voidInvoiceList);
			
			return true;
			
		} else {
			return false;
		}		
	}
	
	public List<Invoice>  retrieveUpdateVoidInvoiceList(List<String> invoiceNumList){
		if ( invoiceNumList.isEmpty() ) {
			return new ArrayList<Invoice>();
		}
		
		List<Invoice> dbVoidInvoiceList = retrieveActiveInvoiceByInvoiceNum(invoiceNumList);
		
		for( Invoice dbInvoice : dbVoidInvoiceList){
			dbInvoice.setStatus(InvoiceStatus.Void);
		}
		return dbVoidInvoiceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Invoice> retrieveActiveInvoiceByInvoiceNum(List<String> invoiceNumList) {
		return em.createQuery(
				"select o from Invoice o" + // 20150422 index check : Invoice.invoiceNum : I_INVOICE_01
				" where o.invoiceNum in :invoiceNumList" +
				" and o.status in :statusList")
				.setParameter("invoiceNumList", invoiceNumList)
				.setParameter("statusList", InvoiceStatus.Outstanding_Settle)
				.getResultList();
	}
	
	private void markDispOrderItemDelete(List<DispOrder> dispOrderList, List<Long> idList, boolean isTpnFlag) {
		for ( DispOrder dispOrder : dispOrderList ) {
			for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				Long id = isTpnFlag ? dispOrderItem.getTpnRequestId() : dispOrderItem.getDeliveryItemId();
				if ( idList.contains(id) ) {
					dispOrderItem.setStatus(DispOrderItemStatus.Deleted);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Pair<List<DispOrder>,List<Invoice>> markDispOrderDelete(List<DispOrder> dispOrderList) {
		List<DispOrder> deletedDispOrderList = new ArrayList<DispOrder>();				
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>(); 
		
		for (DispOrder dispOrder : dispOrderList) {
			boolean markDeleted = true;
			for ( DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList() ) {
				if ( dispOrderItem.getStatus() != DispOrderItemStatus.Deleted){
					markDeleted = false;
					break;
				}
			}
			
			if (markDeleted) {
				voidInvoiceList.addAll(dispOrder.markDeleted());
				deletedDispOrderList.add(dispOrder);
			}
		}
		
		return new Pair(deletedDispOrderList, voidInvoiceList);
	}
	
	private List<DispOrder> retrieveUpdatePrevDispOrderList(List<DispOrder> dispOrderList, List<DispOrder> deletedDispOrderList) {
		Map<Long, DispOrder> deletedDispOrderMap = new HashMap<Long, DispOrder>();
		for (DispOrder deletedDispOrder : deletedDispOrderList) {			
			deletedDispOrderMap.put(deletedDispOrder.getId(), deletedDispOrder);
		}

		List<DispOrder> prevDispOrderList = new ArrayList<DispOrder>();
		for (DispOrder dispOrder : dispOrderList) {
			if ( ! deletedDispOrderMap.containsKey(dispOrder.getId())) {
				prevDispOrderList.add(dispOrder);
			}
		}
		
		return prevDispOrderList;
	}

	private List<DispOrder> retrieveUpdateNewDispOrderList(List<DispOrder> prevDispOrderList) {
		List<DispOrder> newDispOrderList = new ArrayList<DispOrder>(); 
		
		for (DispOrder prevDispOrder : prevDispOrderList) {
			prevDispOrder.loadChild();
			prevDispOrder.getPatient();
			
			String newDispOrderXml = new EclipseLinkXStreamMarshaller().toXML(prevDispOrder);
			DispOrder newDispOrder = (DispOrder)new EclipseLinkXStreamMarshaller().fromXML(newDispOrderXml);
			
			newDispOrderList.add(newDispOrder);
		}
		
		return newDispOrderList;
	}
	
	private void saveUpdateDispOrderList(List<DispOrder> newDispOrderList) {
		for (DispOrder newDispOrder : newDispOrderList) {
			newDispOrder.setPrevDispOrderId(newDispOrder.getId());
			
			newDispOrder.setPrevDispOrderNum(newDispOrder.getDispOrderNum());
			
			Integer dispOrderNum = seqNumService.retrieveDispOrderNum();
			newDispOrder.setDispOrderNum(dispOrderNum);			
			
			newDispOrder.clearId();

			for (Iterator<DispOrderItem> itr = newDispOrder.getDispOrderItemList().iterator(); itr.hasNext();) {
				DispOrderItem newDispOrderItem = itr.next();
				if (newDispOrderItem.getStatus() == DispOrderItemStatus.Deleted) {
					itr.remove();
				}
			}
			
			em.persist(newDispOrder);
		}
	}
	
	public String retrieveChargeOrderNum(String hospCode, String patHospCode){
		return seqNumService.retrieveChargeOrderNum(hospCode, patHospCode);
	}
	
	@SuppressWarnings("unchecked")
	@CacheResult(timeout="3600000", keyable=WorkstoreKeyable.class)
	private Ticket retrieveMpSfiInvoiceTicket(Workstore workstore){
		Ticket ticket = null;
		List<Ticket> ticketList;
		try {
			ticketList = em.createQuery(
					"select o from Ticket o" + // 20150422 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
					" where o.workstore = :workstore" +
					" and o.orderType = :orderType" +
					" and o.ticketDate = :ticketDate" +
					" and o.ticketNum = :ticketNum")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("ticketNum", MP_SFI_INVOICE_NUM)
					.setParameter("ticketDate", df.parse(MP_SFI_INVOICE_DATE_STR), TemporalType.DATE)
					.getResultList();

			
			if ( !ticketList.isEmpty() ) {
				ticket = ticketList.get(0);
			} else {
				logger.info("retrieveMpSfiInvoiceTicket ticket not found hospCode:#0 workstoreCode:#1", workstore.getHospCode(), workstore.getWorkstoreCode());
			}
		} catch (ParseException e) {
			logger.error("Invalid date format #0", MP_SFI_INVOICE_DATE_STR);
		}
		
		return ticket;
	}
}
