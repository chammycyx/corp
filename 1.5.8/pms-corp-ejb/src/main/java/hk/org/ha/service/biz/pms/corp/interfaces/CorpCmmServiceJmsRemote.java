package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;


public interface CorpCmmServiceJmsRemote {
	
	String subscribeMedOrder(String orderNum);
	
	Boolean unsubscribeMedOrder(String orderNum);

	String viewMedOrder(String orderNum);
	
	MedCase saveMedCase(MedCase remoteMedCase);
	
	Patient savePatient(Patient remotePatient);
}
