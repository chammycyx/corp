package hk.org.ha.model.pms.corp.biz.cddh;

import javax.ejb.Local;

@Local
public interface CddhSpecialtyMappingLocal {

	String getUserName();
	void setUserName(String userName);

	String getHospCode();
	void setHospCode(String hospCode);

	String getSpecCode();
	void setSpecCode(String specCode);

	String getCddhSpecCode();
	void setCddhSpecCode(String cddhSpecCode);
	
	boolean getNewRecord();
	void setNewRecord(boolean newRecord);
	
	boolean getSaveComplete();
	void setSaveComplete(boolean saveComplete);
	
	boolean getIsSuccess();
	String getSuccessMessage();
	void setSuccessMessage(String successMessage);
	
	boolean getHasError();
	String getErrorMessage();
	void setErrorMessage(String errorMessage);

	void retrieveCddhSpecialtyMapping();

	void updateCddhSpecialtyMapping();
		
	void clearScreen();
	
	void destroy();	
}
