package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmSupplSiteCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmSupplSiteCacher extends BaseCacher implements DmSupplSiteCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
	
    public DmSupplSite getSupplSiteBySiteCodeSupplSiteEng(String siteCode, String supplSiteEng) {
    	//TODO : SupplSite use composite key
    	String supplSiteKey = siteCode + supplSiteEng;
		return (DmSupplSite) this.getCacher().get(supplSiteKey);
	}

	public List<DmSupplSite> getSupplSiteList() {
		return (List<DmSupplSite>) this.getCacher().getAll();
	}

	public List<DmSupplSite> getSupplSiteListBySiteCode(String prefixSiteCode) {
		List<DmSupplSite> ret = new ArrayList<DmSupplSite>();
		for (DmSupplSite dmSupplSite : getSupplSiteList()){
			if(dmSupplSite.getCompId().getSiteCode().equalsIgnoreCase(prefixSiteCode)){
				ret.add(dmSupplSite);
			}
		}
		return ret;
	}
	
	public List<DmSupplSite> getSupplSiteListBySupplSiteEng(String prefixSupplSiteEng) {
		List<DmSupplSite> ret = new ArrayList<DmSupplSite>();
		boolean found = false;
		for (DmSupplSite dmSupplSite : getSupplSiteList()) {
			if (dmSupplSite.getCompId().getSupplSiteEng().startsWith(prefixSupplSiteEng)) {
				ret.add(dmSupplSite);
				found = true;
			} else {
				if (found) {
					break;
				}
			}
		}
		return ret;
	}

	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmSupplSite> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmSupplSite create(String supplSiteKey) {
			this.getAll();
			return this.internalGet(supplSiteKey);
		}

		@Override
		public Collection<DmSupplSite> createAll() {
			return dmsPmsServiceProxy.retrieveDmSupplSiteList();
		}

		@Override
		public String retrieveKey(DmSupplSite dmSupplSite) {
			return dmSupplSite.getCompId().getSiteCode() + dmSupplSite.getCompId().getSupplSiteEng();
		}
	}

	public static DmSupplSiteCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmSupplSiteCacherInf) Component.getInstance("dmSupplSiteCacher",
				ScopeType.APPLICATION);
	}
}
