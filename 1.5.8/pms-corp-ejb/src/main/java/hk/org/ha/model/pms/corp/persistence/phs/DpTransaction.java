package hk.org.ha.model.pms.corp.persistence.phs;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;
import hk.org.ha.fmk.pms.entity.UpdateDate;
import hk.org.ha.fmk.pms.entity.UpdateUser;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


@Entity
@Table(name = "dp_transaction")
public class DpTransaction {
    @Id
    @Column(name = "transaction_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dpTransactionSeq")
    @SequenceGenerator(name="dpTransactionSeq", sequenceName="SQ_DP_TRANSACTION")
    private Long transactionId;
    
    @Column(name = "institution_code", nullable = false)
    private String institutionCode;
    
    @Column(name = "location_code", nullable = false)
    private String locationCode;
    
    @Column(name = "pms_hosp_code", nullable = false)
    private String pmsHospCode;
    
    @Column(name = "pms_work_store", nullable = false)
    private String pmsWorkStore;
    
    @Column(name = "dispense_date", nullable = false)
	@Temporal(TemporalType.DATE)
    private Date dispenseDate;
    
    @Column(name = "dispense_ticket_num", nullable = false)
    private String dispenseTicketNum;
    
    @Column(name = "dispense_order_num", nullable = false)
    private Integer dispenseOrderNum;
    
    @Column(name = "dispense_line_num", nullable = false)
    private Integer dispenseLineNum;
    
    @Column(name = "item_code", nullable = false)
    private String itemCode;
    
    @Column(name = "dispense_qty", nullable = false)
    private Integer dispenseQty;
    
    @Column(name = "pat_type", nullable = false)
    private String patType;
    
    @Column(name = "dispense_status", nullable = false)
    private String dispenseStatus;
    
    @Column(name = "ward_code")
    private String wardCode;

    @Column(name = "specialty_code")
    private String specialtyCode;
    
    @Column(name = "duration_in_days")
    private Integer durationInDays;
    
    @Column(name = "pat_cat_code")
    private String patCatCode;
    
    @Column(name = "pat_name")
    private String patName;
    
    @Column(name = "pat_id")
    private String patId;
    
    @Column(name = "case_num", nullable = false)
    private String caseNum;
    
    @Column(name = "passport_num")
    private String passportNum;
    
    @Column(name = "mo_code")
    private String moCode;
    
    @Column(name = "transaction_date")
	@Temporal(TemporalType.DATE)
    private Date transactionDate;
    
    @Column(name = "unit_price")
    private Double unitPrice;
    
    @Column(name = "commodity_group")
    private String commodityGroup;
    
    @Column(name = "commodity_type")
    private String commodityType;
    
    @Column(name = "specialty_percentage", nullable = false)
    private Integer specialtyPercentage;
    
    @Column(name = "px_counted_flag", nullable = false)
    private String pxCountedFlag;
    
    @Column(name = "split_consumption_flag", nullable = false)
    private String splitConsumptionFlag;
    
    @Column(name = "exception_update_flag", nullable = false)
    private String exceptionUpdateFlag;
    
    @Column(name = "exception_code", nullable = false)
    private String exceptionCode;
    
    @Column(name = "record_status", nullable = false)
    private String recordStatus;
    
    @Column(name = "stockable_item")
    private String stockableItem;
    
    @Column(name = "suspend")
    private String suspend;
    
    @Column(name = "dangerous_drug")
    private String dangerousDrug;
    
    @Column(name = "stock_week_count")
    private Integer stockWeekCount;

    @Column(name = "infile_batch_num")
    private String infileBatchNum;

    @Column(name = "process_batch_num")
    private String processBatchNum;
    
    @Column(name = "sfi_flag")
    private String sfiFlag;
    
    @Column(name = "charging_institution_code")
    private String chargingInstitutionCode;
    
    @Column(name = "coa_section")
    private String coaSection;
    
    @Column(name = "coa_analytical")
    private String coaAnalytical;
    
    @Column(name = "group_id")
    private Long groupId;
    
 	@Column(name = "interface_id")
 	private String interfaceId;

	@CreateUser
	@Column(name = "create_user", nullable = false, length = 12)
	public String createUser; 
    
	@CreateDate
	@Column(name = "create_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date createDate;

	@UpdateUser
	@Column(name = "update_user", nullable = false, length = 12)
	public String updateUser;
    
	@UpdateDate
	@Column(name = "update_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date updateDate;

    @Version
    @Column(name = "update_version", nullable = false)
    private Integer updateVersion;

    public DpTransaction() {
    }

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPmsHospCode() {
		return pmsHospCode;
	}

	public void setPmsHospCode(String pmsHospCode) {
		this.pmsHospCode = pmsHospCode;
	}

	public String getPmsWorkStore() {
		return pmsWorkStore;
	}

	public void setPmsWorkStore(String pmsWorkStore) {
		this.pmsWorkStore = pmsWorkStore;
	}

	public Date getDispenseDate() {
		return dispenseDate;
	}

	public void setDispenseDate(Date dispenseDate) {
		this.dispenseDate = dispenseDate;
	}

	public String getDispenseTicketNum() {
		return dispenseTicketNum;
	}

	public void setDispenseTicketNum(String dispenseTicketNum) {
		this.dispenseTicketNum = dispenseTicketNum;
	}

	public Integer getDispenseOrderNum() {
		return dispenseOrderNum;
	}

	public void setDispenseOrderNum(Integer dispenseOrderNum) {
		this.dispenseOrderNum = dispenseOrderNum;
	}

	public Integer getDispenseLineNum() {
		return dispenseLineNum;
	}

	public void setDispenseLineNum(Integer dispenseLineNum) {
		this.dispenseLineNum = dispenseLineNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getDispenseQty() {
		return dispenseQty;
	}

	public void setDispenseQty(Integer dispenseQty) {
		this.dispenseQty = dispenseQty;
	}

	public String getPatType() {
		return patType;
	}

	public void setPatType(String patType) {
		this.patType = patType;
	}

	public String getDispenseStatus() {
		return dispenseStatus;
	}

	public void setDispenseStatus(String dispenseStatus) {
		this.dispenseStatus = dispenseStatus;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getSpecialtyCode() {
		return specialtyCode;
	}

	public void setSpecialtyCode(String specialtyCode) {
		this.specialtyCode = specialtyCode;
	}

	public Integer getDurationInDays() {
		return durationInDays;
	}

	public void setDurationInDays(Integer durationInDays) {
		this.durationInDays = durationInDays;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatId() {
		return patId;
	}

	public void setPatId(String patId) {
		this.patId = patId;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPassportNum() {
		return passportNum;
	}

	public void setPassportNum(String passportNum) {
		this.passportNum = passportNum;
	}

	public String getMoCode() {
		return moCode;
	}

	public void setMoCode(String moCode) {
		this.moCode = moCode;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getCommodityGroup() {
		return commodityGroup;
	}

	public void setCommodityGroup(String commodityGroup) {
		this.commodityGroup = commodityGroup;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public Integer getSpecialtyPercentage() {
		return specialtyPercentage;
	}

	public void setSpecialtyPercentage(Integer specialtyPercentage) {
		this.specialtyPercentage = specialtyPercentage;
	}

	public String getPxCountedFlag() {
		return pxCountedFlag;
	}

	public void setPxCountedFlag(String pxCountedFlag) {
		this.pxCountedFlag = pxCountedFlag;
	}

	public String getSplitConsumptionFlag() {
		return splitConsumptionFlag;
	}

	public void setSplitConsumptionFlag(String splitConsumptionFlag) {
		this.splitConsumptionFlag = splitConsumptionFlag;
	}

	public String getExceptionUpdateFlag() {
		return exceptionUpdateFlag;
	}

	public void setExceptionUpdateFlag(String exceptionUpdateFlag) {
		this.exceptionUpdateFlag = exceptionUpdateFlag;
	}

	public String getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public String getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getStockableItem() {
		return stockableItem;
	}

	public void setStockableItem(String stockableItem) {
		this.stockableItem = stockableItem;
	}

	public String getSuspend() {
		return suspend;
	}

	public void setSuspend(String suspend) {
		this.suspend = suspend;
	}

	public String getDangerousDrug() {
		return dangerousDrug;
	}

	public void setDangerousDrug(String dangerousDrug) {
		this.dangerousDrug = dangerousDrug;
	}

	public Integer getStockWeekCount() {
		return stockWeekCount;
	}

	public void setStockWeekCount(Integer stockWeekCount) {
		this.stockWeekCount = stockWeekCount;
	}

	public String getInfileBatchNum() {
		return infileBatchNum;
	}

	public void setInfileBatchNum(String infileBatchNum) {
		this.infileBatchNum = infileBatchNum;
	}

	public String getProcessBatchNum() {
		return processBatchNum;
	}

	public void setProcessBatchNum(String processBatchNum) {
		this.processBatchNum = processBatchNum;
	}

	public String getSfiFlag() {
		return sfiFlag;
	}

	public void setSfiFlag(String sfiFlag) {
		this.sfiFlag = sfiFlag;
	}

	public String getChargingInstitutionCode() {
		return chargingInstitutionCode;
	}

	public void setChargingInstitutionCode(String chargingInstitutionCode) {
		this.chargingInstitutionCode = chargingInstitutionCode;
	}

	public String getCoaSection() {
		return coaSection;
	}

	public void setCoaSection(String coaSection) {
		this.coaSection = coaSection;
	}

	public String getCoaAnalytical() {
		return coaAnalytical;
	}

	public void setCoaAnalytical(String coaAnalytical) {
		this.coaAnalytical = coaAnalytical;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}

	public Integer getUpdateVersion() {
		return updateVersion;
	}

	public void setUpdateVersion(Integer updateVersion) {
		this.updateVersion = updateVersion;
	}
}
