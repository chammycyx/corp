package hk.org.ha.model.pms.corp.biz.order;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;

import javax.ejb.Local;

@Local
public interface OrderManagerLocal  {

	MedOrder retrieveMedOrder(String orderNum);

	MedOrder retrieveMedOrder(String orderNum, List<MedOrderStatus> excludeStatusList);
	
	void lockMedOrder(String orderNum);
	
	void lockMedOrder(Long medOrderId);
	
	PharmOrder retrievePharmOrder(String orderNum);
	
	Workstore retrieveWorkstore(Workstore workstore);
	
	Ticket saveTicket(Ticket remoteTicket);

	MedCase saveMedCase(MedCase remoteMedCase);

	Patient savePatient(Patient remotePatient);
	
	MedOrder insertMedOrder(MedOrder medOrder);

	MedOrder insertMedOrderByUnvet(MedOrder medOrder);
	
	List<DispOrder> markOrderSysDeleted(PharmOrder pharmOrder, Long prevPharmOrderId, Boolean remarkFlag);
	
	void loadPharmOrderList(MedOrder medOrder);
	
	void loadDispOrderList(PharmOrder pharmOrder);

	void loadOrderList(MedOrder medOrder);

	List<MedOrderItem> retrieveMedOrderItemByItemNum(String orderNum, List<Integer> medOrderItemNumList);
	
	Set<DispOrder> retrieveDispOrderList(List<Invoice> voidInvoiceList);
	
	List<Invoice> markSfiInvoiceSysDeleted(List<Invoice> voidInvoiceList, List<Invoice> invoiceList);
	
	void updateEndVettingOldRemarkItemStatus(PharmOrder pharmOrder, Long prevPharmOrderId, Boolean remarkFlag);
	
	MedOrder saveMedOrder(MedOrder medOrder, 
			Boolean remarkFlag, 
			String hospCode,
			String patHospCode);
	
	void savePharmOrder(MedOrder medOrder, 
			PharmOrder pharmOrder, 
			Map<Integer, Integer> itemMap);
	
	// save DispOrder and Invoice
	void saveDispOrder(PharmOrder pharmOrder,
			DispOrder dispOrder,
			DispOrder prevDispOrder);
	
	// save CapdVoucher
	void saveCapdVoucher(DispOrder dispOrder);

	// update MedOrder
	MedOrder updateMedOrderStatus(MedOrder medOrder, Date prevVetDate);
	
	String retrieveClusterCodeByPatHospCode(String patHospCode);
	
	List<DispOrder> retrieveDispOrderListByIdList(List<Long> dispOrderIdList);	
}
