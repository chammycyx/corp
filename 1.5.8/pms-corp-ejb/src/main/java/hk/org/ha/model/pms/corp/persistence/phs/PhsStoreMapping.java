package hk.org.ha.model.pms.corp.persistence.phs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "phs_store_mapping")
public class PhsStoreMapping implements Serializable {
    @Id
    @Column(name="institution_code", nullable = false)
    private String institutionCode;
    
    @Id
    @Column(name="location_code", nullable = false)
    private String locationCode;
    
    @Id
    @Column(name="pms_hosp_code", nullable = false)
    private String pmsHospCode;
    
    @Id
    @Column(name="pms_work_store", nullable = false)
    private String pmsWorkStore;

    @Column(name="trx_file", nullable = false)
    private String trxFile;

    @Column(name="trx_header_file", nullable = false)
    private String trxHeaderFile;

    @Column(name="suspend", nullable = false)
    private String suspend;

    public PhsStoreMapping() {
    }

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPmsHospCode() {
		return pmsHospCode;
	}

	public void setPmsHospCode(String pmsHospCode) {
		this.pmsHospCode = pmsHospCode;
	}

	public String getPmsWorkStore() {
		return pmsWorkStore;
	}

	public void setPmsWorkStore(String pmsWorkStore) {
		this.pmsWorkStore = pmsWorkStore;
	}

	public String getTrxFile() {
		return trxFile;
	}

	public void setTrxFile(String trxFile) {
		this.trxFile = trxFile;
	}

	public String getTrxHeaderFile() {
		return trxHeaderFile;
	}

	public void setTrxHeaderFile(String trxHeaderFile) {
		this.trxHeaderFile = trxHeaderFile;
	}

	public String getSuspend() {
		return suspend;
	}

	public void setSuspend(String suspend) {
		this.suspend = suspend;
	}
}
