package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.service.biz.pms.corp.interfaces.CorpSubscriberJmsRemote;

import javax.ejb.Local;

@Local
public interface CorpSubscriberLocal extends CorpSubscriberJmsRemote {
}
