package hk.org.ha.model.pms.corp.persistence.charging;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.vo.charging.PatientSfi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PATIENT_SFI_PROFILE")
public class PatientSfiProfile extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	private static final String JAXB_CONTEXT_PATIENT_SFI = "hk.org.ha.model.pms.vo.charging";

	@Id
	@Column(name = "HKID")
	private String hkid;
	
	@Lob
	@Column(name = "SFI_XML")
	private String sfiXml;
	
	@Transient
	private PatientSfi patientSfi;

	@PostLoad
    public void postLoad() {
		if ( patientSfi == null && sfiXml != null ) {
			JaxbWrapper<PatientSfi> parientSfiJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_PATIENT_SFI);
			patientSfi = parientSfiJaxbWrapper.unmarshall(sfiXml);
		}
	}
	
	@PrePersist
	@PreUpdate
	public void preSave() {
		if (patientSfi != null) {
			JaxbWrapper<PatientSfi> parientSfiJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_PATIENT_SFI);
			sfiXml = parientSfiJaxbWrapper.marshall(patientSfi);
		}
	}
	
	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getSfiXml() {
		return sfiXml;
	}

	public void setSfiXml(String sfiXml) {
		this.sfiXml = sfiXml;
	}

	public PatientSfi getPatientSfi() {
		return patientSfi;
	}

	public void setPatientSfi(PatientSfi patientSfi) {
		this.patientSfi = patientSfi;
	}
}
