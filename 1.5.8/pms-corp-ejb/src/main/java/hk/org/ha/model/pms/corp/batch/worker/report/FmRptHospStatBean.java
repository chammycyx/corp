package hk.org.ha.model.pms.corp.batch.worker.report;

import java.util.Arrays;
import java.util.Date;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.report.FmReportType;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

/**
 * Session Bean implementation class DayendMainBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("fmRptHospStatBean")
@MeasureCalls
public class FmRptHospStatBean implements FmRptHospStatLocal {
		
	@In
	private FmRptManagerLocal fmRptManager;
	
	@In
	private FmRptFilesManagerLocal fmRptFilesManager;
		
	private Logger logger = null;
	private String jobId = null;;
	private String patHospCode = null;
	private DateTime startDate = null;
	
	private Date batchDate;
	private DateTime batchStartDate = null;
	
    /**
     * Default constructor. 
     */
    public FmRptHospStatBean() {
        // TODO Auto-generated constructor stub
    }

	public void beginChunk(Chunk arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void endChunk() throws Exception {
		
	}
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
            throw new Exception("batch parameter : batchDate is null");
        } else {
        	batchDate = params.getDate("batchDate");
        	batchStartDate = new DateTime(batchDate).dayOfMonth().withMinimumValue();
        }
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
	
	public void processRecord(Record record) throws Exception {
    	patHospCode = record.getString("PAT_HOSP_CODE");
    	startDate = new DateTime();
    	logger.info("Generate Hospital Statistic Report for " + patHospCode + " with batch date " + batchStartDate + " start ");
    	long startTime;
    	long generateMonthlyHospSpecStatReportTime;
    	long generateMonthlyHospStatReportTime;
    	long generateYearlyAccumulatedHospStatReportTime;
    	
    	
    	try {
    		startTime = fmRptManager.processBeginTime();
    		fmRptManager.generateMonthlyHospSpecStatReport(batchStartDate, patHospCode, jobId);
    		generateMonthlyHospSpecStatReportTime = fmRptManager.totalPorcessTime(startTime);
	    	
    		startTime = fmRptManager.processBeginTime();
    		fmRptManager.generateMonthlyHospStatReport(batchStartDate, patHospCode, jobId);
    		generateMonthlyHospStatReportTime = fmRptManager.totalPorcessTime(startTime);
    		 
    		startTime = fmRptManager.processBeginTime();
    		String clusterCode = fmRptManager.generateYearlyAccumulatedHospStatReport(batchStartDate, patHospCode, jobId);
    		generateYearlyAccumulatedHospStatReportTime = fmRptManager.totalPorcessTime(startTime);
    		
    		fmRptFilesManager.moveToDestDirectory(batchStartDate, patHospCode, clusterCode, jobId, startDate,
	    									 	  Arrays.asList(FmReportType.MonthlyHospSpecStat, FmReportType.MonthlyHospStat, 
	    									 			    	FmReportType.YearlyAccumulatedHospStat));
    		
    		logger.info("Generate hospital statistic report for patHosp " + patHospCode + " completed" +
    				"\n1. generateMonthlyHospSpecStatReportTime time used: " + generateMonthlyHospSpecStatReportTime +
    				"\n2. generateMonthlyHospStatReport time used: " + generateMonthlyHospStatReportTime + 
    				"\n3. generateYearlyAccumulatedHospStatReport time used: " + generateYearlyAccumulatedHospStatReportTime);
    		
    	} catch (Exception e) {
    		logger.info(jobId + " with Hospital " + patHospCode + " Fail. Move files to error folder");
    		fmRptFilesManager.moveToErrorDirectory(jobId, startDate);
    		throw e;
		}
    }
}
