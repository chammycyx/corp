package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.order.Ownership;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("ownershipService")
@MeasureCalls
public class OwnershipServiceBean implements OwnershipServiceLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	public Boolean requestOwnership(String orderNum, String hospCode) {

		// grant ownership
		Ownership ownership = this.retrieveAndLockOwnership(orderNum);
		if (ownership == null || (ownership.getOwner() != null && !ownership.getOwner().equals(hospCode))) {
			return Boolean.FALSE;
		}

		// update ownership
		ownership.setOwner(hospCode);
		return Boolean.TRUE;
	}

	public Boolean releaseOwnership(String orderNum, String hospCode) {

		Ownership ownership = this.retrieveAndLockOwnership(orderNum);
		if (ownership == null || !hospCode.equals(ownership.getOwner())) {
			return Boolean.FALSE;
		}

		// update ownership
		ownership.setOwner(null);
		return Boolean.TRUE;
	}

	@SuppressWarnings("unchecked")
	public Ownership retrieveAndLockOwnership(String orderNum) {
		
		List<Ownership> list = em.createQuery(
				"select o from Ownership o" + // 20120225 index check : Ownership.orderNum : UI_OWNERSHIP_01
				" where o.orderNum = :orderNum")
				.setParameter("orderNum", orderNum)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		if (!list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Ownership retrieveOwnership(String orderNum) {
		
		List<Ownership> list = em.createQuery(
				"select o from Ownership o" + // 20120225 index check : Ownership.orderNum : UI_OWNERSHIP_01
				" where o.orderNum = :orderNum")
				.setParameter("orderNum", orderNum)
				.getResultList();
		
		if (!list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public Ownership insertOwnership(String orderNum) {
		Ownership ownership = new Ownership();
		ownership.setOrderNum(orderNum);
		em.persist(ownership);
		return ownership;
	}
	
	public Boolean checkOwnershipLocked(String orderNum) {
		Ownership ownership = retrieveAndLockOwnership(orderNum);

		if (ownership != null) {
			if (ownership.getOwner() != null) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}	
}
