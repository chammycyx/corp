package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.report.FmRptStat;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.corp.udt.batch.worker.FmRptTemplateType;
import hk.org.ha.model.pms.corp.vo.report.FmReportInfo;
import hk.org.ha.model.pms.udt.report.FmReportType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.document.DocumentData;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

/**
 * Session Bean implementation class GenerateFmReportBean
 */
@AutoCreate
@Stateless
@Name("fmRptManager")
public class FmRptManager implements FmRptManagerLocal {
	
	private static final String FM_RPT_STAT_CLASS_NAME = FmRptStat.class.getName();
	private static final String PATIENT_HOSPITAL_CODE = "Patient hospital code";
	private static final String MONTH = "Month";
	private static final String EIS_SPEC_CODE = "EIS specialty code";
	private static final String OPAS_IPAS_SPECIALTY_CODE = "OPAS/IPAS specialty code";
	private static final String OPAS_SUB_SPECIALTY_CODE = "OPAS subspecialty code";
	private static final String PHS_SPECIALTY_CODE = "PHS specialty code";
	private static final String PATIENT_KEY = "Patient key";
	private static final String CASE_NUMBER = "Case number";
	private static final String ORDER_DATE = "Order date";
	private static final String ORDER_NUMBER = "Order number";
	private static final String ORDER_REFERENCE_NUMBER = "Order reference number";
	private static final String ORDER_STATUS = "Order Status";
	private static final String ACTION_STATUS_DESCRIPTION = "Action status description";
	private static final String ORDER_DESCRIPTION = "Order description";
	private static final String DISPLAY_NAME = "Display name";
	private static final String FORM_DESCRIPTION = "Form description";
	private static final String ROUTE_DESCRIPTION = "Route description";
	private static final String SALT_SPECIAL_PROPERTY = "Salt / special property";
	private static final String STRENGTH = "Strength";
	private static final String ITEM_CODE = "Item code";
	private static final String HA_CAN_SOLD = "HA can sold";
	private static final String FORMULARY_STATUS_DESCRIPTION = "Fomulary status description";
	private static final String SFI_CATEGORY = "SFI category";
	private static final String BNF_NUMBER = "BNF number (Principle BNF)";
	private static final String BNF_DESCRIPTION = "BNF description (Principle BNF)";
	private static final String PRIVATE_PATIENT = "Private patient";
	private static final String PAY_CODE = "Pay code";
	private static final String DM_PRESCRIBE_OPTION = "FM prescribe option";
	private static final String CORPORDATE_INDICATION_CODE = "Corpordate indication code";
	private static final String CORPORDATE_INDICATION_DESCRIPTION = "Corporate indication description";
	private static final String SUPPLEMENT_INDICATION_CODE = "Supplementary indication code";
	private static final String SUPPLEMENT_INDICATION_DESCRIPTION = "Supplementary indication description";
	private static final String OTHER_INDICATION_DESCRIPTION = "Other indication description";
	private static final String PRESCRIBING_DOCTOR_CODE = "Prescribing doctor code";
	private static final String AUTHORIZED_DOCTOR_NAME_AND_CODE = "Authorized doctor name and code";
	private static final String ALL = "ALL";
	private static final String SLASH = "/";
	private static final String UNCHECKED = "unchecked";
	private static final String EXCEL_EXTENSION = ".xls";
	private static final String DOT = ".";
	private static final String CSV_EXTENSION = ".csv";
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private static final String SUM_OF_ALL_15_COLUMNS = 
		" sum(o.patCount), sum(o.patSfiCount), sum(o.orderCount), sum(o.orderSfiCount)," +
		" sum(o.orderItemCount), sum(o.issueOrderCount), sum(o.generalDrugCount), sum(o.sDrugCount)," +
		" sum(o.sDrugExistPatCount), sum(o.sDrugOtherCount), sum(o.sDrugIndicatedCount)," +
		" sum(o.sDrugNonFormularyCount), sum(o.sfiCount), sum(o.unregisteredDrugCount), sum(o.sfiSafetyNetCount)";
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@Logger
	private Log logger = null;
	
	@In
	private FmRptExcelRendererInf fmRptExcelRenderer;
	
	@In
	private FmRptFilesManagerLocal fmRptFilesManager;
	
	@In
	private ApplicationProp applicationProp;

	private DateFormat yearFormat = new SimpleDateFormat("yyyy");
	private DateFormat monthFormat = new SimpleDateFormat("MM");
	private DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMM");
	
	private File workDir = null;
	private String reportName = null;
			
	public long processBeginTime()
	{
		return System.currentTimeMillis();
	}
	
	public long totalPorcessTime(long startTime)
	{
		return ((int)(System.currentTimeMillis() - startTime));
	}
	
	public Map<String, File> generateDailyDataCsvData(List<FmReportInfo> nonGeneralDrugList, List<FmReportInfo> allDrugList, 
			Map<String, File> dataFileMap, DateTime batchStartDate, String patHospCode, 
			String clusterCode, String jobId) throws Exception
	{
		reportName = fmRptFilesManager.getReportName(batchStartDate, patHospCode, clusterCode, FmReportType.MonthlyData);
		File nonGenDrugDataFile;
		List<FmReportInfo> nonGenDrugsList = nonGeneralDrugList;
		CsvWriter nonGenDrugCsv = null;
		
		try
		{
			if (dataFileMap.isEmpty())
			{
				nonGenDrugDataFile = File.createTempFile(reportName.substring(0, reportName.indexOf(DOT)), CSV_EXTENSION);
				nonGenDrugCsv = new CsvWriter(new FileWriter(nonGenDrugDataFile, true), ',');
				writeCsvHeader(nonGenDrugCsv);
				writeToCsv(nonGenDrugCsv, nonGenDrugsList);
				dataFileMap.put("nonGenDrugMap", nonGenDrugDataFile);
			}
			else
			{				
				nonGenDrugDataFile = dataFileMap.get("nonGenDrugMap");
				nonGenDrugCsv = new CsvWriter(new FileWriter(nonGenDrugDataFile, true), ',');
				writeToCsv(nonGenDrugCsv, nonGeneralDrugList);
			}
		} 
		finally
		{
			if (nonGenDrugCsv != null)
			{
				nonGenDrugCsv.close();
			}
		}
		
		return dataFileMap;
	}
	
	public void createMonthlyDrugDetailsReport(DateTime batchStartDate, String patHospCode, String clusterCode, String jobId, File nonGenDrugData) throws Exception
	{
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);		
		reportName = fmRptFilesManager.getReportName(batchStartDate, patHospCode, clusterCode, FmReportType.MonthlyData);
				
		if (!workDir.exists()) {
			fmRptFilesManager.createDirectory(workDir);
		}

		CsvReader csvReader = null;
		FileOutputStream out = null;
		
		try {
			File file = new File(workDir + SLASH + reportName);
			out = new FileOutputStream(file);
			
			HSSFWorkbook wb = new HSSFWorkbook();
		    HSSFSheet sheet = wb.createSheet("v_fm_report_data_view");  
		    CreationHelper createHelper = wb.getCreationHelper();
		    HSSFRow row;
		    HSSFCell cell;
		    
		    int rowNum = 0;
		    int orderNumCol = -1;
		    int orderDateCol = -1;
		    
		    csvReader = new CsvReader(new FileReader(nonGenDrugData));
		    while (csvReader.readRecord()) {
		    	// read columns of each row
		    	String[] values = csvReader.getValues();
		    	
		    	// set columns to excel
		    	for (int i = 0; i < values.length; i++)
		    	{
		    		row = sheet.getRow(rowNum);
		    		if (row == null)
		    		{
		    			row = sheet.createRow(rowNum);
		    		}
	
		    		cell = row.createCell(i);
		    		
		    		if (rowNum == 0)
		    		{
		    			if (values[i].equals(ORDER_DATE))
		    			{
		    				orderDateCol = i;
		    			}
		    			else if (values[i].equals(ORDER_NUMBER))
		    			{
		    				orderNumCol = i;
		    			}
		    		}
		    		
		    		
		    		if (i == orderDateCol && rowNum > 0)
		    		{	    			
		    			CellStyle cellStyle = wb.createCellStyle();
		    			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
		    			cell.setCellValue(sdf.parse(values[i]));
		    			cell.setCellStyle(cellStyle);
		    		}
		    		else if (i == orderNumCol && rowNum > 0)
		    		{
		    			cell.setCellValue(Double.valueOf(values[i]));
		    		}
		    		else
		    		{
		    			cell.setCellValue(values[i]);
		    		}
		    	}
		    	
		    	rowNum++;
		    }
			
		    wb.write(out);
		    out.flush();
		    
		} finally {
			if (csvReader != null) {
				csvReader.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}
			
	@SuppressWarnings(UNCHECKED)
	public void generateMonthlyHospSpecStatReport(DateTime batchStartDate,String patHospCode, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Monthly Hospital Specialty Statistic Report for patHospCode " + patHospCode + " with Batch Date " + batchStartDate);
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
			
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,patHospCode : UI_FM_RPT_STAT_01
				" (o.year, o.month, o.patHospCode, o.clusterCode, o.eisSpecCode," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year = :year" +
				" and o.month = :month" +
				" and o.patHospCode = :patHospCode" +
				" group by o.year, o.month, o.patHospCode, o.eisSpecCode, o.clusterCode" +
				" order by o.eisSpecCode")
				.setParameter("year", Integer.parseInt(yearFormat.format(batchStartDate.toDate())))
				.setParameter("month", Integer.parseInt(monthFormat.format(batchStartDate.toDate())))
				.setParameter("patHospCode", patHospCode)
				.getResultList();
			
		if (!fmRptStatList.isEmpty()) {
			String clusterCode = fmRptStatList.get(0).getClusterCode();
			reportName = fmRptFilesManager.getReportName(batchStartDate, patHospCode, clusterCode, FmReportType.MonthlyHospSpecStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMHOSPSPECSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMHOSPSPECSTATREPORT);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void generateMonthlyHospStatReport(DateTime batchStartDate, String patHospCode, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Monthly Hospital Statistic Report for patHospCode " + patHospCode + " with Batch Date " + batchStartDate);
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,patHospCode : UI_FM_RPT_STAT_01
				" (o.year, o.month, o.patHospCode, o.clusterCode," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year = :year" +
				" and o.month = :month" +
				" and o.patHospCode = :patHospCode" +
				" group by o.year, o.month, o.patHospCode, o.clusterCode")
				.setParameter("year", Integer.parseInt(yearFormat.format(batchStartDate.toDate())))
				.setParameter("month", Integer.parseInt(monthFormat.format(batchStartDate.toDate())))
				.setParameter("patHospCode", patHospCode)
				.getResultList();
				
		
		if (!fmRptStatList.isEmpty()) {
			String clusterCode = fmRptStatList.get(0).getClusterCode();
			reportName = fmRptFilesManager.getReportName(batchStartDate, patHospCode, clusterCode, FmReportType.MonthlyHospStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMHOSPSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMHOSPSTATREPORT);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public String generateYearlyAccumulatedHospStatReport(DateTime batchStartDate, String patHospCode, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Yearly Hospital Accumulated Report for patHospCode " + patHospCode + " with Batch Date " + batchStartDate);
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		String clusterCode = "";
		int prevFiscalYear;
		int prevNextYear;	
		
		if (batchStartDate.getMonthOfYear() < 4) {
			prevFiscalYear = batchStartDate.getYear() - 2;
			prevNextYear = batchStartDate.getYear();
		} else {
			prevFiscalYear = batchStartDate.getYear() - 1;
			prevNextYear = batchStartDate.getYear() + 1;
		}
										
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,patHospCode : UI_FM_RPT_STAT_01
				" (o.clusterCode, o.patHospCode, o.fiscalYear," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year >= :prevFinanceYear and o.year <= :nextFinanceYear" +
				" and not (o.month <= :prevFinanceMonth and o.year = :prevFinanceYear)" +
				" and not (o.month >= :nextFinanceMonth and o.year = :nextFinanceYear)" +
				" and o.patHospCode = :patHospCode" +
				" group by o.fiscalYear, o.patHospCode, o.clusterCode")
				.setParameter("prevFinanceYear", prevFiscalYear)
				.setParameter("prevFinanceMonth", 3)
				.setParameter("nextFinanceYear", prevNextYear)
				.setParameter("nextFinanceMonth", 4)
				.setParameter("patHospCode", patHospCode)
				.getResultList();
		
		if (!fmRptStatList.isEmpty()) {
			clusterCode = fmRptStatList.get(0).getClusterCode();
			reportName = fmRptFilesManager.getReportName(batchStartDate, patHospCode, clusterCode, FmReportType.YearlyAccumulatedHospStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMYEARLYSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMYEARLYSTATREPORT);
		}
		
		return clusterCode;
	}
	
	@SuppressWarnings(UNCHECKED)
	public void generateMonthlyClusterSpecStatReport(DateTime batchStartDate, String clusterCode, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Monthly Cluster Specialty Statistic Report for cluster " + clusterCode + " with Batch Date " + batchStartDate);
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,clusterCode : I_FM_RPT_STAT_01
				" (o.year, o.month, o.patHospCode, o.clusterCode, o.eisSpecCode," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year = :year" +
				" and o.month = :month" +
				" and o.clusterCode = :clusterCode" + 
				" group by o.year, o.month, o.patHospCode, o.eisSpecCode, o.clusterCode" +
				" order by o.patHospCode, o.eisSpecCode")
				.setParameter("year", Integer.parseInt(yearFormat.format(batchStartDate.toDate())))
				.setParameter("month", Integer.parseInt(monthFormat.format(batchStartDate.toDate())))
				.setParameter("clusterCode", clusterCode)
				.getResultList();
		if (!fmRptStatList.isEmpty()) {
			reportName = fmRptFilesManager.getReportName(batchStartDate, ALL, clusterCode, FmReportType.MonthlyClusterSpecStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMHOSPSPECSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMHOSPSPECSTATREPORT);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void generateMonthlyClusterStatReport(DateTime batchStartDate, String clusterCode, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Monthly Cluster Statistic Report for cluster " + clusterCode + " with Batch Date " + batchStartDate);
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,clusterCode : I_FM_RPT_STAT_01
				" (o.year, o.month, o.patHospCode, o.clusterCode," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year = :year" +
				" and o.month = :month" +
				" and o.clusterCode = :clusterCode" + 
				" group by o.year, o.month, o.clusterCode, o.patHospCode" +
				" order by o.patHospCode")
				.setParameter("year", Integer.parseInt(yearFormat.format(batchStartDate.toDate())))
				.setParameter("month", Integer.parseInt(monthFormat.format(batchStartDate.toDate())))
				.setParameter("clusterCode", clusterCode)
				.getResultList();
			
		if (!fmRptStatList.isEmpty()) {
			reportName = fmRptFilesManager.getReportName(batchStartDate, ALL, clusterCode, FmReportType.MonthlyClusterStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMHOSPSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMHOSPSTATREPORT);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void generateYearlyAccumulatedClusterStatReport(DateTime batchStartDate, String clusterCode, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Yearly Cluster Accumulated Report for cluster " + clusterCode + " with Batch Date " + batchStartDate);	
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		int prevFiscalYear;
		int prevNextYear;
		
		if (batchStartDate.getMonthOfYear() < 4) {
			prevFiscalYear = batchStartDate.getYear() - 2;
			prevNextYear = batchStartDate.getYear();
		} else {
			prevFiscalYear = batchStartDate.getYear() - 1;
			prevNextYear = batchStartDate.getYear() + 1;
		}
					
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,clusterCode : I_FM_RPT_STAT_01
				" (o.clusterCode, o.patHospCode, o.fiscalYear," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year >= :prevFinanceYear and o.year <= :nextFinanceYear" +
				" and not (o.month <= :prevFinanceMonth and o.year = :prevFinanceYear)" +
				" and not (o.month >= :nextFinanceMonth and o.year = :nextFinanceYear)" +
				" and o.clusterCode = :clusterCode" +
				" group by o.fiscalYear, o.patHospCode, o.clusterCode" +
				" order by o.fiscalYear, o.patHospCode")
				.setParameter("prevFinanceYear", prevFiscalYear)
				.setParameter("prevFinanceMonth", 3)
				.setParameter("nextFinanceYear", prevNextYear)
				.setParameter("nextFinanceMonth", 4)
				.setParameter("clusterCode", clusterCode)
				.getResultList();
		
		if (!fmRptStatList.isEmpty()) {
			reportName = fmRptFilesManager.getReportName(batchStartDate, ALL, clusterCode, FmReportType.YearlyAccumulatedClusterStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMYEARLYSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMYEARLYSTATREPORT);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void generateMonthlyCorpSpecStatReport(DateTime batchStartDate, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Monthly Corp Specialty Statistic Report with Batch Date " + batchStartDate);	
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		
		List<String> hospitalClusterList = em.createQuery(
				"select o.clusterCode from HospitalCluster o") // 20120307 index check : none
				.getResultList();
			
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,clusterCode : I_FM_RPT_STAT_01
				" (o.year, o.month, o.patHospCode, o.clusterCode, o.eisSpecCode," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year = :year" +
				" and o.month = :month" +
				" and o.clusterCode in :clusterList" + 
				" group by o.year, o.month, o.clusterCode, o.patHospCode, o.eisSpecCode" +
				" order by o.year, o.month, o.clusterCode, o.patHospCode, o.eisSpecCode")
				.setParameter("year", Integer.parseInt(yearFormat.format(batchStartDate.toDate())))
				.setParameter("month", Integer.parseInt(monthFormat.format(batchStartDate.toDate())))
				.setParameter("clusterList", hospitalClusterList)
				.getResultList();
	
		if (!fmRptStatList.isEmpty()) {
			reportName = fmRptFilesManager.getReportName(batchStartDate, ALL, ALL, FmReportType.MonthlyCorpSpecStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMHOSPSPECSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMHOSPSPECSTATREPORT);
		}	
	}
	
	@SuppressWarnings(UNCHECKED)
	public void generateMonthlyCorpStatReport(DateTime batchStartDate, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Monthly Corp Statistic Report with Batch Date " + batchStartDate);	
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		
		List<String> hospitalClusterList = em.createQuery(
				"select o.clusterCode from HospitalCluster o")  // 20120307 index check : none
				.getResultList();
		
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,clusterCode : I_FM_RPT_STAT_01
				" (o.year, o.month, o.patHospCode, o.clusterCode," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year = :year" +
				" and o.month = :month" +
				" and o.clusterCode in :clusterList" +
				" group by o.year, o.month, o.clusterCode, o.patHospCode" +
				" order by o.year, o.month, o.clusterCode, o.patHospCode")
				.setParameter("year", Integer.parseInt(yearFormat.format(batchStartDate.toDate())))
				.setParameter("month", Integer.parseInt(monthFormat.format(batchStartDate.toDate())))
				.setParameter("clusterList", hospitalClusterList)
				.getResultList();
		
		if (!fmRptStatList.isEmpty()) {
			reportName = fmRptFilesManager.getReportName(batchStartDate, ALL, ALL, FmReportType.MonthlyCorpStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMHOSPSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMHOSPSTATREPORT);
		}
	}
		
	@SuppressWarnings(UNCHECKED)
	public void generateYearlyAccumulatedCorpStatReport(DateTime batchStartDate, String jobId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Generate Yearly Corp Accumulated Report with Batch Date " + batchStartDate);	
		}
		workDir = fmRptFilesManager.getProcessingDirectory(jobId, DataDirType.Work);
		int prevFiscalYear;
		int prevNextYear;
		
		if (batchStartDate.getMonthOfYear() < 4) {
			prevFiscalYear = batchStartDate.getYear() - 2;
			prevNextYear = batchStartDate.getYear();
		} else {
			prevFiscalYear = batchStartDate.getYear() - 1;
			prevNextYear = batchStartDate.getYear() + 1;
		}
		
		List<String> hospitalClusterList = em.createQuery(
				"select o.clusterCode from HospitalCluster o") // 20120307 index check : none
				.getResultList();
				
		List<FmRptStat> fmRptStatList = em.createQuery(
				"select new " + FM_RPT_STAT_CLASS_NAME + // 20120403 index check : FmRptStat.year,month,clusterCode : I_FM_RPT_STAT_01
				" (o.clusterCode, o.patHospCode, o.fiscalYear," + SUM_OF_ALL_15_COLUMNS + ")" +
				" from FmRptStat o" +
				" where o.year >= :prevFinanceYear and o.year <= :nextFinanceYear" +
				" and not (o.month <= :prevFinanceMonth and o.year = :prevFinanceYear)" +
				" and not (o.month >= :nextFinanceMonth and o.year = :nextFinanceYear)" +
				" and o.clusterCode in :clusterList" +
				" group by o.fiscalYear, o.clusterCode, o.patHospCode" +
				" order by o.fiscalYear, o.clusterCode, o.patHospCode ")
				.setParameter("prevFinanceYear", prevFiscalYear)
				.setParameter("prevFinanceMonth", 3)
				.setParameter("nextFinanceYear", prevNextYear)
				.setParameter("nextFinanceMonth", 4)
				.setParameter("clusterList",	hospitalClusterList)
				.getResultList();
		
		if (!fmRptStatList.isEmpty()) {
			reportName = fmRptFilesManager.getReportName(batchStartDate, ALL, ALL, FmReportType.YearlyAccumulatedCorpStat);
			constrcutFmReportPercentage(fmRptStatList);
			Contexts.getSessionContext().set("fmRptStatList", fmRptStatList);
			fmRptExcelRenderer.retrieveFmTemplateReport(FmRptTemplateType.FMYEARLYSTATREPORT);
			generateReport(workDir, reportName, FmRptTemplateType.FMYEARLYSTATREPORT);
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void uploadReports(DateTime batchStartDate) throws Exception {
		List<String> patHospList = em.createQuery(
				"select distinct o.patHospCode" + // 20120307 index check : none
				" from WorkstoreGroupMapping o") 
				.getResultList();
		
		List<String> clusterList = em.createQuery(
				"select o.clusterCode" + // 20120307 index check : none
				" from HospitalCluster o") 
				.getResultList();
		
		//Upload hospital folder's reports
		for (String patHospCode : patHospList) {
			File folder = new File(applicationProp.getBatchDataDir() + SLASH + fmRptFilesManager.getDirectory(batchStartDate, patHospCode, FmReportType.MonthlyData));
			File[] listOfFiles = folder.listFiles();
			if (listOfFiles != null) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].getName().endsWith(dateTimeFormat.format(batchStartDate.toDate())+EXCEL_EXTENSION)) {
						uploadReportToServer(fmRptFilesManager.getDirectory(batchStartDate, patHospCode, FmReportType.MonthlyData), 
											 listOfFiles[i], listOfFiles[i].getName());
					}
				}
			}
		}
		
		//Upload cluster folder reports
		for (String clusterCode : clusterList) {
			File folder = new File(applicationProp.getBatchDataDir() + SLASH + fmRptFilesManager.getDirectory(batchStartDate, clusterCode, FmReportType.MonthlyClusterSpecStat));
			File[] listOfFiles = folder.listFiles();
			
			if (listOfFiles != null) {
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].getName().endsWith(dateTimeFormat.format(batchStartDate.toDate())+EXCEL_EXTENSION)) {
						uploadReportToServer(fmRptFilesManager.getDirectory(batchStartDate, clusterCode, FmReportType.MonthlyClusterSpecStat), 
											 listOfFiles[i], listOfFiles[i].getName());
					}
				}
			}
		}
		
		//upload all cluster reports
		File folder = new File(applicationProp.getBatchDataDir() + SLASH + fmRptFilesManager.getDirectory(batchStartDate, ALL, FmReportType.MonthlyCorpSpecStat));
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles != null) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].getName().endsWith(dateTimeFormat.format(batchStartDate.toDate())+EXCEL_EXTENSION)) {
					uploadReportToServer(fmRptFilesManager.getDirectory(batchStartDate, ALL, FmReportType.MonthlyCorpSpecStat), 
										 listOfFiles[i], listOfFiles[i].getName());
				}
			}	
		}
	}
	
	private void uploadReportToServer(String directory, File file, String reportName) throws Exception {
		Context context = Contexts.getEventContext();
		context.set("sftp_pms_host", applicationProp.getSftpPmsHost());
		context.set("sftp_pms_username", applicationProp.getSftpPmsUsername());
		context.set("sftp_pms_password", applicationProp.getSftpPmsPassword());
		context.set("sftp_pms_rootDir", applicationProp.getSftpPmsRootDir());
		ChannelSftp fmRptChannelSftp = (ChannelSftp) Component.getInstance("fmRptChannelSftp");
		try {
			fmRptChannelSftp.cd(directory);
			if (logger.isDebugEnabled()) {
				logger.debug("Ftp path is changed to " + fmRptChannelSftp);
			}
		} catch (SftpException e) {
			mkdirs(directory, fmRptChannelSftp);
			fmRptChannelSftp.cd(directory);
			logger.info("The upload directory " + directory + " is created");
			if (logger.isDebugEnabled()) {
				logger.debug("Path is changed to " + directory);
			}
		}
		
		fmRptChannelSftp.put(file.getPath(), reportName);
		logger.info("upload report to " + file.getPath() + " with report name " + reportName + " success");
		fmRptChannelSftp.cd(applicationProp.getSftpPmsRootDir());
	}
	
	private void mkdirs(String directory, ChannelSftp c) throws Exception {
		try {
            SftpATTRS att = c.stat(directory);
            if (att != null) {
                if (att.isDir()) {
                    return;
                }
            }
        } catch (SftpException ex) {
            if (directory.indexOf('/') != -1) {
                mkdirs(directory.substring(0, directory.lastIndexOf('/')), c);
            }
			
            c.mkdir(directory);
        }
    }
		
	private void constrcutFmReportPercentage(List<FmRptStat> fmRptStatList) {
		for (FmRptStat fmRptStat : fmRptStatList) {
			fmRptStat.setPercentageOfGeneralDrug(calculatePercentage(new BigDecimal(fmRptStat.getGeneralDrugCount()), new BigDecimal(fmRptStat.getOrderItemCount())));
			fmRptStat.setPercentageOfSDrug(calculatePercentage(new BigDecimal(fmRptStat.getsDrugCount()), new BigDecimal(fmRptStat.getOrderItemCount())));
			fmRptStat.setPercentageOfSfi(calculatePercentage(new BigDecimal(fmRptStat.getSfiCount()), new BigDecimal(fmRptStat.getOrderItemCount())));
			fmRptStat.setPercentageOfUnregisteredDrug(calculatePercentage(new BigDecimal(fmRptStat.getUnregisteredDrugCount()), new BigDecimal(fmRptStat.getOrderItemCount())));
			fmRptStat.setPercentageOfSfiSafetyNet(calculatePercentage(new BigDecimal(fmRptStat.getSfiSafetyNetCount()), new BigDecimal(fmRptStat.getOrderItemCount())));
			fmRptStat.setPercentageOfPatSfi(calculatePercentage(new BigDecimal(fmRptStat.getPatSfiCount()), new BigDecimal(fmRptStat.getPatCount())));
			fmRptStat.setPercentageOfOrderSfi(calculatePercentage(new BigDecimal(fmRptStat.getOrderSfiCount()), new BigDecimal(fmRptStat.getOrderCount())));
			
			if (fmRptStat.getsDrugCount() > 0)
			{
				fmRptStat.setPercentageOfSDrugExistPat(calculatePercentage(new BigDecimal(fmRptStat.getsDrugExistPatCount()), new BigDecimal(fmRptStat.getsDrugCount())));
				fmRptStat.setPercentageOfSDrugOther(calculatePercentage(new BigDecimal(fmRptStat.getsDrugOtherCount()), new BigDecimal(fmRptStat.getsDrugCount())));
				fmRptStat.setPercentageOfSDrugIndicated(calculatePercentage(new BigDecimal(fmRptStat.getsDrugIndicatedCount()), new BigDecimal(fmRptStat.getsDrugCount())));
				fmRptStat.setPercentageOfSDrugNonFormulary(calculatePercentage(new BigDecimal(fmRptStat.getsDrugNonFormularyCount()), new BigDecimal(fmRptStat.getsDrugCount())));
			}
			else
			{
				fmRptStat.setPercentageOfSDrugExistPat(new BigDecimal(0));
				fmRptStat.setPercentageOfSDrugOther(new BigDecimal(0));
				fmRptStat.setPercentageOfSDrugIndicated(new BigDecimal(0));
				fmRptStat.setPercentageOfSDrugNonFormulary(new BigDecimal(0));
			}
		}
	}
	
	private BigDecimal calculatePercentage(BigDecimal value, BigDecimal base) {
		return new BigDecimal(100).multiply(value.divide(base, 4, BigDecimal.ROUND_HALF_UP), new MathContext(4));
	}
	
	private void generateReport(File workDir, String reportName, FmRptTemplateType reportTemplate) throws Exception {
		if (!workDir.exists()) {
			fmRptFilesManager.createDirectory(workDir);
		}
					
		File file = new File(workDir + SLASH + reportName);
		FileOutputStream out;
		out = new FileOutputStream(file);
		((DocumentData) Contexts.getEventContext().get(reportTemplate.getDataValue())).writeDataToStream(out);
		out.flush();
		out.close();
	}
	
	private void writeCsvHeader(CsvWriter csvOutput) throws IOException
	{
		csvOutput.write(PATIENT_HOSPITAL_CODE);
		csvOutput.write(MONTH);
		csvOutput.write(EIS_SPEC_CODE);
		csvOutput.write(OPAS_IPAS_SPECIALTY_CODE);
		csvOutput.write(OPAS_SUB_SPECIALTY_CODE);
		csvOutput.write(PHS_SPECIALTY_CODE);
		csvOutput.write(PATIENT_KEY);
		csvOutput.write(CASE_NUMBER);
		csvOutput.write(ORDER_DATE);
		csvOutput.write(ORDER_NUMBER);
		csvOutput.write(ORDER_REFERENCE_NUMBER);
		csvOutput.write(ORDER_STATUS);
		csvOutput.write(ACTION_STATUS_DESCRIPTION);
		csvOutput.write(ORDER_DESCRIPTION);
		csvOutput.write(DISPLAY_NAME);
		csvOutput.write(FORM_DESCRIPTION);
		csvOutput.write(ROUTE_DESCRIPTION);
		csvOutput.write(SALT_SPECIAL_PROPERTY);
		csvOutput.write(STRENGTH);
		csvOutput.write(ITEM_CODE);
		csvOutput.write(HA_CAN_SOLD);
		csvOutput.write(FORMULARY_STATUS_DESCRIPTION);
		csvOutput.write(SFI_CATEGORY);
		csvOutput.write(BNF_NUMBER);
		csvOutput.write(BNF_DESCRIPTION);
		csvOutput.write(PRIVATE_PATIENT);
		csvOutput.write(PAY_CODE);
		csvOutput.write(DM_PRESCRIBE_OPTION);
		csvOutput.write(CORPORDATE_INDICATION_CODE);
		csvOutput.write(CORPORDATE_INDICATION_DESCRIPTION);
		csvOutput.write(SUPPLEMENT_INDICATION_CODE);
		csvOutput.write(SUPPLEMENT_INDICATION_DESCRIPTION);
		csvOutput.write(OTHER_INDICATION_DESCRIPTION);
		csvOutput.write(PRESCRIBING_DOCTOR_CODE);
		csvOutput.write(AUTHORIZED_DOCTOR_NAME_AND_CODE);
		csvOutput.endRecord();
	}
	
	private void writeToCsv(CsvWriter csvOutput, List<FmReportInfo> fmReportList) throws IOException
	{		
		for (FmReportInfo fmReportInfo : fmReportList)
		{
			csvOutput.write(fmReportInfo.getPatHospCode());
			csvOutput.write(fmReportInfo.getYearMonth());
			csvOutput.write(fmReportInfo.getEisSpecCode());
			csvOutput.write(fmReportInfo.getPasSpecCode());
			csvOutput.write(fmReportInfo.getPasSubSpecCode());
			csvOutput.write(fmReportInfo.getSpecCode());
			csvOutput.write(fmReportInfo.getPatKey());
			csvOutput.write(fmReportInfo.getCaseNum());
			csvOutput.write(sdf.format(fmReportInfo.getOrderDate()));
			csvOutput.write(fmReportInfo.getOrderNum().toString());
			csvOutput.write(fmReportInfo.getRefNum());
			csvOutput.write(fmReportInfo.getOrderStatus());
			csvOutput.write(fmReportInfo.getActionStatus().getDisplayValue());
			csvOutput.write(fmReportInfo.getOrderDesc());
			csvOutput.write(fmReportInfo.getDisplayName());
			csvOutput.write(fmReportInfo.getFormDesc());
			csvOutput.write(fmReportInfo.getRouteDesc());
			csvOutput.write(fmReportInfo.getSaltProperty());
			csvOutput.write(fmReportInfo.getStrength());
			csvOutput.write(fmReportInfo.getItemCode());
			csvOutput.write(fmReportInfo.getHaSold());
			csvOutput.write(fmReportInfo.getFmStatusDesc());
			csvOutput.write(fmReportInfo.getSfiCateogry());
			csvOutput.write(fmReportInfo.getBnfNum());
			csvOutput.write(fmReportInfo.getBnfDesc());
			csvOutput.write(fmReportInfo.getPrivatePat());
			csvOutput.write(fmReportInfo.getPayCode());
			csvOutput.write(fmReportInfo.getPrescOptionDesc());
			csvOutput.write(fmReportInfo.getCoInd());
			csvOutput.write(fmReportInfo.getCoIndDesc());
			csvOutput.write(fmReportInfo.getSuInd());
			csvOutput.write(fmReportInfo.getSuIndDesc());
			csvOutput.write(fmReportInfo.getOtherMessage());
			csvOutput.write(fmReportInfo.getDoctorCode());
			csvOutput.write(fmReportInfo.getAuthCode());
			csvOutput.endRecord();
		}
	}
	
	@Remove
	public void destory() {
		
	}
}
