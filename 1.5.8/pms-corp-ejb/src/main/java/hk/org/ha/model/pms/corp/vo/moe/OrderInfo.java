package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;

public class OrderInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private Long ordNum;
	
	private String caseNum;
	
	private Integer itemNum;
	
	private Integer convertOrderInd;
	
	public OrderInfo() {
		super();
	}
	
	public OrderInfo(String patHospCode, String orderNum, String caseNum,
			Integer itemNum, Boolean modifyFlag) throws NumberFormatException {
		super();
		this.patHospCode = patHospCode;
		this.ordNum = Long.parseLong(orderNum.substring(3));
		this.caseNum = caseNum;
		this.itemNum = itemNum;
		
		if (Boolean.FALSE == modifyFlag) {
			convertOrderInd = 1;
		}
		else if (Boolean.TRUE == modifyFlag) {
			convertOrderInd = 2;
		}
		else {
			convertOrderInd = 3;
		}
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public Long getOrdNum() {
		return ordNum;
	}

	public void setOrdNum(Long ordNum) {
		this.ordNum = ordNum;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getConvertOrderInd() {
		return convertOrderInd;
	}

	public void setConvertOrderInd(Integer convertOrderInd) {
		this.convertOrderInd = convertOrderInd;
	}
}
