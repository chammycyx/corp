package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.corp.persistence.order.CorporateTask;

import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Stateless
@Name("corporateTaskManager")
@Scope(ScopeType.STATELESS)
public class CorporateTaskManagerBean implements CorporateTaskManagerLocal {

	private static final long GRACE_TIME_OFFSET = 500L;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
				
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean lock(String taskName, Date date) {				
		return lock(taskName, date, 0L);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean lock(String taskName, Date date, long graceTime) {
				
		CorporateTask corporateTask = (CorporateTask) em.createQuery(
				"select o from CorporateTask o" + // 20131203 index check : CorporateTask.taskName : PK_CORPORATE_TASK
				" where o.taskName = :taskName" +
				" and o.taskName is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.setParameter("taskName", taskName)
				.getSingleResult();
		
		long tmpGraceTime = graceTime;
		if (tmpGraceTime > GRACE_TIME_OFFSET) {
			tmpGraceTime -= GRACE_TIME_OFFSET;
		}
		
		if (corporateTask.getTaskDate() == null || 
				date.after(new Date(corporateTask.getTaskDate().getTime() + tmpGraceTime))) {
			corporateTask.setTaskDate(date);
			return true;
		}
		return false;
	}
	
	
}
