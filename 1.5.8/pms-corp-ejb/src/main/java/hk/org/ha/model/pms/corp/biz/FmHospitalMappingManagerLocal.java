package hk.org.ha.model.pms.corp.biz;

import java.util.List;
import java.util.Map;

import hk.org.ha.model.pms.corp.persistence.FmHospitalMapping;
import javax.ejb.Local;

@Local
public interface FmHospitalMappingManagerLocal {
	FmHospitalMapping retrieveFmHospitalMapping(String patHospCode);

	List<FmHospitalMapping> retrieveFmHospitalMappingList();
}
