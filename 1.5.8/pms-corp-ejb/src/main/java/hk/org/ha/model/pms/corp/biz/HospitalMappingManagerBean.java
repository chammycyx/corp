package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("hospitalMappingManager")
@MeasureCalls
public class HospitalMappingManagerBean implements HospitalMappingManagerLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> retrievePatHospCodeList(String dispHospCode) {

		return em.createQuery(
				"select distinct o.patHospCode from HospitalMapping o" + // 20170418 index check : HospitalMapping.hspCode : FK_HOSPITAL_MAPPING_01
				" where o.hospCode = :hospCode" +
				" and o.status = :status")
				.setParameter("hospCode", dispHospCode)
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}

}
