package hk.org.ha.model.pms.corp.batch.worker.reftable.moe;

import hk.org.ha.model.pms.persistence.corp.ActiveWard;
import hk.org.ha.model.pms.persistence.corp.ActiveWardPK;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("activeWardHelper")
@Scope(ScopeType.APPLICATION)
public class ActiveWardHelper {
	
	@SuppressWarnings("unchecked")
	public void updateActiveWardForPms(EntityManager em, List<ActiveWard> activeWardList, List<String> errorPatHospCodeList) {
		List<ActiveWard> orgList = em.createQuery(" SELECT o FROM ActiveWard o ").getResultList();
		Map<ActiveWardPK, ActiveWard> activeWardMap = new HashMap<ActiveWardPK, ActiveWard>();
		for(ActiveWard activeWard : orgList){
			ActiveWardPK activeWardPK = new ActiveWardPK(activeWard.getPatHospCode(), activeWard.getPasWardCode());
			activeWardMap.put(activeWardPK, activeWard);			
		}
		for(ActiveWard activeWard : activeWardList)
		{
			ActiveWardPK key = activeWard.getId();
			ActiveWard orgActiveWard = activeWardMap.get(key);
			if(orgActiveWard != null)
			{
				orgActiveWard.setStatus(activeWard.getStatus());
				orgActiveWard.setEffectiveDate(activeWard.getEffectiveDate());
			}
			else
			{
				em.persist(activeWard);
			}
			activeWardMap.remove(key);
		}
		for(ActiveWard activeWard : activeWardMap.values())
		{
			if( !errorPatHospCodeList.contains(activeWard.getPatHospCode()) ){
				activeWard.setStatus(RecordStatus.Delete);
			}
		}	
		em.flush();
	}
}
