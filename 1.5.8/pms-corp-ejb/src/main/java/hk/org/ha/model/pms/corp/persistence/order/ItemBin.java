package hk.org.ha.model.pms.corp.persistence.order;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Entity
@Table(name = "ITEM_BIN")
public class ItemBin extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", nullable = false, length=3)
	private String hospCode;
	
	@Id
	@Column(name = "WORKSTORE_CODE", nullable = false, length=4)
	private String workstoreCode;
	
	@Id
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;

	@Id
	@Column(name = "BIN_NUM", nullable = false, length = 4)
	private String binNum;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false, insertable = false, updatable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false, insertable = false, updatable = false)
	})
	private Workstore workstore;

	public ItemBin(){
		
	}
			
	public ItemBin(String hospCode, String workstoreCode, String itemCode, String binNum){
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.itemCode = itemCode;
		this.binNum = binNum;
	}
	
	public ItemBinPK getId(){
		return new ItemBinPK(hospCode, workstoreCode, itemCode, binNum);
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	@Override
	public int hashCode() {		
		return new HashCodeBuilder()
			.append(hospCode)
			.append(workstoreCode)
			.append(itemCode)
			.append(binNum)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		ItemBin other = (ItemBin) obj;
		return new EqualsBuilder()
			.append(hospCode, other.getHospCode())
			.append(workstoreCode, other.getWorkstoreCode())
			.append(itemCode, other.getItemCode())
			.append(binNum, other.getBinNum())
			.isEquals();
	}		
	
}
