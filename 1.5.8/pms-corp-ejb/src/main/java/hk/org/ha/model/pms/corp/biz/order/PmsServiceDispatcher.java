package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.disp.DispOrderBatchProcessingType;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.service.biz.pms.interfaces.PmsServiceJmsRemote;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("pmsServiceDispatcher")
@Scope(ScopeType.APPLICATION)
public class PmsServiceDispatcher implements PmsServiceJmsRemote {

	private String componentNamePattern;
	
	public String getComponentNamePattern() {
		return componentNamePattern;
	}

	public void setComponentNamePattern(String componentNamePattern) {
		this.componentNamePattern = componentNamePattern;
	}

	public PmsServiceJmsRemote getProxy(String clusterCode) {
		
		String componentName = componentNamePattern.replace("{clusterCode}", StringUtils.capitalize(clusterCode.toLowerCase()));
				
		PmsServiceJmsRemote jmsRemote = (PmsServiceJmsRemote) Component.getInstance(componentName);
		
		if (jmsRemote == null) {
			throw new UnsupportedOperationException("Component " + componentName + " not found");
		}
		return jmsRemote;
	}
	
	public PmsServiceJmsRemote getProxy(Workstore workstore) {
		
		String clusterCode = workstore.getHospital().getHospitalCluster().getClusterCode();	
		
		return getProxy(clusterCode);
	}
		
	@Override
	public void receiveNewOrder(MedOrder medOrder) {		
		this.getProxy(medOrder.getWorkstore())
				.receiveNewOrder(medOrder);
	}

	@Override
	public void receiveUpdateOrder(OpTrxType opTrxType, MedOrder medOrder) {
		this.getProxy(medOrder.getWorkstore())
				.receiveUpdateOrder(opTrxType, medOrder);
	}

	@Override
	public void receiveConfirmRemark(MedOrder medOrder) {
		this.getProxy(medOrder.getWorkstore())
				.receiveConfirmRemark(medOrder);
	}

	@Override
	public void receiveDiscontinue(OpTrxType opTrxType, MedOrder medOrder) {
		this.getProxy(medOrder.getWorkstore())
				.receiveDiscontinue(opTrxType, medOrder);
	}
	
	@Override
	public void receiveMedProfileOrder(MedProfileOrder medProfileOrder) {
		this.getProxy(medProfileOrder.getWorkstore())
				.receiveMedProfileOrder(medProfileOrder);
	}

	public void receiveChemoOrder(MedOrder medOrder, List<MedProfileOrder> medProfileOrderList) {
		Workstore workstore = null;
		if (medOrder != null) {
			workstore = medOrder.getWorkstore();
		} else if (medProfileOrderList != null) {
			workstore = medProfileOrderList.get(0).getWorkstore();
		}
		this.getProxy(workstore)
				.receiveChemoOrder(medOrder, medProfileOrderList);
	}
	
	@Override
	public void updatePatient(List<MedOrder> medOrderList) {
		this.getProxy(medOrderList.get(0).getWorkstore())
				.updatePatient(medOrderList);
	}
	
	@Override
	public void markProcessingFlagType(String clusterCode, List<Long> dispOrderIdList, Boolean batchProcessingFlag, DispOrderBatchProcessingType batchProcessingType, int chunkSize) {
		this.getProxy(clusterCode)
				.markProcessingFlagType(clusterCode, dispOrderIdList, batchProcessingFlag, batchProcessingType, chunkSize);
	}
	
	@Override
	public void updateDayEndCompleteInfoForDayEnd(String clusterCode, List<Long> dispOrderIdList, Date batchDate, String batchRemark, int chunkSize) {
		this.getProxy(clusterCode)
				.updateDayEndCompleteInfoForDayEnd(clusterCode, dispOrderIdList, batchDate, batchRemark, chunkSize);
	}

	@Override
	public void updateDayEndCompleteInfoForUncollect(String clusterCode, List<Long> dispOrderIdListForNonDayEnd, List<Long> dispOrderIdListForDayEnd, String batchRemark, int chunkSize) {
		this.getProxy(clusterCode)
				.updateDayEndCompleteInfoForUncollect(clusterCode, dispOrderIdListForNonDayEnd, dispOrderIdListForDayEnd, batchRemark, chunkSize);
	}

	@Override
	public void updateDayEndCompleteInfoForSendSuspend(String clusterCode, List<Long> dispOrderIdListForSuspend, List<Long> dispOrderIdListForUnsuspend, String batchRemarkForSuspend, String batchRemarkForUnsuspend, int chunkSize) {
		this.getProxy(clusterCode)
				.updateDayEndCompleteInfoForSendSuspend(clusterCode, dispOrderIdListForSuspend, dispOrderIdListForUnsuspend, batchRemarkForSuspend, batchRemarkForUnsuspend, chunkSize);
	}

	public void markDhLatestFlag(String clusterCode, String caseNum) {
		this.getProxy(clusterCode)
				.markDhLatestFlag(clusterCode, caseNum);
	}
}
