package hk.org.ha.model.pms.corp.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jboss.seam.log.Log;

/**
 * @author tommy.chong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class TimeStamp 
{
    String name;
    long startTime;		
    long lastTime;		
    List<Event> events = new ArrayList<Event>();
    
    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
	public TimeStamp(String name)
	{
		this.name = name;
		reset();
	}

	public void reset()
	{
		synchronized(this)
		{
			startTime = System.currentTimeMillis();		
			lastTime = startTime;		
			events.clear();
		}
	}
	
	public void stampTime(String message)
	{
		synchronized(this)
		{
			Event event = new Event(message, startTime, lastTime);
			events.add(event);
			lastTime = event.getTime();
		}
	}
	
	public Collection getEvents()
	{
		return events;
	}

	public String toString() 
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("[methodCall: name=" + name + ",startTime=" + dateFormat.format(new Date(startTime)) + 
			",endTime=" + dateFormat.format(new Date(lastTime)) + ",events={");
			
		synchronized(this)
		{
			if (events.size() > 0)
			{
				Iterator itr = events.iterator();
				while (itr.hasNext())
				{
					Event event = (Event) itr.next();
					sb.append(event.toString() + ",");
				}
				
				sb.setLength(sb.length() - 1);
			}
		}
		
		return sb.toString();
	}
	
	public void writeLog(Log log) 
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("methodCall: name=" + name + " startTime=" + dateFormat.format(new Date(startTime)) + 
			" endTime=" + dateFormat.format(new Date(lastTime)) + "\n");
			
		synchronized(this)
		{
			if (events.size() > 0)
			{
				int count = 1;
				Iterator itr = events.iterator();
				while (itr.hasNext())
				{
					Event event = (Event) itr.next();
					sb.append("no=" + add0(count++,3) + " event=" + event + "\n");
				}
			}
		}
				
		log.info(sb.toString());	
	}

	class Event
	{
		String  message;
		long time;
		long timeDiff;
		long eventTimeDiff;
		
		Event(String message, long startTime, long lastEventTime)
		{
			this.message = message;
			time = System.currentTimeMillis();
			timeDiff = time - startTime;
			eventTimeDiff = time - lastEventTime;
		}
		
		public long getTime()
		{
			return time;
		}

		public long getTimeDiff()
		{
			return timeDiff;
		}

		public long getEventTimeDiff()
		{
			return eventTimeDiff;
		}

		public String toString() 
		{
			return "[diff=" + add0(timeDiff,6) + 
				",eventDiff=" + add0(eventTimeDiff,6) +
				",message=" + message +"]";
		}

	}	

	public static String add0(long v, int l)
	{
	    long lv = (long)Math.pow(10, l);
	    return String.valueOf(lv + v).substring(1);
	}	
}
