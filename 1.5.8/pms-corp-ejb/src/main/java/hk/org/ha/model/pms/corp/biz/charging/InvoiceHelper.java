package hk.org.ha.model.pms.corp.biz.charging;

import hk.org.ha.fmk.pms.util.ReflectionUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.disp.PharmOrderPatType;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@AutoCreate
@Name("invoiceHelper")
@Scope(ScopeType.APPLICATION)
public class InvoiceHelper {
	
	private static final String PARAM_INV_DOCTYPE = "invoiceDocType";
	private static final String PARAM_INV_STATUS = "invoiceStatusList";	
	private static final String PARAM_ORDERTYPE = "orderType";
	private static final String PARAM_WORKSTORE = "workstore";
	private static final String PARAM_HOSPCODE = "hospCode";
	private static final List<InvoiceStatus> nonSysDeletedInvoiceStatusList = Arrays.asList(InvoiceStatus.SysDeleted);
	private static final List<InvoiceStatus> nonActiveInvoiceStatusList = Arrays.asList(InvoiceStatus.Void,InvoiceStatus.SysDeleted);
		
	public Invoice retrieveInvoiceByInvoiceId(EntityManager em, Long invoiceId) {
		return em.find(Invoice.class, invoiceId);
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceNum(EntityManager em, String invoiceNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){		
		StringBuilder sb = new StringBuilder(
				"select o from InvoiceItem o" + // 20150423 index check : Invoice.invoiceNum : I_INVOICE_01
				" where o.invoice.docType = :invoiceDocType" +
				" and o.invoice.status not in :invoiceStatusList" +
				" and o.invoice.invoiceNum = :invoiceNum");
		
		if ( orderType == OrderType.InPatient ) {
			sb.append(" and o.invoice.orderType = :orderType");
			sb.append(" and o.invoice.hospCode = :hospCode");
		} else {
			//TODO: invoice.workstore, invoice.orderType
			sb.append(" and o.invoice.dispOrder.orderType = :orderType");
			sb.append(" and o.invoice.dispOrder.workstore = :workstore");
		}
		
		if ( patType != SfiInvoiceEnqPatType.Both ) {
			sb.append(" and o.invoice.dispOrder.pharmOrder.patType in :patType");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("invoiceNum", invoiceNum);
		query.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi);
		query.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList );
		query.setParameter(PARAM_ORDERTYPE, orderType);
		
		if ( orderType == OrderType.InPatient ) {
			query.setParameter(PARAM_HOSPCODE, workstore.getHospCode());
		} else {
			query.setParameter(PARAM_WORKSTORE, workstore);
		}
		
		if ( patType == SfiInvoiceEnqPatType.Private ) {
			query.setParameter("patType", Arrays.asList(PharmOrderPatType.Private));
		} else if ( patType == SfiInvoiceEnqPatType.Public ) {
			query.setParameter("patType", PharmOrderPatType.Public_Nep);
		}

		addHintsForInvoiceItem(query);

		return invokeInvoiceItem(query.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceDate(EntityManager em, Date invoiceDate, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){
		DateTime invoiceEndDate = new DateTime(invoiceDate).plusDays(1);	
		
		StringBuilder sb = new StringBuilder(
				"select o from InvoiceItem o" + // 20150423 index check : Invoice.hospCode,orderType,docType,invoiceDate : I_INVOICE_02
				" where o.invoice.docType = :invoiceDocType" + 
				" and o.invoice.status not in :invoiceStatusList");
		
		if ( orderType == OrderType.InPatient ) {
			sb.append(" and o.invoice.orderType = :orderType");
			sb.append(" and o.invoice.invoiceDate >= :invoiceDate");
			sb.append(" and o.invoice.invoiceDate < :invoiceEndDate");
			sb.append(" and o.invoice.hospCode = :hospCode");
		} else {
			//TODO: invoice.invoiceDate, invoice.workstore, invoice.orderType
			sb.append(" and o.invoice.dispOrder.orderType = :orderType");
			sb.append(" and o.invoice.dispOrder.dispDate >= :invoiceDate");
			sb.append(" and o.invoice.dispOrder.dispDate < :invoiceEndDate");
			sb.append(" and o.invoice.dispOrder.workstore = :workstore"); 
		}
		
		if ( patType != SfiInvoiceEnqPatType.Both ) {
			sb.append(" and o.invoice.dispOrder.pharmOrder.patType in :patType");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("invoiceDate", invoiceDate, TemporalType.DATE);
		query.setParameter("invoiceEndDate", invoiceEndDate.toDate(), TemporalType.DATE);
		query.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi);
		query.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList );
		query.setParameter(PARAM_ORDERTYPE, orderType);

		if ( orderType == OrderType.InPatient ) {
			query.setParameter(PARAM_HOSPCODE, workstore.getHospCode());
		} else {
			query.setParameter(PARAM_WORKSTORE, workstore);
		}
		
		if ( patType == SfiInvoiceEnqPatType.Private ) {
			query.setParameter("patType", Arrays.asList(PharmOrderPatType.Private));
		} else if ( patType == SfiInvoiceEnqPatType.Public ) {
			query.setParameter("patType", PharmOrderPatType.Public_Nep);
		}
		
		addHintsForInvoiceItem(query);

		return invokeInvoiceItem(query.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceItem> retrieveSfiInvoiceItemListByCaseNum(EntityManager em, String caseNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){		
		StringBuilder sb = new StringBuilder(
				"select o from InvoiceItem o" + // 20150423 index check : MedCase.caseNum : I_MED_CASE_01
				" where o.invoice.dispOrder.pharmOrder.medCase.caseNum = :caseNum" +
				" and o.invoice.docType = :invoiceDocType" +
				" and o.invoice.status not in :invoiceStatusList");
		
		if ( orderType == OrderType.InPatient ) {
			sb.append(" and o.invoice.orderType = :orderType");
			sb.append(" and o.invoice.hospCode = :hospCode");
		} else {
			//TODO: invoice.workstore, invoice.orderType
			sb.append(" and o.invoice.dispOrder.orderType = :orderType");
			sb.append(" and o.invoice.dispOrder.workstore = :workstore"); 
		}
		
		if ( patType != SfiInvoiceEnqPatType.Both ) {
			sb.append(" and o.invoice.dispOrder.pharmOrder.patType in :patType");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("caseNum", caseNum);
		query.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi);
		query.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList );
		query.setParameter(PARAM_ORDERTYPE, orderType);
		
		if ( orderType == OrderType.InPatient ) {
			query.setParameter(PARAM_HOSPCODE, workstore.getHospCode());
		} else {
			query.setParameter(PARAM_WORKSTORE, workstore);
		}
		
		if ( patType == SfiInvoiceEnqPatType.Private ) {
			query.setParameter("patType", Arrays.asList(PharmOrderPatType.Private));
		} else if ( patType == SfiInvoiceEnqPatType.Public ) {
			query.setParameter("patType", PharmOrderPatType.Public_Nep);
		}
		
		addHintsForInvoiceItem(query);

		return invokeInvoiceItem(query.getResultList());
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceItem> retrieveSfiInvoiceItemListByHkid(EntityManager em, String hkid, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){
		StringBuilder sb = new StringBuilder(
				"select o from InvoiceItem o" + // 20150423 index check : Invoice.hospCode,orderType,docType,hkid : I_INVOICE_03
				" where o.invoice.docType = :invoiceDocType" +
				" and o.invoice.status not in :invoiceStatusList");
		
		if ( orderType == OrderType.InPatient ) {
			sb.append(" and o.invoice.orderType = :orderType");
			sb.append(" and o.invoice.hkid = :hkid");
			sb.append(" and o.invoice.hospCode = :hospCode");
		} else {
			//TODO: invoice.hkid, invoice.workstore, invoice.orderType
			sb.append(" and o.invoice.dispOrder.orderType = :orderType");
			sb.append(" and o.invoice.dispOrder.pharmOrder.hkid = :hkid");
			sb.append(" and o.invoice.dispOrder.workstore = :workstore");
		}
		
		if ( patType != SfiInvoiceEnqPatType.Both ) {
			sb.append(" and o.invoice.dispOrder.pharmOrder.patType in :patType");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hkid", hkid);
		query.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi);
		query.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList );
		query.setParameter(PARAM_ORDERTYPE, orderType);
		
		if ( orderType == OrderType.InPatient ) {
			query.setParameter(PARAM_HOSPCODE, workstore.getHospCode());
		} else {
			query.setParameter(PARAM_WORKSTORE, workstore);
		}
		
		if ( patType == SfiInvoiceEnqPatType.Private ) {
			query.setParameter("patType", Arrays.asList(PharmOrderPatType.Private));
		} else if ( patType == SfiInvoiceEnqPatType.Public ) {
			query.setParameter("patType", PharmOrderPatType.Public_Nep);
		}
		
		addHintsForInvoiceItem(query);

		return invokeInvoiceItem(query.getResultList());
	}	
	
	@SuppressWarnings("unchecked")
	public List<InvoiceItem> retrieveSfiInvoiceItemListByExtactionPeriod(EntityManager em, Integer dayRange, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType){
		DateTime today = new DateTime();
		
		StringBuilder sb = new StringBuilder(
				"select o from InvoiceItem o" + // 20150423 index check : Invoice.hospCode,orderType,docType,invoiceDate : I_INVOICE_02
				" where o.invoice.invoiceDate > :beforeDate" +
				" and o.invoice.invoiceDate < :nextday" + 
				" and o.invoice.docType = :invoiceDocType" + 
				" and o.invoice.status not in :invoiceStatusList");
		
		if ( orderType == OrderType.InPatient ) {
			sb.append(" and o.invoice.orderType = :orderType");
			sb.append(" and o.invoice.hospCode = :hospCode");
		} else {
			//TODO: invoice.workstore, invoice.orderType
			sb.append(" and o.invoice.dispOrder.orderType = :orderType");
			sb.append(" and o.invoice.dispOrder.workstore = :workstore");
		}
		
		if ( patType != SfiInvoiceEnqPatType.Both ) {
			sb.append(" and o.invoice.dispOrder.pharmOrder.patType in :patType");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("nextday", today.plusDays(1).toDate() ,TemporalType.DATE);
		query.setParameter("beforeDate", today.minusDays(dayRange+1).toDate() ,TemporalType.DATE);
		query.setParameter(PARAM_INV_DOCTYPE, InvoiceDocType.Sfi);
		query.setParameter(PARAM_INV_STATUS, includeVoidInvoice ? nonSysDeletedInvoiceStatusList : nonActiveInvoiceStatusList );
		query.setParameter(PARAM_ORDERTYPE, orderType);

		if ( orderType == OrderType.InPatient ) {
			query.setParameter(PARAM_HOSPCODE, workstore.getHospCode());
		} else {
			query.setParameter(PARAM_WORKSTORE, workstore);
		}
		
		if ( patType == SfiInvoiceEnqPatType.Private ) {
			query.setParameter("patType", Arrays.asList(PharmOrderPatType.Private));
		} else if ( patType == SfiInvoiceEnqPatType.Public ) {
			query.setParameter("patType", PharmOrderPatType.Public_Nep);
		}
		
		addHintsForInvoiceItem(query);
		
		return invokeInvoiceItem(query.getResultList());
	}
	
	private void addHintsForInvoiceItem(Query query){
		query.setHint(QueryHints.FETCH, "o.dispOrderItem.pharmOrderItem.medOrderItem.medOrder.patient");
		query.setHint(QueryHints.FETCH, "o.dispOrderItem.pharmOrderItem.pharmOrder.medCase");
		query.setHint(QueryHints.FETCH, "o.dispOrderItem.dispOrder.ticket");
		query.setHint(QueryHints.BATCH, "o.invoice");
	}
	
	private List<InvoiceItem> invokeInvoiceItem(List<InvoiceItem> invoiceItemList){
		for ( InvoiceItem invoiceItem : invoiceItemList) {
			ReflectionUtils.invoke(invoiceItem, "o.invoice.dispOrder.pharmOrder.medOrder");
			ReflectionUtils.invoke(invoiceItem, "o.invoice");
		}
		return invoiceItemList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Invoice> retrieveSfiInvoiceListById(EntityManager em, List<Long> invoiceIdList){
		if ( invoiceIdList.isEmpty() ) {
			return new ArrayList<Invoice>();
		}
		
		return em.createQuery("select o from Invoice o" + // 20150423 index check : Invoice.id : PK_INVOICE
								" where o.id in :invoiceIdList")
								.setParameter("invoiceIdList", invoiceIdList)
								.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Invoice> retrieveSfiInvoiceListByInvoiceNumList(EntityManager em, List<String> invoiceNumList){
		if ( invoiceNumList.isEmpty() ) {
			return new ArrayList<Invoice>();
		}
		
		return em.createQuery("select o from Invoice o" + // 20150423 index check : Invoice.invoiceNum : I_INVOICE_01
								" where o.invoiceNum in :invoiceNumList" +
								" and o.status not in :invoiceStatusList")
								.setParameter("invoiceNumList", invoiceNumList)
								.setParameter(PARAM_INV_STATUS, nonActiveInvoiceStatusList)
								.getResultList();
	}
}
