package hk.org.ha.model.pms.corp.biz.cddh.moe;

import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfo;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseInfoCriteria;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItem;
import hk.org.ha.model.pms.corp.vo.moe.CddhDispenseItemCriteria;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CorpCddhMoeServiceLocal {

	List<CddhDispenseInfo> retrieveCddhDispenseInfoList(CddhDispenseInfoCriteria criteria);
	
	List<CddhDispenseItem> retrieveCddhDispenseItemList(CddhDispenseItemCriteria criteria);	
}
