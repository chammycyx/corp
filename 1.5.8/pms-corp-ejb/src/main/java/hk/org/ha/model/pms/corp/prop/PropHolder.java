package hk.org.ha.model.pms.corp.prop;

import hk.org.ha.model.pms.corp.cache.PropCacherBean;
import hk.org.ha.model.pms.persistence.PropEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Workstore;

public class PropHolder<V> {
	
	private String name;
	
	public PropHolder(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public boolean isEquals(V value) {
		return this.get().equals(value);
	}

	public V get(V def) {
		V v = this.get();
		return (v != null) ? v : def;
	}	
	
	public V get(Workstore workstore, V def) {
		V v = this.get(workstore);
		return (v != null) ? v : def;
	}	
	
	public V get(Hospital hospital, V def) {
		V v = this.get(hospital);
		return (v != null) ? v : def;
	}	

	public void set(V v) {
		PropEntity propEntity = this.getPropEntity();
		propEntity.setConvertedValue(v);
	}
	
	public void set(Workstore workstore, V v) {
		PropEntity propEntity = this.getPropEntity(workstore);
		propEntity.setConvertedValue(v);
	}

	public void set(Hospital hospital, V v) {
		PropEntity propEntity = this.getPropEntity(hospital);
		propEntity.setConvertedValue(v);
	}

	public V get() {
		return getConvertedValue(this.getPropEntity());
	}	

	public V get(Workstore workstore) {		
		return getConvertedValue(this.getPropEntity(workstore));
	}	
		
	public V get(Hospital hospital) {		
		return getConvertedValue(this.getPropEntity(hospital));
	}	

	public PropEntity getPropEntity(Workstore workstore) {
		return PropCacherBean.instance().getPropEntity(workstore, name);		
	}
		
	public PropEntity getPropEntity(Hospital hospital) {
		return PropCacherBean.instance().getPropEntity(hospital, name);		
	}

	public PropEntity getPropEntity() {
		return PropCacherBean.instance().getPropEntity(name);		
	}

	@SuppressWarnings("unchecked")
	private V getConvertedValue(PropEntity propEntity) {
		if (propEntity.getValue() != null) {
			return (V) propEntity.getConvertedValue();
		} else {
			return null;
		}
	}	
}
