package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedCasePasPayFlagType;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.udt.vetting.OrderType;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Session Bean implementation class ExportChargingTrxBean
 */
@AutoCreate
@Stateless
@Name("exportChargingTrx")
@MeasureCalls
public class ExportChargingTrxBean implements ExportChargingTrxLocal {

	private final static char SPACE = ' ';
	private static final int ITEM_NUM_LENGTH = 3;
	
	private static final Pattern MP_CASE_NUM_PATTERN_AE = Pattern.compile("^AE[0-9]+[A-Za-z0-9]$");
	private static final Pattern MP_CASE_NUM_PATTERN_HN = Pattern.compile("^HN[0-9]+[A-Za-z0-9]$");
	
	
    /**
     * Default constructor. 
     */
    public ExportChargingTrxBean() {
        // TODO Auto-generated constructor stub
    }
    
	private Boolean validTransaction(DispOrderItem dispOrderItem, List<String> exceptItemList, Map<StringArray, Boolean> duplicateCheckMap) throws Exception {
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		MedOrderItem medOrderItem = pharmOrderItem.getMedOrderItem();
		MedOrder medOrder = dispOrder.getPharmOrder().getMedOrder();
		Workstore workstore = dispOrder.getWorkstore();
		MedCase medCase = dispOrder.getPharmOrder().getMedCase();
		
		if (dispOrder.getStatus() != DispOrderStatus.Issued || 
			medOrder.getOrderType() != OrderType.OutPatient) {
			
			return Boolean.FALSE;
		}
		
		if (medCase != null && medCase.getCaseNum() != null && 
				(MP_CASE_NUM_PATTERN_AE.matcher(medCase.getCaseNum()).find() || 
				 MP_CASE_NUM_PATTERN_HN.matcher(medCase.getCaseNum()).find()) ) {
			return Boolean.FALSE;
		}
		
		if (exceptItemList.contains(pharmOrderItem.getItemCode())) {
			return Boolean.FALSE;
		}
		
		if (dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord) {
			return Boolean.FALSE;
		}

		// skip refill order (in legacy refill item not inserted to new_dispensed_charge) 
		if (dispOrder.getRefillFlag()) {
			return Boolean.FALSE;
		}

		// skip following item:
		// 1. IP case number order (already skipped above)
		// 2. Manual order item vetted when standard charge flag is off
		// 3. MOE order item vetted when standard charge flag is off, and the incoming MOE order 
		//    from CMS has no chargeHdr and chargeDtl
		// (in legacy these items is not insert to new_dispensed_charge)
		if (medOrderItem.getCmsChargeFlag() == YesNoBlankFlag.Blank) {
			return Boolean.FALSE;
		}

		// prevent DOI with same MOI item num be generated to DC-TRX in duplication, because legacy new_dispensed_charge
		// is based on MedOrderItem
		StringArray duplicateCheckKey = new StringArray(medOrder.getOrderNum().toString(), 
														medOrderItem.getItemNum().toString()); 
		if (duplicateCheckMap.containsKey(duplicateCheckKey)) {
			return Boolean.FALSE;
		}
		
		// skip pharm remark old item 
		// (for safety only, because all DispOrderItem in active order should link to MedOrderItem 
		//  with valid remark item status)
		// (in legacy status of pharm remark old item is set to D in new_dispensed_charge, which 
		//  is not inserted to DC-TRX)
		if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedOrginial || 
				medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete ||
				medOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOldOriginal ||
				medOrderItem.getRemarkItemStatus() == RemarkItemStatus.ConfirmedOriginal ||
				medOrderItem.getRemarkItemStatus() == RemarkItemStatus.Deleted) {
			return Boolean.FALSE;
		}
		
		// check item num length
		if (medOrderItem.getItemNum().toString().length() > ITEM_NUM_LENGTH) {
			throw new Exception("ExportChargingTrxBean: itemNum in medOrderItem is over 3 digits: " +
					"dispHospCode="+ workstore.getHospCode() + ", " +
					"dispOrderNum=" + dispOrder.getDispOrderNum() + ", " +
					"itemNum=" + medOrderItem.getItemNum() + ", " +
					"dispOrderItemId=" + dispOrderItem.getId());
		}
		
		return Boolean.TRUE;
	}
	
    private String getPhsFormatHkid(String hkid) {
		if (hkid == null) {
			return StringUtils.EMPTY;
		}
		
		if (hkid.length() == 8) {
			StringBuilder formatHkid = new StringBuilder();
			formatHkid.append(SPACE);
			formatHkid.append(hkid);
			return formatHkid.toString();
		} else {
			return hkid;
		}
	}
    
	private String getPhsFormatCaseNum(MedCase medCase){
		if (medCase == null || medCase.getCaseNum() == null) {
			return StringUtils.EMPTY;
		}
		
		if (medCase.getCaseNum().length() == 11) {
			StringBuilder formatCaseNum = new StringBuilder();
			formatCaseNum.append(SPACE);
			formatCaseNum.append(medCase.getCaseNum());
			return formatCaseNum.toString();
		} else {
			return medCase.getCaseNum();
		}
	}
	
	private String getPasApptSeq(MedCase medCase) {
		// equivalent to chargeHdr.opasApptSeq, follow setting in OG
		if (medCase != null) {
			if (medCase.getPasApptSeq() != null) {
				return medCase.getPasApptSeq().toString();
			} else {
				return StringUtils.EMPTY;
			}
		} else {
			return "-1";
		}
	}
	
	private String getPasPayFlag(MedCase medCase) {
		// equivalent to chargeHdr.opasPayFlag, follow setting in OG
		if (medCase != null) {
			if (medCase.getPasPayFlagCode() == MedCasePasPayFlagType.Blank) {
				return StringUtils.EMPTY;				
			} else {
				return medCase.getPasPayFlagCode().getDataValue();
			}
		} else {
			return "N";
		}
	}
	
	private String getPasPayCode(MedCase medCase) {
		// equivalent to chargeHdr.opasPayCode, follow setting in OG
		if (medCase != null) {
			return StringUtils.defaultString(medCase.getPasPayCode());
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	private String getMoeOrderNum(MedOrder medOrder) {
		// follow setting of moeOrderNum in legacy new_dispensed_charge
		if (medOrder.getDocType() == MedOrderDocType.Manual) {
			return "0";
		} else {
			return medOrder.getOrderNum().substring(3);
		}
	}

	private String getChargeInd(YesNoBlankFlag cmsChargeFlag) {
		// equivalent to chargeDtl.chargeInd, follow setting in OG
		// (since there is no item with remark status I, R, H, L, D in DC-TRX, no need to hardcode chargeInd to N for that case)
		if (cmsChargeFlag == YesNoBlankFlag.Yes) {
			return "Y";
		} else if (cmsChargeFlag == YesNoBlankFlag.No) { 
			return "N";
		}
			
		// for safety only, chargeFlag = YesNoBlankFlag.Blank case (IP case num order or standard charge flag off) is skipped in DC-TRX
		return StringUtils.EMPTY;
	}
	
	private String getTotalDuration(MedOrderItem medOrderItem) {
		// equivalent to chargeDtl.totalDuration, follow setting in OG
		int totalDuration;
		
		if (medOrderItem.getRxItemType() == RxItemType.Capd) {
			totalDuration = medOrderItem.getCapdRxDrug().getMaxDurationInDay();
		} else {
			// getDurationInDay() is used in OG, but calTotalDurationInDay() need to be used here to recal duration     
			totalDuration = medOrderItem.getRegimen().calTotalDurationInDay();
		}
		
		if (totalDuration > 999) {
			// follow setting of totalDuration in legacy new_dispensed_charge
			return "999";
		} else {
			return String.valueOf(totalDuration);
		}
	}
	
	private String getForceProceed(Boolean forceProceedFlag) {
		if (forceProceedFlag) {
			return "Y";
		} else {
			return "N";
		}
	}	

    public void process(
    		String hospCode,
    		String workstoreCode,
    		List<DispOrderItem> dispOrderItemList,
    		BufferedWriter dctrxWriter,
    		List<String> exceptItemList) throws Exception {
    	
    	Map<StringArray, Boolean> duplicateCheckMap = new HashMap<StringArray, Boolean>();
    	
    	for (DispOrderItem dispOrderItem: dispOrderItemList) {
        	if ( validTransaction(dispOrderItem, exceptItemList, duplicateCheckMap) ) {
    			DispOrder dispOrder = dispOrderItem.getDispOrder();
    			PharmOrder pharmOrder = dispOrder.getPharmOrder();
    			MedOrder medOrder = pharmOrder.getMedOrder();
        		
        		MedCase medCase = pharmOrder.getMedCase();
        		Workstore workstore = dispOrder.getWorkstore();
        		Patient patient = pharmOrder.getPatient();
        		MedOrderItem medOrderItem = dispOrderItem.getPharmOrderItem().getMedOrderItem();

        		// hash MOI item num for duplication check
        		StringArray duplicateCheckKey = new StringArray(medOrder.getOrderNum().toString(), 
						medOrderItem.getItemNum().toString()); 
        		duplicateCheckMap.put(duplicateCheckKey, Boolean.TRUE);

        		
        		StringBuilder transactionLine = new StringBuilder();
        		
        		transactionLine.append(String.format("%-3.3s", StringUtils.defaultString(medOrder.getPatHospCode())))
			 				   .append(String.format("%-3.3s", StringUtils.defaultString(workstore.getHospCode())))
			 				   .append(String.format("%-12.12s", getPhsFormatHkid(patient.getHkid())))
			 				   .append(String.format("%-12.12s", getPhsFormatCaseNum(medCase)))
			 				   .append(String.format("%3d", medOrderItem.getItemNum()))
			 				   .append(String.format("%9.9s", getPasApptSeq(medCase)))
			 				   .append(String.format("%1.1s", getPasPayFlag(medCase)))
			 				   .append(String.format("%-3.3s", getPasPayCode(medCase)))
			 				   .append(String.format("%-20.20s", StringUtils.defaultString(pharmOrder.getReceiptNum())))
			 				   .append(String.format("%9.9s", getMoeOrderNum(medOrder)))
			 				   .append(String.format("%09d", dispOrder.getDispOrderNum()))
			 				   .append(String.format("%1.1s", getChargeInd(medOrderItem.getCmsChargeFlag())))
			 				   .append(String.format("%-3.3s", getTotalDuration(medOrderItem)))
			 				   .append(String.format("%1.1s", getForceProceed(pharmOrder.getForceProceedFlag())))
			 				   .append(DateTimeFormat.forPattern("yyyyMMdd").print(new DateTime(dispOrder.getDispDate())))
			 				   .append(String.format("%-4.4s", StringUtils.defaultString(dispOrder.getSpecCode())));
        		
        		dctrxWriter.write(transactionLine.toString());
        		dctrxWriter.newLine();
        	}
        }
    }

}
