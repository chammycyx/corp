package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.order.ItemBin;
import hk.org.ha.model.pms.corp.persistence.order.ItemBinPK;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("corpMaintService")
@MeasureCalls
public class CorpMaintServiceBean implements CorpMaintServiceLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public void receiveItemBinList(List<ItemLocation> itemLocationList, Workstore workstore) {
		
		HashSet<ItemBin> itemBinSet = new HashSet<ItemBin>();
		for( ItemLocation itemLocation : itemLocationList ){
			ItemBin itemBin = new ItemBin(
					itemLocation.getWorkstore().getHospCode(),
					itemLocation.getWorkstore().getWorkstoreCode(),
					itemLocation.getItemCode(),
					itemLocation.getBinNum());
			itemBinSet.add(itemBin);
		}
		List<ItemBin> inItemBinList = new ArrayList<ItemBin>(itemBinSet);
		
		List<ItemBin> origList = em.createQuery(
				"select o from ItemBin o" + // 20121112 index check : ItemBin.workstore : FK_ITEM_BIN_01
				" where o.workstore = :workstore")
				.setParameter("workstore", workstore)
				.getResultList();	
		
		Map<ItemBinPK, ItemBin> itemBinMap = new HashMap<ItemBinPK, ItemBin>();
		for(ItemBin itemBin : origList){
			ItemBinPK itemBinPK = new ItemBinPK(itemBin.getHospCode(), 
					itemBin.getWorkstoreCode(),
					itemBin.getItemCode(),
					itemBin.getBinNum());
			itemBinMap.put(itemBinPK, itemBin);			
		}
		
		for(ItemBin itemBin : inItemBinList)
		{
			ItemBinPK key = itemBin.getId();
			ItemBin origItemBin = itemBinMap.get(key);
			
			if(origItemBin == null)
			{
				em.persist(itemBin);
			}
			itemBinMap.remove(key);
		}
		em.flush();
		
		for(ItemBin itemBin : itemBinMap.values())
		{
			em.remove(itemBin);
		}	
		em.flush();
	}
}
