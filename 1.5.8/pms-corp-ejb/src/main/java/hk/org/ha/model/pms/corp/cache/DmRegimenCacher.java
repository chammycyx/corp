package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmRegimen;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmRegimenCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmRegimenCacher extends BaseCacher implements DmRegimenCacherInf{

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;

	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}	
	
    public DmRegimen getRegimenByRegimenCode(String regimenCode) {
		return (DmRegimen) this.getCacher().get(regimenCode);
	}

	public List<DmRegimen> getRegimenList() {
		return (List<DmRegimen>) this.getCacher().getAll();
	}

	public List<DmRegimen> getRegimenListByRegimenCode(String prefixRegimenCode) {
		List<DmRegimen> ret = new ArrayList<DmRegimen>();
		boolean found = false;
		for (DmRegimen dmRegimen : getRegimenList()) {
			if (dmRegimen.getRegimenCode().startsWith(prefixRegimenCode)) {
				ret.add(dmRegimen);
				found = true;
			} else {
				if (found) {
					break;
				}
			}
		}
		return ret;
	}

	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmRegimen> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmRegimen create(String regimenCode) {
			this.getAll();
			return this.internalGet(regimenCode);
		}

		@Override
		public Collection<DmRegimen> createAll() {
			return dmsPmsServiceProxy.retrieveDmRegimenList();			
		}

		@Override
		public String retrieveKey(DmRegimen dmRegimen) {
			return dmRegimen.getRegimenCode();
		}
	}

	public static DmRegimenCacherInf instance() {
		if (!Contexts.isApplicationContextActive()) {
			throw new IllegalStateException("No active application scope");
		}
		return (DmRegimenCacherInf) Component.getInstance("dmRegimenCacher", ScopeType.APPLICATION);
	}
}
