package hk.org.ha.model.pms.corp.biz;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface PharmRestServiceLocal {

	@GET
	@Path("initMsWorkstoreDrugCache")
	boolean initMsWorkstoreDrugCache(
			@QueryParam("hospCode") String hospCode, 
			@QueryParam("workstoreCode") String workstoreCode);
}
