package hk.org.ha.model.pms.corp.batch.worker.reftable.phs;

import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.phs.CapdSupplierItem;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.PatientCatPK;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.SpecialtyPK;
import hk.org.ha.model.pms.persistence.phs.Supplier;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.phs.WardPK;
import hk.org.ha.model.pms.persistence.phs.WardStock;
import hk.org.ha.model.pms.persistence.phs.WardStockPK;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("phsDataHelper")
@Scope(ScopeType.APPLICATION)
public class PhsDataHelper {
	
	@Logger
	private Log logger;
		
	@SuppressWarnings("unchecked")
	public void updatePatientCatForPms(EntityManager em, List<PatientCat> newList)
	{
		Map<String, Institution> instMap = retrieveInstitutionList(em);
		
		List<PatientCat> origList = em.createQuery(" SELECT o FROM PatientCat o ").getResultList();	
		Map<PatientCatPK, PatientCat> patientCatMap = new HashMap<PatientCatPK, PatientCat>();
		for(PatientCat patientCat : origList){
			PatientCatPK patientCatPK = new PatientCatPK(patientCat.getInstCode(), patientCat.getPatCatCode());
			patientCatMap.put(patientCatPK, patientCat);			
		}
		
		List<String> vaildInstCodeList = new ArrayList<String>();
		
		for(PatientCat patientCat : newList)
		{
			//check institution exists
			Institution institution = instMap.get(patientCat.getInstCode());
			if( institution == null )
			{
				continue;
			}
			else
			{
				if( !vaildInstCodeList.contains(patientCat.getInstCode()) )
				{
					vaildInstCodeList.add(patientCat.getInstCode());
				}
			}
			
			PatientCatPK key = patientCat.getId();
			PatientCat origPatientCat = patientCatMap.get(key);
			
			if(origPatientCat != null)
			{
				if( !StringUtils.equals(patientCat.getDescription(), origPatientCat.getDescription()) )
				{
					origPatientCat.setDescription(patientCat.getDescription());
				}
				if( patientCat.getStatus() != origPatientCat.getStatus() )
				{
					origPatientCat.setStatus(patientCat.getStatus());
				}
			}
			else
			{
				em.persist(patientCat);
			}
			patientCatMap.remove(key);
		}
		
		for(PatientCat patientCat : patientCatMap.values())
		{
			patientCat.setStatus(RecordStatus.Delete);
		}		
		
		if( !vaildInstCodeList.isEmpty() )
		{
			logger.info(StringUtils.join(vaildInstCodeList.toArray(), ", ")+" data updated.");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateWardForPms(EntityManager em, List<Ward> newList)
	{
		Map<String, Institution> instMap = retrieveInstitutionList(em);
	
		List<Ward> origList = em.createQuery(" SELECT o FROM Ward o ").getResultList();	
		Map<WardPK, Ward> wardMap = new HashMap<WardPK, Ward>();
		for(Ward ward : origList){
			WardPK wardPK = new WardPK(ward.getInstCode(), ward.getWardCode());
			wardMap.put(wardPK, ward);			
		}
		
		List<String> vaildInstCodeList = new ArrayList<String>();
		
		for(Ward ward : newList)
		{
			//check workstoreGroup exists
			Institution institution = instMap.get(ward.getInstCode());
			if( institution == null )
			{
				continue;
			}
			else
			{
				if( !vaildInstCodeList.contains(ward.getInstCode()) )
				{
					vaildInstCodeList.add(ward.getInstCode());
				}
			}
			
			WardPK key = new WardPK(ward.getInstCode(), ward.getWardCode());
			Ward origWard = wardMap.get(key);
			if(origWard != null)
			{
				if( !StringUtils.equals(ward.getDescription(), origWard.getDescription()) )
				{
					origWard.setDescription(ward.getDescription());
				}
				if( ward.getStatus() != origWard.getStatus() )
				{
					origWard.setStatus(ward.getStatus());
				}
			}
			else
			{
				em.persist(ward);
			}
			wardMap.remove(key);
		}
		
		for(Ward ward : wardMap.values())
		{
			ward.setStatus(RecordStatus.Delete);
		}	
		
		if( !vaildInstCodeList.isEmpty() )
		{
			logger.info(StringUtils.join(vaildInstCodeList.toArray(), ", ")+" data updated.");
		}
	}

	@SuppressWarnings("unchecked")
	public void updateSpecialtyForPms(EntityManager em, List<Specialty> newList)
	{
		Map<String, Institution> instMap = retrieveInstitutionList(em);
		
		List<Specialty> origList = em.createQuery(" SELECT o FROM Specialty o ").getResultList();
		Map<SpecialtyPK, Specialty> specialtyMap = new HashMap<SpecialtyPK, Specialty>();
		for(Specialty specialty : origList)
		{
			SpecialtyPK specialtyPK = new SpecialtyPK(specialty.getInstCode(), specialty.getSpecCode());
			specialtyMap.put(specialtyPK, specialty);			
		}
		
		List<String> vaildInstCodeList = new ArrayList<String>();
		
		for(Specialty specialty : newList)
		{
			//check workstoreGroup exists
			Institution institution = instMap.get(specialty.getInstCode());
			if( institution == null )
			{
				continue;
			}
			else
			{
				if( !vaildInstCodeList.contains(specialty.getInstCode()) ){
					vaildInstCodeList.add(specialty.getInstCode());
				}
			}
			
			SpecialtyPK key = specialty.getId();
			Specialty origSpecialty = specialtyMap.get(key);
			
			if(origSpecialty != null)
			{
				if( !StringUtils.equals(specialty.getDescription(), origSpecialty.getDescription()) )
				{
					origSpecialty.setDescription(specialty.getDescription());
				}
				if( specialty.getStatus() != origSpecialty.getStatus() )
				{
					origSpecialty.setStatus(specialty.getStatus());
				}
			}
			else
			{
				em.persist(specialty);
			}
			
			specialtyMap.remove(key);
		}
		for(Specialty specialty : specialtyMap.values())
		{
			specialty.setStatus(RecordStatus.Delete);
		}		
		
		if( !vaildInstCodeList.isEmpty() )
		{
			logger.info(StringUtils.join(vaildInstCodeList.toArray(), ", ")+" data updated.");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateCapdSupplierItemForPms(EntityManager em, List<CapdSupplierItem> newList)
	{
		Map<String, Supplier> supplierMap = retrieveSupplierList(em);
		
		List<CapdSupplierItem> origList = em.createQuery(" SELECT o FROM CapdSupplierItem o ").getResultList();
		Map<String, CapdSupplierItem> capdSupplierItemMap = new HashMap<String, CapdSupplierItem>();
		for(CapdSupplierItem capdSupplierItem : origList)
		{
			capdSupplierItemMap.put(capdSupplierItem.getItemCode(), capdSupplierItem);			
		}
		
		List<String> invaildSupplierCodeList = new ArrayList<String>();
		
		for(CapdSupplierItem capdSupplierItem : newList)
		{
			//check supplier exists
			Supplier supplier = supplierMap.get(capdSupplierItem.getSupplierCode());
			if( supplier == null )
			{
				if( !invaildSupplierCodeList.contains(capdSupplierItem.getSupplierCode()) )
				{
					invaildSupplierCodeList.add(capdSupplierItem.getSupplierCode());
				}
				continue;
			}
			
			CapdSupplierItem origCapdSupplierItem = capdSupplierItemMap.get(capdSupplierItem.getItemCode());
			if(origCapdSupplierItem != null)
			{
				if( !StringUtils.equals(capdSupplierItem.getSupplierCode(), origCapdSupplierItem.getSupplierCode()) )
				{
					origCapdSupplierItem.setSupplierCode(capdSupplierItem.getSupplierCode());
				}
				if( capdSupplierItem.getStatus() != origCapdSupplierItem.getStatus() )
				{
					origCapdSupplierItem.setStatus(capdSupplierItem.getStatus());
				}
			}
			else
			{
				em.persist(capdSupplierItem);
			}
			
			capdSupplierItemMap.remove(capdSupplierItem.getItemCode());
		}

		for(CapdSupplierItem capdSupplierItem : capdSupplierItemMap.values())
		{
			capdSupplierItem.setStatus(RecordStatus.Delete);
		}	
		
		if( !invaildSupplierCodeList.isEmpty() )
		{
			logger.error(StringUtils.join(invaildSupplierCodeList.toArray(), ", ")+" not exists in Supplier table in database.");
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void updateWardStockForPms(EntityManager em, List<WardStock> newList, String instCode)
	{
		Map<WardStockPK, WardStock> wardStockMap = new HashMap<WardStockPK, WardStock>();
		for(WardStock wardStock : (List<WardStock>) em.createQuery(" SELECT o FROM WardStock o where o.instCode = :instCode")
				.setParameter("instCode", instCode)
				.getResultList() )
		{
			WardStockPK key = wardStock.getId();
			wardStockMap.put(key, wardStock);			
		}

		List<Ward> wardList = em.createQuery(" SELECT o FROM Ward o ").getResultList();	
		Map<WardPK, Ward> wardMap = new HashMap<WardPK, Ward>();		
		for(Ward ward : wardList){
			WardPK key = new WardPK(ward.getInstCode(), ward.getWardCode());
			wardMap.put(key, ward);			
		}

		List<String> invaildWardCodeList = new ArrayList<String>();

		for(WardStock wardStock : newList)
		{
			//check ward exists
			WardPK wardKey = new WardPK(wardStock.getInstCode(), wardStock.getWardCode());
			Ward ward = wardMap.get(wardKey);
			if( ward == null ){
				if( !invaildWardCodeList.contains(wardStock.getInstCode()+"-"+wardStock.getWardCode()) )
				{
					invaildWardCodeList.add(wardStock.getInstCode()+"-"+wardStock.getWardCode());
				}
				continue;
			}
			
			WardStockPK key = wardStock.getId();			
			WardStock origWardStock = wardStockMap.get(key);
			
			if(origWardStock != null)
			{
				if (wardStock.getStatus() != origWardStock.getStatus())
				{
					origWardStock.setStatus(wardStock.getStatus());
				}
			}
			else
			{
				em.persist(wardStock);
			}

			wardStockMap.remove(key);
		}
		
		for(WardStock wardStock : wardStockMap.values())
		{
			wardStock.setStatus(RecordStatus.Delete);
		}	
		
		if( !invaildWardCodeList.isEmpty() )
		{
			logger.error(StringUtils.join(invaildWardCodeList.toArray(), ", ")+" not exists in Ward table in PMS Corporate database.");
		}
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Institution> retrieveInstitutionList(EntityManager em){
		List<Institution> instList = em.createQuery("select o from Institution o") // 20160616 index check : none
				.getResultList();
		Map<String, Institution> instMap = new HashMap<String, Institution>();
		for( Institution inst : instList ){
			instMap.put(inst.getInstCode(), inst);
		}
		return instMap;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Supplier> retrieveSupplierList(EntityManager em){
		List<Supplier> supplierList = em.createQuery("select o from Supplier o") // 20160616 index check : none
				.getResultList();
		Map<String, Supplier> supplierMap = new HashMap<String, Supplier>();
		for( Supplier supplier : supplierList ){
			supplierMap.put(supplier.getSupplierCode(), supplier);
		}
		return supplierMap;
	}

}
