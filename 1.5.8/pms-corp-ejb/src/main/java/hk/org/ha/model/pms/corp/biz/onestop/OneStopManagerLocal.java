package hk.org.ha.model.pms.corp.biz.onestop;

import java.util.List;

import hk.org.ha.model.pms.vo.onestop.OneStopOrderItemSummary;
import hk.org.ha.model.pms.vo.onestop.OneStopSummary;

import javax.ejb.Local;

@Local
public interface OneStopManagerLocal {
	
	List<OneStopOrderItemSummary> retrieveOrderByCaseNum(String caseNum);
	
}
