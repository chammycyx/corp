package hk.org.ha.model.pms.corp.persistence.order;

import hk.org.ha.model.pms.persistence.BaseEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CORPORATE_TASK")
public class CorporateTask extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "TASK_NAME", length = 100)
	private String taskName;
	
	@Column(name = "TASK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date taskDate;
		
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Date getTaskDate() {
		if (taskDate == null) {
			return null;
		}
		else {
			return new Date(taskDate.getTime());
		}
	}
	
	public void setTaskDate(Date taskDate) {
		if (taskDate == null) {
			this.taskDate = null;
		}
		else {
			this.taskDate = new Date(taskDate.getTime());
		}
	}	
}
