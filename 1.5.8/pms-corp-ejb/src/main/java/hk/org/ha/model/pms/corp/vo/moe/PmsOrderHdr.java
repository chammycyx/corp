package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;
import java.util.Date;

public class PmsOrderHdr implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String moeOrder;
	private String uncollect;
	private String suspend;
	private String ordStatus;
	private String allowModify;
	private String allowPostComment;
	private String ordSubtype;
	private Date lastUpdDate;
	private String remarkStatus;
	
	public String getMoeOrder() { return moeOrder;}
	public void setMoeOrder(String moeOrder) { this.moeOrder= moeOrder;}
	
	public String getUncollect() { return uncollect;}
	public void setUncollect (String uncollect) { this.uncollect = uncollect;}
	
	public String getSuspend() { return suspend;}
	public void setSuspend (String suspend) { this.suspend = suspend;}
	
	public String getOrdStatus() { return ordStatus;}
	public void setOrdStatus(String ordStatus) { this.ordStatus = ordStatus;}
	
	public String getAllowModify() { return allowModify;}
	public void setAllowModify(String allowModify) { this.allowModify = allowModify;}
	
	public String getAllowPostComment() { return allowPostComment;}
	public void setAllowPostComment(String allowPostComment) { this.allowPostComment = allowPostComment;}
	
	public String getOrdSubtype() { return ordSubtype;}
	public void setOrdSubtype(String ordSubtype) { this.ordSubtype = ordSubtype;}

	public Date getLastUpdDate() {return lastUpdDate == null ? null : new Date(lastUpdDate.getTime()); }
	public void setLastUpdDate(Date lastUpdDate) { this.lastUpdDate = (lastUpdDate == null ? null : new Date(lastUpdDate.getTime()));}

	public String getRemarkStatus() { return remarkStatus;}
	public void setRemarkStatus(String remarkStatus) { this.remarkStatus= remarkStatus;}
	
}
