package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.persistence.order.SeqTable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("seqNumService")
@MeasureCalls
public class SeqNumServiceBean implements SeqNumServiceLocal {

	private static final String REF_SEQ_NAME = "REF_NUM";

	private static final String DISP_SEQ_NAME = "DISP_ORDER_NUM";
		
	private static final String ORDER_SEQ_NAME = "ORDER_NUM";

	private static final String CHARGE_ORDER_SEQ_NAME = "CHARGE_ORDER_NUM";
	
	private static final String REF_NO_SUFFIX = "abcdefghijklmnopqrstuvwxyz";
	
	private static final String DELIMITER = "_";
	
	private static final int REF_NO_PREFIX_LEN = 4;

	private DateFormat fullYearDateFormat = new SimpleDateFormat("yyyy");
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String retrieveOrderNum(String hospCode, String patHospCode) {
		return retrieveOrderNum(ORDER_SEQ_NAME, hospCode, patHospCode);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String retrieveChargeOrderNum(String hospCode, String patHospCode) {
		return retrieveOrderNum(CHARGE_ORDER_SEQ_NAME, hospCode, patHospCode);
	}
	
	private String retrieveOrderNum(String prefix, String hospCode, String patHospCode) {

		String seqName;
		if (StringUtils.equals(hospCode, patHospCode)) {
			seqName = prefix + DELIMITER +patHospCode;
		}
		else {
			seqName = prefix;
		}
		
		SeqTable orderSeq = this.retrieveSeqTable(seqName);

		if (!fullYearDateFormat.format(new Date()).equals(fullYearDateFormat.format(orderSeq.getUpdateDate()))) {
			orderSeq.setCurrNum(orderSeq.getMinNum());
		}
		
		StringBuilder sb = new StringBuilder();

		sb.append(StringUtils.rightPad(patHospCode, 3, ' '));
		if( StringUtils.equals(prefix, CHARGE_ORDER_SEQ_NAME)){
			sb.append(StringUtils.leftPad(String.valueOf(orderSeq.getCurrNum()), 9, '0'));
		}else{
			sb.append(StringUtils.right(String.valueOf(GregorianCalendar.getInstance().get(GregorianCalendar.YEAR)), 2));
			sb.append(StringUtils.leftPad(String.valueOf(orderSeq.getCurrNum()), 7, '0'));
		}

		if (orderSeq.getCurrNum() >= orderSeq.getMaxNum()) {
			orderSeq.setCurrNum(orderSeq.getMinNum());
		}
		else {
			orderSeq.setCurrNum(orderSeq.getCurrNum() + 1);
		}

		return sb.toString();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String retrieveRefNum() {

		SeqTable refSeq = this.retrieveSeqTable(REF_SEQ_NAME);

		String refNum = StringUtils.leftPad(String.valueOf(refSeq.getCurrNum()), 4, '0');

		if (refSeq.getCurrNum() >= refSeq.getMaxNum()) {
			refSeq.setCurrNum(refSeq.getMinNum());
		}
		else {
			refSeq.setCurrNum(refSeq.getCurrNum() + 1);
		}

		return refNum;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer retrieveDispOrderNum() {
	
		SeqTable dispSeq = this.retrieveSeqTable(DISP_SEQ_NAME);
		Long dispOrderNum = dispSeq.getCurrNum();

		if (dispSeq.getCurrNum() >= dispSeq.getMaxNum()) {
			dispSeq.setCurrNum(dispSeq.getMinNum());
		}
		else {
			dispSeq.setCurrNum(dispSeq.getCurrNum() + 1);
		}
		
		return Integer.valueOf(dispOrderNum.intValue());
	}
	
	private SeqTable retrieveSeqTable(String seqName) 
	{
		return (SeqTable) em.createQuery(
			"select o from SeqTable o" + // 20120713 index check : SeqTable.seqName : PK_SEQ_TABLE
			" where o.seqName = :seqName" +
			" and o.seqName is not null") // don't delete : magic code fix bug for https://bugs.eclipse.org/bugs/show_bug.cgi?id=347592
			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
			.setParameter("seqName", seqName)
			.getSingleResult();
	}

	public String retrieveNextRefNum(String refNum) {

		char suffix;

		if (refNum.length() > REF_NO_PREFIX_LEN) {
			suffix = refNum.charAt(REF_NO_PREFIX_LEN);

			int index = REF_NO_SUFFIX.indexOf(suffix) + 1;
			if (index >= REF_NO_SUFFIX.length()) {
				index = 0;
			}

			suffix = REF_NO_SUFFIX.charAt(index);
		}
		else {
			suffix = REF_NO_SUFFIX.charAt(0);
		}

		return refNum.substring(0, REF_NO_PREFIX_LEN) + suffix;
	}
}
