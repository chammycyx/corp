package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmRoute;

import java.util.List;

public interface DmRouteCacherInf extends BaseCacherInf {

    DmRoute getRouteByRouteCode(String routeCode);

	List<DmRoute> getRouteList();

	List<String> getDistinctRouteDesc();
}
