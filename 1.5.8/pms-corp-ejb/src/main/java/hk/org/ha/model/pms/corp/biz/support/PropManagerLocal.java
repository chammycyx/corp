package hk.org.ha.model.pms.corp.biz.support;

import hk.org.ha.model.pms.persistence.reftable.CorporateProp;

import java.util.List;

import javax.ejb.Local;

@Local
public interface PropManagerLocal  {
	
	List<CorporateProp> retrieveCorporatePropList();
	
	void updateCorporatePropList(List<CorporateProp> corporatePropList, String auditLoggerMessageId);
}
