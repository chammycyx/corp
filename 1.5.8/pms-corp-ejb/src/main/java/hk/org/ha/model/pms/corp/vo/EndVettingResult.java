package hk.org.ha.model.pms.corp.vo;

import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;

import java.io.Serializable;
import java.util.Map;

public class EndVettingResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private PharmOrder pharmOrder;
	
	private Map<Long, DeltaChangeType> deltaChangeTypeMap;
	
	private Boolean successFlag;
	
	private String messageCode;
	
	private String[] messageParam;
	
	public EndVettingResult() {
		successFlag = Boolean.TRUE;
	}
	
	public PharmOrder getPharmOrder() {
		return pharmOrder;
	}

	public void setPharmOrder(PharmOrder pharmOrder) {
		this.pharmOrder = pharmOrder;
	}

	public Boolean getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String[] getMessageParam() {
		return messageParam;
	}
	
	public void setMessageParam(String[] messageParam) {
		this.messageParam = messageParam;
	}

	public Map<Long, DeltaChangeType> getDeltaChangeTypeMap() {
		return deltaChangeTypeMap;
	}

	public void setDeltaChangeTypeMap(Map<Long, DeltaChangeType> deltaChangeTypeMap) {
		this.deltaChangeTypeMap = deltaChangeTypeMap;
	}
}
