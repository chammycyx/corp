package hk.org.ha.model.pms.corp.biz.medprofile;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("corpPmsSignOrderCoordinator")
@MeasureCalls
public class CorpPmsSignOrderCoordinator implements CorpPmsSignOrderCoordinatorLocal {

	@In
	private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	private static Map<String, List<SignatureCountResult>> pmsSignatureCountResultRequestMap = new HashMap<String, List<SignatureCountResult>>();	
	
	private long waitTime = 1000L; 
	
	public long getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(long waitTime) {
		this.waitTime = waitTime;
	}
	
	@Override
	public void consolidatePmsSignatureCountResult(String requestId, List<SignatureCountResult> signatureCountResultList) {
		synchronized (pmsSignatureCountResultRequestMap) {
			List<SignatureCountResult> list = pmsSignatureCountResultRequestMap.get(requestId);
			if (list != null) {
				list.addAll(signatureCountResultList);
			}
		}
	}

	@Override
	public List<SignatureCountResult> retrievePmsSignatureCountResultList(String requestId, Date batchDate) {
		synchronized (pmsSignatureCountResultRequestMap) {
			pmsSignatureCountResultRequestMap.put(requestId, new ArrayList<SignatureCountResult>());
		}
		
		pmsSubscriberProxy.requestPmsSignatureCountResult(requestId, batchDate, new Date());
		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
		}

		synchronized (pmsSignatureCountResultRequestMap) {
			return pmsSignatureCountResultRequestMap.remove(requestId);
		}	
	}

}
