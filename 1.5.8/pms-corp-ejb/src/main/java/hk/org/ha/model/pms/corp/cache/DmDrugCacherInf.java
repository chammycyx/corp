package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;

import java.util.List;

public interface DmDrugCacherInf extends BaseCacherInf {

	MsWorkstoreDrug getMsWorkstoreDrug(Workstore workstore, String itemCode);

	List<MsWorkstoreDrug> getMsWorkstoreDrugList(Workstore workstore);	

	DmDrug getDmDrug(String itemCode);	
	
	List<DmDrug> getDmDrugList();

	List<DmDrug> getDmDrugList(Workstore workstore, String prefixItemCode);

	List<DmDrug> getDmDrugList(Workstore workstore, String prefixItemCode, Boolean suspend, Boolean fdn);
	
	List<DmDrug> getDmDrugListByFullDrugDesc(Workstore workstore, String prefixFullDrugDesc);
	
	List<DmDrug> getDmDrugListByDisplayNameFormSaltFmStatus(Workstore workstore, String displayName, String formCode, String saltProperty, String fmStatus);	
	
	List<DmDrug> getDmDrugListByDrugKey(Integer drugKey);
	
	List<DmDrug> getDmDrugListByDrugKey(Workstore workstore, Integer drugKey);
	
	List<DmDrug> getDmDrugListByDrugKeyFmStatus(Workstore workstore, Integer drugKey, String fmStatus);
	
	List<DmDrug> getDmDrugListByDisplayNameFmStatus(Workstore workstore, String displayName, String fmStatus);
	
	List<DmDrug> getDmDrugListByDisplayName(Workstore workstore, String displayName);
	
	List<DmDrug> getDmDrugListByDiluentCode(Workstore workstore, String diluentCode);
	
	List<DmDrug> getDmDrugListBySolvent(Workstore workstore);	
	
	List<DmDrug> getChestItemList(Workstore workstore, String prefixItemCode);
	
	void clearDmDrugCache();
	
	void clearMsWorkstoreDrugCache();
	
	void clearMsWorkstoreDrugCache(Workstore workstore);
	
	void initDmDrugCache(List<DmDrug> dmDrugList);	
}
