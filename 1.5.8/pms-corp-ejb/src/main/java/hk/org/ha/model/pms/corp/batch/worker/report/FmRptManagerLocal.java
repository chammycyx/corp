package hk.org.ha.model.pms.corp.batch.worker.report;

import hk.org.ha.model.pms.corp.vo.report.FmReportInfo;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.joda.time.DateTime;


@Local
public interface FmRptManagerLocal  {
	
	long processBeginTime();
	
	long totalPorcessTime(long startTime);
		
	void generateMonthlyHospSpecStatReport(DateTime batchStartDate, String patHospCode, String jobId) throws Exception;
	
	void generateMonthlyHospStatReport(DateTime batchStartDate, String patHospCode, String jobId) throws Exception;
	
	String generateYearlyAccumulatedHospStatReport(DateTime batchStartDate, String patHospCode, String jobId) throws Exception;
	
	void generateMonthlyClusterSpecStatReport(DateTime batchStartDate, String clusterCode, String jobId) throws Exception;
	
	void generateMonthlyClusterStatReport(DateTime batchStartDate, String clusterCode, String jobId) throws Exception;
	
	void generateYearlyAccumulatedClusterStatReport(DateTime batchStartDate, String clusterCode, String jobId) throws Exception;
	
	void generateMonthlyCorpSpecStatReport(DateTime batchStartDate, String jobId) throws Exception;
	
	void generateMonthlyCorpStatReport(DateTime batchStartDate, String jobId) throws Exception;
	
	void generateYearlyAccumulatedCorpStatReport(DateTime batchStartDate, String jobId) throws Exception;
	
	void uploadReports(DateTime batchStartDate) throws Exception;
		
	void createMonthlyDrugDetailsReport(DateTime batchStartDate, String patHospCode, String clusterCode, String jobId, File nonGenDrugData) throws Exception;
	
	void destory();
	
	Map<String, File> generateDailyDataCsvData(List<FmReportInfo> nonGeneralDrugList, List<FmReportInfo> allDrugList, 
			Map<String, File> dataFileMap, DateTime batchStartDate, String patHospCode, 
			String clusterCode, String jobId) throws Exception;
}
