package hk.org.ha.model.pms.corp.batch.worker.dayend;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.biz.asp.AspReftableManagerLocal;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.asp.persistence.AspHospItem;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.service.biz.pms.asp.interfaces.AspServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("aspDispManager")
@MeasureCalls
public class AspDispManagerBean implements AspDispManagerLocal {
	
	@In
	private AspReftableManagerLocal aspReftableManager;
	
	@Override
	public void filterAndSend(String hospCode, String workstoreCode, List<DispOrderItem> dispOrderItemList, Date batchDate) throws RuntimeException {
		
		List<DispOrderItem> dispOrderItemFilteredList = new ArrayList<DispOrderItem>();
		AspHosp aspHosp = aspReftableManager.retrieveAspHospByHospCode(hospCode);
		
		if(aspHosp == null)
		{
			return;
		}
		
		for(String excludeWorkstore:aspHosp.getExcludeWorkstores())
		{
			if(workstoreCode.equals(excludeWorkstore))
			{
				return;
			}
		}
		
		Map<String, Map<AspType, AspHospItem>> aspHospItemCodeMap = aspReftableManager.constructAspHospItemCodeMap(aspHosp.getAspHospItemList());

		String g6pdCode = aspHosp.getG6pdCode();

		if(aspHospItemCodeMap != null)
		{	
			for(DispOrderItem dispOrderItem: dispOrderItemList)
			{
				if(OrderType.InPatient.equals(dispOrderItem.getDispOrder().getOrderType()))
				{
					String itemCode = dispOrderItem.getPharmOrderItem().getItemCode();

					if(aspHospItemCodeMap.get(itemCode) == null)
					{
						continue;
					}
					
					List<AspHospItem> aspHospItemList = new ArrayList<AspHospItem>(aspHospItemCodeMap.get(itemCode).values());
					if(aspHospItemList.size() > 1)
					{
						dispOrderItem.setAspItemType(AspType.NONE.getDataValue());
					}
					else
					{
						dispOrderItem.setAspItemType(aspHospItemList.get(0).getAspType().getDataValue());
					}
					
					dispOrderItemFilteredList.add(dispOrderItem);
				}	
			}
		}
		
		if(!dispOrderItemFilteredList.isEmpty())
		{
			this.getAspServiceProxy().receiveDispOrderItemList(dispOrderItemFilteredList, null, g6pdCode, batchDate);
		}
	}
	
	private AspServiceJmsRemote getAspServiceProxy()
	{
		return (AspServiceJmsRemote)Component.getInstance("aspServiceProxy", false);
	}

}
