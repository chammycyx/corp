package hk.org.ha.model.pms.corp.cache;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Name("cacherHelper")
public class CacherHelper {
			
	@In
	private DmAdminTimeCacherInf dmAdminTimeCacher;
	
	@In
	private DmBnfCacherInf dmBnfCacher;

	@In
	private DmCorpIndicationCacherInf dmCorpIndicationCacher;

	@In
	private DmDailyFrequencyCacherInf dmDailyFrequencyCacher;
	
	@In
	private DmDrugBnfCacherInf dmDrugBnfCacher;
	
	@In
	private DmFormCacherInf dmFormCacher;
	
	@In
	private DmFormDosageUnitMappingCacherInf dmFormDosageUnitMappingCacher;
	
	@In
	private DmFormVerbMappingCacherInf dmFormVerbMappingCacher;
	
	@In
	private DmFrequencyCacherInf dmFrequencyCacher;
	
	@In
	private DmMedicalOfficerCacherInf dmMedicalOfficerCacher;
	
	@In
	private DmRegimenCacherInf dmRegimenCacher;
	
	@In
	private DmRouteCacherInf dmRouteCacher;
	
	@In
	private DmSiteCacherInf dmSiteCacher;
	
	@In
	private DmSupplFrequencyCacherInf dmSupplFrequencyCacher;
	
	@In
	private DmSupplIndicationCacherInf dmSupplIndicationCacher;

	@In
	private DmSupplSiteCacherInf dmSupplSiteCacher;
	
	@In
	private DmWarningCacherInf dmWarningCacher;
	
	@In
	private MsFmStatusCacherInf msFmStatusCacher;
	
	@In
	private SiteFormVerbCacherInf siteFormVerbCacher;
	
	@In
	private FormVerbCacherInf formVerbCacher;
	
	@In
	private SiteCacherInf siteCacher;
	
	public void clearDmInfo() {
		dmAdminTimeCacher.clear();
		dmBnfCacher.clear();
		dmCorpIndicationCacher.clear();
		dmDailyFrequencyCacher.clear();
		dmDrugBnfCacher.clear();
		dmFormCacher.clear();
		dmFormDosageUnitMappingCacher.clear();
		dmFormVerbMappingCacher.clear();
		dmFrequencyCacher.clear();
		dmMedicalOfficerCacher.clear();
		dmRegimenCacher.clear();
		dmRouteCacher.clear();
		dmSiteCacher.clear();
		dmSupplFrequencyCacher.clear();
		dmSupplIndicationCacher.clear();
		dmSupplSiteCacher.clear();
		dmWarningCacher.clear();
		msFmStatusCacher.clear();
		siteFormVerbCacher.clear();		
		formVerbCacher.clear();
		siteCacher.clear();
	}
}
