package hk.org.ha.model.pms.corp.biz.charging;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.InvoiceItem;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.enquiry.SfiInvoiceEnqPatType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("invoiceManager")
@Scope(ScopeType.STATELESS)
public class InvoiceManagerBean implements InvoiceManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private InvoiceHelper invoiceHelper;
	
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
	private static final String MP_SFI_INVOICE_NUM = "0000";
	private static final String MP_SFI_INVOICE_DATE_STR = "2000-01-01";
	
	public Invoice retrieveInvoiceByInvoiceId(Long invoiceId){
		Invoice invoice = invoiceHelper.retrieveInvoiceByInvoiceId(em, invoiceId);
		if ( invoice != null ) {
			invoice.loadChild(true);
		}
		return invoice;
	}
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceNum(String invoiceNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceHelper.retrieveSfiInvoiceItemListByInvoiceNum(em, invoiceNum, includeVoidInvoice, orderType, workstore, patType);
	}
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByInvoiceDate(Date invoiceDate, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceHelper.retrieveSfiInvoiceItemListByInvoiceDate(em, invoiceDate, includeVoidInvoice, orderType, workstore, patType);
	}
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByCaseNum(String caseNum, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceHelper.retrieveSfiInvoiceItemListByCaseNum(em, caseNum, includeVoidInvoice, orderType, workstore, patType);
	}	
	
	public List<InvoiceItem> retrieveSfiInvoiceItemListByHkid(String hkid, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceHelper.retrieveSfiInvoiceItemListByHkid(em, hkid, includeVoidInvoice, orderType, workstore, patType);
	}	

	public List<InvoiceItem> retrieveSfiInvoiceItemListByExtactionPeriod(Integer dayRange, boolean includeVoidInvoice, OrderType orderType, Workstore workstore, SfiInvoiceEnqPatType patType) {
		return invoiceHelper.retrieveSfiInvoiceItemListByExtactionPeriod(em, dayRange, includeVoidInvoice, orderType, workstore, patType);
	}
	
	public void updateInvoicePaymentStatus(List<Long> invoiceIdList, Map<String, FcsPaymentStatus> fcsPaymentStatusMap){
		List<Invoice> invoiceList = new ArrayList<Invoice>();
		if ( invoiceIdList.isEmpty() ) {
			invoiceList = invoiceHelper.retrieveSfiInvoiceListByInvoiceNumList(em, new ArrayList<String>(fcsPaymentStatusMap.keySet()));
		} else {
			invoiceList = invoiceHelper.retrieveSfiInvoiceListById(em, invoiceIdList);
		}
		for ( Invoice invoice : invoiceList ) {
			FcsPaymentStatus invoicePaymentStatus = fcsPaymentStatusMap.get(invoice.getInvoiceNum());
			if ( invoicePaymentStatus != null ) {
				invoice.setReceiptNum(invoicePaymentStatus.getReceiptNum());					
				invoice.setReceiveAmount(new BigDecimal(invoicePaymentStatus.getReceiptAmount()));
				invoice.setStatus(InvoiceStatus.Settled);	
				invoice.setFcsCheckDate(new DateTime().toDate());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Invoice> retrieveInvoiceByInvoiceNumWoPurchaseRequest(String invoiceNum, String hospCode) {
		List<Invoice> invoiceList = new ArrayList<Invoice>();
		try {
			invoiceList = em.createQuery(
					"select o from Invoice o" + // 20150422 index check : Invoice.invoiceNum : I_INVOICE_01
					" where o.dispOrder.hospCode = :hospCode" +
					" and o.dispOrder.ticketNum = :ticketNum" +
					" and o.dispOrder.ticketDate = :ticketDate" +
					" and o.dispOrder.orderType = :orderType" +
					" and o.invoiceNum = :invoiceNum" +
					" and o.status <> :sysDeleteStatus")
					.setParameter("hospCode", hospCode)
					.setParameter("ticketNum", MP_SFI_INVOICE_NUM)
					.setParameter("ticketDate", df.parse(MP_SFI_INVOICE_DATE_STR), TemporalType.DATE)
					.setParameter("orderType", OrderType.InPatient)
					.setParameter("invoiceNum", invoiceNum)
					.setParameter("sysDeleteStatus", InvoiceStatus.SysDeleted)
					.getResultList();
		} catch (ParseException e) {
			logger.error("Invalid date format #0", MP_SFI_INVOICE_DATE_STR);
		}
		
		for ( Invoice invoice : invoiceList ) {
			invoice.getDispOrder().getPharmOrder().getMedOrder();
			invoice.getDispOrder().getPharmOrder().getPatient();
			invoice.getDispOrder().getPharmOrder().getMedCase();
			for ( InvoiceItem invoiceItem : invoice.getInvoiceItemList() ) {
				invoiceItem.getDispOrderItem().getPharmOrderItem().getMedOrderItem();
			}
		}
		
		return invoiceList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Invoice> retrieveInvoiceListWoPurchaseRequest(String hkid, String caseNum, String hospCode) {
		Date invoiceDate = new Date();
		try {
			invoiceDate = df.parse(MP_SFI_INVOICE_DATE_STR);
		} catch (ParseException e) {
			logger.error("Invalid date format #0", MP_SFI_INVOICE_DATE_STR);
		}
		
		StringBuilder sb = new StringBuilder(
				"select o from Invoice o" + // 20150422 index check : DispOrder.hospCode,ticketDate,patient MedCase.caseNum : I_DISP_ORDER_19 I_MED_CASE_01
				" where o.dispOrder.hospCode = :hospCode" + 
				" and o.dispOrder.ticketNum = :ticketNum" +
				" and o.dispOrder.ticketDate = :ticketDate" +
				" and o.dispOrder.orderType = :orderType" +
				" and o.status <> :sysDeleteStatus" );
		
		if ( StringUtils.isNotBlank(hkid) ) {
			sb.append(" and o.dispOrder.patient.hkid = :hkid");
		} else {
			sb.append(" and o.dispOrder.pharmOrder.medCase.caseNum = :caseNum");
		}
		sb.append(" order by o.invoiceDate desc, o.createDate desc");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hospCode", hospCode);
		query.setParameter("ticketNum", MP_SFI_INVOICE_NUM);
		query.setParameter("ticketDate", invoiceDate, TemporalType.DATE);
		query.setParameter("orderType", OrderType.InPatient);
		query.setParameter("sysDeleteStatus", InvoiceStatus.SysDeleted);
		 
		if ( StringUtils.isNotBlank(hkid) ) {
			query.setParameter("hkid", hkid);
		} else {
			query.setParameter("caseNum", caseNum);
		}
		
		List<Invoice> invoiceList = query.getResultList();
		for ( Invoice invoice : invoiceList ) {
			invoice.getDispOrder().getPharmOrder().getPatient();
			invoice.getDispOrder().getPharmOrder().getMedCase();
		}
		
		return invoiceList;
	}
	
	@SuppressWarnings("unchecked")
	public Map<Long, String> retrieveInvoiceNumByDeliveryItemId(List<Long> deliveryItemIdList){
		Map<Long, String> invoiceNumMap = new HashMap<Long, String>();
		if ( deliveryItemIdList.isEmpty() ) {
			return invoiceNumMap;
		}		
		List<InvoiceItem> invoiceItemList = em.createQuery(
				"select o from InvoiceItem o" + // 20150422 index check : DispOrderItem.deliveryItemId : I_DISP_ORDER_ITEM_01
				" where o.dispOrderItem.deliveryItemId in :deliveryItemIdList")
				.setParameter("deliveryItemIdList", deliveryItemIdList)
				.getResultList();

		for ( InvoiceItem invoiceItem : invoiceItemList ) {
			if (!InvoiceStatus.Outstanding_Settle.contains(invoiceItem.getInvoice().getStatus())) {
    			continue;
    		}
			invoiceNumMap.put(invoiceItem.getDispOrderItem().getDeliveryItemId(), invoiceItem.getInvoice().getInvoiceNum());
		}
		
		return invoiceNumMap;
	}
}
