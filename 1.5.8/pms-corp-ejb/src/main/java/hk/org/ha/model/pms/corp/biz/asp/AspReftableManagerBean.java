package hk.org.ha.model.pms.corp.biz.asp;

import javax.persistence.PersistenceContext;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.asp.persistence.AspHospItem;
import hk.org.ha.model.pms.asp.persistence.AspHospSpec;
import hk.org.ha.model.pms.asp.persistence.AspItem;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.service.biz.pms.asp.interfaces.AspSubscriberJmsRemote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@AutoCreate
@Stateless
@Name("aspReftableManager")
@MeasureCalls
public class AspReftableManagerBean implements AspReftableManagerLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	EntityManager em;
	
	@In
	AspSubscriberJmsRemote aspSubscriberProxy;
	
	@Override
	@CacheResult(timeout = "60000")
	public Map<String, Map<AspType, AspHospItem>> constructAspHospItemCodeMap(List<AspHospItem> aspHospItemList) {
		
		Map<String, Map<AspType, AspHospItem>> aspHospItemCodeMap = new HashMap<String, Map<AspType, AspHospItem>>();
		
		for(AspHospItem aspHospItem:aspHospItemList)
		{
			String itemCode = aspHospItem.getItemCode();
			AspType aspType = aspHospItem.getAspType();
			
			Map<AspType, AspHospItem> itemCodeSubMap = aspHospItemCodeMap.get(itemCode);
			
			if(itemCodeSubMap == null)
    		{
				itemCodeSubMap = new HashMap<AspType, AspHospItem>();
				aspHospItemCodeMap.put(itemCode,itemCodeSubMap);
    		}
			
			if(!itemCodeSubMap.containsKey(aspType))
			{
				itemCodeSubMap.put(aspType, aspHospItem);
			}
		}
		return aspHospItemCodeMap;
	}

	@Override
	@CacheResult(timeout = "60000")
	public Map<AspType, Map<String, AspHospSpec>> constructAspHospSpecCodeMap(List<AspHospSpec> aspHospSpecList) {
		
		Map<AspType, Map<String, AspHospSpec>> aspHospSpecCodeMap = new HashMap<AspType, Map<String, AspHospSpec>>();
		
		for(AspHospSpec aspHospSpec:aspHospSpecList)
		{
			String specCode = aspHospSpec.getSpecCode();
			AspType aspType = aspHospSpec.getAspType();
			
			Map<String, AspHospSpec> specCodeSubMap = aspHospSpecCodeMap.get(aspType);
    		if (specCodeSubMap == null) {
    			specCodeSubMap = new HashMap<String, AspHospSpec>();
    			aspHospSpecCodeMap.put(aspType, specCodeSubMap);
    		}

    		//distinct specCode
    		if(!specCodeSubMap.containsKey(specCode))
    		{
    			specCodeSubMap.put(specCode, aspHospSpec);
    		}
		}
		return aspHospSpecCodeMap;
	}


	@SuppressWarnings("unchecked")
	@Override
	@CacheResult(timeout = "60000")
	public AspHosp retrieveAspHospByHospCode(
			String hospCode) {
		List<AspHosp> aspHospList = em.createQuery(
				"select o from AspHosp o "+ // 20170418 index check : AspHosp.hospCode : PK_ASP_HOSP
				"where o.hospCode = :hospCode "+
				"order by o.hospCode")
				.setParameter("hospCode", hospCode)
				.setHint(QueryHints.BATCH, "o.aspHospItemList")
				.setHint(QueryHints.BATCH, "o.aspHospSpecList")
				.getResultList();

		if(!aspHospList.isEmpty())
		{
			AspHosp aspHosp = aspHospList.get(0);
			
			aspHosp.getAspHospItemList().size();
			aspHosp.getAspHospSpecList().size();
			
			return aspHosp;
		}
		else
		{
			return null;
		}

	}

	@Override
	public void updateAspHosp(AspHosp aspHosp) {

		em.merge(aspHosp);
	
		em.flush();
		em.clear();
		
		aspSubscriberProxy.clearAspHospList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AspHosp> retrieveAspHospList() {
		
		List<AspHosp> aspHospList = em.createQuery(
				"select o from AspHosp o "+ // 20170418 index check : AspHosp.hospCode : PK_ASP_HOSP
				"order by o.hospCode "
				)
				.setHint(QueryHints.BATCH, "o.aspHospItemList")
				.setHint(QueryHints.BATCH, "o.aspHospSpecList")
				.getResultList();
		
		for(AspHosp aspHosp:aspHospList)
		{
			aspHosp.getAspHospItemList().size();
			aspHosp.getAspHospSpecList().size();
			
			for(AspHospItem aspHospItem:aspHosp.getAspHospItemList())
			{
				aspHospItem.getAspItem();
			}
		}
		return aspHospList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> retrieveAspItemCodeList() {
		
		List<String> aspItemList = em.createQuery(
				"select o.itemCode from AspItem o "+ // 20170418 index check : AspHosp.itemCode : PK_ASP_ITEM
				"order by o.itemCode "
				)
				.getResultList();
		
		return aspItemList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@CacheResult(timeout = "60000")
	public Map<String, AspItem> retrieveAspItemMap() {
		Map<String, AspItem> aspItemMap = new HashMap<String, AspItem>();
		
		List<AspItem> aspItemList = em.createQuery(
				"select o from AspItem o "+ // 20170418 index check : AspHosp.itemCode : PK_ASP_ITEM
				"order by o.itemCode "
				)
				.getResultList();
		
		for(AspItem aspItem:aspItemList)
		{
			String itemCode = aspItem.getItemCode();
			if(!aspItemMap.containsKey(itemCode))
			{
				aspItemMap.put(itemCode, aspItem);
			}
		}
		
		return aspItemMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AspHospItem> retrieveAspHospItemListByHospCode(String hospCode) {
		return em.createQuery(
				"select o from AspHospItem o "+ // 20170418 index check : AspHospItem.hospCode : FK_ASP_HOSP_ITEM_01
				" where o.hospCode = :hospCode "+
				" order by o.itemCode ")
				.setParameter("hospCode", hospCode)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateAspHospSpecList(List<AspHospSpec> aspHospSpecList)
	{
		for(AspHospSpec aspHospSpec:aspHospSpecList)
		{
			List<AspHospSpec> existSpecList = em.createQuery(
							"select o from AspHospSpec o"+ // 20170418 index check : AspHospSpec.hospCode,specCode,aspType : PK_ASP_HOSP_SPEC
							" where o.hospCode = :hospCode"+
							" and o.specCode = :specCode"+
							" and o.aspType = :aspType")
							.setParameter("hospCode", aspHospSpec.getHospCode())
							.setParameter("specCode", aspHospSpec.getSpecCode())
							.setParameter("aspType", aspHospSpec.getAspType())
							.getResultList();
			
			if(existSpecList.isEmpty())
			{
				em.persist(aspHospSpec);
			}
		}	
		
		em.flush();
	}
}
