package hk.org.ha.model.pms.corp.batch.worker.dayend;


import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.BatchJobState;
import hk.org.ha.model.pms.udt.RecordStatus;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;


/**
 * Session Bean implementation class DayEndBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("upldPmsFileBean")
public class UpldPmsFileBean implements UpldPmsFileLocal {
	
	private static final String PMS = "PMS";
	private static final String DOT = ".";
	private static final String SLASH = "/";
	private static final String MONTH_DIR_NAME = "month";
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;

	@In
	private DayEndOrderManagerLocal dayEndOrderManager;
	
	@In
	private ApplicationProp applicationProp;

	private Logger logger = null;

	private Date batchDate = null;
	private String year = null;
	private String month = null;
	private String day = null;
	
	private String jobId = null;
	
	public void beginChunk(Chunk chunk) throws Exception {
		
	}

	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}

	public void endChunk() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if (params.getDate("batchDate") == null) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
			year = DateTimeFormat.forPattern("yyyy").print(new DateTime(batchDate));
			month = DateTimeFormat.forPattern("MM").print(new DateTime(batchDate));
			day = DateTimeFormat.forPattern("dd").print(new DateTime(batchDate));
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
	}
	
	@SuppressWarnings("unchecked")
	public void processRecord(Record record) throws Exception {
		Date startDatetime = new Date();
		logger.info("process record start... @" + startDatetime);
		
		List<String> hospCodeList = em.createQuery(
				"select o.hospCode from Hospital o" + // 20120831 index check : none
				" where o.status = :status")
				.setParameter("status", RecordStatus.Active)
				.getResultList();
		
		for (String hospCode : hospCodeList) {
			
			BatchJobState batchJobState = dayEndOrderManager.retrieveBatchJobState(jobId, batchDate, hospCode, null);
			if (batchJobState != null) {
				logger.info("Job " + jobId + " is already done for batchDate [" + batchDate + "], hospCode [" + hospCode + "]");
				continue;
			}
			
			String pmsTargetPath = "./" + applicationProp.getDnldTrxDataSubDir() + "/month/" + year + SLASH + month + SLASH + hospCode;
			
			
			File ftpSourceDir = getOutDoneDirectory(applicationProp.getPmsTrxDir(), PMS, hospCode, true);
			File ftpSourceFile = getFile(ftpSourceDir, applicationProp.getDnldTrxFile(), batchDate);
			
			if ( ! ftpSourceFile.exists() ) {
				logger.error("Trx file does not exist - " + ftpSourceFile.getPath());
				continue;
			}
			
			String ftpTargetFileName = applicationProp.getDnldTrxFile() + "-" + (month+day) + ".dat";
			
			dayEndOrderManager.uploadPmsFile(ftpSourceFile, pmsTargetPath, ftpTargetFileName, logger, jobId, batchDate, hospCode);
		}
		
		Date endDatetime = new Date();
        logger.info("process record complete..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	private File getOutDoneDirectory(
			String trxDir,
			String systemType,
			String hospCode,
			boolean createDir) throws Exception {
		
		StringBuilder basicPath = new StringBuilder();
		basicPath.append(applicationProp.getBatchDataDir())
						.append(SLASH).append(trxDir);
		if (systemType != null) {
			basicPath.append(SLASH).append(systemType);
		}
		basicPath.append(SLASH).append(MONTH_DIR_NAME);
		
		return getOutDoneDirectory(basicPath.toString(), hospCode, createDir);
	}
	
	private File getOutDoneDirectory(
			String basicPath,
			String hospCode,
			boolean createDir) throws Exception {
		
		File basicDir = new File(basicPath);
		if ( ! basicDir.exists()) {
			throw new Exception("UpldPmsFileBean: date directory could not be created as parent directory " + basicDir.getParent() + " not exists.");
		}

		StringBuilder destPath = new StringBuilder();
		destPath.append(basicPath)
						.append(SLASH).append(year)
						.append(SLASH).append(month)
						.append(SLASH).append(hospCode);
		
		File destDir = new File(destPath.toString());
		if ( ! destDir.exists()) {
			if (createDir) {
				if ( ! destDir.mkdirs()) {
					throw new Exception("UpldPmsFileBean: directory " + destDir.getPath() + " could not be found and created. Please check (eg. Access Right...).");
				}
			} else {
				throw new Exception("UpldPmsFileBean: directory " + destDir.getPath() + " could not be found. Please check (eg. Access Right...).");
			}
		}
		
		return destDir;
	}
	
	private File getFile(
			File dir, 
			String fileName,
			Date batchDate) {
		
		StringBuilder finalFileName = new StringBuilder(fileName);
		
		if (batchDate != null) {
			finalFileName.append(DOT)
						 .append(dateFormat.format(batchDate));
		}
		
		File file = new File(
				    dir + SLASH + 
					finalFileName );
		
		return file;
	}
}
