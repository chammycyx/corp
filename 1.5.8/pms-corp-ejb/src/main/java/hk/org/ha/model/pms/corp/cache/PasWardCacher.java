package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.vo.patient.PasWard;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableRuntimeException;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("pasWardCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class PasWardCacher extends BaseCacher implements PasWardCacherInf {
	
	@In
	private PasServiceJmsRemote pasServiceProxy;

	private Map<String, InnerCacher> cacherMap = new HashMap<String, InnerCacher>();

	public void clear() { 
		synchronized (this) {
			cacherMap.clear();
		}
	}	

    public List<PasWard> getPasWardList(String patHospCode) {
    	return (List<PasWard>) this.getCacher(patHospCode).getAll();
    }
	
	private InnerCacher getCacher(String patHospCode) {
		synchronized (this) {
			String key = patHospCode;
			InnerCacher cacher = cacherMap.get(key);
			if (cacher == null) {
				cacher = new InnerCacher(patHospCode, this.getExpireTime());
				cacherMap.put(key, cacher);
			}
			return cacher;
		}
	}
	
	private class InnerCacher extends AbstractCacher<String, PasWard> {
		private String patHospCode = null;

		public InnerCacher(String patHospCode, int expireTime) {
			super(expireTime);
			this.patHospCode = patHospCode;
		}

		public PasWard create(String wardCode) {
			throw new UnsupportedOperationException();
		}
		
		public Collection<PasWard> createAll() {
			try {
				return pasServiceProxy.retrieveIpasWard(patHospCode);
			} catch (PasException e) {
				throw new NestableRuntimeException(e);
			} catch (UnreachableException e) {
				throw new NestableRuntimeException(e);
			}
		}

		public String retrieveKey(PasWard pasWard) {
			return pasWard.getPasWardCode();
		}
	}

	public static PasWardCacher instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (PasWardCacher) Component.getInstance("pasWardCacher", ScopeType.APPLICATION);
    }	
}
