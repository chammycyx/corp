package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmSupplIndication;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmSupplIndicationCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmSupplIndicationCacher extends BaseCacher implements DmSupplIndicationCacherInf {
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;
	
	private List<DmSupplIndication> cachedDmSupplIndicationList;
	
	private Map<String, DmSupplIndication> dmSupplIndicationMap = new HashMap<String, DmSupplIndication>();
	
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
			cachedDmSupplIndicationList = null;
			dmSupplIndicationMap.clear();
		}
	}	
	
	public String getDmSupplIndicationDesc(String supplIndicationCode) {
		this.getDmSupplIndicationList();
		
		if (dmSupplIndicationMap.get(supplIndicationCode) != null) {
			return dmSupplIndicationMap.get(supplIndicationCode).getSupplIndicationDesc();
		}
		return "";
	}
	
	private List<DmSupplIndication> getDmSupplIndicationList() {
		synchronized (this)
		{
			List<DmSupplIndication> dmSupplIndicationList = (List<DmSupplIndication>) this.getCacher().getAll();

			// check whether the Full DmSupplIndicationList expire
			if (cachedDmSupplIndicationList == null || (cachedDmSupplIndicationList != dmSupplIndicationList)) 
			{
				// hold the reference
				cachedDmSupplIndicationList = dmSupplIndicationList;
				
				this.buildDmSupplIndicationKeyMap(cachedDmSupplIndicationList);
			}			

			return cachedDmSupplIndicationList;
		}
	}
	
	private void buildDmSupplIndicationKeyMap(List<DmSupplIndication> dmSupplIndicationList)
	{
		dmSupplIndicationMap.clear();
		
		for (DmSupplIndication dmSupplIndication : dmSupplIndicationList) {
			String key = new String(dmSupplIndication.getId().getSupplIndicationCode());
			dmSupplIndicationMap.put(key, dmSupplIndication);
		}
	}
	
	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmSupplIndication> {
		public InnerCacher(int expireTime) {
			super(expireTime);
		}

		@Override
		public DmSupplIndication create(String key) {
			this.getAll();
			return this.internalGet(key);
		}

		@Override
		public Collection<DmSupplIndication> createAll() {
			return dmsPmsServiceProxy.retrieveDmSupplIndicationList();
		}

		@Override
		public String retrieveKey(DmSupplIndication dmSupplIndication) {
			return dmSupplIndication.getId().getSupplIndicationCode();
		}
	}
	
	public static DmSupplIndicationCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmSupplIndicationCacherInf) Component.getInstance("dmSupplIndicationCacher", ScopeType.APPLICATION);
    }
}