package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asg.udt.api.Lang;
import hk.org.ha.model.pms.asg.vo.ehr.EhrViewerUser;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.vo.label.BaseLabel;
import hk.org.ha.model.pms.vo.label.Instruction;
import hk.org.ha.model.pms.vo.label.Warning;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("corpAsgService")
@MeasureCalls
public class CorpAsgServiceBean implements CorpAsgServiceLocal {

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;

	@SuppressWarnings("unchecked")
	public List<EhrViewerUser> retrieveEhrViewerUserList(){
		List<hk.org.ha.model.pms.uam.vo.ehr.EhrViewerUser> uamUserList = uamServiceProxy.retrieveEhrViewerUserList();
		if ( uamUserList.isEmpty()) {
			return new ArrayList<EhrViewerUser>();
		}
		
		Set<String> hospCodeSet = new HashSet<String>();
		for ( hk.org.ha.model.pms.uam.vo.ehr.EhrViewerUser user : uamUserList ) {
			hospCodeSet.add(user.getHospCode());
		}
		
		List<Workstore> workstoreList = em.createQuery(
				"select o from Workstore o" + // 20171024 index check : Workstore.hospCode : FK_WORKSTORE_01
				" where o.hospCode in :hospCodeList")
				.setParameter("hospCodeList", new ArrayList<String>(hospCodeSet))
				.getResultList();
		
		Map<String, String> patHospCodeMap = new HashMap<String, String>();
		for ( Workstore workstore : workstoreList ) {
			if ( !patHospCodeMap.containsKey(workstore.getHospCode()) ) {
				patHospCodeMap.put(workstore.getHospCode(), workstore.getDefPatHospCode());
				continue;
			}
			
			if ( StringUtils.equals(workstore.getDefPatHospCode(), workstore.getHospCode()) ) {
				patHospCodeMap.put(workstore.getHospCode(), workstore.getDefPatHospCode());
			} else {
				String newDefPatHospCode = workstore.getDefPatHospCode();
				String defPatHospCode = patHospCodeMap.get(workstore.getHospCode());
				if ( newDefPatHospCode.compareToIgnoreCase(defPatHospCode) < 0 ) {
					patHospCodeMap.put(workstore.getHospCode(), newDefPatHospCode);
				}
			}
		}
		
		Map<String, EhrViewerUser> userMap = new HashMap<String, EhrViewerUser>();
		for ( hk.org.ha.model.pms.uam.vo.ehr.EhrViewerUser user : uamUserList ) {
			String patHospCode = patHospCodeMap.get(user.getHospCode());
			String key = patHospCode+user.getUserName();
			if ( userMap.containsKey(key) ) {
				continue;
			}
			EhrViewerUser ehrViewer = new EhrViewerUser();
			ehrViewer.setPatHospCode(patHospCode);
			ehrViewer.setUserId(user.getUserName());
			userMap.put(key, ehrViewer);
		}
		
		return new ArrayList<EhrViewerUser>(userMap.values());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<hk.org.ha.model.pms.asg.vo.api.DispOrder> retrieveDispOrderList(List<Integer> dispOrderNumList, Lang lang, Date fromDispDate) 
	{
		List<hk.org.ha.model.pms.asg.vo.api.DispOrder> retList = new ArrayList<hk.org.ha.model.pms.asg.vo.api.DispOrder>();

		//make sure all dispOrders are being to the same pharmOrder
		Long pharmOrderId = null;
		if (dispOrderNumList.size() > 1) {
			List<Long> pharmOrderIdList = em.createQuery( 
				"select o.pharmOrder.id from DispOrder o" + // 20171024 index check : DispOrder.pharmOrder : FK_DISP_ORDER_02
				" where o.dispOrderNum = :dispOrderNum")
				.setParameter("dispOrderNum", dispOrderNumList.get(0))
				.getResultList();
			if (pharmOrderIdList.isEmpty()) {
				return retList;
			} else {
				pharmOrderId = pharmOrderIdList.get(0);
			}
		}	
		StringBuilder sb = new StringBuilder();		
		sb.append("select o from DispOrder o"); // 20170828 index check : DispOrder.dispOrderNum : I_DISP_ORDER_10
		sb.append(" where o.dispOrderNum in :dispOrderNumList");
		if (pharmOrderId != null) sb.append(" and o.pharmOrder.id = :pharmOrderId");
		if (fromDispDate != null) sb.append(" and o.dispDate >= :dispDate");
		sb.append(" and o.status not in :status");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("dispOrderNumList", dispOrderNumList);
		if (pharmOrderId != null) query.setParameter("pharmOrderId", pharmOrderId);
		if (fromDispDate != null) query.setParameter("dispDate", fromDispDate, TemporalType.DATE);
		query.setParameter("status", DispOrderStatus.Deleted_SysDeleted);
		query.setHint(QueryHints.BATCH, "o.dispOrderItemList");
		List<DispOrder> dispOrderList = query.getResultList();
		
		PrintOption printOption = new PrintOption();
        printOption.setPrintLang((lang == Lang.zh_TW) ? PrintLang.Chi : PrintLang.Eng);
        printOption.setPrintType(PrintType.Normal);
		
		dispOrderList = reorderBy(dispOrderList, dispOrderNumList);
		for (DispOrder dispOrder: dispOrderList) {
			dispOrder.getDispOrderItemList().size();
			dispOrder.getPharmOrder().getPharmOrderItemList().size();
			retList.add(convertDispOrderForApi(dispOrder, printOption));
		}
		
		return retList;
	}
	
	
	private List<DispOrder> reorderBy(List<DispOrder> dispOrderList, List<Integer> dispOrderNumList) {		
		if (dispOrderNumList.isEmpty() || 
				dispOrderList.isEmpty() || 
				dispOrderList.size() != dispOrderNumList.size()) {
			return new ArrayList<DispOrder>();
		}

		List<DispOrder> reorderedList = new ArrayList<DispOrder>();
		for (Integer dispOrderNum : dispOrderNumList) {
			for (DispOrder dispOrder: dispOrderList) {
				if (dispOrder.getDispOrderNum().equals(dispOrderNum)) {
					reorderedList.add(dispOrder);
					break;
				}
			}
		}
		
		return (reorderedList.size() == dispOrderNumList.size()) ? reorderedList : new ArrayList<DispOrder>();
	}
		
	private hk.org.ha.model.pms.asg.vo.api.DispOrder convertDispOrderForApi(DispOrder dispOrder, PrintOption printOption) 
	{
		hk.org.ha.model.pms.asg.vo.api.DispOrder _dispOrder = new hk.org.ha.model.pms.asg.vo.api.DispOrder();
		
		_dispOrder.setDispOrderNum(dispOrder.getDispOrderNum());
		
		for (DispOrderItem item : dispOrder.getDispOrderItemList()) {
			if (item.getStatus() != DispOrderItemStatus.Deleted &&
					item.getBaseLabel() != null) {
				hk.org.ha.model.pms.asg.vo.api.DispOrderItem _item = new hk.org.ha.model.pms.asg.vo.api.DispOrderItem();

				BaseLabel label = item.getBaseLabel();	
				label.setPrintOption(printOption);	
				_item.setWarning(label.getWarningText().split("\\n"));
				_item.setInstruction(label.getInstructionText().split("\\n"));				
				// we are sending pharmOrderItem.itemNum
				_item.setItemNum(item.getPharmOrderItem().getItemNum());
				_item.setItemCode(item.getPharmOrderItem().getItemCode());
				
				_dispOrder.getDispOrderItems().add(_item);
			}
		}
		
		return _dispOrder;		
	}
	
	private String[] convertListToArray(List<String> list) {
		for (Iterator<String> itr = list.iterator(); itr.hasNext();) {
			if (StringUtils.isBlank(itr.next())) {
				itr.remove();
			}
		}
		return list.toArray(new String[list.size()]);
	}

}

