package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;

import java.util.List;

public interface DmFormVerbMappingCacherInf extends BaseCacherInf {

	DmFormVerbMapping getDmFormVerbMappingByFormCodeSiteCode(String formCode, String siteCode);
	
	List<DmFormVerbMapping> getDmFormVerbMappingList();
	
	List<DmFormVerbMapping> getDmFormVerbMappingListByFormCode(String prefixFormCode);
	
	List<DmFormVerbMapping> getDmFormVerbMappingListBySiteCode(String prefixSiteCode);

}
