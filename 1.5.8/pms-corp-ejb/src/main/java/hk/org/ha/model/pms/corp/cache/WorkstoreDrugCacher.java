package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("workstoreDrugCacher")
@Scope(ScopeType.APPLICATION)
public class WorkstoreDrugCacher implements WorkstoreDrugCacherInf {

	@Logger
	private Log logger;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
		
	public WorkstoreDrug getWorkstoreDrug(Workstore workstore, String itemCode) {	
		DmDrug dmDrug = dmDrugCacher.getDmDrug(itemCode);
		if (dmDrug == null) {
			logger.warn("dmDrug not found for itemCode : " + itemCode);
			return null;
		} else {
			return buildWorkstoreDrug(workstore, dmDrug);
		}
	}

	public List<WorkstoreDrug> getWorkstoreDrugList(Workstore workstore, String prefixItemCode) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugList(workstore, prefixItemCode));
	}

	public List<WorkstoreDrug> getWorkstoreDrugList(Workstore workstore, String prefixItemCode, Boolean suspend, Boolean fdn) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugList(workstore, prefixItemCode, suspend, fdn));
	}

	public List<WorkstoreDrug> getWorkstoreDrugListByDisplayName(Workstore workstore, String displayName) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListByDisplayName(workstore, displayName));
	}
	
	public List<WorkstoreDrug> getWorkstoreDrugListByDisplayNameFmStatus(Workstore workstore, String displayName,
			String fmStatus) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListByDisplayNameFmStatus(workstore, displayName, fmStatus));
	}

	public List<WorkstoreDrug> getWorkstoreDrugListByDisplayNameFormSaltFmStatus(
			Workstore workstore, 
			String displayName, String formCode, String saltProperty,
			String fmStatus) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListByDisplayNameFormSaltFmStatus(workstore, displayName, formCode, saltProperty, fmStatus));
	}

	public List<WorkstoreDrug> getWorkstoreDrugListByDrugKey(Workstore workstore, Integer drugKey) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListByDrugKey(workstore, drugKey));
	}

	public List<WorkstoreDrug> getWorkstoreDrugListByDrugKeyFmStatus(Workstore workstore, Integer drugKey,
			String fmStatus) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListByDrugKeyFmStatus(workstore, drugKey, fmStatus));
	}

	public List<WorkstoreDrug> getWorkstoreDrugListByFullDrugDesc(Workstore workstore, String prefixFullDrugDesc) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListByFullDrugDesc(workstore, prefixFullDrugDesc));
	}

	public List<WorkstoreDrug> getWorkstoreDrugListBySolvent(Workstore workstore) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getDmDrugListBySolvent(workstore));
	}	

	public List<WorkstoreDrug> getWorkstoreDrugListForChestItem(Workstore workstore, String prefixItemCode) {
		return this.buildWorkstoreDrugList(workstore, dmDrugCacher.getChestItemList(workstore, prefixItemCode));
	}	
	
	private WorkstoreDrug buildWorkstoreDrug(Workstore workstore, DmDrug dmDrug) {
		return new WorkstoreDrug(dmDrug, dmDrugCacher.getMsWorkstoreDrug(workstore, dmDrug.getItemCode()));
	}
	
	private List<WorkstoreDrug> buildWorkstoreDrugList(Workstore workstore, List<DmDrug> dmDrugList) {
		List<WorkstoreDrug> drugList = new ArrayList<WorkstoreDrug>();
		for (DmDrug dmDrug : dmDrugList) {
			drugList.add(this.buildWorkstoreDrug(workstore, dmDrug));
		}
		return drugList;
	}
	
	public static WorkstoreDrugCacherInf instance()
	{
		if (!Contexts.isApplicationContextActive())
		{
			throw new IllegalStateException("No active application scope");
		}
		return (WorkstoreDrugCacherInf) Component.getInstance("workstoreDrugCacher", ScopeType.APPLICATION);
	}

}
