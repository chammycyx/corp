package hk.org.ha.model.pms.corp.vo.moe;

import java.io.Serializable;

public class OrderInfoSearchCriteria implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String patHospCode;
	private Integer ordNum;
	private String caseNum;
	private String dispHospCode;
	private String dispWorkstore;
	private String lastUpdBy;
	
	public String getPatHospCode() { return patHospCode;}
	public void setPatHospCode(String patHospCode) { this.patHospCode= patHospCode;}

	public Integer getOrdNum() { return ordNum;}
	public void setOrdNum (Integer ordNum) { this.ordNum= ordNum;}

	public String getCaseNum() { return caseNum ;}
	public void setCaseNum (String caseNum) { this.caseNum= caseNum ;}

	public String getDispHospCode() { return dispHospCode;}
	public void setDispHospCode (String dispHospCode) { this.dispHospCode = dispHospCode ;}
	
	public String getDispWorkstore() { return dispWorkstore;}
	public void setDispWorkstore (String dispWorkstore ) { this.dispWorkstore = dispWorkstore ;}

	public String getLastUpdBy() { return lastUpdBy;}
	public void setLastUpdBy (String lastUpdBy ) { this.lastUpdBy = lastUpdBy ;}
	
}
