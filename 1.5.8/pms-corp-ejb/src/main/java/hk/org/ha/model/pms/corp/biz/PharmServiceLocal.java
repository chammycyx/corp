package hk.org.ha.model.pms.corp.biz;
import hk.org.ha.service.biz.pms.corp.interfaces.PharmServiceJmsRemote;

import javax.ejb.Local;

@Local
public interface PharmServiceLocal extends PharmServiceJmsRemote {

}
