package hk.org.ha.model.pms.corp.biz.asp;

import javax.ejb.Local;

import hk.org.ha.service.biz.pms.corp.interfaces.CorpAspServiceJmsRemote;

@Local
public interface CorpAspServiceLocal extends CorpAspServiceJmsRemote {

}
