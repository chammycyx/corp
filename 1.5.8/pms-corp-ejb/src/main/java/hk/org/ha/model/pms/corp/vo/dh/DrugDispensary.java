package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DrugDispensary")
@XmlAccessorType(XmlAccessType.FIELD)
public class DrugDispensary implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String dispensingInstId;
	
	@XmlElement(name="DrugDispensaryInfo")
	private List<DrugDispensaryInfo> drugDispensaryInfoList;

	public String getDispensingInstId() {
		return dispensingInstId;
	}

	public void setDispensingInstId(String dispensingInstId) {
		this.dispensingInstId = dispensingInstId;
	}

	public List<DrugDispensaryInfo> getDrugDispensaryInfoList() {
		return drugDispensaryInfoList;
	}

	public void setDrugDispensaryInfoList(List<DrugDispensaryInfo> drugDispensaryInfoList) {
		this.drugDispensaryInfoList = drugDispensaryInfoList;
	}
}
