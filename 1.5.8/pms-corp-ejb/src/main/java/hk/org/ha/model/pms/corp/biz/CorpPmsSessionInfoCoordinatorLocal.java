package hk.org.ha.model.pms.corp.biz;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CorpPmsSessionInfoCoordinatorLocal {

	void consolidatePmsSessionInfo(String requestId, List<PmsSessionInfo> pmsSessionInfoList);
	
	List<PmsSessionInfo> retrievePmsSessionInfoList(String requestId, Workstore workstore);

}
