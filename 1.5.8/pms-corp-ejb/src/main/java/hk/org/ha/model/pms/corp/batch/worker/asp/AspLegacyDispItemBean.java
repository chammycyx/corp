package hk.org.ha.model.pms.corp.batch.worker.asp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.asp.persistence.AspHospItem;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.corp.biz.HospitalMappingManagerLocal;
import hk.org.ha.model.pms.corp.biz.asp.AspReftableManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.service.biz.pms.asp.interfaces.AspBatchServiceJmsRemote;
import hk.org.ha.service.pms.da.interfaces.cddh.DaCddhServiceJmsRemote;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("aspLegacyDispItemBean")
public class AspLegacyDispItemBean implements AspLegacyDispItemLocal {
	
	private Logger logger = null;
	
	private String jobId = null;
	
	private Date batchDate;
	
	@In
	AspReftableManagerLocal aspReftableManager;
	
	@In
	HospitalMappingManagerLocal hospitalMappingManager;
	
	@In
	AspBatchServiceJmsRemote aspBatchServiceProxy;
	
	@In
	DaCddhServiceJmsRemote daCddhServiceProxy;
	
	@Override
	public void beginChunk(Chunk arg0) throws Exception {
	}

	@Override
	public void endChunk() throws Exception {
	}

	@Remove
	public void destroyBatch() {

	}

	@Override
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch "+info);
		jobId = info.getJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if(params.getDate("batchDate") == null)
		{
			throw new Exception("batch parameter : batchDate is null");
		}
		else
		{
			batchDate = params.getDate("batchDate");
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
	}

	@Override
	public void processRecord(Record record) throws Exception {
		String hospCode = record.getString("HOSP_CODE");
		String g6pdCode = record.getString("G6PD_CODE");
		
		Date startDatetime = new Date();
		logger.info("jobId: "+jobId+" - process record start for hospCode [" + hospCode + "] ... @" + startDatetime);
		
		try{
			AspHosp aspHosp = aspReftableManager.retrieveAspHospByHospCode(hospCode);
			List<AspHospItem> aspHospItemList = aspHosp.getAspHospItemList();
	
			Map<AspType, List<String>> aspHospItemCodeMap = new HashMap<AspType, List<String>>();
	
			for(AspHospItem aspHospItem:aspHospItemList)
			{
				String itemCode = aspHospItem.getItemCode();
				AspType aspType = aspHospItem.getAspType();
	
				if(aspHospItemCodeMap.containsKey(aspType))
				{
					 aspHospItemCodeMap.get(aspType).add(itemCode);
				}
				else
				{
					List<String >itemCodeList = new ArrayList<String>();
					itemCodeList.add(itemCode);
					aspHospItemCodeMap.put(aspType, itemCodeList);
				}
			}
			
			List<DispOrderItem> dispOrderItemList = new ArrayList<DispOrderItem>();
			for(AspType aspType:aspHospItemCodeMap.keySet())
			{
				List<DispOrderItem> subItemList = new ArrayList<DispOrderItem>();
				subItemList.addAll(daCddhServiceProxy.retrieveDaDispOrderItemForAsp(batchDate, hospCode, aspHospItemCodeMap.get(aspType), aspHosp.getExcludeWorkstores()));
	
				for(DispOrderItem dispOrderItem:subItemList)
				{
					dispOrderItem.setAspItemType(aspType.getDataValue());
				}
				dispOrderItemList.addAll(subItemList);
			}
			
			List<String> patHospCodeList = hospitalMappingManager.retrievePatHospCodeList(hospCode);
			
			if(!dispOrderItemList.isEmpty())
			{
				aspBatchServiceProxy.receiveLegacyDispOrderItemList(dispOrderItemList, patHospCodeList, g6pdCode, batchDate);
			}
		}
		catch (Exception e)
		{
			logger.error("jobId: "+jobId+" - Batch job failed. Batch date: "+batchDate+", hospital: "+hospCode);
			throw e;
		}
	
		Date endDatetime = new Date();
        logger.info("jobId: "+jobId+" - process record complete for hospCode [" + hospCode + "] ..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	

}
