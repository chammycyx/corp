package hk.org.ha.model.pms.corp.biz;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;

@Local
public interface CorpStartupLocal {

	@Asynchronous
	void startup(@Duration Long delayDuration);

}
