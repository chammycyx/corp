package hk.org.ha.service.biz.pms.corp.interfaces;


import hk.org.ha.model.pms.asg.udt.api.Lang;
import hk.org.ha.model.pms.asg.vo.ehr.EhrViewerUser;

import java.util.Date;
import java.util.List;


public interface CorpAsgServiceJmsRemote {
	
	List<EhrViewerUser> retrieveEhrViewerUserList();
	
	List<hk.org.ha.model.pms.asg.vo.api.DispOrder> retrieveDispOrderList(List<Integer> dispOrderNumList, Lang lang, Date fromDispDate);
}
