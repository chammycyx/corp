package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.Patient;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("corpCmmService")
@MeasureCalls
public class CorpCmmServiceBean implements CorpCmmServiceLocal {

	private static final String JAXB_CONTEXT_MO = "hk.org.ha.model.pms.persistence.disp";
	
	@In
	private OrderManagerLocal orderManager;
	
	@In
	private OrderSubscribeManagerLocal orderSubscribeManager;
	
	@Override
	public String viewMedOrder(String orderNum) {
		
		MedOrder medOrder = orderManager.retrieveMedOrder(orderNum);
		
		if (medOrder == null) {
			return null;
		}
		
		medOrder.loadChild();
		
		JaxbWrapper<MedOrder> moJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_MO);
		String medOrderXml = moJaxbWrapper.marshall(medOrder);
		
		return medOrderXml;
	}

	@Override
	public Boolean unsubscribeMedOrder(String orderNum) {
		return orderSubscribeManager.unsubscribeMedOrder(orderNum, "CMM");
	}

	@Override
	public String subscribeMedOrder(String orderNum) {
		String medOrderStr = viewMedOrder(orderNum); 
		
		if (medOrderStr != null) {
			orderSubscribeManager.subscribeMedOrder(orderNum, "CMM");
		}

		return medOrderStr;
	}
	
	@Override
	public MedCase saveMedCase(MedCase remoteMedCase) {
		return orderManager.saveMedCase(remoteMedCase);
	}
	
	@Override
	public Patient savePatient(Patient remotePatient) {
		return orderManager.savePatient(remotePatient);
	}	
}
