package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSite;

import java.util.List;

public interface DmSiteCacherInf extends BaseCacherInf {

	DmSite getSiteBySiteCode(String siteCode);
	
	List<DmSite> getSiteList();		

}
