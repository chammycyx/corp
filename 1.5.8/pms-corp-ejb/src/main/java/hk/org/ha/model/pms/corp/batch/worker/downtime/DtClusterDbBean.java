package hk.org.ha.model.pms.corp.batch.worker.downtime;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.biz.ApplicationProp;
import hk.org.ha.model.pms.corp.persistence.order.ItemBin;
import hk.org.ha.model.pms.corp.udt.batch.worker.DataDirType;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.corp.Hospital;
import hk.org.ha.model.pms.persistence.corp.Institution;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;
import hk.org.ha.model.pms.persistence.phs.PatientCat;
import hk.org.ha.model.pms.persistence.phs.Specialty;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.udt.RecordStatus;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dtClusterDbBean")
public class DtClusterDbBean implements DtClusterDbLocal {
        
	private Logger logger = null;
	private Date batchDate = null;
	private Database db = null;
	
	private String jobId = null;
	private String prevJobId = null;

	private static final String PMS_DT_PREFIX = "pms-dt-";
	private static final String TEMPLATE_PREFIX = "template";
	private static final String REFDATA_PREFIX = "refdata";
	private static final String ACCDE_EXT = "accde";
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	@In
    private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	@In
	private ApplicationProp applicationProp;

	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		
		this.logger = logger;

		jobId = info.getJobId();
		prevJobId = info.getPrevJobId();
		
		BatchParameters params = info.getBatchParameters();
		
		if ( null == params.getDate("batchDate") ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = new DateTime(params.getDate("batchDate")).plusDays(1).toDate();
		}
		
    }
    
	public void beginChunk(Chunk chunk) {
	}

	public void processRecord(Record record) throws Exception {

        String clusterCode = record.getString("CLUSTER_CODE");        
    	Date startDatetime = new Date();

    	logger.info("process record start..." + clusterCode +  "@" + startDatetime);

        File prevOutDir = getDirectory(prevJobId, DataDirType.Out);
        File outDir = getDirectory(jobId, DataDirType.Out);
        File workDir = getDirectory(jobId, DataDirType.Work);
        File errorDir = getDirectory(jobId, DataDirType.Error);
        
		File templateFile = getFile(new File(applicationProp.getBatchDataDir() + "/" + prevJobId), 
												PMS_DT_PREFIX + 
												TEMPLATE_PREFIX + 
												"." + ACCDE_EXT);
		File outTemplateFile = getFile(outDir, PMS_DT_PREFIX +
												StringUtils.lowerCase(clusterCode) + 
												"-" +
												dateFormat.format(batchDate) + 
												"." + ACCDE_EXT);

		FileUtils.copyFile(templateFile, outTemplateFile);
        
		File prevOutFile = getFile(prevOutDir, PMS_DT_PREFIX + 
											dateFormat.format(batchDate) + 
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		File workFile = getFile(workDir, PMS_DT_PREFIX +
											StringUtils.lowerCase(clusterCode) + 
											"-" +
											dateFormat.format(batchDate) + 
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		File outFile = getFile(outDir, PMS_DT_PREFIX +
											StringUtils.lowerCase(clusterCode) + 
											"-" +
											dateFormat.format(batchDate) + 
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		File errorFile = getFile(errorDir, PMS_DT_PREFIX +
											StringUtils.lowerCase(clusterCode) + 
											"-" +
											dateFormat.format(batchDate) + 
											"-"+ 
											REFDATA_PREFIX +
											"." + ACCDE_EXT);
		
		FileUtils.copyFile(prevOutFile, workFile);

		try {			
			try {		
		        db = Database.open(workFile);
	
		        updateAccdbDrugAvailabilityTable(clusterCode);
				updateAccdbPhsTable(clusterCode);
				updateAccdbItemBinTable(clusterCode);

			} finally {
				db.close();
			}

		} catch (Exception e) {
			FileUtils.copyFile(workFile, errorFile);
			FileUtils.deleteQuietly(workFile);
			throw e;
		}

		FileUtils.copyFile(workFile, outFile);
		FileUtils.deleteQuietly(workFile);
		
    	Date endDatetime = new Date();
        logger.info("process record complete..." + clusterCode +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");

    }
	
    private void updateAccdbDrugAvailabilityTable(String clusterCode) throws IOException{
		
		Table drugAvailabilityTable = db.getTable("DrugAvailability");
		
		List<Object[]> drugAvailabilityList = new ArrayList<Object[]>();
		
		for (Hospital hospital : this.getHospitalList(clusterCode)) {
			
			for (Workstore workstore : hospital.getWorkstoreList()) {
				
		        logger.info("process retrieve DMS..." + clusterCode + 
		        		"(" + workstore.getHospCode() + "," + workstore.getWorkstoreCode() + ")");
				
				List<MsWorkstoreDrug> msWorkstoreDrugList = dmsPmsServiceProxy.retrieveMsWorkstoreDrugList(workstore.getHospCode(),workstore.getWorkstoreCode());
				
				Map<String, Object> availabilityRowMap = new HashMap<String, Object>();	
				
				for ( Iterator<MsWorkstoreDrug> msWorkstoreDrugIterator = msWorkstoreDrugList.iterator(); msWorkstoreDrugIterator.hasNext(); ) {
					MsWorkstoreDrug msWorkstoreDrug = msWorkstoreDrugIterator.next();
					if (msWorkstoreDrug.getCompId().getItemCode() != null && !"Y".equals(msWorkstoreDrug.getHqSuspend()) ) {
						availabilityRowMap.put("itemCode",msWorkstoreDrug.getCompId().getItemCode());
						availabilityRowMap.put("hospCode",workstore.getHospCode());
						availabilityRowMap.put("workstoreCode",workstore.getWorkstoreCode());
						availabilityRowMap.put("suspend", msWorkstoreDrug.getSuspend());
						availabilityRowMap.put("drugScope", msWorkstoreDrug.getDrugScope());
						
						drugAvailabilityList.add(drugAvailabilityTable.asRow(availabilityRowMap));
					}
				}
				
			}
			
		}
		if (drugAvailabilityList.size()>0) {
			drugAvailabilityTable.addRows(drugAvailabilityList);
			db.flush();
		}

    }
	
    @SuppressWarnings("unchecked")
	private void updateAccdbPhsTable(String clusterCode) throws IOException{

		Table workstoreGroupTable = db.getTable("WorkstoreGroup");
		Table specialtyTable = db.getTable("Specialty");
		Table wardTable = db.getTable("Ward");
		Table patientCatTable = db.getTable("PatientCat");
		Table hospitalTable = db.getTable("Hospital");
		Table workstoreTable = db.getTable("Workstore");

		List<Object[]> workstoreGroupList = new ArrayList<Object[]>();
		List<Object[]> specialtyList = new ArrayList<Object[]>();
		List<Object[]> wardList = new ArrayList<Object[]>();
		List<Object[]> patientCatList = new ArrayList<Object[]>();
		List<Object[]> hospitalList = new ArrayList<Object[]>();
		List<Object[]> workstoreList = new ArrayList<Object[]>();

		Map<String, Object> workstoreGroupRowMap = new HashMap<String, Object>();
		Map<String, Object> specialtyRowMap = new HashMap<String, Object>();	
		Map<String, Object> wardRowMap = new HashMap<String, Object>();	
		Map<String, Object> patientCatRowMap = new HashMap<String, Object>();	
		Map<String, Object> hospitalRowMap = new HashMap<String, Object>();		
		Map<String, Object> workstoreRowMap = new HashMap<String, Object>();	

		List<Institution> institutionList = em.createQuery(
				"select o from Institution o" + // 20170418 index check : none
				" where o.status = :status")
				.setParameter("status", RecordStatus.Active)
				.setHint(QueryHints.BATCH, "o.specialtyList")
				.setHint(QueryHints.BATCH, "o.wardList")
				.setHint(QueryHints.BATCH, "o.patientCatList")
				.getResultList();
		
		for( Institution i : institutionList ){
			i.getSpecialtyList().size();
			i.getWardList().size();
			i.getPatientCatList().size();
		}
		
		List<WorkstoreGroup> phsWorkstoreGroupList = em.createQuery(
				"select o from WorkstoreGroup o" + // 20170418 index check : none
				" where o.status = :status")
				.setParameter("status", RecordStatus.Active)
				.setHint(QueryHints.BATCH, "o.workstoreList")
				.getResultList();

		for (WorkstoreGroup workstoreGroup : phsWorkstoreGroupList) {
			
			workstoreGroupRowMap.put("workstoreGroupCode",workstoreGroup.getWorkstoreGroupCode());
			workstoreGroupList.add(workstoreGroupTable.asRow(workstoreGroupRowMap));
			
			for (Specialty specialty : workstoreGroup.getInstitution().getSpecialtyList()){
				if( specialty.getStatus() == RecordStatus.Active ){
					specialtyRowMap.put("workstoreGroupCode",workstoreGroup.getWorkstoreGroupCode());
					specialtyRowMap.put("specCode",specialty.getSpecCode());
					specialtyList.add(specialtyTable.asRow(specialtyRowMap));
				}
			}

			for (Ward ward : workstoreGroup.getInstitution().getWardList()){
				if( ward.getStatus() == RecordStatus.Active ){
					wardRowMap.put("workstoreGroupCode",workstoreGroup.getWorkstoreGroupCode());
					wardRowMap.put("wardCode",ward.getWardCode());
					wardList.add(wardTable.asRow(wardRowMap));
				}
			}

			for (PatientCat patientCat : workstoreGroup.getInstitution().getPatientCatList()){
				if( patientCat.getStatus() == RecordStatus.Active ){
					patientCatRowMap.put("workstoreGroupCode",workstoreGroup.getWorkstoreGroupCode());
					patientCatRowMap.put("patCatCode",patientCat.getPatCatCode());
					patientCatList.add(patientCatTable.asRow(patientCatRowMap));
				}
			}
			
		}
    			
		for (Hospital hospital : this.getHospitalList(clusterCode)) {

			hospitalRowMap.put("hospCode",hospital.getHospCode());
			hospitalList.add(hospitalTable.asRow(hospitalRowMap));
			
			for (Workstore workstore : hospital.getWorkstoreList()) {

				workstoreRowMap.put("workstoreCode",workstore.getWorkstoreCode());
				workstoreRowMap.put("hospCode",workstore.getHospCode());
				workstoreRowMap.put("workstoreGroupCode",workstore.getWorkstoreGroup().getWorkstoreGroupCode());
				workstoreList.add(workstoreTable.asRow(workstoreRowMap));
				
			}
		}

		if (workstoreGroupList.size()>0) {
			workstoreGroupTable.addRows(workstoreGroupList);
			db.flush();
		}
		if (specialtyList.size()>0) {
			specialtyTable.addRows(specialtyList);
			db.flush();
		}
		if (wardList.size()>0) {
			wardTable.addRows(wardList);
			db.flush();
		}
		if (patientCatList.size()>0) {
			patientCatTable.addRows(patientCatList);
			db.flush();
		}
		if (hospitalList.size()>0) {
			hospitalTable.addRows(hospitalList);
			db.flush();
		}
		if (workstoreList.size()>0) {
			workstoreTable.addRows(workstoreList);
			db.flush();
		}
    }
    
    @SuppressWarnings("unchecked")
	private void updateAccdbItemBinTable(String clusterCode) throws IOException{
		
		Table itemBinTable = db.getTable("ItemBin");
		
		List<Object[]> itemBinList = new ArrayList<Object[]>();
		
		for (Hospital hospital : this.getHospitalList(clusterCode)) {
			
			for (Workstore workstore : hospital.getWorkstoreList()) {
				
				List<ItemBin> itemBinListByWorkstore = em.createQuery(
						"select o from ItemBin o" + // 20121112 index check : ItemBin.workstore : FK_ITEM_BIN_01
						" where o.workstore = :workstore")
						.setParameter("workstore", workstore)
						.getResultList();
				
				Map<String, Object> itemBinRowMap = new HashMap<String, Object>();	
				
				for ( Iterator<ItemBin> itemBinIterator = itemBinListByWorkstore.iterator(); itemBinIterator.hasNext(); ) {
					ItemBin itemBin = itemBinIterator.next();
					itemBinRowMap.put("hospCode",itemBin.getHospCode());
					itemBinRowMap.put("workstoreCode",itemBin.getWorkstoreCode());
					itemBinRowMap.put("itemCode",itemBin.getItemCode());
					itemBinRowMap.put("binNum",itemBin.getBinNum());
					itemBinList.add(itemBinTable.asRow(itemBinRowMap));
				}
			}
		}
		if (itemBinList.size()>0) {
			itemBinTable.addRows(itemBinList);
			db.flush();
		}
    }

    @SuppressWarnings("unchecked")
	private List<Hospital> getHospitalList(String clusterCode) {
		return em.createQuery(
				"select o from Hospital o" + // 20120823 index check : Hospital.hospitalcluster : PK_HOSPITAL_CLUSTER
				" where o.hospitalCluster.clusterCode = :clusterCode") 
				.setParameter("clusterCode", clusterCode)
				.getResultList();
	}
    
	
    public void endChunk() {
    }
    
	private File getDirectory(String jobId,
								DataDirType dataDirType) throws Exception {
		
		File directory = new File(
				applicationProp.getBatchDataDir() + "/" + 
				jobId + "/" + 
				dataDirType.getDataValue());
		
		if (!directory.exists()) {
			createDirectory(directory);
		}
		
		return directory;
	}
	
	private void createDirectory(File directory) throws Exception {
		boolean created = directory.mkdir();
		if (logger.isDebugEnabled()) {
			logger.debug("DtClusterDbBean - createDirectory: create path status: " + created);
		}
		
		if (!directory.exists()) {
			if (!directory.getParentFile().exists()) {
				throw new Exception("DtClusterDbBean: directory " + directory.getPath() + " could not be created as parent directory " + directory.getParent() + " not exists.");
			} else {
				throw new Exception("DtClusterDbBean: directory " + directory.getPath() + " could not be created.");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("DtClusterDbBean - createDirectory: create path successfully: " + directory.getPath());
			}
		}
	}	
	
	private File getFile(
			File dir, 
			String fileName) {;
		
		File file = new File(
				    dir + "/" + 
				    fileName );
		
		return file;
	}
    
    @Remove
    public void destroyBatch() {
    }
}
