package hk.org.ha.model.pms.corp.biz.medprofile;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MedProfileManagerLocal {

	boolean saveDispOrderListForIp(	List<DispOrder> dispOrderList,
									Boolean legacyPersistenceEnabled,
									Boolean fcsPersistenceEnabled,
									Boolean directLabelPrintFlag,
									List<Invoice> tempInvoiceList,
									List<Invoice> voidInvoiceList);
	
	boolean updateDispOrderItemStatusToDeletedByTpnRequestIdList(List<Long> tpnRequestIdList, Boolean legacyPersistenceEnabled);
	
	boolean updateDispOrderItemStatusToDeleted(List<Long> deliveryItemIdList, Boolean legacyPersistenceEnabled);
	
	List<Invoice> retrieveUpdateVoidInvoiceList(List<String> invoiceNumList);
	
	String retrieveChargeOrderNum(String hospCode, String patHospCode);
	
	List<Invoice> retrieveActiveInvoiceByInvoiceNum(List<String> invoiceNumList);
}
