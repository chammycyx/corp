package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
import hk.org.ha.model.pms.udt.vetting.SupplFreqSort;

import java.util.List;

public interface DmSupplFrequencyCacherInf extends BaseCacherInf {

    DmSupplFrequency getDmSupplFrequencyBySupplFreqCode( String supplFreqCode );
	
	List<DmSupplFrequency> getDmSupplFrequencyList( String regimenCode, SupplFreqSort sortSeq );
	
	List<DmSupplFrequency> getDmSupplFrequencySuspendList( String regimenCode, SupplFreqSort sortSeq );
	
}
