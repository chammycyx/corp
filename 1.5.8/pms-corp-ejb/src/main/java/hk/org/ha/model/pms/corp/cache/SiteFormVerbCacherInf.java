package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.vo.FormVerb;
import hk.org.ha.model.pms.dms.vo.SiteFormVerb;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SiteFormVerbCacherInf extends BaseCacherInf {

	List<SiteFormVerb> getSiteFormVerbList();
	
	List<SiteFormVerb> getSiteFormVerbListByFormCode(String formCode);

	FormVerb getDefaultFormVerbByFormCode(String formCode);
}
