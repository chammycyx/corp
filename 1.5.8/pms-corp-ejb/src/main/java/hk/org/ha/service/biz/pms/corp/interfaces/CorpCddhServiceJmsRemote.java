package hk.org.ha.service.biz.pms.corp.interfaces;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.vo.cddh.CddhCriteria;

import java.util.List;

public interface CorpCddhServiceJmsRemote {
	
	List<DispOrderItem> retrieveDispOrderItem(CddhCriteria cddhCriteria);
	
	CddhRemarkStatus checkDispOrderItemRemarkAccess(Long dispOrderId, String loginHospCode, String displayHospCode);
	
	void updateDispOrderItemRemark(DispOrderItem dispOrderItem, Boolean legacyPersistenceEnabled);
	
	void deleteDispOrderItemRemark(DispOrderItem dispOrderItem, Boolean legacyPersistenceEnabled);
	
	List<DispOrder> retrieveDispOrderLikePatName(CddhCriteria cddhCriteria);
	
}
