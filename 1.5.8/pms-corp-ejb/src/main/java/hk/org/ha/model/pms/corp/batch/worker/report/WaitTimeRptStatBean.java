package hk.org.ha.model.pms.corp.batch.worker.report;

import java.util.Calendar;
import java.util.Date;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("waitTimeRptStatBean")
public class WaitTimeRptStatBean implements WaitTimeRptStatLocal {

	private Date batchDate = null;
	private String clusterCode = null;
	private Logger logger = null;
	
	@In
	private PmsSubscriberJmsRemote pmsSubscriberProxy;
	
	public WaitTimeRptStatBean() {
    }

    public void beginChunk(Chunk arg0) throws Exception {
	}

	public void endChunk() throws Exception {
	}
    
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
		}
		
		clusterCode = params.getString("clusterCode");
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (!dtBatchDate.isBefore(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before current date");
        }
	}

	@Remove
	public void destroyBatch() {
		
	}

	@Override
	public void processRecord(Record record) throws Exception {
		logger.info("Generate waiting time report statistics for clusterCode [" + clusterCode + "] with batchDate [" + batchDate + "]");		
		pmsSubscriberProxy.createWaitTimeRptStat(batchDate, clusterCode, new Date());
	}
}
