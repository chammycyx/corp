package hk.org.ha.model.pms.corp.batch.worker.order;

import static hk.org.ha.model.pms.corp.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.model.pms.corp.batch.worker.dayend.DayEndOrderManagerLocal;
import hk.org.ha.model.pms.vo.disp.DayEndDispOrder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.joda.time.DateTime;
/**
 * Session Bean implementation class SendSuspendStatusBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("sendSuspendStatusBean")
public class SendSuspendStatusBean implements SendSuspendStatusLocal {
	
	@In
	private DayEndOrderManagerLocal dayEndOrderManager;
	
	private Logger logger = null;

	private Date startDate = null;
	private Date batchDate = null;

    public SendSuspendStatusBean() {
        
    }

	public void beginChunk(Chunk chunk) throws Exception {
		
	}

	public void endChunk() throws Exception {
		
	}

	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		logger.info("initBatch " + info);
		
		BatchParameters params = info.getBatchParameters();
		
		if ( params.getDate("batchDate") == null ) {
			throw new Exception("batch parameter : batchDate is null");
		} else {
			batchDate = params.getDate("batchDate");
		}
		
        DateTime dtBatchDate = new DateTime(batchDate.getTime());
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        if (dtBatchDate.isAfter(currentDate.getTime())) {
        	throw new Exception("batch parameter : batchDate should be before or equal to current date");
        }
		
        Integer dayEndDuration = BATCH_DAYEND_ORDER_DURATION.get();
        
		if (dayEndDuration == null) {
			throw new Exception("corporate property : batch.dayEnd.order.duration is null");
		} else {
			DateTime dt = new DateTime(batchDate);
			startDate = dt.minusDays(dayEndDuration).toDate();
		}
	}
	
	public void processRecord(Record record) throws Exception {

		String hospCode = record.getString("HOSP_CODE");
		String workstoreCode = record.getString("WORKSTORE_CODE");
		
		Date startDatetime = new Date();
		logger.info("process record start for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + "]... @" + startDatetime);

		
		// new transaction is used because CDDH needs to know Processing status, and   
		// pessimistic lock is used in the function
		dayEndOrderManager.preProcessSendSuspend(
				logger,
				hospCode, 
				workstoreCode, 
				startDate, 
				batchDate);
		
		// new transaction to send message by chunk
		List<List<DayEndDispOrder>> dayEndDispOrderListGroup = dayEndOrderManager.processSendSuspend(
				logger,
				hospCode, 
				workstoreCode,
				startDate,
				batchDate);
		
		List<DayEndDispOrder> dayEndDispOrderListForSuspend = dayEndDispOrderListGroup.get(0);
		List<DayEndDispOrder> dayEndDispOrderListForUnsuspend = dayEndDispOrderListGroup.get(1);
		
		// mark day end status back to None for SUSPEND or UNSUSPEND order
		// (no new transaction is required)
		dayEndOrderManager.postProcessSendSuspend(
				logger,
				hospCode, 
				workstoreCode, 
				dayEndDispOrderListForSuspend,
				dayEndDispOrderListForUnsuspend,
				"[SUSPEND]",
				"[UNSUSPEND]");
			

		Date endDatetime = new Date();
        logger.info("process record complete for hospCode [" + hospCode + "] workstoreCode [" + workstoreCode + "]..." +
        		"@" + endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}
	
	@Remove
	public void destroyBatch() {
		// TODO Auto-generated method stub
		
	}
}
