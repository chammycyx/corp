package hk.org.ha.model.pms.corp.vo;

import java.io.Serializable;
import java.util.Date;

public class PrnPropertyResult implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospCode;

	private String workstoreCode;

	private Integer prnPercent;
	
	private Integer prnDuration;
	
	private String updateUser; 
	
	private Date updateDate;
	
    public PrnPropertyResult() {
    }

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Integer getPrnPercent() {
		return prnPercent;
	}

	public void setPrnPercent(Integer prnPercent) {
		this.prnPercent = prnPercent;
	}

	public Integer getPrnDuration() {
		return prnDuration;
	}

	public void setPrnDuration(Integer prnDuration) {
		this.prnDuration = prnDuration;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}	
}