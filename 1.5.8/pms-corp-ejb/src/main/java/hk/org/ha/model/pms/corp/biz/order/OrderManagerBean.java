package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucher;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.disp.RemarkItemStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionRolledbackLocalException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("orderManager")
@MeasureCalls
public class OrderManagerBean implements OrderManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext(unitName="PMSCOR1_PMS")
	private EntityManager em;
	
	@In
	private OrderHelper orderHelper;
	
	@In
	private OwnershipServiceLocal ownershipService;
	
	@In
	private SeqNumServiceLocal seqNumService;

	@In
	private PatientManagerLocal patientManager;
	
	public MedOrder retrieveMedOrder(String orderNum) {
		return orderHelper.retrieveMedOrder(em, orderNum);
	}

	public MedOrder retrieveMedOrder(String orderNum, List<MedOrderStatus> excludeStatusList) {
		return orderHelper.retrieveMedOrder(em, orderNum, excludeStatusList);
	}	
	
	public void lockMedOrder(String orderNum) {
		orderHelper.lockMedOrder(em, orderNum);
	}

	public void lockMedOrder(Long medOrderId) {
		orderHelper.lockMedOrder(em, medOrderId);
	}
	
	public PharmOrder retrievePharmOrder(String orderNum) {

		return orderHelper.retrievePharmOrder(em, orderNum);
	}
	
	public Workstore retrieveWorkstore(Workstore workstore) {

		return orderHelper.retrieveWorkstore(em, workstore);
	}

	public MedCase saveMedCase(MedCase remoteMedCase) {

		return orderHelper.saveMedCase(em, remoteMedCase, true);
	}

	public Patient savePatient(Patient remotePatient) {
		if (remotePatient == null) {
			return null;
		}
		
		Long patientId = null; 

		try {
			patientId = patientManager.savePatient(remotePatient).getId();
		} catch (TransactionRolledbackLocalException e) {
			if (e.getCause() != null && e.getCause().getCause() != null && 
					e.getCause().getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
				// if patient with same ID is already inserted, skip saving patient 
				logger.warn("Patient is already inserted by another transaction, patKey = " + remotePatient.getPatKey());
				patientId = remotePatient.getId();
			} else {
				throw e;
			}
		}
		
		// since patient is saved in new transaction, need to get managed patient
		return (Patient) em.createQuery(
				"select o from Patient o" + // 20161129 index check : Patient.id : PK_PATIENT
				" where o.id in :idSet")
				.setParameter("idSet", Arrays.asList(patientId))
				.getResultList()
				.get(0);
	}

	public Ticket saveTicket(Ticket remoteTicket, boolean allowCreate) {

		return orderHelper.saveTicket(em, remoteTicket, allowCreate);
	}	

	public Ticket saveTicket(Ticket remoteTicket) {

		return orderHelper.saveTicket(em, remoteTicket);
	}	

	public MedOrder insertMedOrder(MedOrder medOrder) {
		return insertMedOrder(medOrder, true, false);
	}

	public MedOrder insertMedOrderByUnvet(MedOrder medOrder) {
		return insertMedOrder(medOrder, true, true);
	}
	
	public MedOrder insertMedOrder(MedOrder remoteMedOrder, boolean allowCreate,  boolean includeTicket) {
		Patient remotePatient = remoteMedOrder.getPatient();
		remoteMedOrder.setPatient(null);
		
		Long patientId = null; 
		
		if (remotePatient != null) {
			try {
				patientId = patientManager.savePatient(remotePatient).getId();
			} catch (TransactionRolledbackLocalException e) {
				if (e.getCause() != null && e.getCause().getCause() != null && 
						e.getCause().getCause() instanceof java.sql.SQLIntegrityConstraintViolationException) {
					// if patient with same ID is already inserted, skip saving patient 
					logger.warn("Patient is already inserted by another transaction, patKey = " + remotePatient.getPatKey());
					patientId = remotePatient.getId();
				} else {
					throw e;
				}
			}
			
			// since patient is saved in new transaction, need to get managed patient by em.find
			remotePatient = em.find(Patient.class, patientId);
		}
		
		MedOrder medOrder = orderHelper.insertMedOrder(em, remoteMedOrder, allowCreate, includeTicket);
		medOrder.setPatient(remotePatient);
		
		return medOrder;
	}
	
	public List<DispOrder> markOrderSysDeleted(
			PharmOrder pharmOrder, 
			Long prevPharmOrderId,
			Boolean remarkFlag) {
		
		return orderHelper.markOrderSysDeleted(em, pharmOrder, prevPharmOrderId, remarkFlag);
	}	

	public void loadOrderList(MedOrder medOrder) 
	{		
		orderHelper.loadOrderList(em, medOrder);
	}
	
	public void loadPharmOrderList(MedOrder medOrder) 
	{		
		orderHelper.loadPharmOrderList(em, medOrder);
	}

	public void loadDispOrderList(PharmOrder pharmOrder) 
	{
		orderHelper.loadDispOrderList(em, pharmOrder);
	}

	public List<MedOrderItem> retrieveMedOrderItemByItemNum(String orderNum, List<Integer> medOrderItemNumList) {
		return orderHelper.retrieveMedOrderItemByItemNum(em, orderNum, medOrderItemNumList);
	}
	
	public Set<DispOrder> retrieveDispOrderList(List<Invoice> voidInvoiceList) {
		return orderHelper.retrieveDispOrderList(voidInvoiceList);
	}
	
	public List<Invoice> markSfiInvoiceSysDeleted(List<Invoice> voidInvoiceList, List<Invoice> invoiceList){		
		return orderHelper.markSfiInvoiceSysDeleted(em, voidInvoiceList, invoiceList);
	}
	
	public void updateEndVettingOldRemarkItemStatus(PharmOrder pharmOrder, Long prevPharmOrderId, Boolean remarkFlag) {
		// note: updated remark item status of old MedOrderItem is NOT synchronized to cluster
		
		MedOrder medOrder = pharmOrder.getMedOrder();
		if (remarkFlag && medOrder.getDocType() == MedOrderDocType.Normal) {
			MedOrder deleteMedOrder = em.find(MedOrder.class, medOrder.getId());
			
			HashMap<Integer, MedOrderItem> deleteMoiStatusChangedHm = new HashMap<Integer, MedOrderItem>();  
			
			for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
				if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedAdd) {
					boolean recordFound = false;
					for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
						if (deleteMedOrderItem.getOrgItemNum().intValue() == medOrderItem.getOrgItemNum().intValue()) {
							recordFound = true;
						}
					}
					if ( ! recordFound) {
						// delete remarked item in reverse item add remark
						deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
						deleteMoiStatusChangedHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
					}
					
				} else if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify ||
							deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedDelete) {
					
					for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
						if (deleteMedOrderItem.getOrgItemNum().intValue() == medOrderItem.getOrgItemNum().intValue()) {
							boolean markItemDelete = false;
							boolean markOriginalItemDelete = false;
							
							if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.NoRemark) {
								markItemDelete = true; 	// delete remarked item in reverse item modify remark and item delete remark
								if (deleteMedOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
									// delete original item in reverse item modify remark
									markOriginalItemDelete = true;	 
								}
							}
							
							if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
								// if remark is modified again, delete the previous remark item
								markItemDelete = true;
							}
							
							if (markItemDelete) {
								deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
								deleteMoiStatusChangedHm.put(deleteMedOrderItem.getItemNum(), deleteMedOrderItem);
							}
							
							if (markOriginalItemDelete) {
								MedOrderItem orgMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
								orgMedOrderItem.setRemarkItemStatus(RemarkItemStatus.Deleted);
							}
						}
					}
				} 
			}			
			
			for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) {
				if (medOrderItem.getRemarkItemStatus() == RemarkItemStatus.UnconfirmedModify) {
					// mark remarkItemStatus of item update original item (must be in deleteMedOrder)
					MedOrderItem orgMedOrderItem = em.find(MedOrderItem.class, medOrderItem.getPrevMoItemId());
					if (orgMedOrderItem.getMedOrder() == deleteMedOrder) {
						orgMedOrderItem.setRemarkItemStatus(RemarkItemStatus.UnconfirmedOrginial);
						deleteMoiStatusChangedHm.put(orgMedOrderItem.getItemNum(), orgMedOrderItem);
					}
				}
			}
			
			for (MedOrderItem deleteMedOrderItem : deleteMedOrder.getMedOrderItemList()) {
				if ( ! deleteMoiStatusChangedHm.containsKey(deleteMedOrderItem.getItemNum())) {
					// mark other items in old MedOrder to old version
					deleteMedOrderItem.setRemarkItemStatus(RemarkItemStatus.OldVersion);
				}
			}
		}
		
		em.flush();
		em.clear();
	}
	
	public MedOrder saveMedOrder(
			MedOrder medOrder, 
			Boolean remarkFlag, 
			String hospCode,
			String patHospCode) {
		
		Date cmsUpdateDate = medOrder.getCmsUpdateDate(); 

		if (medOrder.getId() == null) 
		{ 
			// new manual order
			medOrder.replaceOrderNum(seqNumService.retrieveOrderNum(hospCode, patHospCode));
			medOrder.setRefNum(seqNumService.retrieveRefNum());
			this.insertMedOrder(medOrder);
			ownershipService.insertOwnership(medOrder.getOrderNum());
			em.flush();
			medOrder.setOrgMedOrderId(medOrder.getId());
		}
		else if (remarkFlag) 
		{ 
			// remark, manual order
			
			// replace Patient by the one in corp MedOrder
			// (because day-end will only update patient info in corp, the patient of cluster MedOrder may be outdated  
			MedOrder prevMedOrder = em.find(MedOrder.class, medOrder.getId());
			if (prevMedOrder.getPatient() != null) {
				medOrder.setPatient(prevMedOrder.getPatient());
			}
			
			// replace CmsExtraXml by the one in corp MedOrder
			// (because CmsExtraXml is lazy and not loaded to cluster MedOrder)
			medOrder.setCmsExtraXml(prevMedOrder.getCmsExtraXml());
			
			medOrder.setPrevMedOrderId(medOrder.getId());
			medOrder.clearId();
			this.insertMedOrder(medOrder);

			if (medOrder.getDocType() == MedOrderDocType.Manual) { // manual order
				medOrder.setPrevOrderNum(medOrder.getOrderNum());
				medOrder.replaceOrderNum(seqNumService.retrieveOrderNum(hospCode, patHospCode));
				if (!medOrder.isDhOrder()) {
					medOrder.setRefNum(seqNumService.retrieveNextRefNum(medOrder.getRefNum()));
					ownershipService.insertOwnership(medOrder.getOrderNum());
				}
			}
		}
		else 
		{ 
			// moe order / dh order
			MedOrder remoteMedOrder = medOrder;
			medOrder = em.find(MedOrder.class, medOrder.getId());
			medOrder.loadChild();
			medOrder.setUrgentFlag(remoteMedOrder.getUrgentFlag());
			medOrder.setPreVetUser(remoteMedOrder.getPreVetUser());
			medOrder.setPreVetDate(remoteMedOrder.getPreVetDate());
			medOrder.setPreVetUserName(remoteMedOrder.getPreVetUserName());
			if (medOrder.isDhOrder()) {				
				medOrder.setWorkstore(em.find(Workstore.class, remoteMedOrder.getWorkstore().getId()));
			}
			if ("-".equals(medOrder.getPatHospCode())) {
				medOrder.setPatHospCode(remoteMedOrder.getPatHospCode());
			}
			if (remoteMedOrder.getOrderNum() == null) {
				medOrder.replaceOrderNum(seqNumService.retrieveOrderNum(hospCode, patHospCode));
			}
			if (medOrder.getPatient() == null) {
				medOrder.setPatient(this.savePatient(remoteMedOrder.getPatient()));
			}
			for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
				MedOrderItem remoteMedOrderItem = remoteMedOrder.getMedOrderItemByItemNum(moi.getItemNum());
				if (remoteMedOrderItem == null) {
					continue;
				}
				moi.setRxItem(remoteMedOrderItem.getRxItem());
				moi.setPreVetNoteText(remoteMedOrderItem.getPreVetNoteText());
				moi.setPreVetNoteUser(remoteMedOrderItem.getPreVetNoteUser());
				moi.setPreVetNoteDate(remoteMedOrderItem.getPreVetNoteDate());
				moi.setDhRxDrug(remoteMedOrderItem.getDhRxDrug());
			}
		}
		
		medOrder.setWorkstationId(null);
		medOrder.setProcessingFlag(Boolean.FALSE);
		medOrder.setCmsUpdateDate(cmsUpdateDate);
				
		em.flush();
		
		return medOrder;
	}
	
	public void savePharmOrder(
			MedOrder medOrder, 
			PharmOrder pharmOrder, 
			Map<Integer, Integer> itemMap) {

		pharmOrder.setWorkstore(this.retrieveWorkstore(pharmOrder.getWorkstore()));

		MedCase medCase = pharmOrder.getMedCase();
		if (medCase != null) {
			medCase.setId(null);	
			pharmOrder.setMedCase(this.saveMedCase(medCase));
		}
		
		pharmOrder.replace(medOrder, itemMap);

		em.persist(pharmOrder);

		em.flush();		
	}
	
	public void saveDispOrder(
			PharmOrder pharmOrder,
			DispOrder dispOrder,
			DispOrder prevDispOrder) {

		dispOrder.replace(pharmOrder);
		dispOrder.setWorkstore(this.retrieveWorkstore(dispOrder.getWorkstore()));		
		dispOrder.setTicket(this.saveTicket(dispOrder.getTicket()));
		dispOrder.setPatient(pharmOrder.getMedOrder().getPatient());

		Integer dispOrderNum = seqNumService.retrieveDispOrderNum();
		dispOrder.setDispOrderNum(dispOrderNum);
		if (prevDispOrder == null) {
			dispOrder.setOrgDispOrderNum(dispOrderNum);
		}
		else {
			dispOrder.setOrgDispOrderNum(prevDispOrder.getOrgDispOrderNum());
			dispOrder.setPrevDispOrderNum(prevDispOrder.getDispOrderNum());
			
			Set<DispOrderItem> delDispOrderItemSet = new HashSet<DispOrderItem>(prevDispOrder.getDispOrderItemList());
			for (DispOrderItem dispOrderItem : dispOrder.getDispOrderItemList()) {
				DispOrderItem prevDispOrderItem = null;
				if( dispOrder.getRefillFlag() ){
					prevDispOrderItem = prevDispOrder.getDispOrderItemByPoOrgItemNum(dispOrderItem.getPharmOrderItem().getOrgItemNum());
					if( prevDispOrderItem != null && prevDispOrderItem.getItemNum().equals(dispOrderItem.getItemNum()) ){
						delDispOrderItemSet.remove(prevDispOrderItem);
					}
				}else{
					prevDispOrderItem = prevDispOrder.getDispOrderItemByPoItemNum(dispOrderItem.getPharmOrderItem().getItemNum());
					if (prevDispOrderItem != null) {
						delDispOrderItemSet.remove(prevDispOrderItem);
					}
				}
			}
			for (DispOrderItem delDispOrderItem : delDispOrderItemSet) {
				delDispOrderItem.setStatus(DispOrderItemStatus.Deleted);
			}
		}

		dispOrder.disconnectInvoiceList();
		
		em.persist(dispOrder);
				
		dispOrder.reconnectInvoiceList();

		for (Invoice invoice : dispOrder.getInvoiceList()) {
			em.persist(invoice);
		}

		em.flush();
	}
	
	public void saveCapdVoucher(
			DispOrder dispOrder) {
		
		for (CapdVoucher capdVoucher : dispOrder.getCapdVoucherList()) {
			em.persist(capdVoucher);
		}

		em.flush();
	}
	
	public MedOrder updateMedOrderStatus(MedOrder medOrder, Date prevVetDate) {

		this.loadPharmOrderList(medOrder);				
				
		for (PharmOrder pharmOrder : new ArrayList<PharmOrder>(medOrder.getPharmOrderList())) {
			pharmOrder.loadChild();
		}
				
		// update MedOrder status
		if (medOrder.getPharmOrderList().isEmpty())  {
			medOrder.setStatus(MedOrderStatus.Outstanding);
		} else  {
			medOrder.setStatus(MedOrderStatus.Complete);
		}
		
		// update MedOrderItem status
		for (MedOrderItem medOrderItem : medOrder.getMedOrderItemList()) 
		{
			if (medOrderItem.getStatus() == MedOrderItemStatus.SysDeleted ||
				medOrderItem.getStatus() == MedOrderItemStatus.Deleted) {
				continue;
			}
			
			if (medOrderItem.getPharmOrderItemList().isEmpty()) {
				medOrderItem.setStatus(MedOrderItemStatus.Outstanding);
			} else {
				medOrderItem.setStatus(MedOrderItemStatus.Completed);
			}
		}
		
		medOrder.setVetBeforeFlag(Boolean.TRUE);
		if (medOrder.getVetDate() == null) {
			if (prevVetDate == null) {
				medOrder.setVetDate(Calendar.getInstance().getTime());
			}
			else {
				medOrder.setVetDate(prevVetDate);
			}
		}
		
		medOrder.disconnectPharmOrderList();

		return medOrder;
	}
	
	@SuppressWarnings("unchecked")
	@CacheResult
	public String retrieveClusterCodeByPatHospCode(String patHospCode) {
		
		List<String> workstoreGroupCodeList = em.createQuery(
				"select o.workstoreGroupCode from WorkstoreGroupMapping o" + // 20120213 index check : none
				" where o.patHospCode = :patHospCode")
				.setParameter("patHospCode", patHospCode)
				.getResultList();

		List<String> hospitalClusterList = em.createQuery(
				"select distinct o.hospital.hospitalCluster.clusterCode from Workstore o" + // 20120213 index check : none
				" where o.workstoreGroup.workstoreGroupCode in :workstoreGroupCodeList ")
				.setParameter("workstoreGroupCodeList", workstoreGroupCodeList)
				.getResultList();
		
		if (hospitalClusterList.isEmpty()) {
			throw new UnsupportedOperationException("Cluster code not found for patHospCode " + patHospCode);
		} else if (hospitalClusterList.size() > 1) {
			throw new UnsupportedOperationException("Data integrity error! More than 1 cluster code found for patHospCode " + patHospCode);
		}
		
		return hospitalClusterList.get(0);	
	}	
	
	@SuppressWarnings("unchecked")
	public List<DispOrder> retrieveDispOrderListByIdList(List<Long> dispOrderIdList) {
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120228 index check : DispOrder.id : PK_DISP_ORDER
				" where o.id in :dispOrderIdList")
				.setParameter("dispOrderIdList", dispOrderIdList)
				.setHint(QueryHints.BATCH, "o.ticket")
				.setHint(QueryHints.BATCH, "o.pharmOrder.medCase")
				.setHint(QueryHints.BATCH, "o.pharmOrder.medOrder")
				.getResultList();
		
		for (DispOrder dispOrder : dispOrderList) {
			dispOrder.getTicket();
			dispOrder.getPharmOrder().getMedCase();
			dispOrder.getPharmOrder().getMedOrder();
		}		
		
		return dispOrderList;
	}		
}
