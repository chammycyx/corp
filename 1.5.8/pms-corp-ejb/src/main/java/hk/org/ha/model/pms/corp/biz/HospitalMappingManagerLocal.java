package hk.org.ha.model.pms.corp.biz;

import java.util.List;

import javax.ejb.Local;

@Local
public interface HospitalMappingManagerLocal {
	List<String> retrievePatHospCodeList(String dispHospCode);
}
