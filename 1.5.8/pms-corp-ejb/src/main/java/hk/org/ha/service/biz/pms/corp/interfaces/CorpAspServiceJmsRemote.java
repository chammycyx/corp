package hk.org.ha.service.biz.pms.corp.interfaces;

import java.util.List;
import java.util.Map;

import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.asp.persistence.AspHospSpec;
import hk.org.ha.model.pms.asp.persistence.AspItem;


public interface CorpAspServiceJmsRemote {

	List<AspHosp> retrieveAspHospList();
	
	AspHosp retrieveAspHospByHospCode(String hospCode);

	void updateAspHosp(AspHosp aspHosp);
	
	Map<String, AspItem> retrieveAspItemMap();
	
	void updateAspHospSpec(List<AspHospSpec> aspHospSpecList);
	
	List<String> retrieveAspItemCodeList();
	
	List<String> retrievePatHospCodeList(String dispHospCode);
}
