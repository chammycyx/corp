package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DrugDispensaryDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private Integer drugItemNum;
	
	@XmlElement
	private String dspnsRecordNum;
	
	@XmlElement
	private String action;
	
	@XmlElement
	private String itemCode;
	
	@XmlElement
	private String universalTradeName;
	
	@XmlElement
	private String genericName;
	
	@XmlElement
	private String itemDesc;
	
	@XmlElement
	private String hkRegNum;
	
	@XmlElement
	private String prodNum;
	
	@XmlElement
	private BigDecimal dispensedQty;
	
	@XmlElement
	private String dispensedUnitName;
	
	@XmlElement
	private String strength;
	
	@XmlElement
	private String batchNum;
	
	@XmlElement
	private Date expiryDate;
	
	@XmlElement
	private String dispensedDrugRecogTerm;
	
	@XmlElement
	private String dispensedDrugIDRecogTerm;
	
	@XmlElement
	private String dispensedDrugDescRecogTerm;

	public Integer getDrugItemNum() {
		return drugItemNum;
	}

	public void setDrugItemNum(Integer drugItemNum) {
		this.drugItemNum = drugItemNum;
	}

	public String getDspnsRecordNum() {
		return dspnsRecordNum;
	}

	public void setDspnsRecordNum(String dspnsRecordNum) {
		this.dspnsRecordNum = dspnsRecordNum;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getUniversalTradeName() {
		return universalTradeName;
	}

	public void setUniversalTradeName(String universalTradeName) {
		this.universalTradeName = universalTradeName;
	}

	public String getGenericName() {
		return genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getHkRegNum() {
		return hkRegNum;
	}

	public void setHkRegNum(String hkRegNum) {
		this.hkRegNum = hkRegNum;
	}

	public String getProdNum() {
		return prodNum;
	}

	public void setProdNum(String prodNum) {
		this.prodNum = prodNum;
	}

	public BigDecimal getDispensedQty() {
		return dispensedQty;
	}

	public void setDispensedQty(BigDecimal dispensedQty) {
		this.dispensedQty = dispensedQty;
	}

	public String getDispensedUnitName() {
		return dispensedUnitName;
	}

	public void setDispensedUnitName(String dispensedUnitName) {
		this.dispensedUnitName = dispensedUnitName;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDispensedDrugRecogTerm() {
		return dispensedDrugRecogTerm;
	}

	public void setDispensedDrugRecogTerm(String dispensedDrugRecogTerm) {
		this.dispensedDrugRecogTerm = dispensedDrugRecogTerm;
	}

	public String getDispensedDrugIDRecogTerm() {
		return dispensedDrugIDRecogTerm;
	}

	public void setDispensedDrugIDRecogTerm(String dispensedDrugIDRecogTerm) {
		this.dispensedDrugIDRecogTerm = dispensedDrugIDRecogTerm;
	}

	public String getDispensedDrugDescRecogTerm() {
		return dispensedDrugDescRecogTerm;
	}

	public void setDispensedDrugDescRecogTerm(String dispensedDrugDescRecogTerm) {
		this.dispensedDrugDescRecogTerm = dispensedDrugDescRecogTerm;
	}	
}
