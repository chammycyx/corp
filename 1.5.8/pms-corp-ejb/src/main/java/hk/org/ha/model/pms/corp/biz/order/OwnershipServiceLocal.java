package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.corp.persistence.order.Ownership;

import javax.ejb.Local;

@Local
public interface OwnershipServiceLocal  {

	Boolean requestOwnership(String orderNum, String hospCode);

	Boolean releaseOwnership(String orderNum, String hospCode);

	Ownership retrieveAndLockOwnership(String orderNum);

	Ownership retrieveOwnership(String orderNum);

	Ownership insertOwnership(String orderNum);
	
	Boolean checkOwnershipLocked(String orderNum);
}
