package hk.org.ha.model.pms.corp.biz.cddh;

import java.util.Map;

import javax.ejb.Local;

import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.udt.label.DeltaChangeType;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpCddhCheckServiceJmsRemote;

@Local
public interface CorpCddhCheckServiceLocal extends CorpCddhCheckServiceJmsRemote {
	Map<Long, DeltaChangeType> updateDeltaChangeTypeForLabel(DispOrder dispOrder);
}
