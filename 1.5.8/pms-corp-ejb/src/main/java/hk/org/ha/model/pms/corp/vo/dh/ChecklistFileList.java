package hk.org.ha.model.pms.corp.vo.dh;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ChecklistFileList implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="File")
	private List<ChecklistFile> fileList;

	public ChecklistFileList() {
	}

	public ChecklistFileList(List<ChecklistFile> fileList) {
		this.fileList = fileList;
	}

	public List<ChecklistFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<ChecklistFile> fileList) {
		this.fileList = fileList;
	}
}
