package hk.org.ha.model.pms.corp.persistence.order;

import hk.org.ha.model.pms.persistence.BaseEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "BATCH_JOB_STATE")
public class BatchJobState extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batchJobStateSeq")
	@SequenceGenerator(name = "batchJobStateSeq", sequenceName = "SQ_BATCH_JOB_STATE", initialValue = 100000000)
	private Long id;
	
	@Column(name = "JOB_NAME", nullable = false, length = 100)
	private String jobName;
	
	@Column(name = "BATCH_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date batchDate;

	@Column(name = "HOSP_CODE", length = 3)
	private String hospCode;
	
	@Column(name = "WORKSTORE_CODE", length = 4)
	private String workstoreCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getHospCode() {
		return hospCode;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
}
