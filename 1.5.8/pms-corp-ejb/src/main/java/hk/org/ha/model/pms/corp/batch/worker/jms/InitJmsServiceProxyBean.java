package hk.org.ha.model.pms.corp.batch.worker.jms;

import hk.org.ha.fmk.pms.batch.chunker.Chunk;
import hk.org.ha.fmk.pms.batch.chunker.Record;
import hk.org.ha.fmk.pms.batch.controller.BatchParameters;
import hk.org.ha.fmk.pms.batch.worker.BatchWorkerInfo;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpSubscriberJmsRemote;
import hk.org.ha.service.biz.pms.interfaces.PmsSubscriberJmsRemote;

import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * Session Bean implementation class InitCacheBean
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("initJmsServiceProxyBean")
public class InitJmsServiceProxyBean implements InitJmsServiceProxyLocal {
	
	private static final String PMS = "PMS";	
	private static final String CORP = "CORP";
	
	private Logger logger = null;
	
	private String appCode = null;	
	
	private String clusterCode = null;
			
	public void initBatch(BatchWorkerInfo info, Logger logger) throws Exception {
		this.logger = logger;
		
		BatchParameters params = info.getBatchParameters();
		
		appCode = params.getString("appCode");
		
		clusterCode = params.getString("clusterCode");		
	}

	public void beginChunk(Chunk arg0) throws Exception {
	}

	public void processRecord(Record arg0) throws Exception {
    	Date startDatetime = new Date();
        logger.info("process record start..." + startDatetime);
        
        if (appCode == null || clusterCode != null || PMS.equals(appCode)) {
	        PmsSubscriberJmsRemote pmsSubscriberProxy = (PmsSubscriberJmsRemote) Component.getInstance("pmsSubscriberProxy");
	        if (pmsSubscriberProxy != null) {
	        	if (clusterCode == null) {
	        		pmsSubscriberProxy.initJmsServiceProxy();
	        	} else {
	        		pmsSubscriberProxy.initJmsServiceProxyByCluster(clusterCode);
	        	}
	        }
        }

        if ((appCode == null && clusterCode == null) || CORP.equals(appCode)) {
	        CorpSubscriberJmsRemote corpSubscriberProxy = (CorpSubscriberJmsRemote) Component.getInstance("corpSubscriberProxy");
	        if (corpSubscriberProxy != null) {
        		corpSubscriberProxy.initJmsServiceProxy();     
	        }
        }
		
		Date endDatetime = new Date();
        logger.info("process record complete..." + 
        		endDatetime + "(" + (endDatetime.getTime() - startDatetime.getTime()) + "ms)");
	}

	public void endChunk() throws Exception {
	}

    @Remove
	public void destroyBatch() {
	}
}
