package hk.org.ha.model.pms.corp.persistence.order;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Hospital;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MANUAL_INVOICE_NUM_RANGE")
public class ManualInvoiceNumRange extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", length = 3)
	private String hospCode;

	@Id
	@Column(name = "HOST_NAME", length = 15)
	private String hostName;
	
    @ManyToOne
    @JoinColumn(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
    private Hospital hospital;

	@Column(name = "MIN_NUM",nullable = false, precision = 4, scale = 0)
	private Integer minNum;

	@Column(name = "MAX_NUM", nullable = false, precision = 4, scale = 0)
	private Integer maxNum;
	
	public String getHospCode() {
		return hospCode;
	}
	
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostName() {
		return hostName;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public void setMinNum(Integer minNum) {
		this.minNum = minNum;
	}

	public Integer getMinNum() {
		return minNum;
	}

	public void setMaxNum(Integer maxNum) {
		this.maxNum = maxNum;
	}

	public Integer getMaxNum() {
		return maxNum;
	}

}
