-- PMS 
create user PMS identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp
quota unlimited on PMS_DATA_01
quota unlimited on PMS_INDX_01;

grant create session to PMS;
grant resource to PMS;

create user PMS_APP_USER identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_APP_USER;
grant create synonym to PMS_APP_USER;

create user PMS_RPT_USER identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_RPT_USER;
grant create synonym to PMS_RPT_USER;

create user PMS_LOG_USER identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_LOG_USER;
grant create synonym to PMS_LOG_USER;

create user PMS_BATCH_USER identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_BATCH_USER;
grant create synonym to PMS_BATCH_USER;

-- OG
create user OG identified by <PASSWORD>
default tablespace OG_DATA_01
temporary tablespace temp
quota unlimited on OG_DATA_01
quota unlimited on OG_INDX_01;

grant create session to OG;
grant resource to OG;

create user OG_APP_USER identified by <PASSWORD>
default tablespace OG_DATA_01
temporary tablespace temp;

grant create session to OG_APP_USER;
grant create synonym to OG_APP_USER;

-- SUPPORT
create user PMS_SUPPORT identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_SUPPORT;

-- UAM
create user UAM identified by <PASSWORD>
default tablespace UAM_DATA_01
temporary tablespace temp;
alter user UAM quota unlimited on UAM_DATA_01;
alter user UAM quota unlimited on UAM_INDX_01;
alter user UAM quota unlimited on UAM_LOG_INDX_01;

grant create session to UAM;
grant resource to UAM;

create user UAM_APP_USER identified by <PASSWORD>
default tablespace UAM_DATA_01
temporary tablespace temp;
alter user UAM_APP_USER quota unlimited on UAM_DATA_01;
alter user UAM_APP_USER quota unlimited on UAM_INDX_01;
alter user UAM_APP_USER quota unlimited on UAM_LOG_INDX_01;

grant create session to UAM_APP_USER;
grant create synonym to UAM_APP_USER;

--//@UNDO
DROP USER PMS;
DROP USER PMS_APP_USER;
DROP USER PMS_RPT_USER;
DROP USER PMS_LOG_USER;
DROP USER PMS_BATCH_USER;
DROP USER PMS_SUPPORT;
DROP USER UAM;
DROP USER OG;
DROP USER DPP;
DROP USER DQA;


