-- CDH
create user CDH identified by <PASSWORD>
default tablespace CDH_DATA_01
temporary tablespace temp
quota unlimited on CDH_DATA_01
quota unlimited on CDH_INDX_01;

grant create session to CDH;
grant resource to CDH;
grant create view to CDH;
grant create procedure to CDH;
grant create function to CDH;

create user CDH_APP_USER identified by <PASSWORD>
default tablespace CDH_DATA_01
temporary tablespace temp;

grant create session to CDH_APP_USER;
grant create synonym to CDH_APP_USER;

-- SUPPORT
create user PMS_SUPPORT identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_SUPPORT;

--//@UNDO
DROP USER CDH;
DROP USER CDHS_APP_USER;
DROP USER PMS_SUPPORT;
