-- PMS 
create user PMS identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp
quota unlimited on PMS_DATA_01
quota unlimited on PMS_INDX_01;

grant create session to PMS;
grant resource to PMS;

create user PMS_APP_USER identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_APP_USER;
grant create synonym to PMS_APP_USER;

-- SUPPORT
create user PMS_SUPPORT identified by <PASSWORD>
default tablespace PMS_DATA_01
temporary tablespace temp;

grant create session to PMS_SUPPORT;

--//@UNDO
DROP USER PMS;
DROP USER PMS_APP_USER;
DROP USER PMS_SUPPORT;
