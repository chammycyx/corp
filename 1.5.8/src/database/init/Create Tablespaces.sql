-- PMS 
create tablespace PMS_DATA_01 datafile '+PMSCORS1/PMS_DATA01_01.dbf'
size 512M autoextend off 
extent management local segment space management auto;

create tablespace PMS_INDX_01 datafile '+PMSCORS1/PMS_INDX01_01.dbf'
size 128M
autoextend off
extent management local segment space management auto;

create tablespace PMS_LOG_INDX_01 datafile '+PMSCORS1/PMS_LOG_INDX01_01.dbf'
size 128M
autoextend off
extent management local segment space management auto;

create tablespace P_PMS_DISP_DATA_2012 datafile '+PMSCORS1/P_PMS_DISP_DATA_2012.dbf'
size 512M
autoextend off
extent management local segment space management auto;

create tablespace P_PMS_MP_DATA_2012  datafile '+PMSCORS1/P_PMS_MP_DATA_2012.dbf'
size 512M
autoextend off
extent management local segment space management auto;

create tablespace P_PMS_LOG_DATA_2012 datafile '+PMSCORS1/P_PMS_LOG_DATA_2012.dbf'
size 512M
autoextend off
extent management local segment space management auto;

-- UAM
create tablespace UAM_DATA_01 datafile '+PMSCORS1/UAM_DATA_01.dbf' size 128M autoextend off extent management local segment space management auto;
create tablespace UAM_INDX_01 datafile '+PMSCORS1/UAM_INDX_01.dbf' size 128M autoextend off extent management local segment space management auto;
create tablespace UAM_LOG_INDX_01 datafile '+PMSCORS1/UAM_LOG_INDX_01.dbf' size 128M autoextend off extent management local segment space management auto;
create tablespace P_UAM_LOG_DATA_2012 datafile '+PMSCORS1/P_UAM_LOG_DATA_2012.dbf' size 128M autoextend off extent management local segment space management auto;

-- OG
create tablespace OG_DATA_01 datafile '+PMSCORS1/OG_DATA_01.dbf' size 128M autoextend off extent management local segment space management auto;
create tablespace OG_INDX_01 datafile '+PMSCORS1/OG_INDX_01.dbf' size 128M autoextend off extent management local segment space management auto;

create tablespace OG_LOG_INDX_01 datafile '+PMSCORS1/OG_LOG_INDX_01.dbf' size 128M autoextend off extent management local segment space management auto;
create tablespace P_OG_DATA_2012 datafile '+PMSCORS1/P_OG_DATA_2012.dbf' size 128M autoextend off extent management local segment space management auto;
create tablespace P_OG_LOG_DATA_2012 datafile '+PMSCORS1/P_OG_LOG_DATA_2012.dbf' size 128M autoextend off extent management local segment space management auto;

-- DMS
create tablespace DMS_DATA_01 datafile '+PMSCMSS1/DMS_DATA_01.dbf' size 2G autoextend off extent management local segment space management auto;
create tablespace DMS_INDX_01 datafile '+PMSCMSS1/DMS_INDX_01.dbf' size 2G autoextend off extent management local segment space management auto;

-- CMM

-- DPP

-- DQA

-- CDH
create tablespace CDH_DATA_01 datafile '+PMSCDHS1/CDH_DATA_01.dbf' size 4G autoextend off extent management local segment space management auto;
create tablespace CDH_INDX_01 datafile '+PMSCDHS1/CDH_INDX_01.dbf' size 1G autoextend off extent management local segment space management auto;
