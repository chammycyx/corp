create user DMS identified by <PASSWORD>
default tablespace DMS_DATA_01
temporary tablespace temp
quota unlimited on DMS_DATA_01
quota unlimited on DMS_INDX_01;

grant create session to DMS;
grant resource to DMS;

grant aq_administrator_role to DMS;
grant execute on sys.dbms_aqadm to DMS;
grant execute on sys.dbms_aq to DMS;
grant execute on sys.dbms_aqin to DMS;
grant execute on sys.dbms_aqjms to DMS;

create user DMS_APP_USER identified by <PASSWORD> 
default tablespace DMS_DATA_01
temporary tablespace temp;

grant create session to DMS_APP_USER;
grant create synonym to DMS_APP_USER;

-- CMM
create user CMM identified by <PASSWORD>
default tablespace CMM_DATA_01
temporary tablespace temp
quota unlimited on CMM_DATA_01
quota unlimited on CMM_INDX_01;

create user CMM_APP_USER identified by <PASSWORD> 
default tablespace CMM_DATA_01
temporary tablespace temp;

grant create session to DMS_APP_USER;
grant create synonym to DMS_APP_USER;


--//@UNDO
DROP USER DMS;
DROP USER DMS_APP_USER;
DROP USER CMM;
DROP USER CMM_APP_USER;


