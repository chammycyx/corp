update PROP set DESCRIPTION='[consult CD3 for change] Duration (in days) of ended Rx to be retained for Rx reconciliation in OP Vetting', MAINT_SCREEN='-', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32063;

--//@UNDO
update PROP set DESCRIPTION='Duration (in days) of ended Rx to be retained for Rx reconciliation in OP Vetting', MAINT_SCREEN='S', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32063;
--//