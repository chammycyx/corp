drop index I_MED_ORDER_02;
create index I_MED_ORDER_02 on MED_ORDER (PAT_HOSP_CODE,  STATUS,  ORDER_TYPE, ORDER_DATE) local online;


--//@UNDO
drop index I_MED_ORDER_02;
create index I_MED_ORDER_02 on MED_ORDER (ORDER_TYPE, ORDER_DATE) local online;
--//