alter table DISP_ORDER add TMP_BATCH_PROCESSING_FLAG number(1) default 0;

update DISP_ORDER set TMP_BATCH_PROCESSING_FLAG = BATCH_PROCESSING_FLAG where BATCH_PROCESSING_FLAG <> 0;
alter table DISP_ORDER modify TMP_BATCH_PROCESSING_FLAG not null;

alter table DISP_ORDER rename column BATCH_PROCESSING_FLAG to DEL_BATCH_PROCESSING_FLAG;
alter table DISP_ORDER rename column TMP_BATCH_PROCESSING_FLAG to BATCH_PROCESSING_FLAG;
alter table DISP_ORDER drop column DEL_BATCH_PROCESSING_FLAG;

comment on column DISP_ORDER.BATCH_PROCESSING_FLAG is 'Batch processing';

--//@UNDO
alter table DISP_ORDER add TMP_BATCH_PROCESSING_FLAG varchar2(1) default '0';

update DISP_ORDER set TMP_BATCH_PROCESSING_FLAG = BATCH_PROCESSING_FLAG where BATCH_PROCESSING_FLAG <> '0';
alter table DISP_ORDER modify TMP_BATCH_PROCESSING_FLAG not null;

alter table DISP_ORDER rename column BATCH_PROCESSING_FLAG to DEL_BATCH_PROCESSING_FLAG;
alter table DISP_ORDER rename column TMP_BATCH_PROCESSING_FLAG to BATCH_PROCESSING_FLAG;

alter table DISP_ORDER drop column DEL_BATCH_PROCESSING_FLAG;

comment on column DISP_ORDER.BATCH_PROCESSING_FLAG is 'Batch processing';
--//