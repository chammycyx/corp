alter table PROP add REMINDER_MESSAGE varchar2(1000) null;

comment on column PROP.REMINDER_MESSAGE is 'Reminder message';

--//@UNDO
alter table PROP drop column REMINDER_MESSAGE;
--//