alter table PHARM_ORDER add REFILL_COUNT number(4) null;
update PHARM_ORDER set REFILL_COUNT=0;

--// No need to update in pms-corp
--update PHARM_ORDER p1
--set p1.REFILL_COUNT = ( select count(*) from REFILL_SCHEDULE r1 where r1.STATUS <> 'N' and r1.REFILL_NUM <> 1 and r1.PHARM_ORDER_ID = p1.ID )
--WHERE p1.STATUS = 'C';

alter table PHARM_ORDER modify REFILL_COUNT not null;

comment on column PHARM_ORDER.REFILL_COUNT is 'Dispensed RefillSchedule counter';

--//@UNDO
alter table PHARM_ORDER drop column REFILL_COUNT;
--//