drop index I_DISP_ORDER_01;
create index I_DISP_ORDER_01 on DISP_ORDER (HOSP_CODE, DISP_DATE) local;


--//@UNDO
drop index I_DISP_ORDER_01;
create index I_DISP_ORDER_01 on DISP_ORDER (DISP_DATE) local;
--//