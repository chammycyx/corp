alter table MED_ORDER add ON_HAND_SOURCE_TYPE varchar2(1);
comment on column MED_ORDER.ON_HAND_SOURCE_TYPE is 'On hand source type';

--//@UNDO
alter table MED_ORDER set unused column ON_HAND_SOURCE_TYPE;
--//