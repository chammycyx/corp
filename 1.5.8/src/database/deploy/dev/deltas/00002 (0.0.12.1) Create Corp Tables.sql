create table CDDH_SPECIALTY_MAPPING (
    SPEC_CODE varchar2(4) not null,
    HOSP_CODE varchar2(3) not null,
    CDDH_SPEC_CODE varchar2(4) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CDDH_SPECIALTY_MAPPING primary key (HOSP_CODE, SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CDDH_SPECIALTY_MAPPING is 'Specialty mapping for CDDH display';
comment on column CDDH_SPECIALTY_MAPPING.SPEC_CODE is 'Specialty code';
comment on column CDDH_SPECIALTY_MAPPING.HOSP_CODE is 'Hospital code';
comment on column CDDH_SPECIALTY_MAPPING.CDDH_SPEC_CODE is 'Specialty code for CDDH display';
comment on column CDDH_SPECIALTY_MAPPING.STATUS is 'Record status';
comment on column CDDH_SPECIALTY_MAPPING.CREATE_USER is 'Created user';
comment on column CDDH_SPECIALTY_MAPPING.CREATE_DATE is 'Created date';
comment on column CDDH_SPECIALTY_MAPPING.UPDATE_USER is 'Last updated user';
comment on column CDDH_SPECIALTY_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column CDDH_SPECIALTY_MAPPING.VERSION is 'Version to serve as optimistic lock value';


create table CDDH_WORKSTORE_GROUP (
    CDDH_WORKSTORE_GROUP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CDDH_WORKSTORE_GROUP primary key (CDDH_WORKSTORE_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CDDH_WORKSTORE_GROUP is 'Workstore group for CDDH display';
comment on column CDDH_WORKSTORE_GROUP.CDDH_WORKSTORE_GROUP_CODE is 'Workstore group code for CDDH display';
comment on column CDDH_WORKSTORE_GROUP.STATUS is 'Record status';
comment on column CDDH_WORKSTORE_GROUP.CREATE_USER is 'Created user';
comment on column CDDH_WORKSTORE_GROUP.CREATE_DATE is 'Created date';
comment on column CDDH_WORKSTORE_GROUP.UPDATE_USER is 'Last updated user';
comment on column CDDH_WORKSTORE_GROUP.UPDATE_DATE is 'Last updated date';
comment on column CDDH_WORKSTORE_GROUP.VERSION is 'Version to serve as optimistic lock value';


create table CDDH_WORKSTORE_GROUP_HOS (
    HOSP_CODE varchar2(3) not null,
    CDDH_WORKSTORE_GROUP_CODE varchar2(3) not null,
    constraint PK_CDDH_WORKSTORE_GROUP_HOS primary key (HOSP_CODE, CDDH_WORKSTORE_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CDDH_WORKSTORE_GROUP_HOS is 'Middle table of CDDH workstore group and hospital';
comment on column CDDH_WORKSTORE_GROUP_HOS.HOSP_CODE is 'Hospital code';
comment on column CDDH_WORKSTORE_GROUP_HOS.CDDH_WORKSTORE_GROUP_CODE is 'Workstore group code for CDDH display';


create table HOSPITAL (
    HOSP_CODE varchar2(3) not null,
    CLUSTER_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL primary key (HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL is 'Hospitals';
comment on column HOSPITAL.HOSP_CODE is 'Hospital code';
comment on column HOSPITAL.CLUSTER_CODE is 'Hospital cluster code';
comment on column HOSPITAL.STATUS is 'Record status';
comment on column HOSPITAL.CREATE_USER is 'Created user';
comment on column HOSPITAL.CREATE_DATE is 'Created date';
comment on column HOSPITAL.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_CLUSTER (
    CLUSTER_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_CLUSTER primary key (CLUSTER_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_CLUSTER is 'Hospital clusters';
comment on column HOSPITAL_CLUSTER.CLUSTER_CODE is 'Hospital cluster code';
comment on column HOSPITAL_CLUSTER.STATUS is 'Record status';
comment on column HOSPITAL_CLUSTER.CREATE_USER is 'Created user';
comment on column HOSPITAL_CLUSTER.CREATE_DATE is 'Created date';
comment on column HOSPITAL_CLUSTER.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_CLUSTER.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_CLUSTER.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_CLUSTER_TASK (
    CLUSTER_CODE varchar2(3) not null,
    TASK_NAME varchar2(100) not null,
    TASK_DATE timestamp null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_CLUSTER_TASK primary key (CLUSTER_CODE, TASK_NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_CLUSTER_TASK is 'Task control of hospital cluster to ensure only one WebLogic node running the listed tasks';
comment on column HOSPITAL_CLUSTER_TASK.CLUSTER_CODE is 'Hospital cluster code';
comment on column HOSPITAL_CLUSTER_TASK.TASK_NAME is 'Task name';
comment on column HOSPITAL_CLUSTER_TASK.TASK_DATE is 'Task execution date';
comment on column HOSPITAL_CLUSTER_TASK.CREATE_USER is 'Created user';
comment on column HOSPITAL_CLUSTER_TASK.CREATE_DATE is 'Created date';
comment on column HOSPITAL_CLUSTER_TASK.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_CLUSTER_TASK.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_CLUSTER_TASK.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_MAPPING (
    HOSP_CODE varchar2(3) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_MAPPING primary key (HOSP_CODE, PAT_HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_MAPPING is 'Relationship between patient hospitals and dispensing hospitals to allow orders of the patient hospital dispensed in the corresponding dispensing hospitals';
comment on column HOSPITAL_MAPPING.HOSP_CODE is 'Hospital code';
comment on column HOSPITAL_MAPPING.PAT_HOSP_CODE is 'Patient hospital code';
comment on column HOSPITAL_MAPPING.STATUS is 'Record status';
comment on column HOSPITAL_MAPPING.CREATE_USER is 'Created user';
comment on column HOSPITAL_MAPPING.CREATE_DATE is 'Created date';
comment on column HOSPITAL_MAPPING.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_MAPPING.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE_GROUP (
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE_GROUP primary key (WORKSTORE_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE_GROUP is 'Group of workstores using the same set of drugs';
comment on column WORKSTORE_GROUP.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WORKSTORE_GROUP.STATUS is 'Record status';
comment on column WORKSTORE_GROUP.CREATE_USER is 'Created user';
comment on column WORKSTORE_GROUP.CREATE_DATE is 'Created date';
comment on column WORKSTORE_GROUP.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE_GROUP.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE_GROUP.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE (
    WORKSTORE_CODE varchar2(4) not null,
    HOSP_CODE varchar2(3) not null,
    DEF_PAT_HOSP_CODE varchar2(3) null,
    OPERATION_PROFILE_ID number(19) null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CDDH_WORKSTORE_GROUP_CODE varchar2(3) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE primary key (HOSP_CODE, WORKSTORE_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE is 'Pharmacy working stores';
comment on column WORKSTORE.WORKSTORE_CODE is 'Workstore code';
comment on column WORKSTORE.HOSP_CODE is 'Hospital code';
comment on column WORKSTORE.DEF_PAT_HOSP_CODE is 'Default patient hospital code';
comment on column WORKSTORE.OPERATION_PROFILE_ID is 'Operation profile id';
comment on column WORKSTORE.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WORKSTORE.CDDH_WORKSTORE_GROUP_CODE is 'Workstore group code for CDDH display';
comment on column WORKSTORE.STATUS is 'Record status';
comment on column WORKSTORE.CREATE_USER is 'Created user';
comment on column WORKSTORE.CREATE_DATE is 'Created date';
comment on column WORKSTORE.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE_GROUP_MAPPING (
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    SORT_SEQ number(10) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE_GROUP_MAPPING primary key (WORKSTORE_GROUP_CODE, PAT_HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE_GROUP_MAPPING is 'Relationship between patient hospitals and workstore groups to maintain the mapping of PHS and PAS specialities';
comment on column WORKSTORE_GROUP_MAPPING.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WORKSTORE_GROUP_MAPPING.PAT_HOSP_CODE is 'Patient hospital code';
comment on column WORKSTORE_GROUP_MAPPING.STATUS is 'Record status';
comment on column WORKSTORE_GROUP_MAPPING.SORT_SEQ is 'Sort sequence';
comment on column WORKSTORE_GROUP_MAPPING.CREATE_USER is 'Created user';
comment on column WORKSTORE_GROUP_MAPPING.CREATE_DATE is 'Created date';
comment on column WORKSTORE_GROUP_MAPPING.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE_GROUP_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE_GROUP_MAPPING.VERSION is 'Version to serve as optimistic lock value';


-- Corp Only

create table FM_RPT_STAT (
    ID number(19) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    EIS_SPEC_CODE varchar2(4) not null,
    YEAR number(10) not null,
    MONTH number(10) not null,
    CLUSTER_CODE varchar2(3) not null,
    FISCAL_YEAR number(10) not null,
    PAT_COUNT number(10) not null,
    PAT_SFI_COUNT number(10) not null,
    ORDER_COUNT number(10) not null,
    ORDER_SFI_COUNT number(10) not null,
    ORDER_ITEM_COUNT number(10) not null,
    ISSUE_ORDER_COUNT number(10) not null,
    GENERAL_DRUG_COUNT number(10) not null,
    S_DRUG_COUNT number(10) not null,
    S_DRUG_EXIST_PAT_COUNT number(10) not null,
    S_DRUG_OTHER_COUNT number(10) not null,
    S_DRUG_INDICATED_COUNT number(10) not null,
    S_DRUG_NON_FORMULARY_COUNT number(10) not null,
    SFI_COUNT number(10) not null,
    UNREGISTERED_DRUG_COUNT number(10) not null,
    SFI_SAFETY_NET_COUNT number(10) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_FM_RPT_STAT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_FM_RPT_STAT_01 unique (YEAR, MONTH, PAT_HOSP_CODE, EIS_SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  FM_RPT_STAT is 'Statistics of formulary management report to analyse the prescriptions prescribed via MOE';
comment on column FM_RPT_STAT.ID is 'Formulary management report statistic id';
comment on column FM_RPT_STAT.PAT_HOSP_CODE is 'Patient hospital code';
comment on column FM_RPT_STAT.EIS_SPEC_CODE is 'EIS specialty code';
comment on column FM_RPT_STAT.YEAR is 'Year of order date';
comment on column FM_RPT_STAT.MONTH is 'Month of order date';
comment on column FM_RPT_STAT.CLUSTER_CODE is 'Hospital cluster code';
comment on column FM_RPT_STAT.FISCAL_YEAR is 'Fiscal year of order date';
comment on column FM_RPT_STAT.PAT_COUNT is 'Number of patients';
comment on column FM_RPT_STAT.PAT_SFI_COUNT is 'Number of patients prescribed SFI items';
comment on column FM_RPT_STAT.ORDER_COUNT is 'Number of prescriptions';
comment on column FM_RPT_STAT.ORDER_SFI_COUNT is 'Number of prescriptions with SFI items';
comment on column FM_RPT_STAT.ORDER_ITEM_COUNT is 'Number of items';
comment on column FM_RPT_STAT.ISSUE_ORDER_COUNT is 'Number of issued prescriptions';
comment on column FM_RPT_STAT.GENERAL_DRUG_COUNT is 'Number of general drugs';
comment on column FM_RPT_STAT.S_DRUG_COUNT is 'Number of special drug prescribed';
comment on column FM_RPT_STAT.S_DRUG_EXIST_PAT_COUNT is 'Number of special drugs with Existing Patient indication';
comment on column FM_RPT_STAT.S_DRUG_OTHER_COUNT is 'Number of special drugs with Other indication';
comment on column FM_RPT_STAT.S_DRUG_INDICATED_COUNT is 'Number of special drug with Indicated indication (exclude Non-Formulary indication)';
comment on column FM_RPT_STAT.S_DRUG_NON_FORMULARY_COUNT is 'Number of special drugs with Non-Formulary indication';
comment on column FM_RPT_STAT.SFI_COUNT is 'Number of SFI items';
comment on column FM_RPT_STAT.UNREGISTERED_DRUG_COUNT is 'Number of unregistered drugs for named patient';
comment on column FM_RPT_STAT.SFI_SAFETY_NET_COUNT is 'Number of SFI items with safety net';
comment on column FM_RPT_STAT.CREATE_USER is 'Created user';
comment on column FM_RPT_STAT.CREATE_DATE is 'Created date';
comment on column FM_RPT_STAT.UPDATE_USER is 'Last updated user';
comment on column FM_RPT_STAT.UPDATE_DATE is 'Last updated date';
comment on column FM_RPT_STAT.VERSION is 'Version to serve as optimistic lock value';


create table MANUAL_INVOICE_NUM_RANGE (
    HOST_NAME varchar2(15) not null,
    HOSP_CODE varchar2(3) not null,
    MIN_NUM number(4) not null,
    MAX_NUM number(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MANUAL_INVOICE_NUM_RANGE primary key (HOST_NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MANUAL_INVOICE_NUM_RANGE is 'Manual invoice number range allowed in each downtime workstation';
comment on column MANUAL_INVOICE_NUM_RANGE.HOST_NAME is 'Workstation name';
comment on column MANUAL_INVOICE_NUM_RANGE.HOSP_CODE is 'Hospital code';
comment on column MANUAL_INVOICE_NUM_RANGE.MIN_NUM is 'Minimum invoice number';
comment on column MANUAL_INVOICE_NUM_RANGE.MAX_NUM is 'Maximum invoice number';
comment on column MANUAL_INVOICE_NUM_RANGE.CREATE_USER is 'Created user';
comment on column MANUAL_INVOICE_NUM_RANGE.CREATE_DATE is 'Created date';
comment on column MANUAL_INVOICE_NUM_RANGE.UPDATE_USER is 'Last updated user';
comment on column MANUAL_INVOICE_NUM_RANGE.UPDATE_DATE is 'Last updated date';
comment on column MANUAL_INVOICE_NUM_RANGE.VERSION is 'Version to serve as optimistic lock value';


create table OWNERSHIP (
    ID number(19) not null,
    ORDER_NUM varchar2(12) not null,
    OWNER varchar2(12) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_OWNERSHIP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_OWNERSHIP_01 unique (ORDER_NUM) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  OWNERSHIP is 'Ownership of medication order';
comment on column OWNERSHIP.ID is 'Ownership id';
comment on column OWNERSHIP.ORDER_NUM is 'Medication order number';
comment on column OWNERSHIP.OWNER is 'Order owner (hospital code)';
comment on column OWNERSHIP.CREATE_USER is 'Created user';
comment on column OWNERSHIP.CREATE_DATE is 'Created date';
comment on column OWNERSHIP.UPDATE_USER is 'Last updated user';
comment on column OWNERSHIP.UPDATE_DATE is 'Last updated date';
comment on column OWNERSHIP.VERSION is 'Version to serve as optimistic lock value';


create table PATIENT_EPISODE (
    ID number(19) not null,
    BATCH_DATE timestamp not null,
    TRX_DATE timestamp not null,
    PAT_HOSP_CODE varchar2(3) not null,
    PAT_KEY varchar2(8) not null,
    PREV_PAT_KEY varchar2(8) not null,
    HKID varchar2(12) not null,
    PREV_HKID varchar2(12) not null,
    CASE_NUM varchar2(12) null,
    STATUS varchar2(1) not null,
    TYPE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PATIENT_EPISODE primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PATIENT_EPISODE is 'Transaction log of patient episode';
comment on column PATIENT_EPISODE.ID is 'Patient episode id';
comment on column PATIENT_EPISODE.BATCH_DATE is 'Batch date';
comment on column PATIENT_EPISODE.TRX_DATE is 'Transaction date';
comment on column PATIENT_EPISODE.PAT_HOSP_CODE is 'Patient hospital code';
comment on column PATIENT_EPISODE.PAT_KEY is 'Patient key';
comment on column PATIENT_EPISODE.PREV_PAT_KEY is 'Previous patient key';
comment on column PATIENT_EPISODE.HKID is 'HKID';
comment on column PATIENT_EPISODE.PREV_HKID is 'Previous HKID';
comment on column PATIENT_EPISODE.CASE_NUM is 'Case number';
comment on column PATIENT_EPISODE.STATUS is 'Job processed status';
comment on column PATIENT_EPISODE.TYPE is 'Type indicated how patient information changed';
comment on column PATIENT_EPISODE.CREATE_USER is 'Created user';
comment on column PATIENT_EPISODE.CREATE_DATE is 'Created date';
comment on column PATIENT_EPISODE.UPDATE_USER is 'Last updated user';
comment on column PATIENT_EPISODE.UPDATE_DATE is 'Last updated date';
comment on column PATIENT_EPISODE.VERSION is 'Version to serve as optimistic lock value';


create table SEQ_TABLE (
    SEQ_NAME varchar2(100) not null,
    CURR_NUM number(19) not null,
    MIN_NUM number(19) not null,
    MAX_NUM number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SEQ_TABLE primary key (SEQ_NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  SEQ_TABLE is 'Sequence table';
comment on column SEQ_TABLE.SEQ_NAME is 'Column used';
comment on column SEQ_TABLE.CURR_NUM is 'Current number';
comment on column SEQ_TABLE.MIN_NUM is 'Minimum number';
comment on column SEQ_TABLE.MAX_NUM is 'Maximum number';
comment on column SEQ_TABLE.CREATE_USER is 'Created user';
comment on column SEQ_TABLE.CREATE_DATE is 'Created date';
comment on column SEQ_TABLE.UPDATE_USER is 'Last updated user';
comment on column SEQ_TABLE.UPDATE_DATE is 'Last updated date';
comment on column SEQ_TABLE.VERSION is 'Version to serve as optimistic lock value';


--//@UNDO
drop table CDDH_SPECIALTY_MAPPING;
drop table CDDH_WORKSTORE_GROUP;
drop table CDDH_WORKSTORE_GROUP_HOS;
drop table HOSPITAL;
drop table HOSPITAL_CLUSTER;
drop table HOSPITAL_CLUSTER_TASK;
drop table HOSPITAL_MAPPING;
drop table WORKSTORE;
drop table WORKSTORE_GROUP;
drop table WORKSTORE_GROUP_MAPPING;
-- Corp Only
drop table FM_RPT_STAT;
drop table MANUAL_INVOICE_NUM_RANGE;
drop table OWNERSHIP;
drop table PATIENT_EPISODE;
drop table SEQ_TABLE;
--//