create index I_DISP_ORDER_T1 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, DAY_END_STATUS, UNCOLLECT_FLAG, DISP_DATE) local online;
drop index I_DISP_ORDER_12;
alter index I_DISP_ORDER_T1 rename to I_DISP_ORDER_12;

--//@UNDO
create index I_DISP_ORDER_T1 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, UNCOLLECT_FLAG, DISP_DATE) local online;
drop index I_DISP_ORDER_12;
alter index I_DISP_ORDER_T1 rename to I_DISP_ORDER_12;
--//