insert into SEQ_TABLE (SEQ_NAME, CURR_NUM, MIN_NUM, MAX_NUM) values ('ORDER_NUM', 9200000, 9200000, 9999999);
update SEQ_TABLE set MAX_NUM = 9199999 where SEQ_NAME like 'ORDER_NUM_%';


--//@UNDO
delete SEQ_TABLE where SEQ_NAME = 'ORDER_NUM';
update SEQ_TABLE set MAX_NUM = 9999999 where SEQ_NAME like 'ORDER_NUM_%';
--//
