alter table DISP_ORDER add VET_DATE timestamp;
alter table DISP_ORDER add TICKET_CREATE_DATE timestamp;
alter table DISP_ORDER add TICKET_DATE date;
alter table DISP_ORDER add TICKET_ORDER_TYPE varchar2(1);
alter table DISP_ORDER add TICKET_NUM varchar2(4);

comment on column DISP_ORDER.VET_DATE is 'Order original vet date for report retrieve';
comment on column DISP_ORDER.TICKET_CREATE_DATE is 'Ticket create date for report retrieve';
comment on column DISP_ORDER.TICKET_DATE is 'Ticket date';
comment on column DISP_ORDER.TICKET_ORDER_TYPE is 'Ticket order type';
comment on column DISP_ORDER.TICKET_NUM is 'Ticket number';

--//@UNDO
alter table DISP_ORDER drop column VET_DATE;
alter table DISP_ORDER drop column TICKET_CREATE_DATE;
alter table DISP_ORDER drop column TICKET_DATE;
alter table DISP_ORDER drop column TICKET_ORDER_TYPE;
alter table DISP_ORDER drop column TICKET_NUM;
--//