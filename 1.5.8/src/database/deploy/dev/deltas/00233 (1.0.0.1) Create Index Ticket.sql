alter table TICKET add constraint UI_TICKET_01 unique (HOSP_CODE, WORKSTORE_CODE, ORDER_TYPE, TICKET_DATE, TICKET_NUM) using index tablespace PMS_INDX_01;


--//@UNDO
alter table TICKET drop constraint UI_TICKET_01;
--//