create table PRN_ROUTE (
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    ROUTE_CODE varchar2(30) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    STATUS varchar2(1) not null,
    constraint PK_PRN_ROUTE primary key (HOSP_CODE, WORKSTORE_CODE, ROUTE_CODE) using index tablespace PMS_INDX_01,
    constraint FK_PRN_ROUTE_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE));
    
create index FK_PRN_ROUTE_01 on PRN_ROUTE (HOSP_CODE, WORKSTORE_CODE) tablespace PMS_INDX_01;

comment on table  PRN_ROUTE is 'PRN Route maintainence';
comment on column PRN_ROUTE.ROUTE_CODE is 'Route code';
comment on column PRN_ROUTE.HOSP_CODE is 'Hospital code';
comment on column PRN_ROUTE.WORKSTORE_CODE is 'Workstore code';
comment on column PRN_ROUTE.CREATE_USER is 'Created user';
comment on column PRN_ROUTE.CREATE_DATE is 'Created date';
comment on column PRN_ROUTE.UPDATE_USER is 'Last updated user';
comment on column PRN_ROUTE.UPDATE_DATE is 'Last updated date';
comment on column PRN_ROUTE.VERSION is 'Version to serve as optimistic lock value';
comment on column PRN_ROUTE.STATUS is 'Record status';


--//@UNDO
drop index FK_PRN_ROUTE_01;

drop table PRN_ROUTE;
--//