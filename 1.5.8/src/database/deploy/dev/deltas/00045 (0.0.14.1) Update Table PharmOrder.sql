alter table PHARM_ORDER add FORCE_PROCEED_FLAG number(1) default 0 null;
update PHARM_ORDER set FORCE_PROCEED_FLAG=0;
alter table PHARM_ORDER modify FORCE_PROCEED_FLAG not null;

alter table PHARM_ORDER add RECEIPT_NUM varchar2(20) null;

comment on column PHARM_ORDER.FORCE_PROCEED_FLAG is 'Force proceeded';
comment on column PHARM_ORDER.RECEIPT_NUM is 'Receipt number';


--//@UNDO
alter table PHARM_ORDER drop column FORCE_PROCEED_FLAG;

alter table PHARM_ORDER drop column RECEIPT_NUM;
--//