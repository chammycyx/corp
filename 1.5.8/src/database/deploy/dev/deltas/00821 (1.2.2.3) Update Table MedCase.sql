alter table MED_CASE add ADMISSION_DATE timestamp;
alter table MED_CASE add DISCHARGE_DATE timestamp;

comment on column MED_CASE.ADMISSION_DATE is 'Admission Date';
comment on column MED_CASE.DISCHARGE_DATE is 'Discharge Date';

--//@UNDO
alter table MED_CASE drop column ADMISSION_DATE;
alter table MED_CASE drop column DISCHARGE_DATE;
--//