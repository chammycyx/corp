alter table MED_ORDER modify SPEC_CODE null;
alter table PHARM_ORDER modify SPEC_CODE null;
alter table DISP_ORDER modify SPEC_CODE null;
alter table DISP_ORDER_ITEM modify CHARGE_SPEC_CODE null;


--//@UNDO
alter table MED_ORDER modify SPEC_CODE not null;
alter table PHARM_ORDER modify SPEC_CODE not null;
alter table DISP_ORDER modify SPEC_CODE not null;
alter table DISP_ORDER_ITEM modify CHARGE_SPEC_CODE not null;
--//