-- ASP_HOSP
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('AHN','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('CMC','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('KH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('KWH','A0001', 'WKW', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('NDH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('PMH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('PWH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('PYN','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('QMH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('QEH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('RH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('TKO','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('TWH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('TMH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('UCH','A0001', '', user, sysdate, user, sysdate, 0);
insert into ASP_HOSP (HOSP_CODE, G6PD_CODE, EXCLUDE_WORKSTORES, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION ) values ('YCH','A0001', '', user, sysdate, user, sysdate, 0);

-- ASP_ITEM
-- BG, IV Antibiotics
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CEFT01', '"Empirical therapy of neutropenic fever","Treatment of documented Pseudomonas aeruginosa infection sensitive to ceftazidim","Empirical treatment of peritonitis [Intraperitoneal Ceftazidime]","Treatment of infection by Burkholderia Pseudomallei [Ceftazidime only]","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CEFT02', '"Empirical therapy of neutropenic fever","Treatment of documented Pseudomonas aeruginosa infection sensitive to ceftazidim","Empirical treatment of peritonitis [Intraperitoneal Ceftazidime]","Treatment of infection by Burkholderia Pseudomallei [Ceftazidime only]","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CEFT03', '"Empirical therapy of neutropenic fever","Treatment of documented Pseudomonas aeruginosa infection sensitive to ceftazidim","Empirical treatment of peritonitis [Intraperitoneal Ceftazidime]","Treatment of infection by Burkholderia Pseudomallei [Ceftazidime only]","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CEFE03', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever","Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CEFE04', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever","Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('LINE13', 'Others', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('MERO01', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever in high risk patients","Treatment of documented infections attributed to ESBL-producing bacteria","Treatment of documented infections due to pathogens that are resistant to other antibiotics","Treatment of meningitis due to gram-negative organism","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('MERO02', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever in high risk patients","Treatment of documented infections attributed to ESBL-producing bacteria","Treatment of documented infections due to pathogens that are resistant to other antibiotics","Treatment of meningitis due to gram-negative organism","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PIPE08', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever","Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('SULP31', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever","Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)","Treatment of infection by Acinetobacter baumannii","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TEIC01', '"Treatment for serious infections caused by beta !V lactam resistant gram positive bacteria (e.g. MRSA, MRSE)","Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy","Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy","Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TEIC02', '"Treatment for serious infections caused by beta !V lactam resistant gram positive bacteria (e.g. MRSA, MRSE)","Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy","Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy","Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TIEN01', '"Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure","Empirical therapy of neutropenic fever in high risk patients","Treatment of documented infections attributed to ESBL-producing bacteria","Treatment of documented infections due to pathogens that are resistant to other antibiotics","Treatment of necrotizing pancreatitis","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('VANC02', '"Treatment for serious infections caused by beta !V lactam resistant gram positive bacteria (e.g. MRSA, MRSE)","Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy","Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy","For treatment of antibiotic-associated colitis in patient that has failed/is intolerant to metronidazole therapy","Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('VANC03', '"Treatment for serious infections caused by beta !V lactam resistant gram positive bacteria (e.g. MRSA, MRSE)","Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy","Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy","For treatment of antibiotic-associated colitis in patient that has failed/is intolerant to metronidazole therapy","Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA","Others"', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AUGM03', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AUGM04', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AZIT03', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CIPR02', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CIPR05', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CLAR05', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('LEVO15', '', user, sysdate, user, sysdate, 1);
insert into ASP_ITEM (ITEM_CODE, INDICATIONS, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('MOXI02', '', user, sysdate, user, sysdate, 1);

-- ASP_HOSP_ITEM
-- AHN BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

--AHN IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('AHN', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- CMC BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- CMC IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'AUGM03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'AUGM04', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('CMC', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- KH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- KH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- KWH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- KWH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'AUGM03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'AUGM04', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('KWH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- NDH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- NDH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('NDH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- QMH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

--QMH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QMH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- QEH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

--QEH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('QEH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- PMH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- PMH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'AUGM03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'AUGM04', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PMH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- PWH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- PWH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PWH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- PYN BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- PYN IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('PYN', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- RH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- RH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('RH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- TKO BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- TKO IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TKO', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- TMH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- TMH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TMH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- TWH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- TWH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('TWH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- UCH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- UCH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('UCH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

-- YCH BG
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CEFT01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CEFT02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CEFT03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CEFE03', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CEFE04', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'LINE13', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'MERO01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'MERO02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'PIPE08', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'SULP31', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'TEIC01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'TEIC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'TIEN01', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'VANC02', 'BG', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'VANC03', 'BG', 1, user, sysdate, user, sysdate, 1);

-- YCH IV
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'AUGM03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'AUGM04', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'AZIT03', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CIPR02', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CIPR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'CLAR05', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'LEVO15', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'LINE13', 'IV', 1, user, sysdate, user, sysdate, 1);
insert into ASP_HOSP_ITEM (HOSP_CODE, ITEM_CODE, ASP_TYPE, VISIBLE_FLAG, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values('YCH', 'MOXI02', 'IV', 1, user, sysdate, user, sysdate, 1);

--//@UNDO
delete from ASP_HOSP_ITEM where HOSP_CODE in ('AHN', 'CMC', 'KH', 'KWH', 'NDH', 'QMH', 'QEH', 'PMH', 'PWH', 'PYN', 'RH', 'TKO', 'TMH', 'TWH', 'UCH', 'YCH');
delete from ASP_ITEM where ITEM_CODE in ('CEFT01','CEFT02','CEFT03','CEFE03','CEFE04','LINE13','MERO01','MERO02','PIPE08','SULP31','TEIC01','TEIC02','TIEN01','VANC02','VANC03');
delete from ASP_ITEM where ITEM_CODE in ('AUGM03','AUGM04','AZIT03','CIPR02','CIPR05','CLAR05','LEVO15','MOXI02');
delete from ASP_HOSP where HOSP_CODE in ('AHN','CMC','KH','KWH','NDH','PMH','PWH','PYN','QMH','QEH','RH','TKO','TWH','TMH','UCH','YCH');
--//
