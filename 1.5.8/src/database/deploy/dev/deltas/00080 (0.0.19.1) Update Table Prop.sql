alter table PROP add DEF_VALUE varchar2(1000) null;

comment on column PROP.DEF_VALUE is 'Default value';

--//@UNDO
alter table PROP drop column DEF_VALUE;
--//