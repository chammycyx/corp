create table CORPORATE_PROP (
    ID number(19) not null,
    PROP_ID number(19) not null,
    VALUE varchar2(1000) null,
    SORT_SEQ number(10) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CORPORATE_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CORPORATE_PROP_01 unique (PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CORPORATE_PROP is 'Properties configured globally in hospital cluster';
comment on column CORPORATE_PROP.ID is 'Corporate property id';
comment on column CORPORATE_PROP.PROP_ID is 'Property id';
comment on column CORPORATE_PROP.VALUE is 'Property value';
comment on column CORPORATE_PROP.SORT_SEQ is 'Sort sequence';
comment on column CORPORATE_PROP.CREATE_USER is 'Created user';
comment on column CORPORATE_PROP.CREATE_DATE is 'Created date';
comment on column CORPORATE_PROP.UPDATE_USER is 'Last updated user';
comment on column CORPORATE_PROP.UPDATE_DATE is 'Last updated date';
comment on column CORPORATE_PROP.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_PROP (
    ID number(19) not null,
    SORT_SEQ number(10) null,
    VALUE varchar2(1000) null,
    HOSP_CODE varchar2(3) not null,
    PROP_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_HOSPITAL_PROP_01 unique (HOSP_CODE, PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_PROP is 'Properties configured by hospital level';
comment on column HOSPITAL_PROP.ID is 'Hospital property id';
comment on column HOSPITAL_PROP.SORT_SEQ is 'Sort sequence';
comment on column HOSPITAL_PROP.VALUE is 'Property value';
comment on column HOSPITAL_PROP.HOSP_CODE is 'Hospital code';
comment on column HOSPITAL_PROP.PROP_ID is 'Property id';
comment on column HOSPITAL_PROP.CREATE_USER is 'Created user';
comment on column HOSPITAL_PROP.CREATE_DATE is 'Created date';
comment on column HOSPITAL_PROP.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_PROP.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_PROP.VERSION is 'Version to serve as optimistic lock value';


create table PROP (
    ID number(19) not null,
    NAME varchar2(100) not null,
    DESCRIPTION varchar2(1000) null,
    VALIDATION varchar2(100) null,
    TYPE varchar2(2) not null,
    SUPPORT_MAINT_FLAG number(1) default 0 not null,
    CACHE_IN_SESSION_FLAG number(1) default 0 not null,
    CACHE_EXPIRE_TIME number(10) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PROP_01 unique (NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PROP is 'Full set of all corporate, hospital, workstore and workstation properties';
comment on column PROP.ID is 'Property id';
comment on column PROP.NAME is 'Property name';
comment on column PROP.DESCRIPTION is 'Description';
comment on column PROP.VALIDATION is 'Validation of property value';
comment on column PROP.SUPPORT_MAINT_FLAG is 'Support ITD maintanance';
comment on column PROP.CACHE_IN_SESSION_FLAG is 'Cache in session';
comment on column PROP.CACHE_EXPIRE_TIME is 'Cache expiry time (if not cache in session)';
comment on column PROP.TYPE is 'Property data type';
comment on column PROP.CREATE_USER is 'Created user';
comment on column PROP.CREATE_DATE is 'Created date';
comment on column PROP.UPDATE_USER is 'Last updated user';
comment on column PROP.UPDATE_DATE is 'Last updated date';
comment on column PROP.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE_PROP (
    ID number(19) not null,
    PROP_ID number(19) not null,
    VALUE varchar2(1000) null,
    SORT_SEQ number(10) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WORKSTORE_PROP_01 unique (HOSP_CODE, WORKSTORE_CODE, PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE_PROP is 'Properties configured by workstore level';
comment on column WORKSTORE_PROP.ID is 'Workstore property id';
comment on column WORKSTORE_PROP.PROP_ID is 'Property id';
comment on column WORKSTORE_PROP.VALUE is 'Property value';
comment on column WORKSTORE_PROP.SORT_SEQ is 'Sort sequence';
comment on column WORKSTORE_PROP.HOSP_CODE is 'Hospital code';
comment on column WORKSTORE_PROP.WORKSTORE_CODE is 'Workstore code';
comment on column WORKSTORE_PROP.CREATE_USER is 'Created user';
comment on column WORKSTORE_PROP.CREATE_DATE is 'Created date';
comment on column WORKSTORE_PROP.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE_PROP.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE_PROP.VERSION is 'Version to serve as optimistic lock value';


--//@UNDO
drop table CORPORATE_PROP;
drop table HOSPITAL_PROP;
drop table PROP;
drop table WORKSTORE_PROP;
--//