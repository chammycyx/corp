alter table MED_ORDER modify CMS_UPDATE_DATE null;
alter table MED_ORDER add DH_PATIENT_XML clob;
alter table MED_ORDER add DH_LATEST_FLAG number(1,0) null;
alter table MED_ORDER modify PRESC_TYPE VARCHAR(2);
alter table MED_ORDER modify PREV_ORDER_NUM VARCHAR(36);

comment on column MED_ORDER.DH_PATIENT_XML is 'Patient XML in DH order';
comment on column MED_ORDER.DH_LATEST_FLAG is 'DH order latest flag';

--//@UNDO
alter table MED_ORDER set unused column DH_LATEST_FLAG;
alter table MED_ORDER set unused column DH_PATIENT_XML;
alter table MED_ORDER modify CMS_UPDATE_DATE not null enable novalidate;
--//
