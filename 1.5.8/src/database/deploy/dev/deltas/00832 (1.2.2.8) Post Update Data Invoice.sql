update INVOICE i
set (i.HOSP_CODE, i.WORKSTORE_CODE, i.HKID, i.INVOICE_DATE, i.ORDER_TYPE) = 
(select d.HOSP_CODE, d.WORKSTORE_CODE, p.HKID, d.DISP_DATE, d.ORDER_TYPE 
from DISP_ORDER d join PHARM_ORDER p on (d.PHARM_ORDER_ID = p.ID) where d.ID = i.DISP_ORDER_ID)
where (i.INVOICE_DATE is null or i.ORDER_TYPE is null)
and i.CREATE_DATE between to_date('20120101', 'yyyymmdd') and to_date('20160101', 'yyyymmdd');

alter table INVOICE modify HOSP_CODE not null;
alter table INVOICE modify WORKSTORE_CODE not null;
alter table INVOICE modify INVOICE_DATE not null;
alter table INVOICE modify ORDER_TYPE not null;

--//@UNDO
alter table INVOICE modify HOSP_CODE null;
alter table INVOICE modify WORKSTORE_CODE null;
alter table INVOICE modify INVOICE_DATE null;
alter table INVOICE modify ORDER_TYPE null;
--//