insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (14003,'charging.sfi.invoice.invoiceNum.prefix','Prefix of SFI invoice number','I','S','pmsadmin',sysdate,'pmsadmin',sysdate,0,0,60,null,'S',null,0);

insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100040,14003,'I','VH', 14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100041,14003,'I','QES',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100042,14003,'I','QMH',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100043,14003,'I','TMH',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100044,14003,'I','LKK',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100045,14003,'I','QEH',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100046,14003,'I','UCH',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (100047,14003,'I','ALC',14003,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

update PROP set CACHE_EXPIRE_TIME=0 where ID in (14004,14005);

update PROP set DESCRIPTION='Mask the number of characters of user ID on dispensing label' where ID=20007;
update PROP set DESCRIPTION='Show dispensing label with user ID' where ID=20008;

--//@UNDO
delete from HOSPITAL_PROP where PROP_ID=14003;
delete from PROP where ID=14003;

update PROP set CACHE_EXPIRE_TIME=60 where ID in (14004,14005);

update PROP set DESCRIPTION='Display dispensing label with the number of characters of user ID masked' where ID=20007;
update PROP set DESCRIPTION='Display dispensing label with user ID' where ID=20008;
