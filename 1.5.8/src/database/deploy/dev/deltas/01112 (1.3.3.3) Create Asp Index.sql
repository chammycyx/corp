create index FK_ASP_HOSP_ITEM_01 on ASP_HOSP_ITEM (HOSP_CODE) tablespace PMS_INDX_01;
create index FK_ASP_HOSP_ITEM_02 on ASP_HOSP_ITEM (ITEM_CODE) tablespace PMS_INDX_01;
create index FK_ASP_HOSP_SPEC_01 on ASP_HOSP_SPEC (HOSP_CODE) tablespace PMS_INDX_01;

--//@UNDO
drop index FK_ASP_HOSP_SPEC_01;
drop index FK_ASP_HOSP_ITEM_02;
drop index FK_ASP_HOSP_ITEM_01;
--//
