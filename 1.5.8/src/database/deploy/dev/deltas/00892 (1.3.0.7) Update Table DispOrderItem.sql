alter table DISP_ORDER_ITEM add TPN_REQUEST_ID number(19) null;
comment on column DISP_ORDER_ITEM.TPN_REQUEST_ID is 'Tpn request id';

--//@UNDO
alter table DISP_ORDER_ITEM set unused column TPN_REQUEST_ID;
--//