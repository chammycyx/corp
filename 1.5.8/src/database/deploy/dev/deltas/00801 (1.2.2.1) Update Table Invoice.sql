alter table INVOICE add HOSP_CODE varchar2(3);
alter table INVOICE add WORKSTORE_CODE varchar2(4);
alter table INVOICE add ORDER_TYPE varchar2(1);
alter table INVOICE add INVOICE_DATE date;
alter table INVOICE add HKID varchar2(12);
alter table INVOICE add CHARGE_ORDER_NUM varchar2(12) null; 

comment on column INVOICE.HOSP_CODE is 'Hospital code';
comment on column INVOICE.WORKSTORE_CODE is 'Workstore code';    
comment on column INVOICE.ORDER_TYPE is 'Order type';
comment on column INVOICE.INVOICE_DATE is 'Invoice date';
comment on column INVOICE.HKID is 'HKID number (same as PHARM_ORDER.HKID)';
comment on column INVOICE.CHARGE_ORDER_NUM is 'Order Num for IPSFI Invoice';

alter table INVOICE add constraint FK_INVOICE_02 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

create index FK_INVOICE_02 on INVOICE (HOSP_CODE, WORKSTORE_CODE) local online;

--//@UNDO
drop index FK_INVOICE_02;

alter table INVOICE drop constraint FK_INVOICE_02;

alter table INVOICE drop column HOSP_CODE;
alter table INVOICE drop column WORKSTORE_CODE;
alter table INVOICE drop column ORDER_TYPE;
alter table INVOICE drop column INVOICE_DATE;
alter table INVOICE drop column HKID;
alter table INVOICE drop column CHARGE_ORDER_NUM;
--//