alter table DISP_ORDER rename column TICKET_ORDER_TYPE to ORDER_TYPE;

comment on column DISP_ORDER.ORDER_TYPE is 'Order Type';


--//@UNDO
alter table DISP_ORDER rename column ORDER_TYPE to TICKET_ORDER_TYPE;

comment on column DISP_ORDER.TICKET_ORDER_TYPE is 'Ticket order type';
--//