alter table MED_ORDER_ITEM add ALERT_DESC varchar2(500) null;
alter table MED_ORDER_ITEM add ALERT_REMARK varchar2(500) null;

comment on column MED_ORDER_ITEM.ALERT_DESC is 'Alert description';
comment on column MED_ORDER_ITEM.ALERT_REMARK is 'Alert remark';

--//@UNDO
alter table MED_ORDER_ITEM drop column ALERT_DESC;
alter table MED_ORDER_ITEM drop column ALERT_REMARK;
--//