alter table MED_ORDER modify PRE_VET_USER_NAME varchar2(150);
alter table MED_ORDER modify WITHHOLD_USER_NAME varchar2(150);

--//@UNDO
alter table MED_ORDER modify PRE_VET_USER_NAME varchar2(48);
alter table MED_ORDER modify WITHHOLD_USER_NAME varchar2(48);
--//