alter table PATIENT add OFFICE_PHONE varchar2(10) null;
alter table PATIENT add OFFICE_PHONE_EXT varchar2(4) null;
alter table PATIENT add OTHER_PHONE varchar2(10) null;
alter table PATIENT add OTHER_PHONE_EXT varchar2(4) null;
comment on column PATIENT.OFFICE_PHONE is 'Office phone number';
comment on column PATIENT.OFFICE_PHONE_EXT is 'Office phone extension';
comment on column PATIENT.OTHER_PHONE is 'Other phone number';
comment on column PATIENT.OTHER_PHONE_EXT is 'Other phone extension';

alter table PHARM_ORDER add DRS_FLAG number(1) null;
alter table PHARM_ORDER add OFFICE_PHONE varchar2(10) null;
alter table PHARM_ORDER add OFFICE_PHONE_EXT varchar2(4) null;
alter table PHARM_ORDER add OTHER_PHONE varchar2(10) null;
alter table PHARM_ORDER add OTHER_PHONE_EXT varchar2(4) null;
comment on column PHARM_ORDER.DRS_FLAG is 'Boolean flag indicates order under drug refill service';
comment on column PHARM_ORDER.OFFICE_PHONE is 'Office phone number';
comment on column PHARM_ORDER.OFFICE_PHONE_EXT is 'Office phone extension';
comment on column PHARM_ORDER.OTHER_PHONE is 'Other phone number';
comment on column PHARM_ORDER.OTHER_PHONE_EXT is 'Other phone extension';

--//@UNDO
alter table PHARM_ORDER set unused column OTHER_PHONE_EXT;
alter table PHARM_ORDER set unused column OTHER_PHONE;
alter table PHARM_ORDER set unused column OFFICE_PHONE_EXT;
alter table PHARM_ORDER set unused column OFFICE_PHONE;
alter table PHARM_ORDER set unused column DRS_FLAG;

alter table PATIENT set unused column OTHER_PHONE_EXT;
alter table PATIENT set unused column OTHER_PHONE;
alter table PATIENT set unused column OFFICE_PHONE_EXT;
alter table PATIENT set unused column OFFICE_PHONE;
--//