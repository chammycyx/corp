alter table ASP_HOSP modify (HOSP_CODE varchar2(3),EXCLUDE_WORKSTORES varchar2(100));

alter table ASP_HOSP_ITEM add constraint FK_ASP_HOSP_ITEM_01 foreign key (HOSP_CODE) references ASP_HOSP (HOSP_CODE);
alter table ASP_HOSP_ITEM add constraint FK_ASP_HOSP_ITEM_02 foreign key (ITEM_CODE) references ASP_ITEM (ITEM_CODE);
alter table ASP_HOSP_SPEC add constraint FK_ASP_HOSP_SPEC_01 foreign key (HOSP_CODE) references ASP_HOSP (HOSP_CODE);

--//@UNDO
alter table ASP_HOSP_ITEM drop constraint FK_ASP_HOSP_ITEM_01;
alter table ASP_HOSP_ITEM drop constraint FK_ASP_HOSP_ITEM_02;
alter table ASP_HOSP_SPEC drop constraint FK_ASP_HOSP_SPEC_01;
--//
