create table PATIENT_PROFILE
(
    PATIENT_ID number(19) not null,
	CDDH_XML CLOB,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_PATIENT_PROFILE primary key (PATIENT_ID) using index tablespace PMS_INDX_01
) partition by hash(PATIENT_ID) (
	partition P_PMS_PATID_DATA_01 tablespace P_PMS_PATID_DATA_01,
	partition P_PMS_PATID_DATA_02 tablespace P_PMS_PATID_DATA_02
);

--comment
comment on table PATIENT_PROFILE is 'Patient profile';
comment on column PATIENT_PROFILE.PATIENT_ID is 'Patient id';
comment on column PATIENT_PROFILE.CDDH_XML is 'Cddh xml';
comment on column PATIENT_PROFILE.CREATE_USER is 'Created user';
comment on column PATIENT_PROFILE.CREATE_DATE is 'Created date';
comment on column PATIENT_PROFILE.UPDATE_USER is 'Last updated user';
comment on column PATIENT_PROFILE.UPDATE_DATE is 'Last updated date';
comment on column PATIENT_PROFILE.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table PATIENT_PROFILE;
--//