alter table INVOICE_ITEM add CHARGE_QTY number(19,4) null;

comment on column INVOICE_ITEM.CHARGE_QTY is 'Charge quantity (private patient)';

--//@UNDO
alter table INVOICE_ITEM drop column CHARGE_QTY;
--//