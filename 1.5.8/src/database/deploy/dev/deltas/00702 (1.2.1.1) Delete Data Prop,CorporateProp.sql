delete from CORPORATE_PROP where PROP_ID = 27006;
delete from PROP where id = 27006;

--//@UNDO
insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27006,'report.fm.legacy.persistence.enabled','Enable data persistence in FM Report legacy database','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (27006,27006,'N',Null,'pmsadmin',sysdate,'pmsadmin',sysdate,1);