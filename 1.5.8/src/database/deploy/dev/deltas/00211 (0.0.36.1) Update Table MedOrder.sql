alter table MED_ORDER add ITEM_REMARK_CONFIRMED_FLAG varchar2(1);
comment on column MED_ORDER.ITEM_REMARK_CONFIRMED_FLAG is 'Remark has been confirmed in item level';
update MED_ORDER set ITEM_REMARK_CONFIRMED_FLAG = 1 where REMARK_STATUS = 'C';
update MED_ORDER set ITEM_REMARK_CONFIRMED_FLAG = 0 where REMARK_STATUS <> 'C';
alter table MED_ORDER modify ITEM_REMARK_CONFIRMED_FLAG not null;

--//@UNDO
alter table MED_ORDER drop column ITEM_REMARK_CONFIRMED_FLAG;
--//
