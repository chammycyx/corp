alter table DISP_ORDER modify TICKET_CREATE_DATE not null;
alter table DISP_ORDER modify TICKET_DATE not null;
alter table DISP_ORDER modify ORDER_TYPE not null;
alter table DISP_ORDER modify TICKET_NUM not null;
alter table DISP_ORDER modify TICKET_ID not null;

alter table DISP_ORDER modify PATIENT_ID not null;

--//@UNDO
alter table DISP_ORDER modify TICKET_CREATE_DATE null;
alter table DISP_ORDER modify TICKET_DATE null;
alter table DISP_ORDER modify ORDER_TYPE null;
alter table DISP_ORDER modify TICKET_NUM null;
alter table DISP_ORDER modify TICKET_ID null;

alter table DISP_ORDER modify PATIENT_ID null;
--//