insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (40001,'order.allowUpdate.duration','[consult CD3 for change] Duration (in days) of orders allowed to update','60,90','dd',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (40001,40001,'60',40001,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
delete from CORPORATE_PROP where PROP_ID in (40001);

delete from PROP where ID in (40001);

--//