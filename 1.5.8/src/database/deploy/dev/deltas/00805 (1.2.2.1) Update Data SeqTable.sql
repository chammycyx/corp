insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) 
values ('CHARGE_ORDER_NUM',820000000,820000000,899999999,sysdate);

insert all into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) 
values ('CHARGE_' || SEQ_NAME ,800000000,800000000,819999999,sysdate)
(select SEQ_NAME from SEQ_TABLE where SEQ_NAME like 'ORDER_NUM_%');

--//@UNDO
delete SEQ_TABLE where SEQ_NAME='CHARGE_ORDER_NUM';

delete SEQ_TABLE where SEQ_NAME like 'CHARGE_ORDER_NUM_%';
--//
