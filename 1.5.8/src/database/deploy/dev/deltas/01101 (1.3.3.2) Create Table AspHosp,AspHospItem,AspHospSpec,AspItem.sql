create table ASP_HOSP
(
	HOSP_CODE varchar2(6) not null,
	G6PD_CODE varchar2(8),
	EXCLUDE_WORKSTORES varchar2(19),
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_ASP_HOSP primary key (HOSP_CODE) using index tablespace PMS_INDX_01
)
tablespace PMS_DATA_01;

comment on column ASP_HOSP.HOSP_CODE is 'Hospital code';
comment on column ASP_HOSP.G6PD_CODE is 'G6PD alert code';
comment on column ASP_HOSP.EXCLUDE_WORKSTORES is 'Excluded workstores list';
comment on column ASP_HOSP.CREATE_USER is 'Created user';
comment on column ASP_HOSP.CREATE_DATE is 'Created date';
comment on column ASP_HOSP.UPDATE_USER is 'Last updated user';
comment on column ASP_HOSP.UPDATE_DATE is 'Last updated date';
comment on column ASP_HOSP.VERSION is 'Version to serve as optimistic lock value';

create table ASP_ITEM
(
	ITEM_CODE varchar2(6) not null,
	INDICATIONS varchar2(2000),
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_ASP_ITEM primary key (ITEM_CODE) using index tablespace PMS_INDX_01
)
tablespace PMS_DATA_01;

comment on column ASP_ITEM.ITEM_CODE is 'Item code';
comment on column ASP_ITEM.INDICATIONS is 'Indications';
comment on column ASP_ITEM.CREATE_USER is 'Created user';
comment on column ASP_ITEM.CREATE_DATE is 'Created date';
comment on column ASP_ITEM.UPDATE_USER is 'Last updated user';
comment on column ASP_ITEM.UPDATE_DATE is 'Last updated date';
comment on column ASP_ITEM.VERSION is 'Version to serve as optimistic lock value';

create table ASP_HOSP_ITEM
(
	HOSP_CODE varchar2(3) not null,
	ITEM_CODE varchar2(6) not null,
	ASP_TYPE varchar2(2) not null,
	VISIBLE_FLAG number(1) default 1 not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_ASP_HOSP_ITEM primary key (HOSP_CODE, ITEM_CODE, ASP_TYPE) using index tablespace PMS_INDX_01
)
tablespace PMS_DATA_01;

comment on column ASP_HOSP_ITEM.HOSP_CODE is 'Hospital code';
comment on column ASP_HOSP_ITEM.ITEM_CODE is 'Item code';
comment on column ASP_HOSP_ITEM.ASP_TYPE is 'ASP Type (BG,IV)';
comment on column ASP_HOSP_ITEM.CREATE_USER is 'Created user';
comment on column ASP_HOSP_ITEM.CREATE_DATE is 'Created date';
comment on column ASP_HOSP_ITEM.UPDATE_USER is 'Last updated user';
comment on column ASP_HOSP_ITEM.UPDATE_DATE is 'Last updated date';
comment on column ASP_HOSP_ITEM.VERSION is 'Version to serve as optimistic lock value';

create table ASP_HOSP_SPEC
(
	HOSP_CODE varchar2(3) not null,
	SPEC_CODE varchar2(4) not null,
	VISIBLE_FLAG number(1) default 1 not null,
	ASP_TYPE varchar2(2) not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_ASP_HOSP_SPEC primary key (HOSP_CODE, SPEC_CODE, ASP_TYPE) using index tablespace PMS_INDX_01
)
tablespace PMS_DATA_01;

comment on column ASP_HOSP_SPEC.HOSP_CODE is 'Hospital code';
comment on column ASP_HOSP_SPEC.SPEC_CODE is 'Pas Specialty code';
comment on column ASP_HOSP_SPEC.VISIBLE_FLAG is 'Flag for specialty filtering';
comment on column ASP_HOSP_SPEC.ASP_TYPE is 'ASP Type (BG,IV)';
comment on column ASP_HOSP_SPEC.CREATE_USER is 'Created user';
comment on column ASP_HOSP_SPEC.CREATE_DATE is 'Created date';
comment on column ASP_HOSP_SPEC.UPDATE_USER is 'Last updated user';
comment on column ASP_HOSP_SPEC.UPDATE_DATE is 'Last updated date';
comment on column ASP_HOSP_SPEC.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table ASP_HOSP_SPEC;
drop table ASP_HOSP_ITEM;
drop table ASP_ITEM;
drop table ASP_HOSP;
--//
