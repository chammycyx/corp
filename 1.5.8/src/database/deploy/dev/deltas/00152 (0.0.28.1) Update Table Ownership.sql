alter table OWNERSHIP drop column VERSION;

--//@UNDO
alter table OWNERSHIP add VERSION number(19) null;
update OWNERSHIP set VERSION = 1;
alter table OWNERSHIP modify VERSION not null;
--//