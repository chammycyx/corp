create index I_MED_ORDER_06 on MED_ORDER (HOSP_CODE, WORKSTORE_CODE, ORDER_TYPE, ORDER_DATE) local online;

--//@UNDO
drop index I_MED_ORDER_06;
--//