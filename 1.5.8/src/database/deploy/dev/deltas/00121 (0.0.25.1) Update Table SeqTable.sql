alter table SEQ_TABLE drop column CREATE_USER;
alter table SEQ_TABLE drop column CREATE_DATE;
alter table SEQ_TABLE drop column UPDATE_USER;
alter table SEQ_TABLE drop column UPDATE_DATE;
alter table SEQ_TABLE drop column VERSION;

--//@UNDO
alter table SEQ_TABLE add CREATE_USER varchar2(12) null;
alter table SEQ_TABLE add CREATE_DATE timestamp null;
alter table SEQ_TABLE add UPDATE_USER varchar2(12) null;
alter table SEQ_TABLE add UPDATE_DATE timestamp null;
alter table SEQ_TABLE add VERSION number(19) null;

update SEQ_TABLE set CREATE_USER='pmsadmin',CREATE_DATE=sysdate, UPDATE_USER='pmsadmin',UPDATE_DATE=sysdate,VERSION=1;

alter table SEQ_TABLE modify CREATE_USER not null;
alter table SEQ_TABLE modify CREATE_DATE not null;
alter table SEQ_TABLE modify UPDATE_USER not null;
alter table SEQ_TABLE modify UPDATE_DATE not null;
alter table SEQ_TABLE modify VERSION not null;
--//