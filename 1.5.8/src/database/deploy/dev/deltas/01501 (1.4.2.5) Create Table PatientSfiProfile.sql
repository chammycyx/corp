create table PATIENT_SFI_PROFILE 
(
	HKID varchar2(12) not null,
	SFI_XML CLOB,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_PATIENT_SFI_PROFILE primary key (HKID) using index tablespace PMS_INDX_01
) partition by hash(HKID) (
	partition P_PMS_HKID_DATA_01 tablespace P_PMS_HKID_DATA_01,
	partition P_PMS_HKID_DATA_02 tablespace P_PMS_HKID_DATA_02
);

--comment
comment on table PATIENT_SFI_PROFILE is 'Patient sfi profile';
comment on column PATIENT_SFI_PROFILE.HKID is 'HKID';
comment on column PATIENT_SFI_PROFILE.SFI_XML is 'Sfi xml';
comment on column PATIENT_SFI_PROFILE.CREATE_USER is 'Created user';
comment on column PATIENT_SFI_PROFILE.CREATE_DATE is 'Created date';
comment on column PATIENT_SFI_PROFILE.UPDATE_USER is 'Last updated user';
comment on column PATIENT_SFI_PROFILE.UPDATE_DATE is 'Last updated date';
comment on column PATIENT_SFI_PROFILE.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table PATIENT_SFI_PROFILE;
--//