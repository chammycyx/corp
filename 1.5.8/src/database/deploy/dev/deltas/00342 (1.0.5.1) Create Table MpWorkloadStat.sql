create table MP_WORKLOAD_STAT (
    ID number(19) not null,
    DAY_END_BATCH_DATE date not null,
    ITEM_COUNT number(10) not null,
    SFI_ITEM_COUNT number(10) not null,
    SAFETY_NET_ITEM_COUNT number(10) not null,
    WARD_STOCK_ITEM_COUNT number(10) not null,
    DANGER_DRUG_ITEM_COUNT number(10) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MP_WORKLOAD_STAT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_MP_WORKLOAD_STAT_01 unique (HOSP_CODE, WORKSTORE_CODE, DAY_END_BATCH_DATE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MP_WORKLOAD_STAT is 'Consolidated in-patient workload statistics';
comment on column MP_WORKLOAD_STAT.id is 'In-patient workload statistic id';
comment on column MP_WORKLOAD_STAT.DAY_END_BATCH_DATE is 'Batch date of day end';
comment on column MP_WORKLOAD_STAT.ITEM_COUNT is 'Count of in-patient dispensing order items';
comment on column MP_WORKLOAD_STAT.SFI_ITEM_COUNT is 'Count of in-patient dispensing order SFI items';
comment on column MP_WORKLOAD_STAT.SAFETY_NET_ITEM_COUNT is 'Count of in-patient dispensing order safety net items';
comment on column MP_WORKLOAD_STAT.WARD_STOCK_ITEM_COUNT is 'Count of in-patient dispensing order ward stock items';
comment on column MP_WORKLOAD_STAT.DANGER_DRUG_ITEM_COUNT is 'Count of in-patient dispensing order danger drug items';
comment on column MP_WORKLOAD_STAT.HOSP_CODE is 'Hospital code';
comment on column MP_WORKLOAD_STAT.WORKSTORE_CODE is 'Workstore code';
comment on column MP_WORKLOAD_STAT.CREATE_USER is 'Created user';
comment on column MP_WORKLOAD_STAT.CREATE_DATE is 'Created date';
comment on column MP_WORKLOAD_STAT.UPDATE_USER is 'Last updated user';
comment on column MP_WORKLOAD_STAT.UPDATE_DATE is 'Last updated date';
comment on column MP_WORKLOAD_STAT.version is 'Version to serve as optimistic lock value';

alter table MP_WORKLOAD_STAT add constraint FK_MP_WORKLOAD_STAT_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);
    
create sequence SQ_MP_WORKLOAD_STAT increment by 50 start with 10000000000049;

create index FK_MP_WORKLOAD_STAT_01 on MP_WORKLOAD_STAT (HOSP_CODE, WORKSTORE_CODE) tablespace PMS_INDX_01;


--//@UNDO
drop index FK_MP_WORKLOAD_STAT_01;

drop sequence SQ_MP_WORKLOAD_STAT;

alter table MP_WORKLOAD_STAT drop constraint FK_MP_WORKLOAD_STAT_01;

drop table MP_WORKLOAD_STAT;
--//