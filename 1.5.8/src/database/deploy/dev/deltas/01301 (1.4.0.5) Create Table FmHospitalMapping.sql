create table FM_HOSPITAL_MAPPING
(
	PAT_HOSP_CODE varchar2(3) not null,
	FROM_CLUSTER_CODE varchar2(3) not null,
	TO_CLUSTER_CODE varchar2(3) not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_FM_HOSPITAL_MAPPING primary key (PAT_HOSP_CODE) using index tablespace PMS_INDX_01
);

--comment
comment on table FM_HOSPITAL_MAPPING is 'Fm hospital mapping';
comment on column FM_HOSPITAL_MAPPING.PAT_HOSP_CODE is 'Patient hospital code';
comment on column FM_HOSPITAL_MAPPING.FROM_CLUSTER_CODE is 'From cluster code';
comment on column FM_HOSPITAL_MAPPING.TO_CLUSTER_CODE is 'To cluster code';
comment on column FM_HOSPITAL_MAPPING.CREATE_USER is 'Created user';
comment on column FM_HOSPITAL_MAPPING.CREATE_DATE is 'Created date';
comment on column FM_HOSPITAL_MAPPING.UPDATE_USER is 'Last updated user';
comment on column FM_HOSPITAL_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column FM_HOSPITAL_MAPPING.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table FM_HOSPITAL_MAPPING;
--//