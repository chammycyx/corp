insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (10017,'alert.hla.dhOrder.override.reason','Overriding reason of HLA-B*1502 test alert for DH Rx','1-200','S',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (10017,10017,'HLA-B*1502 alert is overridden.',10017,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
delete from CORPORATE_PROP where PROP_ID in (10017);

delete from PROP where ID in (10017);

--//