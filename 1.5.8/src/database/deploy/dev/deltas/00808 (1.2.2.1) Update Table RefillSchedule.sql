alter table REFILL_SCHEDULE add USE_MO_ITEM_NUM_FLAG number(1) default 0 not null;
alter table REFILL_SCHEDULE add MO_ORG_ITEM_NUM number(10);

comment on column REFILL_SCHEDULE.USE_MO_ITEM_NUM_FLAG is 'Determine to use MO Item Num as Group Num';
comment on column REFILL_SCHEDULE.MO_ORG_ITEM_NUM is 'MO original item number';

--//@UNDO
alter table REFILL_SCHEDULE drop column USE_MO_ITEM_NUM_FLAG;
alter table REFILL_SCHEDULE drop column MO_ORG_ITEM_NUM;
--//