insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (13005,'cddh.trxDate.min','The earliest transaction date for CDDH enquiry','yyyyMMdd.HHmmssSSS','DT',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (13005,13005,'19991201.000000000',13005,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
delete from CORPORATE_PROP where PROP_ID=13005;
delete from PROP where ID=13005;
--//
