create table DAY_END_RPT_STAT (
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    BATCH_DATE date not null,
    ORDER_COUNT number(10) not null,
    ITEM_COUNT number(10) not null,
    SAMPLE_ITEM_COUNT number(10) not null, 
    SFI_ITEM_COUNT number(10) not null, 
    SAFETY_NET_ITEM_COUNT number(10) not null, 
    CAPD_VOUCHER_COUNT number(10) not null, 
    CAPD_VOUCHER_ITEM_COUNT number(10) not null, 
    MP_ITEM_COUNT number(10) not null, 
    MP_SAMPLE_ITEM_COUNT number(10) not null, 
    MP_SFI_ITEM_COUNT number(10) not null, 
    MP_SAFETY_NET_ITEM_COUNT number(10) not null, 
    MP_WARD_STOCK_ITEM_COUNT number(10) not null, 
    MP_DANGER_DRUG_ITEM_COUNT number(10) not null, 
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_DAY_END_RPT_STAT primary key (HOSP_CODE, WORKSTORE_CODE, BATCH_DATE) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (maxvalue) tablespace P_PMS_DISP_DATA_2012);

comment on table  DAY_END_RPT_STAT is 'Day End Report Stat';
comment on column DAY_END_RPT_STAT.HOSP_CODE is 'Hospital code';
comment on column DAY_END_RPT_STAT.WORKSTORE_CODE is 'Workstore code';
comment on column DAY_END_RPT_STAT.BATCH_DATE is 'Batch date';
comment on column DAY_END_RPT_STAT.ORDER_COUNT is 'Order count';
comment on column DAY_END_RPT_STAT.ITEM_COUNT is 'Item count';
comment on column DAY_END_RPT_STAT.SAMPLE_ITEM_COUNT is 'Sample item count';
comment on column DAY_END_RPT_STAT.SFI_ITEM_COUNT is 'SFI item count';
comment on column DAY_END_RPT_STAT.SAFETY_NET_ITEM_COUNT is 'Safety net item count';
comment on column DAY_END_RPT_STAT.CAPD_VOUCHER_COUNT is 'CAPD voucher count';
comment on column DAY_END_RPT_STAT.CAPD_VOUCHER_ITEM_COUNT is 'CAPD voucher item count';
comment on column DAY_END_RPT_STAT.MP_ITEM_COUNT is 'IP item count';
comment on column DAY_END_RPT_STAT.MP_SAMPLE_ITEM_COUNT is 'IP sample item count';
comment on column DAY_END_RPT_STAT.MP_SFI_ITEM_COUNT is 'IP SFI item count';
comment on column DAY_END_RPT_STAT.MP_SAFETY_NET_ITEM_COUNT is 'IP safety net item';
comment on column DAY_END_RPT_STAT.MP_WARD_STOCK_ITEM_COUNT is 'IP ward stock item';
comment on column DAY_END_RPT_STAT.MP_DANGER_DRUG_ITEM_COUNT is 'IP danger drug item';
comment on column DAY_END_RPT_STAT.CREATE_USER is 'Created user';
comment on column DAY_END_RPT_STAT.CREATE_DATE is 'Created date';
comment on column DAY_END_RPT_STAT.UPDATE_USER is 'Last updated user';
comment on column DAY_END_RPT_STAT.UPDATE_DATE is 'Last updated date';


--//@UNDO
drop table DAY_END_RPT_STAT;
--//
