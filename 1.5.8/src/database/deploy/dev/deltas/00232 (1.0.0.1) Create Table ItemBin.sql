create table ITEM_BIN (
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    ITEM_CODE varchar2(6) not null,
    BIN_NUM varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_ITEM_BIN primary key (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE, BIN_NUM) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  ITEM_BIN is 'For PMS DT label item bin number';
comment on column ITEM_BIN.HOSP_CODE is 'Hospital code';
comment on column ITEM_BIN.WORKSTORE_CODE is 'Workstore code';
comment on column ITEM_BIN.ITEM_CODE is 'Item code';
comment on column ITEM_BIN.BIN_NUM is 'Bin number';
comment on column ITEM_BIN.CREATE_USER is 'Created user';
comment on column ITEM_BIN.CREATE_DATE is 'Created date';
comment on column ITEM_BIN.UPDATE_USER is 'Last updated user';
comment on column ITEM_BIN.UPDATE_DATE is 'Last updated date';
comment on column ITEM_BIN.VERSION is 'Version to serve as optimistic lock value';

alter table ITEM_BIN add constraint FK_ITEM_BIN_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

create index FK_ITEM_BIN_01 on ITEM_BIN (HOSP_CODE, WORKSTORE_CODE) tablespace PMS_INDX_01;


--//@UNDO
drop index FK_ITEM_BIN_01;

alter table ITEM_BIN drop constraint FK_ITEM_BIN_01;

drop table ITEM_BIN;
--//