alter table MANUAL_INVOICE_NUM_RANGE drop constraint PK_MANUAL_INVOICE_NUM_RANGE;
alter table MANUAL_INVOICE_NUM_RANGE add constraint PK_MANUAL_INVOICE_NUM_RANGE primary key (HOSP_CODE, HOST_NAME) using index tablespace PMS_INDX_01;

--//@UNDO
alter table MANUAL_INVOICE_NUM_RANGE drop constraint PK_MANUAL_INVOICE_NUM_RANGE;
alter table MANUAL_INVOICE_NUM_RANGE add constraint PK_MANUAL_INVOICE_NUM_RANGE primary key (HOST_NAME) using index tablespace PMS_INDX_01;
--//