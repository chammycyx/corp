create index I_INVOICE_01 on INVOICE (INVOICE_NUM) local online;
create index I_INVOICE_02 on INVOICE (HOSP_CODE, ORDER_TYPE, DOC_TYPE, INVOICE_DATE) local online;
create index I_INVOICE_03 on INVOICE (HOSP_CODE, ORDER_TYPE, DOC_TYPE, HKID) local online;

--//@UNDO
drop index I_INVOICE_01;
drop index I_INVOICE_02;
drop index I_INVOICE_03;
--//
