alter table CAPD_VOUCHER_ITEM add ITEM_NUM number(10);
comment on column CAPD_VOUCHER_ITEM.ITEM_NUM is 'Item number';

--//@UNDO
alter table CAPD_VOUCHER_ITEM set unused column ITEM_NUM;
--//