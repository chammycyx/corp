update DISP_ORDER do
set do.PATIENT_ID = 
(select mo.PATIENT_ID
from MED_ORDER mo, PHARM_ORDER po
where mo.ID = po.MED_ORDER_ID and po.ID = do.PHARM_ORDER_ID) 
where do.PATIENT_ID is null
and do.CREATE_DATE between to_date('20120101', 'yyyymmdd') and to_date('20160101', 'yyyymmdd');


--//@UNDO
--//