create index I_PATIENT_01 on PATIENT (PAT_KEY) tablespace PMS_INDX_01;
create index I_PATIENT_02 on PATIENT (HKID) tablespace PMS_INDX_01;

create index I_MED_ORDER_01 on MED_ORDER (ORDER_NUM) local;
create index I_MED_ORDER_02 on MED_ORDER (ORDER_TYPE, ORDER_DATE) local;
create index I_MED_ORDER_03 on MED_ORDER (PREV_PATIENT_ID) local;
create index I_MED_ORDER_04 on MED_ORDER (ORG_PATIENT_ID) local;

create index I_PHARM_ORDER_02 on PHARM_ORDER (NAME) local;

create index I_DISP_ORDER_01 on DISP_ORDER (HOSP_CODE, DISP_DATE) local;

create index I_DISP_ORDER_ITEM_01 on DISP_ORDER_ITEM (DELIVERY_ITEM_ID) local;

create index I_FM_RPT_STAT_01 on FM_RPT_STAT (YEAR, MONTH, CLUSTER_CODE) tablespace PMS_INDX_01;


--//@UNDO
drop index I_PATIENT_01;
drop index I_PATIENT_02;

drop index I_MED_ORDER_01;
drop index I_MED_ORDER_02;
drop index I_MED_ORDER_03;
drop index I_MED_ORDER_04;

drop index I_DISP_ORDER_01;

drop index I_DISP_ORDER_ITEM_01;

drop index I_FM_RPT_STAT_01;
--//