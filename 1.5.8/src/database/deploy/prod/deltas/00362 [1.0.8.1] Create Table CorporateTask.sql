create table CORPORATE_TASK (
    TASK_NAME varchar2(100) not null,
    TASK_DATE timestamp null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_CORPORATE_TASK primary key (TASK_NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CORPORATE_TASK is 'Task control of corporate to ensure only one WebLogic node running the listed tasks';
comment on column CORPORATE_TASK.TASK_NAME is 'Task name';
comment on column CORPORATE_TASK.TASK_DATE is 'Task execution date';
comment on column CORPORATE_TASK.CREATE_USER is 'Created user';
comment on column CORPORATE_TASK.CREATE_DATE is 'Created date';
comment on column CORPORATE_TASK.UPDATE_USER is 'Last updated user';
comment on column CORPORATE_TASK.UPDATE_DATE is 'Last updated date';


--//@UNDO
drop table CORPORATE_TASK;
--//