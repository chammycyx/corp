alter table REFILL_SCHEDULE add REFILL_INTERVAL number(10) null;
alter table REFILL_SCHEDULE add REFILL_INTERVAL_LAST number(10) null;

comment on column REFILL_SCHEDULE.REFILL_INTERVAL is 'Current refill interval';
comment on column REFILL_SCHEDULE.REFILL_INTERVAL_LAST is 'Last refill interval';

alter table PHARM_ORDER drop column REFILL_INTERVAL;
alter table PHARM_ORDER drop column REFILL_INTERVAL_LAST;

--//@UNDO
alter table REFILL_SCHEDULE drop column REFILL_INTERVAL;
alter table REFILL_SCHEDULE drop column REFILL_INTERVAL_LAST;

alter table PHARM_ORDER add REFILL_INTERVAL number(10) null;
alter table PHARM_ORDER add REFILL_INTERVAL_LAST number(10) null;

comment on column PHARM_ORDER.REFILL_INTERVAL is 'Current refill interval';
comment on column PHARM_ORDER.REFILL_INTERVAL_LAST is 'Last refill interval';
--//