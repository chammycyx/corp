create table BATCH_JOB_STATE (
    ID number(19) not null,
    JOB_NAME varchar2(100) not null,
    BATCH_DATE date not null,
    HOSP_CODE varchar2(3) null,
    WORKSTORE_CODE varchar2(4) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_BATCH_JOB_STATE primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_BATCH_JOB_STATE_01 unique (JOB_NAME, BATCH_DATE, HOSP_CODE, WORKSTORE_CODE) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (maxvalue) tablespace P_PMS_DISP_DATA_2012);


comment on table  BATCH_JOB_STATE is 'Batch Job State';
comment on column BATCH_JOB_STATE.ID is 'BatchJobState id';
comment on column BATCH_JOB_STATE.JOB_NAME is 'Job Name';
comment on column BATCH_JOB_STATE.BATCH_DATE is 'Batch Date';
comment on column BATCH_JOB_STATE.HOSP_CODE is 'Hospital Code';
comment on column BATCH_JOB_STATE.WORKSTORE_CODE is 'Workstore Code';
comment on column BATCH_JOB_STATE.CREATE_USER is 'Created user';
comment on column BATCH_JOB_STATE.CREATE_DATE is 'Created date';
comment on column BATCH_JOB_STATE.UPDATE_USER is 'Last updated user';
comment on column BATCH_JOB_STATE.UPDATE_DATE is 'Last updated date';

create sequence SQ_BATCH_JOB_STATE increment by 50 start with 10000000000049;


--//@UNDO
drop sequence SQ_BATCH_JOB_STATE;

drop table BATCH_JOB_STATE;
--//
