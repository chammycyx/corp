-- Sys

create sequence SQ_AUDIT_LOG start with 10000000000000;

-- Corp

create sequence SQ_FM_RPT_STAT increment by 50 start with 10000000000049;
create sequence SQ_PATIENT_EPISODE increment by 50 start with 10000000000049;
create sequence SQ_OWNERSHIP increment by 50 start with 10000000000049;

-- Ref

create sequence SQ_CORPORATE_PROP increment by 50 start with 10000000000049;
create sequence SQ_HOSPITAL_PROP increment by 50 start with 10000000000049;
create sequence SQ_PROP increment by 50 start with 10000000000049;
create sequence SQ_WORKSTORE_PROP increment by 50 start with 10000000000049;

-- Disp

create sequence SQ_CAPD_VOUCHER increment by 50 start with 10000000000049;
create sequence SQ_CAPD_VOUCHER_ITEM increment by 50 start with 10000000000049;
create sequence SQ_DISP_ORDER increment by 50 start with 80000000000049;
create sequence SQ_DISP_ORDER_ITEM increment by 50 start with 80000000000049;
create sequence SQ_MED_ORDER_FM increment by 50 start with 10000000000049;
create sequence SQ_INVOICE increment by 50 start with 80000000000049;
create sequence SQ_INVOICE_ITEM increment by 50 start with 80000000000049;
create sequence SQ_MED_CASE increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER_ITEM_ALERT increment by 50 start with 10000000000049;
create sequence SQ_PATIENT increment by 50 start with 10000000000049;
create sequence SQ_PHARM_ORDER increment by 50 start with 80000000000049;
create sequence SQ_PHARM_ORDER_ITEM increment by 50 start with 80000000000049;
create sequence SQ_REFILL_SCHEDULE_ID increment by 50 start with 80000000000049;
create sequence SQ_REFILL_SCHEDULE_ITEM increment by 50 start with 80000000000049;
create sequence SQ_TICKET increment by 50 start with 80000000000049;

--//@UNDO
-- Sys

drop sequence SQ_AUDIT_LOG;

-- Corp

drop sequence SQ_FM_RPT_STAT;
drop sequence SQ_PATIENT_EPISODE;
drop sequence SQ_OWNERSHIP;

-- Ref

drop sequence SQ_CORPORATE_PROP;
drop sequence SQ_HOSPITAL_PROP;
drop sequence SQ_PROP;
drop sequence SQ_WORKSTORE_PROP;

-- Disp

drop sequence SQ_CAPD_VOUCHER;
drop sequence SQ_CAPD_VOUCHER_ITEM;
drop sequence SQ_DISP_ORDER;
drop sequence SQ_DISP_ORDER_ITEM;
drop sequence SQ_MED_ORDER_FM;
drop sequence SQ_INVOICE;
drop sequence SQ_INVOICE_ITEM;
drop sequence SQ_MED_CASE;
drop sequence SQ_MED_ORDER;
drop sequence SQ_MED_ORDER_ITEM;
drop sequence SQ_MED_ORDER_ITEM_ALERT;
drop sequence SQ_PATIENT;
drop sequence SQ_PHARM_ORDER;
drop sequence SQ_PHARM_ORDER_ITEM;
drop sequence SQ_REFILL_SCHEDULE_ID;
drop sequence SQ_REFILL_SCHEDULE_ITEM;
drop sequence SQ_TICKET;

--//