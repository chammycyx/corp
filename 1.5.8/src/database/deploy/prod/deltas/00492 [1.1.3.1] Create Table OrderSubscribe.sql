create table ORDER_SUBSCRIBE (
    ID number(19) not null,
    ORDER_NUM varchar2(12) not null,
    SYSTEM varchar2(12) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_ORDER_SUBSCRIBE primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_ORDER_SUBSCRIBE_01 unique (ORDER_NUM, SYSTEM) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  ORDER_SUBSCRIBE is 'Order Subscribe';
comment on column ORDER_SUBSCRIBE.ID is 'OrderSubscribe id';
comment on column ORDER_SUBSCRIBE.ORDER_NUM is 'Medication order number';
comment on column ORDER_SUBSCRIBE.SYSTEM is 'Subscriber system';
comment on column ORDER_SUBSCRIBE.STATUS is 'Record status';
comment on column ORDER_SUBSCRIBE.CREATE_USER is 'Created user';
comment on column ORDER_SUBSCRIBE.CREATE_DATE is 'Created date';
comment on column ORDER_SUBSCRIBE.UPDATE_USER is 'Last updated user';
comment on column ORDER_SUBSCRIBE.UPDATE_DATE is 'Last updated date';

create sequence SQ_ORDER_SUBSCRIBE increment by 50 start with 10000000000049;


--//@UNDO
drop sequence SQ_ORDER_SUBSCRIBE;

drop table ORDER_SUBSCRIBE;
--//
