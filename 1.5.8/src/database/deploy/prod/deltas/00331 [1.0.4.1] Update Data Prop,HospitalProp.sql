update PROP set DESCRIPTION='Last transaction date of update Patient Episode' where ID=12002;
update PROP set DESCRIPTION='Limited number of transactions allowed for update Patient Episode' where ID=12003;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (12004,'batch.dayEnd.enabled','Enable Day End','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,12004,'Y',HOSP_CODE,12004,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION='Last transaction date of patient episode Batch job' where ID=12002;
update PROP set DESCRIPTION='Limited number of transactions allowed for patient episode Batch job' where ID=12003;

delete from HOSPITAL_PROP where PROP_ID=12004;
delete from PROP where ID=12004;
--//
