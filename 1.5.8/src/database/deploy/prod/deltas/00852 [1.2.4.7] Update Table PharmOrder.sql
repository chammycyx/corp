alter table PHARM_ORDER add ALLOW_COMMENT_BEFORE_FLAG number(1) null;
comment on column PHARM_ORDER.ALLOW_COMMENT_BEFORE_FLAG is 'Boolean flag indicates allow post-dispensing comment before';

--//@UNDO
alter table PHARM_ORDER drop column ALLOW_COMMENT_BEFORE_FLAG;
--//