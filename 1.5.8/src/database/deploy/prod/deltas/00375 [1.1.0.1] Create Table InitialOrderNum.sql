create table INITIAL_ORDER_NUM (
    PAT_HOSP_CODE varchar2(3) not null,
    ORDER_NUM number(10) not null,
    constraint PK_INITIAL_ORDER_NUM primary key (PAT_HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  INITIAL_ORDER_NUM is 'Initial order number of MOE';
comment on column INITIAL_ORDER_NUM.PAT_HOSP_CODE is 'Patient hospital code';
comment on column INITIAL_ORDER_NUM.ORDER_NUM is 'Order number';

--//@UNDO
drop table INITIAL_ORDER_NUM;
--//