insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (12005,'batch.patientTrx.lastTrxDate','[to be obsolete] Last transaction date of update cancel discharge and cancel transfer','yyyyMMdd.HHmmssSSS','DT',0,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (12005,12005,null,12005,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (12006,'batch.patientTrx.limit','[to be obsolete] Limited number of transactions allowed for update cancel discharge and cancel transfer','1-5000','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (12006,12006,'1000',12006,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (12007,'batch.patientTrx.update.interval','[to be obsolete] Interval (in minutes) to update cancel discharge and cancel transfer','5-60','mm',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (12007,12007,'15',12007,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
delete from CORPORATE_PROP where PROP_ID in (12005,12006,12007);
delete from PROP where ID in (12005,12006,12007);