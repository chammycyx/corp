alter table SEQ_TABLE add UPDATE_DATE timestamp (6) null;

update SEQ_TABLE set UPDATE_DATE = sysdate;

alter table SEQ_TABLE modify UPDATE_DATE not null;

comment on column SEQ_TABLE.UPDATE_DATE is 'Last updated date';

--//@UNDO
alter table SEQ_TABLE drop column UPDATE_DATE;
--//