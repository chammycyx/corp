-- CORP

alter table CDDH_SPECIALTY_MAPPING add constraint FK_CDDH_SPECIALTY_MAPPING_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

alter table CDDH_WORKSTORE_GROUP_HOS add constraint FK_CDDH_WORKSTORE_GROUP_HOS_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);
alter table CDDH_WORKSTORE_GROUP_HOS add constraint FK_CDDH_WORKSTORE_GROUP_HOS_02 foreign key (CDDH_WORKSTORE_GROUP_CODE) references CDDH_WORKSTORE_GROUP (CDDH_WORKSTORE_GROUP_CODE);

alter table HOSPITAL add constraint FK_HOSPITAL_01 foreign key (CLUSTER_CODE) references HOSPITAL_CLUSTER (CLUSTER_CODE);

alter table HOSPITAL_CLUSTER_TASK add constraint FK_HOSPITAL_CLUSTER_TASK_01 foreign key (CLUSTER_CODE) references HOSPITAL_CLUSTER (CLUSTER_CODE);

alter table HOSPITAL_MAPPING add constraint FK_HOSPITAL_MAPPING_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

alter table INSTITUTION add constraint FK_INSTITUTION_01 foreign key (CLUSTER_CODE) references HOSPITAL_CLUSTER (CLUSTER_CODE);

alter table ITEM_BIN add constraint FK_ITEM_BIN_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

alter table WORKSTORE add constraint FK_WORKSTORE_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);
alter table WORKSTORE add constraint FK_WORKSTORE_02 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);
alter table WORKSTORE add constraint FK_WORKSTORE_03 foreign key (CDDH_WORKSTORE_GROUP_CODE) references CDDH_WORKSTORE_GROUP (CDDH_WORKSTORE_GROUP_CODE);

alter table WORKSTORE_GROUP add constraint FK_WORKSTORE_GROUP_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

alter table WORKSTORE_GROUP_MAPPING add constraint FK_WORKSTORE_GROUP_MAPPING_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);

alter table MANUAL_INVOICE_NUM_RANGE add constraint FK_MANUAL_INVOICE_NUM_RANGE_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

-- PHS

alter table CAPD_SUPPLIER_ITEM add constraint FK_CAPD_SUPPLIER_ITEM_01 foreign key (SUPPLIER_CODE) references SUPPLIER (SUPPLIER_CODE);

alter table CMM_AVAILABLE_DRUG add constraint FK_CMM_AVAILABLE_DRUG_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

alter table PATIENT_CAT add constraint FK_PATIENT_CAT_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

alter table SPECIALTY add constraint FK_SPECIALTY_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

alter table WARD add constraint FK_WARD_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

alter table WARD_STOCK add constraint FK_WARD_STOCK_01 foreign key (INST_CODE,WARD_CODE) references WARD (INST_CODE,WARD_CODE);

-- Ref

alter table CORPORATE_PROP add constraint FK_CORPORATE_PROP_01 foreign key (PROP_ID) references PROP (ID);

alter table HOSPITAL_PROP add constraint FK_HOSPITAL_PROP_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);
alter table HOSPITAL_PROP add constraint FK_HOSPITAL_PROP_02 foreign key (PROP_ID) references PROP (ID);

alter table PRN_ROUTE add constraint FK_PRN_ROUTE_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

alter table WORKSTORE_PROP add constraint FK_WORKSTORE_PROP_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);
alter table WORKSTORE_PROP add constraint FK_WORKSTORE_PROP_02 foreign key (PROP_ID) references PROP (ID);


-- Disp

alter table CAPD_VOUCHER add constraint FK_CAPD_VOUCHER_01 foreign key (DISP_ORDER_ID) references DISP_ORDER (ID);

alter table CAPD_VOUCHER_ITEM add constraint FK_CAPD_VOUCHER_ITEM_01 foreign key (CAPD_VOUCHER_ID) references CAPD_VOUCHER (ID);
alter table CAPD_VOUCHER_ITEM add constraint FK_CAPD_VOUCHER_ITEM_02 foreign key (DISP_ORDER_ITEM_ID) references DISP_ORDER_ITEM (ID);

alter table DISP_ORDER add constraint FK_DISP_ORDER_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);
alter table DISP_ORDER add constraint FK_DISP_ORDER_02 foreign key (PHARM_ORDER_ID) references PHARM_ORDER (ID);
alter table DISP_ORDER add constraint FK_DISP_ORDER_03 foreign key (TICKET_ID) references TICKET (ID);

alter table DISP_ORDER_ITEM add constraint FK_DISP_ORDER_ITEM_01 foreign key (DISP_ORDER_ID) references DISP_ORDER (ID);
alter table DISP_ORDER_ITEM add constraint FK_DISP_ORDER_ITEM_02 foreign key (PHARM_ORDER_ITEM_ID) references PHARM_ORDER_ITEM (ID);

alter table MED_ORDER_FM add constraint FK_MED_ORDER_FM_01 foreign key (MED_ORDER_ID) references MED_ORDER (ID);

alter table INVOICE add constraint FK_INVOICE_01 foreign key (DISP_ORDER_ID) references DISP_ORDER (ID);

alter table INVOICE_ITEM add constraint FK_INVOICE_ITEM_01 foreign key (INVOICE_ID) references INVOICE (ID);
alter table INVOICE_ITEM add constraint FK_INVOICE_ITEM_02 foreign key (DISP_ORDER_ITEM_ID) references DISP_ORDER_ITEM (ID);

alter table MED_ORDER add constraint FK_MED_ORDER_01 foreign key (PATIENT_ID) references PATIENT (ID);
alter table MED_ORDER add constraint FK_MED_ORDER_02 foreign key (MED_CASE_ID) references MED_CASE (ID);
alter table MED_ORDER add constraint FK_MED_ORDER_03 foreign key (TICKET_ID) references TICKET (ID);
alter table MED_ORDER add constraint FK_MED_ORDER_04 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

alter table MED_ORDER_ITEM add constraint FK_MED_ORDER_ITEM_01 foreign key (MED_ORDER_ID) references MED_ORDER (ID);

alter table MED_ORDER_ITEM_ALERT add constraint FK_MED_ORDER_ITEM_ALERT_01 foreign key (MED_ORDER_ITEM_ID) references MED_ORDER_ITEM (ID);

alter table PHARM_ORDER add constraint FK_PHARM_ORDER_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);
alter table PHARM_ORDER add constraint FK_PHARM_ORDER_02 foreign key (MED_ORDER_ID) references MED_ORDER (ID);
alter table PHARM_ORDER add constraint FK_PHARM_ORDER_03 foreign key (MED_CASE_ID) references MED_CASE (ID);

alter table PHARM_ORDER_ITEM add constraint FK_PHARM_ORDER_ITEM_01 foreign key (MED_ORDER_ITEM_ID) references MED_ORDER_ITEM (ID);
alter table PHARM_ORDER_ITEM add constraint FK_PHARM_ORDER_ITEM_02 foreign key (PHARM_ORDER_ID) references PHARM_ORDER (ID);

alter table REFILL_SCHEDULE add constraint FK_REFILL_SCHEDULE_01 foreign key (PHARM_ORDER_ID) references PHARM_ORDER (ID);
alter table REFILL_SCHEDULE add constraint FK_REFILL_SCHEDULE_02 foreign key (DISP_ORDER_ID) references DISP_ORDER (ID);
alter table REFILL_SCHEDULE add constraint FK_REFILL_SCHEDULE_03 foreign key (TICKET_ID) references TICKET (ID);
alter table REFILL_SCHEDULE add constraint FK_REFILL_SCHEDULE_04 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

alter table REFILL_SCHEDULE_ITEM add constraint FK_REFILL_SCHEDULE_ITEM_01 foreign key (PHARM_ORDER_ITEM_ID) references PHARM_ORDER_ITEM (ID);
alter table REFILL_SCHEDULE_ITEM add constraint FK_REFILL_SCHEDULE_ITEM_02 foreign key (REFILL_SCHEDULE_ID) references REFILL_SCHEDULE (ID);
alter table REFILL_SCHEDULE_ITEM add constraint FK_REFILL_SCHEDULE_ITEM_03 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

alter table TICKET add constraint FK_TICKET_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

--//@UNDO

-- CORP

alter table CDDH_SPECIALTY_MAPPING drop constraint FK_CDDH_SPECIALTY_MAPPING_01;

alter table CDDH_WORKSTORE_GROUP_HOS drop constraint FK_CDDH_WORKSTORE_GROUP_HOS_01;
alter table CDDH_WORKSTORE_GROUP_HOS drop constraint FK_CDDH_WORKSTORE_GROUP_HOS_02;

alter table HOSPITAL drop constraint FK_HOSPITAL_01;

alter table HOSPITAL_CLUSTER_TASK drop constraint FK_HOSPITAL_CLUSTER_TASK_01;

alter table HOSPITAL_MAPPING drop constraint FK_HOSPITAL_MAPPING_01;

alter table INSTITUTION drop constraint FK_INSTITUTION_01;

alter table ITEM_BIN drop constraint FK_ITEM_BIN_01;

alter table WORKSTORE drop constraint FK_WORKSTORE_01;
alter table WORKSTORE drop constraint FK_WORKSTORE_02;
alter table WORKSTORE drop constraint FK_WORKSTORE_03;

alter table WORKSTORE_GROUP drop constraint FK_WORKSTORE_GROUP_01;

alter table WORKSTORE_GROUP_MAPPING drop constraint FK_WORKSTORE_GROUP_MAPPING_01;

alter table MANUAL_INVOICE_NUM_RANGE drop constraint FK_MANUAL_INVOICE_NUM_RANGE_01;

-- PHS

alter table CAPD_SUPPLIER_ITEM drop constraint FK_CAPD_SUPPLIER_ITEM_01;

alter table CMM_AVAILABLE_DRUG drop constraint FK_CMM_AVAILABLE_DRUG_01;

alter table PATIENT_CAT drop constraint FK_PATIENT_CAT_01;

alter table SPECIALTY drop constraint FK_SPECIALTY_01;

alter table WARD drop constraint FK_WARD_01;

alter table WARD_STOCK drop constraint FK_WARD_STOCK_01;

-- Ref

alter table CORPORATE_PROP drop constraint FK_CORPORATE_PROP_01;

alter table HOSPITAL_PROP drop constraint FK_HOSPITAL_PROP_01;
alter table HOSPITAL_PROP drop constraint FK_HOSPITAL_PROP_02;

alter table PRN_ROUTE drop constraint FK_PRN_ROUTE_01;

alter table WORKSTORE_PROP drop constraint FK_WORKSTORE_PROP_01;
alter table WORKSTORE_PROP drop constraint FK_WORKSTORE_PROP_02;

-- Disp

alter table CAPD_VOUCHER drop constraint FK_CAPD_VOUCHER_01;

alter table CAPD_VOUCHER_ITEM drop constraint FK_CAPD_VOUCHER_ITEM_01;
alter table CAPD_VOUCHER_ITEM drop constraint FK_CAPD_VOUCHER_ITEM_02;

alter table DISP_ORDER drop constraint FK_DISP_ORDER_01;
alter table DISP_ORDER drop constraint FK_DISP_ORDER_02;

alter table DISP_ORDER drop constraint FK_DISP_ORDER_03;

alter table DISP_ORDER_ITEM drop constraint FK_DISP_ORDER_ITEM_01;
alter table DISP_ORDER_ITEM drop constraint FK_DISP_ORDER_ITEM_02;

alter table MED_ORDER_FM drop constraint FK_MED_ORDER_FM_01;

alter table INVOICE drop constraint FK_INVOICE_01;

alter table INVOICE_ITEM drop constraint FK_INVOICE_ITEM_01;
alter table INVOICE_ITEM drop constraint FK_INVOICE_ITEM_02;

alter table MED_ORDER drop constraint FK_MED_ORDER_01;
alter table MED_ORDER drop constraint FK_MED_ORDER_02;
alter table MED_ORDER drop constraint FK_MED_ORDER_03;
alter table MED_ORDER drop constraint FK_MED_ORDER_04;

alter table MED_ORDER_ITEM drop constraint FK_MED_ORDER_ITEM_01;

alter table MED_ORDER_ITEM_ALERT drop constraint FK_MED_ORDER_ITEM_ALERT_01;

alter table PHARM_ORDER drop constraint FK_PHARM_ORDER_01;
alter table PHARM_ORDER drop constraint FK_PHARM_ORDER_02;
alter table PHARM_ORDER drop constraint FK_PHARM_ORDER_03;

alter table PHARM_ORDER_ITEM drop constraint FK_PHARM_ORDER_ITEM_01;
alter table PHARM_ORDER_ITEM drop constraint FK_PHARM_ORDER_ITEM_02;

alter table REFILL_SCHEDULE drop constraint FK_REFILL_SCHEDULE_01;
alter table REFILL_SCHEDULE drop constraint FK_REFILL_SCHEDULE_02;
alter table REFILL_SCHEDULE drop constraint FK_REFILL_SCHEDULE_03;

alter table REFILL_SCHEDULE_ITEM drop constraint FK_REFILL_SCHEDULE_ITEM_01;
alter table REFILL_SCHEDULE_ITEM drop constraint FK_REFILL_SCHEDULE_ITEM_02;

alter table TICKET drop constraint FK_TICKET_01;

--//