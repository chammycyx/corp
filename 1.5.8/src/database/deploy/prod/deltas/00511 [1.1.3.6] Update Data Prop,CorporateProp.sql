update PROP set DESCRIPTION = 'Last transaction date of Patient Episode update' where ID=12002;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (12008,'batch.unknownPatient.duration','Duration (in days) of Unknown Patient update','1-7','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (12008,12008,'7',12008,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
update PROP set DESCRIPTION = 'Last transaction date of update Patient Episode' where ID=12002;

delete from CORPORATE_PROP where PROP_ID = 12008;
delete from PROP where ID = 12008;