alter table MED_ORDER add PRESC_TYPE_UPDATE_DATE timestamp;

comment on column MED_ORDER.PRESC_TYPE_UPDATE_DATE is 'Prescription type update date';

--//@UNDO
alter table MED_ORDER drop column PRESC_TYPE_UPDATE_DATE;
--//