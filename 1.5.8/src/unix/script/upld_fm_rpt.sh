#!/bin/ksh
##############################################################################################
#
# Script	:	upld_fm_rpt.sh
#
# Purpose	:	Upload fm report to destination server
#
# History	:	Date		Who			Description
#			20120130	Michael Cheng		Initial version
#
##############################################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name <batchDate YYYYMMDD>"
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP M_UPLD_FM_RPT batchDate=Date:$batchDate 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
