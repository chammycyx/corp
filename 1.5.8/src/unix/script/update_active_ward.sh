#!/bin/ksh
##############################################################################################
#
# Script	:	update_active_ward.sh
#
# Purpose	:	Retrieve ActiveWard for PMS & CORP
#
# History	:	Date		Who			Description
#			20120105	Winnie Cheng		Initial version
#			20140325	Charis Leung		Add App Code parameter
#
##############################################################################################

script_name=`basename $0`
param_count=0

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name"
	exit 1
fi

echo
echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_ACTIVE_WARD 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo
echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status

