#!/bin/ksh
##############################################################################################
#
# Script	:	do_day_end.sh
#
# Purpose	:	PMS Day End
#
# History	: 	Date		Who			Description
#			20111028	David Lee		Initial version
#			20120723	Stephen 		Remove parameter 
#			20130423	Stephen 		Add cluster code parameter 
#			20140320	Charis Leung		Add App Code parameter
#
##############################################################################################


script_name=`basename $0`
param_count=2

if [ $# -ne $param_count ]
then
	echo "Usage:	$script_name <batchDate YYYYMMDD> <clusterCode>" 
	exit 1
fi

batchDate=$1
clusterCode=$2

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_DAY_END batchDate=Date:$batchDate,clusterCode=$clusterCode 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
