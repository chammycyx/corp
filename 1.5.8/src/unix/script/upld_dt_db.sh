#!/bin/ksh
##############################################################################################
#
# Script	:	upld_dt_db.sh
#
# Purpose	:	Upload Access downtime databases to file server
#
# History	:	Date		Who			Description
#			20111104	Alex Kwan		Initial version
#
##############################################################################################


script_name=`basename $0`
param_count=2

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name <batchDate YYYYMMDD> <dtDbDir>"
	exit 1
fi

batchDate=$1
dtDbDir=$2

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_UPLD_DT_DB batchDate=Date:$batchDate,dtDbDir=$dtDbDir 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
