#!/bin/ksh
##############################################################################################
#
# Script	:	get_bc_rmi_url.sh
#
# Purpose	:	return RMI URL of the corresponding Batch Controller 
#
# History	:	Date		Who			Description
#			2014-03-20	Charis Leung		Initial version
#
##############################################################################################

script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:	$script_name <App Code>"
	exit 1
fi

app_code=$1
conf_file=$SCRIPTPATH/conf/app-server.properties

host_name=`grep -w $app_code $conf_file | head -n 1 | cut -f2`
if [ -z "$host_name" ]
then
	echo ERROR: $app_code not exists in app-server.properties
	exit 1
fi

bc_port=`grep -w BC $conf_file | head -n 1 | cut -f3`
if [ -z "$bc_port" ]
then
	echo ERROR: BC not exists in app-server.properties
	exit 1
fi

result=`ping -q -c 1 -w 2 $host_name`
status=$?

if [ $status -eq 0 ]
then
	bc_ip=`echo $result | grep "^PING" | sed -e "s/).*//" | sed -e "s/.*(//"`
	bc_url=t3://$bc_ip:$bc_port
	echo $bc_url
fi

exit $status 
