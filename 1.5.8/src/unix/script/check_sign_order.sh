#!/bin/ksh
##############################################################################################
#
# Script	:	check_sign_order.sh	
#
# Purpose	:	Check order signature coun	
#
# History	:	Date		Who		Description
#			20131127	Terry Wong	Initial version
#
##############################################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name <batchDate YYYYMMDD>"
	exit 1
fi

batchDate=$1

echo
echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_CHECK_SIGN_ORDER batchDate=Date:$batchDate 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo
echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
