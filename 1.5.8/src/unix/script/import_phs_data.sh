#!/bin/ksh
##############################################################################################
#
# Script Name	:	import_phs_data.sh
#
# Purpose	:	import PHS Data to PMS
#
# History	:	Date		Who			Description
# 			20111111	Bowie Wong		Initial version
#			20140320	Charis Leung		Add App Code parameter
#
##############################################################################################


script_name=`basename $0`
param_count=0

if [ $# -ne $param_count ]
then
        echo "Usage:    $script_name"
        exit 1
fi

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_PHS_DATA 2>&1
status=$?

if [ $status -eq 0 ]
then
        echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
        echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
