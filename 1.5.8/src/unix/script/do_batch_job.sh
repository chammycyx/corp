#!/bin/ksh
##############################################################################################
#
# Script	:	do_batch_job.sh
#
# Purpose	:	trigger batch job hosted in application server
#
# History	: 	Date		Who			Description
#			20111010	Charis Leung		Initial version
#			20140320	Charis Leung		Get Batch Controller RMI URL by script
#								Add App Code parameter
#
##############################################################################################

script_name=`basename $0`
param_count=2

if [ $# -lt $param_count ]
then
	echo "Usage:	$script_name <App Code> <Job Id> [Job Parameter(s)]"
	exit 1
fi

app_code=$1
job_id=$2
job_param=$3

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: App Code: $app_code"
echo `date $LOG_DT_FMT`:" $script_name: Job Id: $job_id"
echo `date $LOG_DT_FMT`:" $script_name: Job Parameter(s): $job_param"

url=`$SCRIPTPATH/get_bc_rmi_url.sh $app_code`
status=$?
if [ $status -ne 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: $url"
else
	$JAVA_HOME/bin/java -jar $JAVAPATH/fmk-pms-batch.jar -url $url -u $APP_SERVER_USER -p $APP_SERVER_PWD $job_id $job_param
	status=$?
fi

if [ $status -eq 0 ]
then 
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
