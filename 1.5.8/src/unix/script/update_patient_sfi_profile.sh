#!/bin/ksh
##############################################################################################
#
# Script	:	update_patient_sfi_profile.sh
#
# Purpose	:	Update patinet sfi profile
#
# History	:	Date		Who					Description
#				20120105	Winnie Cheng		Initial version
#
##############################################################################################


script_name=`basename $0`
param_count=0

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name"
	exit 1
fi

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_PATIENT_SFI_PROFILE 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
