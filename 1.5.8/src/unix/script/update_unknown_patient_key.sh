#!/bin/ksh
##############################################################################################
#
# Script	:	update_unknown_patient_key.sh
#
# Purpose	:	Update unknown patient key
#
# History	:	Date		Who			Description
# 			20120303	David Lee		Initial version
# 			20120914	Winnie Cheng		Add parameter		      
#			20140320	Charis Leung		Add App Code parameter
#
##############################################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name <batchDate YYYYMMDD>"
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_UNKNOWN_PATIENT_KEY batchDate=Date:$batchDate 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
