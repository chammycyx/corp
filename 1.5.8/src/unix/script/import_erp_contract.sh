#!/bin/ksh
####################################################################
#
# Script Name  :   import_erp_contract.sh
#
# Purpose      :   import ERP contract to DQA
#
# History      :   Date        Who         Description
#                  20101209    Terry Wong  Initial version
#                  20170818    Ada Ko      Use CORP batch controller
#
####################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:	$script_name <batchDate YYYYMMDD>" 
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_ERP_CONTRACT 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
