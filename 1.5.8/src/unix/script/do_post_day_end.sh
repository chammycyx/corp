#!/bin/ksh
##############################################################################################
#
# Script	:	do_post_dayend.sh	
#
# Purpose	:	Consolidate files generated by do_day_end.sh	
#
# History	: 	Date		Who			Description
#			20111028    	David Lee		Initial version
#			20140320	Charis Leung		Add App Code parameter
#
##############################################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:	$script_name <batchDate YYYYMMDD>" 
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_POST_DAY_END batchDate=Date:$batchDate 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
