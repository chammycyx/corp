#!/bin/ksh
##############################################################################################
#
# Script	:	gen_dt_db.sh	
#
# Purpose	:	Generate Access downtime database by hospital clusters 
#
# History	: 	Date		Who			Description
#			20111017	Alex Kwan		Initial version
#			20120113    	Alex Kwan	   	Update jobId
#			20140320	Charis Leung		Add App Code parameter
#
##############################################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:	$script_name <batchDate YYYYMMDD>" 
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_DT_DB batchDate=Date:$batchDate 2>&1
status=$?

if [ $status -eq 0 ]
then
    echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
