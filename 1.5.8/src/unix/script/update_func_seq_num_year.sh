#!/bin/ksh
####################################################################
#
# Script Name  :   update_func_seq_num_year.sh
#
# Purpose      :   update the yaer column of func_seq_num yearly
#
# History      :   Date        Who         Description
#                  20120110    David Pang  Initial version
#                  20170818    Ada Ko      Use CORP batch controller
#
####################################################################

script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:	$script_name <batchDate YYYYMMDD>" 
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP Y_FUNC_SEQ_NUM_YEAR 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
