#!/bin/ksh
##############################################################################################
#
# Script	:	gen_wait_time_rpt_stat.sh
#
# Purpose	:	Generate waiting time statistics for EDS Waiting Time Audit Report
#
# History	:	Date		Who				Description
# 				20150901	Terry Wong		Initial version
#				20150918	Terry Wong		rename D_WAIT_TIME_AUDIT_STAT to D_WAIT_TIME_RPT_STAT
#
##############################################################################################


script_name=`basename $0`
param_count=1

if [ $# -ne $param_count ]
then
	echo "Usage:    $script_name <batchDate YYYYMMDD>"
	exit 1
fi

batchDate=$1

echo `date $LOG_DT_FMT`:" $script_name: Start"
echo `date $LOG_DT_FMT`:" $script_name: Parameter(s): $*"

$SCRIPTPATH/do_batch_job.sh CORP D_WAIT_TIME_RPT_STAT batchDate=Date:$batchDate 2>&1
status=$?

if [ $status -eq 0 ]
then
	echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
	echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo `date $LOG_DT_FMT`:" $script_name: End"

exit $status
