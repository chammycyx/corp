@echo off
if "%DATABASE_ID%" == "" (
	set DATABASE_ID=%WORKSTATION_ID%
) 
@echo on

call mvn clean install %*