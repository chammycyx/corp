package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmMedicalOfficer;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmMedicalOfficerCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmMedicalOfficerCacher extends BaseCacher implements DmMedicalOfficerCacherInf{
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

    private Map<WorkstorePK, InnerCacher> cacherMap = new HashMap<WorkstorePK, InnerCacher>();	
	    	 
	@Override
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}	
    
	public DmMedicalOfficer getDmMedicalOfficerByMedicalOfficerCode(Workstore workstore, String medicalOfficerCode) {
		return (DmMedicalOfficer) this.getCacher(workstore).get(medicalOfficerCode);
	}
	
	public List<DmMedicalOfficer> getDmMedicalOfficerList(Workstore workstore) {
		return (List<DmMedicalOfficer>) this.getCacher(workstore).getAll();
	}

	public List<DmMedicalOfficer> getDmMedicalOfficerListByMedicalOfficerCode(Workstore workstore, String prefixMedicalOfficerCode) {
		List<DmMedicalOfficer> ret = new ArrayList<DmMedicalOfficer>();
		boolean found = false;
		for (DmMedicalOfficer dmMedicalOfficer : getDmMedicalOfficerList(workstore)) {
			if (dmMedicalOfficer.getCompId().getMedicalOfficerCode().startsWith(prefixMedicalOfficerCode)){
				ret.add(dmMedicalOfficer);
				found = true;
			} else {
				if (found){
					break;
				}
			}
		}
		return ret;
	}
		
    private InnerCacher getCacher(Workstore workstore) {
        synchronized (this) {
        	  WorkstorePK key = workstore.getId();
              InnerCacher cacher = cacherMap.get(key);
              if (cacher == null) {
                    cacher = new InnerCacher(key, this.getExpireTime());
                    cacherMap.put(key, cacher);
              }
              return cacher;
        }
  }

	private class InnerCacher extends AbstractCacher<String, DmMedicalOfficer>
	{		
        private WorkstorePK workstorePK = null;

        public InnerCacher(WorkstorePK workstorePK, int expireTime) {
              super(expireTime);
              this.workstorePK = workstorePK;
        }
		
		@Override
		public DmMedicalOfficer create(String medicalOfficerCode) {
			this.getAll();
			return this.internalGet(medicalOfficerCode);
		}

		@Override
		public Collection<DmMedicalOfficer> createAll() {
			return dmsPmsServiceProxy.retrieveDmMedicalOfficerList(workstorePK.getHospCode(), workstorePK.getWorkstoreCode());
		}

		@Override
		public String retrieveKey(DmMedicalOfficer dmMedicalOfficer) {
			return dmMedicalOfficer.getCompId().getMedicalOfficerCode();
		}		
	}
	
	public static DmMedicalOfficerCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmMedicalOfficerCacherInf) Component.getInstance("dmMedicalOfficerCacher", ScopeType.APPLICATION);
    }

		
}
