package hk.org.ha.model.pms.corp.persistence.report;

import hk.org.ha.model.pms.persistence.BaseEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DAY_END_RPT_STAT")
@IdClass(DayEndRptStatPK.class)
public class DayEndRptStat extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", nullable = false, length = 3)
	private String hospCode;
	
	@Id
	@Column(name = "WORKSTORE_CODE", nullable = false, length = 4)
	private String workstoreCode;
	
	@Id
	@Column(name = "BATCH_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date batchDate;

	@Column(name = "ORDER_COUNT", nullable = false)
	private Integer orderCount;

	@Column(name = "ITEM_COUNT", nullable = false)
	private Integer itemCount;
	
	@Column(name = "SAMPLE_ITEM_COUNT", nullable = false)
	private Integer sampleItemCount;
	
	@Column(name = "SFI_ITEM_COUNT", nullable = false)
	private Integer sfiItemCount;

	@Column(name = "SAFETY_NET_ITEM_COUNT", nullable = false)
	private Integer safetyNetItemCount;

	@Column(name = "CAPD_VOUCHER_COUNT", nullable = false)
	private Integer capdVoucherCount;
	
	@Column(name = "CAPD_VOUCHER_ITEM_COUNT", nullable = false)
	private Integer capdVoucherItemCount;
	
	@Column(name = "MP_ITEM_COUNT", nullable = false)
	private Integer mpItemCount;
	
	@Column(name = "MP_SAMPLE_ITEM_COUNT", nullable = false)
	private Integer mpSampleItemCount;
	
	@Column(name = "MP_SFI_ITEM_COUNT", nullable = false)
	private Integer mpSfiItemCount;
	
	@Column(name = "MP_SAFETY_NET_ITEM_COUNT", nullable = false)
	private Integer mpSafetyNetItemCount;

	@Column(name = "MP_WARD_STOCK_ITEM_COUNT", nullable = false)
	private Integer mpWardStockItemCount;
	
	@Column(name = "MP_DANGER_DRUG_ITEM_COUNT", nullable = false)
	private Integer mpDangerDrugItemCount;
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public Integer getItemCount() {
		return itemCount;
	}

	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}

	public Integer getSampleItemCount() {
		return sampleItemCount;
	}

	public void setSampleItemCount(Integer sampleItemCount) {
		this.sampleItemCount = sampleItemCount;
	}

	public Integer getSfiItemCount() {
		return sfiItemCount;
	}

	public void setSfiItemCount(Integer sfiItemCount) {
		this.sfiItemCount = sfiItemCount;
	}

	public Integer getSafetyNetItemCount() {
		return safetyNetItemCount;
	}

	public void setSafetyNetItemCount(Integer safetyNetItemCount) {
		this.safetyNetItemCount = safetyNetItemCount;
	}

	public Integer getCapdVoucherCount() {
		return capdVoucherCount;
	}

	public void setCapdVoucherCount(Integer capdVoucherCount) {
		this.capdVoucherCount = capdVoucherCount;
	}

	public Integer getCapdVoucherItemCount() {
		return capdVoucherItemCount;
	}

	public void setCapdVoucherItemCount(Integer capdVoucherItemCount) {
		this.capdVoucherItemCount = capdVoucherItemCount;
	}

	public Integer getMpItemCount() {
		return mpItemCount;
	}

	public void setMpItemCount(Integer mpItemCount) {
		this.mpItemCount = mpItemCount;
	}

	public Integer getMpSampleItemCount() {
		return mpSampleItemCount;
	}

	public void setMpSampleItemCount(Integer mpSampleItemCount) {
		this.mpSampleItemCount = mpSampleItemCount;
	}

	public Integer getMpSfiItemCount() {
		return mpSfiItemCount;
	}

	public void setMpSfiItemCount(Integer mpSfiItemCount) {
		this.mpSfiItemCount = mpSfiItemCount;
	}

	public Integer getMpSafetyNetItemCount() {
		return mpSafetyNetItemCount;
	}

	public void setMpSafetyNetItemCount(Integer mpSafetyNetItemCount) {
		this.mpSafetyNetItemCount = mpSafetyNetItemCount;
	}

	public Integer getMpWardStockItemCount() {
		return mpWardStockItemCount;
	}

	public void setMpWardStockItemCount(Integer mpWardStockItemCount) {
		this.mpWardStockItemCount = mpWardStockItemCount;
	}

	public Integer getMpDangerDrugItemCount() {
		return mpDangerDrugItemCount;
	}

	public void setMpDangerDrugItemCount(Integer mpDangerDrugItemCount) {
		this.mpDangerDrugItemCount = mpDangerDrugItemCount;
	}
}
