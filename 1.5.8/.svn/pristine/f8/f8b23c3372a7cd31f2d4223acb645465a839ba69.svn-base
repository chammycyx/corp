package hk.org.ha.model.pms.corp.cache;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmAdminTime;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmAdminTimeCacher")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class DmAdminTimeCacher extends BaseCacher implements DmAdminTimeCacherInf {

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;

	private InnerCacher cacher;
	
	@Override
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}
    
	public DmAdminTime getDmAdminTimeByAdminTimeCode(String adminTimeCode) {
		return (DmAdminTime) this.getCacher().get(adminTimeCode);
	}
	
	public List<DmAdminTime> getDmAdminTimeList() {
		return (List<DmAdminTime>) this.getCacher().getAll();
	}
	
	public List<DmAdminTime> getDmAdminTimeListByAdminTimeCode(String prefixAdminTimeCode) {
		List<DmAdminTime> ret = new ArrayList<DmAdminTime>();
		for (DmAdminTime dmAdminTime : getDmAdminTimeList()) {
			if (dmAdminTime.getAdminTimeCode().startsWith(prefixAdminTimeCode)) {
				ret.add(dmAdminTime);
			}
		}
		return ret;
	}
	
    private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
  }

	private class InnerCacher extends AbstractCacher<String, DmAdminTime>
	{		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }
		
		@Override
		public DmAdminTime create(String adminTimeCode) {
			this.getAll();
			return this.internalGet(adminTimeCode);
		}

		@Override
		public Collection<DmAdminTime> createAll() {
			return dmsPmsServiceProxy.retrieveDmAdminTimeList();
		}

		@Override
		public String retrieveKey(DmAdminTime dmAdminTime) {
			return dmAdminTime.getAdminTimeCode();
		}		
	}
	
	public static DmAdminTimeCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmAdminTimeCacherInf) Component.getInstance("dmAdminTimeCacher", ScopeType.APPLICATION);
    }	
}
