package hk.org.ha.model.pms.corp.biz.order;

import hk.org.ha.model.pms.corp.vo.PrnPropertyResult;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
import hk.org.ha.model.pms.udt.disp.DispTrxType;
import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
import hk.org.ha.model.pms.udt.disp.MpTrxType;
import hk.org.ha.model.pms.udt.disp.OpTrxType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.service.biz.pms.og.interfaces.OgServiceJmsRemote;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("ogServiceDispatcher")
@Scope(ScopeType.APPLICATION)
public class OgServiceDispatcher implements OgServiceJmsRemote {

	@In
	private OgServiceJmsRemote ogMoeServiceProxy;

	@In
	private OgServiceJmsRemote ogIpmoeServiceProxy;
	
	@Override
	public void sendOpOrderToCms(OpTrxType opTrxType, DispOrder dispOrder) {
		if (dispOrder.getPharmOrder().getMedOrder().getPrescType() == MedOrderPrescType.MpDischarge ||
				dispOrder.getPharmOrder().getMedOrder().getPrescType() == MedOrderPrescType.MpHomeLeave) {
			ogIpmoeServiceProxy.sendOpOrderToCms(opTrxType, dispOrder);
		} else {
			ogMoeServiceProxy.sendOpOrderToCms(opTrxType, dispOrder);
		}
	}
	
	@Override
	public void sendMpOrderToCms(MpTrxType mpTrxType, MedProfileMoItem medProfileMoItem, Replenishment replenishment) {
		ogIpmoeServiceProxy.sendMpOrderToCms(mpTrxType, medProfileMoItem, replenishment);
	}
	
	@Override
	public void sendOrderToEpr(DispTrxType dispTrxType, DispOrder dispOrder) {
		if (dispOrder.getPharmOrder().getMedOrder().getOrderType() == OrderType.InPatient ||
				dispOrder.getPharmOrder().getMedOrder().getPrescType() == MedOrderPrescType.MpDischarge ||
				dispOrder.getPharmOrder().getMedOrder().getPrescType() == MedOrderPrescType.MpHomeLeave) {
			ogIpmoeServiceProxy.sendOrderToEpr(dispTrxType, dispOrder);
		} else {
			ogMoeServiceProxy.sendOrderToEpr(dispTrxType, dispOrder);
		}
	}
	
	@Override
	public void sendPrnPropertyToCms(OpTrxType opTrxType, PrnPropertyResult prnPropertyResult) {
		ogMoeServiceProxy.sendPrnPropertyToCms(opTrxType, prnPropertyResult);
	}
}
