package hk.org.ha.model.pms.corp.persistence;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.vo.cddh.PatientCddh;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PATIENT_PROFILE")
public class PatientProfile extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	private static final String JAXB_CONTEXT_PATIENT_CDDH = "hk.org.ha.model.pms.vo.cddh";

	@Id
	@Column(name = "PATIENT_ID")
	private Long patientId;
	
	@Lob
	@Column(name = "CDDH_XML")
	private String cddhXml;
	
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PATIENT_ID", insertable = false, updatable = false)
    private Patient patient;
	
	@Transient
	private PatientCddh patientCddh;

	@PostLoad
    public void postLoad() {
		if ( patientCddh == null && cddhXml != null ) {
			JaxbWrapper<PatientCddh> parientCddhJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_PATIENT_CDDH);
			patientCddh = parientCddhJaxbWrapper.unmarshall(cddhXml);
		}
	}
	
	@PrePersist
	@PreUpdate
	public void preSave() {
		if (patientCddh != null) {
			JaxbWrapper<PatientCddh> parientCddhJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_PATIENT_CDDH);
			cddhXml = parientCddhJaxbWrapper.marshall(patientCddh);
		}
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getCddhXml() {
		return cddhXml;
	}

	public void setCddhXml(String cddhXml) {
		this.cddhXml = cddhXml;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public PatientCddh getPatientCddh() {
		return patientCddh;
	}

	public void setPatientCddh(PatientCddh patientCddh) {
		this.patientCddh = patientCddh;
	}
}
